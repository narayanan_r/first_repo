package com.eway.process;
import javax.xml.namespace.QName;
import javax.xml.soap.*;
import javax.xml.transform.*;
import javax.xml.transform.stream.*;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPHeader;

public class SOAPClientJava {


	public static SOAPMessage createSOAPCreateCustomerRequest(String FirstName, String LastName, String Address1, String Address2, String Suburb, String State, String Company, String Postcode, String Country, String Email, String Mobile) throws Exception {
        MessageFactory messageFactory = MessageFactory.newInstance();
        SOAPMessage soapMessage = messageFactory.createMessage();
        SOAPEnvelope env = soapMessage.getSOAPPart().getEnvelope(); 
        SOAPHeader header = env.getHeader();
        
     // create header entry element.
        QName ageQname = new QName("http://www.eway.com.au/gateway/rebill/manageRebill", "eWAYHeader");
     			SOAPHeaderElement soapHeaderElement = header
     					.addHeaderElement(ageQname);

     			// create the first child element and set the value
     			SOAPElement element1 = soapHeaderElement.addChildElement(
     					"eWAYCustomerID");
//     			element1.setValue("91094380");
     			element1.setValue("13881431");

     			// create the second child element
     			SOAPElement element2 = soapHeaderElement.addChildElement("Username");
     			// create and set the attribute of second child element.
     			// set the element value
//     			element2.setValue("rvinothkumarer@gmail.com.sand");
     			element2.setValue("triton@myphysioapp.com.au");
     			
     			SOAPElement element3 = soapHeaderElement.addChildElement(
     					"Password");
//     			element3.setValue("nipple");
     			element3.setValue("#35!Acrobat");
        
        SOAPPart soapPart = soapMessage.getSOAPPart();

        String serverURI = "http://www.eway.com.au/gateway/rebill/manageRebill/CreateRebillCustomer";
        

        // SOAP Envelope
        SOAPEnvelope envelope = soapPart.getEnvelope();
        envelope.addNamespaceDeclaration("xsi", serverURI);
        envelope.addNamespaceDeclaration("xsd", "http://www.w3.org/2001/XMLSchema");
//        envelope.addNamespaceDeclaration("soap", "http://schemas.xmlsoap.org/soap/envelope/");

        /*
        Constructed SOAP Request Message:
        <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:example="http://ws.cdyne.com/">
            <SOAP-ENV:Header/>
            <SOAP-ENV:Body>
                <example:VerifyEmail>
                    <example:email>mutantninja@gmail.com</example:email>
                    <example:LicenseKey>123</example:LicenseKey>
                </example:VerifyEmail>
            </SOAP-ENV:Body>
        </SOAP-ENV:Envelope>
         */

//        Example Create Rebill Customer request
        // SOAP Body
        QName ageQname1 = new QName("http://www.eway.com.au/gateway/rebill/manageRebill", "CreateRebillCustomer");
        SOAPBody soapBody = envelope.getBody();
        SOAPElement soapBodyElem = soapBody.addChildElement(ageQname1);
        SOAPElement soapBodyElem1 = soapBodyElem.addChildElement("customerTitle");
        soapBodyElem1.addTextNode("Mr.");
        SOAPElement soapBodyElem2 = soapBodyElem.addChildElement("customerFirstName");
        soapBodyElem2.addTextNode(FirstName);
        SOAPElement soapBodyElem3 = soapBodyElem.addChildElement("customerLastName");
        soapBodyElem3.addTextNode(LastName);
        SOAPElement soapBodyElem4 = soapBodyElem.addChildElement("customerAddress");
        soapBodyElem4.addTextNode(Address1+" "+Address2);
        SOAPElement soapBodyElem5 = soapBodyElem.addChildElement("customerSuburb");
        soapBodyElem5.addTextNode(Suburb);
        SOAPElement soapBodyElem6 = soapBodyElem.addChildElement("customerState");
        soapBodyElem6.addTextNode(State);
        SOAPElement soapBodyElem7 = soapBodyElem.addChildElement("customerCompany");
        soapBodyElem7.addTextNode(Company);
        SOAPElement soapBodyElem8 = soapBodyElem.addChildElement("customerPostCode");
        soapBodyElem8.addTextNode(Postcode);
        SOAPElement soapBodyElem9 = soapBodyElem.addChildElement("customerCountry");
        soapBodyElem9.addTextNode(Country);
        SOAPElement soapBodyElem10 = soapBodyElem.addChildElement("customerEmail");
        soapBodyElem10.addTextNode(Email);
        SOAPElement soapBodyElem11 = soapBodyElem.addChildElement("customerFax");
        soapBodyElem11.addTextNode("");
        SOAPElement soapBodyElem12 = soapBodyElem.addChildElement("customerPhone1");
        soapBodyElem12.addTextNode(Mobile);
        SOAPElement soapBodyElem13 = soapBodyElem.addChildElement("customerPhone2");
        soapBodyElem13.addTextNode("");
        SOAPElement soapBodyElem14 = soapBodyElem.addChildElement("customerRef");
        soapBodyElem14.addTextNode("");
        SOAPElement soapBodyElem15 = soapBodyElem.addChildElement("customerJobDesc");
        soapBodyElem15.addTextNode("");
        SOAPElement soapBodyElem16 = soapBodyElem.addChildElement("customerComments");
        soapBodyElem16.addTextNode("");
        SOAPElement soapBodyElem17 = soapBodyElem.addChildElement("customerURL");
        soapBodyElem17.addTextNode("http://www.myphysioapp.com.au/");

        MimeHeaders headers = soapMessage.getMimeHeaders();
        headers.addHeader("SOAPAction", serverURI);

        soapMessage.saveChanges();

        /* Print the request message */
//        System.out.print("Request SOAP Message = ");
//        soapMessage.writeTo(System.out);
//        System.out.println();

        return soapMessage;
    }
	
	public static SOAPMessage createSOAPUpdateCustomerRequest(String CustomerId, String FirstName, String LastName, String Address1, String Address2, String Suburb, String State, String Company, String Postcode, String Country, String Email, String Mobile) throws Exception {
        MessageFactory messageFactory = MessageFactory.newInstance();
        SOAPMessage soapMessage = messageFactory.createMessage();
        SOAPEnvelope env = soapMessage.getSOAPPart().getEnvelope(); 
        SOAPHeader header = env.getHeader();
        
     // create header entry element.
        QName ageQname = new QName("http://www.eway.com.au/gateway/rebill/manageRebill", "eWAYHeader");
     			SOAPHeaderElement soapHeaderElement = header
     					.addHeaderElement(ageQname);

     			// create the first child element and set the value
     			SOAPElement element1 = soapHeaderElement.addChildElement(
     					"eWAYCustomerID");
//     			element1.setValue("91094380");
     			element1.setValue("13881431");

     			// create the second child element
     			SOAPElement element2 = soapHeaderElement.addChildElement("Username");
     			// create and set the attribute of second child element.
     			// set the element value
//     			element2.setValue("rvinothkumarer@gmail.com.sand");
     			element2.setValue("triton@myphysioapp.com.au");
     			
     			SOAPElement element3 = soapHeaderElement.addChildElement(
     					"Password");
//     			element3.setValue("nipple");
     			element3.setValue("#35!Acrobat");
        
        SOAPPart soapPart = soapMessage.getSOAPPart();

        String serverURI = "http://www.eway.com.au/gateway/rebill/manageRebill/UpdateRebillCustomer";
        

        // SOAP Envelope
        SOAPEnvelope envelope = soapPart.getEnvelope();
        envelope.addNamespaceDeclaration("xsi", serverURI);
        envelope.addNamespaceDeclaration("xsd", "http://www.w3.org/2001/XMLSchema");
//        envelope.addNamespaceDeclaration("soap", "http://schemas.xmlsoap.org/soap/envelope/");

        /*
        Constructed SOAP Request Message:
        <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:example="http://ws.cdyne.com/">
            <SOAP-ENV:Header/>
            <SOAP-ENV:Body>
                <example:VerifyEmail>
                    <example:email>mutantninja@gmail.com</example:email>
                    <example:LicenseKey>123</example:LicenseKey>
                </example:VerifyEmail>
            </SOAP-ENV:Body>
        </SOAP-ENV:Envelope>
         */

//        Example Create Rebill Customer request
        // SOAP Body
        QName ageQname1 = new QName("http://www.eway.com.au/gateway/rebill/manageRebill", "UpdateRebillCustomer");
        SOAPBody soapBody = envelope.getBody();
        SOAPElement soapBodyElem = soapBody.addChildElement(ageQname1);
        SOAPElement soapBodyElem1 = soapBodyElem.addChildElement("RebillCustomerID");
        soapBodyElem1.addTextNode(CustomerId);
        SOAPElement soapBodyElem2 = soapBodyElem.addChildElement("customerTitle");
        soapBodyElem2.addTextNode("Mr.");
        SOAPElement soapBodyElem3 = soapBodyElem.addChildElement("customerFirstName");
        soapBodyElem3.addTextNode(FirstName);
        SOAPElement soapBodyElem4 = soapBodyElem.addChildElement("customerLastName");
        soapBodyElem4.addTextNode(LastName);
        SOAPElement soapBodyElem5 = soapBodyElem.addChildElement("customerAddress");
        soapBodyElem5.addTextNode(Address1+" "+Address2);
        SOAPElement soapBodyElem6 = soapBodyElem.addChildElement("customerSuburb");
        soapBodyElem6.addTextNode(Suburb);
        SOAPElement soapBodyElem7 = soapBodyElem.addChildElement("customerState");
        soapBodyElem7.addTextNode(State);
        SOAPElement soapBodyElem8 = soapBodyElem.addChildElement("customerCompany");
        soapBodyElem8.addTextNode(Company);
        SOAPElement soapBodyElem9 = soapBodyElem.addChildElement("customerPostCode");
        soapBodyElem9.addTextNode(Postcode);
        SOAPElement soapBodyElem10 = soapBodyElem.addChildElement("customerCountry");
        soapBodyElem10.addTextNode(Country);
        SOAPElement soapBodyElem11 = soapBodyElem.addChildElement("customerEmail");
        soapBodyElem11.addTextNode(Email);
        SOAPElement soapBodyElem12 = soapBodyElem.addChildElement("customerFax");
        soapBodyElem12.addTextNode("");
        SOAPElement soapBodyElem13 = soapBodyElem.addChildElement("customerPhone1");
        soapBodyElem13.addTextNode(Mobile);
        SOAPElement soapBodyElem14 = soapBodyElem.addChildElement("customerPhone2");
        soapBodyElem14.addTextNode("");
        SOAPElement soapBodyElem15 = soapBodyElem.addChildElement("customerRef");
        soapBodyElem15.addTextNode("");
        SOAPElement soapBodyElem16 = soapBodyElem.addChildElement("customerJobDesc");
        soapBodyElem16.addTextNode("");
        SOAPElement soapBodyElem17 = soapBodyElem.addChildElement("customerComments");
        soapBodyElem17.addTextNode("");
        SOAPElement soapBodyElem18 = soapBodyElem.addChildElement("customerURL");
        soapBodyElem18.addTextNode("http://www.myphysioapp.com.au/");

        MimeHeaders headers = soapMessage.getMimeHeaders();
        headers.addHeader("SOAPAction", serverURI);

        soapMessage.saveChanges();

        /* Print the request message */
//        System.out.print("Request SOAP Message = ");
//        soapMessage.writeTo(System.out);
//        System.out.println();

        return soapMessage;
    }
	
	public static SOAPMessage createSOAPRecurringTransactionDetailsRequest(String CustomerId, String RebillId, String StartDate, String EndDate, String Status) throws Exception {
        MessageFactory messageFactory = MessageFactory.newInstance();
        SOAPMessage soapMessage = messageFactory.createMessage();
        SOAPEnvelope env = soapMessage.getSOAPPart().getEnvelope(); 
        SOAPHeader header = env.getHeader();
        
     // create header entry element.
        QName ageQname = new QName("http://www.eway.com.au/gateway/rebill/manageRebill", "eWAYHeader");
     			SOAPHeaderElement soapHeaderElement = header
     					.addHeaderElement(ageQname);

     			// create the first child element and set the value
     			SOAPElement element1 = soapHeaderElement.addChildElement(
     					"eWAYCustomerID");
//     			element1.setValue("91094380");
     			element1.setValue("13881431");

     			// create the second child element
     			SOAPElement element2 = soapHeaderElement.addChildElement("Username");
     			// create and set the attribute of second child element.
     			// set the element value
//     			element2.setValue("rvinothkumarer@gmail.com.sand");
     			element2.setValue("triton@myphysioapp.com.au");
     			
     			SOAPElement element3 = soapHeaderElement.addChildElement(
     					"Password");
//     			element3.setValue("nipple");
     			element3.setValue("#35!Acrobat");
        
        SOAPPart soapPart = soapMessage.getSOAPPart();

        String serverURI = "http://www.eway.com.au/gateway/rebill/manageRebill/QueryTransactions";
        

        // SOAP Envelope
        SOAPEnvelope envelope = soapPart.getEnvelope();
        envelope.addNamespaceDeclaration("xsi", serverURI);
        envelope.addNamespaceDeclaration("xsd", "http://www.w3.org/2001/XMLSchema");
//        envelope.addNamespaceDeclaration("soap", "http://schemas.xmlsoap.org/soap/envelope/");

        /*
        Constructed SOAP Request Message:
        <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:example="http://ws.cdyne.com/">
            <SOAP-ENV:Header/>
            <SOAP-ENV:Body>
                <example:VerifyEmail>
                    <example:email>mutantninja@gmail.com</example:email>
                    <example:LicenseKey>123</example:LicenseKey>
                </example:VerifyEmail>
            </SOAP-ENV:Body>
        </SOAP-ENV:Envelope>
         */

//        Example Create Rebill Customer request
        // SOAP Body
        QName ageQname1 = new QName("http://www.eway.com.au/gateway/rebill/manageRebill", "QueryTransactions");
        SOAPBody soapBody = envelope.getBody();
        SOAPElement soapBodyElem = soapBody.addChildElement(ageQname1);
        SOAPElement soapBodyElem1 = soapBodyElem.addChildElement("RebillCustomerID");
        soapBodyElem1.addTextNode(CustomerId);
        SOAPElement soapBodyElem2 = soapBodyElem.addChildElement("RebillID");
        soapBodyElem2.addTextNode(RebillId);
        SOAPElement soapBodyElem3 = soapBodyElem.addChildElement("startDate");
        soapBodyElem3.addTextNode(StartDate);
        SOAPElement soapBodyElem4 = soapBodyElem.addChildElement("endDate");
        soapBodyElem4.addTextNode(EndDate);
        SOAPElement soapBodyElem5 = soapBodyElem.addChildElement("status");
        soapBodyElem5.addTextNode(Status);

        MimeHeaders headers = soapMessage.getMimeHeaders();
        headers.addHeader("SOAPAction", serverURI);

        soapMessage.saveChanges();

        /* Print the request message */
//        System.out.print("Request SOAP Message = ");
//        soapMessage.writeTo(System.out);
//        System.out.println();

        return soapMessage;
    }
	
	public static SOAPMessage createSOAPDeleteRebillEventRequest(String CustomerId, String RebillId) throws Exception {
        MessageFactory messageFactory = MessageFactory.newInstance();
        SOAPMessage soapMessage = messageFactory.createMessage();
        SOAPEnvelope env = soapMessage.getSOAPPart().getEnvelope(); 
        SOAPHeader header = env.getHeader();
        
     // create header entry element.
        QName ageQname = new QName("http://www.eway.com.au/gateway/rebill/manageRebill", "eWAYHeader");
     			SOAPHeaderElement soapHeaderElement = header
     					.addHeaderElement(ageQname);

     			// create the first child element and set the value
     			SOAPElement element1 = soapHeaderElement.addChildElement(
     					"eWAYCustomerID");
//     			element1.setValue("91094380");
     			element1.setValue("13881431");

     			// create the second child element
     			SOAPElement element2 = soapHeaderElement.addChildElement("Username");
     			// create and set the attribute of second child element.
     			// set the element value
//     			element2.setValue("rvinothkumarer@gmail.com.sand");
     			element2.setValue("triton@myphysioapp.com.au");
     			
     			SOAPElement element3 = soapHeaderElement.addChildElement(
     					"Password");
//     			element3.setValue("nipple");
     			element3.setValue("#35!Acrobat");
        
        SOAPPart soapPart = soapMessage.getSOAPPart();

        String serverURI = "http://www.eway.com.au/gateway/rebill/manageRebill/DeleteRebillEvent";
        

        // SOAP Envelope
        SOAPEnvelope envelope = soapPart.getEnvelope();
        envelope.addNamespaceDeclaration("xsi", serverURI);
        envelope.addNamespaceDeclaration("xsd", "http://www.w3.org/2001/XMLSchema");
//        envelope.addNamespaceDeclaration("soap", "http://schemas.xmlsoap.org/soap/envelope/");

        /*
        Constructed SOAP Request Message:
        <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:example="http://ws.cdyne.com/">
            <SOAP-ENV:Header/>
            <SOAP-ENV:Body>
                <example:VerifyEmail>
                    <example:email>mutantninja@gmail.com</example:email>
                    <example:LicenseKey>123</example:LicenseKey>
                </example:VerifyEmail>
            </SOAP-ENV:Body>
        </SOAP-ENV:Envelope>
         */

//        Example Create Rebill Customer request
        // SOAP Body
        QName ageQname1 = new QName("http://www.eway.com.au/gateway/rebill/manageRebill", "DeleteRebillEvent");
        SOAPBody soapBody = envelope.getBody();
        SOAPElement soapBodyElem = soapBody.addChildElement(ageQname1);
        SOAPElement soapBodyElem1 = soapBodyElem.addChildElement("RebillCustomerID");
        soapBodyElem1.addTextNode(CustomerId);
        SOAPElement soapBodyElem2 = soapBodyElem.addChildElement("RebillID");
        soapBodyElem2.addTextNode(RebillId);

        MimeHeaders headers = soapMessage.getMimeHeaders();
        headers.addHeader("SOAPAction", serverURI);

        soapMessage.saveChanges();

        /* Print the request message */
//        System.out.print("Request SOAP Message = ");
//        soapMessage.writeTo(System.out);
//        System.out.println();

        return soapMessage;
    }
	
	public static SOAPMessage soapResponseDeleteCustomer(String CustomerId) throws Exception {
        MessageFactory messageFactory = MessageFactory.newInstance();
        SOAPMessage soapMessage = messageFactory.createMessage();
        SOAPEnvelope env = soapMessage.getSOAPPart().getEnvelope(); 
        SOAPHeader header = env.getHeader();
        
     // create header entry element.
        QName ageQname = new QName("http://www.eway.com.au/gateway/rebill/manageRebill", "eWAYHeader");
     			SOAPHeaderElement soapHeaderElement = header
     					.addHeaderElement(ageQname);

     			// create the first child element and set the value
     			SOAPElement element1 = soapHeaderElement.addChildElement(
     					"eWAYCustomerID");
//     			element1.setValue("91094380");
     			element1.setValue("13881431");

     			// create the second child element
     			SOAPElement element2 = soapHeaderElement.addChildElement("Username");
     			// create and set the attribute of second child element.
     			// set the element value
//     			element2.setValue("rvinothkumarer@gmail.com.sand");
     			element2.setValue("triton@myphysioapp.com.au");
     			
     			SOAPElement element3 = soapHeaderElement.addChildElement(
     					"Password");
//     			element3.setValue("nipple");
     			element3.setValue("#35!Acrobat");
        
        SOAPPart soapPart = soapMessage.getSOAPPart();

        String serverURI = "http://www.eway.com.au/gateway/rebill/manageRebill/DeleteRebillCustomer";
        

        // SOAP Envelope
        SOAPEnvelope envelope = soapPart.getEnvelope();
        envelope.addNamespaceDeclaration("xsi", serverURI);
        envelope.addNamespaceDeclaration("xsd", "http://www.w3.org/2001/XMLSchema");
//        envelope.addNamespaceDeclaration("soap", "http://schemas.xmlsoap.org/soap/envelope/");

        /*
        Constructed SOAP Request Message:
        <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:example="http://ws.cdyne.com/">
            <SOAP-ENV:Header/>
            <SOAP-ENV:Body>
                <example:VerifyEmail>
                    <example:email>mutantninja@gmail.com</example:email>
                    <example:LicenseKey>123</example:LicenseKey>
                </example:VerifyEmail>
            </SOAP-ENV:Body>
        </SOAP-ENV:Envelope>
         */

//        Example Create Rebill Customer request
        // SOAP Body
        QName ageQname1 = new QName("http://www.eway.com.au/gateway/rebill/manageRebill", "DeleteRebillCustomer");
        SOAPBody soapBody = envelope.getBody();
        SOAPElement soapBodyElem = soapBody.addChildElement(ageQname1);
        SOAPElement soapBodyElem1 = soapBodyElem.addChildElement("RebillCustomerID");
        soapBodyElem1.addTextNode(CustomerId);

        MimeHeaders headers = soapMessage.getMimeHeaders();
        headers.addHeader("SOAPAction", serverURI);

        soapMessage.saveChanges();

        /* Print the request message */
//        System.out.print("Request SOAP Message = ");
//        soapMessage.writeTo(System.out);
//        System.out.println();

        return soapMessage;
    }
	
	public static SOAPMessage createSOAPCreateRebillEventRequest(String RebillCustomerID, String InvRef, String InvDes, String CCName, String CCNumber, String CCMM, String CCYY, String TotAmt, String strDate, String nextMonth, String fiveYear) throws Exception {
        MessageFactory messageFactory = MessageFactory.newInstance();
        SOAPMessage soapMessage = messageFactory.createMessage();
        SOAPEnvelope env = soapMessage.getSOAPPart().getEnvelope(); 
        SOAPHeader header = env.getHeader();
        
     // create header entry element.
        QName ageQname = new QName("http://www.eway.com.au/gateway/rebill/manageRebill", "eWAYHeader");
     			SOAPHeaderElement soapHeaderElement = header
     					.addHeaderElement(ageQname);

     			// create the first child element and set the value
     			SOAPElement element1 = soapHeaderElement.addChildElement(
     					"eWAYCustomerID");
//     			element1.setValue("91094380");
     			element1.setValue("13881431");

     			// create the second child element
     			SOAPElement element2 = soapHeaderElement.addChildElement("Username");
     			// create and set the attribute of second child element.
     			// set the element value
//     			element2.setValue("rvinothkumarer@gmail.com.sand");
     			element2.setValue("triton@myphysioapp.com.au");
     			
     			SOAPElement element3 = soapHeaderElement.addChildElement(
     					"Password");
//     			element3.setValue("nipple");
     			element3.setValue("#35!Acrobat");
        
        SOAPPart soapPart = soapMessage.getSOAPPart();

        String serverURI = "http://www.eway.com.au/gateway/rebill/manageRebill/CreateRebillEvent";
        

        // SOAP Envelope
        SOAPEnvelope envelope = soapPart.getEnvelope();
        envelope.addNamespaceDeclaration("xsi", serverURI);
        envelope.addNamespaceDeclaration("xsd", "http://www.w3.org/2001/XMLSchema");
//        envelope.addNamespaceDeclaration("soap", "http://schemas.xmlsoap.org/soap/envelope/");

        /*
        Constructed SOAP Request Message:
        <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:example="http://ws.cdyne.com/">
            <SOAP-ENV:Header/>
            <SOAP-ENV:Body>
                <example:VerifyEmail>
                    <example:email>mutantninja@gmail.com</example:email>
                    <example:LicenseKey>123</example:LicenseKey>
                </example:VerifyEmail>
            </SOAP-ENV:Body>
        </SOAP-ENV:Envelope>
         */

//        Example Create Rebill Event request
        // SOAP Body
        QName ageQname1 = new QName("http://www.eway.com.au/gateway/rebill/manageRebill", "CreateRebillEvent");
        SOAPBody soapBody = envelope.getBody();
        SOAPElement soapBodyElem = soapBody.addChildElement(ageQname1);
        SOAPElement soapBodyElem1 = soapBodyElem.addChildElement("RebillCustomerID");
        soapBodyElem1.addTextNode(RebillCustomerID);
        SOAPElement soapBodyElem2 = soapBodyElem.addChildElement("RebillInvRef");
        soapBodyElem2.addTextNode(InvRef);
        SOAPElement soapBodyElem3 = soapBodyElem.addChildElement("RebillInvDes");
        soapBodyElem3.addTextNode(InvDes);
        SOAPElement soapBodyElem4 = soapBodyElem.addChildElement("RebillCCName");
        soapBodyElem4.addTextNode(CCName);
        SOAPElement soapBodyElem5 = soapBodyElem.addChildElement("RebillCCNumber");
        soapBodyElem5.addTextNode(CCNumber);
        SOAPElement soapBodyElem6 = soapBodyElem.addChildElement("RebillCCExpMonth");
        soapBodyElem6.addTextNode(CCMM);
        SOAPElement soapBodyElem7 = soapBodyElem.addChildElement("RebillCCExpYear");
        soapBodyElem7.addTextNode(CCYY);
        SOAPElement soapBodyElem8 = soapBodyElem.addChildElement("RebillInitAmt");
        soapBodyElem8.addTextNode(TotAmt);
        SOAPElement soapBodyElem9 = soapBodyElem.addChildElement("RebillInitDate");
        soapBodyElem9.addTextNode(strDate);
        SOAPElement soapBodyElem10 = soapBodyElem.addChildElement("RebillRecurAmt");
        soapBodyElem10.addTextNode(TotAmt);
        SOAPElement soapBodyElem11 = soapBodyElem.addChildElement("RebillStartDate");
        soapBodyElem11.addTextNode(nextMonth);
        SOAPElement soapBodyElem12 = soapBodyElem.addChildElement("RebillInterval");
        soapBodyElem12.addTextNode("1");
        SOAPElement soapBodyElem13 = soapBodyElem.addChildElement("RebillIntervalType");
        soapBodyElem13.addTextNode("3");
        SOAPElement soapBodyElem14 = soapBodyElem.addChildElement("RebillEndDate");
        soapBodyElem14.addTextNode(fiveYear);

        MimeHeaders headers = soapMessage.getMimeHeaders();
        headers.addHeader("SOAPAction", serverURI);

        soapMessage.saveChanges();

        /* Print the request message */
//        System.out.print("Request SOAP Message = ");
//        soapMessage.writeTo(System.out);
//        System.out.println();

        return soapMessage;
    }
	
	public static SOAPMessage createSOAPUpdateRebillEventRequest(String RebillCustomerID, String RebillID, String InvRef, String InvDes, String CCName, String CCNumber, String CCMM, String CCYY, String TotAmt, String strDate, String nextMonth, String fiveYear) throws Exception {
        MessageFactory messageFactory = MessageFactory.newInstance();
        SOAPMessage soapMessage = messageFactory.createMessage();
        SOAPEnvelope env = soapMessage.getSOAPPart().getEnvelope(); 
        SOAPHeader header = env.getHeader();
        
     // create header entry element.
        QName ageQname = new QName("http://www.eway.com.au/gateway/rebill/manageRebill", "eWAYHeader");
     			SOAPHeaderElement soapHeaderElement = header
     					.addHeaderElement(ageQname);

     			// create the first child element and set the value
     			SOAPElement element1 = soapHeaderElement.addChildElement(
     					"eWAYCustomerID");
//     			element1.setValue("91094380");
     			element1.setValue("13881431");

     			// create the second child element
     			SOAPElement element2 = soapHeaderElement.addChildElement("Username");
     			// create and set the attribute of second child element.
     			// set the element value
//     			element2.setValue("rvinothkumarer@gmail.com.sand");
     			element2.setValue("triton@myphysioapp.com.au");
     			
     			SOAPElement element3 = soapHeaderElement.addChildElement(
     					"Password");
//     			element3.setValue("nipple");
     			element3.setValue("#35!Acrobat");
        
        SOAPPart soapPart = soapMessage.getSOAPPart();

        String serverURI = "http://www.eway.com.au/gateway/rebill/manageRebill/UpdateRebillEvent";
        

        // SOAP Envelope
        SOAPEnvelope envelope = soapPart.getEnvelope();
        envelope.addNamespaceDeclaration("xsi", serverURI);
        envelope.addNamespaceDeclaration("xsd", "http://www.w3.org/2001/XMLSchema");
//        envelope.addNamespaceDeclaration("soap", "http://schemas.xmlsoap.org/soap/envelope/");

        /*
        Constructed SOAP Request Message:
        <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:example="http://ws.cdyne.com/">
            <SOAP-ENV:Header/>
            <SOAP-ENV:Body>
                <example:VerifyEmail>
                    <example:email>mutantninja@gmail.com</example:email>
                    <example:LicenseKey>123</example:LicenseKey>
                </example:VerifyEmail>
            </SOAP-ENV:Body>
        </SOAP-ENV:Envelope>
         */

//        Example Create Rebill Event request
        // SOAP Body
        QName ageQname1 = new QName("http://www.eway.com.au/gateway/rebill/manageRebill", "UpdateRebillEvent");
        SOAPBody soapBody = envelope.getBody();
        SOAPElement soapBodyElem = soapBody.addChildElement(ageQname1);
        SOAPElement soapBodyElem1 = soapBodyElem.addChildElement("RebillCustomerID");
        soapBodyElem1.addTextNode(RebillCustomerID);
        SOAPElement soapBodyElem2 = soapBodyElem.addChildElement("RebillID");
        soapBodyElem2.addTextNode(RebillID);
        SOAPElement soapBodyElem3 = soapBodyElem.addChildElement("RebillInvRef");
        soapBodyElem3.addTextNode(InvRef);
        SOAPElement soapBodyElem4 = soapBodyElem.addChildElement("RebillInvDes");
        soapBodyElem4.addTextNode(InvDes);
        SOAPElement soapBodyElem5 = soapBodyElem.addChildElement("RebillCCName");
        soapBodyElem5.addTextNode(CCName);
        SOAPElement soapBodyElem6 = soapBodyElem.addChildElement("RebillCCNumber");
        soapBodyElem6.addTextNode(CCNumber);
        SOAPElement soapBodyElem7 = soapBodyElem.addChildElement("RebillCCExpMonth");
        soapBodyElem7.addTextNode(CCMM);
        SOAPElement soapBodyElem8 = soapBodyElem.addChildElement("RebillCCExpYear");
        soapBodyElem8.addTextNode(CCYY);
        SOAPElement soapBodyElem9 = soapBodyElem.addChildElement("RebillInitAmt");
        soapBodyElem9.addTextNode(TotAmt);
        SOAPElement soapBodyElem10 = soapBodyElem.addChildElement("RebillInitDate");
        soapBodyElem10.addTextNode(strDate);
        SOAPElement soapBodyElem11 = soapBodyElem.addChildElement("RebillRecurAmt");
        soapBodyElem11.addTextNode(TotAmt);
        SOAPElement soapBodyElem12 = soapBodyElem.addChildElement("RebillStartDate");
        soapBodyElem12.addTextNode(nextMonth);
        SOAPElement soapBodyElem13 = soapBodyElem.addChildElement("RebillInterval");
        soapBodyElem13.addTextNode("1");
        SOAPElement soapBodyElem14 = soapBodyElem.addChildElement("RebillIntervalType");
        soapBodyElem14.addTextNode("3");
        SOAPElement soapBodyElem15 = soapBodyElem.addChildElement("RebillEndDate");
        soapBodyElem15.addTextNode(fiveYear);

        MimeHeaders headers = soapMessage.getMimeHeaders();
        headers.addHeader("SOAPAction", serverURI);

        soapMessage.saveChanges();

        /* Print the request message */
//        System.out.print("Request SOAP Message = ");
//        soapMessage.writeTo(System.out);
//        System.out.println();

        return soapMessage;
    }

    /**
     * Method used to print the SOAP Response
     */
    public static void printSOAPResponse(SOAPMessage soapResponse) throws Exception {
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        Source sourceContent = soapResponse.getSOAPPart().getContent();
//        System.out.print("\nResponse SOAP Message = ");
        StreamResult result = new StreamResult(System.out);
        transformer.transform(sourceContent, result);
    }

}