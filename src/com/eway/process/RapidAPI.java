package com.eway.process;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ResourceBundle;

import org.apache.commons.codec.binary.Base64;

/**
 *  RapidAPI class is main class which acts like a helper class. It collects bean objects from Servlets and sends those to Parser for conversion.
 *  It sends request to eWAY and gets response from eWAY.
 */

public class RapidAPI
{
	// Load the configuration
	public final ResourceBundle configResource = ResourceBundle.getBundle("config");

	/**
	 * Create Access Code [HTTP JSON]
	 * @param CreateAccessCodeRequest CreateAccessCodeRequest object
	 * @return CreateAccessCodeResponse object
	 */
	public com.eway.json.bean.CreateAccessCodeResponse CreateAccessCode(com.eway.json.bean.CreateAccessCodeRequest CreateAccessCodeRequest)
	{
		String parsedValue = "";

		// Convert An Object to Target Formats
		parsedValue = Parser.objtojson(CreateAccessCodeRequest);

		System.out.println("\n============== HTTP JSON CreateAccessCode Request =============\n");
		System.out.println(parsedValue);
		System.out.println("\n===============================================================\n");

		String responseVal = "";
		responseVal = CreateAccessCodePOST(parsedValue);

		System.out.println("\n============= HTTP JSON CreateAccessCode Response ==============\n");
		System.out.println(responseVal);
		System.out.println("\n================================================================\n");

		// Convert Response Back TO An Object
		com.eway.json.bean.CreateAccessCodeResponse CreateAccessCodeResponse = Parser.jsontoobj(responseVal);

		return CreateAccessCodeResponse;
	}

	/**
	 * Create Access Code [HTTP XML]
	 * @param CreateAccessCodeRequest CreateAccessCodeRequest object
	 * @return CreateAccessCodeResponse object
	 */
	public com.eway.xml.bean.CreateAccessCodeResponse CreateAccessCode(com.eway.xml.bean.CreateAccessCodeRequest CreateAccessCodeRequest)
	{
		String parsedValue = "";

		// Convert An Object to Target Formats
		parsedValue = Parser.objtoxml(CreateAccessCodeRequest);

		System.out.println("\n============== HTTP XML CreateAccessCode Request =============\n");
		System.out.println(parsedValue);
		System.out.println("\n==============================================================\n");

		String responseVal = "";
		responseVal = CreateAccessCodePOST(parsedValue);

		System.out.println("\n============= HTTP XML CreateAccessCode Response ==============\n");
		System.out.println(responseVal);
		System.out.println("\n===============================================================\n");

		// Convert Response Back TO An Object
		com.eway.xml.bean.CreateAccessCodeResponse CreateAccessCodeResponse = Parser.xmltoobj(responseVal);

		return CreateAccessCodeResponse;
	}
	
	/**
	 * Create Access Code [HTTP JSON]
	 * @param CreateTokenRequest CreateTokenRequest object
	 * @return DirectPaymentResponse object
	 */
	public String CreateToken(com.eway.json.bean.CreateTokenRequest CreateTokenRequest)
	{
		String parsedValue = "";

		// Convert An Object to Target Formats
		parsedValue = Parser.objtojson(CreateTokenRequest);

		System.out.println("\n============== HTTP JSON CreateToken Request =============\n");
		System.out.println(parsedValue);
		System.out.println("\n===============================================================\n");

		String responseVal = "";
		responseVal = CreateTokenPOST(parsedValue);
		

		System.out.println("\n============= HTTP JSON DirectPayment Response ==============\n");
		System.out.println(responseVal);
		System.out.println("\n================================================================\n");

		// Convert Response Back TO An Object
//		com.eway.json.bean.DirectPaymentResponse DirectPaymentResponse = Parser.tokenjsontoobj(responseVal);

		return responseVal;
	}
	
	/**
	 * Create Token Code [HTTP XML]
	 * @param CreateTokenRequest CreateTokenRequest object
	 * @return CreateTokenResponse object
	 */
	/*public com.eway.xml.bean.DirectPaymentResponse CreateToken(com.eway.xml.bean.CreateTokenRequest CreateTokenRequest)
	{
		String parsedValue = "";

		// Convert An Object to Target Formats
		parsedValue = Parser.objtoxml(CreateTokenRequest);

		System.out.println("\n============== HTTP XML CreateToken Request =============\n");
		System.out.println(parsedValue);
		System.out.println("\n==============================================================\n");

		String responseVal = "";
		responseVal = CreateTokenPOST(parsedValue);

		System.out.println("\n============= HTTP XML DirectPaymentResponse Response ==============\n");
		System.out.println(responseVal);
		System.out.println("\n===============================================================\n");

		// Convert Response Back TO An Object
		com.eway.xml.bean.DirectPaymentResponse DirectPaymentResponse = Parser.tokenxmltoobj(responseVal);

		return DirectPaymentResponse;
	}*/


	/**
	 * Get Result with Access Code [HTTP JSON]
	 * @param GetAccessCodeResultRequest Access Code String
	 * @return GetAccessCodeResultResponse object
	 */
	public com.eway.json.bean.GetAccessCodeResultResponse GetAccessCodeResult(com.eway.json.bean.GetAccessCodeResultRequest GetAccessCodeResultRequest)
	{
		String parsedValue = "";
		
		// Convert An Object to Target Formats
		parsedValue = Parser.objtojson2(GetAccessCodeResultRequest);
				
		System.out.println("\n============== HTTP JSON GetAccessCodeResult Request =============\n");
		System.out.println(parsedValue);
		System.out.println("\n==================================================================\n");

		String responseVal = "";
		responseVal = GetAccessCodeResultPOST(parsedValue);

		System.out.println("\n============= HTTP JSON GetAccessCodeResult Response ==============\n");
		System.out.println(responseVal);
		System.out.println("\n===================================================================\n");

		// Convert Response Back TO An Object
		com.eway.json.bean.GetAccessCodeResultResponse GetAccessCodeResultResponse = Parser.jsontoobj2(responseVal);

		return GetAccessCodeResultResponse;
	}

	
	/**
	 * Get Result with Access Code [HTTP XML]
	 * @param GetAccessCodeResultRequest Access Code String
	 * @return GetAccessCodeResultResponse object
	 */
	public com.eway.xml.bean.GetAccessCodeResultResponse GetAccessCodeResult(com.eway.xml.bean.GetAccessCodeResultRequest GetAccessCodeResultRequest)
	{
		String parsedValue = "";

		// Convert An Object to Target Formats
		parsedValue = Parser.objtoxml2(GetAccessCodeResultRequest);
					
		System.out.println("\n============== HTTP XML GetAccessCodeResult Request =============\n");
		System.out.println(parsedValue);
		System.out.println("\n==================================================================\n");

		String responseVal = "";
		responseVal = GetAccessCodeResultPOST(parsedValue);

		System.out.println("\n============= HTTP XML GetAccessCodeResult Response ==============\n");
		System.out.println(responseVal);
		System.out.println("\n==================================================================\n");

		// Convert Response Back TO An Object
		com.eway.xml.bean.GetAccessCodeResultResponse GetAccessCodeResultResponse = Parser.xmltoobj2(responseVal);

		return GetAccessCodeResultResponse;
	}


	/**
	 * Create Access Code Via HTTP POST
	 * @param XML/JSON Format String
	 * @return XML/JSON Format Response String
	 */
	public String CreateAccessCodePOST(String parsedValue)
	{
		String urlVal = configResource.getString("paymentServicePOSTCreateAccessCode");
		String responseVal = CallHttpPostUrl(parsedValue, urlVal);
		return responseVal;
	}
	
	/**
	 * Create Token Code Via HTTP POST
	 * @param XML/JSON Format String
	 * @return XML/JSON Format Response String
	 */
	public String CreateTokenPOST(String parsedValue)
	{
		String urlVal = configResource.getString("paymentServicePOSTCreateToken");
		String responseVal = CallJSONHttpPostUrl(parsedValue, urlVal);
		return responseVal;
	}

	
	/**
	 * Get Result with Access Code Via HTTP POST
	 * @param XML/JSON Format String
	 * @return XML/JSON Format Response String
	 */
	public String GetAccessCodeResultPOST(String parsedValue)
	{
		String urlVal = configResource.getString("paymentServicePOSTGetAccessCodeResult");
		String responseVal = CallHttpPostUrl(parsedValue, urlVal);
		return responseVal;
	}


	/**
	 * Get Result with Access Code Via HTTP POST
	 * @param  Post Parameter String 
	 * @param URL String
	 * @return HTTP Response String
	 */
	public String CallHttpPostUrl(String postParameter, String apiUrl)
	{
		StringBuffer responseMessage = new StringBuffer();
		HttpURLConnection httpURLConnection = null;

		try
		{
			String userName = configResource.getString("paymentUsername");
			String password = configResource.getString("paymentPassword");

			String loginPassword = userName + ":" + password;
			String encodedString = Base64.encodeBase64String(loginPassword.getBytes());
			//System.out.println("\nfinalEncoded: " + encodedString);

			URL url = new URL(apiUrl);
			httpURLConnection = (HttpURLConnection) url.openConnection();
			httpURLConnection.setRequestProperty("Authorization", "Basic " + encodedString);
			httpURLConnection.setRequestMethod("POST");
			httpURLConnection.setDoInput(true);
			httpURLConnection.setDoOutput(true);
			httpURLConnection.setUseCaches(false);

			if (configResource.getString("requestFormat").equalsIgnoreCase("XML"))
				httpURLConnection.setRequestProperty("Content-Type", "text/xml");
			else
				httpURLConnection.setRequestProperty("Content-Type", "application/json");

			DataOutputStream wr = new DataOutputStream(httpURLConnection.getOutputStream());
			wr.writeBytes(postParameter);
			wr.flush();
			wr.close();

			BufferedReader br = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
			String line = "";

			while ((line = br.readLine()) != null)
			{
				responseMessage.append(line);
			}
		}
		catch (Exception e)
		{
			httpURLConnection.disconnect();
			System.out.println("\nException in CreateAccessCodePOST method\n");
			e.printStackTrace();
		}

		return responseMessage.toString();
	}
	
	/**
	 * Get Result with Access Code Via HTTP POST
	 * @param  Post Parameter String 
	 * @param URL String
	 * @return HTTP Response String
	 */
	public String CallJSONHttpPostUrl(String postParameter, String apiUrl)
	{
		StringBuffer responseMessage = new StringBuffer();
		HttpURLConnection httpURLConnection = null;

		try
		{
			String userName = configResource.getString("paymentUsername");
			String password = configResource.getString("paymentPassword");

			String loginPassword = userName + ":" + password;
			String encodedString = Base64.encodeBase64String(loginPassword.getBytes());
			//System.out.println("\nfinalEncoded: " + encodedString);

			URL url = new URL(apiUrl);
			httpURLConnection = (HttpURLConnection) url.openConnection();
			httpURLConnection.setRequestProperty("Authorization", "Basic " + encodedString);
			httpURLConnection.setRequestMethod("POST");
			httpURLConnection.setDoInput(true);
			httpURLConnection.setDoOutput(true);
			httpURLConnection.setUseCaches(false);

				httpURLConnection.setRequestProperty("Content-Type", "application/json");

			DataOutputStream wr = new DataOutputStream(httpURLConnection.getOutputStream());
			wr.writeBytes(postParameter);
			wr.flush();
			wr.close();

			BufferedReader br = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
			String line = "";

			while ((line = br.readLine()) != null)
			{
				responseMessage.append(line);
			}
		}
		catch (Exception e)
		{
			httpURLConnection.disconnect();
			System.out.println("\nException in CreateAccessCodePOST method\n");
			e.printStackTrace();
		}

		return responseMessage.toString();
	}
	
	/**
	  * Get Result with Access Code Via HTTP POST
	  * @param  Post Parameter String 
	  * @param URL String
	  * @return HTTP Response String
	  */
	 public String CallHttpGetURL(String getParameter, String apiUrl)
	 {
	  StringBuffer responseMessage = new StringBuffer();
	  HttpURLConnection httpURLConnection = null;

	  try
	  {
	   String userName = configResource.getString("paymentUsername");
	   String password = configResource.getString("paymentPassword");
	   
//	   String userName = configResource.getString("paymentResUsername");
//	   String password = configResource.getString("paymentResPassword");
	   
	   String loginPassword = userName + ":" + password;
	   String encodedString = Base64.encodeBase64String(loginPassword.getBytes());
	   //System.out.println("\nfinalEncoded: " + encodedString);
	   apiUrl = apiUrl + getParameter;
	   System.out.println("apiUrl = "+apiUrl);
	   URL url = new URL(apiUrl);
	   
	   httpURLConnection = (HttpURLConnection) url.openConnection();
	   httpURLConnection.setRequestProperty("Authorization", "Basic " + encodedString);
	   httpURLConnection.setRequestMethod("GET");
	   httpURLConnection.setDoInput(true);
	   httpURLConnection.setDoOutput(true);
	   httpURLConnection.setUseCaches(false);

	   BufferedReader br = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
	   String line = "";

	   while ((line = br.readLine()) != null)
	   {
	    responseMessage.append(line);
	   }
	  }
	  catch (Exception e)
	  {
	   httpURLConnection.disconnect();
	   System.out.println("\nException in CreateAccessCodeGET method\n");
	   e.printStackTrace();
	  }
	  System.out.println("responseMessagegET = "+responseMessage.toString());
	  return responseMessage.toString();
	 }
}
