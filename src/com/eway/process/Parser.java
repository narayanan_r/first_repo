package com.eway.process;

import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import com.eway.json.bean.CreateAccessCodeRequest;
import com.eway.json.bean.CreateAccessCodeResponse;
import com.eway.json.bean.CreateTokenRequest;
import com.eway.json.bean.DirectPaymentResponse;
import com.eway.json.bean.GetAccessCodeResultRequest;
import com.eway.json.bean.GetAccessCodeResultResponse;
import com.google.gson.Gson;

/**
 *  Parser class converts java object class to/from different sending options like XML and JSON.
 */

public class Parser
{
	/**
	 * Convert bean object to xml string for create access code request
	 * @param CreateAccessCodeRequest object
	 * @return xml as String
	 */
	public static String objtoxml(com.eway.xml.bean.CreateAccessCodeRequest CreateAccessCodeRequest)
	{
		String xml = "";

		try
		{
			JAXBContext jaxbContext = JAXBContext.newInstance(com.eway.xml.bean.CreateAccessCodeRequest.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
			StringWriter stringWriter = new StringWriter();

			jaxbMarshaller.marshal(CreateAccessCodeRequest, stringWriter);

			xml = stringWriter.toString();
		}
		catch (JAXBException e)
		{
			System.out.println("\nException in objtoxml method\n");
			e.printStackTrace();
		}

		return xml;
	}
	
	
	/**
	 * Convert bean object to xml string for get access code request
	 * @param GetAccessCodeResultRequest object
	 * @return xml as String
	 */
	public static String objtoxml2(com.eway.xml.bean.GetAccessCodeResultRequest GetAccessCodeResultRequest)
	{
		String xml = "";

		try
		{
			JAXBContext jaxbContext = JAXBContext.newInstance(com.eway.xml.bean.GetAccessCodeResultRequest.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
			StringWriter stringWriter = new StringWriter();

			jaxbMarshaller.marshal(GetAccessCodeResultRequest, stringWriter);

			xml = stringWriter.toString();
		}
		catch (JAXBException e)
		{
			System.out.println("\nException in objtoxml2 method\n");
			e.printStackTrace();
		}

		return xml;
	}

	
	/**
	 * Convert bean object to json string for create access code request
	 * @param CreateAccessCodeRequest object
	 * @return json as String
	 */
	public static String objtojson(CreateTokenRequest CreateTokenRequest)
	{
		String json = "";

		Gson gson = new Gson();
		json = gson.toJson(CreateTokenRequest);

		return json;
	}
	
	/**
	 * Convert bean object to json string for create access code request
	 * @param CreateAccessCodeRequest object
	 * @return json as String
	 */
	public static String objtojson(CreateAccessCodeRequest CreateAccessCodeRequest)
	{
		String json = "";

		Gson gson = new Gson();
		json = gson.toJson(CreateAccessCodeRequest);

		return json;
	}

	
	/**
	 * Convert bean object to json string for get access code request
	 * @param GetAccessCodeResultRequest object
	 * @return json as String
	 */
	public static String objtojson2(GetAccessCodeResultRequest GetAccessCodeResultRequest)
	{
		String json = "";

		Gson gson = new Gson();
		json = gson.toJson(GetAccessCodeResultRequest);

		return json;
	}

	
	/**
	 * Convert xml string to bean object for create access code response
	 * @param xml as String
	 * @return CreateAccessCodeResponse object
	 */
	public static com.eway.xml.bean.CreateAccessCodeResponse xmltoobj(String xml)
	{
		com.eway.xml.bean.CreateAccessCodeResponse CreateAccessCodeResponse = null;

		try
		{
			JAXBContext jaxbContext = JAXBContext.newInstance(com.eway.xml.bean.CreateAccessCodeResponse.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

			StringReader stringReader = new StringReader(xml);

			CreateAccessCodeResponse = (com.eway.xml.bean.CreateAccessCodeResponse) jaxbUnmarshaller.unmarshal(stringReader);
		}
		catch (JAXBException e)
		{
			System.out.println("\nException in xmltoobj method\n");
			e.printStackTrace();
		}

		return CreateAccessCodeResponse;
	}
	

	/**
	 * Convert xml string to bean object for get access code response
	 * @param xml as String
	 * @return GetAccessCodeResultResponse object
	 */
	public static com.eway.xml.bean.GetAccessCodeResultResponse xmltoobj2(String xml)
	{
		com.eway.xml.bean.GetAccessCodeResultResponse GetAccessCodeResultResponse = null;

		try
		{
			JAXBContext jaxbContext = JAXBContext.newInstance(com.eway.xml.bean.GetAccessCodeResultResponse.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

			StringReader stringReader = new StringReader(xml);

			// output pretty printed
			GetAccessCodeResultResponse = (com.eway.xml.bean.GetAccessCodeResultResponse) jaxbUnmarshaller.unmarshal(stringReader);
		}
		catch (JAXBException e)
		{
			System.out.println("\nException in xmltoobj2 method\n");
			e.printStackTrace();
		}

		return GetAccessCodeResultResponse;
	}

	
	/**
	 * Convert json string to bean object for create access code response
	 * @param json as String
	 * @return CreateAccessCodeResponse object
	 */
	public static CreateAccessCodeResponse jsontoobj(String json)
	{
		Gson gson = new Gson();
		CreateAccessCodeResponse CreateAccessCodeResponse = gson.fromJson(json, CreateAccessCodeResponse.class);

		return CreateAccessCodeResponse;
	}
	
	/**
	 * Convert json string to bean object for create access code response
	 * @param json as String
	 * @return DirectPaymentResponse object
	 */
	public static DirectPaymentResponse tokenjsontoobj(String json)
	{
		Gson gson = new Gson();
		DirectPaymentResponse DirectPaymentResponse = gson.fromJson(json, DirectPaymentResponse.class);

		return DirectPaymentResponse;
	}

	
	/**
	 * Convert json string to bean object for get access code response
	 * @param json as String
	 * @return GetAccessCodeResultResponse object
	 */
	public static GetAccessCodeResultResponse jsontoobj2(String json)
	{
		Gson gson = new Gson();
		GetAccessCodeResultResponse GetAccessCodeResultResponse = gson.fromJson(json, GetAccessCodeResultResponse.class);

		return GetAccessCodeResultResponse;
	}
}
