package com.eway.xml.bean;

import javax.xml.bind.annotation.XmlElement;


@com.sun.xml.internal.txw2.annotation.XmlElement
public class Verification
{
	private String CVN = null;
	private String Address = null;
	private String Email = null;
	private String Mobile = null;
	private String Phone = null;
	
	public String getCVN()
	{
		return CVN;
	}
	@XmlElement(name = "CVN")
	public void setCVN(String cvn)
	{
		CVN = cvn;
	}
	
	public String getAddress()
	{
		return Address;
	}
	@XmlElement(name = "Address")
	public void setAddress(String address)
	{
		Address = address;
	}
	
	public String getEmail()
	{
		return Email;
	}
	@XmlElement(name = "Email")
	public void setEmail(String email)
	{
		Email = email;
	}
	
	public String getMobile()
	{
		return Mobile;
	}
	@XmlElement(name = "Mobile")
	public void setMobile(String mobile)
	{
		Mobile = mobile;
	}
	
	public String getPhone()
	{
		return Phone;
	}
	@XmlElement(name = "Phone")
	public void setPhone(String phone)
	{
		Phone = phone;
	}
}
