package com.eway.xml.bean;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "CreateAccessCodeRequest")
public class CreateAccessCodeRequest
{
	public Customer Customer;
	public ShippingAddress ShippingAddress;
	public Items Items;
	public Options Options;
	public Payment Payment;
    
	@XmlElement(name = "RedirectUrl")
	public String RedirectUrl;
	@XmlElement(name = "Method")
    public String Method;
	@XmlElement(name = "TransactionType")
    public String TransactionType;
	@XmlElement(name = "CustomerIP")
    public String CustomerIP;
	@XmlElement(name = "DeviceID")
    public String DeviceID;

    public CreateAccessCodeRequest()
    {
        Customer = new Customer();
        ShippingAddress = new ShippingAddress();
        Payment = new Payment();
    }
}
