package com.eway.xml.bean;

import javax.xml.bind.annotation.XmlElement;


@com.sun.xml.internal.txw2.annotation.XmlElement
public class LineItem
{
	private String SKU = null;
	private String Description = null;
	private int Quantity = 0;
	private int UnitCost = 0;
	private int Tax = 0;
	private int Total = 0;
	
	public String getSKU()
	{
		return SKU;
	}
	@XmlElement(name = "SKU")
	public void setSKU(String sku)
	{
		SKU = sku;
	}

	public String getDescription()
	{
		return Description;
	}
	@XmlElement(name = "Description")
	public void setDescription(String description)
	{
		Description = description;
	}
	
	public int getQuantity() {
		return Quantity;
	}
	@XmlElement(name = "Quantity")
	public void setQuantity(int quantity) {
		Quantity = quantity;
	}
	
	public int getUnitCost() {
		return UnitCost;
	}
	@XmlElement(name = "UnitCost")
	public void setUnitCost(int unitCost) {
		UnitCost = unitCost;
	}
	
	public int getTax() {
		return Tax;
	}
	@XmlElement(name = "Tax")
	public void setTax(int tax) {
		Tax = tax;
	}
	
	public int getTotal() {
		return Total;
	}
	@XmlElement(name = "Total")
	public void setTotal(int total) {
		Total = total;
	}
	
}