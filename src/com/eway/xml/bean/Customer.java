package com.eway.xml.bean;

import javax.xml.bind.annotation.XmlElement;

@com.sun.xml.internal.txw2.annotation.XmlElement
public class Customer
{
	private String TokenCustomerID = null;
    private String Reference = null;
    private String Title = null;
    private String FirstName = null;
    private String LastName = null;
    private String CompanyName = null;
    private String JobDescription = null;
    private String Street1 = null;
    private String Street2 = null;
    private String City = null;
    private String State = null;
    private String PostalCode = null;
    private String Country = null;
    private String Email = null;
    private String Phone = null;
    private String Mobile = null;
    private String Comments = null;
    private String Fax = null;
    private String Url = null;
    private String CardNumber = null;
    private String CardStartMonth = null;
    private String CardStartYear = null;
    private String CardIssueNumber = null;
    private String CardName = null;
    private String CardExpiryMonth = null;
    private String CardExpiryYear = null;
    private String IsActive = null;
    
    public String getTokenCustomerID()
	{
		return TokenCustomerID;
	}
    @XmlElement(name = "TokenCustomerID")
	public void setTokenCustomerID(String tokenCustomerID)
	{
		this.TokenCustomerID = tokenCustomerID;
	}
	
	public String getReference()
	{
		return Reference;
	}
	@XmlElement(name = "Reference")
	public void setReference(String Reference)
	{
		this.Reference = Reference;
	}
	
	public String getTitle()
	{
		return Title;
	}
	@XmlElement(name = "Title")
	public void setTitle(String title)
	{
		this.Title = title;
	}
	
	public String getFirstName()
	{
		return FirstName;
	}
	@XmlElement(name = "FirstName")
	public void setFirstName(String firstName)
	{
		this.FirstName = firstName;
	}
	
	public String getLastName()
	{
		return LastName;
	}
	@XmlElement(name = "LastName")
	public void setLastName(String lastName)
	{
		this.LastName = lastName;
	}
	
	public String getCompanyName()
	{
		return CompanyName;
	}
	@XmlElement(name = "CompanyName")
	public void setCompanyName(String companyName)
	{
		this.CompanyName = companyName;
	}
	
	public String getJobDescription()
	{
		return JobDescription;
	}
	@XmlElement(name = "JobDescription")
	public void setJobDescription(String jobDescription)
	{
		this.JobDescription = jobDescription;
	}
	
	public String getStreet1()
	{
		return Street1;
	}
	@XmlElement(name = "Street1")
	public void setStreet1(String street1)
	{
		this.Street1 = street1;
	}
	
	public String getStreet2()
	{
		return Street2;
	}
	@XmlElement(name = "Street2")
	public void setStreet2(String street2)
	{
		this.Street2 = street2;
	}
	
	public String getCity()
	{
		return City;
	}
	@XmlElement(name = "City")
	public void setCity(String city)
	{
		this.City = city;
	}
	
	public String getState()
	{
		return State;
	}
	@XmlElement(name = "State")
	public void setState(String state)
	{
		this.State = state;
	}
	
	public String getPostalCode()
	{
		return PostalCode;
	}
	@XmlElement(name = "PostalCode")
	public void setPostalCode(String postalCode)
	{
		this.PostalCode = postalCode;
	}
	
	public String getCountry()
	{
		return Country;
	}
	@XmlElement(name = "Country")
	public void setCountry(String country)
	{
		this.Country = country;
	}
	
	public String getEmail()
	{
		return Email;
	}
	@XmlElement(name = "Email")
	public void setEmail(String email)
	{
		this.Email = email;
	}
	
	public String getPhone()
	{
		return Phone;
	}
	@XmlElement(name = "Phone")
	public void setPhone(String phone)
	{
		this.Phone = phone;
	}
	
	public String getMobile()
	{
		return Mobile;
	}
	@XmlElement(name = "Mobile")
	public void setMobile(String mobile)
	{
		this.Mobile = mobile;
	}
	
	public String getComments()
	{
		return Comments;
	}
	@XmlElement(name = "Comments")
	public void setComments(String comments)
	{
		this.Comments = comments;
	}
	
	public String getFax()
	{
		return Fax;
	}
	@XmlElement(name = "Fax")
	public void setFax(String fax)
	{
		this.Fax = fax;
	}
	
	public String getUrl()
	{
		return Url;
	}
	@XmlElement(name = "Url")
	public void setUrl(String url)
	{
		this.Url = url;
	}
	
	public String getCardNumber()
	{
		return CardNumber;
	}
	@XmlElement(name = "CardNumber")
	public void setCardNumber(String cardNumber)
	{
		CardNumber = cardNumber;
	}
	
	public String getCardStartMonth()
	{
		return CardStartMonth;
	}
	@XmlElement(name = "CardStartMonth")
	public void setCardStartMonth(String cardStartMonth)
	{
		CardStartMonth = cardStartMonth;
	}
	
	public String getCardStartYear()
	{
		return CardStartYear;
	}
	@XmlElement(name = "CardStartYear")
	public void setCardStartYear(String cardStartYear)
	{
		CardStartYear = cardStartYear;
	}
	
	public String getCardIssueNumber()
	{
		return CardIssueNumber;
	}
	@XmlElement(name = "CardIssueNumber")
	public void setCardIssueNumber(String cardIssueNumber)
	{
		CardIssueNumber = cardIssueNumber;
	}
	
	public String getCardName()
	{
		return CardName;
	}
	@XmlElement(name = "CardName")
	public void setCardName(String cardName)
	{
		CardName = cardName;
	}
	
	public String getCardExpiryMonth()
	{
		return CardExpiryMonth;
	}
	@XmlElement(name = "CardExpiryMonth")
	public void setCardExpiryMonth(String cardExpiryMonth)
	{
		CardExpiryYear = cardExpiryMonth;
	}
	
	public String getCardExpiryYear()
	{
		return CardExpiryYear;
	}
	@XmlElement(name = "CardExpiryYear")
	public void setCardExpiryYear(String cardExpiryYear)
	{
		CardExpiryYear = cardExpiryYear;
	}
	
	public String getIsActive()
	{
		return IsActive;
	}
	@XmlElement(name = "IsActive")
	public void setIsActive(String isActive)
	{
		IsActive = isActive;
	}
}
