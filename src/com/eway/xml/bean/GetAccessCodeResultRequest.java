package com.eway.xml.bean;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "GetAccessCodeResultRequest")
public class GetAccessCodeResultRequest
{
	private String AccessCode = null;
	
	public String getAccessCode()
	{
		return AccessCode;
	}
	@XmlElement(name = "AccessCode")
	public void setAccessCode(String accessCode)
	{
		AccessCode = accessCode;
	}
}
