package com.eway.xml.bean;

import javax.xml.bind.annotation.XmlElement;

@com.sun.xml.internal.txw2.annotation.XmlElement
public class Option
{
	private String Value = null;

	public String getValue()
	{
		return Value;
	}
	@XmlElement(name = "Value")
	public void setValue(String value)
	{
		this.Value = value;
	}
}
