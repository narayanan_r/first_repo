package com.eway.xml.bean;

import javax.xml.bind.annotation.XmlElement;


@com.sun.xml.internal.txw2.annotation.XmlElement
public class Payment
{
	private String TotalAmount = null;
	/// <summary>The merchant's invoice number</summary>
	private String InvoiceNumber = null;
	/// <summary>merchants invoice description</summary>
	private String InvoiceDescription = null;
	/// <summary>The merchant's invoice reference</summary>
	private String InvoiceReference = null;
	/// <summary>The merchant's currency</summary>
	private String CurrencyCode = null;
	
	
	public String getTotalAmount()
	{
		return TotalAmount;
	}
	@XmlElement(name = "TotalAmount")
	public void setTotalAmount(String totalAmount)
	{
		this.TotalAmount = totalAmount;
	}
	
	public String getInvoiceNumber()
	{
		return InvoiceNumber;
	}
	@XmlElement(name = "InvoiceNumber")
	public void setInvoiceNumber(String invoiceNumber)
	{
		this.InvoiceNumber = invoiceNumber;
	}
	
	public String getInvoiceDescription()
	{
		return InvoiceDescription;
	}
	@XmlElement(name = "InvoiceDescription")
	public void setInvoiceDescription(String invoiceDescription)
	{
		this.InvoiceDescription = invoiceDescription;
	}
	
	public String getInvoiceReference()
	{
		return InvoiceReference;
	}
	@XmlElement(name = "InvoiceReference")
	public void setInvoiceReference(String invoiceReference)
	{
		this.InvoiceReference = invoiceReference;
	}
	
	public String getCurrencyCode()
	{
		return CurrencyCode;
	}
	@XmlElement(name = "CurrencyCode")
	public void setCurrencyCode(String currencyCode)
	{
		this.CurrencyCode = currencyCode;
	}
}
