package com.eway.xml.bean;

import javax.xml.bind.annotation.XmlElement;


@com.sun.xml.internal.txw2.annotation.XmlElement
public class ShippingAddress
{
    private String FirstName = null;
    private String LastName = null;
    private String Street1 = null;
    private String Street2 = null;
    private String City = null;
    private String State = null;
    private String Country = null;
    private String PostalCode = null;
    private String Email = null;
    private String Phone = null;
    private String ShippingMethod = null;
    
    
	public String getFirstName()
	{
		return FirstName;
	}
	@XmlElement(name = "FirstName")
	public void setFirstName(String firstName)
	{
		this.FirstName = firstName;
	}
	
	public String getLastName()
	{
		return LastName;
	}
	@XmlElement(name = "LastName")
	public void setLastName(String lastName)
	{
		this.LastName = lastName;
	}
	
	public String getStreet1()
	{
		return Street1;
	}
	@XmlElement(name = "Street1")
	public void setStreet1(String street1)
	{
		this.Street1 = street1;
	}
	
	public String getStreet2()
	{
		return Street2;
	}
	@XmlElement(name = "Street2")
	public void setStreet2(String street2)
	{
		this.Street2 = street2;
	}
	
	public String getCity()
	{
		return City;
	}
	@XmlElement(name = "City")
	public void setCity(String city)
	{
		this.City = city;
	}
	
	public String getState()
	{
		return State;
	}
	@XmlElement(name = "State")
	public void setState(String state)
	{
		this.State = state;
	}
	
	public String getCountry()
	{
		return Country;
	}
	@XmlElement(name = "Country")
	public void setCountry(String country)
	{
		this.Country = country;
	}
	
	public String getPostalCode()
	{
		return PostalCode;
	}
	@XmlElement(name = "PostalCode")
	public void setPostalCode(String postalCode)
	{
		this.PostalCode = postalCode;
	}
	
	public String getEmail()
	{
		return Email;
	}
	@XmlElement(name = "Email")
	public void setEmail(String email)
	{
		this.Email = email;
	}
	
	public String getPhone()
	{
		return Phone;
	}
	@XmlElement(name = "Phone")
	public void setPhone(String phone)
	{
		this.Phone = phone;
	}
	
	public String getShippingMethod()
	{
		return ShippingMethod;
	}
	@XmlElement(name = "ShippingMethod")
	public void setShippingMethod(String shippingMethod)
	{
		this.ShippingMethod = shippingMethod;
	}
}
