package com.eway.json.bean;

import javax.xml.bind.annotation.XmlElement;

public class CardDetails {
	
	private String Name = null;
    private String Number = null;
    private String ExpiryMonth = null;
    private String ExpiryYear = null;
    private String CVN = null;
    
	public String getName() {
		return Name;
	}
	@XmlElement(name = "Name")
	public void setName(String name) {
		Name = name;
	}
	public String getNumber() {
		return Number;
	}
	@XmlElement(name = "Number")
	public void setNumber(String number) {
		Number = number;
	}
	public String getExpiryMonth() {
		return ExpiryMonth;
	}
	@XmlElement(name = "ExpiryMonth")
	public void setExpiryMonth(String expiryMonth) {
		ExpiryMonth = expiryMonth;
	}
	public String getExpiryYear() {
		return ExpiryYear;
	}
	@XmlElement(name = "ExpiryYear")
	public void setExpiryYear(String expiryYear) {
		ExpiryYear = expiryYear;
	}
	public String getCVN() {
		return CVN;
	}
	@XmlElement(name = "CVN")
	public void setCVN(String cVN) {
		CVN = cVN;
	}
  
    
}

