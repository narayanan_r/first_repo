package com.eway.json.bean;

import javax.xml.bind.annotation.XmlElement;


@com.sun.xml.internal.txw2.annotation.XmlElement
public class LineItem
{
	private String SKU = null;
	private String Description = null;

	
	public String getSKU()
	{
		return SKU;
	}
	@XmlElement(name = "SKU")
	public void setSKU(String sku)
	{
		SKU = sku;
	}

	public String getDescription()
	{
		return Description;
	}
	@XmlElement(name = "Description")
	public void setDescription(String description)
	{
		Description = description;
	}
}