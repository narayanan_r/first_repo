package com.eway.json.bean;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "GetAccessCodeResultResponse")
public class GetAccessCodeResultResponse
{
	private String AccessCode = null;
	private String AuthorisationCode = null;
	private String ResponseCode = null;
	private String ResponseMessage = null;
	private String InvoiceNumber = null;
	private String InvoiceReference = null;
	private String TotalAmount = null;
	private String TransactionID = null;
	private String TransactionStatus = null;
	private String TokenCustomerID = null;
	private String BeagleScore = null;
	
	public Verification Verification = null;
	public Option[] Options = null;
	@XmlElement(name = "Errors")
	public String Errors = null;
	
	
	public GetAccessCodeResultResponse()
	{
		Verification = new Verification();
	}
	
	
	public String getAccessCode()
	{
		return AccessCode;
	}
	@XmlElement(name = "AccessCode")
	public void setAccessCode(String accessCode)
	{
		AccessCode = accessCode;
	}
	
	public String getAuthorisationCode()
	{
		return AuthorisationCode;
	}
	@XmlElement(name = "AuthorisationCode")
	public void setAuthorisationCode(String authorisationCode)
	{
		AuthorisationCode = authorisationCode;
	}

	public String getResponseCode()
	{
		return ResponseCode;
	}
	@XmlElement(name = "ResponseCode")
	public void setResponseCode(String responseCode)
	{
		ResponseCode = responseCode;
	}

	public String getResponseMessage()
	{
		return ResponseMessage;
	}
	@XmlElement(name = "ResponseMessage")
	public void setResponseMessage(String responseMessage)
	{
		ResponseMessage = responseMessage;
	}

	public String getInvoiceNumber()
	{
		return InvoiceNumber;
	}
	@XmlElement(name = "InvoiceNumber")
	public void setInvoiceNumber(String invoiceNumber)
	{
		InvoiceNumber = invoiceNumber;
	}

	public String getInvoiceReference()
	{
		return InvoiceReference;
	}
	@XmlElement(name = "InvoiceReference")
	public void setInvoiceReference(String invoiceReference)
	{
		InvoiceReference = invoiceReference;
	}

	public String getTotalAmount()
	{
		return TotalAmount;
	}
	@XmlElement(name = "TotalAmount")
	public void setTotalAmount(String totalAmount)
	{
		TotalAmount = totalAmount;
	}

	public String getTransactionID()
	{
		return TransactionID;
	}
	@XmlElement(name = "TransactionID")
	public void setTransactionID(String transactionID)
	{
		TransactionID = transactionID;
	}

	public String getTransactionStatus()
	{
		return TransactionStatus;
	}
	@XmlElement(name = "TransactionStatus")
	public void setTransactionStatus(String transactionStatus)
	{
		TransactionStatus = transactionStatus;
	}

	public String getTokenCustomerID()
	{
		return TokenCustomerID;
	}
	@XmlElement(name = "TokenCustomerID")
	public void setTokenCustomerID(String tokenCustomerID)
	{
		TokenCustomerID = tokenCustomerID;
	}

	public String getBeagleScore()
	{
		return BeagleScore;
	}
	@XmlElement(name = "BeagleScore")
	public void setBeagleScore(String beagleScore)
	{
		BeagleScore = beagleScore;
	}
}
