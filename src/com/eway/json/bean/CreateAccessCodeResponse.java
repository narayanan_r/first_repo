package com.eway.json.bean;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name = "CreateAccessCodeResponse")
public class CreateAccessCodeResponse
{
	public Customer Customer;
	public Payment Payment;
	/*public ShippingAddress ShippingAddress;
	public Items Items;
	public Options Options;*/
    
	/*public String RedirectUrl;
    public String Method;*/
	
	@XmlElement(name = "AccessCode")
    public String AccessCode;
	@XmlElement(name = "FormActionURL")
    public String FormActionURL;
	@XmlElement(name = "Errors")
    public String Errors;
    
    /*private CustomerIP;
    private DeviceID;*/

    public CreateAccessCodeResponse()
    {
        Customer = new Customer();
        Payment = new Payment();
        
       /* ShippingAddress = new ShippingAddress();*/
        /*Items = new Items();
        Options = new Options();*/
       /* customerIP = $_SERVER["SERVER_NAME"];*/
    }
}
