package com.eway.json.bean;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "CreateAccessCodeRequest")
public class CreateAccessCodeRequest
{
	public Customer Customer;
	public ShippingAddress ShippingAddress;
	public LineItem[] Items = new LineItem[2];
	public Option[] Options = new Option[3];
	public Payment Payment;
    
	@XmlElement(name = "RedirectUrl")
	public String RedirectUrl;
	@XmlElement(name = "Method")
    public String Method;
	@XmlElement(name = "CustomerIP")
    public String CustomerIP;
	@XmlElement(name = "DeviceID")
    public String DeviceID;

    public CreateAccessCodeRequest()
    {
        Customer = new Customer();
        ShippingAddress = new ShippingAddress();
        Payment = new Payment();
    }
}
