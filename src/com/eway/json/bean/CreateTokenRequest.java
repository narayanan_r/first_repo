package com.eway.json.bean;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "CreateTokenRequest")
public class CreateTokenRequest
{
	public Customer Customer;
	public Payment Payment;
//	public CardDetails CardDetails;
    
	@XmlElement(name = "RedirectUrl")
	public String RedirectUrl;
	@XmlElement(name = "Method")
    public String Method;
	@XmlElement(name = "TransactionType")
    public String TransactionType;
	@XmlElement(name = "CustomerIP")
    public String CustomerIP;
	@XmlElement(name = "DeviceID")
    public String DeviceID;

    public CreateTokenRequest()
    {
        Customer = new Customer();
        Payment = new Payment();
//        CardDetails = new CardDetails();
    }
}
