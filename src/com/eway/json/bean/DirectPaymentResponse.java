package com.eway.json.bean;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name = "DirectPaymentResponse")
public class DirectPaymentResponse
{
	public Customer Customer;
	public Payment Payment;
//	public CardDetails CardDetails;
	/*public ShippingAddress ShippingAddress;
	public Items Items;
	public Options Options;*/
    
	/*public String RedirectUrl;
    public String Method;*/
	
	@XmlElement(name = "TokenCustomerID")
    public String TokenCustomerID;
//	@XmlElement(name = "DirectPaymentResponse")
//    public String DirectPaymentResponse;
	@XmlElement(name = "Errors")
    public String Errors;
    
    /*private CustomerIP;
    private DeviceID;*/

    public DirectPaymentResponse()
    {
        Customer = new Customer();
        Payment = new Payment();
//        CardDetails = new CardDetails();
        
       /* ShippingAddress = new ShippingAddress();*/
        /*Items = new Items();
        Options = new Options();*/
       /* customerIP = $_SERVER["SERVER_NAME"];*/
    }
}
