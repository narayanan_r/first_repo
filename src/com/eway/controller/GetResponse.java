package com.eway.controller;

import java.io.IOException;
import java.util.ResourceBundle;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.eway.json.bean.GetAccessCodeResultRequest;
import com.eway.json.bean.GetAccessCodeResultResponse;
import com.eway.process.RapidAPI;

/***
 *  GetResponse Servlet is called by eWAY as a redirect url. It requests the results of the transaction from eWAY.
 */

public class GetResponse extends HttpServlet
{
	private static final long serialVersionUID = 1L;
	private final ResourceBundle configResource = ResourceBundle.getBundle("config");
	
	public GetResponse()
	{
		super();
	}

	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		HttpSession session = request.getSession(false);
		
		try
		{
			String accessCode = request.getParameter("AccessCode");
			System.out.println("accessCode"+accessCode);
			String method = configResource.getString("requestMethod");
			String format = configResource.getString("requestFormat");
			System.out.println("method"+method);
			System.out.println("format"+format);
			
			if(method.equalsIgnoreCase("POST") && format.equalsIgnoreCase("JSON"))
			{
				GetAccessCodeResultResponse GetAccessCodeResultResponse = new GetAccessCodeResultResponse();
				
				GetAccessCodeResultRequest GetAccessCodeResultRequest = new GetAccessCodeResultRequest();
				GetAccessCodeResultRequest.setAccessCode(accessCode);
				
				RapidAPI rapid = new RapidAPI();
				GetAccessCodeResultResponse = rapid.GetAccessCodeResult(GetAccessCodeResultRequest);
				
				if(GetAccessCodeResultResponse.Errors == null || GetAccessCodeResultResponse.Errors.equals("") || GetAccessCodeResultResponse.Errors == "")
				{
					session.setAttribute("GetResponse", GetAccessCodeResultResponse);
					response.sendRedirect("resultjson.jsp");
				}
				else
				{
					session.setAttribute("errors", GetAccessCodeResultResponse.Errors);
					response.sendRedirect("personalDetail.jsp");
				}
			}
			else if(method.equalsIgnoreCase("POST") && format.equalsIgnoreCase("XML"))
			{
				com.eway.xml.bean.GetAccessCodeResultResponse GetAccessCodeResultResponse = new com.eway.xml.bean.GetAccessCodeResultResponse();
				
				com.eway.xml.bean.GetAccessCodeResultRequest GetAccessCodeResultRequest = new com.eway.xml.bean.GetAccessCodeResultRequest();
				GetAccessCodeResultRequest.setAccessCode(accessCode);
				
				RapidAPI rapid = new RapidAPI();
				GetAccessCodeResultResponse = rapid.GetAccessCodeResult(GetAccessCodeResultRequest);
				
				if(GetAccessCodeResultResponse.Errors == null || GetAccessCodeResultResponse.Errors.equals("") || GetAccessCodeResultResponse.Errors == "")
				{
					session.setAttribute("GetResponse", GetAccessCodeResultResponse);
					System.out.println("GetResponse"+GetAccessCodeResultResponse.getTransactionID());
					response.sendRedirect("resultxml.jsp");
				}
				else
				{
					session.setAttribute("errors", GetAccessCodeResultResponse.Errors);
					response.sendRedirect("personalDetail.jsp");
				}
			}
		}
		catch (Exception e)
		{
			System.out.println("\nException in GetResponse\n");
			e.printStackTrace();
			response.sendRedirect("personalDetail.jsp");
		}
	}
}
