package com.eway.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.eway.xml.bean.CreateAccessCodeRequest;
import com.eway.xml.bean.CreateAccessCodeResponse;
import com.eway.xml.bean.Items;
import com.eway.xml.bean.LineItem;
import com.eway.xml.bean.Option;
import com.eway.xml.bean.Options;
import com.eway.process.RapidAPI;

/****
 *  RapidAPIXMLService Servlet is responsible for handling XML based calls by sending a request to eWAY containing details of 
 *  the transaction, including the amount, the invoice number and the customer details.
 */

public class RapidAPIXMLService extends HttpServlet
{
	private static final long serialVersionUID = 1L;

	public RapidAPIXMLService()
	{
		super();
	}

	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		HttpSession session = request.getSession(true);
		
		try
		{
			String txtCustomerRef = request.getParameter("txtCustomerRef");
			String ddlTitle = request.getParameter("ddlTitle");
			String txtFirstName = request.getParameter("txtFirstName");
			String txtLastName = request.getParameter("txtLastName");
			String txtCompanyName = request.getParameter("txtCompanyName");
			String txtJobDescription = request.getParameter("txtJobDescription");
			String txtStreet = request.getParameter("txtStreet");
			String txtCity = request.getParameter("txtCity");
			String txtState = request.getParameter("txtState");
			String txtPostalcode = request.getParameter("txtPostalcode");
			String txtCountry = request.getParameter("txtCountry");
			String txtEmail = request.getParameter("txtEmail");
			String txtPhone = request.getParameter("txtPhone");
			String txtMobile = request.getParameter("txtMobile");
			String txtComments = request.getParameter("txtComments");
			String txtFax = request.getParameter("txtFax");
			String txtUrl = request.getParameter("txtUrl");
			
			String txtShippingFirstName = request.getParameter("txtShippingFirstName");
			String txtShippingLastName = request.getParameter("txtShippingLastName");
			String txtShippingStreet1 = request.getParameter("txtShippingStreet1");
			String txtShippingStreet2 = request.getParameter("txtShippingStreet2");
			String txtShippingCity = request.getParameter("txtShippingCity");
			String txtShippingState = request.getParameter("txtShippingState");
			String txtShippingCountry = request.getParameter("txtShippingCountry");
			String txtShippingPostalCode = request.getParameter("txtShippingPostalCode");
			String txtShippingEmail = request.getParameter("txtShippingEmail");
			String txtShippingPhone = request.getParameter("txtShippingPhone");
			
			String txtAmount = request.getParameter("txtAmount");
			String txtInvoiceNumber = request.getParameter("txtInvoiceNumber");
			String txtInvoiceDescription = request.getParameter("txtInvoiceDescription");
			String txtInvoiceReference = request.getParameter("txtInvoiceReference");
			String txtCurrencyCode = request.getParameter("txtCurrencyCode");
			
			String txtOption1 = request.getParameter("txtOption1");
			String txtOption2 = request.getParameter("txtOption2");
			String txtOption3 = request.getParameter("txtOption3");
			
			String txtRedirectURL = request.getParameter("txtRedirectURL");
			String ddlMethod = request.getParameter("ddlMethod");
			
			//Create AccessCode Request Object
			CreateAccessCodeRequest CreateAccessCodeRequest = new CreateAccessCodeRequest();
			
			//Populate values for Customer Object
		    //Note: TokenCustomerID is Required Field When Update an existing TokenCustomer
			CreateAccessCodeRequest.Customer.setReference(txtCustomerRef);
			CreateAccessCodeRequest.Customer.setFirstName(txtFirstName); 		//Note: FirstName is Required Field When Create/Update a TokenCustomer
			CreateAccessCodeRequest.Customer.setLastName(txtLastName);			//Note: LastName is Required Field When Create/Update a TokenCustomer
			CreateAccessCodeRequest.Customer.setReference(txtInvoiceReference);
			CreateAccessCodeRequest.Customer.setTitle(ddlTitle); 				//Note: Title is Required Field When Create/Update a TokenCustomer
			CreateAccessCodeRequest.Customer.setCompanyName(txtCompanyName);
			CreateAccessCodeRequest.Customer.setJobDescription(txtJobDescription);
			CreateAccessCodeRequest.Customer.setStreet1(txtStreet);
			CreateAccessCodeRequest.Customer.setCity(txtCity);
			CreateAccessCodeRequest.Customer.setState(txtState);
			CreateAccessCodeRequest.Customer.setPostalCode(txtPostalcode);
			CreateAccessCodeRequest.Customer.setCountry(txtCountry);			//Note: Country is Required Field When Create/Update a TokenCustomer
			CreateAccessCodeRequest.Customer.setEmail(txtEmail);
			CreateAccessCodeRequest.Customer.setPhone(txtPhone);
			CreateAccessCodeRequest.Customer.setMobile(txtMobile);
			CreateAccessCodeRequest.Customer.setComments(txtComments);
			CreateAccessCodeRequest.Customer.setFax(txtFax);
			CreateAccessCodeRequest.Customer.setUrl(txtUrl);
			
			//Populate values for ShippingAddress Object.
		    //This values can be taken from a Form POST as well. Now is just some dummy data.
			CreateAccessCodeRequest.ShippingAddress.setFirstName(txtShippingFirstName);
			CreateAccessCodeRequest.ShippingAddress.setLastName(txtShippingLastName);
			CreateAccessCodeRequest.ShippingAddress.setStreet1(txtShippingStreet1);
			CreateAccessCodeRequest.ShippingAddress.setStreet2(txtShippingStreet2);
			CreateAccessCodeRequest.ShippingAddress.setCity(txtShippingCity);
			CreateAccessCodeRequest.ShippingAddress.setState(txtShippingState);
			CreateAccessCodeRequest.ShippingAddress.setCountry(txtShippingCountry);
			CreateAccessCodeRequest.ShippingAddress.setPostalCode(txtShippingPostalCode);
			CreateAccessCodeRequest.ShippingAddress.setEmail(txtShippingEmail);
			CreateAccessCodeRequest.ShippingAddress.setPhone(txtShippingPhone);
			CreateAccessCodeRequest.ShippingAddress.setShippingMethod("LowCost");		//ShippingMethod, e.g. "LowCost", "International", "Military". Check the spec for available values.
			
			//Populate values for Payment Object
		    //Note: TotalAmount is a Required Field When Process a Payment, TotalAmount should set to "0" or leave EMPTY when Create/Update A TokenCustomer
			CreateAccessCodeRequest.Payment.setTotalAmount(txtAmount);
			CreateAccessCodeRequest.Payment.setInvoiceNumber(txtInvoiceNumber);
			CreateAccessCodeRequest.Payment.setInvoiceDescription(txtInvoiceDescription);
			CreateAccessCodeRequest.Payment.setInvoiceReference(txtInvoiceReference);
			CreateAccessCodeRequest.Payment.setCurrencyCode(txtCurrencyCode);
			
			//Populate values for LineItems
			LineItem lineItem1 = new LineItem();
			lineItem1.setSKU("SKU1");
			lineItem1.setDescription("Description1");
			
			LineItem lineItem2 = new LineItem();
			lineItem2.setSKU("SKU2");
			lineItem2.setDescription("Description2");
			
			CreateAccessCodeRequest.Items = new Items();
			CreateAccessCodeRequest.Items.LineItem[0] = lineItem1;
			CreateAccessCodeRequest.Items.LineItem[1] = lineItem2;
			
			
			//Populate values for Options
			Option option1 = new Option();
			option1.setValue(txtOption1);
			
			Option option2 = new Option();
			option2.setValue(txtOption2);
			
			Option option3 = new Option();
			option3.setValue(txtOption3);
			
			CreateAccessCodeRequest.Options = new Options();
			CreateAccessCodeRequest.Options.Option[0] = option1;
			CreateAccessCodeRequest.Options.Option[1] = option2;
			CreateAccessCodeRequest.Options.Option[2] = option3;
			
			
			//Url to the page for getting the result with an AccessCode
		    //Note: RedirectUrl is a Required Field For all cases
			CreateAccessCodeRequest.RedirectUrl = txtRedirectURL;
			
			//Method for this request. e.g. ProcessPayment, Create TokenCustomer, Update TokenCustomer & TokenPayment
			CreateAccessCodeRequest.Method = ddlMethod;
			
			//Create RapidAPI Service
			RapidAPI rapidAPI = new RapidAPI();
			
			//Call RapidAPI
			CreateAccessCodeResponse CreateAccessCodeResponse = rapidAPI.CreateAccessCode(CreateAccessCodeRequest);
			
			
			//Check if any error returns
			if(CreateAccessCodeResponse.Errors == null || CreateAccessCodeResponse.Errors.equals("") || CreateAccessCodeResponse.Errors == "")
			{
				//Save result into Session. payment.jsp and results.jsp will retrieve this result from Session
				session.setAttribute("TotalAmount", txtAmount);
				session.setAttribute("InvoiceReference", txtInvoiceReference);
				session.setAttribute("ResponseText", CreateAccessCodeResponse);
				
				//All good then redirect to the payment page
				response.sendRedirect("paymentxml.jsp");
			}
			else
			{
				session.setAttribute("errors", CreateAccessCodeResponse.Errors);
				response.sendRedirect("checkout.jsp");
			}
		}
		catch (Exception e)
		{
			System.out.println("\nException in RapidAPIXMLService\n");
			e.printStackTrace();
			response.sendRedirect("checkout.jsp");
		}
	}
}
