package au.com.ourbodycorp;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.LinkedList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import au.com.ourbodycorp.model.ProgramExercise;
import au.com.ourbodycorp.model.managers.LibraryManager;

/**
 * Servlet implementation class GetAdherenceScore
 */
public class GetAdherenceScore extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		 LibraryManager lm=new LibraryManager();
		 try {
		       String programId = request.getParameter("programId");
		       if (StringUtils.isBlank(programId)) {
		         response.sendError(400, "No programId provided");
		         return;
		       }
		       LinkedList<ProgramExercise> pe=lm.getAdherenceScore(Integer.parseInt(programId));
		 }catch (Exception e) {
		       throw new ServletException(e);
	     }
    }
}
