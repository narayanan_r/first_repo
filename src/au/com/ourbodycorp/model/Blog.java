package au.com.ourbodycorp.model;

import au.com.ourbodycorp.model.managers.PostManager;

import java.util.List;

public class Blog {
  private long id;
  private String name;
  private String slug;
  private String tagline;
  private long banner;
  private long owner;
  private List<Long> users;
  private long postId;
  private String indexTemplate;
  private String entryTemplate;
  private String archiveTemplate;

  private String indexJsp;
  private String entryJsp;
  private String archiveJsp;

  private long indexCount;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSlug() {
    return slug;
  }

  public void setSlug(String slug) {
    this.slug = slug;
  }

  public String getTagline() {
    return tagline;
  }

  public void setTagline(String tagline) {
    this.tagline = tagline;
  }

  public long getBanner() {
    return banner;
  }

  public void setBanner(long banner) {
    this.banner = banner;
  }

  public long getOwner() {
    return owner;
  }

  public void setOwner(long owner) {
    this.owner = owner;
  }

  public long getPostId() {
    return postId;
  }

  public void setPostId(long postId) {
    this.postId = postId;
  }

  public List<Long> getUsers() throws Exception {
    if (users == null && id > 0) {
      PostManager pm = new PostManager();
      users = pm.getBlogUsers(id);
    }
    return users;
  }

  public String getIndexTemplate() {
    return indexTemplate;
  }

  public void setIndexTemplate(String indexTemplate) {
    this.indexTemplate = indexTemplate;
  }

  public String getEntryTemplate() {
    return entryTemplate;
  }

  public void setEntryTemplate(String entryTemplate) {
    this.entryTemplate = entryTemplate;
  }

  public String getArchiveTemplate() {
    return archiveTemplate;
  }

  public void setArchiveTemplate(String archiveTemplate) {
    this.archiveTemplate = archiveTemplate;
  }

  public String getIndexJsp() {
    return indexJsp;
  }

  public void setIndexJsp(String indexJsp) {
    this.indexJsp = indexJsp;
  }

  public String getEntryJsp() {
    return entryJsp;
  }

  public void setEntryJsp(String entryJsp) {
    this.entryJsp = entryJsp;
  }

  public String getArchiveJsp() {
    return archiveJsp;
  }

  public void setArchiveJsp(String archiveJsp) {
    this.archiveJsp = archiveJsp;
  }

  public long getIndexCount() {
    return indexCount;
  }

  public void setIndexCount(long indexCount) {
    this.indexCount = indexCount;
  }
}
