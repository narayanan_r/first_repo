package au.com.ourbodycorp.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class Invoice {
  private long id;
  private long corpId;
  private long number;
  private Date created;
  private Date due;
  private long unitId;
  private long userId;
  private String company;
  private String name;
  private String address;
  private Date sent;
  private Date paid;
  private BigDecimal total;
  private List<InvoiceItem> items;
  private boolean complete;
  private int year;
  private long budgetItem;

  public long getBudgetItem() {
    return budgetItem;
  }

  public void setBudgetItem(long budgetItem) {
    this.budgetItem = budgetItem;
  }

  public int getYear() {
    return year;
  }

  public void setYear(int year) {
    this.year = year;
  }

  public boolean isComplete() {
    return complete;
  }

  public void setComplete(boolean complete) {
    this.complete = complete;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public long getCorpId() {
    return corpId;
  }

  public void setCorpId(long corpId) {
    this.corpId = corpId;
  }

  public long getNumber() {
    return number;
  }

  public void setNumber(long number) {
    this.number = number;
  }

  public Date getCreated() {
    return created;
  }

  public void setCreated(Date created) {
    this.created = created;
  }

  public Date getDue() {
    return due;
  }

  public void setDue(Date due) {
    this.due = due;
  }

  public long getUnitId() {
    return unitId;
  }

  public void setUnitId(long unitId) {
    this.unitId = unitId;
  }

  public long getUserId() {
    return userId;
  }

  public void setUserId(long userId) {
    this.userId = userId;
  }

  public String getCompany() {
    return company;
  }

  public void setCompany(String company) {
    this.company = company;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public Date getSent() {
    return sent;
  }

  public void setSent(Date sent) {
    this.sent = sent;
  }

  public Date getPaid() {
    return paid;
  }

  public void setPaid(Date paid) {
    this.paid = paid;
  }

  public List<InvoiceItem> getItems() {
    return items;
  }

  public void setItems(List<InvoiceItem> items) {
    this.items = items;
  }

  public BigDecimal getTotal() {
    return total;
  }

  public void setTotal(BigDecimal total) {
    this.total = total;
  }

  public String getStatus() {
    if (sent == null && paid == null) {
      return "unsent";
    } else if (getDue().before(new Date())) {
      return "overdue";
    } else if (paid == null) {
      return "unpaid";
    } else {
      return "paid";
    }
  }

}
