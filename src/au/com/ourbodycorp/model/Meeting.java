package au.com.ourbodycorp.model;

import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: bas
 * Date: 27/02/12
 * Time: 12:56 PM
 * To change this template use File | Settings | File Templates.
 */
public class Meeting {
  public Long id;
  public String title;
  public String type;
  public Date date;
  public Date end_date;
  public String location;
  public String status;
  public boolean minutesDone;
  public String minutes;
  public String agenda;
  public Long creator;
  public Date created;
  public Date updated;
  public String description;

  public enum Status {
    a("Accepted", "Accept", "icon-ok-sign"),
    m("Maybe", "Maybe", "icon-question-sign"),
    d("Declined", "Decline", "icon-remove-sign"),
    i("Invited", "Invite", "icon-info-sign");

    private String title;
    private String action;
    private String icon;

    Status(String title, String action, String icon) {
      this.action = action;
      this.title = title;
      this.icon = icon;
    }

    public String getAction() {
      return action;
    }

    public String getIcon() {
      return icon;
    }

    public void setIcon(String icon) {
      this.icon = icon;
    }

    public String getTitle() {
      return title;
    }
  }
}
