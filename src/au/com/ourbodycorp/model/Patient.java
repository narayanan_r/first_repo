package au.com.ourbodycorp.model;

import java.util.Date;

public class Patient {
  private long id;
  private long corpId;
  private String reference;
  private String firstName;
  private String lastName;
  private String email;
  private String phone;
  private Date created;
  private long creator;
  private String new_patient;
  private long cliniko_id;
  private String check_patient_exist;
  private String password;
  private long subscriptionId;
  private Date subscriptionDate; 
 
private boolean isuserloginfirst = false;
 

public boolean isIsuserloginfirst() {
	return isuserloginfirst;
}

public void setIsuserloginfirst(boolean isuserloginfirst) {
	this.isuserloginfirst = isuserloginfirst;
}

public long getSubscriptionId() {
	return subscriptionId;
}

public void setSubscriptionId(long subscriptionId) {
	this.subscriptionId = subscriptionId;
}

public Date getSubscriptionDate() {
	return subscriptionDate;
}

public void setSubscriptionDate(Date subscriptionDate) {
	this.subscriptionDate = subscriptionDate;
}

public String getFullName() {
    if (firstName != null && lastName != null) {
      return firstName + " " + lastName;
    } else if (firstName != null) {
      return firstName;
    } else if (lastName != null) {
      return lastName;
    } else {
      return null;
    }
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public long getCorpId() {
    return corpId;
  }

  public void setCorpId(long corpId) {
    this.corpId = corpId;
  }

  public String getReference() {
    return reference;
  }

  public void setReference(String reference) {
    this.reference = reference;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = (email != null) ? email.toLowerCase() : email;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public Date getCreated() {
    return created;
  }

  public void setCreated(Date created) {
    this.created = created;
  }

  public long getCreator() {
    return creator;
  }

  public void setCreator(long creator) {
    this.creator = creator;
  }

  public String getUserName() {
    return lastName.trim().toLowerCase().replaceAll("[^a-z0-9]+", "");
  }

	public String getNew_patient() {
		return new_patient;
	}
	
	public void setNew_patient(String new_patient) {
		this.new_patient = new_patient;
	}
	
	public long getCliniko_id() {
		return cliniko_id;
	}
	
	public void setCliniko_id(long cliniko_id) {
		this.cliniko_id = cliniko_id;
	}

	public String getCheck_patient_exist() {
		return check_patient_exist;
	}

	public void setCheck_patient_exist(String check_patient_exist) {
		this.check_patient_exist = check_patient_exist;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
  
}
