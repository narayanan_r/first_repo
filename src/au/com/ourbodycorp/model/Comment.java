package au.com.ourbodycorp.model;

import java.util.Date;

public class Comment {
  private long id;
  private long postId;
  private long author;
  private Date posted;
  private String comment;
  private String authorName;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public long getPostId() {
    return postId;
  }

  public void setPostId(long postId) {
    this.postId = postId;
  }

  public long getAuthor() {
    return author;
  }

  public void setAuthor(long author) {
    this.author = author;
  }

  public Date getPosted() {
    return posted;
  }

  public void setPosted(Date posted) {
    this.posted = posted;
  }

  public String getComment() {
    return comment;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }

  public String getAuthorName() {
    return authorName;
  }

  public void setAuthorName(String authorName) {
    this.authorName = authorName;
  }
}
