package au.com.ourbodycorp.model;

import java.util.Date;

public class Attendance {

  enum State {yes, no, maybe}

  private long postId;
  private long memberId;
  private Date lastChange;
  private State state;
  private String aircraft;
  private String notes;
}
