package au.com.ourbodycorp.model;

import java.util.Date;

public class ProgramExercise {
  private long id;
  private long programId;
  private long exerciseId;
  private int sequence;
  private String type;
  private String name;
  private String[] equipment;
  private int sets;
  private int reps;
  private int holds;
  private int rest;
  private String side;
  private String description;
  private String base;
  private Long keyPhotoId;
  private int keyPhotoVersion;
  private boolean showSides;
  private int maxSets;
  private int maxReps;
  private Date createdDate;
  private int weight;
  
  



public void setShowSides(boolean showSides) {
	this.showSides = showSides;
}

public int getWeight() {
	return weight;
}

public void setWeight(int weight) {
	this.weight = weight;
}

public Date getCreatedDate() {
	return createdDate;
}

public void setCreatedDate(Date createdDate) {
	this.createdDate = createdDate;
}

public boolean isShowSides() {
    return showSides;
  }


  public int getKeyPhotoVersion() {
    return keyPhotoVersion;
  }

  public void setKeyPhotoVersion(int keyPhotoVersion) {
    this.keyPhotoVersion = keyPhotoVersion;
  }

  public String getBase() {
    return base;
  }

  public void setBase(String base) {
    this.base = base;
  }



  public Long getKeyPhotoId() {
    return keyPhotoId;
  }

  public void setKeyPhotoId(Long keyPhotoId) {
    this.keyPhotoId = keyPhotoId;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public long getProgramId() {
    return programId;
  }

  public void setProgramId(long programId) {
    this.programId = programId;
  }

  public long getExerciseId() {
    return exerciseId;
  }

  public void setExerciseId(long exerciseId) {
    this.exerciseId = exerciseId;
  }

  public int getSequence() {
    return sequence;
  }

  public void setSequence(int sequence) {
    this.sequence = sequence;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String[] getEquipment() {
    return equipment;
  }

  public void setEquipment(String[] equipment) {
    this.equipment = equipment;
  }

  public int getSets() {
    return sets;
  }

  public void setSets(int sets) {
    this.sets = sets;
  }

  public int getReps() {
    return reps;
  }

  public void setReps(int reps) {
    this.reps = reps;
  }

  public int getHolds() {
    return holds;
  }

  public void setHolds(int holds) {
    this.holds = holds;
  }

  public int getRest() {
    return rest;
  }

  public void setRest(int rest) {
    this.rest = rest;
  }

  public String getSide() {
    return side;
  }
  
  public String getV1SafeSide() {
    if (side == null) return null;
    
    if (side.equalsIgnoreCase("alternating")) return "Both";
    
    return side;
  }

  public void setSide(String side) {
    this.side = side;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

	public int getMaxSets() {
		return maxSets;
	}

	public void setMaxSets(int maxSets) {
		this.maxSets = maxSets;
	}

	public int getMaxReps() {
		return maxReps;
	}

	public void setMaxReps(int maxReps) {
		this.maxReps = maxReps;
	}

}
