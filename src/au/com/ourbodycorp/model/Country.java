package au.com.ourbodycorp.model;

public class Country {
  private String iso;
  private String name;
  private String displayName;
  private String iso3;
  private int num;
  private String dial;

  public String getDial() {
    return dial;
  }

  public void setDial(String dial) {
    this.dial = dial;
  }

  public String getIso() {
    return iso;
  }

  public void setIso(String iso) {
    this.iso = iso;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDisplayName() {
    return displayName;
  }

  public void setDisplayName(String displayName) {
    this.displayName = displayName;
  }

  public String getIso3() {
    return iso3;
  }

  public void setIso3(String iso3) {
    this.iso3 = iso3;
  }

  public int getNum() {
    return num;
  }

  public void setNum(int num) {
    this.num = num;
  }
}
