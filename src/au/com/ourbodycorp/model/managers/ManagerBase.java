package au.com.ourbodycorp.model.managers;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import au.com.ourbodycorp.Config;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

public class ManagerBase {
  protected DataSource ds = null;

  public ManagerBase() {
    try {
      InitialContext ic = new InitialContext();
      ds = (DataSource) ic.lookup("java:comp/env/jdbc/WebDS");

    } catch (NamingException ne) {
      //Unlikely, so ignore, there will be an NPE in extending classes.
    }
  }

  protected long getSequence(String name, Connection conn) throws Exception {
    long id;

    PreparedStatement ps = conn.prepareStatement("select nextval(?) as id");
    ps.setString(1, name);
    ResultSet rs = ps.executeQuery();
    if (rs.next()) {
      id = rs.getLong("id");
    } else {
      throw new Exception("No sequence value");
    }
    return id;
  }

  protected long getSequence(String name) throws Exception {
    long id;
    Connection conn = null;
    try {
      conn = ds.getConnection();
      PreparedStatement ps = conn.prepareStatement("select nextval(?) as id");
      ps.setString(1, name);
      ResultSet rs = ps.executeQuery();
      if (rs.next()) {
        id = rs.getLong("id");
      } else {
        throw new Exception("No sequence value");
      }
    } finally {
      if (conn != null) {
        conn.close();
      }
    }
    return id;
  }
  
  protected String createInsert(String table, String[] columns) {
    StringBuilder sb = new StringBuilder();
    sb.append("insert into ").append(table).append(" (");
    int i = 0;
    for (String column : columns) {
      i++;
      sb.append(column);
      if (i < columns.length) {
        sb.append(", ");
      }
    }
    sb.append(") values (");
    i = 0;
    for (String column : columns) {
      i++;
      sb.append("?");
      if (i < columns.length) {
        sb.append(", ");
      }
    }

    sb.append(")");
    return sb.toString();
  }

  protected String createUpdate(String table, String[] columns, String where) {
    StringBuilder sb = new StringBuilder();

    sb.append("update ").append(table).append(" set ");
    int i = 0;
    for (String column : columns) {
      i++;
      sb.append(column).append(" = ?");
      if (i < columns.length) {
        sb.append(", ");
      }
    }
    
    sb.append(" where ").append(where);
    return sb.toString();
  }

  public String emptyToNull(String value) {
    if (value == null || value.trim().length() == 0) {
      return null;
    } else {
      return value;
    }
  }

  public int simpleCount(String sql) throws Exception {
    int rv = 0;
    Connection conn = ds.getConnection();
    try {
      Statement st = conn.createStatement();
      ResultSet rs = st.executeQuery(sql);
      if (rs.next()) {
        rv = rs.getInt("num");
      }
    } finally {
      conn.close();
    }
    return rv;
  }
}
