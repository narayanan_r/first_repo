package au.com.ourbodycorp.model.managers;

import au.com.ourbodycorp.model.Attachment;
import au.com.ourbodycorp.model.Folder;
import au.com.ourbodycorp.model.Post;

import java.sql.*;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class AttachmentManager extends ManagerBase {

  public static final String FOLDER_IMAGE = "Image Attachments";
  public static final String FOLDER_FILE = "File Attachments";
  public static final String FOLDER_MEDIA = "Media Attachments";

  private static HashMap<String, Folder> foldersByName = new HashMap<String, Folder>();

  public Folder getRootFolder(String name) throws Exception {
    Folder f = foldersByName.get(name);
    if (f == null) {
      f = getFolder(0, name);
      if (f != null) {
        foldersByName.put(name, f);
      }
    }
    return f;
  }


  public List<Folder> listFolders(long parent) throws Exception {
    LinkedList<Folder> folders = new LinkedList<Folder>();
    Connection conn = null;
    try {
      conn = ds.getConnection();
      PreparedStatement pst;
      if (parent > 0) {
          pst = conn.prepareStatement("select * from folders where parent = ? order by lower(name) asc");
          pst.setLong(1, parent);
        } else {
          pst = conn.prepareStatement("select * from folders where parent is null order by lower(name) asc");
        }
      ResultSet rs = pst.executeQuery();
      while (rs.next()) {
        folders.add(createFolder(rs));
      }
    } finally {
      if (conn!=null) conn.close();
    }
    return folders;
  }

  public Folder getFolder(long parent, String name) throws Exception {
    Connection conn = null;
    Folder f = null;
      try {
        conn = ds.getConnection();
        PreparedStatement pst;
        if (parent > 0) {
          pst = conn.prepareStatement("select * from folders where name = ? and parent = ?");
          pst.setLong(2, parent);
        } else {
          pst = conn.prepareStatement("select * from folders where name = ? and parent is null");
        }
        pst.setString(1, name);
        ResultSet rs = pst.executeQuery();
        if (rs.next()) {
          f = createFolder(rs);
        }
      } finally {
        if (conn != null) conn.close();
      }
    return f;
  }

  private Folder createFolder(ResultSet rs) throws Exception {
    Folder f = new Folder();
    f.setId(rs.getLong("id"));
    f.setName(rs.getString("name"));
    f.setParentId(rs.getLong("parent"));
    return f;
  }

  public Folder getFolder(long id) throws Exception {
    Folder f = null;
    Connection conn = null;
    try {
      conn = ds.getConnection();
      PreparedStatement pst = conn.prepareStatement("select * from folders where id = ?");
      pst.setLong(1, id);
      ResultSet rs = pst.executeQuery();
      if (rs.next()) {
        f = createFolder(rs);
      }
    } finally {
      if (conn != null) {
        conn.close();
      }
    }
    return f;
  }

  public long getFolderCount(long folderId) throws Exception {
    Connection conn = null;
    long cnt = 0;
    try {
      conn = ds.getConnection();
      PreparedStatement pst = conn.prepareStatement("select count(*) as cnt from attachments where folder = ?");
      pst.setLong(1, folderId);
      ResultSet rs = pst.executeQuery();
      if (rs.next()) {
        cnt = rs.getLong("cnt");
      }
    } finally {
      if (conn != null) {
        conn.close();
      }
    }
    return cnt;
  }

  public Folder getParentFolder(long id) throws Exception {
    Folder f = null;
    Connection conn = null;
    try {
      conn = ds.getConnection();
      PreparedStatement pst = conn.prepareStatement("select * from folders where id = (select parent from folders where id = ?)");
      pst.setLong(1, id);
      ResultSet rs = pst.executeQuery();
      if (rs.next()) {
        f = createFolder(rs);
      }
    } finally {
      if (conn != null) {
        conn.close();
      }
    }
    return f;
  }

  public Folder[] getFolderHierarchy(Folder folder) throws Exception {
    LinkedList<Folder> folders = new LinkedList<Folder>();
    while (folder != null) {
      folders.add(folder);
      if (folder.getParentId() > 0) {
        folder = getFolder(folder.getParentId());
      } else {
        break;
      }
    }
    Folder[] f = new Folder[folders.size()];
    for (int i = 0; i < f.length; i++) {
      f[i] = folders.get(folders.size() - i - 1);
    }
    return f;
  }

  public Folder saveFolder(Folder f) throws Exception {
    if (f.getName() == null || f.getName().trim().length() == 0) {
      throw new NullPointerException("name must not be empty");
    }
    String columns[] = {"id", "parent", "name"};
    Connection conn = null;
    try {
      conn = ds.getConnection();
      PreparedStatement pst;
      if (f.getId() == 0) {
        f.setId(getSequence("folderId"));
        pst = conn.prepareStatement(createInsert("folders", columns));
      } else {
        pst = conn.prepareStatement(createUpdate("folders", columns, "id = ?"));
        pst.setLong(columns.length + 1, f.getId());
      }
      int i = 0;
      pst.setLong(++i, f.getId());
      if (f.getParentId() > 0) {
        pst.setLong(++i, f.getParentId());
      } else {
        pst.setNull(++i, Types.BIGINT);
      }
      pst.setString(++i, f.getName());
      pst.executeUpdate();
    } finally {
      if (conn != null) {
        conn.close();
      }
    }
    return f;
  }

  public Attachment saveAttachment(Attachment a) throws Exception {
    return saveAttachment(a, 0);
  }

  public Attachment saveAttachment(Attachment a, long documentVersionId) throws Exception {
    Connection conn = null;
    String columns[] = {"id", "version", "filename", "mimetype", "caption", "uploaded", "updated",
        "userId", "width", "height", "size", "type", "folder", "data", "link", "active"};

    if (a.getFolderId() == 0) {
      Folder f = getRootFolder(a.getType().getFolderName());
      if (f != null) {
        a.setFolderId(f.getId());
      }
    }

    if (a.getType() == null) {
      if (a.getExtension().equalsIgnoreCase("png") || a.getExtension().equalsIgnoreCase("jpg") || a.getExtension().equalsIgnoreCase("jpeg")) {
        a.setType(Attachment.AttachmentType.image);
      } else {
        a.setType(Attachment.AttachmentType.file);
      }
    }

    try {
      conn = ds.getConnection();
      conn.setAutoCommit(false);
      PreparedStatement ps;
      if (a.getId() == 0) {
        a.setId(getSequence("attachmentId"));
        String sql = createInsert("attachments", columns);
        ps = conn.prepareStatement(sql);
      } else {
        String sql = createUpdate("attachments", columns, "id = ?");
        a.setVersion(a.getVersion() + 1);
        a.setUpdated(new Date());
        ps = conn.prepareStatement(sql);
        ps.setLong(columns.length + 1, a.getId());
      }
      int i = 0;
      ps.setLong(++i, a.getId());
      ps.setLong(++i, a.getVersion());
      ps.setString(++i, a.getFullFileName());
      ps.setString(++i, a.getMimeType());
      ps.setString(++i, a.getCaption());
      ps.setTimestamp(++i, new Timestamp(a.getUploaded().getTime()));
      ps.setTimestamp(++i, new Timestamp(a.getUpdated().getTime()));
      ps.setLong(++i, a.getUserId());
      ps.setInt(++i, a.getWidth());
      ps.setInt(++i, a.getHeight());
      ps.setLong(++i, a.getSize());
      ps.setString(++i, a.getType().name());
      if (a.getFolderId() > 0) {
        ps.setLong(++i, a.getFolderId());
      } else {
        ps.setNull(++i, Types.BIGINT);
      }
      ps.setBytes(++i, a.getData());
      ps.setString(++i, a.getLink());
      ps.setBoolean(++i, a.isActive());

      ps.executeUpdate();

      if (documentVersionId > 0) {
        PreparedStatement pst = conn.prepareStatement("select max(seq) as seq from post_attachments where versionId = ?");
        pst.setLong(1, documentVersionId);
        ResultSet rs = pst.executeQuery();
        int next = 0;
        if (rs.next()) {
          next = rs.getInt("seq");
        }
        rs.close();
        pst.close();
        next++;

        pst = conn.prepareStatement("insert into post_attachments (versionId, attachmentId, seq, display) values (?, ?, ?, ?)");
        pst.setLong(1, documentVersionId);
        pst.setLong(2, a.getId());
        pst.setInt(3, next);
        pst.setBoolean(4, true);
        pst.executeUpdate();
        pst.close();
      }

      conn.commit();
    } finally {
      if (conn != null) {
        conn.setAutoCommit(true);
        conn.close();
      }
    }
    return a;
  }

  public List<Attachment> getGalleryImages(int limit, int offset) throws Exception {
    LinkedList<Attachment> al = new LinkedList<Attachment>();
    Connection conn = null;
    try {
      conn = ds.getConnection();
      String sql = "select a.id, version, filename, caption, uploaded, a.updated,\n" +
          "a.memberId, width, height, thumbWidth, thumbHeight, mimetype, size, display\n" +
          "from attachments a join post_attachments pa on attachmentId = a.id\n" +
          "join posts p on pa.postId = p.id " +
          "where p.type = ? and p.status = ? order by uploaded desc";
      if (limit > 0) {
        sql += " limit " + limit + " offset " + offset;
      }
      PreparedStatement ps = conn.prepareStatement(sql);
      ps.setString(1, Post.PostType.gallery.name());
      ps.setString(2, Post.Status.published.name());

      ResultSet rs = ps.executeQuery();
      while (rs.next()) {
        al.add(createAttachment(rs));
      }

    } finally {
      if (conn != null) conn.close();
    }
    return al;
  }

  public List<Attachment> getAttachmentsInFolder(long folder, Attachment.AttachmentType type, int offset, int limit, String order) throws Exception {
    LinkedList<Attachment> attachments = new LinkedList<Attachment>();
    Connection conn = null;
    try {
      conn = ds.getConnection();
      String sql = "select id, version, filename, caption, uploaded, updated,\n" +
          "userId, width, height, mimetype, size, type, folder, link, active\n" +
          "from attachments where folder = ?";
      if (type != null) {
        sql += " and type = '" + type.name() + "'";
      }

      if (order != null && order.trim().length() > 0) {
        sql += " order by " + order;
      } else {
        sql += " order by lower(filename) asc";
      }

      if (limit > 0) {
        sql += " offset " + offset + " limit " + limit;
      }
      PreparedStatement pst = conn.prepareStatement(sql);
      pst.setLong(1, folder);
      ResultSet rs = pst.executeQuery();
      while (rs.next()) {
        attachments.add(createAttachment(rs));
      }
    } finally {
      if (conn != null) conn.close();
    }
    return attachments;
  }

  public List<Attachment> getAttachmentsForPost(long versionId, Attachment.AttachmentType type) throws Exception {
    return getAttachmentsForPost(versionId, type, 0, 0);
  }

  public List<Attachment> getAttachmentsForPost(long versionId, Attachment.AttachmentType type, int offset, int limit) throws Exception {
    return getAttachmentsForPost(versionId, type, offset, limit, false);
  }

  public List<Attachment> getAttachmentsForPost(long versionId, Attachment.AttachmentType type, int offset, int limit, boolean visibleOnly) throws Exception {
    LinkedList<Attachment> al = new LinkedList<Attachment>();
    Connection conn = null;
    try {
      conn = ds.getConnection();
      String sql = "select id, version, filename, caption, uploaded, updated,\n" +
          "userId, width, height, mimetype, size, type, folder, display, link, active\n" +
          "from attachments join post_attachments on attachmentId = id\n" +
          "where versionId = ? and type = ?";

      if (visibleOnly) {
        sql += " and display = 't'";
      }

      sql += " order by seq";

      if (limit > 0) {
        sql += " offset " + offset + " limit " + limit;
      }
      PreparedStatement ps = conn.prepareStatement(sql);
      ps.setLong(1, versionId);
      ps.setString(2, type.name());

      ResultSet rs = ps.executeQuery();
      while (rs.next()) {
        al.add(createAttachment(rs));
      }

    } finally {
      if (conn != null) conn.close();
    }
    return al;
  }

  public List<Attachment> getAttachmentsForMember(long memberId) throws Exception {
    LinkedList<Attachment> al = new LinkedList<Attachment>();
    Connection conn = null;
    try {
      conn = ds.getConnection();
      PreparedStatement ps = conn.prepareStatement("select id, version, filename, caption, uploaded, updated,\n" +
          "memberId, width, height, thumbWidth, thumbHeight, mimetype, size, folder, link, active\n" +
          "from attachments where memberId = ? order by uploaded desc");
      ps.setLong(1, memberId);

      ResultSet rs = ps.executeQuery();
      while (rs.next()) {
        al.add(createAttachment(rs));
      }

    } finally {
      if (conn != null) conn.close();
    }
    return al;
  }

  public Attachment getAttachment(long id, boolean data) throws Exception {
    Attachment a = null;
    Connection conn = null;
    try {
      conn = ds.getConnection();

      StringBuilder sb = new StringBuilder();
      sb.append("select id, version, filename, caption, uploaded, updated,");
      sb.append("userId, width, height, mimetype, size, type, folder, link, active");
      if (data) {
        sb.append(", data");
      }
      sb.append(" from attachments where id = ?");

      PreparedStatement ps = conn.prepareStatement(sb.toString());
      ps.setLong(1, id);

      ResultSet rs = ps.executeQuery();
      if (rs.next()) {
        a = createAttachment(rs);
      }

    } finally {
      if (conn != null) conn.close();
    }
    return a;
  }

  private Attachment createAttachment(ResultSet rs) throws Exception {
    Attachment a = new Attachment();
    a.setId(rs.getLong("id"));
    a.setVersion(rs.getLong("version"));
    a.setFilename(rs.getString("filename"));
    a.setCaption(rs.getString("caption"));
    a.setUploaded(rs.getTimestamp("uploaded"));
    a.setUpdated(rs.getTimestamp("updated"));
    a.setUserId(rs.getLong("userId"));
    a.setHeight(rs.getInt("height"));
    a.setWidth(rs.getInt("width"));
    a.setMimeType(rs.getString("mimetype"));
    a.setSize(rs.getLong("size"));
    a.setType(Attachment.AttachmentType.valueOf(rs.getString("type")));
    a.setFolderId(rs.getLong("folder"));
    a.setLink(rs.getString("link"));
    a.setActive(rs.getBoolean("active"));

    boolean data;
    try {
      data = rs.findColumn("data") > 0;
    } catch (SQLException e) {
      data = false;
    }

    if (data) {
      a.setData(rs.getBytes("data"));
    }

    boolean display;
    try {
      display = rs.findColumn("display") > 0;
    } catch (SQLException e) {
      display = false;
    }

    if (display) {
      a.setDisplay(rs.getBoolean("display"));
    }

    return a;
  }


  public void addAttachmentToPost(long versionId, long attachmentId) throws Exception {
    Connection conn = null;
    try {
      conn = ds.getConnection();
      PreparedStatement pst = conn.prepareStatement("select max(seq) as seq from post_attachments where versionId = ?");
      pst.setLong(1, versionId);
      ResultSet rs = pst.executeQuery();
      int next = 0;
      if (rs.next()) {
        next = rs.getInt("seq");
      }
      rs.close();
      pst.close();
      next++;

      pst = conn.prepareStatement("insert into post_attachments (versionId, attachmentId, seq, display) values (?, ?, ?, ?)");
      pst.setLong(1, versionId);
      pst.setLong(2, attachmentId);
      pst.setInt(3, next);
      pst.setBoolean(4, true);
      pst.executeUpdate();

    } finally {
      if (conn != null) {
        conn.close();
      }
    }
  }

  public void removeAttachmentFromPost(long versionId, long attachmentId) throws Exception {
    Connection conn = null;

    try {
      conn = ds.getConnection();
      PreparedStatement pst = conn.prepareStatement("select remove_attachment_from_post(?, ?)");
      pst.setLong(1, versionId);
      pst.setLong(2, attachmentId);
      pst.execute();
    } finally {
      if (conn != null) {
        conn.close();
      }
    }
  }

  public void setVisible(long versionId, long attachmentId, boolean visible) throws Exception {
    Connection conn = null;
    try {
      conn = ds.getConnection();
      PreparedStatement pst = conn.prepareStatement("update post_attachments set display = ? where versionId = ? and attachmentId = ?");
      pst.setBoolean(1, visible);
      pst.setLong(2, versionId);
      pst.setLong(3, attachmentId);
      pst.executeUpdate();
    } finally {
      if (conn != null) {
        conn.close();
      }
    }
  }

  public void swapOrder(long versionId, long a, long b) throws Exception {
    Connection conn = null;
    try {
      conn = ds.getConnection();
      PreparedStatement pst = conn.prepareStatement("select swap_attachment_order(?, ?, ?)");
      pst.setLong(1, versionId);
      pst.setLong(2, a);
      pst.setLong(3, b);
      pst.execute();
    } finally {
      if (conn != null) {
        conn.close();
      }
    }
  }

  public void delete(long id) throws Exception {
    Connection conn = null;
    try {
      conn = ds.getConnection();

      conn.setAutoCommit(false);
      PreparedStatement pst = conn.prepareStatement("delete from post_attachments where attachmentId = ?");
      pst.setLong(1, id);
      pst.executeUpdate();

      pst = conn.prepareStatement("delete from attachments where id = ?");
      pst.setLong(1, id);
      pst.executeUpdate();
      conn.commit();
      conn.setAutoCommit(true);

    } finally {
      if (conn != null) {
        conn.close();
      }
    }
  }
 
}
