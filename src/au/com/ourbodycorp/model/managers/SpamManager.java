package au.com.ourbodycorp.model.managers;

import au.com.ourbodycorp.Config;
import au.com.ourbodycorp.MailMessage;
import au.com.ourbodycorp.Util;
import au.com.ourbodycorp.model.Spam;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIUtils;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.net.URI;
import java.sql.*;
import java.util.*;
import java.util.Date;

public class SpamManager extends ManagerBase {

  private String[] columns = {"id", "email", "title", "fname", "lname", "lowerName", "address1", "address2", "suburb", "state", "country", "postcode", "phone",
                              "subscribed", "updated", "optinEmail", "optinPost", "confirm"};

  private Spam createSpam(ResultSet rs) throws Exception {
    Spam spam = new Spam();

    spam.setId(rs.getLong("id"));
    spam.setEmail(rs.getString("email"));
    spam.setFirstName(rs.getString("fname"));
    spam.setLastName(rs.getString("lname"));
    spam.setAddress1(rs.getString("address1"));
    spam.setAddress2(rs.getString("address2"));
    spam.setSuburb(rs.getString("suburb"));
    spam.setState(rs.getString("state"));
    spam.setCountry(rs.getString("country"));
    spam.setState(rs.getString("postcode"));
    spam.setPhone(rs.getString("phone"));
    spam.setSubscribed(rs.getDate("subscribed"));
    spam.setUpdated(rs.getDate("updated"));
    spam.setOptInEmail(rs.getBoolean("optinEmail"));
    spam.setOptInPost(rs.getBoolean("optinPost"));
    spam.setConfirm(rs.getLong("confirm"));

    return spam;
  }

  public Spam save(Spam spam) throws Exception {
    Connection conn = null;
    try {
      conn = ds.getConnection();
      PreparedStatement pst;
      if (spam.getId() == 0) {
        spam.setId(getSequence("spamId"));
        spam.setSubscribed(new Date());
        pst = conn.prepareStatement(createInsert("spam", columns));
      } else {
        spam.setUpdated(new Date());
        pst = conn.prepareStatement(createUpdate("spam", columns, "id = ?"));
        pst.setLong(columns.length + 1, spam.getId());
      }

      if (spam.getConfirm() == 0) {
        spam.setConfirm((long) (Math.random() * 10000.0));
      }

      int i = 0;
      pst.setLong(++i, spam.getId());
      pst.setString(++i, (emptyToNull(spam.getEmail()) != null) ? spam.getEmail().toLowerCase() : null);
      pst.setString(++i, emptyToNull(spam.getTitle()));
      pst.setString(++i, emptyToNull(spam.getFirstName()));
      pst.setString(++i, emptyToNull(spam.getLastName()));
      pst.setString(++i, (emptyToNull(spam.getFirstName()) != null) ? spam.getFirstName().toLowerCase() : null);
      pst.setString(++i, emptyToNull(spam.getAddress1()));
      pst.setString(++i, emptyToNull(spam.getAddress2()));
      pst.setString(++i, emptyToNull(spam.getSuburb()));
      pst.setString(++i, emptyToNull(spam.getState()));
      pst.setString(++i, emptyToNull(spam.getCountry()));
      pst.setString(++i, emptyToNull(spam.getPostcode()));
      pst.setString(++i, emptyToNull(spam.getPhone()));
      if (spam.getSubscribed() != null) {
        pst.setTimestamp(++i, new Timestamp(spam.getSubscribed().getTime()));
      } else {
        pst.setNull(++i, Types.TIMESTAMP);
      }
      if (spam.getUpdated() != null) {
        pst.setTimestamp(++i, new Timestamp(spam.getUpdated().getTime()));
      } else {
        pst.setNull(++i, Types.TIMESTAMP);
      }
      pst.setBoolean(++i, spam.isOptInEmail());
      pst.setBoolean(++i, spam.isOptInPost());
      pst.setLong(++i, spam.getConfirm());

      pst.executeUpdate();

    } finally {
      if (conn != null) {
        conn.close();
      }
    }
    return spam;
  }

  public List<Spam> getByEmail(String email) throws Exception {
    List<Spam> spams = new LinkedList<Spam>();
    Connection conn = null;
    try {
      conn = ds.getConnection();
      PreparedStatement pst = conn.prepareStatement("select * from spam where email = ?");
      pst.setString(1, email.trim().toLowerCase());
      ResultSet rs = pst.executeQuery();
      while (rs.next()) {
        spams.add(createSpam(rs));
      }
    } finally {
      if (conn != null) {
        conn.close();
      }
    }
    return spams;
  }

  public Spam getById(long id) throws Exception {
    Connection conn = null;
    Spam spam = null;
    try {
      conn = ds.getConnection();
      PreparedStatement pst = conn.prepareStatement("select * from spam where id = ?");
      pst.setLong(1, id);
      ResultSet rs = pst.executeQuery();
      if (rs.next()) {
        spam = createSpam(rs);
      }
    } finally {
      if (conn != null) {
        conn.close();
      }
    }
    return spam;
  }

  public void submitSpam(Spam spam) throws Exception {
    boolean doEmail = false;
      try {
      List<NameValuePair> formparams = new ArrayList<NameValuePair>();
      formparams.add(new BasicNameValuePair("CreateDocument", ""));
      formparams.add(new BasicNameValuePair("Title", Util.notNull(spam.getTitle())));
      formparams.add(new BasicNameValuePair("FirstName", Util.notNull(spam.getFirstName())));
      formparams.add(new BasicNameValuePair("LastName", Util.notNull(spam.getLastName())));
      formparams.add(new BasicNameValuePair("MailAddress", Util.notNull(spam.getEmail())));
      formparams.add(new BasicNameValuePair("StreetAddress1", Util.notNull(spam.getAddress1())));
      formparams.add(new BasicNameValuePair("StreetAddress2", Util.notNull(spam.getAddress2())));
      formparams.add(new BasicNameValuePair("City", Util.notNull(spam.getSuburb())));
      formparams.add(new BasicNameValuePair("State", Util.notNull(spam.getState())));
      formparams.add(new BasicNameValuePair("Country", Util.notNull(spam.getCountry())));
      formparams.add(new BasicNameValuePair("Zip", Util.notNull(spam.getPostcode())));

      DefaultHttpClient httpclient = new DefaultHttpClient();

      URI uri = URIUtils.createURI("http", "domino.ourbodycorp.com.au", 80, "/applications/submit.nsf/fmprogram", URLEncodedUtils.format(formparams, "UTF-8"), null);
      HttpGet get = new HttpGet(uri);
      HttpResponse re = httpclient.execute(get);
      doEmail = (re.getStatusLine().getStatusCode() != 200);
      } catch (Exception e) {
          e.printStackTrace();
          doEmail = true;
      }

      if (doEmail) {
          String recipient = Config.getString("email.subscriptions");
          if (recipient != null && recipient.trim().length() > 0) {
              Properties props = new Properties();
              props.setProperty("type", Util.notNull("New"));
              props.setProperty("name", Util.notNull(spam.getFirstName()));
              props.setProperty("email", Util.notNull(spam.getEmail()));
              props.setProperty("address1", Util.notNull(spam.getAddress1()));
              props.setProperty("address2", Util.notNull(spam.getAddress2()));
              props.setProperty("suburb", Util.notNull(spam.getSuburb()));
              props.setProperty("state", Util.notNull(spam.getState()));
              props.setProperty("postcode", Util.notNull(spam.getPostcode()));
              props.setProperty("country", Util.notNull(spam.getCountry()));
              props.setProperty("optinEmail", "" + spam.isOptInEmail());
              props.setProperty("optinPost", "" + spam.isOptInPost());

              MailMessage msg = new MailMessage(props, "program.vm", recipient, "Failed new subscribe request",true);
              msg.send();
          }
      }
  }

}
