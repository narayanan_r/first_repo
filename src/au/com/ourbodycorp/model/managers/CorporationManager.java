package au.com.ourbodycorp.model.managers;

import au.com.ourbodycorp.Config;
import au.com.ourbodycorp.MailMessage;
import au.com.ourbodycorp.model.*;

import java.sql.*;
import java.util.*;
import java.util.Date;

import javax.swing.text.html.HTMLDocument.HTMLReader.PreAction;

public class CorporationManager extends ManagerBase {

    private static final long MILLIS_IN_DAY = 60*60*24*1000;
    
    private String corpSql = "select a.*, b.printable_name as countryName from corporation a join country b on a.country = b.iso";
    private String corpUserSql = "select cu.*, cr.*, u.title as utitle, u.id as UserId, firstName, lastname, email, phone, mobile, address1, address2, suburb, state, postcode from corp_users cu join corp_roles cr on role = id join users u on cu.userId = u.id";
    private String unitWithOwnerSql = "select a.*, b.type, since, until, current, c.id as UserId, firstName, lastname, email, phone, mobile, preferredaddress, printable_name as countryName\n"
            + "from units a left join unit_users b on a.id = b.unitId\n"
            + "join users c on b.userId = c.id "
            + "join country d on c.country = d.iso";
    private String inviteSql = "select i.*, firstName, lastName, c.name as corpName from invitations i join users u on inviter = u.id join corporation c on corpId = c.id";
    private String unitSql = "select u.*, c.name as corpName, slug, printable_name as countryName, us.id as userId, firstName, lastName,"
            + "email, uu.type as uutype, since, until, current, preferredaddress\n"
            + "from Units u join Corporation c on u.corpId = c.id join country ct on u.country = ct.iso\n"
            + "left join unit_users uu on uu.unitId = u.id left join users us on us.id = uu.userId";
    private String unitSqlNoOwner = "select u.*, c.name as corpName, slug, printable_name as countryName "
            + "from Units u join Corporation c on u.corpId = c.id join country ct on u.country = ct.iso";

    public Corporation createCorporation(ResultSet rs) throws Exception {
        Corporation c = new Corporation();

        c.setId(rs.getLong("id"));
        if (rs.getString("type") != null) {
            c.setType(Corporation.Type.valueOf(rs.getString("type")));
        }
        c.setRegNum(rs.getString("regnum"));
        c.setName(rs.getString("name"));
        c.setSlug(rs.getString("slug"));
        c.setAddress1(rs.getString("address1"));
        c.setAddress2(rs.getString("address2"));
        c.setState(rs.getString("state"));
        c.setPostcode(rs.getString("postCode"));
        c.setCountry(rs.getString("country"));
        c.setSuburb(rs.getString("suburb"));
        c.setCountryName(rs.getString("countryName"));
        c.setCreated(rs.getTimestamp("created"));
        c.setStartOfFinancialYear(rs.getInt("sofy"));
        c.setFinancialYear(rs.getInt("fy"));
        c.setTimeZone(TimeZone.getTimeZone(rs.getString("timezone")));
        c.setLocked(rs.getString("locked"));
        c.setLogoId(rs.getLong("logo"));
        c.setCompanyId(rs.getLong("company"));
        c.setMaxPatients(rs.getInt("max_patients"));
        c.setMaxPractitioners(rs.getInt("max_practitioners"));
        c.setSubType(rs.getString("sub_type"));
        c.setExpiry(rs.getTimestamp("expiry"));
        c.setSmsCount(rs.getInt("sms_count"));
        c.setCliniko_apikey(rs.getString("cliniko_apikey"));
        c.setSession_timeout(rs.getLong("session_timeout"));

        return c;
    }

    public CorporationRole createCorporationRole(ResultSet rs) throws Exception {
        CorporationRole role = new CorporationRole();

        role.setId(rs.getString("id"));
        role.setName(rs.getString("name"));

        return role;
    }

    public CorporationUser createCorporationUser(ResultSet rs, User user, Corporation corp) throws Exception {
        CorporationUser u = new CorporationUser();

        if (user == null || user.getId() != rs.getLong("userId")) {
            user = new User();
            user.setId(rs.getLong("userId"));
            user.setTitle(rs.getString("utitle"));
            user.setFirstName(rs.getString("firstname"));
            user.setLastName(rs.getString("lastname"));
            user.setEmail(rs.getString("email"));
            user.setPhone(rs.getString("phone"));
            user.setMobile(rs.getString("mobile"));
            user.setAddress1(rs.getString("address1"));
            user.setAddress2(rs.getString("address2"));
            user.setSuburb(rs.getString("suburb"));
            user.setState(rs.getString("state"));
            user.setPostcode(rs.getString("postcode"));
        }

        if (corp == null || corp.getId() != rs.getLong("corpId")) {
            corp = new Corporation();
            corp.setId(rs.getLong("corpId"));
        }

        u.setUser(user);
        u.setCorporation(corp);
        u.setRole(createCorporationRole(rs));
        u.setRegDate(rs.getTimestamp("regdate"));
        u.setInviterId(rs.getLong("inviter"));

        return u;
    }

    public Unit createUnit(ResultSet rs, Corporation corp) throws Exception {
        Unit unit = new Unit();

        if (corp == null || corp.getId() != rs.getLong("corpId")) {
            corp = new Corporation();
            corp.setId(rs.getLong("corpId"));
        }

        unit.setId(rs.getLong("id"));
        unit.setCorporation(corp);
        unit.setName(rs.getString("name"));
        unit.setAddress1(rs.getString("address1"));
        unit.setAddress2(rs.getString("address2"));
        unit.setState(rs.getString("state"));
        unit.setPostcode(rs.getString("postCode"));
        unit.setCountry(rs.getString("country"));
        unit.setCountryName(rs.getString("countryName"));
        unit.setNotes(rs.getString("notes"));
        unit.setUev(rs.getInt("uev"));

        return unit;
    }

    public UnitUser createUnitUser(ResultSet rs, Corporation corp) throws Exception {
        UnitUser u = new UnitUser();

        User user = new User();
        user.setId(rs.getLong("userId"));
        user.setFirstName(rs.getString("firstname"));
        user.setLastName(rs.getString("lastname"));
        user.setEmail(rs.getString("email"));
        user.setPhone(rs.getString("phone"));
        user.setMobile(rs.getString("mobile"));

        u.setUnit(createUnit(rs, corp));
        u.setUser(user);
        u.setCurrent(rs.getBoolean("current"));
        u.setPreferredAddress(rs.getBoolean("preferredaddress"));
        u.setSince(rs.getTimestamp("since"));
        u.setUntil(rs.getTimestamp("until"));
        u.setType(UnitUser.Type.valueOf(rs.getString("type")));

        return u;
    }

    public Unit getUnit(long unitId) throws Exception {
        Unit unit = null;
        Connection conn = null;
        try {
            conn = ds.getConnection();
            PreparedStatement pst = conn.prepareStatement(unitSqlNoOwner + " where u.id = ?");
            pst.setLong(1, unitId);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                unit = createUnit(rs);
            }
            rs.close();
            pst.close();
            if (unit != null) {
                unit.setOwners(new LinkedList<UnitUser>());
                pst = conn.prepareStatement("select * from users where id in (select userId from unit_users where type = ? and unitId = ?)");
                pst.setString(1, UnitUser.Type.owner.name());
                pst.setLong(2, unitId);
                rs = pst.executeQuery();
                UserManager um = new UserManager();
                while (rs.next()) {
                    User u = um.createUser(rs);
                    UnitUser uu = new UnitUser();
                    uu.setUser(u);
                    uu.setType(UnitUser.Type.owner);
                    unit.getOwners().add(uu);
                }
            }
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return unit;
    }

    public List<UnitUser> getUnitsForUser(long userId, long corpId) throws Exception {
        LinkedList<UnitUser> uu = new LinkedList<UnitUser>();
        Connection conn = null;
        try {
            conn = ds.getConnection();
            PreparedStatement pst = conn.prepareStatement(unitWithOwnerSql + " where c.id = ? and corpId = ?");
            pst.setLong(1, userId);
            pst.setLong(2, corpId);
            ResultSet rs = pst.executeQuery();
            Corporation corp = new Corporation();
            corp.setId(corpId);
            while (rs.next()) {
                uu.add(createUnitUser(rs, corp));
            }
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return uu;
    }

    public Corporation save(Corporation c, long userId) throws Exception {
        String columns[] = {"id", "type", "regnum", "name", "slug", "address1", "address2", "suburb", "state", "postcode", "timezone", "country", "sofy", "fy", "logo", "company", "max_patients", "max_practitioners", "locked", "sub_type", "expiry", "sms_count","mainlocation"};
        Connection conn = null;
        Connection conn2 = null;
        boolean isNew = false;
        try {
            conn = ds.getConnection();
            conn.setAutoCommit(false);
            PreparedStatement pst;
            if (c.getId() == 0) {
                c.setId(getSequence("corpId"));
                pst = conn.prepareStatement(createInsert("corporation", columns));
                isNew = true;
            }
            else {
                pst = conn.prepareStatement(createUpdate("corporation", columns, "id = ?"));
                pst.setLong(columns.length + 1, c.getId());
            }
            int i = 0;
            pst.setLong(++i, c.getId());
            if (c.getType() != null) {
                pst.setString(++i, c.getType().name());
            }
            else {
                pst.setNull(++i, Types.VARCHAR);
            }
            pst.setString(++i, c.getRegNum());
            pst.setString(++i, c.getName());
            pst.setString(++i, c.getSlug());
            pst.setString(++i, c.getAddress1());
            pst.setString(++i, c.getAddress2());
            pst.setString(++i, c.getSuburb());
            pst.setString(++i, c.getState());
            pst.setString(++i, c.getPostcode());
            pst.setString(++i, c.getTimeZone().getID());
            pst.setString(++i, c.getCountry());
            if (c.getStartOfFinancialYear() > 0) {
                pst.setInt(++i, c.getStartOfFinancialYear());
            }
            else {
                pst.setNull(++i, Types.INTEGER);
            }
            if (c.getFinancialYear() > 0) {
                pst.setInt(++i, c.getFinancialYear());
            }
            else {
                pst.setNull(++i, Types.INTEGER);
            }

            if (c.getLogoId() > 0) {
                pst.setLong(++i, c.getLogoId());
            }
            else {
                pst.setNull(++i, Types.BIGINT);
            }

            if (c.getCompanyId() > 0) {
                pst.setLong(++i, c.getCompanyId());
            }
            else {
                pst.setNull(++i, Types.BIGINT);
            }

            pst.setInt(++i, c.getMaxPatients());
            pst.setInt(++i, c.getMaxPractitioners());
            pst.setString(++i, c.getLocked());
            pst.setString(++i, c.getSubType());
            if (c.getExpiry() != null) {
                pst.setTimestamp(++i, new Timestamp(c.getExpiry().getTime()));
            }
            else {
                pst.setNull(++i, Types.TIMESTAMP);
            }
            pst.setInt(++i, (isNew)? 20 : c.getSmsCount());
            if(c.getCompanyId()>0)
            {
            	conn2=ds.getConnection();
            	PreparedStatement pst2=conn2.prepareStatement("update company set name=? where id=?");
            	pst2.setString(1, c.getName());
            	pst2.setLong(2, c.getCompanyId());
            	pst2.executeUpdate();
            	pst2.close();
            	
            }
            pst.setBoolean(++i, c.isMainLocation());
            pst.executeUpdate();
            pst.close();
           
            

            if (isNew && userId > 0) {
                pst = conn.prepareStatement("insert into corp_users (userId, corpId, role) values (?, ?, ?)");
                pst.setLong(1, userId);
                pst.setLong(2, c.getId());
                pst.setString(3, "AD");
                pst.executeUpdate();
                pst.close();

                if (c.getCompanyId() > 0) {
                    pst = conn.prepareStatement("insert into company_owners (company, user_id) values (?, ?)");
                    pst.setLong(1, c.getId());
                    pst.setLong(2, userId);
                }
            }
          
            conn.commit();
        }
        finally {
            if (conn != null) {
                conn.setAutoCommit(true);
                conn.close();
            }
        }
        return c;
    }

    public List<Corporation> getCorporations(long userId) throws Exception {
        Connection conn = null;
        LinkedList<Corporation> corps = new LinkedList<Corporation>();
        try {
            conn = ds.getConnection();
            PreparedStatement pst = conn.prepareStatement(corpSql + " where id in (select corpId from corp_users where userId = ?)");
            pst.setLong(1, userId);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                corps.add(createCorporation(rs));
            }
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }

        return corps;
    }
    
    public List<Corporation> getExerciseid(long companyid, String name, long uid, long id) throws Exception {
        Connection conn = null;
        LinkedList<Corporation> Exerceid = new LinkedList<Corporation>();
        try {
            conn = ds.getConnection();
            PreparedStatement pst = conn.prepareStatement("select id from exercises where name = ? and corp_id= ? and (shareid = ? or id = ?)");
            pst.setString(1, name);
            pst.setLong(2, companyid);
            pst.setLong(3, uid);
            pst.setLong(4, id);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
            	Corporation em = new Corporation();
                em = new Corporation();
                em.setId(rs.getLong("id"));
                Exerceid.add(em);
            }
        }catch(Exception e){
        	e.printStackTrace();
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }

        return Exerceid;
    }
    
    public List<Corporation> getExerciseidforPrograms(long companyid, String name, long uid, long id) throws Exception {
        Connection conn = null;
        LinkedList<Corporation> Exerceid = new LinkedList<Corporation>();
        try {
            conn = ds.getConnection();
            PreparedStatement pst = conn.prepareStatement("select id,code from programs where name = ? and corp_id= ? and (shareid = ? or id = ?) and patient is null and group_id is null");
            pst.setString(1, name);
            pst.setLong(2, companyid);
            pst.setLong(3, uid);
            pst.setLong(4, id);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
            	Corporation em = new Corporation();
                em = new Corporation();
                em.setId(rs.getLong("id"));
                em.setName(rs.getString("code"));
                Exerceid.add(em);
            }
        }catch(Exception e){
        	e.printStackTrace();
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }

        return Exerceid;
    }
    
    public List<Corporation> getExerciseidforPatientPrograms(long companyid, String name, long uid, long id) throws Exception {
        Connection conn = null;
        LinkedList<Corporation> Exerceid = new LinkedList<Corporation>();
        try {
            conn = ds.getConnection();
            PreparedStatement pst = conn.prepareStatement("select id,code from programs where name = ? and corp_id= ? and (shareid = ? or id = ?) and patient is NULL");
            pst.setString(1, name);
            pst.setLong(2, companyid);
            pst.setLong(3, uid);
            pst.setLong(4, id);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
            	Corporation em = new Corporation();
                em = new Corporation();
                em.setId(rs.getLong("id"));
                em.setName(rs.getString("code"));
                Exerceid.add(em);
            }
        }catch(Exception e){
        	e.printStackTrace();
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }

        return Exerceid;
    }
    
    public List<Corporation> getExerciseidforGroupPrograms(long companyid, String name, long uid, long id) throws Exception {
        Connection conn = null;
        LinkedList<Corporation> Exerceid = new LinkedList<Corporation>();
        try {
            conn = ds.getConnection();
            PreparedStatement pst = conn.prepareStatement("select id,code from programs where name = ? and corp_id= ? and (shareid = ? or id = ?) and group_id is NULL");
            pst.setString(1, name);
            pst.setLong(2, companyid);
            pst.setLong(3, uid);
            pst.setLong(4, id);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
            	Corporation em = new Corporation();
                em = new Corporation();
                em.setId(rs.getLong("id"));
                em.setName(rs.getString("code"));
                Exerceid.add(em);
            }
        }catch(Exception e){
        	e.printStackTrace();
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }

        return Exerceid;
    }
    
    public List<Corporation> getExerciseidforPatientorGroupPrograms(long companyid, String name, long uid, long id) throws Exception {
        Connection conn = null;
        LinkedList<Corporation> Exerceid = new LinkedList<Corporation>();
        try {
            conn = ds.getConnection();
            PreparedStatement pst = conn.prepareStatement("select p.id,p.code,p.corp_id,p.patient,p.group_id from programs p join corporation c on c.id =  p.corp_id where p.name = ? and c.company= ? and (p.shareid = ? or p.id = ?) and (p.patient is not NULL or p.group_id is not null)");
            pst.setString(1, name);
            pst.setLong(2, companyid);
            pst.setLong(3, uid);
            pst.setLong(4, id);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
            	Corporation em = new Corporation();
                em = new Corporation();
                em.setId(rs.getLong("id"));
                em.setName(rs.getString("code"));
                em.setCompanyId(rs.getLong("corp_id"));
                em.setMaxPatients((int) rs.getLong("patient"));
                em.setMaxPractitioners((int) rs.getLong("group_id"));
                Exerceid.add(em);
            }
        }catch(Exception e){
        	e.printStackTrace();
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }

        return Exerceid;
    }
    
    public List<Corporation> getExerciseidforProgramUnique(long id) throws Exception {
        Connection conn = null;
        LinkedList<Corporation> Exerceid = new LinkedList<Corporation>();
        try {
            conn = ds.getConnection();
            PreparedStatement pst = conn.prepareStatement("select id,code from programs where id = ?");
            pst.setLong(1, id);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
            	Corporation em = new Corporation();
                em = new Corporation();
                em.setId(rs.getLong("id"));
                em.setName(rs.getString("code"));
                Exerceid.add(em);
            }
        }catch(Exception e){
        	e.printStackTrace();
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }

        return Exerceid;
    }
    
    public List<Corporation> getExerciseidforArticles(long companyid, String name, long uid, long id) throws Exception {
        Connection conn = null;
        LinkedList<Corporation> Exerceid = new LinkedList<Corporation>();
        try {
            conn = ds.getConnection();
            PreparedStatement pst = conn.prepareStatement("select id from info where headline = ? and corp_id = ? and (shareid = ? or id = ?)");
            pst.setString(1, name);
            pst.setLong(2, companyid);
            pst.setLong(3, uid);
            pst.setLong(4, id);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
            	Corporation em = new Corporation();
                em = new Corporation();
                em.setId(rs.getLong("id"));
                Exerceid.add(em);
            }
        }catch(Exception e){
        	e.printStackTrace();
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }

        return Exerceid;
    }
    
    public List<Corporation> getExerciseidforNews(long companyid, String name,long uid, long id) throws Exception {
        Connection conn = null;
        LinkedList<Corporation> Exerceid = new LinkedList<Corporation>();
        try {
            conn = ds.getConnection();
            PreparedStatement pst = conn.prepareStatement("select id from mini_news where headline = ? and corp_id = ? and (shareid = ? or id = ?)");
            pst.setString(1, name);
            pst.setLong(2, companyid);
            pst.setLong(3, uid);
            pst.setLong(4, id);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
            	Corporation em = new Corporation();
                em = new Corporation();
                em.setId(rs.getLong("id"));
                Exerceid.add(em);
            }
        }catch(Exception e){
        	e.printStackTrace();
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }

        return Exerceid;
    }

    public List<Corporation> getAllCorporations() throws Exception {
        Connection conn = null;
        LinkedList<Corporation> corps = new LinkedList<Corporation>();
        try {
            conn = ds.getConnection();
            PreparedStatement pst = conn.prepareStatement(corpSql);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                corps.add(createCorporation(rs));
            }
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }

        return corps;
    }
    
    public List<Corporation> getAllActiveTrialCorporations() throws Exception {
        Connection conn = null;
        LinkedList<Corporation> corps = new LinkedList<Corporation>();
        try {
            conn = ds.getConnection();
            PreparedStatement pst = conn.prepareStatement(corpSql + " where created >= CURRENT_TIMESTAMP - interval '10 day' order by created desc");
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                corps.add(createCorporation(rs));
            }
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }

        return corps;
    }
    
    public List<Corporation> getCorporationsOfType(String type) throws Exception {
        Connection conn = null;
        LinkedList<Corporation> corps = new LinkedList<Corporation>();
        try {
            conn = ds.getConnection();
            PreparedStatement pst = conn.prepareStatement(corpSql + " where sub_type = ?");
            pst.setString(1, type);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                corps.add(createCorporation(rs));
            }
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }

        return corps;
    }

    public List<Corporation> getCorporationsForCompany(long companyId) throws Exception {
        Connection conn = null;
        LinkedList<Corporation> corps = new LinkedList<Corporation>();
        try {
            conn = ds.getConnection();
            PreparedStatement pst = conn.prepareStatement(corpSql + " where id in (select corpId from corp_users where company = ?)");
            pst.setLong(1, companyId);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                corps.add(createCorporation(rs));
            }
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }

        return corps;
    }

    public List<Corporation> getExpiryCorporations(int days) throws Exception {
        Connection conn = null;
        LinkedList<Corporation> corps = new LinkedList<Corporation>();
        try {
            conn = ds.getConnection();
            PreparedStatement pst = conn.prepareStatement(corpSql + " where sub_type = 'trial' and expiry > CURRENT_TIMESTAMP and expiry <= CURRENT_TIMESTAMP + interval '" + days + " day'");
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                corps.add(createCorporation(rs));
            }
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }

        return corps;
    }

    public List<Corporation> getExpiredCorporations() throws Exception {
        Connection conn = null;
        LinkedList<Corporation> corps = new LinkedList<Corporation>();
        try {
            conn = ds.getConnection();
            PreparedStatement pst = conn.prepareStatement(corpSql + " where sub_type = 'trial' and created < CURRENT_TIMESTAMP - interval '10 day' order by created desc");
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                corps.add(createCorporation(rs));
            }
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }

        return corps;
    }
    
    public boolean hasTrialExpired(long corporationId) throws Exception {
        Corporation corp = getCorporation(corporationId);
        if (corp == null) return false;
        
        //only trials can expire
        if (corp.getSubType() == null || !corp.getSubType().equalsIgnoreCase("trial")) return false;
        
        Date createdAtDate = corp.getCreated();
        if (createdAtDate == null) return false;
        
        //it's still active if the created date + 11 days is greater than the current time
        if ((createdAtDate.getTime() + (1000*60*60*24*11)) > System.currentTimeMillis()) return false;
        
        
        return true;
    }
    
    public int trialDaysLeft(long corporationId) throws Exception {
        Corporation corp = getCorporation(corporationId);
        if (corp == null) return -1;
        
        //only trials can expire
        if (corp.getSubType() == null || !corp.getSubType().equalsIgnoreCase("trial")) return -1;
        
        Date createdAtDate = corp.getCreated();
        if (createdAtDate == null) return -1;
        
        //it's still active if the created date + 11 days is greater than the current time
        int daysIntoTrial = Math.round(((System.currentTimeMillis() - createdAtDate.getTime()) / MILLIS_IN_DAY));
        
        return 30 - daysIntoTrial;
    }
    
    public boolean isExpired(long corporationId) throws Exception {
        Corporation corp = getCorporation(corporationId);
        if (corp == null) return false;
        
        //only trials can expire
        if (corp.getSubType() == null || !corp.getSubType().equalsIgnoreCase("trial")) return false;
        
        Date createdAtDate = corp.getCreated();
        if (createdAtDate == null) return false;
        
        //it's still active if the created date + 11 days is greater than the current time
        int daysIntoTrial = Math.round(((System.currentTimeMillis() - createdAtDate.getTime()) / MILLIS_IN_DAY));
        
        if(30 - daysIntoTrial < -1){
        	return true;
        }else{
        	return false;
        }
    }

    public Corporation getCorporation(long id) throws Exception {
        Connection conn = null;
        Corporation c = null;
        try {
            conn = ds.getConnection();
            PreparedStatement pst = conn.prepareStatement(corpSql + " where id = ?");
            pst.setLong(1, id);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                c = createCorporation(rs);
            }
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }

        return c;
    }

    public Corporation getCorporation(String slug) throws Exception {
        Connection conn = null;
        Corporation c = null;
        try {
            conn = ds.getConnection();
            PreparedStatement pst = conn.prepareStatement(corpSql + " where slug = ?");
            pst.setString(1, slug);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                c = createCorporation(rs);
            }
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }

        return c;
    }

    public Corporation getCorporationByName(String name) throws Exception {
        Connection conn = null;
        Corporation c = null;
        try {
            conn = ds.getConnection();
            PreparedStatement pst = conn.prepareStatement(corpSql + " where lower(a.name) = ?");
            pst.setString(1, name.trim().toLowerCase());
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                c = createCorporation(rs);
            }
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }

        return c;
    }

    public HashMap<Long, CorporationUser> getUserMapForCorporation(long id) throws Exception {
        List<CorporationUser> users = getUsersForCorporation(id);
        HashMap<Long, CorporationUser> map = new HashMap<Long, CorporationUser>();
        for (CorporationUser user : users) {
            map.put(user.getUser().getId(), user);
        }
        return map;
    }

    public List<CorporationUser> getUsersForCorporation(long id) throws Exception {
        LinkedList<CorporationUser> cu = new LinkedList<CorporationUser>();
        Connection conn = null;
        try {
            conn = ds.getConnection();
            PreparedStatement pst = conn.prepareStatement(corpUserSql + " where corpId = ? order by firstName || lastName || u.id asc, u.id asc, role");
            pst.setLong(1, id);
            ResultSet rs = pst.executeQuery();
            CorporationUser current = null;
            while (rs.next()) {
                CorporationUser u = createCorporationUser(rs, null, null);

                if (current == null || current.getUser().getId() != u.getUser().getId()) {
                    current = u;
                    cu.add(u);
                }
                current.addRole(u.getRole());

            }
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return cu;
    }

    public Map<String, String> getRoleMap() throws Exception {
        LinkedHashMap<String, String> map = new LinkedHashMap<String, String>();
        Connection conn = null;
        try {
            conn = ds.getConnection();
            PreparedStatement pst = conn.prepareStatement("select * from corp_roles order by seq");
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                map.put(rs.getString("id"), rs.getString("name"));
            }

        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return map;
    }

    public List<CorporationRole> getRolesForUserInCorporation(long userId, long corpId) throws Exception {
        LinkedList<CorporationRole> roles = new LinkedList<CorporationRole>();
        Connection conn = null;
        try {
            conn = ds.getConnection();
            PreparedStatement pst = conn.prepareStatement("select r.* from corp_roles r join corp_users on role = id where userId = ? and corpId = ? order by seq asc");
            pst.setLong(1, userId);
            pst.setLong(2, corpId);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                roles.add(createCorporationRole(rs));
            }
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return roles;
    }

    public void addUserToCorp(long corpId, long userId, String roles[]) throws Exception {
        Connection conn = null;
        try {
            conn = ds.getConnection();
            conn.setAutoCommit(false);
            PreparedStatement pst = conn.prepareStatement("insert into corp_users (userId, corpId, role) values (?, ?, ?)");
            pst.setLong(1, userId);
            pst.setLong(2, corpId);
            for (String role : roles) {
                pst.setString(3, role);
                pst.executeUpdate();
            }
            conn.commit();
        }
        finally {
            if (conn != null) {
                conn.setAutoCommit(true);
                conn.close();
            }
        }
    }

    public Invitation createInvitation(ResultSet rs) throws Exception {
        Invitation i = new Invitation();

        i.setId(rs.getString("id"));
        i.setName(rs.getString("name"));
        i.setSent(rs.getTimestamp("sent"));
        i.setExpires(rs.getTimestamp("expires"));
        i.setAccepted(rs.getTimestamp("accepted"));
        i.setAcceptor(rs.getLong("acceptor"));
        i.setEmail(rs.getString("email"));
        i.setInviterId(rs.getLong("inviter"));
        i.setCorpId(rs.getLong("corpId"));

        i.setRoles((String[]) rs.getArray("roles").getArray());

        if (rs.getArray("units") != null) {
            Long[] units = (Long[]) rs.getArray("units").getArray();
            long[] un = new long[units.length];
            for (int j = 0; j < un.length; j++) {
                un[j] = units[j];
            }
            i.setUnits(un);
        }

        User u = new User();
        u.setId(i.getInviterId());
        u.setFirstName(rs.getString("firstName"));
        u.setLastName(rs.getString("lastName"));
        i.setInviter(u);

        Corporation corp = new Corporation();
        corp.setId(i.getCorpId());
        corp.setName(rs.getString("corpName"));
        i.setCorporation(corp);

        return i;
    }
    
    public User createUser(ResultSet rs) throws Exception {

        User u = new User();
        u.setFirstName(rs.getString("firstName"));
        u.setLastName(rs.getString("lastName"));

        return u;
    }

    public Invitation getInvitation(String id) throws Exception {
        Connection conn = null;
        Invitation i = null;
        try {
            conn = ds.getConnection();
            PreparedStatement pst = conn.prepareStatement(inviteSql + " where i.id = ?");
            pst.setString(1, id);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                i = createInvitation(rs);
            }
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return i;
    }
    
    public User getUser(String email) throws Exception {
        Connection conn = null;
        User u = null;
        try {
            conn = ds.getConnection();
            PreparedStatement pst = conn.prepareStatement(" select * from users where email = ?");
            pst.setString(1, email);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
            	 u = createUser(rs);
            }
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return u;
    }

    public Invitation getInvitation(long acceptor, boolean newOnly) throws Exception {
        Connection conn = null;
        Invitation i = null;
        try {
            conn = ds.getConnection();
            String sql = inviteSql + " where i.acceptor = ?";
            if (newOnly) {
                sql += " and accepted is null";
            }
            PreparedStatement pst = conn.prepareStatement(sql);
            pst.setLong(1, acceptor);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                i = createInvitation(rs);
            }
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return i;
    }

    public Invitation save(Invitation i) throws Exception {
        String[] columns = {"id", "name", "accepted", "acceptor", "email", "inviter", "corpId", "roles", "units"};
        Connection conn = null;
        try {
            conn = ds.getConnection();
            PreparedStatement pst;
            if (i.getId() == null) {
                i.setId(UUID.randomUUID().toString());
                pst = conn.prepareStatement(createInsert("invitations", columns));
            }
            else {
                pst = conn.prepareStatement(createUpdate("invitations", columns, "id = ?"));
                pst.setString(columns.length + 1, i.getId());
            }

            int x = 0;
            pst.setString(++x, i.getId());
            pst.setString(++x, i.getName());
            Timestamp ts = null;
            if (i.getAccepted() != null) {
                ts = new Timestamp(i.getAccepted().getTime());
            }
            pst.setTimestamp(++x, ts);

            if (i.getAcceptor() > 0) {
                pst.setLong(++x, i.getAcceptor());
            }
            else {
                pst.setNull(++x, Types.BIGINT);
            }
            pst.setString(++x, i.getEmail());
            pst.setLong(++x, i.getInviterId());
            pst.setLong(++x, i.getCorpId());

            if (i.getRoles() != null && i.getRoles().length > 0) {
                pst.setArray(++x, conn.createArrayOf("varchar", i.getRoles()));
            }
            else {
                pst.setNull(++x, Types.ARRAY);
            }

            if (i.getUnits() != null && i.getUnits().length > 0) {
                Long[] units = new Long[i.getUnits().length];
                for (int y = 0; y < units.length; y++) {
                    units[y] = i.getUnits()[y];
                }
                pst.setArray(++x, conn.createArrayOf("bigint", units));
            }
            else {
                pst.setNull(++x, Types.ARRAY);
            }

            pst.executeUpdate();

        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }

        return i;
    }

    public List<Invitation> getInvitations(long corpId) throws Exception {
        Connection conn = null;
        LinkedList<Invitation> i = new LinkedList<Invitation>();
        try {
            conn = ds.getConnection();
            PreparedStatement pst = conn.prepareStatement(inviteSql + " where corpId = ? order by sent desc");
            pst.setLong(1, corpId);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                i.add(createInvitation(rs));
            }
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return i;
    }

    public void sendInvitation(Invitation i) throws Exception {
        // make sure all fields we need are there. (user/corp name)
    	String url = null;
        i = getInvitation(i.getId());
        if(Config.getString("https").equals("true")){
        	url = Config.getString("url.secure");
        }else{
        	url = Config.getString("url");
        }
        Properties props = new Properties();
        props.put("name", i.getName());
        props.put("firstName", i.getInviter().getFirstName());
        props.put("lastName", i.getInviter().getLastName());
        props.put("corp", i.getCorporation().getName());
        props.put("email", i.getEmail());
        props.put("url", url+"/users/invite.jsp?key=" + i.getId());
        MailMessage msg = new MailMessage(props, "invite.vm", i.getEmail(), "Invitation to join " + i.getCorporation().getName(), true);

        try {
            msg.send();
        }
        catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }
    
    public void sendPromotion(String email, String promocode, String company, String validity, String discount) throws Exception {
        // make sure all fields we need are there. (user/corp name)
        User u = (User) getUser(email);
        String url = null;
        String urlhttp = null;
        if(Config.getString("https").equals("true")){
        	url = Config.getString("url.secure").replaceFirst("^(https://)","");
        	urlhttp = "https://";
        }else{
        	url = Config.getString("url").replaceFirst("^(http://)","");
        	urlhttp = "http://";
        }
        Properties props = new Properties();
        props.put("name", u.getFirstName()+" "+u.getLastName());
        props.put("validity", validity);
        props.put("discount", discount);
        props.put("promocode", promocode);
        props.put("email", email);
        props.put("url", urlhttp+company+"."+url+ "/corp/subscription/promotion_check_valid.jsp?promocode="+promocode);
        MailMessage msg = new MailMessage(props, "promotion.vm", email,"Your Fiizio Promo Code",true);

        try {
            msg.send();
        }
        catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    public void sendInvitationAccept(Invitation i) throws Exception {
        // make sure all fields we need are there. (user/corp name)
        i = getInvitation(i.getId());

        UserManager um = new UserManager();
        User inviter = um.getUser(i.getInviterId());
        User acceptor = um.getUser(i.getAcceptor());

        Properties props = new Properties();
        props.put("name", acceptor.getFirstName() + " " + acceptor.getLastName());
        props.put("firstName", inviter.getFirstName());
        props.put("lastName", inviter.getLastName());
        props.put("corp", i.getCorporation().getName());
        //props.put("url", Config.getString("url") + "/users/invite.jsp?key=" + i.getId());
        MailMessage msg = new MailMessage(props, "invite_accept.vm", inviter.getEmail(), acceptor.getFirstName() + " " + acceptor.getLastName() + " has accepted your invitation",true);

        try {
            msg.send();
        }
        catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

    }

    public boolean isReserved(String slug) throws Exception {
        String res[] = Config.getString("reserved").split(" ");
        List<String> list = Arrays.asList(res);
        return list.contains(slug);
    }

    public Unit createUnit(ResultSet rs) throws Exception {
        Unit u = new Unit();

        Corporation c = new Corporation();
        c.setId(rs.getLong("corpId"));
        c.setName(rs.getString("corpName"));
        c.setSlug(rs.getString("slug"));

        u.setId(rs.getLong("id"));
        u.setCorporation(c);
        u.setName(rs.getString("name"));
        u.setAddress1(rs.getString("address1"));
        u.setAddress2(rs.getString("address2"));
        u.setSuburb(rs.getString("suburb"));
        u.setState(rs.getString("state"));
        u.setPostcode(rs.getString("postcode"));
        u.setCountry(rs.getString("country"));
        u.setCountryName(rs.getString("countryName"));
        u.setAdded(rs.getTimestamp("added"));
        u.setNotes(rs.getString("notes"));
        u.setUev(rs.getInt("uev"));

        return u;
    }

    public List<Unit> getUnitsForCorp(long corpId) throws Exception {
        LinkedList<Unit> units = new LinkedList<Unit>();
        Connection conn = null;
        try {
            conn = ds.getConnection();
            PreparedStatement pst = conn.prepareStatement(unitSql + " where corpId = ? and (since is null or now() > since) and (until is null or until > now()) order by lower(u.name) asc");
            pst.setLong(1, corpId);
            ResultSet rs = pst.executeQuery();
            Unit current = null;
            while (rs.next()) {
                Unit unit = createUnit(rs);
                if (current == null || current.getId() != unit.getId()) {
                    current = unit;
                    units.add(unit);
                }
                long userId = rs.getLong("userId");
                if (userId > 0) {
                    UnitUser uu = new UnitUser();

                    User u = new User();
                    u.setFirstName(rs.getString("firstName"));
                    u.setLastName(rs.getString("lastName"));
                    u.setId(userId);
                    u.setEmail(rs.getString("email"));
                    uu.setUser(u);

                    uu.setCurrent(rs.getBoolean("current"));
                    uu.setPreferredAddress(rs.getBoolean("preferredaddress"));
                    uu.setSince(rs.getTimestamp("since"));
                    uu.setUntil(rs.getTimestamp("until"));
                    uu.setType(UnitUser.Type.valueOf(rs.getString("uutype")));
                    uu.setUnit(current);

                    current.addOwner(uu);
                }

            }
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return units;
    }

    public Unit save(Unit unit) throws Exception {
        String[] cols = {"id", "corpId", "name", "address1", "address2", "suburb", "state", "postcode", "country", "notes", "uev"};
        Connection conn = null;
        try {
            conn = ds.getConnection();
            PreparedStatement pst;
            if (unit.getId() == 0) {
                unit.setId(getSequence("unitId"));
                pst = conn.prepareStatement(createInsert("units", cols));
            }
            else {
                pst = conn.prepareStatement(createUpdate("units", cols, "id = ?"));
                pst.setLong(cols.length + 1, unit.getId());
            }
            int i = 0;
            pst.setLong(++i, unit.getId());
            pst.setLong(++i, unit.getCorporation().getId());
            pst.setString(++i, unit.getName());
            pst.setString(++i, unit.getAddress1());
            pst.setString(++i, unit.getAddress2());
            pst.setString(++i, unit.getSuburb());
            pst.setString(++i, unit.getState());
            pst.setString(++i, unit.getPostcode());
            pst.setString(++i, unit.getCountry());
            pst.setString(++i, unit.getNotes());
            pst.setInt(++i, unit.getUev());
            pst.executeUpdate();
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return unit;
    }

    public boolean isUnitNameAvailable(String name, long corpId, long unitId) throws Exception {
        boolean rv = false;
        Connection conn = null;
        try {
            conn = ds.getConnection();
            PreparedStatement pst = conn.prepareStatement("select id from units where lower(name) = ? and corpid = ? and id != ?");
            pst.setString(1, name.toLowerCase());
            pst.setLong(2, corpId);
            pst.setLong(3, unitId);
            ResultSet rs = pst.executeQuery();
            rv = !rs.next();
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return rv;
    }

    public Unit getLastUnitForCorp(long corpId) throws Exception {
        Unit unit = null;
        Connection conn = null;
        try {
            conn = ds.getConnection();
            PreparedStatement pst = conn.prepareStatement(unitSql + " where corpId = ? order by added desc limit 1");
            pst.setLong(1, corpId);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                unit = createUnit(rs);
            }
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return unit;
    }

    public void deleteUnitUser(long userId, long unitId) throws Exception {
        Connection conn = null;
        try {
            conn = ds.getConnection();
            PreparedStatement pst = conn.prepareStatement("delete from unit_users where unitid = ?  and userid = ?");
            pst.setLong(1, unitId);
            pst.setLong(2, userId);
            pst.executeUpdate();
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public List<Unit> getAvailableUnits(long corpId, long userId) throws Exception {
        LinkedList<Unit> units = new LinkedList<Unit>();
        Connection conn = null;
        try {
            conn = ds.getConnection();
            String sql = unitSqlNoOwner + " where corpid = ? and u.id not in (select unitid from unit_users where userid = ?) order by u.name asc";
            PreparedStatement pst = conn.prepareStatement(sql);
            pst.setLong(1, corpId);
            pst.setLong(2, userId);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                units.add(createUnit(rs));
            }
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return units;
    }

    public void save(UnitUser uu) throws Exception {
        String columns[] = {"unitid", "userid", "type", "since", "until", "current", "preferredaddress"};
        Connection conn = null;
        try {
            conn = ds.getConnection();

            PreparedStatement check = conn.prepareStatement("select * from unit_users where unitid = ?  and userid = ? and type = ?");
            check.setLong(1, uu.getUnit().getId());
            check.setLong(2, uu.getUser().getId());
            check.setString(3, uu.getType().name());
            ResultSet rs = check.executeQuery();
            boolean isUpdate = rs.next();
            rs.close();
            check.close();

            PreparedStatement pst;
            if (isUpdate) {
                pst = conn.prepareStatement(createUpdate("unit_users", columns, "unitid = ?  and userid = ? and type = ?"));
                pst.setLong(columns.length + 1, uu.getUnit().getId());
                pst.setLong(columns.length + 2, uu.getUser().getId());
                pst.setString(columns.length + 3, uu.getType().name());
            }
            else {
                pst = conn.prepareStatement(createInsert("unit_users", columns));
            }
            int i = 0;
            pst.setLong(++i, uu.getUnit().getId());
            pst.setLong(++i, uu.getUser().getId());
            pst.setString(++i, uu.getType().name());
            if (uu.getSince() != null) {
                pst.setTimestamp(++i, new Timestamp(uu.getSince().getTime()));
            }
            else {
                pst.setNull(++i, Types.TIMESTAMP);
            }
            if (uu.getUntil() != null) {
                pst.setTimestamp(++i, new Timestamp(uu.getUntil().getTime()));
            }
            else {
                pst.setNull(++i, Types.TIMESTAMP);
            }
            pst.setBoolean(++i, uu.isCurrent());
            pst.setBoolean(++i, uu.isPreferredAddress());
            pst.executeUpdate();

        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public void addRole(long userId, long corpId, String role, long inviter) throws Exception {
        Connection conn = null;
        try {
            conn = ds.getConnection();
            PreparedStatement pst = conn.prepareStatement("insert into corp_users (userid, corpid, role, inviter) values (?, ?, ?, ?)");
            pst.setLong(1, userId);
            pst.setLong(2, corpId);
            pst.setString(3, role);
            if (inviter > 0) {
                pst.setLong(4, inviter);
            }
            else {
                pst.setNull(4, Types.BIGINT);
            }
            pst.execute();
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public void removeRole(long userId, long corpId, String role) throws Exception {
        Connection conn = null;
        try {
            conn = ds.getConnection();
            PreparedStatement pst1 = conn.prepareStatement("select id from users where email='kumar.sathish345@gmail.com' or email='shagul001@gmail.com'");
            ResultSet rs = pst1.executeQuery();
            if(rs.next()){
            	 System.out.println("You can't remove admin users for kumar.sathish345@gmail.com");
            }else{
            	 PreparedStatement pst = conn.prepareStatement("delete from corp_users where userid = ? and corpid = ? and role = ?");
                 pst.setLong(1, userId);
                 pst.setLong(2, corpId);
                 pst.setString(3, role);
                 pst.execute();
            }
            
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public void removeUser(long userId, long corpId) throws Exception {
        Connection conn = null;
        long id2=0;
        try {
            conn = ds.getConnection();
            conn.setAutoCommit(false);
            PreparedStatement pst1 = conn.prepareStatement("select id from users where email='kumar.sathish345@gmail.com' or email='shagul001@gmail.com'");
            ResultSet rs = pst1.executeQuery();
            if(rs.next()){
            id2=rs.getLong("id");
            }
                if(id2==userId){
            
            	 System.out.println("You can't remove admin user for kumar.sathish345@gmail.com");
            }else{
            	String n1;
                PreparedStatement pst2 = conn.prepareStatement("select email  from users where id=?");
                pst2.setLong(1,userId);
                ResultSet rs2=pst2.executeQuery();
                if(rs2.next()){
                n1=rs2.getString("email");
                PreparedStatement pst3 = conn.prepareStatement("delete from invitations where email=?");
                pst3.setString(1,n1);
                pst3.executeUpdate();
                pst3.close();
                }
          
            PreparedStatement pst = conn.prepareStatement("delete from corp_users where userid = ? and corpid = ?");
            pst.setLong(1, userId);
            pst.setLong(2, corpId);
            pst.executeUpdate();
            pst.close();

            pst = conn.prepareStatement("delete from unit_users where userid = ? and unitid in (select id from units where corpid = ?)");
            pst.setLong(1, userId);
            pst.setLong(2, corpId);
            pst.executeUpdate();
            pst.close();

            conn.commit();
            }
             
        }
        finally {
            if (conn != null) {
                conn.setAutoCommit(true);
                conn.close();
            }
        }
    }

    public void setRoles(List<CorporationRole> roles, long userId, long corpId) throws Exception {
        Connection conn = null;
        try {
            conn = ds.getConnection();
            conn.setAutoCommit(false);
            PreparedStatement pst = conn.prepareStatement("delete from corp_users where userId = ? and corpId = ?");
            pst.setLong(1, userId);
            pst.setLong(2, corpId);
            pst.executeUpdate();
            pst.close();
            pst = conn.prepareStatement("insert into corp_users (userId, corpId, role) values (?, ?, ?)");
            pst.setLong(1, userId);
            pst.setLong(2, corpId);
            for (CorporationRole role : roles) {
                pst.setString(3, role.getId());
                pst.executeUpdate();
            }
            pst.close();
            conn.commit();
        }
        finally {
            if (conn != null) {
                conn.setAutoCommit(true);
                conn.close();
            }
        }
    }

    public void accept(Invitation invite, User u) throws Exception {
        long corpId = invite.getCorpId();
        List<CorporationRole> roles = getRolesForUserInCorporation(u.getId(), corpId);
        List<UnitUser> units = getUnitsForUser(u.getId(), corpId);
        List<Unit> allUnits = getUnitsForCorp(corpId);
        long[] uids = new long[allUnits.size()];

        UnitUser.Type unitType = UnitUser.Type.owner;
        for (String role : invite.getRoles()) {
            boolean exists = false;
            for (CorporationRole corporationRole : roles) {
                if (role.equalsIgnoreCase("TN")) {
                    unitType = UnitUser.Type.tenant;
                }
                if (corporationRole.getId().equalsIgnoreCase(role)) {
                    exists = true;
                    break;
                }
            }
            if (!exists) {
                addRole(u.getId(), corpId, role, invite.getInviter().getId());
            }
        }

        invite.setAccepted(new Date());
        invite.setAcceptor(u.getId());
        save(invite);
        sendInvitationAccept(invite);
    }

    public void setCorpMeta(long corpId, String key, String value) throws Exception {
        Connection conn = ds.getConnection();
        try {
            PreparedStatement pst = conn.prepareStatement("update corp_meta set text_val = ? where key = ? and corp_id = ?");
            pst.setString(1, value);
            pst.setString(2, key);
            pst.setLong(3, corpId);
            int i = pst.executeUpdate();
            pst.close();

            if (i == 0) {
                pst = conn.prepareStatement("insert into corp_meta (key, corp_id, text_val) values (?, ?, ?)");
                pst.setString(1, key);
                pst.setLong(2, corpId);
                pst.setString(3, value);
                pst.execute();
                pst.close();
            }
        }
        finally {
            conn.close();
        }
    }

    public Map<String, CorpMeta> getCorpMeta(long corpId) throws Exception {
        LinkedHashMap<String, CorpMeta> map = new LinkedHashMap<String, CorpMeta>();

        Connection conn = ds.getConnection();
        try {
            PreparedStatement pst = conn.prepareStatement("select name, type, v.* from corp_meta v join corp_meta_keys k on v.key = k.key and corp_id = ? order by seq");
            pst.setLong(1, corpId);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                CorpMeta cm = new CorpMeta();
                cm.setCorpId(rs.getLong("corp_id"));
                cm.setKey(rs.getString("key"));
                cm.setName(rs.getString("name"));
                cm.setStringValue(rs.getString("text_val"));
                cm.setLongValue(rs.getLong("int_val"));
                cm.setDoubleValue(rs.getDouble("dbl_val"));
                cm.setDateValue(rs.getTimestamp("date_val"));
                map.put(cm.getKey(), cm);
            }
            rs.close();
            pst.close();
        }
        finally {
            conn.close();
        }

        return map;
    }
    
    public List<String> verifyKey (String email) throws Exception
    {
    	Connection conn = null;
       LinkedList<String> lst=new LinkedList<String>();
        try {
            conn = ds.getConnection();
            PreparedStatement pst = conn.prepareStatement("select id from invitations where email = ? order by sent desc" );
            pst.setString(1, email);
            ResultSet rs=pst.executeQuery();
            if(rs.next())
            {
            	lst.add(rs.getString("id"));
            } 
            pst.close();
            rs.close();
          }
        finally
        {
        	if(conn!=null)
        	{
        		conn.close();
        	}
        }
        return lst;
      }
    public List<String> pendingStatus (String email) throws Exception
    {
    	Connection conn = null;
    	PreparedStatement pst=null;
    	LinkedList<String> lst=new LinkedList<String>();
        try {
            conn = ds.getConnection();
            pst = conn.prepareStatement("SELECT inviter FROM invitations where  email=? and acceptor isNull" );
            pst.setString(1, email);
            ResultSet rs = pst.executeQuery();
	         while (rs.next()) {
	        	  PreparedStatement pst2=conn.prepareStatement("select u.firstname,u.lastname,c.name,u.email,i.id from invitations i join users u  on inviter = u.id join corporation c on c.id=i.corpid  where i.inviter=? and i.acceptor isNull and now() < expires");
	          pst2.setLong(1, rs.getLong("inviter"));
	          ResultSet rs2 = pst2.executeQuery();
	            while(rs2.next())
	            {
	            	lst.add(rs2.getString("firstname"));
	            	lst.add(rs2.getString("lastname"));
	            	lst.add(rs2.getString("name"));
	            	lst.add(rs2.getString("email"));
	            	lst.add(rs2.getString("id"));
	            
	            }
	            
	         }
          }
        finally
        {
        	if(conn!=null)
        	{
        		conn.close();
        	}
        	if(pst!=null)
        	{
        		pst.close();
        	}
        }
        return lst;
      }

    
    public void chkForNameUpdate(long shareId, String name) throws SQLException{
    	String exerciseName = null;
        Connection connection = ds.getConnection();
        PreparedStatement statement = connection.prepareStatement("select name from exercises where shareid = ?");
    	statement.setLong(1, shareId);
    	ResultSet resultset = statement.executeQuery();
    	while(resultset.next()){
    		exerciseName = resultset.getString("name");
    	}
    	statement.close();
    	resultset.close();
    	
    	
    	if(exerciseName!=name){
    		statement = connection.prepareStatement("update exercises set name = ? where shareid = ?");
    		statement.setString(1, name);
    		statement.setLong(2, shareId);
    		statement.executeUpdate();
    	}
    	statement.close();
    	connection.close();
    }
    public void chkForTemplateNameUpdate(long shareId, String name,String itype,int intr) throws SQLException{
    	String exerciseName = null;
        Connection connection = ds.getConnection();
        PreparedStatement stmt = connection.prepareStatement("select name from programs where shareid = ?");
    	stmt.setLong(1, shareId);
    	ResultSet resultset = stmt.executeQuery();
    	while(resultset.next()){
    		exerciseName = resultset.getString("name");
    	}
    	stmt.close();
    	resultset.close();
    	
    	
    	if(exerciseName!=name){
    		stmt = connection.prepareStatement("update programs set name = ?where shareid = ?");
    		stmt.setString(1, name);
    		stmt.setLong(2, shareId);
    		stmt.executeUpdate();
    		stmt.close();
    	}
    	stmt = connection.prepareStatement("update programs set int_type = ?, int_val = ? where shareid = ?");
		stmt.setString(1, itype);
		stmt.setInt(2, intr);
		stmt.setLong(3, shareId);
		stmt.executeUpdate();
    	stmt.close();
    	connection.close();
    }
    public void chkForArticleNameUpdate(long shareId, String headline) throws SQLException{
    	String exerciseName = null;
        Connection connection = ds.getConnection();
        PreparedStatement stmt = connection.prepareStatement("select headline from info where shareid = ?");
    	stmt.setLong(1, shareId);
    	ResultSet resultset = stmt.executeQuery();
    	while(resultset.next()){
    		exerciseName = resultset.getString("headline");
    	}
    	stmt.close();
    	resultset.close();
    	
    	
    	if(exerciseName!=headline){
    		stmt = connection.prepareStatement("update info set headline = ? where shareid = ?");
    		stmt.setString(1, headline);
    		stmt.setLong(2, shareId);
    		stmt.executeUpdate();
    	}
    	stmt.close();
    	connection.close();
    }
    public void chkForNewsNameUpdate(long shareId, String headline) throws SQLException{
    	String exerciseName = null;
        Connection connection = ds.getConnection();
        PreparedStatement stmt = connection.prepareStatement("select headline from mini_news where shareid = ?");
    	stmt.setLong(1, shareId);
    	ResultSet resultset = stmt.executeQuery();
    	while(resultset.next()){
    		exerciseName = resultset.getString("headline");
    	}
    	stmt.close();
    	resultset.close();
    	
    	
    	if(exerciseName!=headline){
    		stmt = connection.prepareStatement("update mini_news set headline = ? where shareid = ?");
    		stmt.setString(1, headline);
    		stmt.setLong(2, shareId);
    		stmt.executeUpdate();
    	}
    	stmt.close();
    	connection.close();
    }
    
    public int getSubLocationCount(long corpId){
    	int subLocCnt = 0;
    	try{
    		Connection connection = ds.getConnection();
    		PreparedStatement stmt = connection.prepareStatement("select count(*) as subLocationCnt from corporation where company = (select company from corporation where id = ? and mainlocation = 't') and id <> ? and mainlocation = 'f'");
        	stmt.setLong(1, corpId);
        	stmt.setLong(2, corpId);
        	ResultSet resultset = stmt.executeQuery();
        	while(resultset.next()){
        		subLocCnt = resultset.getInt("subLocationCnt");
        	}
        	resultset.close();
        	stmt.close();
        	connection.close();
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    	return subLocCnt;
    }
    
    public List<String> getSubLocations(long corpId){
    	List<String> subLocationLst = new ArrayList<String>();
    	try{
    		Connection connection = ds.getConnection();
            PreparedStatement stmt = connection.prepareStatement("select id from corporation where company = (select company from corporation where id = ? and mainlocation = 't') and id <> ? and mainlocation = 'f'");
        	stmt.setLong(1, corpId);
        	stmt.setLong(2, corpId);
        	ResultSet resultset = stmt.executeQuery();
        	while(resultset.next()){
        		subLocationLst.add(String.valueOf(resultset.getLong("id")));
        	}
        	resultset.close();
        	stmt.close();
        	connection.close();
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    	return subLocationLst;
    }
}