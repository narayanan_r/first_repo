package au.com.ourbodycorp.model.managers;

import au.com.ourbodycorp.model.*;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;

public class PostManager extends ManagerBase {

  private String postColumns[] = {"versionId", "id", "seqNo", "blogId", "parent", "status", "userId", "type", "posted",
        "updated", "startDate", "endDate", "title", "excerpt", "body", "format", "external",
        "gallery", "shortCode", "allowComments", "icon", "banner", "blogLink", "bannerbg"};


  private Post createPost(ResultSet rs) throws Exception {
    Post p = new Post();

    p.setVersionId(rs.getLong("versionId"));
    p.setId(rs.getLong("id"));
    p.setParent(rs.getLong("parent"));
    p.setBlogId(rs.getLong("blogId"));
    p.setSeqNo(rs.getLong("seqno"));
    p.setStatus(Post.Status.valueOf(rs.getString("status")));
    p.setUserId(rs.getInt("userId"));
    p.setType(Post.PostType.valueOf(rs.getString("type")));
    p.setPosted(rs.getTimestamp("posted"));
    p.setUpdated(rs.getTimestamp("updated"));
    p.setStart(rs.getTimestamp("startDate"));
    p.setEnd(rs.getTimestamp("endDate"));
    p.setTitle(rs.getString("title"));
    p.setExcerpt(rs.getString("excerpt"));
    p.setBody(rs.getString("body"));
    p.setFormat(Post.Format.valueOf(rs.getString("format")));
    p.setExternal(rs.getBoolean("external"));
    p.setGallery(rs.getBoolean("gallery"));
    p.setShortCode(rs.getString("shortCode"));
    p.setAuthor(rs.getString("author"));
    p.setAllowComments(rs.getBoolean("allowComments"));
    p.setIconId(rs.getLong("icon"));
    p.setBannerId(rs.getLong("banner"));
    p.setBlogId(rs.getLong("blogId"));
    p.setBlogLink(rs.getLong("blogLink"));
    p.setBannerBackground(rs.getString("bannerbg"));

    boolean extended;
    try {
      extended = rs.findColumn("iId") > 0;
    } catch (SQLException e) {
      extended = false;
    }
    if (extended) {
      if (rs.getInt("iId") > 0) {
        Attachment a = new Attachment();
        a.setId(rs.getInt("iId"));
        a.setFilename(rs.getString("iFilename"));
        a.setCaption(rs.getString("iCaption"));
        a.setMimeType(rs.getString("iMimeType"));
        a.setLink(rs.getString("iLink"));
        p.setIcon(a);
      }
      if (rs.getInt("bId") > 0) {
        Attachment b = new Attachment();
        b.setId(rs.getInt("bId"));
        b.setFilename(rs.getString("bFilename"));
        b.setCaption(rs.getString("bCaption"));
        b.setMimeType(rs.getString("bMimeType"));
        b.setLink(rs.getString("bLink"));
        p.setBanner(b);
      }
    }

    return p;
  }

  public void deletePost(long id) throws Exception {
    Connection conn = null;
    try {
      conn = ds.getConnection();
      PreparedStatement pst = conn.prepareStatement("update posts set status = ? where status in (?, ?) and id = ?");
      pst.setString(1, Post.Status.deleted.name());
      pst.setString(2, Post.Status.published.name());
      pst.setString(3, Post.Status.draft.name());
      pst.setLong(4, id);
      pst.executeUpdate();
    } finally {
      if (conn != null) {
        conn.close();
      }
    }
  }

  public void unPublish(long id) throws Exception {
    Connection conn = null;
    try {
      conn = ds.getConnection();
      conn.setAutoCommit(false);

      PreparedStatement pst = conn.prepareStatement("update posts set status = ? where status = ? and id = ?");
      pst.setString(1, Post.Status.obsolete.name());
      pst.setString(2, Post.Status.draft.name());
      pst.setLong(3, id);

      pst.executeUpdate();

      pst.setString(1, Post.Status.draft.name());
      pst.setString(2, Post.Status.published.name());
      pst.setLong(3, id);
      pst.executeUpdate();

      conn.commit();
    } finally {
      if (conn != null) {
        conn.setAutoCommit(true);
        conn.close();
      }
    }
  }

  public Post getPost(long versionId) throws Exception {
    Post post = null;
    Connection conn = null;
    try {
      conn = ds.getConnection();
      PreparedStatement pst = conn.prepareStatement("select * from admin_posts where versionId = ?");
      pst.setLong(1, versionId);
      ResultSet rs = pst.executeQuery();
      if (rs.next()) {
        post = createPost(rs);
      }

    } finally {
      if (conn != null) conn.close();
    }

    return post;
  }

  // Get latest published version of a post.
  public Post getLivePost(long id) throws Exception {
    Post post = null;
    Connection conn = null;
    try {
      conn = ds.getConnection();
      PreparedStatement pst = conn.prepareStatement("select * from live_posts_img where id = ?");
      pst.setLong(1, id);
      ResultSet rs = pst.executeQuery();
      if (rs.next()) {
        post = createPost(rs);
      }

    } finally {
      if (conn != null) conn.close();
    }

    return post;

  }

  public Post getBlogPost(long blogId, int year, int month, String slug) throws Exception {
    Connection conn = null;
    Post p = null;
    try {
      conn = ds.getConnection();
      PreparedStatement pst = conn.prepareStatement("select * from live_posts_img where blogId = ? and shortcode = ? and date_part('month', posted) = ? and date_part('year', posted) = ?");
      pst.setLong(1, blogId);
      pst.setString(2, slug);
      pst.setInt(3, month);
      pst.setInt(4, year);
      ResultSet rs = pst.executeQuery();
      if (rs.next()) {
        p = createPost(rs);
      }
    } finally {
      if (conn != null) {
        conn.close();
      }
    }
    return p;
  }

  public List<Post> getBlogPosts(long blogId, Date start, Date end) throws Exception {
    LinkedList<Post> posts = new LinkedList<Post>();
    Connection conn = null;
    try {
      conn = ds.getConnection();
      PreparedStatement pst = conn.prepareStatement("select * from live_posts_img where blogId = ? and posted >= ? and posted < ? and posted <= current_timestamp order by posted asc");
      pst.setLong(1, blogId);
      pst.setTimestamp(2, new Timestamp(start.getTime()));
      pst.setTimestamp(3, new Timestamp(end.getTime()));
      ResultSet rs = pst.executeQuery();

      while (rs.next()) {
        posts.add(createPost(rs));
      }
    } finally {
      if (conn != null) conn.close();
    }
    return posts;
  }

  public List<Post> getBlogPosts(long blogId, long limit) throws Exception {
    LinkedList<Post> posts = new LinkedList<Post>();
    Connection conn = null;
    try {
      conn = ds.getConnection();
      PreparedStatement pst = conn.prepareStatement("select * from live_posts_img where blogId = ? and posted <= current_timestamp order by posted desc limit ?");
      pst.setLong(1, blogId);
      pst.setLong(2, limit);
      ResultSet rs = pst.executeQuery();

      while (rs.next()) {
        posts.add(createPost(rs));
      }
    } finally {
      if (conn != null) conn.close();
    }
    return posts;
  }

  public List<BlogArchive> getArchiveMonths(long blogId) throws Exception {
    LinkedList<BlogArchive> months = new LinkedList<BlogArchive>();
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    Connection conn = null;
    try {
      conn = ds.getConnection();
      PreparedStatement pst = conn.prepareStatement("select distinct date_part('year', posted) as year, date_part('month', posted) as month, " +
          "count(id) as cnt from live_posts_img where blogId = ? and posted <= current_timestamp\n" +
          "group by date_part('year', posted), date_part('month', posted)\n" +
          "order by date_part('year', posted) desc, date_part('month', posted) desc");
      pst.setLong(1, blogId);
      ResultSet rs = pst.executeQuery();
      while (rs.next()) {
        Date date = sdf.parse(String.format("%d-%2d-01", rs.getInt("year"), rs.getInt("month")));
        months.add(new BlogArchive(date, rs.getInt("cnt")));
      }
    } finally {
      if (conn != null) {
        conn.close();
      }
    }
    return months;
  }

  public Post getLatestAnchorPoint(long blogId) throws Exception {
    Connection conn = null;
    Post p = null;
    try {
      Blog blog = getBlog(blogId);
      if (blog != null && blog.getPostId() > 0) {
        conn = ds.getConnection();
        PreparedStatement pst = conn.prepareStatement("select * from admin_posts_latest where id = ?");
        pst.setLong(1, blog.getPostId());
        ResultSet rs = pst.executeQuery();
        if (rs.next()) {
          p = createPost(rs);
        }
      }
    } finally {
      if (conn != null) {
        conn.close();
      }
    }
    return p;
  }

  public List<Post> getLivePosts(Post.PostType type, long parent, boolean withImages) throws Exception {
    LinkedList<Post> posts = new LinkedList<Post>();
    Connection conn = null;
    try {
      String view = (withImages) ? "live_posts_img" : "live_posts";
      String p = (parent == 0) ? "parent is null" : "parent = ?";
      conn = ds.getConnection();
      String sql = "select * from " + view + " where type = ? and " + p + " order by seqno";
      PreparedStatement pst = conn.prepareStatement(sql);
      pst.setString(1, type.name());
      if (parent > 0) {
        pst.setLong(2, parent);
      }
      ResultSet rs = pst.executeQuery();
      while (rs.next()) {
        posts.add(createPost(rs));
      }
    } finally {
      if (conn != null) conn.close();
    }
    return posts;
  }

  public Post getDraftPost(long id) throws Exception {
    Post post = null;
    Connection conn = null;
    try {
      conn = ds.getConnection();
      PreparedStatement pst = conn.prepareStatement("select * from draft_posts where id = ?");
      pst.setLong(1, id);
      ResultSet rs = pst.executeQuery();
      if (rs.next()) {
        post = createPost(rs);
      }

    } finally {
      if (conn != null) conn.close();
    }

    return post;

  }

  public Post getPost(String shortCode) throws Exception {
    return getPost(shortCode, 0);
  }

  public Post getPost(String shortCode, long parent) throws Exception {
    Post post = null;
    Connection conn = null;
    try {
      conn = ds.getConnection();
      PreparedStatement pst;
      if (parent > 0) {
        pst = conn.prepareStatement("select * from live_posts where shortCode = ? and parent = ?");
        pst.setLong(2, parent);
      } else {
        pst = conn.prepareStatement("select * from live_posts where shortCode = ? and parent is null");
      }
      pst.setString(1, shortCode);
      ResultSet rs = pst.executeQuery();
      if (rs.next()) {
        post = createPost(rs);
      }

    } finally {
      if (conn != null) conn.close();
    }

    return post;
  }

  public Post getLivePostForCategory(long category, int limit, int offset) throws Exception {
    Post post = null;
    Connection conn = null;

    try {
      conn = ds.getConnection();
      String sql = "select * from live_posts_img where versionId in (select versionId from post_categories where categoryId = ?) order by posted desc";
      if (limit > 0) {
        sql += " offset " + offset + " limit " + limit;
      }
      PreparedStatement pst = conn.prepareStatement(sql);
      pst.setLong(1, category);

      ResultSet rs = pst.executeQuery();
      if (rs.next()) {
        post = createPost(rs);
      }

    } finally {
      if (conn != null) conn.close();
    }

    return post;
  }

  public Post getLivePostForBlog(long blog, int limit, int offset) throws Exception {
    Post post = null;
    Connection conn = null;

    try {
      conn = ds.getConnection();
      String sql = "select * from live_posts_img where blogId = ?) order by posted desc";
      if (limit > 0) {
        sql += " offset " + offset + " limit " + limit;
      }
      PreparedStatement pst = conn.prepareStatement(sql);
      pst.setLong(1, blog);

      ResultSet rs = pst.executeQuery();
      if (rs.next()) {
        post = createPost(rs);
      }

    } finally {
      if (conn != null) conn.close();
    }

    return post;
  }

  public List<Post> sortPages(List<Post> posts) {
    LinkedList<Post> sorted = new LinkedList<Post>();
    HashMap<Long, LinkedList<Post>> map = new HashMap<Long, LinkedList<Post>>();
    HashMap<Long, Long> draftVersions = new HashMap<Long, Long>();
    HashMap<Long, Long> publishedVersions = new HashMap<Long, Long>();


    for (Post post : posts) {
      if (post.getStatus() == Post.Status.draft) {
        draftVersions.put(post.getId(), post.getVersionId());
      } else {
        publishedVersions.put(post.getId(), post.getVersionId());
      }
      if (post.getStatus() == Post.Status.draft && publishedVersions.get(post.getId()) != null) {
        continue;
      }
      if (map.get(post.getParent()) == null) {
        map.put(post.getParent(), new LinkedList<Post>());
      }
      LinkedList<Post> children = map.get(post.getParent());
      children.add(post);

    }

    LinkedList<Post> root = map.get(new Long(0));
    Collections.sort(root);
    for (Post post : root) {
      post.setDepth(0);
      processParent(post, sorted, map);
    }

    LinkedList<Post> finalList = new LinkedList<Post>();
    for (Post post : sorted) {
      if (post.getStatus() == Post.Status.draft && publishedVersions.get(post.getId()) != null) {
        continue;
      } else if (post.getStatus() == Post.Status.published && draftVersions.get(post.getId()) != null) {
        post.setDraftVersion(draftVersions.get(post.getId()));
      }
      finalList.add(post);
    }

    return finalList;
  }

  private void processParent(Post parent, LinkedList<Post> sorted, HashMap<Long, LinkedList<Post>> map) {
    sorted.add(parent);
    if (map.get(parent.getId()) != null) {
      LinkedList<Post> children = map.get(parent.getId());
      Collections.sort(children);
      for (Post child : children) {
        child.setDepth(parent.getDepth() + 1);
        processParent(child, sorted, map);
      }
    }

  }

  public List<Post> getAdminPosts(long parentOrBlogId, Post.PostType type, int limit, int offset) throws Exception {
    LinkedList<Post> posts = new LinkedList<Post>();

    Connection conn = null;

    try {
      conn = ds.getConnection();

      StringBuilder sb = new StringBuilder("select * from admin_posts");

      if (parentOrBlogId > 0 || type != null) {
        sb.append(" where ");
      }

      if (parentOrBlogId > 0 && type == Post.PostType.blog) {
        sb.append("blogId = ?");
      } else if (parentOrBlogId > 0) {
        sb.append("parent = ?");
      }

      if (type != null) {
        if (parentOrBlogId > 0) {
          sb.append(" and ");
        }
        sb.append("type = ?");
      }

      if (limit > 0) {
        sb.append(" offset ? limit ?");
      }

      PreparedStatement pst = conn.prepareStatement(sb.toString());
      int i = 0;
      if (parentOrBlogId > 0) {
        pst.setLong(++i, parentOrBlogId);
      }
      if (type != null) {
        pst.setString(++i, type.name());
      }

      if (limit > 0) {
        pst.setInt(++i, offset);
        pst.setInt(++i, limit);
      }


      ResultSet rs = pst.executeQuery();
      while (rs.next()) {
        posts.add(createPost(rs));
      }


    } finally {
      if (conn != null) conn.close();
    }

    return posts;
  }


  public List<Post> getAdminPostsLatest(long parent, Post.PostType type, int limit, int offset) throws Exception {
    LinkedList<Post> posts = new LinkedList<Post>();

    Connection conn = null;

    try {
      conn = ds.getConnection();

      StringBuilder sb = new StringBuilder("select * from admin_posts_latest");

      if (parent > 0 || type != null) {
        sb.append(" where ");
      }

      if (parent > 0) {
        sb.append("parent = ?");
      }

      if (type != null) {
        if (parent > 0) {
          sb.append(" and ");
        }
        sb.append("type = ?");
      }

      if (limit > 0) {
        sb.append(" offset ? limit ?");
      }

      PreparedStatement pst = conn.prepareStatement(sb.toString());
      int i = 0;
      if (parent > 0) {
        pst.setLong(++i, parent);
      }
      if (type != null) {
        pst.setString(++i, type.name());
      }

      if (limit > 0) {
        pst.setInt(++i, offset);
        pst.setInt(++i, limit);
      }


      ResultSet rs = pst.executeQuery();
      while (rs.next()) {
        posts.add(createPost(rs));
      }


    } finally {
      if (conn != null) conn.close();
    }

    return posts;
  }

  public List<Post> getEventList(int daysPast, int daysFuture, Post.Status status) throws Exception {
    LinkedList<Post> posts = new LinkedList<Post>();

    Connection conn = null;

    try {
      conn = ds.getConnection();

      Calendar start = Calendar.getInstance();
      start.add(Calendar.DATE, daysPast * -1);
      Calendar end = Calendar.getInstance();
      end.add(Calendar.DATE, daysFuture);

      StringBuilder sb = new StringBuilder("select p.*, firstName || ' ' || lastName as author, a.id as attachmentId, filename, caption, thumbwidth, thumbheight, size, commentCount\n" +
          "from posts p \n" +
          "join members m on p.memberid = m.id \n" +
          "left join (select pa.postId, paa.attachmentId from post_attachments paa join (select postId, min(seq) as seq from post_attachments pa group by postId) pa on paa.seq = pa.seq and pa.postId = paa.postId) q\n" +
          "on q.postId = p.id\n" +
          "left join attachments a on a.id = q.attachmentId\n" +
          "left join (select postId, count(id) as commentCount from comments group by postId) cc on cc.postId = p.id\n" +
          "where p.type = 'event'\n" +
          "and startDate between ? and ?");

      if (status != null) {
        sb.append(" and p.status = ?");
      }
      sb.append(" order by startDate asc");

      PreparedStatement pst = conn.prepareStatement(sb.toString());
      pst.setTimestamp(1, new Timestamp(start.getTime().getTime()));
      pst.setTimestamp(2, new Timestamp(end.getTime().getTime()));

      if (status != null) {
        pst.setString(3, status.name());
      }

      ResultSet rs = pst.executeQuery();
      while (rs.next()) {
        posts.add(createPost(rs));
      }


    } finally {
      if (conn != null) conn.close();
    }

    return posts;
  }

  // Create draft clone
  public long createDraft(long versionId) throws Exception {
    long newVersionId = 0;

    Connection conn = null;
    try {
      conn = ds.getConnection();
      newVersionId = getSequence("versionId");
      conn.setAutoCommit(false);

      Statement st = conn.createStatement();

      StringBuilder sb = new StringBuilder("insert into posts (versionId");
      for (String column : postColumns) {
        if (!column.equalsIgnoreCase("versionId")) {
          sb.append(",").append(column);
        }
      }
      sb.append(") (select ");
      sb.append(newVersionId);
      for (String column : postColumns) {
        if (!column.equalsIgnoreCase("versionId")) {
          sb.append(",");
          if (column.equalsIgnoreCase("status")) {
            sb.append("'draft'");
          } else {
            sb.append(column);
          }
        }
      }
      sb.append(" from posts where versionId = ").append(versionId).append(")");

      st.executeUpdate(sb.toString());
      st.executeUpdate("update posts set updated = current_timestamp where versionId = " + newVersionId);

      st.executeUpdate("insert into post_attachments (versionId, attachmentId, seq, display) (select " + newVersionId + ", attachmentId, seq, display from post_attachments where versionId = " + versionId + ")");

      st.executeUpdate("insert into post_categories (versionId, categoryId) (select " + newVersionId + ", categoryId from post_categories where versionId = "+ versionId + ")");

      conn.commit();
    } finally {
      if (conn != null) {
        conn.setAutoCommit(true);
        conn.close();
      }
    }

    return newVersionId;
  }

  public void swapOrder(long a, long b) throws Exception {
    Connection conn = null;
    try {
      conn = ds.getConnection();
      PreparedStatement pst = conn.prepareStatement("select * from swap_order(?, ?)");
      pst.setLong(1, a);
      pst.setLong(2, b);
      pst.execute();
    } finally {
      if (conn != null) {
        conn.close();
      }
    }
  }

  public Post savePost(Post p) throws Exception {
    Connection conn = null;

    try {
      conn = ds.getConnection();
      conn.setAutoCommit(false);
      PreparedStatement ps;
      if (p.getId() == 0 || p.getVersionId() == 0) {
        p.setVersionId(getSequence("versionId"));
        if (p.getId() == 0) {
          p.setId(getSequence("postId"));
        }
        if (p.getPosted() == null) {
          p.setPosted(new Date());
        } else {
          p.setUpdated(new Date());
        }
        String sql = createInsert("posts", postColumns);
        ps = conn.prepareStatement(sql);
      } else {
        String sql = createUpdate("posts", postColumns, "versionId = ?");
        p.setUpdated(new Date());
        ps = conn.prepareStatement(sql);
        ps.setLong(postColumns.length + 1, p.getVersionId());
      }
      if (p.getSeqNo() == 0) {
        long next = getNextSeqno(p.getParent());
        p.setSeqNo(next);
      }
      int i = 0;
      ps.setLong(++i, p.getVersionId());
      ps.setLong(++i, p.getId());
      ps.setLong(++i, p.getSeqNo());
      if (p.getBlogId() > 0) {
        ps.setLong(++i, p.getBlogId());
      } else {
        ps.setNull(++i, Types.BIGINT);
      }
      if (p.getParent() > 0) {
        ps.setLong(++i, p.getParent());
      } else {
        ps.setNull(++i, Types.BIGINT);
      }
      ps.setString(++i, p.getStatus().name());
      ps.setLong(++i, p.getUserId());
      ps.setString(++i, p.getType().name());
      ps.setTimestamp(++i, new Timestamp(p.getPosted().getTime()));
      ps.setTimestamp(++i, new Timestamp(p.getUpdated().getTime()));
      if (p.getStart() != null) {
        ps.setTimestamp(++i, new Timestamp(p.getStart().getTime()));
      } else {
        ps.setNull(++i, Types.TIMESTAMP);
      }
      if (p.getEnd() != null) {
        ps.setTimestamp(++i, new Timestamp(p.getEnd().getTime()));
      } else {
        ps.setNull(++i, Types.TIMESTAMP);
      }
      ps.setString(++i, p.getTitle());
      ps.setString(++i, p.getExcerpt());
      ps.setString(++i, p.getBody());
      ps.setString(++i, p.getFormat().name());
      ps.setBoolean(++i, p.isExternal());
      ps.setBoolean(++i, p.isGallery());
      ps.setString(++i, p.getShortCode());
      ps.setBoolean(++i, p.isAllowComments());
      if (p.getIconId() > 0) {
        ps.setLong(++i, p.getIconId());
      } else {
        ps.setNull(++i, Types.BIGINT);
      }
      if (p.getBannerId() > 0) {
        ps.setLong(++i, p.getBannerId());
      } else {
        ps.setNull(++i, Types.BIGINT);
      }
      if (p.getBlogLink() > 0) {
        ps.setLong(++i, p.getBlogLink());
      } else {
        ps.setNull(++i, Types.BIGINT);
      }
      ps.setString(++i, p.getBannerBackground());

      ps.executeUpdate();

      if (p.getStatus() == Post.Status.published) {
        ps = conn.prepareStatement("update posts set status = ? where id = ? and versionId != ? and status != ?");
        ps.setString(1, Post.Status.obsolete.name());
        ps.setLong(2, p.getId());
        ps.setLong(3, p.getVersionId());
        ps.setString(4, Post.Status.deleted.name());
        ps.executeUpdate();
      }

      if (p.isUrlChanged()) {
        ps = conn.prepareStatement("delete from url_cache where url like ?");
        ps.setString(1, p.getUrl() + "%");
        ps.executeUpdate();
      }

      if (p.getBlogLink() > 0) {
        ps = conn.prepareStatement("update blogs set postId = ? where id = ?");
        ps.setLong(1, p.getId());
        ps.setLong(2, p.getBlogLink());
        ps.executeUpdate();
      }

      conn.commit();

    } finally {
      if (conn != null) {
        conn.setAutoCommit(true);
        conn.close();
      }
    }

    return p;
  }

  public long getNextSeqno(long parent) throws Exception {
    Connection conn = null;
    long seqno = 1;
    try {
      conn = ds.getConnection();
      String sql = "select max(seqno) as max from posts where parent " + ((parent > 0) ? "= " + parent : "is null");
      Statement st = conn.createStatement();
      ResultSet rs = st.executeQuery(sql);
      if (rs.next()) {
        seqno = rs.getLong("max") + 1;
      }
    } finally {
      if (conn != null) conn.close();
    }
    return seqno;
  }

  public Blog saveBlog(Blog blog) throws Exception {

    Connection conn = null;
    String columns[] = {"id", "name", "slug", "tagline", "banner", "owner"};
    try {
      conn = ds.getConnection();
      PreparedStatement ps;
      if (blog.getId() == 0 ) {
        blog.setId(getSequence("blogId"));
        String sql = createInsert("blogs", columns);
        ps = conn.prepareStatement(sql);
      } else {
        String sql = createUpdate("blogs", columns, "id = ?");
        ps = conn.prepareStatement(sql);
        ps.setLong(columns.length + 1, blog.getId());
      }
      int i = 0;
      ps.setLong(++i, blog.getId());
      ps.setString(++i, blog.getName());
      ps.setString(++i, blog.getSlug());
      ps.setString(++i, blog.getTagline());
      ps.setLong(++i, blog.getBanner());
      ps.setLong(++i, blog.getOwner());

      ps.executeUpdate();

    } finally {
      if (conn != null) {
        conn.close();
      }
    }

    return blog;
  }

  public void addBlogUser(long blogId, long userId) throws Exception {
    Connection conn = null;
    try {
      conn = ds.getConnection();
      PreparedStatement pst = conn.prepareStatement("insert into blogs_users (blogId, userId) values (?,?)");
      pst.setLong(1, blogId);
      pst.setLong(2, userId);
      pst.executeUpdate();
    } finally {
      if (conn != null) {
        conn.close();
      }
    }
  }

  public void removeBlogUser(long blogId, long userId) throws Exception {
    Connection conn = null;
    try {
      conn = ds.getConnection();
      PreparedStatement pst = conn.prepareStatement("delete from blogs_users where blogId = ? and userId = ?");
      pst.setLong(1, blogId);
      pst.setLong(2, userId);
      pst.executeUpdate();
    } finally {
      if (conn != null) {
        conn.close();
      }
    }
  }

  public List<Long> getBlogUsers(long blogId) throws Exception {
    Connection conn = null;
    LinkedList<Long> users = new LinkedList<Long>();
    try {
      conn = ds.getConnection();
      PreparedStatement pst = conn.prepareStatement("select userId from blogs_users where blogId = ?");
      pst.setLong(1, blogId);
      ResultSet rs = pst.executeQuery();
      while (rs.next()) {
        users.add(rs.getLong("userId"));
      }
    } finally {
      if (conn != null) {
        conn.close();
      }
    }
    return users;
  }

  public List<Blog> getBlogs(long userId) throws Exception {
    Connection conn = null;
    LinkedList<Blog> blogs = new LinkedList<Blog>();
    try {
      conn = ds.getConnection();
      PreparedStatement pst;
      if (userId >0) {
        pst = conn.prepareStatement("select * from blogs where blogId in (select blogId from blogs_users where userId = ?) or owner = ? order by lower(name)");
        pst.setLong(1, userId);
        pst.setLong(2, userId);
      } else {
        pst = conn.prepareStatement("select * from blogs order by lower(name)");
      }

      ResultSet rs = pst.executeQuery();
      while (rs.next()) {
        Blog blog = new Blog();
        blog.setId(rs.getLong("id"));
        blog.setName(rs.getString("name"));
        blog.setSlug(rs.getString("slug"));
        blog.setTagline(rs.getString("tagline"));
        blog.setBanner(rs.getLong("banner"));
        blog.setOwner(rs.getLong("owner"));
        blog.setPostId(rs.getLong("postId"));
        blogs.add(blog);
        blog.setIndexCount(rs.getLong("indexCount"));
      }
    } finally {
      if (conn != null) {
        conn.close();
      }
    }
    return blogs;
  }

  public Blog getBlog(long blogId) throws Exception {
    Connection conn = null;
    Blog blog = null;
    try {
      conn = ds.getConnection();
      PreparedStatement pst;
      pst = conn.prepareStatement("select b.*,\n" +
          "i.jsp as jsp_index,\n" +
          "e.jsp as jsp_post,\n" +
          "a.jsp as jsp_archive\n" +
          "from \tblogs b\n" +
          "left join blog_templates i on b.tpl_index = i.template\n" +
          "left join blog_templates e on b.tpl_post = e.template\n" +
          "left join blog_templates a on b.tpl_archive = a.template\n" +
          "where id = ?");
      pst.setLong(1, blogId);

      ResultSet rs = pst.executeQuery();
      if (rs.next()) {
        blog = new Blog();
        blog.setId(rs.getLong("id"));
        blog.setName(rs.getString("name"));
        blog.setSlug(rs.getString("slug"));
        blog.setTagline(rs.getString("tagline"));
        blog.setBanner(rs.getLong("banner"));
        blog.setOwner(rs.getLong("owner"));
        blog.setPostId(rs.getLong("postId"));
        blog.setIndexJsp(rs.getString("jsp_index"));
        blog.setEntryJsp(rs.getString("jsp_post"));
        blog.setArchiveJsp(rs.getString("jsp_archive"));
        blog.setIndexTemplate(rs.getString("tpl_index"));
        blog.setEntryTemplate(rs.getString("tpl_post"));
        blog.setArchiveTemplate(rs.getString("tpl_archive"));
        blog.setIndexCount(rs.getLong("indexCount"));
      }
    } finally {
      if (conn != null) {
        conn.close();
      }
    }
    return blog;
  }

  public Blog getBlog(String slug) throws Exception {
    Connection conn = null;
    Blog blog = null;
    try {
      conn = ds.getConnection();
      PreparedStatement pst;
      pst = conn.prepareStatement("select b.*,\n" +
          "i.jsp as jsp_index,\n" +
          "e.jsp as jsp_post,\n" +
          "a.jsp as jsp_archive\n" +
          "from \tblogs b\n" +
          "left join blog_templates i on b.tpl_index = i.template\n" +
          "left join blog_templates e on b.tpl_post = e.template\n" +
          "left join blog_templates a on b.tpl_archive = a.template where slug = ?");
      pst.setString(1, slug);

      ResultSet rs = pst.executeQuery();
      if (rs.next()) {
        blog = new Blog();
        blog.setId(rs.getLong("id"));
        blog.setName(rs.getString("name"));
        blog.setSlug(rs.getString("slug"));
        blog.setTagline(rs.getString("tagline"));
        blog.setBanner(rs.getLong("banner"));
        blog.setOwner(rs.getLong("owner"));
        blog.setPostId(rs.getLong("postId"));
        blog.setIndexJsp(rs.getString("jsp_index"));
        blog.setEntryJsp(rs.getString("jsp_post"));
        blog.setArchiveJsp(rs.getString("jsp_archive"));
        blog.setIndexTemplate(rs.getString("tpl_index"));
        blog.setEntryTemplate(rs.getString("tpl_post"));
        blog.setArchiveTemplate(rs.getString("tpl_archive"));
      }
    } finally {
      if (conn != null) {
        conn.close();
      }
    }
    return blog;
  }

  public Category addCategory(Category cat) throws Exception {
    Connection conn = null;
    try {
      cat.setId(getSequence("categoryId"));

      conn = ds.getConnection();
      PreparedStatement pst = conn.prepareStatement("insert into categories (id, blogId, category) values (?,?,?)");
      pst.setLong(1, cat.getId());
      if (cat.getBlogId() > 0) {
        pst.setLong(2, cat.getBlogId());
      } else {
        pst.setNull(2, Types.BIGINT);
      }
      pst.setString(3, cat.getCategory());
      pst.executeUpdate();
    } finally {
      if (conn != null) {
        conn.close();
      }
    }
    return cat;
  }

  public List<Category> listCategories(long blogId) throws Exception {
    Connection conn = null;
    LinkedList<Category> cats = new LinkedList<Category>();
    try {
      conn = ds.getConnection();
      PreparedStatement pst;
      if (blogId > 0) {
        pst = conn.prepareStatement("select * from categories where blogId = ? order by lower(category)");
        pst.setLong(1, blogId);
      } else {
        pst = conn.prepareStatement("select * from categories where blogId is null order by lower(category)");
      }

      ResultSet rs = pst.executeQuery();
      while (rs.next()) {
        Category cat = new Category();
        cat.setId(rs.getLong("id"));
        cat.setBlogId(rs.getLong("blogId"));
        cat.setCategory(rs.getString("category"));
        cats.add(cat);
      }
    } finally {
      if (conn != null) {
        conn.close();
      }
    }
    return cats;
  }

  public List<Category> getCategoriesForPost(long versionId) throws Exception {
    Connection conn = null;
    LinkedList<Category> cats = new LinkedList<Category>();
    try {
      conn = ds.getConnection();
      PreparedStatement pst;
      pst = conn.prepareStatement("select * from categories where id in (select categoryId from post_categories where versionId = ?) order by lower(category)");
      pst.setLong(1, versionId);

      ResultSet rs = pst.executeQuery();
      while (rs.next()) {
        Category cat = new Category();
        cat.setId(rs.getLong("id"));
        cat.setBlogId(rs.getLong("blogId"));
        cat.setCategory(rs.getString("category"));
        cats.add(cat);
      }
    } finally {
      if (conn != null) {
        conn.close();
      }
    }
    return cats;
  }

  public Comment saveComment(Comment c) throws Exception {
    Connection conn = null;
    String columns[] = {"id", "postid", "memberid", "comment"};
    try {
      conn = ds.getConnection();
      PreparedStatement ps;
      if (c.getId() == 0) {
        c.setId(getSequence("commentId"));
        String sql = createInsert("comments", columns);
        ps = conn.prepareStatement(sql);
      } else {
        String sql = createUpdate("comments", columns, "id = ?");
        ps = conn.prepareStatement(sql);
        ps.setLong(columns.length + 1, c.getId());
      }
      int i = 0;
      ps.setLong(++i, c.getId());
      ps.setLong(++i, c.getPostId());
      ps.setLong(++i, c.getAuthor());
      ps.setString(++i, c.getComment());

      ps.executeUpdate();

    } finally {
      if (conn != null) {
        conn.close();
      }
    }

    return c;
  }

  public List<Comment> getComments(long postId) throws Exception  {
    LinkedList<Comment> comments = new LinkedList<Comment>();
    Connection conn = null;

    try {
      conn = ds.getConnection();
      PreparedStatement pst = conn.prepareStatement("select c.*, firstName || ' ' || lastName as authorName from comments c join members m on memberId = m.id where postId = ? order by posted asc");
      pst.setLong(1, postId);

      ResultSet rs = pst.executeQuery();
      while (rs.next()) {
        Comment c = new Comment();
        c.setId(rs.getLong("id"));
        c.setPostId(rs.getLong("postId"));
        c.setAuthor(rs.getLong("memberId"));
        c.setAuthorName(rs.getString("authorName"));
        c.setPosted(rs.getTimestamp("posted"));
        c.setComment(rs.getString("comment"));
        comments.add(c);
      }


    } finally {
      if (conn != null) {
        conn.close();
      }
    }
    return comments;

  }

  public void addSubscription(long postId, long memberId) throws Exception{
    Connection conn = null;

    try {
      conn = ds.getConnection();
      PreparedStatement pst = conn.prepareStatement("insert into subscriptions (postId, memberId, deleteKey) values (?, ?, ?)");
      pst.setLong(1, postId);
      pst.setLong(2, memberId);
      pst.setString(3, UUID.randomUUID().toString());
      pst.execute();
    } finally {
      if (conn != null) {
        conn.close();
      }
    }
  }

  public void deleteSubscription(String deleteKey) throws Exception {
    Connection conn = null;

    try {
      conn = ds.getConnection();
      PreparedStatement pst = conn.prepareStatement("delete from subscriptions where deleteKey = ?");
      pst.setString(1, deleteKey);
      pst.execute();
    } finally {
      if (conn != null) {
        conn.close();
      }
    }
  }

  public void deleteSubscription(long postId, long memberId) throws Exception {
    Connection conn = null;

    try {
      conn = ds.getConnection();
      PreparedStatement pst = conn.prepareStatement("delete from subscriptions where postId = ? and memberId = ?");
      pst.setLong(1, postId);
      pst.setLong(2, memberId);
      pst.execute();
    } finally {
      if (conn != null) {
        conn.close();
      }
    }
  }

  public boolean isSubscribed(long postId, long memberId) throws Exception {
    Connection conn = null;
    boolean is = false;
    try {
      conn = ds.getConnection();
      PreparedStatement pst = conn.prepareStatement("select * from subscriptions where postId = ? and memberId = ?");
      pst.setLong(1, postId);
      pst.setLong(2, memberId);
      ResultSet rs = pst.executeQuery();
      is = rs.next();
    } finally {
      if (conn != null) {
        conn.close();
      }
    }
    return is;
  }

  public List<Subscriber> getSubscribers(long postId) throws Exception {
    Connection conn = null;
    LinkedList<Subscriber> subs = new LinkedList<Subscriber>();
    try {
      conn = ds.getConnection();
      PreparedStatement pst = conn.prepareStatement("select memberId, firstName, email, deleteKey from members join subscriptions on id = memberId where postId = ?");
      pst.setLong(1, postId);
      ResultSet rs = pst.executeQuery();
      while (rs.next()) {
        Subscriber s = new Subscriber();
        s.setMemberId(rs.getLong("memberId"));
        s.setFirstName(rs.getString("firstName"));
        s.setEmail(rs.getString("email"));
        s.setUnsubKey(rs.getString("deleteKey"));
        subs.add(s);
      }
    } finally {
      if (conn != null) {
        conn.close();
      }
    }
    return subs;
  }

  public long getPostId(String url) throws Exception {
    Connection conn = null;
    long id = 0;
    try {
      /*
      conn = ds.getConnection();
      PreparedStatement pst = conn.prepareStatement("select postId from url_cache where url = ?");
      pst.setString(1, url);
      ResultSet rs = pst.executeQuery();
      if (rs.next()) {
        return rs.getLong("postId");
      }
      */
      String u = url.trim();
      if (u.endsWith("/")) u = u.substring(0, u.length() - 1);
      u = u.substring(1);

      String[] parts = u.split("/");
      long parent = 0;
      for (int i = 0; i < parts.length; i++) {
        Post p = getPost(parts[i].toLowerCase(), parent);
        if (p != null) {
          id = p.getId();
          parent = p.getId();
        } else {
          break;
        }
      }

      /*pst = conn.prepareStatement("insert into url_cache (url, postId) values (?,?)");
      pst.setString(1, url);
      if (id > 0) {
        pst.setLong(2, id);
      } else {
        pst.setNull(2, Types.BIGINT);
      }
      try {
        pst.executeUpdate();
      } catch (SQLException e) {
        //ignore, race condition
        throw e;
      }
      */

    } finally {
      if (conn != null) conn.close();
    }
    return id;
  }

  public List<Post> getLivePostsForAttachment(long attachmentId) throws Exception {
    LinkedList<Post> posts = new LinkedList<Post>();
    Connection conn = null;
    try {
      conn = ds.getConnection();
      PreparedStatement pst = conn.prepareStatement("select * from live_posts where versionId in (select versionId from post_attachments where attachmentId = ?)");
      pst.setLong(1, attachmentId);
      ResultSet rs = pst.executeQuery();
      while (rs.next()) {
        posts.add(createPost(rs));
      }
    } finally {
      if (conn != null) {
        conn.close();
      }
    }
    return posts;

  }

}
