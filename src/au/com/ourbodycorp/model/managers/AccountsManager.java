package au.com.ourbodycorp.model.managers;

import au.com.ourbodycorp.Config;
import au.com.ourbodycorp.Util;
import au.com.ourbodycorp.model.*;

import java.math.BigDecimal;
import java.sql.*;
import java.util.*;
import java.util.Date;

public class AccountsManager extends ManagerBase {
  public Account createAccount(ResultSet rs) throws Exception {
    Account acct = new Account();

    acct.setCorpId(rs.getLong("corp_id"));
    acct.setId(rs.getLong("id"));
    acct.setParent(rs.getLong("parent"));
    acct.setName(rs.getString("name"));
    acct.setType(Account.Type.valueOf(rs.getString("type")));
    acct.setUnitId(rs.getLong("unit_id"));

    return acct;
  }

  public Balance createBalance(ResultSet rs) throws Exception {
    Balance bal = new Balance();

    bal.setAccountId(rs.getLong("acct_id"));
    bal.setYear(rs.getInt("year"));
    bal.setOpening(rs.getBigDecimal("opening"));
    bal.setClosing(rs.getBigDecimal("closing"));

    return bal;
  }

  public AccountEntry createEntry(ResultSet rs) throws Exception {
    AccountEntry entry =  new AccountEntry();
    entry.setId(rs.getLong("id"));
    entry.setAccountId(rs.getLong("acct_id"));
    entry.setYear(rs.getInt("year"));
    entry.setEffectiveDate(rs.getDate("effective"));
    entry.setEntryDate(rs.getDate("entry"));
    entry.setDescription(rs.getString("description"));
    entry.setAmount(rs.getBigDecimal("amount"));

    return entry;
  }

  public Budget createBudget(ResultSet rs) throws Exception {
    au.com.ourbodycorp.model.Budget budget = new au.com.ourbodycorp.model.Budget();

    budget.setAccountId(rs.getLong("acct_id"));
    budget.setYear(rs.getInt("year"));
    budget.setDescription(rs.getString("description"));
    budget.setAmount(rs.getBigDecimal("amount"));

    return budget;
  }

  public Invoice createInvoice(ResultSet rs) throws Exception {
    Invoice i = new Invoice();
    i.setId(rs.getLong("id"));
    i.setCorpId(rs.getLong("corp_id"));
    i.setNumber(rs.getLong("num"));
    i.setCreated(rs.getTimestamp("created"));
    i.setDue(rs.getTimestamp("due"));
    i.setUnitId(rs.getLong("unit"));
    i.setUserId(rs.getLong("user_id"));
    i.setCompany(rs.getString("company"));
    i.setName(rs.getString("name"));
    i.setAddress(rs.getString("address"));
    i.setSent(rs.getTimestamp("sent"));
    i.setPaid(rs.getTimestamp("paid"));
    i.setTotal(rs.getBigDecimal("total"));
    i.setComplete(rs.getBoolean("complete"));
    i.setYear(rs.getInt("year"));
    i.setBudgetItem(rs.getLong("budget_item"));
    return i;
  }

  public InvoiceItem createInvoiceItem(ResultSet rs) throws Exception {
    InvoiceItem i =new InvoiceItem();
    i.setId(rs.getLong("id"));
    i.setInvoiceId(rs.getLong("invoice"));
    i.setSequence(rs.getInt("seq"));
    i.setQuantity(rs.getFloat("qty"));
    i.setPrice(rs.getBigDecimal("price"));
    i.setItem(rs.getString("item"));
    return i;
  }

  private void addChildren(List<Account> list, HashMap<Long, LinkedList<Account>> map, List<Account> children, int depth) {
    Collections.sort(children);
    for (Account child : children) {
      child.setDepth(depth);
      list.add(child);
      List<Account> more = map.get(child.getId());
      if (more != null && more.size() > 0) {
        addChildren(list, map, more, depth + 1);
      }
    }
  }

  public List<Account> getAccountHierarchy(long corpId) throws Exception {
    return getAccountHierarchy(corpId, null);
  }

  public List<Account> getAccountHierarchy(long corpId, Account.Type type) throws Exception {

    HashMap<Long, LinkedList<Account>> map = new HashMap<Long, LinkedList<Account>>();
    LinkedList<Account> accounts = new LinkedList<Account>();
    Connection conn = null;
    try {
      conn = ds.getConnection();
      PreparedStatement pst;
      if (type == null) {
        pst = conn.prepareStatement("select * from accounts left join balances on id = acct_id where corp_id = ?");
      } else {
        pst = conn.prepareStatement("select * from accounts left join balances on id = acct_id where corp_id = ? and type = ?");
        pst.setString(2, type.name());
      }
      pst.setLong(1, corpId);
      ResultSet rs = pst.executeQuery();
      while (rs.next()) {
        Account a = createAccount(rs);
        a.setBalance(createBalance(rs));
        LinkedList<Account> pl = map.get(a.getParent());
        if (pl == null) {
          pl = new LinkedList<Account>();
          map.put(a.getParent(), pl);
        }
        pl.add(a);
      }
      rs.close();

      List<Account> root = map.get(0L);
      if (root != null && root.size() > 0) {
        addChildren(accounts, map, root, 0);
      }

    } finally {
      if (conn != null) {
        conn.close();
      }
    }

    return accounts;
  }

  public Balance save(Balance b) throws Exception {
    String[] columns = {"acct_id", "year", "opening", "closing"};

    Connection conn = null;
    try {
      conn = ds.getConnection();
      conn.setAutoCommit(false);
      PreparedStatement pst;
      Balance test = getBalance(b.getAccountId(), b.getYear());
      if (test != null) {
        pst = conn.prepareStatement(createUpdate("balances", columns, "acct_id = ? and year = ?"));
        pst.setLong(columns.length + 1, b.getAccountId());
        pst.setInt(columns.length + 2, b.getYear());
      } else {
        pst = conn.prepareStatement(createInsert("balances", columns));
      }
      int i = 0;
      pst.setLong(++i, b.getAccountId());
      pst.setInt(++i, b.getYear());
      pst.setBigDecimal(++i, b.getOpening());
      pst.setBigDecimal(++i, b.getClosing());
      pst.execute();
      if (test != null) {
        updateClosingBalance(conn,b.getYear(), b.getAccountId());
      }
      conn.commit();
    } finally {
      if (conn != null) {
        conn.setAutoCommit(true);
        conn.close();
      }
    }
    return b;
  }

  public Account save(Account a) throws Exception {
    String[] columns = {"corp_id", "id", "parent", "type", "name", "unit_id"};
    Connection conn = null;
    try {
      conn = ds.getConnection();
      PreparedStatement pst;
      if (a.getId() == 0) {
        a.setId(getSequence("accountId"));
        pst = conn.prepareStatement(createInsert("accounts", columns));
      } else {
        pst = conn.prepareStatement(createUpdate("accounts", columns, "id = ?"));
        pst.setLong(columns.length + 1, a.getId());
      }
      pst.setLong(1, a.getCorpId());
      pst.setLong(2, a.getId());
      if (a.getParent() == 0) {
        pst.setNull(3, Types.BIGINT);
      } else {
        pst.setLong(3, a.getParent());
      }
      pst.setString(4, a.getType().name());
      pst.setString(5, a.getName());
      if (a.getUnitId() == 0) {
        pst.setNull(6, Types.BIGINT);
      } else {
        pst.setLong(6, a.getUnitId());
      }
      pst.executeUpdate();

    } finally {
      if (conn != null) {
        conn.close();
      }
    }
    return a;
  }

  public void deleteAccount(long id) throws Exception {
    Connection conn = null;
    try {
      conn = ds.getConnection();
      conn.setAutoCommit(false);

      PreparedStatement pst = conn.prepareStatement("delete from budget where acct_id = ?");
      pst.setLong(1, id);
      pst.executeUpdate();
      pst.close();

      pst = conn.prepareStatement("delete from acct_entries where acct_id = ?");
      pst.setLong(1, id);
      pst.executeUpdate();
      pst.close();

      pst = conn.prepareStatement("delete from balances where acct_id = ?");
      pst.setLong(1, id);
      pst.executeUpdate();
      pst.close();

      pst = conn.prepareStatement("delete from accounts where id = ?");
      pst.setLong(1, id);
      pst.executeUpdate();
      pst.close();

      conn.commit();
    } finally {
      if (conn != null) {
        conn.setAutoCommit(true);
        conn.close();
      }
    }
  }

  public AccountEntry getEntry(long id) throws Exception {
    AccountEntry entry = null;
    Connection conn = null;
    try {
      conn = ds.getConnection();
      PreparedStatement pst = conn.prepareStatement("select * from acct_entries where is = ?");
      pst.setLong(1, id);
      ResultSet rs = pst.executeQuery();
      if (rs.next()) {
        entry =  createEntry(rs);
      }
    } finally {
      if (conn != null) {
        conn.close();
      }
    }
    return entry;
  }

  public void addEntry(AccountEntry entry, Connection conn) throws Exception {
    LinkedList<AccountEntry> el = new LinkedList<AccountEntry>();
    el.add(entry);
    addEntries(el, conn);
  }

  public void addEntries(List<AccountEntry> entries, Connection conn) throws Exception {
    String[] columns = {"acct_id", "year", "entry", "effective", "description", "amount", "id", "budget_item"};

    PreparedStatement pst = conn.prepareStatement(createInsert("acct_entries", columns));
    for (AccountEntry entry : entries) {
      entry.setId(getSequence("acctEntryId"));
      pst.setLong(1, entry.getAccountId());
      pst.setInt(2, entry.getYear());
      pst.setTimestamp(3, new Timestamp(entry.getEntryDate().getTime()));
      pst.setTimestamp(4, new Timestamp(entry.getEffectiveDate().getTime()));
      pst.setString(5, entry.getDescription());
      pst.setBigDecimal(6, entry.getAmount());
      pst.setLong(7, entry.getId());
      if (entry.getBudgetItem() != 0) {
        pst.setLong(8, entry.getBudgetItem());
      } else {
        pst.setNull(8, Types.BIGINT);
      }
      pst.execute();
      updateClosingBalance(conn, entry.getYear(), entry.getAccountId());
    }
    pst.close();
  }

  public void addEntries(List<AccountEntry> entries) throws Exception {
    String[] columns = {"acct_id", "year", "entry", "effective", "description", "amount", "id", "budget_item"};
    Connection conn = null;
    try {
      conn = ds.getConnection();
      conn.setAutoCommit(false);
      PreparedStatement pst = conn.prepareStatement(createInsert("acct_entries", columns));
      for (AccountEntry entry : entries) {
        entry.setId(getSequence("acctEntryId"));
        pst.setLong(1, entry.getAccountId());
        pst.setInt(2, entry.getYear());
        pst.setTimestamp(3, new Timestamp(entry.getEntryDate().getTime()));
        pst.setTimestamp(4, new Timestamp(entry.getEffectiveDate().getTime()));
        pst.setString(5, entry.getDescription());
        pst.setBigDecimal(6, entry.getAmount());
        pst.setLong(7, entry.getId());
        if (entry.getBudgetItem() != 0) {
          pst.setLong(8, entry.getBudgetItem());
        } else {
          pst.setNull(8, Types.BIGINT);
        }
        pst.execute();
        updateClosingBalance(conn, entry.getYear(), entry.getAccountId());
        if (entry.getBudgetItem() != 0) {
          updateBudgetBalance(conn, entry.getYear(), entry.getBudgetItem(), entry.getAmount());
        }
      }
      conn.commit();
    } finally {
      if (conn != null) {
        conn.setAutoCommit(true);
        conn.close();
      }
    }
  }

  public void deleteEntry(AccountEntry entry) throws Exception {
    Connection conn = null;
    try {
      conn = ds.getConnection();
      conn.setAutoCommit(false);
      PreparedStatement pst = conn.prepareStatement("delete from acct_entries where id = ?");
      pst.setLong(1, entry.getId());
      int rows = pst.executeUpdate();
      updateClosingBalance(conn, entry.getYear(), entry.getAccountId());
      conn.commit();
    } finally {
      if (conn != null) {
        conn.setAutoCommit(true);
        conn.close();
      }
    }
  }

  public void updateBudgetBalance(Connection conn, int year, long budgetItem, BigDecimal amount) throws Exception {
    PreparedStatement pst = conn.prepareStatement("update budget_item set balance = balance + ? where id = ? and year = ?");
    pst.setBigDecimal(1, amount);
    pst.setLong(2, budgetItem);
    pst.setInt(3, year);
    pst.executeUpdate();
    pst.close();
  }
  /*
  public void updateBudgetBalance(Connection conn, int year, long budgetItem) throws Exception {
    PreparedStatement pst = conn.prepareStatement("update budget_item set balance = (select sum(amount) " +
        "from acct_entries e join accounts a on acct_id = a.id where year = ? and corp_id = (select corp_id from budget_groups where id = (select group_id from budget_item where id = ?)) " +
        "and budget_item = ?) where id = ? and year = ?");
    pst.setInt(1, year);
    pst.setLong(2, budgetItem);
    pst.setLong(3, budgetItem);
    pst.setLong(4, budgetItem);
    pst.setInt(5, year);
    pst.executeUpdate();
    pst.close();
  }
  */

  public void updateClosingBalance(Connection conn, int year, long accountId) throws Exception {
    // Need to do test or balance will become NULL if no entries!
    PreparedStatement test = conn.prepareStatement("select count(*) as cnt from acct_entries where acct_id = ? and year = ?");
    test.setLong(1, accountId);
    test.setInt(2, year);
    ResultSet rs = test.executeQuery();
    if (rs.next() && rs.getInt("cnt") > 0) {
      PreparedStatement pst = conn.prepareStatement("update balances set closing = opening + (select sum(amount) from acct_entries where acct_id = ? and year = ?) where acct_id = ? and year = ?");
      pst.setLong(1, accountId);
      pst.setInt(2, year);
      pst.setLong(3, accountId);
      pst.setInt(4, year);
      pst.executeUpdate();
      pst.close();
    } else {
      PreparedStatement pst = conn.prepareStatement("update balances set closing = opening where acct_id = ? and year = ?");
      pst.setLong(1, accountId);
      pst.setInt(2, year);
      pst.executeUpdate();
      pst.close();
    }
    test.close();

  }

  public Budget save(Budget budget) throws Exception {
    String[] cols = {"id", "acct_id", "year", "description", "amount"};
    Connection conn = null;
    try {
      conn = ds.getConnection();
      PreparedStatement pst;
      if (budget.getId() == 0) {
        pst = conn.prepareStatement(createInsert("budget", cols));
      } else {
        pst = conn.prepareStatement(createUpdate("budget", cols, "id = ?"));
        pst.setLong(cols.length + 1, budget.getId());
      }
      pst.setLong(1, budget.getId());
      pst.setLong(2, budget.getAccountId());
      pst.setInt(3, budget.getYear());
      pst.setString(4, budget.getDescription());
      pst.setBigDecimal(5, budget.getAmount());
      pst.executeUpdate();
    } finally {
      if (conn != null) {
        conn.close();
      }
    }
    return budget;
  }

  public void deleteBudget(long id) throws Exception {
    Connection conn = null;
    try {
      conn = ds.getConnection();
      PreparedStatement pst = conn.prepareStatement("delete from budget where id = ?");
      pst.setLong(1, id);
      pst.execute();
    } finally {
      if (conn != null) {
        conn.close();
      }
    }
  }

  public HashMap<Long, Budget> getBudgetMap(long corpId, int year) throws Exception {
    HashMap<Long, Budget> map = new HashMap<Long, Budget>();
    Connection conn = null;
    try {
      conn = ds.getConnection();
      PreparedStatement pst = conn.prepareStatement("select * from budget where year = ? and acct_id in (select id from accounts where corp_id = ?)");
      pst.setInt(1, year);
      pst.setLong(2, corpId);
      ResultSet rs = pst.executeQuery();
      while (rs.next()) {
        Budget b = createBudget(rs);
        map.put(b.getAccountId(), b);
      }
    } finally {
      if (conn != null) {
        conn.close();
      }
    }
    return map;
  }

  public List<Integer> availableYears(long corpId) throws Exception {
    LinkedList<Integer> years = new LinkedList<Integer>();

    Connection conn = null;
    try {
      conn = ds.getConnection();
      PreparedStatement pst = conn.prepareStatement("select distinct year from balances where acct_id in (select id from accounts where corp_id = ?) order by year desc");
      ResultSet rs = pst.executeQuery();
      while (rs.next()) {
        years.add(rs.getInt("year"));
      }
    } finally {
      if (conn != null) {
        conn.close();
      }
    }

    return years;
  }

  public void bootStrap(long corpId, int month, int year) throws Exception {
    Connection conn = null;
    try {
      conn = ds.getConnection();
      conn.setAutoCommit(false);

      PreparedStatement pst;


      pst = conn.prepareStatement("update corporation set sofy = ?, fy = ?, invoiceNum = 0 where id = ?");
      pst.setInt(1, month);
      pst.setInt(2, year);
      pst.setLong(3, corpId);
      pst.executeUpdate();
      pst.close();

      String[] columns = {"corp_id", "id", "parent", "type", "name", "unit_id"};
      LinkedList<Account> as = new LinkedList<Account>();


      long id = getSequence("accountId");
      long parent = id;

      as.add(new Account(id, corpId, 0, Account.Type.grp, "Units"));
      parent = id;

      CorporationManager cm = new CorporationManager();
      List<Unit> units = cm.getUnitsForCorp(corpId);
      for (Unit unit : units) {
        id = getSequence("accountId");
        as.add(new Account(id, corpId, parent, Account.Type.unt, unit.getName(), unit.getId()));
      }

      id = getSequence("accountId");
      long bankGroup = id;
      as.add(new Account(id, corpId, 0, Account.Type.grp, "Bank"));

      id = getSequence("accountId");
      as.add(new Account(id, corpId, bankGroup, Account.Type.bnk, "Admin Account"));

      id = getSequence("accountId");
      as.add(new Account(id, corpId, bankGroup, Account.Type.bnk, "Sinking Fund Account"));

      //Budget items
      LinkedList<BudgetGroup> groups = new LinkedList<BudgetGroup>();
      LinkedList<BudgetItem> items = new LinkedList<BudgetItem>();

      id = getSequence("budgetGroupId");
      parent = id;
      groups.add(new BudgetGroup(id, corpId, 1, "Income"));
      items.add(new BudgetItem(getSequence("budgetItemId"), year, "c", parent, "Strata Fees"));
      items.add(new BudgetItem(getSequence("budgetItemId"), year, "c", parent, "Special Levies"));
      items.add(new BudgetItem(getSequence("budgetItemId"), year, "c", parent, "Sinking Fund"));

      id = getSequence("budgetGroupId");
      parent = id;
      groups.add(new BudgetGroup(id, corpId, 2, "Admin"));

      items.add(new BudgetItem(getSequence("budgetItemId"), year, "d", parent, "Accounting"));
      items.add(new BudgetItem(getSequence("budgetItemId"), year, "d", parent, "Audit"));
      items.add(new BudgetItem(getSequence("budgetItemId"), year, "d", parent, "Bank Fees"));
      items.add(new BudgetItem(getSequence("budgetItemId"), year, "d", parent, "Our Body Corp"));
      items.add(new BudgetItem(getSequence("budgetItemId"), year, "d", parent, "Miscellaneous"));

      id = getSequence("budgetGroupId");
      parent = id;
      groups.add(new BudgetGroup(id, corpId, 3, "Maintenance"));
      items.add(new BudgetItem(getSequence("budgetItemId"), year, "d", parent, "Cleaning"));
      items.add(new BudgetItem(getSequence("budgetItemId"), year, "d", parent, "Electrical"));
      items.add(new BudgetItem(getSequence("budgetItemId"), year, "d", parent, "Fire System"));
      items.add(new BudgetItem(getSequence("budgetItemId"), year, "d", parent, "Grounds"));
      items.add(new BudgetItem(getSequence("budgetItemId"), year, "d", parent, "Miscellaneous"));


      id = getSequence("budgetGroupId");
      parent = id;
      groups.add(new BudgetGroup(id, corpId, 4, "Utilities"));
      items.add(new BudgetItem(getSequence("budgetItemId"), year, "d", parent, "Electricity"));
      items.add(new BudgetItem(getSequence("budgetItemId"), year, "d", parent, "Gas"));
      items.add(new BudgetItem(getSequence("budgetItemId"), year, "d", parent, "Water"));
      items.add(new BudgetItem(getSequence("budgetItemId"), year, "d", parent, "Miscellaneous"));




      String[] bcols= {"acct_id", "year", "opening", "closing"};
      PreparedStatement bst = conn.prepareStatement(createInsert("balances", bcols));

      pst = conn.prepareStatement(createInsert("accounts", columns));
      for (Account a : as) {
        pst.setLong(1, a.getCorpId());
        pst.setLong(2, a.getId());
        if (a.getParent() == 0) {
          pst.setNull(3, Types.BIGINT);
        } else {
          pst.setLong(3, a.getParent());
        }
        pst.setString(4, a.getType().name());
        pst.setString(5, a.getName());
        if (a.getUnitId() > 0) {
          pst.setLong(6, a.getUnitId());
        } else {
          pst.setNull(6, Types.BIGINT);
        }
        pst.executeUpdate();

        bst.setLong(1, a.getId());
        bst.setInt(2, year);
        bst.setBigDecimal(3, new BigDecimal(0));
        bst.setBigDecimal(4, new BigDecimal(0));
        bst.executeUpdate();
      }
      pst.close();
      bst.close();

      pst = conn.prepareStatement("insert into budget_groups (id, corp_id, seq, title) values (?, ?, ?, ?)");
      bst = conn.prepareStatement("insert into budget_item (id, year, type, group_id, title, planned, balance) values (?,?,?,?,?,0.0,0.0)");
      for (BudgetGroup group : groups) {
        pst.setLong(1, group.getId());
        pst.setLong(2, group.getCorpId());
        pst.setInt(3, group.getSeq());
        pst.setString(4, group.getTitle());
        pst.execute();
      }
      pst.close();

      for (BudgetItem item : items) {
        bst.setLong(1, item.getId());
        bst.setInt(2, item.getYear());
        bst.setString(3, item.getType());
        bst.setLong(4, item.getGroupId());
        bst.setString(5, item.getTitle());
        bst.execute();
      }
      bst.close();

      conn.commit();
    } finally {
      if (conn != null) {
        conn.setAutoCommit(true);
        conn.close();
      }
    }
  }

  public long nextInvoiceNum(long corpId, Connection conn) throws Exception {
    long num = 0;

      PreparedStatement update = conn.prepareStatement("update corporation set invoiceNum = invoiceNum + 1 where id = ?");
      update.setLong(1, corpId);
      update.executeUpdate();
      update.close();

      PreparedStatement get = conn.prepareStatement("select invoiceNum from corporation where id = ?");
      get.setLong(1, corpId);
      ResultSet rs = get.executeQuery();
      if (rs.next()) {
        num = rs.getLong("invoiceNum");
      } else {
        throw new Exception("No invoice ID found, WTF?");
      }
      rs.close();
      get.close();

    return num;
  }

  public long nextInvoiceNum(long corpId) throws Exception {
    long num = 0;
    Connection conn = null;
    try {
      conn = ds.getConnection();
      conn.setAutoCommit(false);

      PreparedStatement update = conn.prepareStatement("update corporation set invoiceNum = invoiceNum + 1 where id = ?");
      update.setLong(1, corpId);
      update.executeUpdate();
      update.close();

      PreparedStatement get = conn.prepareStatement("select invoiceNum from corporation where id = ?");
      get.setLong(1, corpId);
      ResultSet rs = get.executeQuery();
      if (rs.next()) {
        num = rs.getLong("invoiceNum");
      } else {
        throw new Exception("No invoice ID found, WTF?");
      }
      rs.close();
      get.close();
      conn.commit();
    } finally {
      if (conn != null) {
        conn.setAutoCommit(true);
        conn.close();
      }
    }

    return num;
  }

  public Balance getBalance(long id, long fy) throws Exception {
    Balance b = null;
    Connection conn = null;
    try {
      conn = ds.getConnection();
      PreparedStatement pst = conn.prepareStatement("select * from balances where acct_id = ? and year = ?");
      pst.setLong(1, id);
      pst.setLong(2, fy);
      ResultSet rs= pst.executeQuery();
      if (rs.next()) {
        b = createBalance(rs);
      }
    } finally {
      if (conn != null) {
        conn.close();
      }
    }
    return b;
  }

  public Account getAccount(long id) throws Exception {
    Account a = null;
    Connection conn = null;
    try {
      conn = ds.getConnection();
      PreparedStatement pst = conn.prepareStatement("select * from accounts where id = ?");
      pst.setLong(1, id);
      ResultSet rs = pst.executeQuery();
      if (rs.next()) {
        a = createAccount(rs);
      }
    } finally {
      if (conn != null) {
        conn.close();
      }
    }
    return a;
  }

  public Account getAccountForUnit(long unitId) throws Exception {
    Account a = null;
    Connection conn = null;
    try {
      conn = ds.getConnection();
      PreparedStatement pst = conn.prepareStatement("select * from accounts where unit_id = ?");
      pst.setLong(1, unitId);
      ResultSet rs = pst.executeQuery();
      if (rs.next()) {
        a = createAccount(rs);
      }
    } finally {
      if (conn != null) {
        conn.close();
      }
    }
    return a;
  }

  public List<Account> getAccounts(Long corpId, Account.Type type) throws Exception {
    LinkedList<Account> accounts = new LinkedList<Account>();
    Connection conn = null;
    try {
      conn = ds.getConnection();
      PreparedStatement pst = conn.prepareStatement("select * from accounts a join balances b on a.id = b.acct_id where corp_id = ? and type = ? order by lower(name) asc");
      pst.setLong(1, corpId);
      pst.setString(2, type.name());
      ResultSet rs = pst.executeQuery();
      while (rs.next()) {
        Account a = createAccount(rs);
        a.setBalance(createBalance(rs));
        accounts.add(a);
      }
    } finally {
      if (conn != null) {
        conn.close();
      }
    }
    return accounts;
  }

  public List<AccountEntry> getAccountEntries(long id, int offset, int count) throws Exception {
    List<AccountEntry> entries = new LinkedList<AccountEntry>();
    Connection conn = null;
    try {
      conn = ds.getConnection();
      String sql = "select * from acct_entries where acct_id = ? order by entry desc";
      if (count > 0) {
        sql += " limit " + count;

        if (offset > 0) {
          sql += " offset " + offset;
        }
      }


      PreparedStatement pst = conn.prepareStatement(sql);
      pst.setLong(1, id);
      ResultSet rs = pst.executeQuery();
      while (rs.next()) {
        entries.add(createEntry(rs));
      }
    } finally {
      if (conn != null) {
        conn.close();
      }
    }
    return entries;
  }

  public long getUnitParentId(long corpId) throws Exception {
    long id = 0;
    Connection conn = null;
    try {
      conn = ds.getConnection();
      PreparedStatement pst = conn.prepareStatement("select id from accounts where type = ? and name = ?");
      pst.setString(1, Account.Type.grp.name());
      pst.setString(2, "Units");
      ResultSet rs = pst.executeQuery();
      if (rs.next()) {
        id = rs.getLong("id");
      }
    } finally {
      if (conn != null) {
        conn.close();
      }
    }
    return id;
  }

  public void payInvoice(long id, long corpId, Date date, long budgetItem, int year, BigDecimal amount) throws Exception {
    Connection conn = ds.getConnection();
    try {
      conn.setAutoCommit(false);
      PreparedStatement pst = conn.prepareStatement("update invoices set paid = ? where id = ? and corp_id = ?");
      pst.setTimestamp(1, new Timestamp(date.getTime()));
      pst.setLong(2, id);
      pst.setLong(3, corpId);
      if (pst.executeUpdate() > 0) {
        updateBudgetBalance(conn, year, budgetItem, amount);
      } else {
        throw new Exception("Non existing invoice (" + id + "," + corpId + ")");
      }
      conn.commit();
    } finally {
      conn.setAutoCommit(true);
      conn.close();
    }
  }

  public void save(List<Invoice> invoices) throws Exception {
    String[] cols = {"id", "corp_id", "num", "created", "due", "unit", "user_id", "company", "name",
                    "address", "sent", "paid", "total", "complete", "year", "budget_item"};
    String[] dCols = {"id", "invoice", "seq", "qty", "price", "item"};

    Connection conn = null;
    try {
      conn = ds.getConnection();
      conn.setAutoCommit(false);
      PreparedStatement pst = conn.prepareStatement(createInsert("invoices", cols));
      PreparedStatement iPst = conn.prepareStatement(createInsert("invoice_items", dCols));
      PreparedStatement unitPst = conn.prepareStatement("select id from accounts where unit_id = ?");
      LinkedList<AccountEntry> entries = new LinkedList<AccountEntry>();
      int inv = 0;
      int itm = 0;
      for (Invoice i : invoices) {
        System.out.println("inv: " + inv++);
        i.setId(getSequence("invoiceId"));
        System.out.println("ID: " + i.getId());
        i.setNumber(nextInvoiceNum(i.getCorpId(), conn));
        System.out.println("inv" + i.getNumber());
        pst.setLong(1, i.getId());
        pst.setLong(2, i.getCorpId());
        pst.setLong(3, i.getNumber());
        pst.setTimestamp(4, new Timestamp(i.getCreated().getTime()));
        if (i.getDue() != null) {
          pst.setTimestamp(5, new Timestamp(i.getDue().getTime()));
        } else {
          pst.setNull(5, Types.TIMESTAMP);
        }
        if (i.getUnitId() > 0) {
          pst.setLong(6, i.getUnitId());
        } else {
          pst.setNull(6, Types.BIGINT);
        }
        if (i.getUserId() > 0) {
          pst.setLong(7, i.getUserId());
        } else {
          pst.setNull(7, Types.BIGINT);
        }
        pst.setString(8, i.getCompany());
        pst.setString(9, i.getName());
        pst.setString(10, i.getAddress());
        if (i.getSent() != null) {
          pst.setTimestamp(11, new Timestamp(i.getSent().getTime()));
        } else {
          pst.setNull(11, Types.TIMESTAMP);
        }
        if (i.getPaid() != null) {
          pst.setTimestamp(12, new Timestamp(i.getPaid().getTime()));
        } else {
          pst.setNull(12, Types.TIMESTAMP);
        }
        pst.setBigDecimal(13, i.getTotal());
        pst.setBoolean(14, i.isComplete());
        pst.setInt(15, i.getYear());
        if (i.getBudgetItem() != 0) {
          pst.setLong(16, i.getBudgetItem());
        } else {
          pst.setNull(16, Types.BIGINT);
        }
        pst.execute();

        List<InvoiceItem> items = i.getItems();
        for (InvoiceItem item : items) {
          System.out.println("itm: " + itm++);
          item.setInvoiceId(i.getId());
          item.setId(getSequence("invoiceItemId"));
          iPst.setLong(1, item.getId());
          iPst.setLong(2, item.getInvoiceId());
          iPst.setInt(3, item.getSequence());
          iPst.setDouble(4, item.getQuantity());
          iPst.setBigDecimal(5, item.getPrice());
          iPst.setString(6, item.getItem());
          iPst.execute();
        }

        if (i.getUnitId() > 0) {
          unitPst.setLong(1, i.getUnitId());
          ResultSet rs = unitPst.executeQuery();
          if (rs.next()) {
            AccountEntry entry = new AccountEntry();
            entry.setAccountId(rs.getLong("id"));
            entry.setAmount(i.getTotal());
            entry.setDescription(i.getName());
            entry.setEffectiveDate(new Date());
            entry.setEntryDate(new Date());
            entry.setYear(i.getYear());
            entry.setBudgetItem(0);
            entries.add(entry);
          }
        }

      }
      if (entries.size() > 0) {
        addEntries(entries, conn);
      }

      conn.commit();
    } finally {
      if (conn != null) {
        conn.setAutoCommit(true);
        conn.close();
      }
    }
  }

  public long countInvoices(long corpId, String type) throws Exception {
    Connection conn = null;
    long count = 0;
    try {
      conn = ds.getConnection();
      PreparedStatement pst;
      String order = "created desc";
      String sql = "select count(*) as cnt from invoices where corp_id = ?";
      if (type.equals("unpaid")) {
        sql += " and paid is null";
      } else if (type.equals("overdue")) {
        sql += " and due < current_timestamp";
      } else if (type.equals("paid")) {
        sql += " and paid is not null";
      } else if (type.equals("unsent")) {
        sql += " and sent is null";
      } else if (type.equals("sent")) {
        sql += " and sent is not null";
      } else if (type.equals("incomplete")) {
        sql += " and complete = 'f'";
      }

      pst = conn.prepareStatement(sql);
      pst.setLong(1, corpId);
      ResultSet rs = pst.executeQuery();
      if (rs.next()) {
        count = rs.getLong("cnt");
      }
    } finally {
      if (conn != null) {
        conn.close();
      }
    }
    return count;
  }

  public List<Invoice> getInvoices(long corpId, String type, long offset, long limit) throws Exception {
    Connection conn = null;
    LinkedList<Invoice> invoices = new LinkedList<Invoice>();
    try {
      conn = ds.getConnection();
      PreparedStatement pst;
      String order = "created desc";
      String sql = "select * from invoices where corp_id = ?";
      if (type.equals("unpaid")) {
        sql += " and paid is null";
      } else if (type.equals("overdue")) {
        sql += " and due < current_timestamp";
      } else if (type.equals("paid")) {
        sql += " and paid is not null";
      } else if (type.equals("unsent")) {
        sql += " and sent is null";
      } else if (type.equals("sent")) {
        sql += " and sent is not null";
      } else if (type.equals("incomplete")) {
        sql += " and complete = 'f'";
      }

      sql += " order by " + order;

      if (limit > 0) {
        sql += " limit " + limit;
      }
      if (offset > 0) {
        sql += " offset " + offset;
      }

      pst = conn.prepareStatement(sql);
      pst.setLong(1, corpId);
      ResultSet rs = pst.executeQuery();
      while (rs.next()) {
        invoices.add(createInvoice(rs));
      }
    } finally {
      if (conn != null) {
        conn.close();
      }
    }
    return invoices;
  }

  public void markPaid(Invoice invoice, Date payDate, int year, long account) throws Exception {
    Connection conn = ds.getConnection();
    try {
      PreparedStatement pst = conn.prepareStatement("update invoices set paid = ? where id = ?");
      pst.setTimestamp(1, new Timestamp(payDate.getTime()));
      pst.setLong(2, invoice.getId());
      pst.executeUpdate();
      pst.close();
      if (invoice.getBudgetItem() > 0) {
        updateBudgetBalance(conn, year, invoice.getBudgetItem(), invoice.getTotal().abs());
      }

      if (invoice.getUnitId() > 0) {
        PreparedStatement unitPst = conn.prepareStatement("select id from accounts where unit_id = ?");
        unitPst.setLong(1, invoice.getUnitId());
        ResultSet rs = unitPst.executeQuery();
        long acctId = 0;
        if (rs.next()) {
          acctId = rs.getLong("id");
        }
        rs.close();
        unitPst.close();
        if (acctId > 0) {
          AccountEntry ae = new AccountEntry();
          ae.setAccountId(acctId);
          ae.setAmount(invoice.getTotal().multiply(BigDecimal.valueOf(-1)));
          ae.setEntryDate(new Date());
          ae.setEffectiveDate(payDate);
          ae.setDescription("Invoice " + invoice.getNumber());
          ae.setYear(year);
          addEntry(ae, conn);

          ae = new AccountEntry();
          ae.setAccountId(account);
          ae.setAmount(invoice.getTotal());
          ae.setEntryDate(new Date());
          ae.setEffectiveDate(payDate);
          ae.setDescription("Invoice " + invoice.getNumber());
          ae.setYear(year);
          addEntry(ae, conn);

        }

      }

    } finally {
      conn.setAutoCommit(true);
      conn.close();
    }
  }

  public void markUnpaid(Invoice invoice, int year, long account) throws Exception {
    Connection conn = ds.getConnection();
    try {
      PreparedStatement pst = conn.prepareStatement("update invoices set paid = null where id = ?");
      pst.setLong(1, invoice.getId());
      pst.executeUpdate();
      pst.close();
      if (invoice.getBudgetItem() > 0) {
        updateBudgetBalance(conn, year, invoice.getBudgetItem(), invoice.getTotal().multiply(BigDecimal.valueOf(-1)));
      }

      if (invoice.getUnitId() > 0) {
        PreparedStatement unitPst = conn.prepareStatement("select id from accounts where unit_id = ?");
        unitPst.setLong(1, invoice.getUnitId());
        ResultSet rs = unitPst.executeQuery();
        long acctId = 0;
        if (rs.next()) {
          acctId = rs.getLong("id");
        }
        rs.close();
        unitPst.close();
        if (acctId > 0) {
          AccountEntry ae = new AccountEntry();
          ae.setAccountId(acctId);
          ae.setAmount(invoice.getTotal());
          ae.setEntryDate(new Date());
          ae.setEffectiveDate(ae.getEntryDate());
          ae.setDescription("Undo Payment Invoice " + invoice.getNumber());
          ae.setYear(year);
          addEntry(ae, conn);

          ae = new AccountEntry();
          ae.setAccountId(account);
          ae.setAmount(invoice.getTotal().multiply(BigDecimal.valueOf(-1)));
          ae.setEntryDate(new Date());
          ae.setEffectiveDate(ae.getEntryDate());
          ae.setDescription("Undo Payment Invoice " + invoice.getNumber());
          ae.setYear(year);
          addEntry(ae, conn);
        }

      }

    } finally {
      conn.setAutoCommit(true);
      conn.close();
    }
  }

  public Invoice getInvoice(long corpId, long number) throws Exception {
    Connection conn = null;
    Invoice invoice = null;
    try {
      conn = ds.getConnection();
      PreparedStatement pst = conn.prepareStatement("select * from invoices where num = ? and corp_id = ?");
      pst.setLong(1, number);
      pst.setLong(2, corpId);
      ResultSet rs = pst.executeQuery();
      if (rs.next()) {
        invoice = createInvoice(rs);
        rs.close();
        pst.close();
        PreparedStatement iPst = conn.prepareStatement("select * from invoice_items where invoice = ? order by seq asc");
        iPst.setLong(1, invoice.getId());
        LinkedList<InvoiceItem> items = new LinkedList<InvoiceItem>();
        invoice.setItems(items);
        ResultSet iRs = iPst.executeQuery();
        while (iRs.next()) {
          items.add(createInvoiceItem(iRs));
        }
        iRs.close();
        iPst.close();
      }
    } finally {
      if (conn != null) {
        conn.close();
      }
    }
    return invoice;
  }

  public Invoice getInvoice(long id) throws Exception {
    Connection conn = null;
    Invoice invoice = null;
    try {
      conn = ds.getConnection();
      PreparedStatement pst = conn.prepareStatement("select * from invoices where id = ?");
      pst.setLong(1, id);
      ResultSet rs = pst.executeQuery();
      if (rs.next()) {
        invoice = createInvoice(rs);
        rs.close();
        pst.close();
        PreparedStatement iPst = conn.prepareStatement("select * from invoice_items where invoice = ? order by seq asc");
        iPst.setLong(1, invoice.getId());
        LinkedList<InvoiceItem> items = new LinkedList<InvoiceItem>();
        invoice.setItems(items);
        ResultSet iRs = iPst.executeQuery();
        while (iRs.next()) {
          items.add(createInvoiceItem(iRs));
        }
        iRs.close();
        iPst.close();
      }
    } finally {
      if (conn != null) {
        conn.close();
      }
    }
    return invoice;
  }

  public void resetAccounts(long corpId) throws Exception {
    Connection conn = Util.getConnection();
    try {
      PreparedStatement pst = conn.prepareStatement("delete from acct_entries where ");

    } finally {
      conn.setAutoCommit(true);
      conn.close();
    }
  }

  public void sendInvoice(Invoice invoice, String email) throws Exception {

    String url = Config.getString("url") + "/corp/accounts/invoice.pdf?override=0dfc912027f301759cdbd325dd22ce4f&num=" + invoice.getId();

  }

}
