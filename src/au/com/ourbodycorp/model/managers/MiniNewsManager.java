package au.com.ourbodycorp.model.managers;

import au.com.ourbodycorp.Util;
import au.com.ourbodycorp.model.Corporation;
import au.com.ourbodycorp.model.MiniNews;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URL;

import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import javax.net.ssl.HttpsURLConnection;

public class MiniNewsManager extends ManagerBase {

    public MiniNews save(MiniNews news) throws Exception {
        String columns[] = {"id", "corp_id", "created", "pushed", "status", "headline", "body", "link", "photo", "sharelocation", "shareid"};
        Connection conn = ds.getConnection();
        try {
            PreparedStatement pst;
            PreparedStatement pstDel;
            if (news.getId() == 0) {
                news.setId(getSequence("miniNewsId", conn));
                pst = conn.prepareStatement(createInsert("mini_news", columns));
            }
            else {
                pst = conn.prepareStatement(createUpdate("mini_news", columns, "id = ?"));
                pst.setLong(columns.length + 1, news.getId());
            }
            int i = 0;
            pst.setLong(++i, news.getId());
            pst.setLong(++i, news.getCorpId());
            pst.setTimestamp(++i, new Timestamp(news.getCreated().getTime()));
            pst.setTimestamp(++i, (news.getPushed() != null) ? new Timestamp(news.getPushed().getTime()) : null);
            pst.setString(++i, news.getStatus());
            pst.setString(++i, news.getHeadline());
            pst.setString(++i, news.getBody());
            pst.setString(++i, news.getLink());
            if (news.getPhotoId() > 0) {
                pst.setLong(++i, news.getPhotoId());
            }
            else {
                pst.setNull(++i, Types.BIGINT);
            }
            pst.setBoolean(++i, news.isSharelocation());
            if(news.getShareid() > 0){
            	pst.setLong(++i, news.getShareid());
            }else{
            	pst.setLong(++i, news.getId());
            }
            pst.executeUpdate();
            pst.close();
            if(news.isSharelocation() == false){
            	
            	pstDel = conn.prepareStatement("select id from mini_news where headline = ? and not id = ? and (shareid = ? or id = ?)");
            	pstDel.setString(1, news.getHeadline());
            	pstDel.setLong(2, news.getId());
            	pstDel.setLong(3, news.getShareid());
            	pstDel.setLong(4, news.getId());
                ResultSet rs = pstDel.executeQuery();
                
                while (rs.next()) {
                    pstDel = conn.prepareStatement("delete from mini_news where id = ?");
                	pstDel.setLong(1, rs.getLong("id"));
                	pstDel.executeUpdate();
                	pstDel.close();
                }
                
                pstDel = conn.prepareStatement("DELETE FROM mini_news WHERE headline = ? and not id= ? and (shareid = ? or id = ?)");
                pstDel.setString(1, news.getHeadline());
                pstDel.setLong(2, news.getId());
                pstDel.setLong(3, news.getShareid());
                pstDel.setLong(4, news.getId());
                pstDel.executeUpdate();
                pstDel.close();

//                conn.commit();
            }
        }catch(Exception e){
        	e.printStackTrace();
        }
        finally {
            conn.close();
        }
        return news;
    }

    public void deleteNews(long id) throws Exception {
        Connection conn = ds.getConnection();
        try {
            Statement st = conn.createStatement();
            st.execute("delete from mini_news where id = " + id);
            st.close();
        }
        finally {
            conn.close();
        }
    }

    public MiniNews getNews(long id) throws Exception {
        Connection conn = ds.getConnection();
        MiniNews n = null;
        try {
            PreparedStatement pst = conn.prepareStatement("select * from mini_news where id = ?");
            pst.setLong(1, id);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                n = new MiniNews();
                n.setId(rs.getLong("id"));
                n.setBody(rs.getString("body"));
                n.setCorpId(rs.getLong("corp_id"));
                n.setCreated(rs.getTimestamp("created"));
                n.setHeadline(rs.getString("headline"));
                n.setLink(rs.getString("link"));
                n.setPhotoId(rs.getLong("photo"));
                n.setStatus(rs.getString("status"));
                n.setPushed(rs.getTimestamp("pushed"));
                n.setSharelocation(rs.getBoolean("sharelocation"));
                n.setShareid(rs.getLong("shareid"));
            }
            rs.close();
            pst.close();
        }
        finally {
            conn.close();
        }
        return n;
    }

    public List<MiniNews> listNews(long corpId, int limit, int offset, String status) throws Exception {
        LinkedList<MiniNews> news = new LinkedList<MiniNews>();
        Connection conn = ds.getConnection();
        MiniNews n = null;
        try {
            String sql = "select * from mini_news where corp_id = ?";

            if (status != null) {
                sql += " and status = ?";
            }
            sql += " order by created desc";
            if (limit > 0) {
                sql += " limit " + limit;
            }
            if (offset > 0) {
                sql += " offset " + offset;
            }
            PreparedStatement pst = conn.prepareStatement(sql);
            pst.setLong(1, corpId);
            if (status != null) {
                pst.setString(2, status);
            }
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                n = new MiniNews();
                news.add(n);
                n.setId(rs.getLong("id"));
                n.setBody(rs.getString("body"));
                n.setCorpId(rs.getLong("corp_id"));
                n.setCreated(rs.getTimestamp("created"));
                n.setHeadline(rs.getString("headline"));
                n.setLink(rs.getString("link"));
                n.setPhotoId(rs.getLong("photo"));
                n.setStatus(rs.getString("status"));
                n.setPushed(rs.getTimestamp("pushed"));
            }
            rs.close();
            pst.close();
        }
        finally {
            conn.close();
        }
        return news;
    }
    
    public void send(MiniNews newsItem) throws Exception {
        OutputStream outputStream = null;
        HttpsURLConnection connection = null;
        BufferedWriter writer = null;
        try {
            URL pushUrl = new URL("https://api.parse.com/1/push");
            connection = (HttpsURLConnection) pushUrl.openConnection();
            
            //prod keys
            connection.setRequestProperty("X-Parse-Application-Id", "8aZhlEK7gJdVdTNv4IPHljCxtDlucfNkHsELmT62");
            connection.setRequestProperty("X-Parse-REST-API-Key", "k7hkrEqJsHyG8l4EVA5hy9l5P9919BKjEmzWGVm8");  
            
            //dev keys
//            connection.setRequestProperty("X-Parse-Application-Id", "80RJrTjwnIH3tMGZYCPd4p1zdokOI7z02jVovGpa");
//            connection.setRequestProperty("X-Parse-REST-API-Key", "FmbzOwHEoIIbcv7zh6AxviW8NdNVtrqZxqRmvtDK");
            
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(15000);
            connection.setReadTimeout(15000);
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);
            connection.setRequestMethod("POST");

            Corporation practice = new CorporationManager().getCorporation(newsItem.getCorpId());
            
            LinkedHashMap<String, Object> jsonMap = new LinkedHashMap<String, Object>();
            ArrayList channels = new ArrayList();
            jsonMap.put("channels", channels);
            channels.add("Practice-" + practice.getSlug());
            
            LinkedHashMap<String, Object> data = new LinkedHashMap<String, Object>();
            jsonMap.put("data", data);
            data.put("alert", newsItem.getHeadline());
            data.put("badge", "Increment");
            data.put("sound", "notif.aif");
            data.put("action", "com.fiizio.fiiziolive");
            data.put("newsid", newsItem.getId());
            
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            String jsonPostStr = gson.toJson(jsonMap);
            
            outputStream = connection.getOutputStream();
            writer = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
            writer.write(jsonPostStr);
            System.out.println("String:" + jsonPostStr);
            writer.close();
            outputStream.close();
            if (connection.getResponseCode() != 200){
                System.out.println("Response Message:" + connection.getResponseMessage() + " with code " + connection.getResponseCode());
                throw new IOException("Push sending failed");
            }
            
            System.out.println("Sent news push message");
        }
        catch (Exception e){
            e.printStackTrace();
            throw new IOException("Push news sending failed");
        }
        finally {
            Util.close(writer);
            Util.close(outputStream);
            Util.close(connection);
        }
    }
}
