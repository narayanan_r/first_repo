package au.com.ourbodycorp.model.managers;

import au.com.ourbodycorp.model.Menu;
import au.com.ourbodycorp.model.MenuItem;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public class MenuManager  extends ManagerBase {

  private MenuItem createMenuItem(ResultSet rs) throws Exception {
    MenuItem item = new MenuItem();
    item.setId(rs.getLong("id"));
    item.setMenuId(rs.getLong("menuId"));
    item.setParent(rs.getLong("parent"));
    item.setOrdering(rs.getLong("ordering"));
    item.setName(rs.getString("name"));
    item.setSlug(rs.getString("slug"));
    item.setPostId(rs.getLong("postId"));
    item.setCategoryId(rs.getLong("categoryId"));
    item.setAttachmentId(rs.getLong("attachmentId"));
    item.setBlogId(rs.getLong("blogId"));
    return item;
  }

  public List<Menu> getMenus() throws Exception {
    LinkedList<Menu> menus = new LinkedList<Menu>();
    Connection conn = null;
    try {
      conn = ds.getConnection();
      Statement st = conn.createStatement();
      ResultSet rs = st.executeQuery("select * from menus order by lower(name)");
      while (rs.next()) {
        menus.add(new Menu(rs.getLong("id"), rs.getString("name")));
      }
    } finally {
      if (conn != null) conn.close();
    }
    return menus;
  }

  public Menu getMenu(long menuId) throws Exception {
    Menu menu = null;
    Connection conn = null;
    try {
      conn = ds.getConnection();
      Statement st = conn.createStatement();
      ResultSet rs = st.executeQuery("select * from menus where id = " + menuId);
      if (rs.next()) {
        menu = new Menu(rs.getLong("id"), rs.getString("name"));
      }
    } finally {
      if (conn != null) conn.close();
    }
    return menu;
  }

  public List<MenuItem> getMenuItems(long menuId) throws Exception {
    LinkedList<MenuItem> items = new LinkedList<MenuItem>();
    Connection conn = null;
    try {
      conn = ds.getConnection();
      PreparedStatement st = conn.prepareStatement("select * from menuItems where menuId = ? order by parent, ordering, lower(name)");
      st.setLong(1, menuId);
      ResultSet rs = st.executeQuery();
      while (rs.next()) {
        items.add(createMenuItem(rs));
      }
    } finally {
      if (conn != null) conn.close();
    }
    return items;
  }

  public List<MenuItem> getTopLevelMenuItems(String menu) throws Exception {
    LinkedList<MenuItem> items = new LinkedList<MenuItem>();
    Connection conn = null;
    try {
      conn = ds.getConnection();
      PreparedStatement st = conn.prepareStatement("select * from menuItems where menuId = (select id from menus where name = ?) and parent is null order by ordering, lower(name)");
      st.setString(1, menu);
      ResultSet rs = st.executeQuery();
      while (rs.next()) {
        items.add(createMenuItem(rs));
      }
    } finally {
      if (conn != null) conn.close();
    }
    return items;
  }

  public List<MenuItem> getChildMenuItems(long parentId) throws Exception {
    LinkedList<MenuItem> items = new LinkedList<MenuItem>();
    Connection conn = null;
    try {
      conn = ds.getConnection();
      PreparedStatement st = conn.prepareStatement("select * from menuItems where parent = ? order by ordering, lower(name)");
      st.setLong(1, parentId);
      ResultSet rs = st.executeQuery();
      while (rs.next()) {
        items.add(createMenuItem(rs));
      }
    } finally {
      if (conn != null) conn.close();
    }
    return items;
  }

  public MenuItem saveMenuItem(MenuItem item) throws Exception {
    Connection conn = null;
    String[] columns = {"id", "menuId", "parent", "ordering", "name", "slug", "postId", "categoryId", "attachmentId", "blogId"};
    try {
      conn = ds.getConnection();
      PreparedStatement ps;
      if (item.getId() == 0) {
        item.setId(getSequence("menuItemId"));
        ps = conn.prepareStatement(createInsert("menu_items", columns));
      } else {
        ps = conn.prepareStatement(createUpdate("menu_items", columns, "id = ?"));
        ps.setLong(columns.length + 1, item.getId());
      }
      int i = 0;
      ps.setLong(++i, item.getId());
      ps.setLong(++i, item.getMenuId());
      if (item.getParent() > 0) {
        ps.setLong(++i, item.getParent());
      } else {
        ps.setNull(++i, Types.BIGINT);
      }
      ps.setLong(++i, item.getOrdering());
      ps.setString(++i, item.getName());
      ps.setString(++i, item.getSlug());

      if (item.getPostId() > 0) {
        ps.setLong(++i, item.getPostId());
      } else {
        ps.setNull(++i, Types.BIGINT);
      }

      if (item.getCategoryId() > 0) {
        ps.setLong(++i, item.getCategoryId());
      } else {
        ps.setNull(++i, Types.BIGINT);
      }

      if (item.getAttachmentId() > 0) {
        ps.setLong(++i, item.getAttachmentId());
      } else {
        ps.setNull(++i, Types.BIGINT);
      }

      if (item.getBlogId() > 0) {
        ps.setLong(++i, item.getBlogId());
      } else {
        ps.setNull(++i, Types.BIGINT);
      }

      ps.executeUpdate();

    } finally {
      if (conn != null) conn.close();
    }
    return item;
  }

  public void deleteMenuItem(long id) throws Exception {
    Connection conn = null;
    try {
      conn = ds.getConnection();
      PreparedStatement pst = conn.prepareStatement("delete from menu_items where id = ?");
      pst.executeUpdate();
    } finally {
      if (conn != null) {
        conn.close();
      }
    }
  }

}
