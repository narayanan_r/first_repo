
package au.com.ourbodycorp.model.managers;
import au.com.ourbodycorp.model.*;
import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.velocity.runtime.parser.node.MathUtils;
import org.joda.time.DateTime;

import au.com.ourbodycorp.Util;
import au.com.ourbodycorp.model.Attachment;
import au.com.ourbodycorp.model.CancelSubscription;
import au.com.ourbodycorp.model.CardDetails;
import au.com.ourbodycorp.model.CartDetails;
import au.com.ourbodycorp.model.Corporation;
import au.com.ourbodycorp.model.Exercise;
import au.com.ourbodycorp.model.ExerciseMedia;
import au.com.ourbodycorp.model.Folder;
import au.com.ourbodycorp.model.MyInfo;
import au.com.ourbodycorp.model.Patient;
import au.com.ourbodycorp.model.PaymentHistory;
import au.com.ourbodycorp.model.PlanPrice;
import au.com.ourbodycorp.model.Program;
import au.com.ourbodycorp.model.ProgramExercise;
import au.com.ourbodycorp.model.Promotion;

public class LibraryManager extends ManagerBase {
    public Folder getFolder(long id) throws Exception {
        Folder f;
        f = null;
        Connection conn = null;
        try {
            conn = this.ds.getConnection();
            PreparedStatement pst = conn.prepareStatement("select * from exercise_category where id = ?");
            pst.setLong(1, id);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                f = new Folder();
                f.setCorpId(rs.getLong("corp_id"));
                f.setId(rs.getLong("id"));
                f.setName(rs.getString("name"));
                f.setParentId(rs.getLong("parent"));
            }
            rs.close();
            pst.close();
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return f;
    }

    public Exercise save(Exercise e, long companyid, String copyExercise) throws Exception {
        String[] columns = new String[]{"id", "category", "corp_id", "name", "repTime", "reps", "restTime", "setRestTime", "description", "created", "creator", "type", "show_sides", "sharelocation", "hideexercise", "shareid", "sets", "holds", "rest", "side", "weight"};
        Connection conn = null;
        try {
            try {
                PreparedStatement pst;
                conn = this.ds.getConnection();
                if (e.getId() == 0) {
                    e.setId(this.getSequence("exerciseId"));
                    e.setCreated(new java.util.Date());
                    pst = conn.prepareStatement(this.createInsert("exercises", columns));
                } else {
                    pst = conn.prepareStatement(this.createUpdate("exercises", columns, "id = ?"));
                    pst.setLong(columns.length + 1, e.getId());
                }
                int i = 0;
                pst.setLong(++i, e.getId());
                if (e.getCategory() > 0) {
                    pst.setLong(++i, e.getCategory());
                } else {
                    pst.setNull(++i, -5);
                }
                if (e.getCorpId() > 0) {
                    pst.setLong(++i, e.getCorpId());
                } else {
                    pst.setNull(++i, -5);
                }
                pst.setString(++i, e.getName());
                pst.setInt(++i, e.getRepTime());
                pst.setInt(++i, e.getReps());
                pst.setInt(++i, e.getRestTime());
                pst.setInt(++i, e.getSetRestTime());
                pst.setString(++i, e.getDescription());
                pst.setTimestamp(++i, new Timestamp(e.getCreated().getTime()));
                pst.setLong(++i, e.getCreator());
                pst.setString(++i, e.getType());
                pst.setBoolean(++i, e.isShowSides());
                pst.setBoolean(++i, e.isShareLocation());
                pst.setBoolean(++i, e.isHideExercise());
                if (e.getShareid() > 0) {
                    pst.setLong(++i, e.getShareid());
                } else {
                    pst.setLong(++i, e.getId());
                }
                pst.setInt(++i, e.getSets());
                pst.setInt(++i, e.getHolds());
                pst.setInt(++i, e.getRest());
                pst.setString(++i, e.getSide());
                pst.setInt(++i, e.getWeight());
                pst.executeUpdate();
                pst.close();
                if (!e.isShareLocation() && copyExercise == null) {
                    PreparedStatement pstDel = conn.prepareStatement("select e.id from exercises e join corporation c on c.id =  e.corp_id where e.name = ? and not e.id = ? and c.company = ? and (e.shareid = ? or e.id = ?) order by e.id");
                    pstDel.setString(1, e.getName());
                    pstDel.setLong(2, e.getId());
                    pstDel.setLong(3, companyid);
                    pstDel.setLong(4, e.getShareid());
                    pstDel.setLong(5, e.getId());
                    pstDel.setInt(++i, e.getSets());
                    pstDel.setInt(++i, e.getHolds());
                    pstDel.setInt(++i, e.getRest());
                    pstDel.setString(++i, e.getSide());
                    pstDel.setInt(++i, e.getWeight());
                    ResultSet rs = pstDel.executeQuery();
                    while (rs.next()) {
                        this.delWithExcerciseId(rs.getLong("id"), "program_exercises");
                        this.delWithExcerciseId(rs.getLong("id"), "exercise_media");
                        this.delWithExcerciseId(rs.getLong("id"), "bodyparts");
                        this.delWithExcerciseId(rs.getLong("id"), "exercise_types");
                    }
                    pstDel = conn.prepareStatement("DELETE FROM exercises WHERE name = ? and id in (select e.id from exercises e join corporation c on c.id =  e.corp_id where e.name = ? and not e.id = ? and c.company = ? and (e.shareid = ? or e.id = ?) order by e.id)");
                    pstDel.setString(1, e.getName());
                    pstDel.setString(2, e.getName());
                    pstDel.setLong(3, e.getId());
                    pstDel.setLong(4, companyid);
                    pstDel.setLong(5, e.getShareid());
                    pstDel.setLong(6, e.getId());
                    pstDel.setInt(++i, e.getSets());
                    pstDel.setInt(++i, e.getHolds());
                    pstDel.setInt(++i, e.getRest());
                    pstDel.setString(++i, e.getSide());
                    pstDel.setInt(++i, e.getWeight());
                    pstDel.executeUpdate();
                    pstDel.close();
                }
            }
            catch (Exception ee) {
                ee.printStackTrace();
                if (conn != null) {
                    conn.close();
                }
            }
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return e;
    }

    public void delWithExcerciseId(long id, String tableName) throws SQLException {
        Connection conn = null;
        try {
            try {
                conn = this.ds.getConnection();
                PreparedStatement pstDel = conn.prepareStatement("delete from " + tableName + " where exercise = ?");
                pstDel.setLong(1, id);
                pstDel.executeUpdate();
                pstDel.close();
            }
            catch (Exception e) {
                e.printStackTrace();
                if (conn != null) {
                    conn.close();
                }
            }
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public List<Exercise> getExercises(Exercise.BodyPart bodyPart, Exercise.Type type, long corpId) throws Exception {
        return this.getExercises(bodyPart, type, corpId, false, false);
    }

    public List<Exercise> getExercisesBySearch(Exercise.BodyPart bodyPart, Exercise.Type type, long corpId, String query) throws Exception {
        return this.getExercisesBySearch(bodyPart, type, corpId, false, false, query);
    }

    public List<Exercise> getExercises(Exercise.BodyPart bodyPart, Exercise.Type type, long corpId, boolean exclusive, boolean hiddenExercise) throws Exception {
        LinkedList<Exercise> exercises;
        exercises = new LinkedList<Exercise>();
        Connection conn = null;
        try {
            conn = this.ds.getConnection();
            String sql = "select e.*, m.id as media, m.version as media_version from exercises e left join exercise_media m on e.id = m.exercise and key_photo = true";
            if (bodyPart != null) {
                sql = String.valueOf(sql) + " where e.id in (select exercise from bodyparts where bodypart = ?)";
            }
            if (type != null) {
                sql = String.valueOf(sql) + (bodyPart == null ? " where e.id in (select exercise from exercise_types where type = ?)" : " and e.id in (select exercise from exercise_types where type = ?)");
            }
            if (corpId > 0 && exclusive) {
                sql = String.valueOf(sql) + (bodyPart == null && type == null ? new StringBuilder(" where corp_id = ").append(corpId).toString() : new StringBuilder(" and corp_id = ").append(corpId).toString());
            } else if (corpId > 0) {
                sql = hiddenExercise ? String.valueOf(sql) + (bodyPart == null && type == null ? new StringBuilder(" where (corp_id is null or corp_id = ").append(corpId).append(") and (hideexercise = true or hideexercise = false)").toString() : new StringBuilder(" and (corp_id is null or corp_id = ").append(corpId).append(") and (hideexercise = true or hideexercise = false)").toString()) : String.valueOf(sql) + (bodyPart == null && type == null ? new StringBuilder(" where (corp_id is null or corp_id = ").append(corpId).append(") and hideexercise = false").toString() : new StringBuilder(" and (corp_id is null or corp_id = ").append(corpId).append(") and hideexercise = false").toString());
            } else if (corpId == -1) {
                sql = String.valueOf(sql) + (bodyPart == null && type == null ? " where corp_id is null" : " and corp_id is null");
            }
            sql = String.valueOf(sql) + " order by lower(e.name) asc";
            PreparedStatement pst = conn.prepareStatement(sql);
            int i = 0;
            if (bodyPart != null) {
                pst.setString(++i, bodyPart.name());
            }
            if (type != null) {
                pst.setString(++i, type.name());
            }
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                Exercise e = new Exercise();
                e.setId(rs.getLong("id"));
                e.setCategory(rs.getLong("category"));
                e.setCorpId(rs.getLong("corp_id"));
                e.setName(rs.getString("name"));
                e.setRepTime(rs.getInt("repTime"));
                e.setReps(rs.getInt("reps"));
                e.setRestTime(rs.getInt("restTime"));
                e.setSetRestTime(rs.getInt("setRestTime"));
                e.setDescription(rs.getString("description"));
                e.setCreated((java.util.Date)rs.getTimestamp("created"));
                e.setCreator(rs.getLong("creator"));
                e.setKeyPhotoId(Long.valueOf(rs.getLong("media")));
                e.setKeyPhotoVersion(rs.getInt("media_version"));
                e.setType(rs.getString("type"));
                exercises.add(e);
            }
            rs.close();
            pst.close();
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return exercises;
    }

    public List<Exercise> getExercisesBySearch(Exercise.BodyPart bodyPart, Exercise.Type type, long corpId, boolean exclusive, boolean hiddenExercise, String query) throws Exception {
        LinkedList<Exercise> exercises;
        exercises = new LinkedList<Exercise>();
        Connection conn = null;
        try {
            conn = this.ds.getConnection();
            String sql = "select e.*, m.id as media, m.version as media_version from exercises e left join exercise_media m on e.id = m.exercise and key_photo = true";
            if (bodyPart != null) {
                sql = String.valueOf(sql) + " where e.id in (select exercise from bodyparts where bodypart = ?)";
            }
            if (type != null) {
                sql = String.valueOf(sql) + (bodyPart == null ? " where e.id in (select exercise from exercise_types where type = ?)" : " and e.id in (select exercise from exercise_types where type = ?)");
            }
            if (corpId > 0 && exclusive) {
                sql = String.valueOf(sql) + (bodyPart == null && type == null ? new StringBuilder(" where corp_id = ").append(corpId).toString() : new StringBuilder(" and corp_id = ").append(corpId).toString());
            } else if (corpId > 0) {
                sql = hiddenExercise ? String.valueOf(sql) + (bodyPart == null && type == null ? new StringBuilder(" where (corp_id is null or corp_id = ").append(corpId).append(") and (hideexercise = true or hideexercise = false)").toString() : new StringBuilder(" and (corp_id is null or corp_id = ").append(corpId).append(") and (hideexercise = true or hideexercise = false)").toString()) : String.valueOf(sql) + (bodyPart == null && type == null ? new StringBuilder(" where (corp_id is null or corp_id = ").append(corpId).append(") and hideexercise = false").toString() : new StringBuilder(" and (corp_id is null or corp_id = ").append(corpId).append(") and hideexercise = false").toString());
            } else if (corpId == -1) {
                sql = String.valueOf(sql) + (bodyPart == null && type == null ? " where corp_id is null" : " and corp_id is null");
            }
            sql = String.valueOf(sql) + " and lower(e.name) like ? order by lower(e.name) asc";
            PreparedStatement pst = conn.prepareStatement(sql);
            int i = 0;
            if (bodyPart != null) {
                pst.setString(++i, bodyPart.name());
            }
            if (type != null) {
                pst.setString(++i, type.name());
            }
            if (query != null) {
                pst.setString(++i, "%" + query.toLowerCase() + "%");
            }
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                Exercise e = new Exercise();
                e.setId(rs.getLong("id"));
                e.setCategory(rs.getLong("category"));
                e.setCorpId(rs.getLong("corp_id"));
                e.setName(rs.getString("name"));
                e.setRepTime(rs.getInt("repTime"));
                e.setReps(rs.getInt("reps"));
                e.setRestTime(rs.getInt("restTime"));
                e.setSetRestTime(rs.getInt("setRestTime"));
                e.setDescription(rs.getString("description"));
                e.setCreated((java.util.Date)rs.getTimestamp("created"));
                e.setCreator(rs.getLong("creator"));
                e.setKeyPhotoId(Long.valueOf(rs.getLong("media")));
                e.setKeyPhotoVersion(rs.getInt("media_version"));
                e.setType(rs.getString("type"));
                exercises.add(e);
            }
            rs.close();
            pst.close();
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return exercises;
    }

    public List<Exercise> getExercisesBySearchforAddExercise(Exercise.BodyPart bodyPart, Exercise.Type type, long corpId, String query) throws Exception {
        LinkedList<Exercise> exercises;
        exercises = new LinkedList<Exercise>();
        Connection conn = null;
        try {
            conn = this.ds.getConnection();
            String sql = "select e.*, m.id as media, m.version as media_version from exercises e left join exercise_media m on e.id = m.exercise and key_photo = true";
            if (bodyPart != null) {
                sql = String.valueOf(sql) + " where e.id in (select exercise from bodyparts where bodypart = ?)";
            }
            if (type != null) {
                sql = String.valueOf(sql) + (bodyPart == null ? " where e.id in (select exercise from exercise_types where type = ?)" : " and e.id in (select exercise from exercise_types where type = ?)");
            }
            if (corpId > 0) {
                sql = String.valueOf(sql) + (bodyPart == null && type == null ? new StringBuilder(" where (corp_id is null or corp_id = ").append(corpId).append(")").toString() : new StringBuilder(" and (corp_id is null or corp_id = ").append(corpId).append(")").toString());
            } else if (corpId == -1) {
                sql = String.valueOf(sql) + (bodyPart == null && type == null ? " where corp_id is null" : " and corp_id is null");
            }
            sql = String.valueOf(sql) + " and lower(e.name) like ? order by lower(e.name) asc";
            PreparedStatement pst = conn.prepareStatement(sql);
            int i = 0;
            if (bodyPart != null) {
                pst.setString(++i, bodyPart.name());
            }
            if (type != null) {
                pst.setString(++i, type.name());
            }
            if (query != null) {
                pst.setString(++i, "%" + query.toLowerCase() + "%");
            }
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                Exercise e = new Exercise();
                e.setId(rs.getLong("id"));
                e.setCategory(rs.getLong("category"));
                e.setCorpId(rs.getLong("corp_id"));
                e.setName(rs.getString("name"));
                e.setRepTime(rs.getInt("repTime"));
                e.setReps(rs.getInt("reps"));
                e.setRestTime(rs.getInt("restTime"));
                e.setSetRestTime(rs.getInt("setRestTime"));
                e.setDescription(rs.getString("description"));
                e.setCreated((java.util.Date)rs.getTimestamp("created"));
                e.setCreator(rs.getLong("creator"));
                e.setKeyPhotoId(Long.valueOf(rs.getLong("media")));
                e.setKeyPhotoVersion(rs.getInt("media_version"));
                e.setType(rs.getString("type"));
                exercises.add(e);
            }
            rs.close();
            pst.close();
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return exercises;
    }

    public Exercise getExercise(long id) throws Exception {
        Exercise e;
        e = null;
        Connection conn = null;
        try {
            try {
                conn = this.ds.getConnection();
                PreparedStatement pst = conn.prepareStatement("select * from exercises where id = ?");
                pst.setLong(1, id);
                ResultSet rs = pst.executeQuery();
                if (rs.next()) {
                    e = new Exercise();
                    e.setId(rs.getLong("id"));
                    e.setCategory(rs.getLong("category"));
                    e.setCorpId(rs.getLong("corp_id"));
                    e.setName(rs.getString("name"));
                    e.setRepTime(rs.getInt("repTime"));
                    e.setReps(rs.getInt("reps"));
                    e.setRestTime(rs.getInt("restTime"));
                    e.setSetRestTime(rs.getInt("setRestTime"));
                    e.setDescription(rs.getString("description"));
                    e.setCreated((java.util.Date)rs.getTimestamp("created"));
                    e.setCreator(rs.getLong("creator"));
                    e.setType(rs.getString("type"));
                    e.setShowSides(rs.getBoolean("show_sides"));
                    e.setShareLocation(rs.getBoolean("shareLocation"));
                    e.setHideExercise(rs.getBoolean("hideexercise"));
                    e.setSide(rs.getString("side"));
                    e.setRest(rs.getInt("rest"));
                    e.setHolds(rs.getInt("holds"));
                    e.setSets(rs.getInt("sets"));
                    e.setRepsperseconds(rs.getInt("holds"));
                    e.setWeight(rs.getInt("weight"));
                }
                rs.close();
                pst.close();
                if (e != null) {
                    pst = conn.prepareStatement("select bodypart from bodyparts where exercise = ?");
                    pst.setLong(1, e.getId());
                    rs = pst.executeQuery();
                    e.setBodyParts(new LinkedList());
                    while (rs.next()) {
                        e.getBodyParts().add(Exercise.BodyPart.valueOf((String)rs.getString("bodypart")));
                    }
                    rs.close();
                    pst.close();
                    pst = conn.prepareStatement("select type from exercise_types where exercise = ?");
                    pst.setLong(1, e.getId());
                    rs = pst.executeQuery();
                    e.setTypes(new LinkedList());
                    while (rs.next()) {
                        e.getTypes().add(Exercise.Type.valueOf((String)rs.getString("type")));
                    }
                    rs.close();
                    pst.close();
                }
            }
            catch (Exception ee) {
                ee.printStackTrace();
                if (conn != null) {
                    conn.close();
                }
            }
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return e;
    }

    public int nextExerciseMediaSequence(long exerciseId) throws Exception {
        int seq;
        Connection conn = null;
        seq = 1;
        try {
            conn = this.ds.getConnection();
            PreparedStatement pst = conn.prepareStatement("select max(seq) as seq from exercise_media where exercise = ?");
            pst.setLong(1, exerciseId);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                seq = rs.getInt("seq") + 1;
            }
            rs.close();
            pst.close();
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return seq;
    }

    public List<ExerciseMedia> getExerciseMedia(long id) throws Exception {
        LinkedList<ExerciseMedia> eml;
        eml = new LinkedList<ExerciseMedia>();
        Connection conn = null;
        try {
            conn = this.ds.getConnection();
            PreparedStatement pst = conn.prepareStatement("select id, exercise, seq, type, url, caption, name, key_photo, version from exercise_media where exercise = ? order by seq asc");
            pst.setLong(1, id);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                ExerciseMedia em = new ExerciseMedia();
                em = new ExerciseMedia();
                em.setId(rs.getLong("id"));
                em.setExercise(rs.getLong("exercise"));
                em.setSeq(rs.getInt("seq"));
                em.setType(rs.getString("type"));
                em.setUrl(rs.getString("url"));
                em.setCaption(rs.getString("caption"));
                em.setName(rs.getString("name"));
                em.setKey(rs.getBoolean("key_photo"));
                em.setVersion(rs.getInt("version"));
                eml.add(em);
            }
            rs.close();
            pst.close();
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return eml;
    }

    public List<ExerciseMedia> getExerciseMediaWithData(long id) throws Exception {
        LinkedList<ExerciseMedia> eml;
        eml = new LinkedList<ExerciseMedia>();
        Connection conn = null;
        try {
            conn = this.ds.getConnection();
            PreparedStatement pst = conn.prepareStatement("select id, exercise, seq, type, url, caption, name, key_photo, data, version from exercise_media where exercise = ? order by seq asc");
            pst.setLong(1, id);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                ExerciseMedia em = new ExerciseMedia();
                em = new ExerciseMedia();
                em.setId(rs.getLong("id"));
                em.setExercise(rs.getLong("exercise"));
                em.setSeq(rs.getInt("seq"));
                em.setType(rs.getString("type"));
                em.setUrl(rs.getString("url"));
                em.setCaption(rs.getString("caption"));
                em.setName(rs.getString("name"));
                em.setKey(rs.getBoolean("key_photo"));
                em.setVersion(rs.getInt("version"));
                em.setData(rs.getBytes("data"));
                eml.add(em);
            }
            rs.close();
            pst.close();
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return eml;
    }

    public List<Corporation> getCorpid(String name, long companyid, long uid, long id) throws Exception {
        LinkedList<Corporation> corpid;
        corpid = new LinkedList<Corporation>();
        Connection conn = null;
        try {
            conn = this.ds.getConnection();
            PreparedStatement pst = conn.prepareStatement("select e.id,e.corp_id,c.name from exercises e join corporation c on c.id =  e.corp_id where e.name = ? and c.company = ? and (e.shareid = ? or e.id = ?)");
            pst.setString(1, name);
            pst.setLong(2, companyid);
            pst.setLong(3, uid);
            pst.setLong(4, id);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                Corporation em = new Corporation();
                em = new Corporation();
                em.setId(rs.getLong("id"));
                em.setCompanyId(rs.getLong("corp_id"));
                em.setName(rs.getString("name"));
                corpid.add(em);
            }
            rs.close();
            pst.close();
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return corpid;
    }

    public List<Corporation> getCorpidforArticles(String name, long companyid, long uid, long id) throws Exception {
        LinkedList<Corporation> corpid;
        corpid = new LinkedList<Corporation>();
        Connection conn = null;
        try {
            conn = this.ds.getConnection();
            PreparedStatement pst = conn.prepareStatement("select i.id,i.corp_id,c.name from info i join corporation c on c.id =  i.corp_id where i.headline = ? and c.company = ? and (i.shareid = ? or i.id = ?)");
            pst.setString(1, name);
            pst.setLong(2, companyid);
            pst.setLong(3, uid);
            pst.setLong(4, id);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                Corporation em = new Corporation();
                em = new Corporation();
                em.setId(rs.getLong("id"));
                em.setCompanyId(rs.getLong("corp_id"));
                em.setName(rs.getString("name"));
                corpid.add(em);
            }
            rs.close();
            pst.close();
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return corpid;
    }

    public List<Corporation> getCorpidforPrograms(String name, long companyid, long uid, long id) throws Exception {
        LinkedList<Corporation> corpid;
        corpid = new LinkedList<Corporation>();
        Connection conn = null;
        try {
            conn = this.ds.getConnection();
            PreparedStatement pst = conn.prepareStatement("select p.id,p.corp_id,c.name from programs p join corporation c on c.id =  p.corp_id where p.name = ? and c.company = ? and (p.shareid = ? or p.id = ?) and p.patient is NULL and p.group_id is NULL");
            pst.setString(1, name);
            pst.setLong(2, companyid);
            pst.setLong(3, uid);
            pst.setLong(4, id);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                Corporation em = new Corporation();
                em = new Corporation();
                em.setId(rs.getLong("id"));
                em.setCompanyId(rs.getLong("corp_id"));
                em.setName(rs.getString("name"));
                corpid.add(em);
            }
            rs.close();
            pst.close();
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return corpid;
    }

    public List<Corporation> getCorpidforPatientPrograms(String name, long companyid, String share, long uid, long id) throws Exception {
        LinkedList<Corporation> corpid;
        corpid = new LinkedList<Corporation>();
        Connection conn = null;
        try {
            conn = this.ds.getConnection();
            PreparedStatement pst = share != null ? conn.prepareStatement("select p.id,p.corp_id,c.name from programs p join corporation c on c.id =  p.corp_id where p.name = ? and p.corp_id = ? and (p.shareid = ? or p.id = ?) and (patient is null or group_id is null)") : conn.prepareStatement("select p.id,p.corp_id,c.name from programs p join corporation c on c.id =  p.corp_id where p.name = ? and c.company = ? and (p.shareid = ? or p.id = ?)");
            pst.setString(1, name);
            pst.setLong(2, companyid);
            pst.setLong(3, uid);
            pst.setLong(4, id);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                Corporation em = new Corporation();
                em = new Corporation();
                em.setId(rs.getLong("id"));
                em.setCompanyId(rs.getLong("corp_id"));
                em.setName(rs.getString("name"));
                corpid.add(em);
            }
            rs.close();
            pst.close();
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return corpid;
    }

    public List<Corporation> getEidforDelPatientPrograms(String name, long uid, long id) throws Exception {
        LinkedList<Corporation> corpid;
        corpid = new LinkedList<Corporation>();
        Connection conn = null;
        try {
            conn = this.ds.getConnection();
            PreparedStatement pst = conn.prepareStatement("select id from programs where name = ? and (shareid = ? or id = ?) and (patient is not NULL or group_id is not NULL)");
            pst.setString(1, name);
            pst.setLong(2, uid);
            pst.setLong(3, id);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                Corporation em = new Corporation();
                em = new Corporation();
                em.setId(rs.getLong("id"));
                corpid.add(em);
            }
            rs.close();
            pst.close();
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return corpid;
    }

    public List<Corporation> getEidforDelTemplatePrograms(String name, long uid, long id) throws Exception {
        LinkedList<Corporation> corpid;
        corpid = new LinkedList<Corporation>();
        Connection conn = null;
        try {
            conn = this.ds.getConnection();
            PreparedStatement pst = conn.prepareStatement("select id from programs where name = ? and (shareid = ? or id = ?) and patient is NULL and group_id is NULL");
            pst.setString(1, name);
            pst.setLong(2, uid);
            pst.setLong(3, id);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                Corporation em = new Corporation();
                em = new Corporation();
                em.setId(rs.getLong("id"));
                corpid.add(em);
            }
            rs.close();
            pst.close();
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return corpid;
    }

    public List<Corporation> getCorpidforNews(String name, long companyid, long uid, long id) throws Exception {
        LinkedList<Corporation> corpid;
        corpid = new LinkedList<Corporation>();
        Connection conn = null;
        try {
            conn = this.ds.getConnection();
            PreparedStatement pst = conn.prepareStatement("select m.id,m.corp_id,c.name from mini_news m join corporation c on c.id =  m.corp_id where m.headline = ? and c.company = ? and (m.shareid = ? or m.id = ?)");
            pst.setString(1, name);
            pst.setLong(2, companyid);
            pst.setLong(3, uid);
            pst.setLong(4, id);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                Corporation em = new Corporation();
                em = new Corporation();
                em.setId(rs.getLong("id"));
                em.setCompanyId(rs.getLong("corp_id"));
                em.setName(rs.getString("name"));
                corpid.add(em);
            }
            rs.close();
            pst.close();
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return corpid;
    }

    public void deleteExerciseMedia(long id) throws Exception {
        ExerciseMedia em = this.getExerciseMedia(id, false);
        Connection conn = this.ds.getConnection();
        try {
            conn.setAutoCommit(false);
            PreparedStatement pst = conn.prepareStatement("delete from exercise_media where id = ?");
            pst.setLong(1, id);
            pst.executeUpdate();
            pst.close();
            pst = conn.prepareStatement("update exercise_media set seq = seq - 1 where exercise = ? and seq > ?");
            pst.setLong(1, em.getExercise());
            pst.setInt(2, em.getSeq());
            pst.executeUpdate();
            pst.close();
        }
        finally {
            conn.setAutoCommit(true);
            conn.close();
        }
    }

    public ExerciseMedia getExerciseMedia(long id, boolean data) throws Exception {
        ExerciseMedia em;
        em = null;
        Connection conn = null;
        try {
            conn = this.ds.getConnection();
            PreparedStatement pst = null;
            pst = conn.prepareStatement("select id, exercise, seq, type, url, caption, name, version, key_photo" + (data ? ", data" : "") + " from exercise_media where id = ? ");
            pst.setLong(1, id);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                em = new ExerciseMedia();
                em.setId(rs.getLong("id"));
                em.setExercise(rs.getLong("exercise"));
                em.setSeq(rs.getInt("seq"));
                em.setType(rs.getString("type"));
                em.setUrl(rs.getString("url"));
                em.setCaption(rs.getString("caption"));
                em.setName(rs.getString("name"));
                em.setKey(rs.getBoolean("key_photo"));
                em.setVersion(rs.getInt("version"));
                if (data) {
                    em.setData(rs.getBytes("data"));
                }
            }
            rs.close();
            pst.close();
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return em;
    }

    public ExerciseMedia getExerciseMediaByLoc(long id, String name, boolean data) throws Exception {
        ExerciseMedia em;
        em = null;
        Connection conn = null;
        try {
            conn = this.ds.getConnection();
            PreparedStatement pst = null;
            pst = conn.prepareStatement("select id, exercise, seq, type, url, caption, name, version, key_photo" + (data ? ", data" : "") + " from exercise_media where exercise = ? and caption = ?");
            pst.setLong(1, id);
            pst.setString(2, name);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                em = new ExerciseMedia();
                em.setId(rs.getLong("id"));
                em.setExercise(rs.getLong("exercise"));
                em.setSeq(rs.getInt("seq"));
                em.setType(rs.getString("type"));
                em.setUrl(rs.getString("url"));
                em.setCaption(rs.getString("caption"));
                em.setName(rs.getString("name"));
                em.setKey(rs.getBoolean("key_photo"));
                em.setVersion(rs.getInt("version"));
                if (data) {
                    em.setData(rs.getBytes("data"));
                }
            }
            rs.close();
            pst.close();
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return em;
    }

    public ExerciseMedia getExerciseMediaDelByLoc(long id, String name, boolean data) throws Exception {
        ExerciseMedia em;
        em = null;
        Connection conn = null;
        try {
            conn = this.ds.getConnection();
            PreparedStatement pst = null;
            pst = conn.prepareStatement("select id, exercise, seq, type, url, caption, name, version, key_photo" + (data ? ", data" : "") + " from exercise_media where id = ? and caption = ?");
            pst.setLong(1, id);
            pst.setString(2, name);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                em = new ExerciseMedia();
                em.setId(rs.getLong("id"));
                em.setExercise(rs.getLong("exercise"));
                em.setSeq(rs.getInt("seq"));
                em.setType(rs.getString("type"));
                em.setUrl(rs.getString("url"));
                em.setCaption(rs.getString("caption"));
                em.setName(rs.getString("name"));
                em.setKey(rs.getBoolean("key_photo"));
                em.setVersion(rs.getInt("version"));
                if (data) {
                    em.setData(rs.getBytes("data"));
                }
            }
            rs.close();
            pst.close();
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return em;
    }

    public void swapMedia(long em1, long em2) throws Exception {
        Connection conn = null;
        try {
            conn = this.ds.getConnection();
            conn.setAutoCommit(false);
            int seq1 = 0;
            int seq2 = 0;
            PreparedStatement pst = conn.prepareStatement("select seq from exercise_media where id = ?");
            pst.setLong(1, em1);
            ResultSet rs = pst.executeQuery();
            if (!rs.next()) {
                return;
            }
            seq1 = rs.getInt("seq");
            rs.close();
            pst.setLong(1, em2);
            rs = pst.executeQuery();
            if (!rs.next()) {
                return;
            }
            seq2 = rs.getInt("seq");
            rs.close();
            pst.close();
            pst = conn.prepareStatement("update exercise_media set seq = ? where id = ?");
            pst.setInt(1, seq2);
            pst.setLong(2, em1);
            pst.executeUpdate();
            pst.setInt(1, seq1);
            pst.setLong(2, em2);
            pst.executeUpdate();
            pst.close();
            conn.commit();
        }
        finally {
            if (conn != null) {
                conn.setAutoCommit(true);
                conn.close();
            }
        }
    }

    public ExerciseMedia save(ExerciseMedia em) throws Exception {
        String[] columns = new String[]{"id", "exercise", "seq", "type", "url", "caption", "name", "key_photo", "data", "version"};
        Connection conn = null;
        try {
            PreparedStatement pst;
            conn = this.ds.getConnection();
            boolean addData = true;
            if (em.getId() == 0) {
                em.setId(this.getSequence("exerciseMediaId", conn));
                pst = conn.prepareStatement(this.createInsert("exercise_media", columns));
            } else {
                if (em.getData() == null || em.getData().length == 0) {
                    columns = new String[]{"id", "exercise", "seq", "type", "url", "caption", "name", "key_photo"};
                    addData = false;
                }
                pst = conn.prepareStatement(this.createUpdate("exercise_media", columns, "id = ?"));
                pst.setLong(columns.length + 1, em.getId());
            }
            int i = 0;
            pst.setLong(++i, em.getId());
            pst.setLong(++i, em.getExercise());
            pst.setInt(++i, em.getSeq());
            pst.setString(++i, em.getType());
            pst.setString(++i, em.getUrl());
            pst.setString(++i, em.getCaption());
            pst.setString(++i, em.getName());
            pst.setBoolean(++i, em.isKey());
            if (addData) {
                pst.setBytes(++i, em.getData());
                pst.setInt(++i, em.getVersion());
            }
            pst.executeUpdate();
            pst.close();
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return em;
    }

    public CardDetails saveCardDetails(CardDetails cd) throws Exception {
        String[] columns = new String[]{"id", "tokencustomerid", "accountno", "month", "year", "card_type", "name_on_card", "corp_id", "company", "userid", "created"};
        Connection conn = null;
        try {
            try {
                conn = this.ds.getConnection();
                cd.setId(this.getSequence("carddetailsid", conn));
                PreparedStatement pst = conn.prepareStatement(this.createInsert("card_details", columns));
                int i = 0;
                pst.setLong(++i, cd.getId());
                pst.setLong(++i, cd.getTokencustomerid());
                pst.setString(++i, cd.getAccountno());
                pst.setLong(++i, cd.getMonth());
                pst.setLong(++i, cd.getYear());
                pst.setString(++i, cd.getCard_type());
                pst.setString(++i, cd.getName_on_card());
                pst.setLong(++i, cd.getCorp_id());
                pst.setLong(++i, cd.getCompany());
                pst.setLong(++i, cd.getUserid());
                pst.setTimestamp(++i, new Timestamp(cd.getCreated().getTime()));
                pst.executeUpdate();
                pst.close();
            }
            catch (Exception e) {
                e.printStackTrace();
                if (conn != null) {
                    conn.close();
                }
            }
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return cd;
    }

    public CancelSubscription saveCancelSubscriptionDetails(CancelSubscription cs) throws Exception {
        String[] columns = new String[]{"id", "rebill_customer_id", "rebill_id", "order_date", "canceled_date", "reason", "corp_id", "company", "userid", "created"};
        Connection conn = null;
        try {
            try {
                conn = this.ds.getConnection();
                cs.setId(this.getSequence("cancelsubscriptionid", conn));
                PreparedStatement pst = conn.prepareStatement(this.createInsert("cancel_subscription", columns));
                int i = 0;
                pst.setLong(++i, cs.getId());
                pst.setLong(++i, cs.getRebill_customer_id());
                pst.setLong(++i, cs.getRebill_id());
                pst.setTimestamp(++i, new Timestamp(cs.getOrder_date().getTime()));
                pst.setTimestamp(++i, null);
                pst.setString(++i, cs.getReason());
                pst.setLong(++i, cs.getCorp_id());
                pst.setLong(++i, cs.getCompany());
                pst.setLong(++i, cs.getUserid());
                pst.setTimestamp(++i, new Timestamp(cs.getCreated().getTime()));
                pst.executeUpdate();
                pst.close();
            }
            catch (Exception e) {
                e.printStackTrace();
                if (conn != null) {
                    conn.close();
                }
            }
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return cs;
    }

    public PlanPrice savePlanPriceDetails(PlanPrice pp) throws Exception {
        String[] columns = new String[]{"id", "plan_name", "cents_per_sms", "max_users", "max_locations", "practice_logo", "push_notification", "currency_type", "tax", "price", "type", "invoice_number", "creator", "created"};
        Connection conn = null;
        try {
            try {
                PreparedStatement pst;
                conn = this.ds.getConnection();
                if (pp.getId() == 0) {
                    pp.setId(this.getSequence("planpriceid", conn));
                    pst = conn.prepareStatement(this.createInsert("plan_price", columns));
                } else {
                    pst = conn.prepareStatement(this.createUpdate("plan_price", columns, "id = ?"));
                    pst.setLong(columns.length + 1, pp.getId());
                }
                int i = 0;
                pst.setLong(++i, pp.getId());
                pst.setString(++i, pp.getPlan_name());
                pst.setLong(++i, pp.getCents_per_sms());
                pst.setLong(++i, pp.getMax_users());
                pst.setLong(++i, pp.getMax_locations());
                pst.setBoolean(++i, pp.isPractice_logo());
                pst.setBoolean(++i, pp.isPush_notification());
                pst.setString(++i, pp.getCurrency_type());
                pst.setBigDecimal(++i, pp.getTax());
                pst.setBigDecimal(++i, pp.getPrice());
                pst.setString(++i, pp.getType());
                pst.setString(++i, pp.getInvoice_number());
                pst.setLong(++i, pp.getCreator());
                pst.setTimestamp(++i, new Timestamp(pp.getCreated().getTime()));
                pst.executeUpdate();
                pst.close();
            }
            catch (Exception e) {
                e.printStackTrace();
                if (conn != null) {
                    conn.close();
                }
            }
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return pp;
    }

    public PlanPrice savePlanPriceHistoryDetails(PlanPrice pp) throws Exception {
        String[] columns = new String[]{"id", "plan_name", "cents_per_sms", "max_users", "max_locations", "practice_logo", "push_notification", "currency_type", "tax", "price", "type", "invoice_number", "creator", "created"};
        Connection conn = null;
        try {
            try {
                conn = this.ds.getConnection();
                pp.setId(this.getSequence("planpricehistoryid", conn));
                PreparedStatement pst = conn.prepareStatement(this.createInsert("plan_price_history", columns));
                int i = 0;
                pst.setLong(++i, pp.getId());
                pst.setString(++i, pp.getPlan_name());
                pst.setLong(++i, pp.getCents_per_sms());
                pst.setLong(++i, pp.getMax_users());
                pst.setLong(++i, pp.getMax_locations());
                pst.setBoolean(++i, pp.isPractice_logo());
                pst.setBoolean(++i, pp.isPush_notification());
                pst.setString(++i, pp.getCurrency_type());
                pst.setBigDecimal(++i, pp.getTax());
                pst.setBigDecimal(++i, pp.getPrice());
                pst.setString(++i, pp.getType());
                pst.setString(++i, pp.getInvoice_number());
                pst.setLong(++i, pp.getCreator());
                pst.setTimestamp(++i, new Timestamp(pp.getCreated().getTime()));
                pst.executeUpdate();
                pst.close();
            }
            catch (Exception e) {
                e.printStackTrace();
                if (conn != null) {
                    conn.close();
                }
            }
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return pp;
    }

    public PaymentHistory savePaymentHistoryDetails(PaymentHistory pp) throws Exception {
        String[] columns = new String[]{"id", "user_id", "corp_id", "company", "access_code", "response", "total_amount", "invoice_number", "trans_id", "trans_status", "created"};
        Connection conn = null;
        try {
            try {
                conn = this.ds.getConnection();
                pp.setId(this.getSequence("paymenthistoryid", conn));
                PreparedStatement pst = conn.prepareStatement(this.createInsert("payment_history", columns));
                int i = 0;
                pst.setLong(++i, pp.getId());
                pst.setLong(++i, pp.getUserid());
                pst.setLong(++i, pp.getCorp_id());
                pst.setLong(++i, pp.getCompany());
                pst.setString(++i, pp.getAccess_code());
                pst.setString(++i, pp.getResponse());
                pst.setBigDecimal(++i, pp.getTotal_amount());
                pst.setString(++i, pp.getInvoice_number());
                pst.setLong(++i, pp.getTrans_id());
                pst.setBoolean(++i, pp.isTrans_status());
                pst.setTimestamp(++i, new Timestamp(pp.getCreated().getTime()));
                pst.executeUpdate();
                pst.close();
            }
            catch (Exception e) {
                e.printStackTrace();
                if (conn != null) {
                    conn.close();
                }
            }
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return pp;
    }

    public Promotion savePromotionDetails(Promotion pp) throws Exception {
        String[] columns = new String[]{"id", "promo_code", "expires", "discount", "email", "creator", "created"};
        Connection conn = null;
        try {
            try {
                PreparedStatement pst;
                conn = this.ds.getConnection();
                if (pp.getId() == 0) {
                    pp.setId(this.getSequence("promotionid", conn));
                    pst = conn.prepareStatement(this.createInsert("promotion", columns));
                    int i = 0;
                    pst.setLong(++i, pp.getId());
                    pst.setString(++i, pp.getPromo_code());
                    pst.setTimestamp(++i, new Timestamp(pp.getExpires().getTime()));
                    pst.setLong(++i, pp.getDiscount());
                    pst.setString(++i, pp.getEmail());
                    pst.setLong(++i, pp.getCreator());
                    pst.setTimestamp(++i, new Timestamp(pp.getCreated().getTime()));
                } else {
                    String[] columns1 = new String[]{"id", "sent"};
                    pst = conn.prepareStatement(this.createUpdate("promotion", columns1, "id = ?"));
                    pst.setLong(columns1.length + 1, pp.getId());
                    int i = 0;
                    pst.setLong(++i, pp.getId());
                    pst.setTimestamp(++i, new Timestamp(pp.getSent().getTime()));
                }
                pst.executeUpdate();
                pst.close();
            }
            catch (Exception e) {
                e.printStackTrace();
                if (conn != null) {
                    conn.close();
                }
            }
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return pp;
    }

    public CartDetails saveCartDetails(CartDetails cd) throws Exception {
        String[] columns = new String[]{"id", "plan", "type", "corp_id", "company", "userid", "created"};
        Connection conn = null;
        try {
            try {
                conn = this.ds.getConnection();
                cd.setId(this.getSequence("cartdetailsid", conn));
                PreparedStatement pst = conn.prepareStatement(this.createInsert("cart_details", columns));
                int i = 0;
                pst.setLong(++i, cd.getId());
                pst.setLong(++i, cd.getPlan());
                pst.setString(++i, cd.getType());
                pst.setLong(++i, cd.getCorp_id());
                pst.setLong(++i, cd.getCompany());
                pst.setLong(++i, cd.getUserid());
                pst.setTimestamp(++i, new Timestamp(cd.getCreated().getTime()));
                pst.executeUpdate();
                pst.close();
            }
            catch (Exception e) {
                e.printStackTrace();
                if (conn != null) {
                    conn.close();
                }
            }
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return cd;
    }

    public void setKeyPhoto(long exercise, long media) throws Exception {
        Connection conn = null;
        try {
            conn = this.ds.getConnection();
            conn.setAutoCommit(false);
            PreparedStatement pst = conn.prepareStatement("update exercise_media set key_photo = false where exercise = ?");
            pst.setLong(1, exercise);
            pst.executeUpdate();
            pst.close();
            pst = conn.prepareStatement("update exercise_media set key_photo = true where id = ?");
            pst.setLong(1, media);
            pst.executeUpdate();
            pst.close();
            conn.commit();
        }
        finally {
            if (conn != null) {
                conn.setAutoCommit(true);
                conn.close();
            }
        }
    }

    public int countPatients(long corpId) throws Exception {
        int count;
        count = 0;
        Connection conn = this.ds.getConnection();
        try {
            PreparedStatement pst = conn.prepareStatement("select count(id) as total from patient where corp_id = ?");
            pst.setLong(1, corpId);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                count = rs.getInt("total");
            }
            rs.close();
            pst.close();
        }
        finally {
            conn.close();
        }
        return count;
    }

    public int countPromotions() throws Exception {
        int count;
        count = 0;
        Connection conn = this.ds.getConnection();
        try {
            PreparedStatement pst = conn.prepareStatement("select count(id) as total from promotion");
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                count = rs.getInt("total");
            }
            rs.close();
            pst.close();
        }
        finally {
            conn.close();
        }
        return count;
    }

    public int countPaymentHistory(long userid) throws Exception {
        int count;
        count = 0;
        Connection conn = this.ds.getConnection();
        try {
            PreparedStatement pst = conn.prepareStatement("select count(id) as total from payment_history where user_id = ?");
            pst.setLong(1, userid);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                count = rs.getInt("total");
            }
            rs.close();
            pst.close();
        }
        finally {
            conn.close();
        }
        return count;
    }

    public List<Patient> getPatients(long corpId, int offset, int limit) throws Exception {
        return this.getPatients(corpId, offset, limit, "lower(lname) asc");
    }

    public List<Patient> getPatients(long corpId, int offset, int limit, String order) throws Exception {
        LinkedList<Patient> patients;
        patients = new LinkedList<Patient>();
        Connection conn = this.ds.getConnection();
        try {
            PreparedStatement pst = conn.prepareStatement("select * from patient where corp_id = ? order by created desc offset ? limit ?");
            pst.setLong(1, corpId);
            pst.setInt(2, offset);
            pst.setInt(3, limit);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                Patient p = new Patient();
                p.setId(rs.getLong("id"));
                p.setCorpId(rs.getLong("corp_id"));
                p.setReference(rs.getString("reference"));
                p.setFirstName(rs.getString("fname"));
                p.setLastName(rs.getString("lname"));
                p.setEmail(rs.getString("email"));
                p.setPhone(rs.getString("phone"));
                p.setCreated((java.util.Date)rs.getTimestamp("created"));
                p.setCreator(rs.getLong("creator"));
                p.setNew_patient(rs.getString("new_patient"));
                p.setCliniko_id(rs.getLong("cliniko_id"));
                p.setSubscriptionId(rs.getLong("subscription_id"));
                p.setSubscriptionDate((java.util.Date)rs.getTimestamp("subscriptiondate"));
                patients.add(p);
            }
        }
        finally {
            conn.close();
        }
        return patients;
    }

    public List<Promotion> getPromotions(int offset, int limit) throws Exception {
        return this.getPromotions(offset, limit, "created desc");
    }

    public List<Promotion> getPromotions(int offset, int limit, String order) throws Exception {
        LinkedList<Promotion> promotions;
        promotions = new LinkedList<Promotion>();
        Connection conn = this.ds.getConnection();
        try {
            PreparedStatement pst = conn.prepareStatement("select * from promotion order by " + order + " offset ? limit ?");
            pst.setInt(1, offset);
            pst.setInt(2, limit);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                Promotion p = new Promotion();
                p.setId(rs.getLong("id"));
                p.setPromo_code(rs.getString("promo_code"));
                p.setExpires((java.util.Date)rs.getTimestamp("expires"));
                p.setDiscount(rs.getLong("discount"));
                p.setEmail(rs.getString("email"));
                p.setSent((java.util.Date)rs.getTimestamp("sent"));
                p.setCompany(rs.getLong("company"));
                p.setAccepted((java.util.Date)rs.getTimestamp("accepted"));
                p.setCreator(rs.getLong("creator"));
                p.setCreated((java.util.Date)rs.getTimestamp("created"));
                promotions.add(p);
            }
        }
        finally {
            conn.close();
        }
        return promotions;
    }

    public List<Patient> getPatientsCreatedThisMonth(long corpId) throws Exception {
		LinkedList<Patient> patients = new LinkedList<Patient>();

		Connection conn = ds.getConnection();
		try {
			PreparedStatement pst = conn.prepareStatement(
					"select * from patient where corp_id = ? and created > ? order by created desc limit 100");
			pst.setLong(1, corpId);

			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.DAY_OF_MONTH, 1);
			Date startOfMonth = cal.getTime();
			pst.setDate(2, new java.sql.Date(startOfMonth.getTime()));
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {
				Patient p = new Patient();
				p.setId(rs.getLong("id"));
				p.setCorpId(rs.getLong("corp_id"));
				p.setReference(rs.getString("reference"));
				p.setFirstName(rs.getString("fname"));
				p.setLastName(rs.getString("lname"));
				p.setEmail(rs.getString("email"));
				p.setPhone(rs.getString("phone"));
				p.setCreated(rs.getTimestamp("created"));
				p.setCreator(rs.getLong("creator"));
				patients.add(p);
			}
		} finally {
			conn.close();
		}

		return patients;
	}


    public int countOfPatientsCreatedThisMonth(long corpId) throws Exception {
        return this.countOfPatientsCreatedMonthsAgo(corpId, 0);
    }

    public int countOfPatientsCreatedLastMonth(long corpId) throws Exception {
        return this.countOfPatientsCreatedMonthsAgo(corpId, 1);
    }

	private int countOfPatientsCreatedMonthsAgo(long corpId, int monthsAgo) throws Exception {
		Connection conn = ds.getConnection();
		int count = 0;
		try {
			PreparedStatement pst = conn.prepareStatement(
					"select count(*) as total from patient where corp_id = ? and created >= ? and created <= ?");
			pst.setLong(1, corpId);

			// starting from
			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.DAY_OF_MONTH, 1);
			if (monthsAgo > 0) {
				cal.add(Calendar.MONTH, monthsAgo * -1);
			}
			Date startOfMonth = cal.getTime();
			pst.setDate(2, new java.sql.Date(startOfMonth.getTime()));

			// ending at
			cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
			Date endOfMonth = cal.getTime();
			pst.setDate(3, new java.sql.Date(endOfMonth.getTime()));

			ResultSet rs = pst.executeQuery();
			if (rs.next()) {
				count = rs.getInt("total");
			}
			rs.close();
			pst.close();
		} finally {
			conn.close();
		}

		return count;
	}

    public int countOfProgramsCreatedThisMonth(long corpId) throws Exception {
        return this.countOfProgramsCreatedMonthsAgo(corpId, 0);
    }

    public int countOfProgramsCreatedLastMonth(long corpId) throws Exception {
        return this.countOfProgramsCreatedMonthsAgo(corpId, 1);
    }

    private int countOfProgramsCreatedMonthsAgo(long corpId, int monthsAgo) throws Exception {
		Connection conn = ds.getConnection();
		int count = 0;
		try {
			PreparedStatement pst = conn.prepareStatement(
					"select count(*) as total from programs where corp_id = ? and created >= ? and created <= ?");
			pst.setLong(1, corpId);

			// starting from
			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.DAY_OF_MONTH, 1);
			if (monthsAgo > 0) {
				cal.add(Calendar.MONTH, monthsAgo * -1);
			}
			Date startOfMonth = cal.getTime();
			pst.setDate(2, new java.sql.Date(startOfMonth.getTime()));

			// ending at
			cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
			Date endOfMonth = cal.getTime();
			pst.setDate(3, new java.sql.Date(endOfMonth.getTime()));

			ResultSet rs = pst.executeQuery();
			if (rs.next()) {
				count = rs.getInt("total");
			}
			rs.close();
			pst.close();
		} finally {
			conn.close();
		}

		return count;
	}

    public int countOfPractitioners(long corpId) throws Exception {
        int count;
        Connection conn = this.ds.getConnection();
        count = 0;
        try {
            PreparedStatement pst = conn.prepareStatement("select count(*) as total from corp_users where corpid = ?");
            pst.setLong(1, corpId);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                count = rs.getInt("total");
            }
            rs.close();
            pst.close();
        }
        finally {
            conn.close();
        }
        return count;
    }

    public int findPatientsCount(long corpId, String query) throws Exception {
        int total;
        total = 0;
        Connection conn = this.ds.getConnection();
        try {
            PreparedStatement pst = conn.prepareStatement("select count(*) as total from patient where corp_id = ? and (lower(reference) like ? or lower(fname) like ? or lower(lname) like ? or lower(email) like ?)");
            pst.setLong(1, corpId);
            pst.setString(2, String.valueOf(query.toLowerCase()) + "%");
            pst.setString(3, String.valueOf(query.toLowerCase()) + "%");
            pst.setString(4, String.valueOf(query.toLowerCase()) + "%");
            pst.setString(5, String.valueOf(query.toLowerCase()) + "%");
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                total = rs.getInt("total");
            }
            rs.close();
            pst.close();
        }
        finally {
            conn.close();
        }
        return total;
    }

    public int findPromotionsCount(String query) throws Exception {
        int total;
        total = 0;
        Connection conn = this.ds.getConnection();
        try {
            PreparedStatement pst = conn.prepareStatement("select count(*) as total from promotion where lower(email) like ?");
            pst.setString(1, String.valueOf(query.toLowerCase()) + "%");
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                total = rs.getInt("total");
            }
            rs.close();
            pst.close();
        }
        finally {
            conn.close();
        }
        return total;
    }

    public List<Patient> findPatients(long corpId, String query, int offset, int limit) throws Exception {
        LinkedList<Patient> patients;
        patients = new LinkedList<Patient>();
        Connection conn = this.ds.getConnection();
        try {
            PreparedStatement pst = conn.prepareStatement("select * from patient where corp_id = ? and (lower(reference) like ? or lower(fname) like ? or lower(lname) like ? or lower(email) like ?) order by lower(fname) asc offset ? limit ?");
            pst.setLong(1, corpId);
            pst.setString(2, String.valueOf(query.toLowerCase()) + "%");
            pst.setString(3, String.valueOf(query.toLowerCase()) + "%");
            pst.setString(4, String.valueOf(query.toLowerCase()) + "%");
            pst.setString(5, String.valueOf(query.toLowerCase()) + "%");
            pst.setInt(6, offset);
            pst.setInt(7, limit);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                Patient p = new Patient();
                p.setId(rs.getLong("id"));
                p.setCorpId(rs.getLong("corp_id"));
                p.setReference(rs.getString("reference"));
                p.setFirstName(rs.getString("fname"));
                p.setLastName(rs.getString("lname"));
                p.setEmail(rs.getString("email"));
                p.setPhone(rs.getString("phone"));
                p.setCreated((java.util.Date)rs.getTimestamp("created"));
                p.setCreator(rs.getLong("creator"));
                p.setNew_patient(rs.getString("new_patient"));
                p.setCliniko_id(rs.getLong("cliniko_id"));
                p.setSubscriptionId(rs.getLong("subscription_id"));
                p.setSubscriptionDate((java.util.Date)rs.getTimestamp("subscriptiondate"));
                patients.add(p);
            }
            rs.close();
            pst.close();
        }
        finally {
            conn.close();
        }
        return patients;
    }

    public List<Promotion> getPromotions(String query, int offset, int limit) throws Exception {
        LinkedList<Promotion> promotions;
        promotions = new LinkedList<Promotion>();
        Connection conn = this.ds.getConnection();
        try {
            PreparedStatement pst = conn.prepareStatement("select * from promotion where lower(email) like ? order by lower(email) asc offset ? limit ?");
            pst.setString(1, String.valueOf(query.toLowerCase()) + "%");
            pst.setInt(2, offset);
            pst.setInt(3, limit);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                Promotion p = new Promotion();
                p.setId(rs.getLong("id"));
                p.setPromo_code(rs.getString("promo_code"));
                p.setExpires((java.util.Date)rs.getTimestamp("expires"));
                p.setDiscount(rs.getLong("discount"));
                p.setEmail(rs.getString("email"));
                p.setSent((java.util.Date)rs.getTimestamp("sent"));
                p.setCompany(rs.getLong("company"));
                p.setAccepted((java.util.Date)rs.getTimestamp("accepted"));
                p.setCreator(rs.getLong("creator"));
                p.setCreated((java.util.Date)rs.getTimestamp("created"));
                promotions.add(p);
            }
            rs.close();
            pst.close();
        }
        finally {
            conn.close();
        }
        return promotions;
    }

    public Patient getPatient(long id) throws Exception {
        Patient p;
        p = null;
        Connection conn = this.ds.getConnection();
        try {
            PreparedStatement pst = conn.prepareStatement("select * from patient where id = ?");
            pst.setLong(1, id);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                p = new Patient();
                p.setId(rs.getLong("id"));
                p.setCorpId(rs.getLong("corp_id"));
                p.setReference(rs.getString("reference"));
                p.setFirstName(rs.getString("fname"));
                p.setLastName(rs.getString("lname"));
                p.setEmail(rs.getString("email"));
                p.setPhone(rs.getString("phone"));
                p.setCreated((java.util.Date)rs.getTimestamp("created"));
                p.setCreator(rs.getLong("creator"));
                p.setSubscriptionId(rs.getLong("subscription_id"));
                p.setSubscriptionDate((java.util.Date)rs.getTimestamp("subscriptiondate"));
            }
            rs.close();
        }
        finally {
            conn.close();
        }
        return p;
    }

    public Patient save(Patient p) throws Exception {
        String[] columns = new String[]{"id", "corp_id", "reference", "fname", "lname", "email", "phone", "creator", "created", "new_patient", "cliniko_id", "check_patient_exist", "subscription_id", "subscriptiondate", "isuserloggedinonce"};
        Connection conn = this.ds.getConnection();
        SubscriptionManager sm = new SubscriptionManager();
        long subid = 0;
        try {
            PreparedStatement pst;
            if (p.getId() == 0) {
                p.setId(this.getSequence("patientId", conn));
                pst = conn.prepareStatement(this.createInsert("patient", columns));
            } else {
                pst = conn.prepareStatement(this.createUpdate("patient", columns, "id = ?"));
                pst.setLong(columns.length + 1, p.getId());
                subid = sm.getPatientSubscriptionId(p.getId());
                System.out.println(" In Save patient update::subid::" + subid);
            }
            int i = 0;
            pst.setLong(++i, p.getId());
            pst.setLong(++i, p.getCorpId());
            pst.setString(++i, p.getReference());
            pst.setString(++i, p.getFirstName());
            pst.setString(++i, p.getLastName());
            pst.setString(++i, p.getEmail());
            pst.setString(++i, p.getPhone());
            pst.setLong(++i, p.getCreator());
            pst.setTimestamp(++i, new Timestamp(p.getCreated().getTime()));
            pst.setString(++i, p.getNew_patient());
            pst.setLong(++i, p.getCliniko_id());
            pst.setString(++i, p.getCheck_patient_exist());
            if (p.getSubscriptionId() > 0) {
                System.out.println("in save updated p.getSubscriptionId ==" + p.getSubscriptionId());
                if (subid == p.getSubscriptionId()) {
                    System.out.println("if statement");
                    pst.setLong(++i, p.getSubscriptionId());
                    pst.setTimestamp(++i, (Timestamp)p.getSubscriptionDate());
                    System.out.println(String.valueOf(p.getSubscriptionId()) + " - " + (Timestamp)p.getSubscriptionDate());
                } else {
                    System.out.println("else");
                    pst.setLong(++i, p.getSubscriptionId());
                    java.util.Date now = new java.util.Date();
                    pst.setTimestamp(++i, new Timestamp(now.getTime()));
                    System.out.println(String.valueOf(p.getSubscriptionId()) + " - " + new Timestamp(now.getTime()));
                }
            } else {
                System.out.println("elseeeee");
                pst.setLong(++i, 0);
                pst.setTimestamp(++i, null);
            }
            System.out.println("Active:::");
            pst.setBoolean(++i, p.isIsuserloginfirst());
            System.out.println("Before:::" + pst);
            pst.executeUpdate();
            System.out.println("After:::" + pst);
            pst.close();
        }
        finally {
            conn.close();
        }
        return p;
    }

    public Program createProgram(ResultSet rs) throws Exception {
        Program p = new Program();
        p.setId(rs.getLong("id"));
        p.setCorpId(rs.getLong("corp_id"));
        p.setPatientId(rs.getLong("patient"));
        p.setName(rs.getString("name"));
        p.setSymptom(rs.getString("symptom"));
        p.setCode(rs.getString("code"));
        p.setInterval(rs.getInt("int_val"));
        p.setIntervalType(rs.getString("int_type"));
        p.setCreated((java.util.Date)rs.getTimestamp("created"));
        p.setCreator(rs.getLong("creator"));
        p.setRetrieved((java.util.Date)rs.getTimestamp("retrieved"));
        p.setLastUpdated((java.util.Date)rs.getTimestamp("lastupdated"));
        p.setGroupId(rs.getLong("group_id"));
        p.setSharelocation(rs.getBoolean("sharelocation"));
        p.setShareprogram(rs.getBoolean("shareprogram"));
        p.setSharegroup(rs.getBoolean("sharegroup"));
        p.setShareid(rs.getLong("shareid"));
        return p;
    }

    public Promotion createPromotion(ResultSet rs) throws Exception {
        Promotion p = new Promotion();
        p.setId(rs.getLong("id"));
        p.setPromo_code(rs.getString("promo_code"));
        p.setExpires((java.util.Date)rs.getTimestamp("expires"));
        p.setDiscount(rs.getLong("discount"));
        p.setEmail(rs.getString("email"));
        p.setSent((java.util.Date)rs.getTimestamp("sent"));
        p.setCompany(rs.getLong("company"));
        p.setAccepted((java.util.Date)rs.getTimestamp("accepted"));
        p.setCreator(rs.getLong("creator"));
        p.setCreated((java.util.Date)rs.getTimestamp("created"));
        return p;
    }

    public PlanPrice createInvoiceNumber(ResultSet rs) throws Exception {
        PlanPrice p = new PlanPrice();
        p.setId(rs.getLong("id"));
        p.setInvoice_number(rs.getString("invoice_number"));
        return p;
    }

    public PlanPrice createPlanPrice(ResultSet rs) throws Exception {
        PlanPrice p = new PlanPrice();
        p.setId(rs.getLong("id"));
        p.setPlan_name(rs.getString("plan_name"));
        p.setCents_per_sms(rs.getLong("cents_per_sms"));
        p.setMax_users(rs.getLong("max_users"));
        p.setMax_locations(rs.getLong("max_locations"));
        p.setPractice_logo(rs.getBoolean("practice_logo"));
        p.setPush_notification(rs.getBoolean("push_notification"));
        p.setCurrency_type(rs.getString("currency_type"));
        p.setTax(rs.getBigDecimal("tax"));
        p.setPrice(rs.getBigDecimal("price"));
        p.setType(rs.getString("type"));
        p.setInvoice_number(rs.getString("invoice_number"));
        p.setCreator(rs.getLong("creator"));
        p.setCreated((java.util.Date)rs.getTimestamp("created"));
        return p;
    }

    public void deleteExercise(long id) throws Exception {
        Connection conn = this.ds.getConnection();
        try {
            conn.setAutoCommit(false);
            PreparedStatement pst = conn.prepareStatement("delete from program_exercises where exercise = ?");
            pst.setLong(1, id);
            pst.executeUpdate();
            pst.close();
            pst = conn.prepareStatement("delete from exercise_media where exercise = ?");
            pst.setLong(1, id);
            pst.executeUpdate();
            pst.close();
            pst = conn.prepareStatement("delete from bodyparts where exercise = ?");
            pst.setLong(1, id);
            pst.executeUpdate();
            pst.close();
            pst = conn.prepareStatement("delete from exercise_types where exercise = ?");
            pst.setLong(1, id);
            pst.executeUpdate();
            pst.close();
            pst = conn.prepareStatement("delete from exercises where id = ?");
            pst.setLong(1, id);
            pst.executeUpdate();
            pst.close();
            conn.commit();
        }
        finally {
            conn.setAutoCommit(true);
            conn.close();
        }
    }

    public void deletePatient(long id) throws Exception {
        Connection conn = this.ds.getConnection();
        try {
            conn.setAutoCommit(false);
            PreparedStatement pst = conn.prepareStatement("delete from program_info where program in (select id from programs where patient = ?)");
            pst.setLong(1, id);
            pst.executeUpdate();
            pst.close();
            pst = conn.prepareStatement("delete from program_notes where program in (select id from programs where patient = ?)");
            pst.setLong(1, id);
            pst.executeUpdate();
            pst.close();
            pst = conn.prepareStatement("delete from program_exercises where program in (select id from programs where patient = ?)");
            pst.setLong(1, id);
            pst.executeUpdate();
            pst.close();
            pst = conn.prepareStatement("delete from program_messages where program_id in (select id from programs where patient = ?)");
            pst.setLong(1, id);
            pst.executeUpdate();
            pst.close();
            pst = conn.prepareStatement("delete from programs where patient = ?");
            pst.setLong(1, id);
            pst.executeUpdate();
            pst.close();
            pst = conn.prepareStatement("delete from patient_groups where patient_id = ?");
            pst.setLong(1, id);
            pst.executeUpdate();
            pst.close();
            pst = conn.prepareStatement("delete from pwreset_patient where patientid=?");
            pst.setLong(1, id);
            pst.executeUpdate();
            pst.close();
            pst = conn.prepareStatement("delete from patient where id = ?");
            pst.setLong(1, id);
            pst.executeUpdate();
            pst.close();
            pst = conn.prepareStatement("delete from payment_subscription where user_id = ?");
            pst.setLong(1, id);
            pst.executeUpdate();
            pst.close();
            conn.commit();
        }
        finally {
            conn.setAutoCommit(true);
            conn.close();
        }
    }

    public void deleteProgram(long id) throws Exception {
        Connection conn = this.ds.getConnection();
        try {
            conn.setAutoCommit(false);
            PreparedStatement pst = conn.prepareStatement("delete from program_info where program = ?");
            pst.setLong(1, id);
            pst.executeUpdate();
            pst.close();
            pst = conn.prepareStatement("delete from program_notes where program = ?");
            pst.setLong(1, id);
            pst.executeUpdate();
            pst.close();
            pst = conn.prepareStatement("delete from program_exercises where program = ?");
            pst.setLong(1, id);
            pst.executeUpdate();
            pst.close();
            pst = conn.prepareStatement("delete from program_messages where program_id = ?");
            pst.setLong(1, id);
            pst.executeUpdate();
            pst.close();
            pst = conn.prepareStatement("delete from programs where id = ?");
            pst.setLong(1, id);
            pst.executeUpdate();
            pst.close();
            conn.commit();
        }
        finally {
            conn.setAutoCommit(true);
            conn.close();
        }
    }

    public Program getProgram(long id) throws Exception {
        Program p;
        Connection conn = this.ds.getConnection();
        p = null;
        try {
            PreparedStatement pst = conn.prepareStatement("select * from programs where id = ?");
            pst.setLong(1, id);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                p = this.createProgram(rs);
            }
        }
        finally {
            conn.close();
        }
        return p;
    }

    public PlanPrice getPlanPrice(long id) throws Exception {
        PlanPrice p;
        Connection conn = this.ds.getConnection();
        p = null;
        try {
            PreparedStatement pst = conn.prepareStatement("select * from plan_price where id = ?");
            pst.setLong(1, id);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                p = this.createPlanPrice(rs);
            }
        }
        finally {
            conn.close();
        }
        return p;
    }

    public LinkedList<Program> getProgramforAllLocation(long id) throws Exception {
        LinkedList<Program> pl;
        Connection conn = this.ds.getConnection();
        pl = new LinkedList<Program>();
        try {
            PreparedStatement pst = conn.prepareStatement("select * from programs where id = ?");
            pst.setLong(1, id);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                Program p = new Program();
                pl.add(p);
                p.setId(rs.getLong("id"));
                p.setCorpId(rs.getLong("corp_id"));
                p.setPatientId(rs.getLong("patient"));
                p.setName(rs.getString("name"));
                p.setSymptom(rs.getString("symptom"));
                p.setCode(rs.getString("code"));
                p.setInterval(rs.getInt("int_val"));
                p.setIntervalType(rs.getString("int_type"));
                p.setCreated((java.util.Date)rs.getTimestamp("created"));
                p.setCreator(rs.getLong("creator"));
                p.setRetrieved((java.util.Date)rs.getTimestamp("retrieved"));
                p.setLastUpdated((java.util.Date)rs.getTimestamp("lastupdated"));
                p.setGroupId(rs.getLong("group_id"));
                p.setSharelocation(rs.getBoolean("sharelocation"));
            }
        }
        finally {
            conn.close();
        }
        return pl;
    }

    public Program getProgram(String code) throws Exception {
        Program p;
        Connection conn = this.ds.getConnection();
        p = null;
        try {
            PreparedStatement pst = conn.prepareStatement("select * from programs where code = ?");
            pst.setString(1, code);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                p = this.createProgram(rs);
            }
        }
        finally {
            conn.close();
        }
        return p;
    }

    public Promotion getPromotion(String code) throws Exception {
        Promotion p;
        Connection conn = this.ds.getConnection();
        p = null;
        try {
            PreparedStatement pst = conn.prepareStatement("select * from promotion where promo_code = ?");
            pst.setString(1, code);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                p = this.createPromotion(rs);
            }
        }
        finally {
            conn.close();
        }
        return p;
    }

    public PlanPrice getInvoiceNumber(String code) throws Exception {
        PlanPrice p;
        Connection conn = this.ds.getConnection();
        p = null;
        try {
            PreparedStatement pst = conn.prepareStatement("select * from plan_price where invoice_number = ?");
            pst.setString(1, code);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                p = this.createInvoiceNumber(rs);
            }
        }
        finally {
            conn.close();
        }
        return p;
    }

    public List<Program> getPrograms(long patientId) throws Exception {
        LinkedList<Program> p;
        Connection conn = this.ds.getConnection();
        p = new LinkedList<Program>();
        try {
            PreparedStatement pst = conn.prepareStatement("select * from programs where patient = ? order by created desc");
            pst.setLong(1, patientId);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                p.add(this.createProgram(rs));
            }
        }
        finally {
            conn.close();
        }
        return p;
    }

    public List<Program> getProgramsForGroup(long groupId) throws Exception {
        LinkedList<Program> p;
        Connection conn = this.ds.getConnection();
        p = new LinkedList<Program>();
        try {
            PreparedStatement pst = conn.prepareStatement("select * from programs where group_id = ? order by created desc");
            pst.setLong(1, groupId);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                p.add(this.createProgram(rs));
            }
        }
        finally {
            conn.close();
        }
        return p;
    }

    public List<Program> getProgramTemplates() throws Exception {
        return this.getProgramTemplates(0, Boolean.valueOf(true));
    }

    public List<Program> getProgramTemplates(long corpId) throws Exception {
        return this.getProgramTemplates(corpId, Boolean.valueOf(false));
    }

    public List<Program> getProgramTemplates(long corpId, Boolean isAdmin) throws Exception {
        LinkedList<Program> p;
        Connection conn = this.ds.getConnection();
        p = new LinkedList<Program>();
        try {
            PreparedStatement pst;
            if (isAdmin.booleanValue()) {
                pst = conn.prepareStatement("select * from programs where corp_id is null and patient is null and group_id is null order by lower(name) asc");
            } else {
                pst = conn.prepareStatement("select * from programs where ( corp_id = ? or corp_id is null) and patient is null and group_id is null order by lower(name) asc");
                pst.setLong(1, corpId);
            }
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                p.add(this.createProgram(rs));
            }
        }
        finally {
            conn.close();
        }
        return p;
    }

    public String newProgramCode() throws Exception {
        Program p = null;
        String code = null;
        while ((p = this.getProgram(code = Util.randomString((int)6))) != null) {
        }
        return code;
    }

    public Program save(Program p, String copyProgram) throws Exception {
        Connection conn = this.ds.getConnection();
        String[] columns = new String[]{"id", "corp_id", "patient", "name", "symptom", "code", "int_val", "int_type", "created", "creator", "retrieved", "lastupdated", "group_id", "sharelocation", "shareprogram", "sharegroup", "shareid"};
        try {
            try {
                PreparedStatement pst;
                if (p.getId() == 0) {
                    pst = conn.prepareStatement(this.createInsert("programs", columns));
                    p.setId(this.getSequence("programId", conn));
                    p.setCode(this.newProgramCode());
                } else {
                    pst = conn.prepareStatement(this.createUpdate("programs", columns, "id = ?"));
                    pst.setLong(columns.length + 1, p.getId());
                }
                int i = 0;
                pst.setLong(++i, p.getId());
                if (p.getCorpId() == 0) {
                    pst.setNull(++i, -5);
                } else {
                    pst.setLong(++i, p.getCorpId());
                }
                if (p.getPatientId() == 0) {
                    pst.setNull(++i, -5);
                } else {
                    pst.setLong(++i, p.getPatientId());
                }
                pst.setString(++i, p.getName());
                pst.setString(++i, p.getSymptom());
                pst.setString(++i, p.getCode());
                pst.setInt(++i, p.getInterval());
                pst.setString(++i, p.getIntervalType() != null ? p.getIntervalType().name() : null);
                pst.setTimestamp(++i, new Timestamp(new java.util.Date().getTime()));
                pst.setLong(++i, p.getCreator());
                if (p.getRetrieved() != null) {
                    pst.setTimestamp(++i, new Timestamp(p.getRetrieved().getTime()));
                } else {
                    pst.setNull(++i, 93);
                }
                if (p.getLastUpdated() != null) {
                    pst.setTimestamp(++i, new Timestamp(p.getLastUpdated().getTime()));
                } else {
                    pst.setNull(++i, 93);
                }
                if (p.getGroupId() == 0) {
                    pst.setNull(++i, -5);
                } else {
                    pst.setLong(++i, p.getGroupId());
                }
                pst.setBoolean(++i, p.isSharelocation());
                pst.setBoolean(++i, p.isShareprogram());
                pst.setBoolean(++i, p.isSharegroup());
                if (p.getShareid() > 0) {
                    pst.setLong(++i, p.getShareid());
                } else {
                    pst.setLong(++i, p.getId());
                }
                pst.executeUpdate();
                pst.close();
                if (!p.isSharelocation() && copyProgram == null) {
                    PreparedStatement pstDel = conn.prepareStatement("select id from programs where name = ? and not id = ? and (shareid = ? or id = ?) and patient is null and group_id is null");
                    pstDel.setString(1, p.getName());
                    pstDel.setLong(2, p.getId());
                    pstDel.setLong(3, p.getShareid());
                    pstDel.setLong(4, p.getId());
                    ResultSet rs = pstDel.executeQuery();
                    while (rs.next()) {
                        conn.setAutoCommit(true);
                        pstDel = conn.prepareStatement("delete from program_notes where program = ?");
                        pstDel.setLong(1, rs.getLong("id"));
                        pstDel.executeUpdate();
                        pstDel.close();
                        pstDel = conn.prepareStatement("delete from program_exercises where program = ?");
                        pstDel.setLong(1, rs.getLong("id"));
                        pstDel.executeUpdate();
                        pstDel.close();
                        pstDel = conn.prepareStatement("delete from program_info where program = ?");
                        pstDel.setLong(1, rs.getLong("id"));
                        pstDel.executeUpdate();
                        pstDel.close();
                    }
                    pstDel = conn.prepareStatement("DELETE FROM programs WHERE name = ? and not id= ? and (shareid = ? or id = ?) and patient is null and group_id is null");
                    pstDel.setString(1, p.getName());
                    pstDel.setLong(2, p.getId());
                    pstDel.setLong(3, p.getShareid());
                    pstDel.setLong(4, p.getId());
                    pstDel.executeUpdate();
                    pstDel.close();
                }
            }
            catch (Exception e) {
                e.printStackTrace();
                conn.close();
            }
        }
        finally {
            conn.close();
        }
        return p;
    }

    public Program save(Program p) throws Throwable {
        block19 : {
            Connection conn;
            conn = this.ds.getConnection();
            String[] columns = new String[]{"id", "corp_id", "patient", "name", "symptom", "code", "int_val", "int_type", "created", "creator", "retrieved", "lastupdated", "group_id", "sharelocation", "shareprogram", "sharegroup", "shareid"};
            try {
                try {
                    PreparedStatement pst;
                    System.out.println("inside program  save method  =====>"+p);
                    if (p.getId() == 0) {
                        System.out.println("Save Before adding exercise in program in save");
                        pst = conn.prepareStatement(this.createInsert("programs", columns));
                        p.setId(this.getSequence("programId", conn));
                        p.setCode(this.newProgramCode());
                    } else {
                        System.out.println("Save Before update exercise in program in save");
                        pst = conn.prepareStatement(this.createUpdate("programs", columns, "id = ?"));
                        pst.setLong(columns.length + 1, p.getId());
                    }
                    int i = 0;
                    pst.setLong(++i, p.getId());
                    if (p.getCorpId() == 0) {
                        pst.setNull(++i, -5);
                    } else {
                        pst.setLong(++i, p.getCorpId());
                    }
                    if (p.getPatientId() == 0) {
                        pst.setNull(++i, -5);
                    } else {
                        pst.setLong(++i, p.getPatientId());
                    }
                    pst.setString(++i, p.getName());
                    pst.setString(++i, p.getSymptom());
                    pst.setString(++i, p.getCode());
                    pst.setInt(++i, p.getInterval());
                    pst.setString(++i, p.getIntervalType() != null ? p.getIntervalType().name() : null);
                    pst.setTimestamp(++i, new Timestamp(p.getCreated().getTime()));
                    pst.setLong(++i, p.getCreator());
                    if (p.getRetrieved() != null) {
                        pst.setTimestamp(++i, new Timestamp(p.getRetrieved().getTime()));
                    } else {
                        pst.setNull(++i, 93);
                    }
                    if (p.getLastUpdated() != null) {
                        pst.setTimestamp(++i, new Timestamp(p.getLastUpdated().getTime()));
                    } else {
                        pst.setNull(++i, 93);
                    }
                    if (p.getGroupId() == 0) {
                        pst.setNull(++i, -5);
                    } else {
                        pst.setLong(++i, p.getGroupId());
                    }
                    pst.setBoolean(++i, p.isSharelocation());
                    pst.setBoolean(++i, p.isShareprogram());
                    pst.setBoolean(++i, p.isSharegroup());
                    if (p.getShareid() > 0) {
                        pst.setLong(++i, p.getShareid());
                    } else {
                        pst.setLong(++i, p.getId());
                    }
                    pst.executeUpdate();
                    System.out.println("After save  exercise in program in save");
                    pst.close();
                }
                catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("handle exception in program " + e.getMessage());
                    conn.close();
                    break block19;
                }
            }
            catch (Throwable throwable) {
                conn.close();
                throw throwable;
            }
            conn.close();
        }
        System.out.println("Save After adding exercise in program");
        return p;
    }

    public Program saveJSON(Program p) throws Exception {
        Connection conn = this.ds.getConnection();
        String[] columns = new String[]{"id", "corp_id", "patient", "name", "symptom", "code", "int_val", "int_type", "created", "creator", "retrieved", "lastupdated", "group_id", "sharelocation", "shareprogram", "sharegroup", "shareid"};
        try {
            try {
                PreparedStatement pst;
                if (p.getId() == 0) {
                    pst = conn.prepareStatement(this.createInsert("programs", columns));
                    p.setId(this.getSequence("programId", conn));
                    p.setCode(this.newProgramCode());
                } else {
                    pst = conn.prepareStatement(this.createUpdate("programs", columns, "id = ?"));
                    pst.setLong(columns.length + 1, p.getId());
                }
                int i = 0;
                pst.setLong(++i, p.getId());
                if (p.getCorpId() == 0) {
                    pst.setNull(++i, -5);
                } else {
                    pst.setLong(++i, p.getCorpId());
                }
                if (p.getPatientId() == 0) {
                    pst.setNull(++i, -5);
                } else {
                    pst.setLong(++i, p.getPatientId());
                }
                pst.setString(++i, p.getName());
                pst.setString(++i, p.getSymptom());
                pst.setString(++i, p.getCode());
                pst.setInt(++i, p.getInterval());
                pst.setString(++i, p.getIntervalType() != null ? p.getIntervalType().name() : null);
                pst.setTimestamp(++i, new Timestamp(p.getCreated().getTime()));
                pst.setLong(++i, p.getCreator());
                if (p.getRetrieved() != null) {
                    pst.setTimestamp(++i, new Timestamp(p.getRetrieved().getTime()));
                } else {
                    pst.setNull(++i, 93);
                }
                if (p.getLastUpdated() != null) {
                    pst.setTimestamp(++i, new Timestamp(p.getLastUpdated().getTime()));
                } else {
                    pst.setNull(++i, 93);
                }
                if (p.getGroupId() == 0) {
                    pst.setNull(++i, -5);
                } else {
                    pst.setLong(++i, p.getGroupId());
                }
                pst.setBoolean(++i, p.isSharelocation());
                pst.setBoolean(++i, p.isShareprogram());
                pst.setBoolean(++i, p.isSharegroup());
                if (p.getShareid() > 0) {
                    pst.setLong(++i, p.getShareid());
                } else {
                    pst.setLong(++i, p.getId());
                }
                pst.executeUpdate();
                pst.close();
            }
            catch (Exception e) {
                e.printStackTrace();
                conn.close();
            }
        }
        finally {
            conn.close();
        }
        return p;
    }

    public int nextExerciseSequence(long programId) throws Exception {
        int seq;
        Connection conn = null;
        seq = 1;
        try {
            conn = this.ds.getConnection();
            PreparedStatement pst = conn.prepareStatement("select max(seq) as seq from program_exercises where program = ?");
            pst.setLong(1, programId);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                seq = rs.getInt("seq") + 1;
            }
            rs.close();
            pst.close();
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return seq;
    }

    public void deleteProgramExercise(long id) throws Exception {
        Connection conn = this.ds.getConnection();
        try {
            conn.setAutoCommit(false);
            PreparedStatement pst = conn.prepareStatement("select program from program_exercises where id = ?");
            pst.setLong(1, id);
            ResultSet rs = pst.executeQuery();
            long program = 0;
            if (rs.next()) {
                program = rs.getLong("program");
            }
            rs.close();
            pst.close();
            if (program == 0) {
                return;
            }
            pst = conn.prepareStatement("delete from program_exercises where id = ?");
            pst.setLong(1, id);
            pst.executeUpdate();
            pst.close();
            pst = conn.prepareStatement("select id from program_exercises where program = ? order by seq asc");
            pst.setLong(1, program);
            rs = pst.executeQuery();
            int i = 0;
            PreparedStatement pu = conn.prepareStatement("update program_exercises set seq = ? where id = ?");
            while (rs.next()) {
                pu.setLong(1, ++i);
                pu.setLong(2, rs.getLong("id"));
                pu.executeUpdate();
            }
            pu.close();
            rs.close();
            pst.close();
            conn.commit();
        }
        finally {
            conn.setAutoCommit(true);
            conn.close();
        }
    }

    public ProgramExercise getProgramExercise(long id) throws Exception {
        ProgramExercise p;
        Connection conn = this.ds.getConnection();
        p = null;
        try {
            PreparedStatement pst = conn.prepareStatement("select * from program_exercises where id = ?");
            pst.setLong(1, id);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                p = new ProgramExercise();
                p.setId(rs.getLong("id"));
                p.setProgramId(rs.getLong("program"));
                p.setExerciseId(rs.getLong("exercise"));
                p.setSequence(rs.getInt("seq"));
                p.setType(rs.getString("type"));
                p.setName(rs.getString("name"));
                p.setSets(rs.getInt("sets"));
                p.setReps(rs.getInt("reps"));
                p.setHolds(rs.getInt("holds"));
                p.setRest(rs.getInt("rest"));
                p.setSide(rs.getString("side"));
                p.setBase(rs.getString("base"));
                p.setDescription(rs.getString("description"));
                p.setShowSides(rs.getBoolean("show_sides"));
                p.setWeight(rs.getInt("weight"));
                System.out.println("edit ex list value" + (Object)p);
            }
        }
        finally {
            conn.close();
        }
        return p;
    }

    public ProgramExercise getProgramExerciseforProgram(long id, String name) throws Exception {
        ProgramExercise p;
        Connection conn = this.ds.getConnection();
        p = null;
        try {
        	System.out.println("getProgramExerciseforProgram ");
            PreparedStatement pst = conn.prepareStatement("select * from program_exercises where program = ? and name = ?");
            pst.setLong(1, id);
            pst.setString(2, name);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                p = new ProgramExercise();
                p.setId(rs.getLong("id"));
                p.setProgramId(rs.getLong("program"));
                p.setExerciseId(rs.getLong("exercise"));
                p.setSequence(rs.getInt("seq"));
                p.setType(rs.getString("type"));
                p.setName(rs.getString("name"));
                p.setSets(rs.getInt("sets"));
                p.setReps(rs.getInt("reps"));
                p.setHolds(rs.getInt("holds"));
                p.setRest(rs.getInt("rest"));
                p.setSide(rs.getString("side"));
                p.setBase(rs.getString("base"));
                p.setDescription(rs.getString("description"));
                p.setShowSides(rs.getBoolean("show_sides"));
            }
        }
        finally {
            conn.close();
        }
        return p;
    }

    public List<ProgramExercise> getProgramExercises(long programId) throws Exception {
        LinkedList<ProgramExercise> pl;
        Connection conn = this.ds.getConnection();
        pl = new LinkedList<ProgramExercise>();
        try {
            PreparedStatement pst = conn.prepareStatement("select pe.*, em.id as media, em.version as media_version from program_exercises pe left join exercise_media em on pe.exercise = em.exercise and key_photo = true where program = ? order by seq asc");
            pst.setLong(1, programId);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                ProgramExercise p = new ProgramExercise();
                pl.add(p);
                p.setId(rs.getLong("id"));
                p.setProgramId(rs.getLong("program"));
                p.setExerciseId(rs.getLong("exercise"));
                p.setSequence(rs.getInt("seq"));
                p.setType(rs.getString("type"));
                p.setName(rs.getString("name"));
                p.setSets(rs.getInt("sets"));
                p.setReps(rs.getInt("reps"));
                p.setHolds(rs.getInt("holds"));
                p.setRest(rs.getInt("rest"));
                p.setSide(rs.getString("side"));
                p.setDescription(rs.getString("description"));
                p.setKeyPhotoId(Long.valueOf(rs.getLong("media")));
                p.setBase(rs.getString("base"));
                p.setKeyPhotoVersion(rs.getInt("media_version"));
                p.setShowSides(rs.getBoolean("show_sides"));
            }
        }
        finally {
            conn.close();
        }
        return pl;
    }

    public List<Program> getProgramCodes(String email) throws Exception {
        LinkedList<Program> pl;
        Connection conn = this.ds.getConnection();
        pl = new LinkedList<Program>();
        try {
            PreparedStatement pst = conn.prepareStatement("select p.code, p.corp_id from programs p join patient pa on pa.id = p.patient where pa.email = ?");
            pst.setString(1, email);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                Program p = new Program();
                pl.add(p);
                p.setCode(rs.getString("code"));
                p.setCorpId(rs.getLong("corp_id"));
            }
        }
        finally {
            conn.close();
        }
        return pl;
    }

    public ProgramExercise save(ProgramExercise p) throws Exception {
        Connection conn = this.ds.getConnection();
        String[] columns = new String[]{"id", "program", "exercise", "seq", "type", "name", "sets", "reps", "holds", "rest", "side", "description", "base", "show_sides", "maxreps", "maxsets", "weight"};
        try {
            try {
                PreparedStatement pst;
                System.out.println("inside program exercise save method");
                boolean isNewExercise = false;
                if (p.getId() == 0) {
                    System.out.println("before adding save setting in program exercise");
                    pst = conn.prepareStatement(this.createInsert("program_exercises", columns));
                    p.setId(this.getSequence("programExerciseId", conn));
                    isNewExercise = true;
                } else {
                    System.out.println("before update save setting in program exercise");
                    System.out.println("program exercise id" + p.getId());
                    pst = conn.prepareStatement(this.createUpdate("program_exercises", columns, "id = ?"));
                    pst.setLong(columns.length + 1, p.getId());
                    System.out.println("pst program exercise id" + pst);
                }
                int i = 0;
                pst.setLong(++i, p.getId());
                pst.setLong(++i, p.getProgramId());
                if (p.getExerciseId() > 0) {
                    pst.setLong(++i, p.getExerciseId());
                } else {
                    pst.setNull(++i, -5);
                }
                pst.setInt(++i, p.getSequence());
                pst.setString(++i, p.getType());
                pst.setString(++i, p.getName());
                pst.setInt(++i, p.getSets());
                pst.setInt(++i, p.getReps());
                pst.setInt(++i, p.getHolds());
                pst.setInt(++i, p.getRest());
                pst.setString(++i, p.getSide());
                pst.setString(++i, p.getDescription());
                pst.setString(++i, p.getBase());
                pst.setBoolean(++i, p.isShowSides());
                if (isNewExercise) {
                    pst.setInt(++i, 0);
                    pst.setInt(++i, 0);
                } else {
                    if (p.getReps() < p.getMaxReps()) {
                        pst.setInt(++i, p.getReps());
                    } else {
                        pst.setInt(++i, p.getMaxReps());
                    }
                    if (p.getSets() < p.getMaxSets()) {
                        pst.setInt(++i, p.getSets());
                    } else {
                        pst.setInt(++i, p.getMaxSets());
                    }
                }
                pst.setInt(++i, p.getWeight());
                System.out.println("Save setting sets" + p.getName());
                System.out.println("After adding save setting in program exercise" + pst);
                pst.executeUpdate();
                System.out.println("After adding save setting in program exercise");
                pst.close();
            }
            catch (Exception e) {
                e.printStackTrace();
                System.out.println("handle exception in program exercise" + e.getMessage());
                conn.close();
            }
        }
        finally {
            conn.close();
        }
        return p;
    }

    public void swapExercises(long ex1, long ex2) throws Exception {
        Connection conn = null;
        try {
            conn = this.ds.getConnection();
            conn.setAutoCommit(false);
            int seq1 = 0;
            int seq2 = 0;
            PreparedStatement pst = conn.prepareStatement("select seq from program_exercises where id = ?");
            pst.setLong(1, ex1);
            System.out.println("Exercise id::" + pst);
            ResultSet rs = pst.executeQuery();
            if (!rs.next()) {
                return;
            }
            System.out.println("Seq updated succesfully");
            seq1 = rs.getInt("seq") + 1;
            rs.close();
            pst.setLong(1, ex2);
            rs = pst.executeQuery();
            if (!rs.next()) {
                return;
            }
            seq2 = rs.getInt("seq");
            rs.close();
            pst.close();
            pst = conn.prepareStatement("update program_exercises set seq = ? where id = ?");
            pst.setInt(1, seq2);
            pst.setLong(2, ex1);
            pst.executeUpdate();
            System.out.println("Ex1 execise updated successly AF" + pst);
            pst.setInt(1, seq1);
            pst.setLong(2, ex2);
            System.out.println("Execise updated successly bf");
            pst.executeUpdate();
            System.out.println("EX2 Execise updated successly AF" + pst);
            pst.close();
            conn.commit();
            System.out.println("program_exercises set seq updated succesfully");
        }
        finally {
            if (conn != null) {
                conn.setAutoCommit(true);
                conn.close();
            }
        }
    }

    public List<MyInfo> listInfo(long corpId, Exercise.BodyPart bp, Exercise.Type type) throws Exception {
        return this.listInfo(corpId, bp, type, false);
    }

    public List<MyInfo> listInfo(long corpId, Exercise.BodyPart bp, Exercise.Type type, boolean exclusive) throws Exception {
        LinkedList<MyInfo> myInfos;
        myInfos = new LinkedList<MyInfo>();
        Connection conn = this.ds.getConnection();
        try {
            String sql = "select i.*, a.id as aid, version, filename from info i left join attachments a on photo = a.id";
            sql = corpId > 0 && exclusive ? String.valueOf(sql) + " where corp_id = " + corpId : (corpId > 0 ? String.valueOf(sql) + " where (corp_id = " + corpId + " or corp_id is null)" : String.valueOf(sql) + " where corp_id is null");
            if (bp != null) {
                sql = String.valueOf(sql) + " and i.id in (select info from info_bodyparts where bodypart = '" + bp.name() + "')";
            }
            if (type != null) {
                sql = String.valueOf(sql) + " and i.id in (select info from info_exercise_types where type = '" + type.name() + "')";
            }
            sql = String.valueOf(sql) + " order by lower(headline) asc";
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                MyInfo info = new MyInfo();
                info.setId(rs.getLong("id"));
                info.setLink("link");
                info.setBody(rs.getString("body"));
                info.setCorpId(rs.getLong("corp_id"));
                info.setHeadline(rs.getString("headline"));
                info.setPhotoId(rs.getLong("photo"));
                if (rs.getLong("aid") > 0) {
                    Attachment a = new Attachment();
                    a.setId(rs.getLong("aid"));
                    a.setVersion(rs.getLong("version"));
                    a.setFilename(rs.getString("filename"));
                    info.setAttachment(a);
                }
                myInfos.add(info);
            }
            rs.close();
            st.close();
        }
        finally {
            conn.close();
        }
        return myInfos;
    }

    public List<MyInfo> listInfoForProgram(long program) throws Exception {
        LinkedList<MyInfo> myInfos;
        myInfos = new LinkedList<MyInfo>();
        Connection conn = this.ds.getConnection();
        try {
            String sql = "select i.*, a.id as aid, version, filename from info i left join attachments a on photo = a.id where i.id in (select info from program_info where program = ?) order by lower(headline) asc";
            PreparedStatement st = conn.prepareStatement(sql);
            st.setLong(1, program);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                MyInfo info = new MyInfo();
                info.setId(rs.getLong("id"));
                info.setBody(rs.getString("body"));
                info.setCorpId(rs.getLong("corp_id"));
                info.setHeadline(rs.getString("headline"));
                info.setPhotoId(rs.getLong("photo"));
                info.setLink("link");
                if (rs.getLong("aid") > 0) {
                    Attachment a = new Attachment();
                    a.setId(rs.getLong("aid"));
                    a.setVersion(rs.getLong("version"));
                    a.setFilename(rs.getString("filename"));
                    info.setAttachment(a);
                }
                myInfos.add(info);
            }
            rs.close();
            st.close();
        }
        finally {
            conn.close();
        }
        return myInfos;
    }

    public MyInfo getInfo(long id) throws Exception {
        MyInfo info;
        info = null;
        Connection conn = this.ds.getConnection();
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("select i.*, a.id as aid, version, filename from info i left join attachments a on photo = a.id where i.id = " + id);
            if (rs.next()) {
                info = new MyInfo();
                info.setId(rs.getLong("id"));
                info.setBody(rs.getString("body"));
                info.setLink(rs.getString("link"));
                info.setCorpId(rs.getLong("corp_id"));
                info.setHeadline(rs.getString("headline"));
                info.setPhotoId(rs.getLong("photo"));
                info.setSharelocation(rs.getBoolean("sharelocation"));
                if (rs.getLong("aid") > 0) {
                    Attachment a = new Attachment();
                    a.setId(rs.getLong("aid"));
                    a.setVersion(rs.getLong("version"));
                    a.setFilename(rs.getString("filename"));
                    info.setAttachment(a);
                }
            }
            rs.close();
            st.close();
            if (info != null) {
                PreparedStatement pst = conn.prepareStatement("select bodypart from info_bodyparts where info = ?");
                pst.setLong(1, info.getId());
                rs = pst.executeQuery();
                info.setBodyParts(new LinkedList());
                while (rs.next()) {
                    info.getBodyParts().add(Exercise.BodyPart.valueOf((String)rs.getString("bodypart")));
                }
                rs.close();
                pst.close();
                pst = conn.prepareStatement("select type from info_exercise_types where info = ?");
                pst.setLong(1, info.getId());
                rs = pst.executeQuery();
                info.setTypes(new LinkedList());
                while (rs.next()) {
                    info.getTypes().add(Exercise.Type.valueOf((String)rs.getString("type")));
                }
                rs.close();
                pst.close();
            }
        }
        finally {
            conn.close();
        }
        return info;
    }

    public MyInfo save(MyInfo info) throws Exception {
        String[] columns = new String[]{"id", "corp_id", "headline", "body", "link", "photo", "sharelocation", "shareid"};
        Connection conn = this.ds.getConnection();
        try {
            PreparedStatement pst;
            if (info.getId() == 0) {
                info.setId(this.getSequence("infoId", conn));
                pst = conn.prepareStatement(this.createInsert("info", columns));
            } else {
                pst = conn.prepareStatement(this.createUpdate("info", columns, "id=?"));
                pst.setLong(columns.length + 1, info.getId());
            }
            int i = 0;
            pst.setLong(++i, info.getId());
            if (info.getCorpId() > 0) {
                pst.setLong(++i, info.getCorpId());
            } else {
                pst.setNull(++i, -5);
            }
            pst.setString(++i, info.getHeadline());
            pst.setString(++i, info.getBody());
            pst.setString(++i, info.getLink());
            if (info.getPhotoId() > 0) {
                pst.setLong(++i, info.getPhotoId());
            } else {
                pst.setNull(++i, -5);
            }
            pst.setBoolean(++i, info.isSharelocation());
            if (info.getShareid() > 0) {
                pst.setLong(++i, info.getShareid());
            } else {
                pst.setLong(++i, info.getId());
            }
            pst.executeUpdate();
            if (!info.isSharelocation()) {
                PreparedStatement pstDel = conn.prepareStatement("select id from info where headline = ? and not id = ? and (shareid = ? or id = ?)");
                pstDel.setString(1, info.getHeadline());
                pstDel.setLong(2, info.getId());
                pstDel.setLong(3, info.getShareid());
                pstDel.setLong(4, info.getId());
                ResultSet rs = pstDel.executeQuery();
                while (rs.next()) {
                    pstDel = conn.prepareStatement("delete from program_info where info = ?");
                    pstDel.setLong(1, rs.getLong("id"));
                    pstDel.executeUpdate();
                    pstDel.close();
                    pstDel = conn.prepareStatement("delete from info_bodyparts where info = ?");
                    pstDel.setLong(1, rs.getLong("id"));
                    pstDel.executeUpdate();
                    pstDel.close();
                    pstDel = conn.prepareStatement("delete from info_exercise_types where info = ?");
                    pstDel.setLong(1, rs.getLong("id"));
                    pstDel.executeUpdate();
                    pstDel.close();
                }
                pstDel = conn.prepareStatement("DELETE FROM info WHERE headline = ? and not id= ? and (shareid = ? or id = ?)");
                pstDel.setString(1, info.getHeadline());
                pstDel.setLong(2, info.getId());
                pstDel.setLong(3, info.getShareid());
                pstDel.setLong(4, info.getId());
                pstDel.executeUpdate();
                pstDel.close();
            }
        }
        finally {
            conn.close();
        }
        return info;
    }

    public void deleteInfo(long id) throws Exception {
        Connection conn = this.ds.getConnection();
        try {
            conn.setAutoCommit(false);
            Statement st = conn.createStatement();
            st.executeUpdate("delete from program_info where info = " + id);
            st.executeUpdate("delete from info_bodyparts where info = " + id);
            st.executeUpdate("delete from info_exercise_types where info = " + id);
            st.executeUpdate("delete from info where id = " + id);
            conn.commit();
            st.close();
        }
        finally {
            conn.setAutoCommit(true);
            conn.close();
        }
    }

    public void addInfo(long program, long info) throws Exception {
        Connection conn = this.ds.getConnection();
        try {
            PreparedStatement pst = conn.prepareStatement("insert into program_info (program, info) values (?, ?)");
            pst.setLong(1, program);
            pst.setLong(2, info);
            pst.execute();
            pst.close();
        }
        finally {
            conn.close();
        }
    }

    public void addNote(long program, String date, String note) throws Exception {
		Connection conn = ds.getConnection();
		try {
			DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			Date myDate = formatter.parse(date);
			java.sql.Date sqlDate = new java.sql.Date(myDate.getTime());
			PreparedStatement pst = conn
					.prepareStatement("insert into program_notes (id, program, date, note) values (?, ?, ?, ?)");
			pst.setLong(1, getSequence("programNoteId", conn));
			pst.setLong(2, program);
			pst.setDate(3, sqlDate);
			pst.setString(4, note);
			pst.execute();
			pst.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			conn.close();
		}
	}
    public void removeInfo(long program, long info) throws Exception {
        Connection conn = this.ds.getConnection();
        try {
            PreparedStatement pst = conn.prepareStatement("delete from program_info where program = ? and info = ?");
            pst.setLong(1, program);
            pst.setLong(2, info);
            pst.execute();
            pst.close();
        }
        finally {
            conn.close();
        }
    }

    public void logSMS(long corpId, String rcpt, String msg) throws Exception {
        Connection conn = this.ds.getConnection();
        try {
            PreparedStatement pst = conn.prepareStatement("insert into sms_log (corp_id, rcpt, message) values (?, ?, ?)");
            pst.setLong(1, corpId);
            pst.setString(2, rcpt);
            pst.setString(3, msg);
            pst.execute();
            pst.close();
            CorporationManager corpManager = new CorporationManager();
            Corporation corp = corpManager.getCorporation(corpId);
            corp.setSmsCount(corp.getSmsCount() - 1);
            corpManager.save(corp, 0);
        }
        finally {
            conn.close();
        }
    }

    public List<Program> recentPrograms(long corpId, int count) throws Exception {
        LinkedList<Program> p;
        Connection conn = this.ds.getConnection();
        p = new LinkedList<Program>();
        try {
            PreparedStatement pst = conn.prepareStatement("select p.*, fname, lname from programs p join patient u on patient = u.id where p.corp_id = ? order by p.created desc limit ?");
            pst.setLong(1, corpId);
            pst.setInt(2, count);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                Program pg = this.createProgram(rs);
                pg.setFirstName(rs.getString("fname"));
                pg.setLastName(rs.getString("lname"));
                p.add(pg);
            }
        }
        finally {
            conn.close();
        }
        return p;
    }


	public List<Program> programsCreatedThisMonth(long corpId) throws Exception {
		Connection conn = ds.getConnection();
		LinkedList<Program> p = new LinkedList<Program>();
		try {
			PreparedStatement pst = conn.prepareStatement(
					"select p.*, fname, lname from programs p join patient u on patient = u.id where p.corp_id = ? and p.created > ? order by p.created desc limit 100");
			pst.setLong(1, corpId);
			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.DAY_OF_MONTH, 1);
			Date startOfMonth = cal.getTime();
			pst.setDate(2, new java.sql.Date(startOfMonth.getTime()));
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {
				Program pg = createProgram(rs);
				pg.setFirstName(rs.getString("fname"));
				pg.setLastName(rs.getString("lname"));
				p.add(pg);

			}
		} finally {
			conn.close();
		}
		return p;
	}

    public boolean companyExists(String name) throws Exception {
        boolean exists;
        exists = false;
        Connection conn = this.ds.getConnection();
        try {
            PreparedStatement pst = conn.prepareStatement("select id from company where lower(name) = ?");
            pst.setString(1, name.toLowerCase());
            ResultSet rs = pst.executeQuery();
            exists = rs.next();
            rs.close();
            pst.close();
        }
        finally {
            conn.close();
        }
        return exists;
    }

    public List<Company> searchCompanies(String q) throws Exception {
        LinkedList<Company> companies;
        companies = new LinkedList<Company>();
        Connection conn = this.ds.getConnection();
        try {
            String sql = "select c.id, c.name, c.contact, c.phone, c.state, c.email, count(p.id) as corpCount from company c left join corporation p on c.id = p.company where lower(c.name) like ? or lower(c.contact) like ? group by c.id, c.name, c.contact, c.phone, c.state, c.email order by lower(c.name) asc";
            PreparedStatement st = conn.prepareStatement(sql);
            st.setString(1, "%" + q.toLowerCase() + "%");
            st.setString(2, "%" + q.toLowerCase() + "%");
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Company c = new Company();
                c.setId(rs.getLong("id"));
                c.setName(rs.getString("name"));
                c.setContact(rs.getString("contact"));
                c.setEmail(rs.getString("email"));
                c.setState(rs.getString("state"));
                c.setPhone(rs.getString("phone"));
                c.setCorpCount(rs.getInt("corpCount"));
                companies.add(c);
            }
            rs.close();
            st.close();
        }
        finally {
            conn.close();
        }
        return companies;
    }

    public void deleteOwnerFromCompany(long companyId, long userId) throws Exception {
        Connection conn = this.ds.getConnection();
        try {
            PreparedStatement pst = conn.prepareStatement("delete from company_owners where company = ? and user_id = ?");
            pst.setLong(1, companyId);
            pst.setLong(2, userId);
            pst.execute();
        }
        finally {
            conn.close();
        }
    }

    public void addOwnerToCompany(long companyId, long userId) throws Exception {
        Connection conn = this.ds.getConnection();
        try {
            PreparedStatement pst = conn.prepareStatement("insert into company_owners (company, user_id) values (?,?)");
            pst.setLong(1, companyId);
            pst.setLong(2, userId);
            pst.execute();
        }
        finally {
            conn.close();
        }
    }

    public List<Company> ExpiryCompanies(int days) throws Exception {
        LinkedList<Company> companies;
        companies = new LinkedList<Company>();
        Connection conn = this.ds.getConnection();
        try {
            String sql = "select c.id, c.name, c.contact, c.phone, c.state, c.email, count(p.id) as corpCount, min(expiry) as expiry from company c left join corporation p on c.id = p.company where c.id in (select company from corporation where expiry is not null and expiry > CURRENT_TIMESTAMP and expiry <= CURRENT_TIMESTAMP + interval '" + days + "  day') group by c.id, c.name, c.contact, c.phone, c.state, c.email order by min(expiry) asc";
            if (days < 0) {
                sql = "select c.id, c.name, c.contact, c.phone, c.state, c.email, count(p.id) as corpCount, min(expiry) as expiry from company c left join corporation p on c.id = p.company where c.id in (select company from corporation where expiry is not null and expiry <= CURRENT_TIMESTAMP - interval '" + days * -1 + "  day') group by c.id, c.name, c.contact, c.phone, c.state, c.email order by min(expiry) asc";
            }
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                Company c = new Company();
                c.setId(rs.getLong("id"));
                c.setName(rs.getString("name"));
                c.setContact(rs.getString("contact"));
                c.setEmail(rs.getString("email"));
                c.setState(rs.getString("state"));
                c.setPhone(rs.getString("phone"));
                c.setCorpCount(rs.getInt("corpCount"));
                c.setExpiry((java.util.Date)rs.getTimestamp("expiry"));
                companies.add(c);
            }
            rs.close();
            st.close();
        }
        finally {
            conn.close();
        }
        return companies;
    }

    public List<Company> listCompanies(int offset, int limit) throws Exception {
        LinkedList<Company> companies;
        companies = new LinkedList<Company>();
        Connection conn = this.ds.getConnection();
        try {
            String sql = "select c.id, c.name, c.contact, c.phone, c.state, c.email, count(p.id) as corpCount from company c left join corporation p on c.id = p.company group by c.id, c.name, c.contact, c.phone, c.state, c.email order by lower(c.name) asc";
            if (limit > 0) {
                sql = String.valueOf(sql) + " offset " + offset + " limit " + limit;
            }
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                Company c = new Company();
                c.setId(rs.getLong("id"));
                c.setName(rs.getString("name"));
                c.setContact(rs.getString("contact"));
                c.setEmail(rs.getString("email"));
                c.setState(rs.getString("state"));
                c.setPhone(rs.getString("phone"));
                c.setCorpCount(rs.getInt("corpCount"));
                companies.add(c);
            }
            rs.close();
            st.close();
        }
        finally {
            conn.close();
        }
        return companies;
    }

    public boolean hasCompanies(long userId) throws Exception {
        boolean companies;
        companies = false;
        Connection conn = this.ds.getConnection();
        try {
            String sql = "select company from company_owners where user_id = ? limit 1";
            PreparedStatement st = conn.prepareStatement(sql);
            st.setLong(1, userId);
            ResultSet rs = st.executeQuery();
            companies = rs.next();
            rs.close();
            st.close();
        }
        finally {
            conn.close();
        }
        return companies;
    }

    public List<Company> listCompaniesForUser(long userId) throws Exception {
        LinkedList<Company> companies;
        companies = new LinkedList<Company>();
        Connection conn = this.ds.getConnection();
        try {
            String sql = "select c.id, c.name, c.contact, c.phone, c.state, c.email, max_corp, count(p.id) as corpCount from company c left join corporation p on c.id = p.company where c.id in (select company from company_owners where user_id = ?) group by c.id, c.name, c.contact, c.phone, c.state, c.email, max_corp order by lower(c.name) asc";
            PreparedStatement st = conn.prepareStatement(sql);
            st.setLong(1, userId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Company c = new Company();
                c.setId(rs.getLong("id"));
                c.setName(rs.getString("name"));
                c.setContact(rs.getString("contact"));
                c.setEmail(rs.getString("email"));
                c.setState(rs.getString("state"));
                c.setPhone(rs.getString("phone"));
                c.setMaxCorp(rs.getInt("max_corp"));
                c.setCorpCount(rs.getInt("corpCount"));
                companies.add(c);
            }
            rs.close();
            st.close();
        }
        finally {
            conn.close();
        }
        return companies;
    }

    public boolean isCompanyOwner(long companyId, long userId) throws Exception {
        boolean isOwner;
        Connection conn = this.ds.getConnection();
        isOwner = false;
        try {
            PreparedStatement pst = conn.prepareStatement("select * from company_owners where company = ? and user_id = ?");
            pst.setLong(1, companyId);
            pst.setLong(2, userId);
            ResultSet rs = pst.executeQuery();
            isOwner = rs.next();
            rs.close();
            pst.close();
        }
        finally {
            conn.close();
        }
        return isOwner;
    }

    public Company getCompany(long id) throws Exception {
        Company c;
        c = null;
        Connection conn = this.ds.getConnection();
        try {
            PreparedStatement pst = conn.prepareStatement("select * from company where id = ?");
            pst.setLong(1, id);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                c = new Company();
                c.setId(rs.getLong("id"));
                c.setName(rs.getString("name"));
                c.setContact(rs.getString("contact"));
                c.setEmail(rs.getString("email"));
                c.setAddress1(rs.getString("address1"));
                c.setAddress2(rs.getString("address2"));
                c.setSuburb(rs.getString("suburb"));
                c.setState(rs.getString("state"));
                c.setPostcode(rs.getString("postcode"));
                c.setPhone(rs.getString("phone"));
                c.setCountry(rs.getString("country"));
                c.setMaxCorp(rs.getInt("max_corp"));
            }
            rs.close();
            pst.close();
        }
        finally {
            conn.close();
        }
        return c;
    }

    public Company save(Company company) throws Exception {
        Connection conn = this.ds.getConnection();
        String[] columns = new String[]{"id", "name", "contact", "email", "address1", "address2", "suburb", "state", "postcode", "country", "phone", "max_corp"};
        try {
            PreparedStatement pst;
            conn.setAutoCommit(false);
            if (company.getId() == 0) {
                company.setId(this.getSequence("companyId", conn));
                pst = conn.prepareStatement(this.createInsert("company", columns));
            } else {
                pst = conn.prepareStatement(this.createUpdate("company", columns, "id=?"));
                pst.setLong(columns.length + 1, company.getId());
            }
            int i = 0;
            pst.setLong(++i, company.getId());
            pst.setString(++i, company.getName());
            pst.setString(++i, company.getContact());
            pst.setString(++i, company.getEmail());
            pst.setString(++i, company.getAddress1());
            pst.setString(++i, company.getAddress2());
            pst.setString(++i, company.getSuburb());
            pst.setString(++i, company.getState());
            pst.setString(++i, company.getPostcode());
            pst.setString(++i, company.getCountry());
            pst.setString(++i, company.getPhone());
            pst.setInt(++i, company.getMaxCorp());
            pst.execute();
            pst.close();
            if (company.getOwners() != null) {
                pst = conn.prepareStatement("delete from company_owners where company = ?");
                pst.setLong(1, company.getId());
                pst.execute();
                pst.close();
                pst = conn.prepareStatement("insert into company_owners (company, user_id) values (?, ?)");
                pst.setLong(1, company.getId());
                for (User u : company.getOwners()) {
                    pst.setLong(2, u.getId());
                    pst.execute();
                }
            }
            pst.close();
            conn.commit();
        }
        finally {
            conn.setAutoCommit(true);
            conn.close();
        }
        return company;
    }

    public ProgramNote save(ProgramNote note) throws Exception {
        Connection conn = this.ds.getConnection();
        String[] columns = new String[]{"id", "program", "date", "note"};
        try {
            PreparedStatement pst;
            if (note.getId() == 0) {
                note.setId(this.getSequence("programNoteId", conn));
                pst = conn.prepareStatement(this.createInsert("program_notes", columns));
            } else {
                pst = conn.prepareStatement(this.createUpdate("program_notes", columns, "id=?"));
                pst.setLong(columns.length + 1, note.getId());
            }
            int i = 0;
            pst.setLong(++i, note.getId());
            pst.setLong(++i, note.getProgramId());
            pst.setTimestamp(++i, new Timestamp(note.getDate().getTime()));
            pst.setString(++i, note.getNote());
            pst.execute();
            pst.close();
        }
        finally {
            conn.close();
        }
        return note;
    }

    public void deleteNote(long noteId) throws Exception {
        Connection conn = this.ds.getConnection();
        try {
            Statement st = conn.createStatement();
            st.execute("delete from program_notes where id = " + noteId);
            st.close();
        }
        finally {
            conn.close();
        }
    }

    public List<ProgramNote> getNotes(long programId) throws Exception {
        LinkedList<ProgramNote> notes;
        Connection conn = this.ds.getConnection();
        notes = new LinkedList<ProgramNote>();
        try {
            PreparedStatement st = conn.prepareStatement("select * from program_notes where program = ? order by date desc");
            st.setLong(1, programId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                ProgramNote n = new ProgramNote();
                n.setId(rs.getLong("id"));
                n.setProgramId(rs.getLong("program"));
                n.setDate((java.util.Date)rs.getTimestamp("date"));
                n.setNote(rs.getString("note"));
                notes.add(n);
            }
        }
        finally {
            conn.close();
        }
        return notes;
    }

    public ProgramNote getNote(long id) throws Exception {
        ProgramNote n;
        Connection conn = this.ds.getConnection();
        n = null;
        try {
            PreparedStatement st = conn.prepareStatement("select * from program_notes where id = ?");
            st.setLong(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                n = new ProgramNote();
                n.setId(rs.getLong("id"));
                n.setProgramId(rs.getLong("program"));
                n.setDate((java.util.Date)rs.getTimestamp("date"));
                n.setNote(rs.getString("note"));
            }
        }
        finally {
            conn.close();
        }
        return n;
    }

    public List<SMSStat> getSMSUageByMonth(long corpId, int limit) throws Exception {
        LinkedList<SMSStat> stats;
        Connection conn = this.ds.getConnection();
        stats = new LinkedList<SMSStat>();
        try {
            String sql = "select to_char(sent, 'YYYY-MM') as month, count(id) as num from sms_log where corp_id = ? group by month order by month desc limit " + limit;
            PreparedStatement pst = conn.prepareStatement(sql);
            pst.setLong(1, corpId);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                SMSStat s = new SMSStat();
                s.setMonth(rs.getString("month"));
                s.setCount(rs.getInt("num"));
                stats.add(s);
            }
        }
        finally {
            conn.close();
        }
        return stats;
    }

    public long copyProgram(long id, long patientId, long groupId, long creator, String copyProgram) throws Exception {
		Program p = getProgram(id);
		List<ProgramExercise> pe = getProgramExercises(id);
		List<MyInfo> infos = listInfoForProgram(id);
		List<ProgramNote> notes = getNotes(id);

		p.setId(0);
		if (patientId > 0)
			p.setPatientId(patientId);
		if (groupId > 0)
			p.setGroupId(groupId);
		p.setCreator(creator);
		save(p, copyProgram);

		for (ProgramExercise e : pe) {
			e.setId(0);
			e.setProgramId(p.getId());
			save(e);
		}

		for (MyInfo info : infos) {
			addInfo(p.getId(), info.getId());
		}

		for (ProgramNote note : notes) {
			addNote(p.getId(), note.getDate().toString(), note.getNote());
		}

		return p.getId();
	}

    public long copyProgramforAllLocations(long id, long creator, long programid) throws Exception {
		Program p = getProgram(id);
		List<ProgramExercise> pe = getProgramExercises(id);
		List<MyInfo> infos = listInfoForProgram(id);
		List<ProgramNote> notes = getNotes(id);

		for (ProgramExercise e : pe) {
			e.setId(0);
			e.setProgramId(programid);
			save(e);
		}

		for (MyInfo info : infos) {
			addInfo(programid, info.getId());
		}

		for (ProgramNote note : notes) {
			addNote(programid, note.getDate().toString(), note.getNote());
		}
		return p.getId();
	}

	public long copyProgramforAllLocationPatients(long id, long creator, long programid) throws Exception {
		Program p = getProgram(id);
		List<ProgramExercise> pe = getProgramExercises(id);
		List<MyInfo> infos = listInfoForProgram(id);
		List<ProgramNote> notes = getNotes(id);

		Connection conn = ds.getConnection();
		PreparedStatement pstDel;
		conn.setAutoCommit(true);

		pstDel = conn.prepareStatement("delete from program_notes where program = ?");
		pstDel.setLong(1, programid);
		pstDel.executeUpdate();
		pstDel.close();

		pstDel = conn.prepareStatement("delete from program_exercises where program = ?");
		pstDel.setLong(1, programid);
		pstDel.executeUpdate();
		pstDel.close();

		pstDel = conn.prepareStatement("delete from program_info where program = ?");
		pstDel.setLong(1, programid);
		pstDel.executeUpdate();
		pstDel.close();
		conn.close();

		for (ProgramExercise e : pe) {
			e.setId(0);
			e.setProgramId(programid);
			save(e);
		}

		for (MyInfo info : infos) {
			addInfo(programid, info.getId());
		}

		for (ProgramNote note : notes) {
			addNote(programid, note.getDate().toString(), note.getNote());
		}
		return p.getId();
	}
    public List<PatientGroup> getPatientGroups(long corpId) throws Exception {
        LinkedList<PatientGroup> patientGroups;
        patientGroups = new LinkedList<PatientGroup>();
        Connection conn = this.ds.getConnection();
        try {
            PreparedStatement pst = conn.prepareStatement("select * from groups where corp_id = ? order by group_name DESC");
            pst.setLong(1, corpId);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                PatientGroup pg = new PatientGroup();
                pg.setId(rs.getLong("id"));
                pg.setCorporationId(Long.valueOf(rs.getLong("corp_id")));
                pg.setGroupName(rs.getString("group_name"));
                patientGroups.add(pg);
            }
        }
        finally {
            conn.close();
        }
        return patientGroups;
    }

    public PatientGroup getPatientGroup(long id) throws Exception {
        PatientGroup pg;
        pg = null;
        Connection conn = this.ds.getConnection();
        try {
            PreparedStatement pst = conn.prepareStatement("select * from groups where id = ?");
            pst.setLong(1, id);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                pg = new PatientGroup();
                pg.setId(rs.getLong("id"));
                pg.setCorporationId(Long.valueOf(rs.getLong("corp_id")));
                pg.setGroupName(rs.getString("group_name"));
            }
            rs.close();
        }
        finally {
            conn.close();
        }
        return pg;
    }

    public List<Patient> getPatientsInGroup(long groupId) throws Exception {
        LinkedList<Patient> patients;
        patients = new LinkedList<Patient>();
        Connection conn = this.ds.getConnection();
        try {
            PreparedStatement pst = conn.prepareStatement("select p.* from patient p join patient_groups pg on p.id = pg.patient_id where pg.group_id = ? order by p.lname ASC");
            pst.setLong(1, groupId);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                Patient p = new Patient();
                p.setId(rs.getLong("id"));
                p.setCorpId(rs.getLong("corp_id"));
                p.setReference(rs.getString("reference"));
                p.setFirstName(rs.getString("fname"));
                p.setLastName(rs.getString("lname"));
                p.setEmail(rs.getString("email"));
                p.setPhone(rs.getString("phone"));
                p.setCreated((java.util.Date)rs.getTimestamp("created"));
                p.setCreator(rs.getLong("creator"));
                patients.add(p);
            }
        }
        finally {
            conn.close();
        }
        return patients;
    }

    public PatientGroup save(PatientGroup pg) throws Exception {
        String[] columns = new String[]{"id", "group_name", "corp_id"};
        Connection conn = this.ds.getConnection();
        try {
            PreparedStatement pst;
            if (pg.getId() == 0) {
                pg.setId(this.getSequence("groupid", conn));
                pst = conn.prepareStatement(this.createInsert("groups", columns));
            } else {
                pst = conn.prepareStatement(this.createUpdate("groups", columns, "id = ?"));
                pst.setLong(columns.length + 1, pg.getId());
            }
            int i = 0;
            pst.setLong(++i, pg.getId());
            pst.setString(++i, pg.getGroupName());
            pst.setLong(++i, pg.getCorporationId());
            pst.executeUpdate();
            pst.close();
        }
        finally {
            conn.close();
        }
        return pg;
    }

    public int countOfPatientsInGroup(long groupId) throws Exception {
        int count;
        Connection conn = this.ds.getConnection();
        count = 0;
        try {
            PreparedStatement pst = conn.prepareStatement("select count(*) as total from patient_groups where group_id = ?");
            pst.setLong(1, groupId);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                count = rs.getInt("total");
            }
            rs.close();
            pst.close();
        }
        finally {
            conn.close();
        }
        return count;
    }

    public void addPatientToGroup(long patientId, long groupId) throws Exception {
        String[] columns = new String[]{"patient_id", "group_id"};
        Connection conn = this.ds.getConnection();
        try {
            PreparedStatement pst = conn.prepareStatement(this.createInsert("patient_groups", columns));
            int i = 0;
            pst.setLong(++i, patientId);
            pst.setLong(++i, groupId);
            pst.executeUpdate();
            pst.close();
        }
        finally {
            conn.close();
        }
    }

    public void removePatientFromGroup(long patientId, long groupId) throws Exception {
        Connection conn = this.ds.getConnection();
        try {
            PreparedStatement pst = conn.prepareStatement("delete from patient_groups where patient_id = ? and group_id = ?");
            pst.setLong(1, patientId);
            pst.setLong(2, groupId);
            pst.executeUpdate();
            pst.close();
        }
        finally {
            conn.close();
        }
    }

    public List<PatientGroup> getGroupsForPatient(long patientId) throws Exception {
        LinkedList<PatientGroup> groups;
        groups = new LinkedList<PatientGroup>();
        Connection conn = this.ds.getConnection();
        try {
            PreparedStatement pst = conn.prepareStatement("select g.* from groups g join patient_groups pg on g.id = pg.group_id where pg.patient_id = ? order by g.group_name DESC");
            pst.setLong(1, patientId);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                PatientGroup pg = new PatientGroup();
                pg.setId(rs.getLong("id"));
                pg.setCorporationId(Long.valueOf(rs.getLong("corp_id")));
                pg.setGroupName(rs.getString("group_name"));
                groups.add(pg);
            }
        }
        finally {
            conn.close();
        }
        return groups;
    }

    public Exercise cloneExerciseToMyExercises(long exerciseId, Corporation corporation, User user, long companyid,
			long uid) throws Exception {
		Exercise toClone = getExercise(exerciseId);

		Exercise newExercise = new Exercise();
		newExercise.setCategory(toClone.getCategory());
		newExercise.setCorpId(corporation.getId());
		newExercise.setCreated(new Date());
		newExercise.setCreator(user.getId());
		newExercise.setDescription(toClone.getDescription());
		newExercise.setName(toClone.getName());
		newExercise.setRepTime(toClone.getRepTime());
		newExercise.setRestTime(toClone.getRestTime());
		newExercise.setSetRestTime(toClone.getSetRestTime());
		newExercise.setShowSides(toClone.isShowSides());
		newExercise.setShareLocation(false);
		newExercise.setHideExercise(true);
		newExercise.setType(toClone.getType());
		String copyExercise = "true";
		newExercise.setShareid(uid);
		newExercise = save(newExercise, companyid, copyExercise);

		// the exercise is now saved and has an id, so we need to recreate all
		// the assets and lists the other one had
		// copy over all the exercise media
		List<ExerciseMedia> mediaFromOldExercise = getExerciseMediaWithData(exerciseId);
		long keyPhotoId = 0;
		int keyPhotoVersion = 0;
		if (mediaFromOldExercise != null) {
			for (ExerciseMedia media : mediaFromOldExercise) {
				media.setId(0);
				media.setExercise(newExercise.getId());
				ExerciseMedia savedMedia = save(media);

				if (media.isKey()) {
					keyPhotoId = savedMedia.getId();
					keyPhotoVersion = savedMedia.getVersion();
				}
			}
		}

		newExercise.setKeyPhotoId(keyPhotoId);
		newExercise.setKeyPhotoVersion(keyPhotoVersion);

		// copy over the body parts
		Connection conn = ds.getConnection();
		if (toClone.getBodyParts() != null) {
			PreparedStatement statement = conn
					.prepareStatement("insert into bodyparts (exercise, bodypart) values (?,?)");
			for (Exercise.BodyPart bodyPart : toClone.getBodyParts()) {
				statement.setLong(1, newExercise.getId());
				statement.setString(2, bodyPart.toString());
				statement.execute();
			}

			statement.close();
		}

		if (toClone.getTypes() != null) {
			PreparedStatement statement = conn
					.prepareStatement("insert into exercise_types (exercise, type) values (?,?)");
			for (Exercise.Type type : toClone.getTypes()) {
				statement.setLong(1, newExercise.getId());
				statement.setString(2, type.toString());
				statement.execute();
			}

			statement.close();
		}

		return newExercise;
	}
    public static boolean validCCNumber(String n) {
        try {
            int j = n.length();
            String[] s1 = new String[j];
            int i = 0;
            while (i < n.length()) {
                s1[i] = "" + n.charAt(i);
                ++i;
            }
            int checksum = 0;
            int i2 = s1.length - 1;
            while (i2 >= 0) {
                int k = 0;
                if (i2 > 0) {
                    k = Integer.valueOf(s1[i2 - 1]) * 2;
                    if (k > 9) {
                        String s = "" + k;
                        k = Integer.valueOf(s.substring(0, 1)) + Integer.valueOf(s.substring(1));
                    }
                    checksum += Integer.valueOf(s1[i2]) + k;
                } else {
                    checksum += Integer.valueOf(s1[0]).intValue();
                }
                i2 -= 2;
            }
            if (checksum % 10 == 0) {
                return true;
            }
            return false;
        }
        catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean getPatient(String email) throws Exception {
        boolean isExists;
        Object p = null;
        isExists = false;
        Connection conn = this.ds.getConnection();
        try {
            PreparedStatement pst = conn.prepareStatement("select * from patient where email = ?");
            pst.setString(1, email);
            ResultSet rs = pst.executeQuery();
            if (!isExists && rs.next()) {
                isExists = true;
            }
            rs.close();
        }
        finally {
            conn.close();
        }
        return isExists;
    }

    public boolean getPatientExist(String email, String password) throws Exception {
        boolean isExists;
        Object p = null;
        isExists = false;
        Connection conn = this.ds.getConnection();
        try {
            PreparedStatement pst = conn.prepareStatement("select * from patient where email = ? and password = ?");
            pst.setString(1, email);
            pst.setString(2, password);
            ResultSet rs = pst.executeQuery();
            if (!isExists && rs.next()) {
                isExists = true;
            }
            rs.close();
        }
        finally {
            conn.close();
        }
        return isExists;
    }

    public boolean getPatientEmailExist(String email) throws Exception {
        boolean isExists;
        Object p = null;
        isExists = false;
        Connection conn = this.ds.getConnection();
        try {
            PreparedStatement pst = conn.prepareStatement("select * from patient where email = ?");
            pst.setString(1, email);
            ResultSet rs = pst.executeQuery();
            if (!isExists && rs.next()) {
                isExists = true;
            }
            rs.close();
        }
        finally {
            conn.close();
        }
        return isExists;
    }
    public List<String> getPatientSubscriptoin(String email) throws Exception {
		boolean isValid = false;
		Connection conn = ds.getConnection();
		List<String> status = new ArrayList<String>();
		try {
			PreparedStatement pst = conn.prepareStatement(
					"select p.subscriptiondate,s.subscription_period_type,s.subscription_period from patient p,subscription s where s.id=p.subscription_id and p.email=?");
			pst.setString(1, email);
			ResultSet rs = pst.executeQuery();
			if (!isValid && rs.next()) {
				int period = 0;
				String type = "";
				Date dt = null;
				DateTime now = null;
				DateTime subscriptionDate = null;
				DateTime expired = null;
				dt = rs.getTimestamp("subscriptiondate");
				type = rs.getString("subscription_period_type");
				period = rs.getInt("subscription_period");
				subscriptionDate = new DateTime(dt);
				now = new DateTime();
				if (type.equalsIgnoreCase("day")) {
					expired = subscriptionDate.plusDays((period));
				} else if (type.equalsIgnoreCase("week")) {
					expired = subscriptionDate.plusWeeks(period);
				} else if (type.equalsIgnoreCase("month")) {
					expired = subscriptionDate.plusMonths(period);
				} else if (type.equalsIgnoreCase("year")) {
					expired = subscriptionDate.plusYears(period);
				}
				if (expired != null) {
					if (now.toLocalDate().isBefore(expired.toLocalDate())) {
						isValid = true;
					}
					status.add(String.valueOf(isValid));
					status.add(expired.minusDays(1).toString());
				}
			}
			rs.close();
			pst.close();
		} finally {
			conn.close();
		}
		return status;
	}

    public List<String> checkPatinetSubscriptionIsVaild(String email) throws Exception {
		boolean user = false;
		boolean isValid = false;
		long subId = 0;
		Connection conn = ds.getConnection();
		List<String> status = new ArrayList<String>();
		try {
			PreparedStatement pst = conn
					.prepareStatement("select id,subscriptiondate,subscription_id from patient where email = ?");
			pst.setString(1, email);
			ResultSet rs = pst.executeQuery();
			if (rs.next()) {
				user = true;
				subId = rs.getLong("subscription_id");
			}
			rs.close();
			pst.close();
			status.add(String.valueOf(user));
			status.add(String.valueOf(subId));
			if (user == true && subId > 1) {
				pst = conn.prepareStatement(
						"select p.subscriptiondate,s.subscription_period_type,s.subscription_period from patient p,subscription s where s.id=p.subscription_id and p.email=?");
				pst.setString(1, email);
				rs = pst.executeQuery();
				if (!isValid && rs.next()) {
					int period = 0;
					String type = "";
					Date dt = null;
					DateTime now = null;
					DateTime subscriptionDate = null;
					DateTime expired = null;
					dt = rs.getTimestamp("subscriptiondate");
					type = rs.getString("subscription_period_type");
					period = rs.getInt("subscription_period");
					subscriptionDate = new DateTime(dt);
					now = new DateTime();
					if (type.equalsIgnoreCase("day")) {
						expired = subscriptionDate.plusDays((period));
					} else if (type.equalsIgnoreCase("week")) {
						expired = subscriptionDate.plusWeeks(period);
					} else if (type.equalsIgnoreCase("month")) {
						expired = subscriptionDate.plusMonths(period);
					} else if (type.equalsIgnoreCase("year")) {
						expired = subscriptionDate.plusYears(period);
					}
					if (expired != null) {
						if (now.toLocalDate().isBefore(expired.toLocalDate())) {
							isValid = true;
						}
					}
				}
				rs.close();
				pst.close();
				conn.close();
			}
			status.add(String.valueOf(isValid));
		} finally {
			conn.close();
		}
		return status;
	}

    public ProgramExercise updateProgramExercise(String exerciseId, String sets, String reps, String time, String repTime, String setRestTime, String seq, String programId, String maxSets, String maxReps) throws Exception {
        ProgramExercise p;
        p = new ProgramExercise();
        Connection conn = this.ds.getConnection();
        long exerciseData = Long.parseLong(exerciseId);
        int seqData = Integer.parseInt(seq);
        int programIdData = Integer.parseInt(programId);
        int setsData = Integer.parseInt(sets);
        int repsData = Integer.parseInt(reps);
        int maxSetsData = Integer.parseInt(maxSets);
        int maxRepsData = Integer.parseInt(maxReps);
        try {
            if (exerciseId != null && exerciseId != null && exerciseId != null) {
                PreparedStatement pst = conn.prepareStatement("select * from program_exercises where exercise = ? and program = ? and seq =  ?");
                pst.setLong(1, exerciseData);
                pst.setInt(2, programIdData);
                pst.setInt(3, seqData);
                ResultSet rs = pst.executeQuery();
                if (rs.next()) {
                    p = new ProgramExercise();
                    p.setId(rs.getLong("id"));
                    p.setProgramId(rs.getLong("program"));
                    p.setExerciseId(rs.getLong("exercise"));
                    p.setSequence(rs.getInt("seq"));
                    p.setType(rs.getString("type"));
                    p.setName(rs.getString("name"));
                    p.setSets(rs.getInt("sets"));
                    p.setReps(rs.getInt("reps"));
                    p.setHolds(rs.getInt("holds"));
                    p.setRest(rs.getInt("rest"));
                    p.setCreatedDate((java.util.Date)new Timestamp(new java.util.Date().getTime()));
                    p.setSide(rs.getString("side"));
                    p.setBase(rs.getString("base"));
                    p.setDescription(rs.getString("description"));
                    p.setShowSides(rs.getBoolean("show_sides"));
                    p.setMaxReps(rs.getInt("maxreps"));
                    p.setMaxSets(rs.getInt("maxsets"));
                }
            }
            System.out.println("maxRepsData" + maxRepsData);
            System.out.println("maxSetsData" + maxSetsData);
            System.out.println("date--" + p.getCreatedDate());
            if (p.getId() != 0) {
                PreparedStatement pst2 = conn.prepareStatement("insert into program_exercises (id,program,exercise,seq,sets,reps,type,maxsets,maxreps,createddate) values (?,?,?,?,?,?,?,?,?,?)");
                pst2.setLong(1, this.getSequence("programexerciseid", conn));
                pst2.setLong(2, programIdData);
                pst2.setLong(3, exerciseData);
                pst2.setInt(4, seqData);
                pst2.setInt(5, setsData);
                pst2.setInt(6, repsData);
                pst2.setString(7, p.getType());
                pst2.setInt(8, maxSetsData);
                pst2.setInt(9, maxRepsData);
                pst2.setTimestamp(10, new Timestamp(new java.util.Date().getTime()));
                pst2.execute();
                pst2.close();
            }
        }
        finally {
            conn.close();
        }
        return p;
    }

    public PaymentSubscription savePaymentSubscription(String totalAmount, String subscriptionid, String transactionId, String transStatus, String email, String corpId) throws Exception {
        PaymentSubscription paymentSubscription;
        Connection conn = this.ds.getConnection();
        Long totalAmountData = null;
        if (StringUtils.isNotBlank((String)totalAmount)) {
            totalAmountData = Long.parseLong(totalAmount);
        }
        java.util.Date toMonth = null;
        Calendar cal = Calendar.getInstance();
        paymentSubscription = new PaymentSubscription();
        Patient p = new Patient();
        try {
            try {
                if (totalAmount != null && subscriptionid != null && transactionId != null && transStatus != null && email != null) {
                    PreparedStatement pst = conn.prepareStatement("select * from patient where email = ?");
                    pst.setString(1, email);
                    ResultSet rs = pst.executeQuery();
                    if (rs.next()) {
                        p.setId(rs.getLong("id"));
                    }
                    paymentSubscription.setId(this.getSequence("paymentsubscriptionid", conn));
                    paymentSubscription.setSubscriptionId(subscriptionid);
                    if (subscriptionid.equals("au.com.fiizioapp.100")) {
                        paymentSubscription.setSubId("0");
                        paymentSubscription.setExpired(null);
                    }
                    if (subscriptionid.equals("au.com.fiiziosub.0")) {
                        paymentSubscription.setSubId("2");
                        cal.add(5, 3);
                        java.util.Date todate1 = cal.getTime();
                        paymentSubscription.setExpired(todate1);
                        System.out.println("Expired" + paymentSubscription.getExpired());
                    }
                    if (subscriptionid.equals("au.com.fiiziosub.1")) {
                        paymentSubscription.setSubId("3");
                        cal.add(2, 1);
                        toMonth = cal.getTime();
                        paymentSubscription.setExpired(toMonth);
                        System.out.println("Expired" + paymentSubscription.getExpired());
                    }
                    if (subscriptionid.equals("au.com.fiiziosub.3")) {
                        paymentSubscription.setSubId("4");
                        cal.add(2, 3);
                        toMonth = cal.getTime();
                        paymentSubscription.setExpired(toMonth);
                        System.out.println("Expired" + paymentSubscription.getExpired());
                    }
                    if (subscriptionid.equals("au.com.fiiziosub.6")) {
                        paymentSubscription.setSubId("5");
                        cal.add(2, 6);
                        toMonth = cal.getTime();
                        paymentSubscription.setExpired(toMonth);
                    }
                    paymentSubscription.setTotalAmount(totalAmountData.longValue());
                    paymentSubscription.setTransactionDate(new java.util.Date());
                    System.out.println(paymentSubscription.getTransactionDate());
                    paymentSubscription.setTransactionId(transactionId);
                    paymentSubscription.setTransStatus(transStatus);
                    paymentSubscription.setUserId(p.getId());
                    Long patientSubid = Long.parseLong(paymentSubscription.getSubId());
                    pst = conn.prepareStatement("update patient set subscription_id = ? where id = ?");
                    pst.setLong(1, patientSubid);
                    pst.setLong(2, paymentSubscription.getUserId());
                    pst.executeUpdate();
                    pst.close();
                    String query = null;
                    if (corpId != null) {
                        Long corpIds = Long.parseLong(corpId);
                        paymentSubscription.setCorpId(corpIds.longValue());
                        query = "insert into payment_subscription (id,total_amount,subscription_id,transaction_id,transaction_status,transaction_date,user_id,sub_id,corp_id,expired) values (?,?,?,?,?,?,?,?,?,?)";
                    } else if (paymentSubscription.getExpired() != null && corpId == null) {
                        query = "insert into payment_subscription (id,total_amount,subscription_id,transaction_id,transaction_status,transaction_date,user_id,sub_id,expired) values (?,?,?,?,?,?,?,?,?)";
                    } else if (paymentSubscription.getExpired() == null) {
                        query = "insert into payment_subscription (id,total_amount,subscription_id,transaction_id,transaction_status,transaction_date,user_id,sub_id) values (?,?,?,?,?,?,?,?)";
                    }
                    PreparedStatement statement = conn.prepareStatement(query);
                    statement.setLong(1, paymentSubscription.getId());
                    statement.setLong(2, paymentSubscription.getTotalAmount());
                    statement.setString(3, paymentSubscription.getSubscriptionId());
                    statement.setString(4, paymentSubscription.getTransactionId());
                    statement.setString(5, paymentSubscription.getTransStatus());
                    statement.setTimestamp(6, new Timestamp(paymentSubscription.getTransactionDate().getTime()));
                    statement.setLong(7, paymentSubscription.getUserId());
                    statement.setString(8, paymentSubscription.getSubId());
                    if (corpId != null) {
                        statement.setLong(9, paymentSubscription.getCorpId());
                        statement.setTimestamp(10, new Timestamp(paymentSubscription.getExpired().getTime()));
                    }
                    if (paymentSubscription.getExpired() != null && corpId == null) {
                        statement.setTimestamp(9, new Timestamp(paymentSubscription.getExpired().getTime()));
                    }
                    statement.execute();
                    statement.close();
                    paymentSubscription.setTotalAmount(paymentSubscription.getTotalAmount());
                    paymentSubscription.setSubscriptionId(subscriptionid);
                    paymentSubscription.setTransactionId(transactionId);
                    paymentSubscription.setUserId(paymentSubscription.getUserId());
                    paymentSubscription.setTransStatus("Success");
                }
            }
            catch (Exception e) {
                e.printStackTrace();
                conn.close();
            }
        }
        finally {
            conn.close();
        }
        return paymentSubscription;
    }

    public Map<String, Boolean> checkPatientPaymentSubscriptionIsVaild(String email) throws Exception {
        HashMap<String, Boolean> map;
        boolean user = false;
        boolean isValid = false;
        boolean noSub = false;
        boolean expired = false;
        long patientId = 0;
        boolean isuserloginfirst = false;
        long subsId = 0;
        String transStatus = "Success";
        map = new HashMap<String, Boolean>();
        Connection conn = this.ds.getConnection();
        try {
            try {
                PreparedStatement pst = conn.prepareStatement("select id,isuserloggedinonce,subscription_id from patient where email = ?");
                pst.setString(1, email);
                ResultSet rs = pst.executeQuery();
                if (rs.next()) {
                    user = true;
                    patientId = rs.getLong("id");
                    isuserloginfirst = rs.getBoolean("isuserloggedinonce");
                    subsId = rs.getLong("subscription_id");
                    System.out.println("isuserloginfirst--value--" + isuserloginfirst);
                    System.out.println("subscription_id--value--" + subsId);
                    java.util.Date transactionDate = new java.util.Date();
                    Timestamp timestamp = new Timestamp(transactionDate.getTime());
                    System.out.println(timestamp.getTime());
                    PaymentSubscription paymentSubscription = new PaymentSubscription();
                    if (!isuserloginfirst && subsId != 0) {
                        isuserloginfirst = true;
                        PreparedStatement pst1 = conn.prepareStatement("UPDATE patient  SET isuserloggedinonce=?  where email = ?");
                        pst1.setBoolean(1, true);
                        pst1.setString(2, email);
                        pst1.execute();
                        pst1.close();
                        Calendar cal = Calendar.getInstance();
                        java.util.Date expiredDate = new java.util.Date();
                        if (subsId == 2) {
                            cal.add(5, 3);
                            expiredDate = cal.getTime();
                        }
                        if (subsId == 3) {
                            cal.add(2, 1);
                            expiredDate = cal.getTime();
                        }
                        if (subsId == 4) {
                            cal.add(2, 3);
                            expiredDate = cal.getTime();
                        }
                        if (subsId == 5) {
                            cal.add(2, 6);
                            expiredDate = cal.getTime();
                        }
                        PreparedStatement pst2 = conn.prepareStatement("UPDATE payment_subscription  SET transaction_date = ?,expired=?  where user_id=?");
                        pst2.setTimestamp(1, new Timestamp(timestamp.getTime()));
                        System.out.println("todaydate====" + new Timestamp(timestamp.getTime()));
                        pst2.setTimestamp(2, new Timestamp(expiredDate.getTime()));
                        pst2.setLong(3, patientId);
                        pst2.execute();
                        pst2.close();
                    }
                }
                map.put("user", user);
                rs.close();
                pst.close();
                PaymentSubscription ps = null;
                PreparedStatement pst1 = conn.prepareStatement("select p.id as id, p.total_amount as totalAmount,p.subscription_id as subscriptionId,p.transaction_id as transactionId,p.transaction_status as transactionStatus,p.transaction_date as transactionDate,p.expired as expiredDate, p.user_id as userId,p.sub_id as subId  from payment_subscription p  where user_id= ? and transaction_status = ?order by id desc limit 1");
                pst1.setLong(1, patientId);
                pst1.setString(2, transStatus);
                ResultSet rs1 = pst1.executeQuery();
                while (rs1.next()) {
                    ps = new PaymentSubscription();
                    ps.setId(rs1.getLong("id"));
                    ps.setTotalAmount(rs1.getLong("totalAmount"));
                    ps.setSubscriptionId(rs1.getString("subscriptionId"));
                    ps.setTransactionId(rs1.getString("transactionId"));
                    ps.setTransStatus(rs1.getString("transactionStatus"));
                    ps.setTransactionDate((java.util.Date)rs1.getTimestamp("transactionDate"));
                    ps.setExpired((java.util.Date)rs1.getTimestamp("expiredDate"));
                    ps.setUserId(rs1.getLong("userId"));
                    ps.setSubId(rs1.getString("subId"));
                }
                if (ps != null) {
                    java.util.Date expiredDate = ps.getExpired();
                    java.util.Date now = new java.util.Date();
                    System.out.println("ps.getSubId()::" + ps.getSubId());
                    if (ps.getSubId().equals("0") || ps.getSubId().equals("1")) {
                        System.err.println("No Subscription");
                        noSub = true;
                        map.put("NoSubscription", noSub);
                    } else if (ps.getSubId().equals("2")) {
                        if (expiredDate.compareTo(now) > 0) {
                            isValid = true;
                        } else if (expiredDate.compareTo(now) < 0) {
                            expired = true;
                            map.put("TrailExpired", expired);
                        } else {
                            isValid = true;
                        }
                    } else if (ps.getSubId().equals("3")) {
                        if (expiredDate.compareTo(now) > 0) {
                            isValid = true;
                        } else if (expiredDate.compareTo(now) < 0) {
                            expired = true;
                            map.put("oneMonthExpired", expired);
                        } else {
                            isValid = true;
                        }
                    } else if (ps.getSubId().equals("4")) {
                        if (expiredDate.compareTo(now) > 0) {
                            isValid = true;
                        } else if (expiredDate.compareTo(now) < 0) {
                            expired = true;
                            map.put("threeMonthExpired", expired);
                        } else {
                            isValid = true;
                        }
                    } else if (ps.getSubId().equals("5")) {
                        if (expiredDate.compareTo(now) > 0) {
                            isValid = true;
                        } else if (expiredDate.compareTo(now) < 0) {
                            expired = true;
                            map.put("sixMonthExpired", expired);
                        } else {
                            isValid = true;
                        }
                    } else {
                        isValid = false;
                    }
                }
                if (!isuserloginfirst) {
                    // empty if block
                }
                map.put("subscription", isValid);
                rs1.close();
                pst1.close();
            }
            catch (Exception e) {
                e.printStackTrace();
                conn.close();
            }
        }
        finally {
            conn.close();
        }
        return map;
    }

    public PaymentSubscription savePaymentSubscription(PaymentSubscription paymentSubscription, String email) throws Exception {
        Connection conn = this.ds.getConnection();
        Calendar cal = Calendar.getInstance();
        try {
            try {
                boolean user = false;
                long patientId = 0;
                java.util.Date toMonth = null;
                PreparedStatement pst = conn.prepareStatement("select id from patient where email = ?");
                pst.setString(1, email);
                ResultSet rs = pst.executeQuery();
                if (rs.next()) {
                    user = true;
                    patientId = rs.getLong("id");
                    System.out.println("patientId" + patientId);
                }
                rs.close();
                pst.close();
                if (user) {
                    if (paymentSubscription.getSubId().equals("0")) {
                        paymentSubscription.setSubscriptionId("au.com.fiizioapp.100");
                        paymentSubscription.setExpired(null);
                    }
                    if (paymentSubscription.getSubId().equals("1")) {
                        paymentSubscription.setSubscriptionId("au.com.fiizioapp.1000");
                        paymentSubscription.setExpired(null);
                    }
                    if (paymentSubscription.getSubId().equals("2")) {
                        paymentSubscription.setSubscriptionId("au.com.fiiziosub.0");
                        cal.add(5, 3);
                        java.util.Date todate1 = cal.getTime();
                        paymentSubscription.setExpired(todate1);
                        System.out.println("Expired" + paymentSubscription.getExpired());
                    }
                    if (paymentSubscription.getSubId().equals("3")) {
                        paymentSubscription.setSubscriptionId("au.com.fiiziosub.1");
                        cal.add(2, 1);
                        toMonth = cal.getTime();
                        paymentSubscription.setExpired(toMonth);
                        System.out.println("Expired" + paymentSubscription.getExpired());
                    }
                    if (paymentSubscription.getSubId().equals("4")) {
                        paymentSubscription.setSubscriptionId("au.com.fiiziosub.3");
                        cal.add(2, 3);
                        toMonth = cal.getTime();
                        paymentSubscription.setExpired(toMonth);
                        System.out.println("Expired" + paymentSubscription.getExpired());
                    }
                    if (paymentSubscription.getSubId().equals("5")) {
                        paymentSubscription.setSubscriptionId("au.com.fiiziosub.6");
                        cal.add(2, 6);
                        toMonth = cal.getTime();
                        paymentSubscription.setExpired(toMonth);
                    }
                    PreparedStatement pst2 = null;
                    pst2 = paymentSubscription.getExpired() != null ? conn.prepareStatement("insert into payment_subscription (id,total_amount,subscription_id,transaction_id,transaction_status,transaction_date,user_id,sub_id,expired) values (?,?,?,?,?,?,?,?,?)") : conn.prepareStatement("insert into payment_subscription (id,total_amount,subscription_id,transaction_id,transaction_status,transaction_date,user_id,sub_id) values (?,?,?,?,?,?,?,?)");
                    pst2.setLong(1, this.getSequence("paymentsubscriptionid", conn));
                    pst2.setLong(2, paymentSubscription.getTotalAmount());
                    pst2.setString(3, paymentSubscription.getSubscriptionId());
                    pst2.setString(4, paymentSubscription.getTransactionId());
                    pst2.setString(5, paymentSubscription.getTransStatus());
                    pst2.setTimestamp(6, new Timestamp(paymentSubscription.getTransactionDate().getTime()));
                    pst2.setLong(7, patientId);
                    pst2.setString(8, paymentSubscription.getSubId());
                    if (paymentSubscription.getExpired() != null) {
                        pst2.setTimestamp(9, new Timestamp(paymentSubscription.getExpired().getTime()));
                    }
                    pst2.execute();
                    pst2.close();
                }
            }
            catch (Exception e) {
                e.printStackTrace();
                conn.close();
            }
        }
        finally {
            conn.close();
        }
        return paymentSubscription;
    }

    public PaymentSubscription getPaymentSubscription(long id) throws Exception {
        PaymentSubscription p;
        Connection conn = this.ds.getConnection();
        Calendar cal = Calendar.getInstance();
        p = null;
        try {
            PreparedStatement pst = conn.prepareStatement("select * from payment_subscription where id = ?");
            pst.setLong(1, id);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                p = new PaymentSubscription();
                p.setId(rs.getLong("id"));
                p.setExpired((java.util.Date)rs.getDate("expired"));
                p.setTransactionDate((java.util.Date)rs.getDate("transactionDate"));
                System.out.println("expired" + p.getExpired());
            }
        }
        finally {
            conn.close();
        }
        return p;
    }

    public Exercise save(Exercise e, long companyId) {
        block20 : {
            String[] columns = new String[]{"id", "category", "corp_id", "name", "repTime", "reps", "restTime", "setRestTime", "description", "created", "creator", "type", "show_sides", "sharelocation", "hideexercise", "shareid", "sets", "holds", "rest", "side", "weight"};
            System.out.println("name : " + e.getName());
            System.out.println("repsperseconds : " + e.getRepsperseconds());
            System.out.println("sidevalue : " + e.getSide());
            System.out.println("reps : " + e.getReps());
            System.out.println("sets : " + e.getSets());
            System.out.println("rest : " + e.getRest());
            System.out.println("hold : " + e.getHolds());
            Connection conn = null;
            try {
                try {
                    PreparedStatement pst;
                    conn = this.ds.getConnection();
                    if (e.getId() == 0) {
                        e.setId(this.getSequence("exerciseId"));
                        e.setCreated(new java.util.Date());
                        pst = conn.prepareStatement(this.createInsert("exercises", columns));
                    } else {
                        pst = conn.prepareStatement(this.createUpdate("exercises", columns, "id = ?"));
                        pst.setLong(columns.length + 1, e.getId());
                    }
                    int i = 0;
                    pst.setLong(++i, e.getId());
                    if (e.getCategory() > 0) {
                        pst.setLong(++i, e.getCategory());
                    } else {
                        pst.setNull(++i, -5);
                    }
                    if (e.getCorpId() > 0) {
                        pst.setLong(++i, e.getCorpId());
                    } else {
                        pst.setNull(++i, -5);
                    }
                    pst.setString(++i, e.getName());
                    pst.setInt(++i, e.getRepTime());
                    pst.setInt(++i, e.getReps());
                    pst.setInt(++i, e.getRestTime());
                    pst.setInt(++i, e.getSetRestTime());
                    pst.setString(++i, e.getDescription());
                    pst.setTimestamp(++i, new Timestamp(e.getCreated().getTime()));
                    pst.setLong(++i, e.getCreator());
                    pst.setString(++i, e.getType());
                    pst.setBoolean(++i, e.isShowSides());
                    pst.setBoolean(++i, e.isShareLocation());
                    pst.setBoolean(++i, e.isHideExercise());
                    if (e.getShareid() > 0) {
                        pst.setLong(++i, e.getShareid());
                    } else {
                        pst.setLong(++i, e.getId());
                    }
                    pst.setInt(++i, e.getSets());
                    pst.setInt(++i, e.getHolds());
                    pst.setInt(++i, e.getRest());
                    pst.setString(++i, e.getSide());
                    pst.setInt(++i, e.getWeight());
                    System.out.println("result set" + pst);
                    pst.executeUpdate();
                    System.out.println(pst + "result set");
                    pst.close();
                }
                catch (Exception ee) {
                    ee.printStackTrace();
                    if (conn == null) break block20;
                    try {
                        conn.close();
                    }
                    catch (SQLException e1) {
                        e1.printStackTrace();
                    }
                }
            }
            finally {
                if (conn != null) {
                    try {
                        conn.close();
                    }
                    catch (SQLException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        }
        return e;
    }

    public List<ProgramExercise> saveProgramexcercisedate(long programId, long exerciseId, int sequence) throws Exception {
        ArrayList<ProgramExercise> datelist;
        datelist = new ArrayList<ProgramExercise>();
        Connection conn = null;
        try {
            conn = this.ds.getConnection();
            PreparedStatement pst = conn.prepareStatement("select * from program_exercises order by createdDate asc limit 7");
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                ProgramExercise pe = new ProgramExercise();
                pe.setCreatedDate((java.util.Date)rs.getDate("createddate"));
                pe.setMaxSets(rs.getInt("maxSets"));
                pe.setMaxReps(rs.getInt("maxReps"));
                datelist.add(pe);
                System.out.println("datelist---" + datelist);
            }
            rs.close();
            pst.close();
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return datelist;
    }

    public List<PaymentSubscription> findPaymentSubscription() throws Exception {
        LinkedList<PaymentSubscription> paymentSubscription;
        paymentSubscription = new LinkedList<PaymentSubscription>();
        Connection conn = this.ds.getConnection();
        try {
            String query = "select * from payment_subscription";
            PreparedStatement pst = conn.prepareStatement(query);
            System.out.println("query query--" + query);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                PaymentSubscription ps = new PaymentSubscription();
                ps.setTransactionDate((java.util.Date)rs.getTimestamp("transaction_date"));
                ps.setId(rs.getLong("id"));
                ps.setUserId(rs.getLong("user_id"));
                ps.setExpired((java.util.Date)rs.getDate("expired"));
                System.out.println("iiiiiiiiiddddddddd" + rs.getLong("id"));
                System.out.println("user_id" + rs.getLong("user_id"));
                System.out.println("expired date---" + rs.getDate("expired"));
                paymentSubscription.add(ps);
            }
            rs.close();
            pst.close();
        }
        finally {
            conn.close();
        }
        return paymentSubscription;
    }

    public PaymentSubscription findPaymentSubscriptionById(long id) throws Exception {
        PaymentSubscription ps;
        ps = null;
        Connection conn = this.ds.getConnection();
        try {
            PreparedStatement pst = conn.prepareStatement("select * from payment_subscription where user_id = ? order by id desc limit 1");
            pst.setLong(1, id);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                ps = new PaymentSubscription();
                ps.setTransactionDate((java.util.Date)rs.getTimestamp("transaction_date"));
                ps.setId(rs.getLong("id"));
                ps.setUserId(rs.getLong("user_id"));
                ps.setExpired((java.util.Date)rs.getDate("expired"));
                System.out.println("iiiiiiiiiddddddddd based on id" + rs.getLong("id"));
                System.out.println("user_id based on id" + rs.getLong("user_id"));
                System.out.println("expired date---based on id" + rs.getDate("expired"));
            }
            rs.close();
            pst.close();
        }
        finally {
            conn.close();
        }
        return ps;
    }

    public Exercise getExerciseedit(long id) throws Exception {
        Exercise e;
        e = null;
        Connection conn = null;
        try {
            try {
                conn = this.ds.getConnection();
                PreparedStatement pst = conn.prepareStatement("select * from exercises where id = ?");
                pst.setLong(1, id);
                ResultSet rs = pst.executeQuery();
                if (rs.next()) {
                    e = new Exercise();
                    e.setId(rs.getLong("id"));
                    e.setCategory(rs.getLong("category"));
                    e.setCorpId(rs.getLong("corp_id"));
                    e.setName(rs.getString("name"));
                    e.setRepTime(rs.getInt("repTime"));
                    e.setReps(rs.getInt("reps"));
                    e.setRestTime(rs.getInt("restTime"));
                    e.setSide(rs.getString("side"));
                    e.setRest(rs.getInt("rest"));
                    e.setHolds(rs.getInt("holds"));
                    e.setSets(rs.getInt("sets"));
                    e.setRepsperseconds(rs.getInt("holds"));
                    e.setSetRestTime(rs.getInt("setRestTime"));
                    e.setDescription(rs.getString("description"));
                    e.setCreated((java.util.Date)rs.getTimestamp("created"));
                    e.setCreator(rs.getLong("creator"));
                    e.setType(rs.getString("type"));
                    e.setShowSides(rs.getBoolean("show_sides"));
                    e.setShareLocation(rs.getBoolean("shareLocation"));
                    e.setHideExercise(rs.getBoolean("hideexercise"));
                    e.setWeight(rs.getInt("weight"));
                }
                rs.close();
                pst.close();
                if (e != null) {
                    pst = conn.prepareStatement("select bodypart from bodyparts where exercise = ?");
                    pst.setLong(1, e.getId());
                    rs = pst.executeQuery();
                    e.setBodyParts(new LinkedList());
                    while (rs.next()) {
                        e.getBodyParts().add(Exercise.BodyPart.valueOf((String)rs.getString("bodypart")));
                    }
                    rs.close();
                    pst.close();
                    pst = conn.prepareStatement("select type from exercise_types where exercise = ?");
                    pst.setLong(1, e.getId());
                    rs = pst.executeQuery();
                    e.setTypes(new LinkedList());
                    while (rs.next()) {
                        e.getTypes().add(Exercise.Type.valueOf((String)rs.getString("type")));
                    }
                    rs.close();
                    pst.close();
                }
            }
            catch (Exception ee) {
                ee.printStackTrace();
                if (conn != null) {
                    conn.close();
                }
            }
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return e;
    }

    public void addProgramExercises(long programid, String exerciseIds) throws Throwable {
    	
    	System.out.println("Inside library manager====>");
        String[] exId = exerciseIds.split(",");
        Long[] exerciseId = new Long[exId.length];
        int i = 0;
        while (i < exId.length) {
            exerciseId[i] = Long.valueOf(exId[i]);
            ++i;
        }
        ArrayList<Long> exerciseIdList = new ArrayList<Long>();
        Long[] arrlong = exerciseId;
        int n = arrlong.length;
        int n2 = 0;
        while (n2 < n) {
            Long excerid = arrlong[n2];
            exerciseIdList.add(excerid);
            ++n2;
        }
        LibraryManager lm = new LibraryManager();
        Program program = null;
        try {
            program = lm.getProgram(programid);
            for (Long Eid : exerciseIdList) {
                Exercise ex = lm.getExercise(Eid.longValue());
                ProgramExercise pe = new ProgramExercise();
                pe.setExerciseId(ex.getId());
                pe.setProgramId(Eid.longValue());
                pe.setName(ex.getName());
                pe.setDescription(ex.getDescription());
                pe.setType("exercise");
                pe.setSequence(lm.nextExerciseSequence(Eid.longValue()));
                pe.setBase(ex.getType());
                pe.setShowSides(ex.isShowSides());
                pe.setSets(3);
                pe.setReps(15);
                pe.setHolds(3);
                pe.setRest(10);
                pe.setDescription(ex.getDescription());
                pe.setWeight(0);
                if (ex.isShowSides()) {
                    pe.setSide("Both");
                }
                pe.setProgramId(programid);
                ProgramExercise programExercise = lm.getProgramExerciseByProgramAndExercise(programid, ex.getId());
                if (programExercise != null && programExercise.getId() > 0) {
                    pe.setId(programExercise.getId());
                }
                pe = lm.save(pe);
            }
            program.setLastUpdated(new java.util.Date());
            lm.save(program);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ProgramExercise getProgramExerciseByProgramAndExercise(long programId, long exerciseId) throws Exception {
        ProgramExercise p;
        Connection conn = this.ds.getConnection();
        p = null;
        try {
            PreparedStatement pst = conn.prepareStatement("select * from program_exercises where program = ? and id = ?");
            pst.setLong(1, programId);
            pst.setLong(2, exerciseId);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                p = new ProgramExercise();
                p.setId(rs.getLong("id"));
                p.setProgramId(rs.getLong("program"));
                p.setExerciseId(rs.getLong("exercise"));
                p.setSequence(rs.getInt("seq"));
                p.setType(rs.getString("type"));
                p.setName(rs.getString("name"));
                p.setSets(rs.getInt("sets"));
                p.setReps(rs.getInt("reps"));
                p.setHolds(rs.getInt("holds"));
                p.setRest(rs.getInt("rest"));
                p.setSide(rs.getString("side"));
                p.setBase(rs.getString("base"));
                p.setDescription(rs.getString("description"));
                p.setShowSides(rs.getBoolean("show_sides"));
            }
        }
        finally {
            conn.close();
        }
        return p;
    }

    public BarchartData saveProgressReportData(String duration, Boolean finished, String lastProgramId, String patientId, String programId, java.util.Date dateDone, String programFreqType, String programFreqValue, String painLevel, String note) throws Exception {
        BarchartData progressReport;
        progressReport = new BarchartData();
        Connection conn = this.ds.getConnection();
        try {
        	PreparedStatement pst;
        	if(note==null||note.isEmpty()){
                 pst = conn.prepareStatement("insert into progress_Report (id,duration,finished,lastProgramId,patientId,programId,dateDone,programfreqtype,programfreqvalue,painlevel) values (?,?,?,?,?,?,?,?,?,?)");

        	}else{
            pst = conn.prepareStatement("insert into progress_Report (id,duration,finished,lastProgramId,patientId,programId,dateDone,programfreqtype,programfreqvalue,painlevel,note) values (?,?,?,?,?,?,?,?,?,?,?)");
            pst.setString(11, note);
        	}
            pst.setLong(1, this.getSequence("progressreportid", conn));
            pst.setInt(2, Integer.parseInt(duration));
            pst.setBoolean(3, finished);
            pst.setInt(4, Integer.parseInt(lastProgramId));
            pst.setInt(5, Integer.parseInt(patientId));
            pst.setInt(6, Integer.parseInt(programId));
            pst.setTimestamp(7, new Timestamp(dateDone.getTime()));
            pst.setString(8, programFreqType);
            pst.setInt(9, Integer.parseInt(programFreqValue));
            pst.setInt(10, Integer.parseInt(painLevel));
            System.out.println("pst" + pst);
            pst.executeUpdate();
            progressReport.setTitle("Barchat Data");
            progressReport.setMessage("Barchart data save successfully");
            System.out.println(" save date succesfully");
            pst.close();
        }
        finally {
            conn.close();
        }
        return progressReport;
    }

    public ArrayList<BarchartData> saveTotalDuration(long programId, String startDate, String endDate) throws Exception {
        ArrayList<BarchartData> barchart;
        SimpleDateFormat s = new SimpleDateFormat("MM/dd/yy");
        Calendar cal = Calendar.getInstance();
        cal.setTime(s.parse(endDate));
        cal.add(5, 1);
        java.util.Date end = cal.getTime();
        endDate = s.format(end);
        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yy");
        java.util.Date date = formatter.parse(startDate);
        Timestamp fromDate = new Timestamp(date.getTime());
        java.util.Date date1 = formatter.parse(endDate);
        Timestamp toDate = new Timestamp(date1.getTime());
if(fromDate.after(toDate)){
	 cal.setTime(s.parse(endDate));
     cal.add(5, -1);
     end = cal.getTime();
     endDate = s.format(end);
     
     Calendar cal1 = Calendar.getInstance();
     cal1.setTime(s.parse(startDate));
     cal1.add(5, 1);
     java.util.Date  start = cal1.getTime();
     startDate = s.format(start);
     
     date = formatter.parse(startDate);
     fromDate = new Timestamp(date.getTime());
     date1 = formatter.parse(endDate);
     toDate = new Timestamp(date1.getTime());
}
        ArrayList<ProgramExercise> prgmexercises = new ArrayList<ProgramExercise>();
        barchart = new ArrayList<BarchartData>();
        ArrayList avgscore = new ArrayList();
        System.out.println(String.valueOf(prgmexercises.size()) + "prgmexercises size");
        Connection conn = null;
        ProgramExercise p = new ProgramExercise();
        try {
        	String actualfreqtype = null;
        	int actualfrqval = 0;
        	int frequencyscore = 0;
        	  int repbasedscore=0;
        	  int timebasedscore=0;
        	  int totalscore=0;
            conn = this.ds.getConnection();
            Program prgm = new Program();
            PreparedStatement psts = conn.prepareStatement("select * from programs where id=?");
            psts.setLong(1, programId);
            ResultSet rst = psts.executeQuery();
            if (rst.next()) {
                prgm.setIntervalType(rst.getString("int_type"));
                prgm.setInterval(rst.getInt("int_val"));
                actualfreqtype = rst.getString("int_type");
                actualfrqval = rst.getInt("int_val");
            }
            PreparedStatement ps = conn.prepareStatement("select * from program_exercises where program = ? and type =?");
            ps.setLong(1, programId);
            ps.setString(2, "exercise");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int rest;
                int side = 0;
               int tside=0;
                p = new ProgramExercise();
                p.setId(rs.getLong("id"));
                p.setProgramId(rs.getLong("program"));
                p.setExerciseId(rs.getLong("exercise"));
                p.setSequence(rs.getInt("seq"));
                p.setType(rs.getString("type"));
                p.setBase(rs.getString("base"));
                p.setName(rs.getString("name"));
                if (rs.getString("base").equals("rep")) {
                    p.setReps(rs.getInt("reps"));
                    p.setHolds(rs.getInt("holds"));
                    int reps = rs.getInt("reps");
                    int reppersec = rs.getInt("holds");
                    p.setRest(rs.getInt("rest"));
                    rest = rs.getInt("rest");
                    p.setSets(rs.getInt("sets"));
                    int sets = rs.getInt("sets");
                    p.setSide(rs.getString("side"));
                    System.out.println("sideddddde::::::::::::::::" + rs.getString("side"));
                    p.setDescription(rs.getString("description"));
                    p.setShowSides(rs.getBoolean("show_sides"));
                    if (rs.getString("side")==null) {
                    	System.out.println("inside sideddddde::::::::::::::::" + rs.getString("side"));
                        side = 1;
                    }else if (rs.getString("side").equalsIgnoreCase("null")) {
                        side = 1;
                    }else if (rs.getString("side").equalsIgnoreCase("")) {
                        side = 1;
                    }else if (rs.getString("side").equalsIgnoreCase("Left")) {
                        side = 1;
                    } else if (rs.getString("side").equalsIgnoreCase("Right")) {
                        side = 1;
                    } else if (rs.getString("side").equalsIgnoreCase("Both")) {
                        side = 2;
                    } else if (rs.getString("side").equalsIgnoreCase("Alternating")) {
                        side = 2;
                    }
                    repbasedscore = repbasedscore+(reps * reppersec) * (sets * side);
                } else if (rs.getString("base").equals("timed")) {
                    p.setHolds(rs.getInt("holds"));
                    int holds = rs.getInt("holds");
                    p.setSets(rs.getInt("sets"));
                    int setfortimed = rs.getInt("sets");
                    p.setRest(rs.getInt("rest"));
                    int trest = rs.getInt("rest");
                    p.setSets(rs.getInt("sets"));
                    int tsets = rs.getInt("sets");
                    p.setSide(rs.getString("side"));
                    System.out.println("sideddddde::::::::::::::::" + rs.getString("side"));
                    p.setDescription(rs.getString("description"));
                    p.setShowSides(rs.getBoolean("show_sides"));
                    if (rs.getString("side")==null) {
                    	System.out.println("inside sideddddde::::::::::::::::" + rs.getString("side"));
                        tside = 1;
                    }else if (rs.getString("side").equalsIgnoreCase("null")) {
                        tside = 1;
                    }else if (rs.getString("side").equalsIgnoreCase("")) {
                        tside = 1;
                    }else if (rs.getString("side").equalsIgnoreCase("Left")) {
                        tside = 1;
                    } else if (rs.getString("side").equalsIgnoreCase("Right")) {
                        tside = 1;
                    } else if (rs.getString("side").equalsIgnoreCase("Both")) {
                        tside = 2;
                    } else if (rs.getString("side").equalsIgnoreCase("Alternating")) {
                        tside = 2;
                    }
                    timebasedscore = timebasedscore + (holds * setfortimed) * (tsets * tside);

                }
                          prgmexercises.add(p);
            }
            totalscore = (repbasedscore + timebasedscore)*actualfrqval;
            System.out.println("actualfrqval" + actualfrqval);
            System.out.println("repbasedscore" + repbasedscore);
            System.out.println("timebasedscore" + timebasedscore);
            System.out.println("totalscore::::::::::::::::" + totalscore);
            rs.close();
            ps.close();
            PreparedStatement pst1 = conn.prepareStatement("UPDATE progress_Report  SET  totalduration=? where programid=?");
            pst1.setInt(1, totalscore);
            pst1.setLong(2, programId);
            pst1.execute();
            System.out.println("totalscore Update::::::::::::::::" + pst1);
            pst1.close();
            PreparedStatement pst2 = conn.prepareStatement("select * from progress_Report where programid = ? and datedone>=?  and datedone<=? order by datedone desc");
            if (fromDate.after(toDate)) {
                pst2.setLong(1, programId);
                pst2.setTimestamp(2, toDate);
                pst2.setTimestamp(3, fromDate);
            } else {
                pst2.setLong(1, programId);
                pst2.setTimestamp(2, fromDate);
                pst2.setTimestamp(3, toDate);
            }
            System.out.println("Query for add score::::::::::::::::" + pst2);
            ResultSet rsset = pst2.executeQuery();
            while (rsset.next()) {
                int adherncescore = 0;
                BarchartData bc = new BarchartData();
                bc.setId(Long.valueOf(rsset.getLong("id")));
                long id = rsset.getLong("id");
                bc.setDuration(Integer.valueOf(rsset.getInt("duration")));
                bc.setFinished(Boolean.valueOf(rsset.getBoolean("finished")));
                bc.setLastProgram(Integer.valueOf(rsset.getInt("lastProgramId")));
                bc.setProgramId(Integer.valueOf(rsset.getInt("patientId")));
                bc.setPatientId(Integer.valueOf(rsset.getInt("programId")));
                bc.setDateDone((java.util.Date)rsset.getDate("dateDone"));
                bc.setProgramFreqType(rsset.getString("programFreqType"));
                bc.setProgramFreqValue(Integer.valueOf(rsset.getInt("programFreqValue")));
                bc.setPainLevel(rsset.getString("painlevel"));
                bc.setTotalduration(Integer.valueOf(rsset.getInt("totalduration")));
                bc.setNote(rsset.getString("note"));
                int duration = rsset.getInt("duration");
                if(duration==0){
                	adherncescore=0;
            	}else {
            		adherncescore=((duration*100)/totalscore);	;
            	}
                bc.setAdherncescore(Integer.valueOf(adherncescore));
                String barfreqtype = rsset.getString("programFreqType");
                int barfreqval = rsset.getInt("programFreqValue");
                if (barfreqtype.equalsIgnoreCase(actualfreqtype)) {
                	if(barfreqval==0){
                		frequencyscore=0;
                	}else{
                		frequencyscore=((barfreqval*100)/actualfrqval);	
                	}
                    bc.setFrequencyscore(Integer.valueOf(frequencyscore));
                }
                System.out.println("adherncescore::::::::::" + adherncescore);
                PreparedStatement pstr = conn.prepareStatement("UPDATE progress_Report  SET  adherncescore=? ,frequencyscore=? where id=?");
                pstr.setInt(1, adherncescore);
                pstr.setInt(2, frequencyscore);
                pstr.setLong(3, id);
                pstr.execute();
                System.out.println("frequencyscore::::::::::" + frequencyscore);
                pstr.close();
                barchart.add(bc);
            }
            rsset.close();
            pst2.close();
            conn.close();
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return barchart;
    }

    public ArrayList<BarchartData> scoreAvg(long programId, String startDate, String endDate) throws Exception {
        ArrayList<BarchartData> barchart;
        SimpleDateFormat s =  new SimpleDateFormat("MM/dd/yy");
        Calendar cal = Calendar.getInstance();
        cal.setTime(s.parse(endDate));
        cal.add(5, 1);
        java.util.Date end = cal.getTime();
        endDate = s.format(end);
        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yy");
        java.util.Date date = formatter.parse(startDate);
        Timestamp fromDate = new Timestamp(date.getTime());
        java.util.Date date1 = formatter.parse(endDate);
        Timestamp toDate = new Timestamp(date1.getTime());
        barchart = new ArrayList<BarchartData>();
        Connection conn = this.ds.getConnection();
        try {
            PreparedStatement avgofps = conn.prepareStatement("SELECT adherncescore,frequencyscore,datedone,programfreqvalue FROM (SELECT *,ROW_NUMBER() OVER (PARTITION BY datedone::date ORDER BY datedone DESC) rn FROM progress_Report WHERE programid = ?  and datedone>=? and datedone<=?) t WHERE rn = 1;");
            if (fromDate.after(toDate)) {
            	 Calendar c = Calendar.getInstance(); 
                 c.setTime(s.parse(startDate)); 
                 c.add(Calendar.DATE, 1);
                 Date t = c.getTime();
                 startDate= s.format(t);
                 java.util.Date date2 = formatter.parse(startDate);
                  toDate = new Timestamp(date2.getTime());
                avgofps.setLong(1, programId);
                avgofps.setTimestamp(2, toDate);
                avgofps.setTimestamp(3, fromDate);
            } else {
                avgofps.setLong(1, programId);
                avgofps.setTimestamp(2, fromDate);
                avgofps.setTimestamp(3, toDate);
            }
            System.out.println("avgofps" + avgofps);
            ResultSet avgrs = avgofps.executeQuery();
            while(avgrs.next()) {
            	  BarchartData avg = new BarchartData();
                avg.setAdhenceAvg(Integer.valueOf(avgrs.getInt("adherncescore")));
                avg.setFreqAvg(Integer.valueOf(avgrs.getInt("frequencyscore")));
                avg.setProgramFreqValue(Integer.valueOf(avgrs.getInt("programfreqvalue")));
                avg.setDateDone(avgrs.getTimestamp("datedone"));
                barchart.add(avg);
            }
            avgofps.close();
            avgrs.close();
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return barchart;
    }
    
    public ArrayList<BarchartData> painLevelAvg(long programId, String startDate, String endDate) throws Exception {
        ArrayList<BarchartData> barchart;
        SimpleDateFormat s =  new SimpleDateFormat("MM/dd/yy");
        Calendar cal = Calendar.getInstance();
        cal.setTime(s.parse(endDate));
        cal.add(5, 1);
        java.util.Date end = cal.getTime();
        endDate = s.format(end);
        if (s.parse(startDate).after(s.parse(endDate))){
      	  Calendar c = Calendar.getInstance(); 
            c.setTime(s.parse(startDate)); 
            c.add(Calendar.DATE, 1);
            Date t = c.getTime();
            startDate= s.format(t);
      }
        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yy");
        java.util.Date date = formatter.parse(startDate);
        Timestamp fromDate = new Timestamp(date.getTime());
        java.util.Date date1 = formatter.parse(endDate);
        Timestamp toDate = new Timestamp(date1.getTime());
        barchart = new ArrayList<BarchartData>();
        Connection conn = this.ds.getConnection();
        try {
            PreparedStatement avgofps = conn.prepareStatement("select ROUND(avg(painlevel),2) as painlevel,to_char(datedone,'dd/mm/yyyy') as date from progress_Report where programid = ? and datedone>=? and datedone<=? group by date order by date desc");
            if (fromDate.after(toDate)) {
                avgofps.setLong(1, programId);
                avgofps.setTimestamp(2, toDate);
                avgofps.setTimestamp(3, fromDate);
            } else {
                avgofps.setLong(1, programId);
                avgofps.setTimestamp(2, fromDate);
                avgofps.setTimestamp(3, toDate);
            }
            System.out.println("avgofps" + avgofps);
            ResultSet avgrs = avgofps.executeQuery();
            while(avgrs.next()) {
            	BarchartData avg = new BarchartData();
                avg.setPainlevlAvg(Float.valueOf(avgrs.getFloat("painlevel")));
                java.util.Date datedid = new java.text.SimpleDateFormat("dd/MM/yyyy").parse(avgrs.getString("date"));
                avg.setDateDone(datedid);
                barchart.add(avg);
            }
            avgofps.close();
            avgrs.close();
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return barchart;
    }
    
    public Program getActualFreq(long programId) throws Exception {
        Program p;
        p = new Program();
        Connection conn = null;
        conn = this.ds.getConnection();
        PreparedStatement pst = conn.prepareStatement("  select * from programs where id=?");
        pst.setLong(1, programId);
        ResultSet rs = pst.executeQuery();
        try {
            if (rs.next()) {
                p.setIntervalType(rs.getString("int_type"));
                p.setInterval(rs.getInt("int_val"));
            }
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return p;
    }

    /*
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Enabled aggressive exception aggregation
     * Lifted jumps to return sites
     */
    public ArrayList<String> getDateofBarchart(String type,String today)
    		throws Exception {

    	ArrayList<String> yAxis = new ArrayList<String>();
    	 Calendar cal = Calendar.getInstance();
         Date date = cal.getTime();
         System.out.println("today"+date);
    	 SimpleDateFormat s = new SimpleDateFormat("MM/dd/yy");
    	 int yLabel = 0;  
    	Connection conn = null;
    	try {
    		if(type==null){
    	
    		     while(yLabel <=6)  
    		     {  
    		     	if(yLabel==0){
    		     	     System.err.println(today+"currentdate");
    		     	     yAxis.add(today);
    		     	}
    		     	else{
    		     		cal.add(Calendar.DATE, -1);
    		     		Date todate1 = cal.getTime();     
    		     	     String todate = s.format(todate1);            
    		     	     System.err.println(todate+"todate"); 
    		     	    yAxis.add(todate);
    		     	     }
    		     yLabel=yLabel+1;  
    		     }     
    		}
    		
    		else if(type!=null){
    			
    			if(type.equalsIgnoreCase("previous")){
    				 while(yLabel<=6)  
    			     {  
    			     	if(yLabel==0){
    			     	     System.err.println(today+"currentdate");
    			     	     yAxis.add(today);
    			     	}
    			     	else{
    			     		cal.setTime(s.parse(today));
    			     		cal.add(Calendar.DATE, -1);
    			     	     System.err.println(s.format(cal.getTime())+"previouu");  
    				     	    yAxis.add(s.format(cal.getTime()));
    				     	   today=s.format(cal.getTime());
    			     	     }
    			     	
    			     yLabel=yLabel+1;  
    			     }
    				 
    			     	
    			}else{
    				
    				 while(yLabel<=6)  
    			     {  
    			     	if(yLabel==0){
    			     	     System.err.println(today+"currentdate");
    			     	     yAxis.add(today);
    			     	}
    			     	else{
    			     		 cal.setTime(s.parse(today));
    			     		  cal.add(Calendar.DATE, +1);
    			     		  System.err.println(s.format(cal.getTime())+"next");  
    				     	    yAxis.add(s.format(cal.getTime()));
    				     	   today=s.format(cal.getTime());
    			     	     }
    			     yLabel=yLabel+1;  
    			     }
    				 
    			}
    			
    		}
    		conn = ds.getConnection();

    }finally {
    	if (conn != null) {
    		conn.close();
    	}
    }

    return yAxis;

    }
    public ArrayList<BarchartData> getBarchartData(String programId, String patientId) throws Exception {
        ArrayList<BarchartData> barchart;
        ArrayList<ProgramExercise> prgmexercises = new ArrayList<ProgramExercise>();
        barchart = new ArrayList<BarchartData>();
        ArrayList<BarchartData> frqList = new ArrayList<BarchartData>();
        ProgramExercise p = new ProgramExercise();
        Connection conn = null;
        try {
            conn = this.ds.getConnection();
            PreparedStatement ps = conn.prepareStatement("select * from program_exercises where program = ?");
            ps.setLong(1, Integer.parseInt(programId));
            ResultSet rs = ps.executeQuery();
            int totalholds = 0;
            int totalreps = 0;
            int totalside = 0;
            int totalreppersec = 0;
            int diffbtwrest = 0;
            int totalsets = 0;
            boolean totalrest = false;
            int timebasedsets = 0;
            int totalreprest = 0;
            int totaltimerest = 0;
            int totalscore = 0;
            while (rs.next()) {
                int side;
                int rest;
                p = new ProgramExercise();
                p.setId(rs.getLong("id"));
                p.setProgramId(rs.getLong("program"));
                p.setExerciseId(rs.getLong("exercise"));
                p.setSequence(rs.getInt("seq"));
                p.setType(rs.getString("type"));
                if (rs.getString("type").equals("rest")) {
                    p.setRest(rs.getInt("rest"));
                    int repsall = rs.getInt("rest");
                    diffbtwrest += repsall;
                    continue;
                }
                p.setBase(rs.getString("base"));
                p.setName(rs.getString("name"));
                if (rs.getString("base").equals("rep")) {
                    p.setReps(rs.getInt("reps"));
                    p.setHolds(rs.getInt("holds"));
                    int reps = rs.getInt("reps");
                    totalreps += reps;
                    int reppersec = rs.getInt("holds");
                    totalreppersec += reppersec;
                    p.setRest(rs.getInt("rest"));
                    rest = rs.getInt("rest");
                    totalreprest += rest;
                } else if (rs.getString("base").equals("timed")) {
                    p.setHolds(rs.getInt("holds"));
                    int holds = rs.getInt("holds");
                    totalholds += holds;
                    p.setSets(rs.getInt("sets"));
                    int setfortimed = rs.getInt("sets");
                    timebasedsets += setfortimed;
                    p.setRest(rs.getInt("rest"));
                    rest = rs.getInt("rest");
                    totaltimerest += rest;
                }
                p.setSets(rs.getInt("sets"));
                int sets = rs.getInt("sets");
                totalsets += sets;
                p.setSide(rs.getString("side"));
                p.setDescription(rs.getString("description"));
                p.setShowSides(rs.getBoolean("show_sides"));
                if (rs.getString("side") == null) {
                    side = 1;
                    totalside += side;
                } else if (rs.getString("side").equals("Left")) {
                    side = 1;
                    totalside += side;
                } else if (rs.getString("side").equals("Right")) {
                    side = 1;
                    totalside += side;
                } else if (rs.getString("side").equals("Both")) {
                    side = 2;
                    totalside += side;
                } else if (rs.getString("side").equals("Alternating")) {
                    side = 2;
                    totalside += side;
                }
                prgmexercises.add(p);
            }
            int repbasedscore = (totalreps * totalreppersec + totalreprest) * (totalsets * totalside + diffbtwrest);
            int timebasedscore = (totalholds * timebasedsets + totaltimerest) * (totalsets * totalside + diffbtwrest);
            totalscore = repbasedscore + timebasedscore;
            System.out.println("repbasedscore" + repbasedscore);
            System.out.println("timebasedscore" + timebasedscore);
            rs.close();
            ps.close();
            PreparedStatement pst1 = conn.prepareStatement("UPDATE progress_Report  SET  totalduration=? where programid=?");
            pst1.setInt(1, totalscore);
            pst1.setLong(2, Integer.parseInt(programId));
            pst1.execute();
            pst1.close();
            PreparedStatement pst2 = conn.prepareStatement("select * from progress_Report where programid = ?");
            pst2.setLong(2, Integer.parseInt(programId));
            ResultSet rsset = pst2.executeQuery();
            while (rsset.next()) {
                int adherncescore = 0;
                BarchartData bc = new BarchartData();
                bc.setId(Long.valueOf(rsset.getLong("id")));
                long id = rsset.getLong("id");
                bc.setDuration(Integer.valueOf(rsset.getInt("duration")));
                bc.setFinished(Boolean.valueOf(rsset.getBoolean("finished")));
                bc.setLastProgram(Integer.valueOf(rsset.getInt("lastProgramId")));
                bc.setProgramId(Integer.valueOf(rsset.getInt("patientId")));
                bc.setPatientId(Integer.valueOf(rsset.getInt("programId")));
                bc.setDateDone((java.util.Date)rsset.getDate("dateDone"));
                bc.setProgramFreqType(rsset.getString("programFreqType"));
                bc.setProgramFreqValue(Integer.valueOf(rsset.getInt("programFreqValue")));
                bc.setPainLevel(rsset.getString("painlevel"));
                bc.setTotalduration(Integer.valueOf(rsset.getInt("totalduration")));
                int duration = rsset.getInt("duration");
                int totalduration = rsset.getInt("totalduration");
                int subtotalduration = totalduration / 2;
                adherncescore = totalduration == 0 ? 0 : (duration == totalduration ? 100 : (subtotalduration <= duration ? 50 : 0));
                bc.setAdherncescore(Integer.valueOf(adherncescore));
                System.out.println("adherncescore::::::::::" + adherncescore);
                PreparedStatement pstr = conn.prepareStatement("UPDATE progress_Report  SET  adherncescore=? where id=?");
                pstr.setInt(1, adherncescore);
                pstr.setLong(2, id);
                pstr.execute();
                System.out.println("UPDATEadherncescore::::::::::" + adherncescore);
                pstr.close();
                barchart.add(bc);
            }
            PreparedStatement pst = conn.prepareStatement("SELECT  progress_Report.programid, progress_Report.id ,programs.int_type, programs.int_val, progress_Report.programfreqtype, progress_Report.programfreqtype,progress_Report.programfreqvalue,progress_Report.datedone FROM programs INNER JOIN progress_Report ON programs.id=progress_Report.programId where progress_Report.programId=?");
            pst.setLong(1, Integer.parseInt(programId));
            ResultSet rd = pst.executeQuery();
            while (rd.next()) {
                int frequencyscore = 0;
                BarchartData bcv = new BarchartData();
                Program psv = new Program();
                bcv.setId(Long.valueOf(rd.getLong("id")));
                long id = rd.getLong("id");
                bcv.setDateDone((java.util.Date)rd.getDate("dateDone"));
                bcv.setProgramFreqType(rd.getString("programFreqType"));
                bcv.setProgramFreqValue(Integer.valueOf(rd.getInt("programFreqValue")));
                psv.setIntervalType(rd.getString("int_type"));
                psv.setInterval(rd.getInt("int_val"));
                String barfreqtype = rd.getString("programFreqType");
                String programfreqtype = rd.getString("int_type");
                int barfreqval = rd.getInt("programFreqValue");
                int programfreqval = rd.getInt("int_val");
                int subprogramfreqval = programfreqval / 2;
                if (barfreqtype.equalsIgnoreCase(programfreqtype)) {
                    frequencyscore = programfreqval * 2 == barfreqval ? 200 : (programfreqval * 2 / 2 <= barfreqval ? 150 : (programfreqval <= barfreqval ? 100 : (subprogramfreqval <= barfreqval ? 50 : 0)));
                    bcv.setFrequencyscore(Integer.valueOf(frequencyscore));
                }
                System.out.println("frequencyscore::::::::::" + frequencyscore);
                PreparedStatement pstr = conn.prepareStatement("UPDATE progress_Report  SET  frequencyscore=? where id=?");
                pstr.setInt(1, frequencyscore);
                pstr.setLong(2, id);
                pstr.execute();
                System.out.println("UPDATEfrequencyscore::::::::::" + frequencyscore);
                pstr.close();
                frqList.add(bcv);
            }
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return barchart;
    }

    public ArrayList<BarchartData> findadherencescoreforcurrentweek(long programId, String today) throws Exception {
        ArrayList<BarchartData> barchart;
        SimpleDateFormat s = new SimpleDateFormat("dd/mm/yy");
        Calendar cal = Calendar.getInstance();
        cal.setTime(s.parse(today));
        cal.add(5, -8);
        java.util.Date end = cal.getTime();
        String endDate = s.format(end);
        
        Calendar c = Calendar.getInstance(); 
        c.setTime(s.parse(today)); 
        c.add(Calendar.DATE, 1);
        Date t = c.getTime();
        today= s.format(t);
        
        SimpleDateFormat formatter = new SimpleDateFormat("dd/mm/yy");
        java.util.Date date = formatter.parse(today);
        Timestamp fromDate = new Timestamp(date.getTime());
        java.util.Date date1 = formatter.parse(endDate);
        Timestamp toDate = new Timestamp(date1.getTime());
        ArrayList<ProgramExercise> prgmexercises = new ArrayList<ProgramExercise>();
        barchart = new ArrayList<BarchartData>();
        System.out.println(String.valueOf(prgmexercises.size()) + "prgmexercises size");
        Connection conn = null;
        ProgramExercise p = new ProgramExercise();
        try {
        	int totalholds = 0;
        	int totalreps = 0;
        	int totalside = 0;
        	int totalreppersec = 0;
        	int diffbtwrest = 0;
        	int totalsets = 0;
        	boolean totalrest = false;
        	int timebasedsets = 0;
        	int totalreprest = 0;
        	int totaltimerest = 0;
        	int totalscore = 0;
        	String actualfreqtype = null;
        	int actualfrqval = 0;
        	int frequencyscore = 0;
        	int reptotalside=0;
        	int timetotalsets=0;
        	int repbasedscore=0;
        	int timebasedscore=0;
        	int tside=0;
            conn = this.ds.getConnection();
            Program prgm = new Program();
            PreparedStatement psts = conn.prepareStatement("select * from programs where id=?");
            psts.setLong(1, programId);
            ResultSet rst = psts.executeQuery();
            if (rst.next()) {
                prgm.setIntervalType(rst.getString("int_type"));
                prgm.setInterval(rst.getInt("int_val"));
                actualfreqtype = rst.getString("int_type");
                actualfrqval = rst.getInt("int_val");
            }
            PreparedStatement ps = conn.prepareStatement("select * from program_exercises where program = ?");
            ps.setLong(1, programId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int rest;
                int side = 0;
                p = new ProgramExercise();
                p.setId(rs.getLong("id"));
                p.setProgramId(rs.getLong("program"));
                p.setExerciseId(rs.getLong("exercise"));
                p.setSequence(rs.getInt("seq"));
                p.setType(rs.getString("type"));
                if (rs.getString("type").equals("rest")) {
                    p.setRest(rs.getInt("rest"));
                    int repsall = rs.getInt("rest");
                    diffbtwrest += repsall;
                    continue;
                }
                p.setBase(rs.getString("base"));
                p.setName(rs.getString("name"));
                if (rs.getString("base").equals("rep")) {
                    p.setReps(rs.getInt("reps"));
                    p.setHolds(rs.getInt("holds"));
                    int reps = rs.getInt("reps");
                    totalreps += reps;
                    System.out.println("totalreps::::::::::::::::" + totalreps);
                    int reppersec = rs.getInt("holds");
                    totalreppersec += reppersec;
                    System.out.println("totalreppersec::::::::::::::::" + totalreppersec);
                    p.setRest(rs.getInt("rest"));
                    rest = rs.getInt("rest");
                    totalreprest += rest;
                    System.out.println("totalreprest::::::::::::::::" + totalreprest);
                    
                    p.setSets(rs.getInt("sets"));
                    int sets = rs.getInt("sets");
                    totalsets += sets;
                    p.setSide(rs.getString("side"));
                    System.out.println("sideddddde::::::::::::::::" + rs.getString("side"));
                    p.setDescription(rs.getString("description"));
                    p.setShowSides(rs.getBoolean("show_sides"));
                    if (rs.getString("side")==null) {
                    	System.out.println("inside sideddddde::::::::::::::::" + rs.getString("side"));
                        side = 1;
                        reptotalside += side;
                    }else if (rs.getString("side").equalsIgnoreCase("null")) {
                        side = 1;
                        reptotalside += side;
                    }else if (rs.getString("side").equalsIgnoreCase("")) {
                        side = 1;
                        reptotalside += side;
                    }else if (rs.getString("side").equalsIgnoreCase("Left")) {
                        side = 1;
                        reptotalside += side;
                    } else if (rs.getString("side").equalsIgnoreCase("Right")) {
                        side = 1;
                        reptotalside += side;
                    } else if (rs.getString("side").equalsIgnoreCase("Both")) {
                        side = 2;
                        reptotalside += side;
                    } else if (rs.getString("side").equalsIgnoreCase("Alternating")) {
                        side = 2;
                        reptotalside += side;
                    }
        
                    repbasedscore = repbasedscore+(reps * reppersec) * (sets * side);
 
                } else if (rs.getString("base").equals("timed")) {
                    p.setHolds(rs.getInt("holds"));
                    int holds = rs.getInt("holds");
                    totalholds += holds;
                    p.setSets(rs.getInt("sets"));
                    int setfortimed = rs.getInt("sets");
                    timebasedsets += setfortimed;
                    p.setRest(rs.getInt("rest"));
                    int trest = rs.getInt("rest");
                    p.setSets(rs.getInt("sets"));
                    int tsets = rs.getInt("sets");
                    p.setSide(rs.getString("side"));
                    System.out.println("sideddddde::::::::::::::::" + rs.getString("side"));
                    p.setDescription(rs.getString("description"));
                    p.setShowSides(rs.getBoolean("show_sides"));
                    if (rs.getString("side")==null) {
                    	System.out.println("inside sideddddde::::::::::::::::" + rs.getString("side"));
                        tside = 1;
                        totalside += side;
                    }else if (rs.getString("side").equalsIgnoreCase("null")) {
                        tside = 1;
                        totalside += side;
                    }else if (rs.getString("side").equalsIgnoreCase("")) {
                        tside = 1;
                        totalside += side;
                    }else if (rs.getString("side").equalsIgnoreCase("Left")) {
                        tside = 1;
                        totalside += side;
                    } else if (rs.getString("side").equalsIgnoreCase("Right")) {
                        tside = 1;
                        totalside += side;
                    } else if (rs.getString("side").equalsIgnoreCase("Both")) {
                        tside = 2;
                        totalside += side;
                    } else if (rs.getString("side").equalsIgnoreCase("Alternating")) {
                        tside = 2;
                        totalside += side;
                    }
                    timebasedscore = timebasedscore + (holds * setfortimed ) * (tsets * tside);

                }
                          prgmexercises.add(p);
            }
            System.out.println("totalsets::::::::::::::::" + totalsets);
            System.out.println("totalside::::::::::::::::" + totalside);
            totalscore = (repbasedscore + timebasedscore)*actualfrqval;
            System.out.println("actualfrqval" + actualfrqval);
            System.out.println("repbasedscore" + repbasedscore);
            System.out.println("timebasedscore" + timebasedscore);
            System.out.println("totalscore::::::::::::::::" + totalscore);
            rs.close();
            ps.close();
            PreparedStatement pst1 = conn.prepareStatement("UPDATE progress_Report  SET  totalduration=? where programid=?");
            pst1.setInt(1, totalscore);
            pst1.setLong(2, programId);
            pst1.execute();
            System.out.println("totalscore Update::::::::::::::::" + pst1);
            pst1.close();
            PreparedStatement pst2 = conn.prepareStatement("select * from progress_Report where programid = ? and datedone>=?  and datedone<=? order by datedone desc");
            if (fromDate.after(toDate)) {
                pst2.setLong(1, programId);
                pst2.setTimestamp(2, toDate);
                pst2.setTimestamp(3, fromDate);
            } else {
                pst2.setLong(1, programId);
                pst2.setTimestamp(2, fromDate);
                pst2.setTimestamp(3, toDate);
            }
            System.out.println("Query for add score::::::::::::::::" + pst2);
            ResultSet rsset = pst2.executeQuery();
            while (rsset.next()) {
                int adherncescore = 0;
                BarchartData bc = new BarchartData();
                bc.setId(Long.valueOf(rsset.getLong("id")));
                long id = rsset.getLong("id");
                bc.setDuration(Integer.valueOf(rsset.getInt("duration")));
                bc.setFinished(Boolean.valueOf(rsset.getBoolean("finished")));
                bc.setLastProgram(Integer.valueOf(rsset.getInt("lastProgramId")));
                bc.setProgramId(Integer.valueOf(rsset.getInt("patientId")));
                bc.setPatientId(Integer.valueOf(rsset.getInt("programId")));
                bc.setDateDone((java.util.Date)rsset.getDate("dateDone"));
                bc.setProgramFreqType(rsset.getString("programFreqType"));
                bc.setProgramFreqValue(Integer.valueOf(rsset.getInt("programFreqValue")));
                bc.setPainLevel(rsset.getString("painlevel"));
                bc.setTotalduration(Integer.valueOf(rsset.getInt("totalduration")));
                bc.setNote(rsset.getString("note"));
                int duration = rsset.getInt("duration");
                int totalduration = rsset.getInt("totalduration");
                if(duration==0){
                	adherncescore=0;
            	}else {
            		adherncescore=((duration*100)/totalscore);	;
            	}
                bc.setAdherncescore(Integer.valueOf(adherncescore));
                String barfreqtype = rsset.getString("programFreqType");
                int barfreqval = rsset.getInt("programFreqValue");
                if (barfreqtype.equalsIgnoreCase(actualfreqtype)) {
                	if(barfreqval==0){
                		frequencyscore=0;
                	}else{
                		frequencyscore=((barfreqval*100)/actualfrqval);	
                	}
                    bc.setFrequencyscore(Integer.valueOf(frequencyscore));
                }
                System.out.println("adherncescore::::::::::" + adherncescore);
                PreparedStatement pstr = conn.prepareStatement("UPDATE progress_Report  SET  adherncescore=? ,frequencyscore=? where id=?");
                pstr.setInt(1, adherncescore);
                pstr.setInt(2, frequencyscore);
                pstr.setLong(3, id);
                pstr.execute();
                System.out.println("frequencyscore::::::::::" + frequencyscore);
                pstr.close();
                barchart.add(bc);
            }
            rsset.close();
            pst2.close();
            conn.close();
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return barchart;
    }

    public ArrayList<BarchartData> findadherencescoreforpastweek(long programId, String lastdate) throws Exception {
        ArrayList<BarchartData> barchart;
        SimpleDateFormat s = new SimpleDateFormat("dd/mm/yy");
        Calendar cal = Calendar.getInstance();
        cal.setTime(s.parse(lastdate));
        cal.add(5, -8);
        java.util.Date end = cal.getTime();
        String endDate = s.format(end);
        SimpleDateFormat formatter = new SimpleDateFormat("dd/mm/yy");
        java.util.Date date = formatter.parse(lastdate);
        Timestamp fromDate = new Timestamp(date.getTime());
        java.util.Date date1 = formatter.parse(endDate);
        Timestamp toDate = new Timestamp(date1.getTime());
        ArrayList<ProgramExercise> prgmexercises = new ArrayList<ProgramExercise>();
        barchart = new ArrayList<BarchartData>();
        System.out.println(String.valueOf(prgmexercises.size()) + "prgmexercises size");
        Connection conn = null;
        ProgramExercise p = new ProgramExercise();
        try {
        	int totalholds = 0;
        	int totalreps = 0;
        	int totalside = 0;
        	int totalreppersec = 0;
        	int diffbtwrest = 0;
        	int totalsets = 0;
        	boolean totalrest = false;
        	int timebasedsets = 0;
        	int totalreprest = 0;
        	int totaltimerest = 0;
        	int totalscore = 0;
        	String actualfreqtype = null;
        	int actualfrqval = 0;
        	int frequencyscore = 0;
        	int reptotalside=0;
        	int timetotalsets=0;
            conn = this.ds.getConnection();
            Program prgm = new Program();
            PreparedStatement psts = conn.prepareStatement("select * from programs where id=?");
            psts.setLong(1, programId);
            ResultSet rst = psts.executeQuery();
            if (rst.next()) {
                prgm.setIntervalType(rst.getString("int_type"));
                prgm.setInterval(rst.getInt("int_val"));
                actualfreqtype = rst.getString("int_type");
                actualfrqval = rst.getInt("int_val");
            }
            PreparedStatement ps = conn.prepareStatement("select * from program_exercises where program = ?");
            ps.setLong(1, programId);
            ResultSet rs = ps.executeQuery();
            int repbasedscore=0;
            int timebasedscore=0;
            while (rs.next()) {
                int rest;
                int side = 0;
                p = new ProgramExercise();
                p.setId(rs.getLong("id"));
                p.setProgramId(rs.getLong("program"));
                p.setExerciseId(rs.getLong("exercise"));
                p.setSequence(rs.getInt("seq"));
                p.setType(rs.getString("type"));
                if (rs.getString("type").equals("rest")) {
                    p.setRest(rs.getInt("rest"));
                    int repsall = rs.getInt("rest");
                    diffbtwrest += repsall;
                    continue;
                }
                p.setBase(rs.getString("base"));
                p.setName(rs.getString("name"));
                if (rs.getString("base").equals("rep")) {
                    p.setReps(rs.getInt("reps"));
                    p.setHolds(rs.getInt("holds"));
                    int reps = rs.getInt("reps");
                  //  totalreps += reps;
                    System.out.println("totalreps::::::::::::::::" + totalreps);
                    int reppersec = rs.getInt("holds");
                 //   totalreppersec += reppersec;
                    System.out.println("totalreppersec::::::::::::::::" + totalreppersec);
                    p.setRest(rs.getInt("rest"));
                    rest = rs.getInt("rest");
                   // totalreprest += rest;
                    System.out.println("totalreprest::::::::::::::::" + totalreprest);
                    
                    p.setSets(rs.getInt("sets"));
                    int sets = rs.getInt("sets");
                   // totalsets += sets;
                    p.setSide(rs.getString("side"));
                    System.out.println("sideddddde::::::::::::::::" + rs.getString("side"));
                    p.setDescription(rs.getString("description"));
                    p.setShowSides(rs.getBoolean("show_sides"));
                    if (rs.getString("side")==null) {
                    	System.out.println("inside sideddddde::::::::::::::::" + rs.getString("side"));
                        side = 1;
                        reptotalside += side;
                    }else if (rs.getString("side").equalsIgnoreCase("null")) {
                        side = 1;
                        reptotalside += side;
                    }else if (rs.getString("side").equalsIgnoreCase("")) {
                        side = 1;
                        reptotalside += side;
                    }else if (rs.getString("side").equalsIgnoreCase("Left")) {
                        side = 1;
                        reptotalside += side;
                    } else if (rs.getString("side").equalsIgnoreCase("Right")) {
                        side = 1;
                        reptotalside += side;
                    } else if (rs.getString("side").equalsIgnoreCase("Both")) {
                        side = 2;
                        reptotalside += side;
                    } else if (rs.getString("side").equalsIgnoreCase("Alternating")) {
                        side = 2;
                       // reptotalside += side;
                    }
                     repbasedscore = repbasedscore+(reps * reppersec) * (sets * side);
                    
                } else if (rs.getString("base").equals("timed")) {
                    p.setHolds(rs.getInt("holds"));
                    int holds = rs.getInt("holds");
                   // totalholds += holds;
                    p.setSets(rs.getInt("sets"));
                    int setfortimed = rs.getInt("sets");
                  //  timebasedsets += setfortimed;
                    p.setRest(rs.getInt("rest"));
                   int trest = rs.getInt("rest");
                  //  totaltimerest += rest;
                    
                    p.setSets(rs.getInt("sets"));
                    int tsets = rs.getInt("sets");
                   // timetotalsets += sets;
                    p.setSide(rs.getString("side"));
                    System.out.println("sideddddde::::::::::::::::" + rs.getString("side"));
                    p.setDescription(rs.getString("description"));
                    p.setShowSides(rs.getBoolean("show_sides"));
                    int tside=0;
                    if (rs.getString("side")==null) {
                    	System.out.println("inside sideddddde::::::::::::::::" + rs.getString("side"));
                        tside = 1;
                        totalside += side;
                    }else if (rs.getString("side").equalsIgnoreCase("null")) {
                    	tside = 1;
                        totalside += side;
                    }else if (rs.getString("side").equalsIgnoreCase("")) {
                    	tside = 1;
                        totalside += side;
                    }else if (rs.getString("side").equalsIgnoreCase("Left")) {
                    	tside = 1;
                        totalside += side;
                    } else if (rs.getString("side").equalsIgnoreCase("Right")) {
                    	tside = 1;
                        totalside += side;
                    } else if (rs.getString("side").equalsIgnoreCase("Both")) {
                    	tside = 2;
                        totalside += side;
                    } else if (rs.getString("side").equalsIgnoreCase("Alternating")) {
                    	tside = 2;
                       // totalside += side;
                    }
                    timebasedscore = timebasedscore + (holds * setfortimed) * (tsets * tside);
        
                }
                          prgmexercises.add(p);
            }
            System.out.println("totalsets::::::::::::::::" + totalsets);
            System.out.println("totalside::::::::::::::::" + totalside);
          
            
            totalscore = (repbasedscore + timebasedscore)*actualfrqval;
            System.out.println("actualfrqval" + actualfrqval);
            System.out.println("repbasedscore" + repbasedscore);
            System.out.println("timebasedscore" + timebasedscore);
            System.out.println("totalscore::::::::::::::::" + totalscore);
            rs.close();
            ps.close();
            PreparedStatement pst1 = conn.prepareStatement("UPDATE progress_Report  SET  totalduration=? where programid=?");
            pst1.setInt(1, totalscore);
            pst1.setLong(2, programId);
            pst1.execute();
            System.out.println("totalscore Update::::::::::::::::" + pst1);
            pst1.close();
            PreparedStatement pst2 = conn.prepareStatement("select * from progress_Report where programid = ? and datedone>=?  and datedone<=? order by datedone desc");
            if (fromDate.after(toDate)) {
                pst2.setLong(1, programId);
                pst2.setTimestamp(2, toDate);
                pst2.setTimestamp(3, fromDate);
            } else {
                pst2.setLong(1, programId);
                pst2.setTimestamp(2, fromDate);
                pst2.setTimestamp(3, toDate);
            }
            System.out.println("Query for add score::::::::::::::::" + pst2);
            ResultSet rsset = pst2.executeQuery();
            while (rsset.next()) {
                int adherncescore = 0;
                BarchartData bc = new BarchartData();
                bc.setId(Long.valueOf(rsset.getLong("id")));
                long id = rsset.getLong("id");
                bc.setDuration(Integer.valueOf(rsset.getInt("duration")));
                bc.setFinished(Boolean.valueOf(rsset.getBoolean("finished")));
                bc.setLastProgram(Integer.valueOf(rsset.getInt("lastProgramId")));
                bc.setProgramId(Integer.valueOf(rsset.getInt("patientId")));
                bc.setPatientId(Integer.valueOf(rsset.getInt("programId")));
                bc.setDateDone((java.util.Date)rsset.getDate("dateDone"));
                bc.setProgramFreqType(rsset.getString("programFreqType"));
                bc.setProgramFreqValue(Integer.valueOf(rsset.getInt("programFreqValue")));
                bc.setPainLevel(rsset.getString("painlevel"));
                bc.setTotalduration(Integer.valueOf(rsset.getInt("totalduration")));
                bc.setNote(rsset.getString("note"));
                int duration = rsset.getInt("duration");
                int totalduration = rsset.getInt("totalduration");
                if(duration==0){
                	adherncescore=0;
            	}else {
            		adherncescore=((duration*100)/totalduration);	;
            	}
                bc.setAdherncescore(Integer.valueOf(adherncescore));
                String barfreqtype = rsset.getString("programFreqType");
                int barfreqval = rsset.getInt("programFreqValue");
                if (barfreqtype.equalsIgnoreCase(actualfreqtype)) {
                	if(barfreqval==0){
                		frequencyscore=0;
                	}else{
                		frequencyscore=((barfreqval*100)/actualfrqval);	
                	}
                    bc.setFrequencyscore(Integer.valueOf(frequencyscore));
                }
                System.out.println("adherncescore::::::::::" + adherncescore);
                PreparedStatement pstr = conn.prepareStatement("UPDATE progress_Report  SET  adherncescore=? ,frequencyscore=? where id=?");
                pstr.setInt(1, adherncescore);
                pstr.setInt(2, frequencyscore);
                pstr.setLong(3, id);
                pstr.execute();
                System.out.println("frequencyscore::::::::::" + frequencyscore);
                pstr.close();
                barchart.add(bc);
            }
            rsset.close();
            pst2.close();
            conn.close();
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return barchart;
    }

    public ArrayList<BarchartData> scoreAvgForThisWeek(long programId, String today) throws Exception {
        ArrayList<BarchartData> barchart;
        SimpleDateFormat s = new SimpleDateFormat("dd/mm/yy");
        Calendar cal = Calendar.getInstance();
        cal.setTime(s.parse(today));
        cal.add(5, -8);
        java.util.Date end = cal.getTime();
        String endDate = s.format(end);
        
        Calendar c = Calendar.getInstance(); 
        c.setTime(s.parse(today)); 
        c.add(Calendar.DATE, 1);
        Date t = c.getTime();
        today= s.format(t);
        
        SimpleDateFormat formatter = new SimpleDateFormat("dd/mm/yy");
        java.util.Date date = formatter.parse(today);
        Timestamp fromDate = new Timestamp(date.getTime());
        java.util.Date date1 = formatter.parse(endDate);
        Timestamp toDate = new Timestamp(date1.getTime());
        barchart = new ArrayList<BarchartData>();
        Connection conn = this.ds.getConnection();
        try {
            PreparedStatement avgofps = conn.prepareStatement("SELECT adherncescore,frequencyscore,datedone,programfreqvalue FROM (SELECT *,ROW_NUMBER() OVER (PARTITION BY datedone::date ORDER BY datedone DESC) rn FROM progress_Report WHERE programid = ?  and datedone>=? and datedone<=?) t WHERE rn = 1;");
            if (fromDate.after(toDate)) {
                avgofps.setLong(1, programId);
                avgofps.setTimestamp(2, toDate);
                avgofps.setTimestamp(3, fromDate);
            } else {
                avgofps.setLong(1, programId);
                avgofps.setTimestamp(2, fromDate);
                avgofps.setTimestamp(3, toDate);
            }
            System.out.println("avgofps:::::::::"+avgofps);
            ResultSet avgrs = avgofps.executeQuery();
            while(avgrs.next()) {
            	 BarchartData avg = new BarchartData();
                avg.setAdhenceAvg(Integer.valueOf(avgrs.getInt("adherncescore")));
                avg.setFreqAvg(Integer.valueOf(avgrs.getInt("frequencyscore")));
                avg.setProgramFreqValue(Integer.valueOf(avgrs.getInt("programfreqvalue")));
                avg.setDateDone(avgrs.getTimestamp("datedone"));
                barchart.add(avg);
            }
            avgofps.close();
            avgrs.close();
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return barchart;
    }

    public ArrayList<BarchartData> painLevelAvgForThisWeek(long programId, String today) throws Exception {
        ArrayList<BarchartData> barchart;
        SimpleDateFormat s = new SimpleDateFormat("dd/MM/yy");
        Calendar cal = Calendar.getInstance();
        cal.setTime(s.parse(today));
        cal.add(5, -8);
        java.util.Date end = cal.getTime();
        String endDate = s.format(end);
        
        
        Calendar c = Calendar.getInstance(); 
        c.setTime(s.parse(today)); 
        c.add(Calendar.DATE, 1);
        Date t = c.getTime();
        today= s.format(t);
        
        
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yy");
        java.util.Date date = formatter.parse(today);
        Timestamp fromDate = new Timestamp(date.getTime());
        java.util.Date date1 = formatter.parse(endDate);
        Timestamp toDate = new Timestamp(date1.getTime());
        barchart = new ArrayList<BarchartData>();
        Connection conn = this.ds.getConnection();
        try {
            PreparedStatement avgofps = conn.prepareStatement("select ROUND(avg(painlevel),2) as painlevel,to_char(datedone,'mm/dd/yyyy') as date from progress_Report where programid = ? and datedone>=? and datedone<=? group by date order by date desc");
            if (fromDate.after(toDate)) {
                avgofps.setLong(1, programId);
                avgofps.setTimestamp(2, toDate);
                avgofps.setTimestamp(3, fromDate);
            } else {
                avgofps.setLong(1, programId);
                avgofps.setTimestamp(2, fromDate);
                avgofps.setTimestamp(3, toDate);
            }
            System.out.println("avgofps:::::::::"+avgofps);
            ResultSet avgrs = avgofps.executeQuery();
            while(avgrs.next()) {
            	BarchartData avg = new BarchartData();
                avg.setPainlevlAvg(Float.valueOf(avgrs.getFloat("painlevel")));
                java.util.Date datedid = new java.text.SimpleDateFormat("dd/MM/yyyy").parse(avgrs.getString("date"));
                avg.setDateDone(datedid);
                barchart.add(avg);
            }
            avgofps.close();
            avgrs.close();
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return barchart;
    }

    public ArrayList<BarchartData> painLevelAvgforLastWeek(long programId, String lastdate) throws Exception {
        ArrayList<BarchartData> barchart;
        SimpleDateFormat s = new SimpleDateFormat("dd/MM/yy");
        Calendar cal = Calendar.getInstance();
        cal.setTime(s.parse(lastdate));
        cal.add(5, -8);
        java.util.Date end = cal.getTime();
        String endDate = s.format(end);
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yy");
        java.util.Date date = formatter.parse(lastdate);
        Timestamp fromDate = new Timestamp(date.getTime());
        java.util.Date date1 = formatter.parse(endDate);
        Timestamp toDate = new Timestamp(date1.getTime());
        barchart = new ArrayList<BarchartData>();
        Connection conn = this.ds.getConnection();
        try {
            PreparedStatement avgofps = conn.prepareStatement("select ROUND(avg(painlevel),2) as painlevel,to_char(datedone,'mm/dd/yyyy') as date from progress_Report where programid = ? and datedone>=? and datedone<=? group by date order by date desc");
            if (fromDate.after(toDate)) {
                avgofps.setLong(1, programId);
                avgofps.setTimestamp(2, toDate);
                avgofps.setTimestamp(3, fromDate);
            } else {
                avgofps.setLong(1, programId);
                avgofps.setTimestamp(2, fromDate);
                avgofps.setTimestamp(3, toDate);
            }
            System.out.println("avgofps:::::::::"+avgofps);
            ResultSet avgrs = avgofps.executeQuery();
            while(avgrs.next()) {
            	BarchartData avg = new BarchartData();
                avg.setPainlevlAvg(Float.valueOf(avgrs.getFloat("painlevel")));
                java.util.Date datedid = new java.text.SimpleDateFormat("dd/MM/yyyy").parse(avgrs.getString("date"));
                avg.setDateDone(datedid);
                barchart.add(avg);
            }
            avgofps.close();
            avgrs.close();
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return barchart;
    }
    public ArrayList<BarchartData> scoreAvgForLastWeek(long programId, String lastdate) throws Exception {
        ArrayList<BarchartData> barchart;
        SimpleDateFormat s = new SimpleDateFormat("dd/mm/yy");
        Calendar cal = Calendar.getInstance();
        cal.setTime(s.parse(lastdate));
        cal.add(5, -8);
        java.util.Date end = cal.getTime();
        String endDate = s.format(end);
        SimpleDateFormat formatter = new SimpleDateFormat("dd/mm/yy");
        java.util.Date date = formatter.parse(lastdate);
        Timestamp fromDate = new Timestamp(date.getTime());
        java.util.Date date1 = formatter.parse(endDate);
        Timestamp toDate = new Timestamp(date1.getTime());
        barchart = new ArrayList<BarchartData>();
        Connection conn = this.ds.getConnection();
        try {
            PreparedStatement avgofps = conn.prepareStatement("SELECT adherncescore,frequencyscore,datedone,programfreqvalue FROM (SELECT *,ROW_NUMBER() OVER (PARTITION BY datedone::date ORDER BY datedone DESC) rn FROM progress_Report WHERE programid = ?  and datedone>=? and datedone<=?) t WHERE rn = 1;");
            if (fromDate.after(toDate)) {
                avgofps.setLong(1, programId);
                avgofps.setTimestamp(2, toDate);
                avgofps.setTimestamp(3, fromDate);
            } else {
                avgofps.setLong(1, programId);
                avgofps.setTimestamp(2, fromDate);
                avgofps.setTimestamp(3, toDate);
            }
            System.out.println("avgofps:::::::::"+avgofps);
            ResultSet avgrs = avgofps.executeQuery();
            while(avgrs.next()) {
            	 BarchartData avg = new BarchartData();
                avg.setAdhenceAvg(Integer.valueOf(avgrs.getInt("adherncescore")));
                avg.setFreqAvg(Integer.valueOf(avgrs.getInt("frequencyscore")));
                avg.setProgramFreqValue(Integer.valueOf(avgrs.getInt("programfreqvalue")));
                avg.setDateDone(avgrs.getTimestamp("datedone"));
                barchart.add(avg);
            }
            avgofps.close();
            avgrs.close();
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return barchart;
    }

    public ArrayList<BarchartData> getFreqScore(long programId, String today) throws Exception {
        ArrayList<BarchartData> frqList;
        SimpleDateFormat s = new SimpleDateFormat("dd/MM/yy");
        Calendar cal = Calendar.getInstance();
        cal.setTime(s.parse(today));
        cal.add(5, -8);
        java.util.Date end = cal.getTime();
        String endDate = s.format(end);
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yy");
        java.util.Date date = formatter.parse(today);
        Timestamp fromDate = new Timestamp(date.getTime());
        java.util.Date date1 = formatter.parse(endDate);
        Timestamp toDate = new Timestamp(date1.getTime());
        frqList = new ArrayList<BarchartData>();
        Connection conn = null;
        String actualfreqtype = null;
        int actualfrqval = 0;
        try {
            conn = this.ds.getConnection();
            Program prgm = new Program();
            PreparedStatement psts = conn.prepareStatement("  select * from programs where id=?");
            psts.setLong(1, programId);
            ResultSet rst = psts.executeQuery();
            if (rst.next()) {
                prgm.setIntervalType(rst.getString("int_type"));
                prgm.setInterval(rst.getInt("int_val"));
                actualfreqtype = rst.getString("int_type");
                actualfrqval = rst.getInt("int_val");
            }
            PreparedStatement pst = conn.prepareStatement("SELECT  progress_Report.programid, progress_Report.id ,programs.int_type, programs.int_val, progress_Report.programfreqtype, progress_Report.programfreqtype,progress_Report.programfreqvalue,progress_Report.datedone FROM programs INNER JOIN progress_Report ON programs.id=progress_Report.programId where progress_Report.programId=? and datedone>=?  and datedone<=?  order by datedone desc");
            pst.setLong(1, programId);
            pst.setTimestamp(2, fromDate);
            pst.setTimestamp(3, toDate);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                int frequencyscore = 0;
                BarchartData bcv = new BarchartData();
                Program p = new Program();
                bcv.setId(Long.valueOf(rs.getLong("id")));
                long id = rs.getLong("id");
                bcv.setDateDone((java.util.Date)rs.getDate("dateDone"));
                bcv.setProgramFreqType(rs.getString("programFreqType"));
                bcv.setProgramFreqValue(Integer.valueOf(rs.getInt("programFreqValue")));
                p.setIntervalType(rs.getString("int_type"));
                p.setInterval(rs.getInt("int_val"));
                String barfreqtype = rs.getString("programFreqType");
                String programfreqtype = rs.getString("int_type");
                int barfreqval = rs.getInt("programFreqValue");
                int programfreqval = rs.getInt("int_val");
                int subprogramfreqval = programfreqval / 2;
                if (barfreqtype.equalsIgnoreCase(programfreqtype)) {
                	if((programfreqval*2)<=barfreqval){
    					frequencyscore=200;
    				}else if(((programfreqval*2)/2)<=barfreqval){
    					frequencyscore=150;	
    				}else if(programfreqval<=barfreqval){
        					frequencyscore=100;
        				}else if(subprogramfreqval<=barfreqval){
        					frequencyscore=50;	
        				}else{
        					frequencyscore=0;	
        				}
                    bcv.setFrequencyscore(Integer.valueOf(frequencyscore));
                }
                System.out.println("frequencyscore::::::::::" + frequencyscore);
                PreparedStatement pstr = conn.prepareStatement("UPDATE progress_Report  SET  frequencyscore=? where id=?");
                pstr.setInt(1, frequencyscore);
                pstr.setLong(2, id);
                pstr.execute();
                System.out.println("UPDATEfrequencyscore::::::::::" + frequencyscore);
                pstr.close();
                frqList.add(bcv);
            }
            rs.close();
            pst.close();
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return frqList;
    }

    public BarchartData getMostRecentdata(long programId) throws Exception {
        BarchartData recentdata;
        Connection conn = null;
        conn = this.ds.getConnection();
        recentdata = new BarchartData();
        PreparedStatement pst = conn.prepareStatement("select * from progress_report where programid=? order by datedone desc limit 1");
        pst.setLong(1, programId);
        ResultSet rsset = pst.executeQuery();
        try {
            if (rsset.next()) {
                recentdata.setId(Long.valueOf(rsset.getLong("id")));
                long id = rsset.getLong("id");
                recentdata.setDuration(Integer.valueOf(rsset.getInt("duration")));
                recentdata.setFinished(Boolean.valueOf(rsset.getBoolean("finished")));
                recentdata.setLastProgram(Integer.valueOf(rsset.getInt("lastProgramId")));
                recentdata.setProgramId(Integer.valueOf(rsset.getInt("patientId")));
                recentdata.setPatientId(Integer.valueOf(rsset.getInt("programId")));
                recentdata.setDateDone((java.util.Date)rsset.getDate("dateDone"));
                recentdata.setProgramFreqType(rsset.getString("programFreqType"));
                recentdata.setProgramFreqValue(Integer.valueOf(rsset.getInt("programFreqValue")));
                recentdata.setPainLevel(rsset.getString("painlevel"));
                recentdata.setTotalduration(Integer.valueOf(rsset.getInt("totalduration")));
                recentdata.setAdherncescore(Integer.valueOf(rsset.getInt("adherncescore")));
                recentdata.setFrequencyscore(Integer.valueOf(rsset.getInt("frequencyscore")));
                recentdata.setNote(rsset.getString("note"));
            }
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return recentdata;
    }

    public Boolean findpgidinprogressreport(long programId) throws Exception {
        Boolean result;
        Connection conn = null;
        result = false;
        conn = this.ds.getConnection();
        PreparedStatement pst = conn.prepareStatement("select * from progress_report where programid=? ");
        pst.setLong(1, programId);
        ResultSet rsset = pst.executeQuery();
        try {
            if (rsset.next()) {
                result = true;
            }
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return result;
    }
    
    public BarchartData sumOfData(long programId, String startDate, String endDate) throws Exception {
        ArrayList<BarchartData> barchart;
        SimpleDateFormat s =  new SimpleDateFormat("dd/MM/yyyy");
        Calendar cal = Calendar.getInstance();
        cal.setTime(s.parse(endDate));
        cal.add(5, 1);
        java.util.Date end = cal.getTime();
        endDate = s.format(end);
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        java.util.Date date = formatter.parse(startDate);
        Timestamp fromDate = new Timestamp(date.getTime());
        java.util.Date date1 = formatter.parse(endDate);
        Timestamp toDate = new Timestamp(date1.getTime());
        barchart = new ArrayList<BarchartData>();
        Connection conn = this.ds.getConnection();
        BarchartData avg = new BarchartData();
        try {
            PreparedStatement avgofps = conn.prepareStatement("SELECT id,duration,programfreqvalue,datedone FROM (SELECT *,ROW_NUMBER() OVER (PARTITION BY datedone::date ORDER BY datedone DESC) rn FROM progress_Report WHERE programid = ?  and datedone>=? and datedone<=?) t WHERE rn = 1;");
                avgofps.setLong(1, programId);
                avgofps.setTimestamp(2, fromDate);
                avgofps.setTimestamp(3, toDate);
                ResultSet avgrs = avgofps.executeQuery();
                System.out.println("avgofps" + avgofps);

            if (avgrs.next() == false) { 
            	
                avg.setDuration(Integer.valueOf(0));
                avg.setProgramFreqValue(Integer.valueOf(0));
                avg.setDateDone(fromDate);
            	System.out.println("ResultSet in empty in Java");
            	}else{
                avg.setDuration(Integer.valueOf(avgrs.getInt("duration")));
                avg.setProgramFreqValue(Integer.valueOf(avgrs.getInt("programfreqvalue")));
             avg.setDateDone(avgrs.getTimestamp("datedone"));
             System.out.println(avgrs.getTimestamp("datedone"));
            }
            avgofps.close();
            avgrs.close();
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return avg;
    }

	public LinkedList<ProgramExercise> getAdherenceScore(int parseInt) {
		// TODO Auto-generated method stub
		return null;
	}

}
