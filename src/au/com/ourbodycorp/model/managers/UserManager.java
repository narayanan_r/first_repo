package au.com.ourbodycorp.model.managers;

import au.com.ourbodycorp.Config;
import au.com.ourbodycorp.MailMessage;
import au.com.ourbodycorp.UserMap;
import au.com.ourbodycorp.model.Attachment;
import au.com.ourbodycorp.model.EmailChange;
import au.com.ourbodycorp.model.User;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.sql.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.UUID;

public class UserManager extends ManagerBase {
    
    public static final int TUTORIAL_NOT_DONE = 0;
    public static final int TUTORIAL_WENT_TO_PATIENT_PAGE = 1;
    public static final int TUTORIAL_WENT_TO_PROGRAM_PAGE = 2;
    public static final int TUTORIAL_FINISHED = 100;
    public static final int TUTORIAL_NOT_AVAILABLE = 105;

    public static final String USER_SQL = "select u.*, c.name as countryName from users u left join country c on u.country = c.iso";

    public User createUser(ResultSet rs) throws SQLException {
        User u = new User();

        u.setAbout(rs.getString("about"));
        u.setName(rs.getString("username"));
        u.setAddress1(rs.getString("address1"));
        u.setAddress2(rs.getString("address2"));
        u.setAttachmentId(rs.getLong("attachmentId"));
        u.setEmail(rs.getString("email"));
        u.setTitle(rs.getString("title"));
        u.setFirstName(rs.getString("firstName"));
        u.setId(rs.getLong("id"));
        u.setRememberKey(rs.getString("rememberKey"));
        u.setLastName(rs.getString("lastName"));
        u.setPassword(rs.getString("password"));
        u.setPhone(rs.getString("phone"));
        u.setMobile(rs.getString("mobile"));
        u.setPostcode(rs.getString("postcode"));
        u.setRegistered(rs.getTimestamp("registered"));
        u.setLastSeen(rs.getTimestamp("lastSeen"));
        u.setState(rs.getString("state"));
        u.setStatus(User.Status.valueOf(rs.getString("status")));
        u.setSuburb(rs.getString("suburb"));
        u.setCountry(rs.getString("country"));
        u.setLastCorp(rs.getLong("last_corp"));
        u.setDisclaimed(rs.getBoolean("disclaimed"));
        u.setApplicationId(rs.getInt("application_id"));
        u.setClinikoAPIKey(rs.getString("cliniko_apikey"));
        try {
            u.setCountryName(rs.getString("countryName"));
        }
        catch (Exception e) {
            // Dont' care;
        }
        u.setVerifyKey(rs.getString("verifyKey"));
        if (rs.getString("type") != null) {
            u.setType(User.Type.valueOf(rs.getString("type")));
        }

        boolean extended;
        try {
            extended = rs.findColumn("caption") > 0;
        }
        catch (SQLException e) {
            extended = false;
        }

        if (extended) {
            if (rs.getInt("attachmentId") > 0) {
                Attachment a = new Attachment();
                a.setId(rs.getInt("attachmentId"));
                a.setFilename(rs.getString("filename"));
                a.setCaption(rs.getString("caption"));
                u.setAttachment(a);
            }
        }

        return u;
    }

    public User getUser(long id) throws Exception {
        User user = null;

        Connection conn = null;

        try {
            conn = ds.getConnection();
            PreparedStatement ps = conn.prepareStatement(USER_SQL + " where u.id = ?");
            ps.setLong(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                user = createUser(rs);
            }
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }


        return user;
    }

    public User getUserByEmailOrUserName(String key) throws Exception {
        User user = null;

        Connection conn = null;

        try {
            conn = ds.getConnection();
            PreparedStatement ps = conn.prepareStatement(USER_SQL + " where email = ? or username = ?");
            ps.setString(1, key);
            ps.setString(2, key);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                user = createUser(rs);
            }
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }


        return user;
    }
    
    public User getUserByCorpid(Long key) throws Exception {
        User user = null;
        Long userid = null;

        Connection conn = null;

        try {
            conn = ds.getConnection();
            PreparedStatement ps = conn.prepareStatement("select userid from corp_users where corpid =?");
            ps.setLong(1, key);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
            	userid = rs.getLong("userid");
            }
            PreparedStatement psm = conn.prepareStatement(USER_SQL + " where id = ?");
            psm.setLong(1, userid);
            ResultSet rsm = psm.executeQuery();
            if (rsm.next()) {
                user = createUser(rsm);
            }
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }


        return user;
    }

    public List<User> findUsers(String key, int limit, int offset) throws Exception {
        LinkedList<User> users = new LinkedList<User>();

        Connection conn = null;

        try {
            conn = ds.getConnection();
            PreparedStatement ps = conn.prepareStatement(USER_SQL + " where lower(email) like ? or lower(lastname) like ? limit ? offset ?");
            ps.setString(1, key + "%");
            ps.setString(2, key + "%");
            ps.setInt(3, limit);
            ps.setInt(4, offset);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                users.add(createUser(rs));
            }
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }

        return users;
    }

    public User getUserByEmail(String email) throws Exception {
        User user = null;

        Connection conn = null;

        try {
            conn = ds.getConnection();
            PreparedStatement ps = conn.prepareStatement(USER_SQL + " where lower(email) = ?");
            ps.setString(1, email.toLowerCase());
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                user = createUser(rs);
            }
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }


        return user;
    }

    public User getUserByRememberKey(String key) throws Exception {
        User user = null;

        Connection conn = null;

        try {
            conn = ds.getConnection();
            PreparedStatement ps = conn.prepareStatement(USER_SQL + " where rememberKey = ?");
            ps.setString(1, key);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                user = createUser(rs);
            }
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return user;
    }

    public User getUserByVerifyKey(String key) throws Exception {
        User user = null;

        Connection conn = null;

        try {
            conn = ds.getConnection();
            PreparedStatement ps = conn.prepareStatement(USER_SQL + " where verifyKey = ?");
            ps.setString(1, key);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                user = createUser(rs);
            }
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return user;
    }

    public User getUserByPasswordResetKey(String key) throws Exception {
        User user = null;

        Connection conn = null;

        try {
            conn = ds.getConnection();
            PreparedStatement ps = conn.prepareStatement(USER_SQL + " where id = (select userId from pwreset where resetKey = ? and expires > now())");
            ps.setString(1, key);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                user = createUser(rs);
            }
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return user;
    }

    public void deletePasswordReset(long userId) throws Exception {
        Connection conn = null;
        try {
            conn = ds.getConnection();
            PreparedStatement pst = conn.prepareStatement("delete from pwreset where userId = ?");
            pst.setLong(1, userId);
            pst.execute();
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public List<String[]> getUserStatuses() throws Exception {
        LinkedList<String[]> result = new LinkedList<String[]>();

        Connection conn = null;

        try {
            conn = ds.getConnection();
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("select status, count(status) as num from users group by status order by status");
            while (rs.next()) {
                String[] row = new String[2];
                row[0] = rs.getString("status");
                row[1] = rs.getString("num");
                result.add(row);
            }
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return result;
    }

    public List<User> getUsers(User.Status status) throws Exception {
        LinkedList<User> users = new LinkedList<User>();

        Connection conn = null;

        try {
            conn = ds.getConnection();
            PreparedStatement ps;
            if (status != null) {
                ps = conn.prepareStatement(USER_SQL + " where status = ? order by registered desc");
                ps.setString(1, status.name());
            }
            else {
                ps = conn.prepareStatement(USER_SQL + " where status != ? order by registered desc");
                ps.setString(1, User.Status.deleted.name());
            }
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                User user = createUser(rs);
                users.add(user);
            }
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }

        return users;
    }

    public List<String> getRoles() throws Exception {
        LinkedList<String> roles = new LinkedList<String>();

        Connection conn = null;

        try {
            conn = ds.getConnection();
            PreparedStatement ps = conn.prepareStatement("select name from roles order by name");

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                roles.add(rs.getString("name"));
            }
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }

        return roles;

    }

    public List<String> getRolesForUser(long userId) throws Exception {
        LinkedList<String> roles = new LinkedList<String>();

        Connection conn = null;

        try {
            conn = ds.getConnection();
            PreparedStatement ps = conn.prepareStatement("select role from user_roles where userId = ? order by role");
            ps.setLong(1, userId);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                roles.add(rs.getString("role"));
            }
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }

        return roles;
    }

    public List<User> getUsersForRole(String role) throws Exception {
        LinkedList<User> users = new LinkedList<User>();

        Connection conn = null;

        try {
            conn = ds.getConnection();
            PreparedStatement ps = conn.prepareStatement(USER_SQL + " where status = ? and userId in (select userId from user_roles where role = ?) order by lower(firstName), lower(lastName)");
            ps.setString(1, User.Status.authorised.name());
            ps.setString(2, role);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                User user = createUser(rs);
                users.add(user);
            }
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }

        return users;
    }

    public void resetRememberKey(long id) throws Exception {
        Connection conn = null;
        try {
            conn = ds.getConnection();
            PreparedStatement pst = conn.prepareStatement("update users set rememberKey = ? where id = ?");
            pst.setString(1, UUID.randomUUID().toString());
            pst.setLong(2, id);
            pst.executeUpdate();
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public User saveUser(User user) throws Exception {
        Connection conn = null;
        String columns[] = {"id", "username", "firstName", "lastName", "email", "password", "address1",
            "address2", "suburb", "state", "postcode", "phone", "type", "about", "status", "attachmentId", "rememberKey",
            "lastSeen", "country", "mobile", "verifyKey", "title", "last_corp", "disclaimed"};
        try {
            conn = ds.getConnection();
            conn.setAutoCommit(false);
            PreparedStatement ps;
            if (user.getId() == 0) {
                user.setId(getSequence("userId"));
                String sql = createInsert("users", columns);
                ps = conn.prepareStatement(sql);
            }
            else {
                String sql = createUpdate("users", columns, "id = ?");
                ps = conn.prepareStatement(sql);
                ps.setLong(columns.length + 1, user.getId());
            }
            int i = 0;
            ps.setLong(++i, user.getId());
            ps.setString(++i, user.getName());
            ps.setString(++i, user.getFirstName());
            ps.setString(++i, user.getLastName());
            ps.setString(++i, user.getEmail());
            ps.setString(++i, user.getPassword());
            ps.setString(++i, user.getAddress1());
            ps.setString(++i, user.getAddress2());
            ps.setString(++i, user.getSuburb());
            ps.setString(++i, user.getState());
            ps.setString(++i, user.getPostcode());
            ps.setString(++i, user.getPhone());
            if (user.getType() != null) {
                ps.setString(++i, user.getType().name());
            }
            else {
                ps.setNull(++i, Types.VARCHAR);
            }
            ps.setString(++i, user.getAbout());
            ps.setString(++i, user.getStatus().name());
            if (user.getAttachmentId() < 1) {
                ps.setNull(++i, java.sql.Types.BIGINT);
            }
            else {
                ps.setLong(++i, user.getAttachmentId());
            }
            ps.setString(++i, user.getRememberKey());
            ps.setTimestamp(++i, new Timestamp(user.getLastSeen().getTime()));
            ps.setString(++i, user.getCountry());
            ps.setString(++i, user.getMobile());
            ps.setString(++i, user.getVerifyKey());
            ps.setString(++i, user.getTitle());
            if (user.getLastCorp() > 0) {
                ps.setLong(++i, user.getLastCorp());
            }
            else {
                ps.setNull(++i, Types.BIGINT);
            }
            ps.setBoolean(++i, user.isDisclaimed());
//            ps.setLong(++i, user.getApplicationId());

            ps.executeUpdate();
            ps.close();

            List<String> roles = (user.isRolesChanged()) ? user.getRoles() : null;
            if (roles != null) {
                ps = conn.prepareStatement("delete from user_roles where userId = ?");
                ps.setLong(1, user.getId());
                ps.executeUpdate();
                ps.close();
                ps = conn.prepareStatement("insert into user_roles (userId, role) values (?, ?)");
                for (String role : roles) {
                    ps.setLong(1, user.getId());
                    ps.setString(2, role);
                    ps.executeUpdate();
                }
                ps.close();
            }
            conn.setAutoCommit(true);
        }
        catch (SQLException e) {
            conn.rollback();
            throw e;
        }
        finally {
            if (conn != null) {
                conn.setAutoCommit(true);
                conn.close();
            }
        }

        return user;
    }

    public void updateStatus(User.Status status, int[] ids) throws Exception {
        if (ids.length == 0) {
            return;
        }

        Connection conn = null;

        try {
            conn = ds.getConnection();
            StringBuilder sb = new StringBuilder();
            sb.append("update users set status = ? where id in (");
            for (int i = 0; i < ids.length - 1; i++) {
                sb.append("?,");
            }
            sb.append("?)");

            PreparedStatement pst = conn.prepareStatement(sb.toString());
            pst.setString(1, status.name());
            int i = 1;
            for (int id : ids) {
                pst.setInt(++i, id);
            }

            pst.executeUpdate();

        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
    
    public void updateAPIKey(String apiKey, long id) throws Exception {

        Connection conn = null;

        try {
            conn = ds.getConnection();
            StringBuilder sb = new StringBuilder();
            sb.append("update users set cliniko_apikey = ? where id = ?");
            PreparedStatement pst = conn.prepareStatement(sb.toString());
            pst.setString(1, apiKey);
            pst.setLong(2, id);
            pst.executeUpdate();

        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public List<User> getSubscribed(long postId) throws Exception {
        LinkedList<User> users = new LinkedList<User>();

        Connection conn = null;

        try {
            conn = ds.getConnection();
            PreparedStatement ps = conn.prepareStatement("select * from users where status = ? "
                    + "and id in (select memberId from subscriptions where postId = ?) "
                    + "order by firstName, lastName");
            ps.setString(1, User.Status.authorised.name());
            ps.setLong(2, postId);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                User user = createUser(rs);
                users.add(user);
            }
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }

        return users;
    }

    public String startPasswordReset(long userId) throws Exception {
        Connection conn = null;
        String key = UUID.randomUUID().toString();
        try {
            conn = ds.getConnection();
            PreparedStatement pst = conn.prepareStatement("delete from pwreset where userId = ?");
            pst.setLong(1, userId);
            pst.executeUpdate();
            pst.close();
            pst = conn.prepareStatement("insert into pwreset (userId, resetKey) values (?, ?)");
            pst.setLong(1, userId);
            pst.setString(2, key);
            pst.executeUpdate();
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return key;
    }

    public String startEmailChange(long userId, String email) throws Exception {
        Connection conn = null;
        String key = UUID.randomUUID().toString();
        try {
            conn = ds.getConnection();
            PreparedStatement pst = conn.prepareStatement("delete from emailchange where userId = ?");
            pst.setLong(1, userId);
            pst.executeUpdate();
            pst.close();
            pst = conn.prepareStatement("insert into emailchange (userId, changeKey, email) values (?, ?, ?)");
            pst.setLong(1, userId);
            pst.setString(2, key);
            pst.setString(3, email);
            pst.executeUpdate();
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return key;
    }

    public void sendEmailChangeEmail(User user, String key, String email) throws Exception {
        Properties props = new Properties();
        props.put("firstName", user.getFirstName());
        props.put("lastName", user.getLastName());
        props.put("email", email);
        props.put("url", Config.getString("url") + "/users/email3.jsp?key=" + key);
        MailMessage msg = new MailMessage(props, "email_new.vm", email, "Change of email address",false);

        try {
            msg.send();
        }
        catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    public EmailChange getEmailForChangeKey(String key) throws Exception {
        EmailChange ec = null;
        Connection conn = null;
        try {
            conn = ds.getConnection();
            PreparedStatement pst = conn.prepareStatement("select * from emailchange where changeKey = ? and expires > now()");
            pst.setString(1, key);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                ec = new EmailChange();
                ec.setEmail(rs.getString("email"));
                ec.setKey(rs.getString("changeKey"));
                ec.setUserId(rs.getLong("userId"));
            }
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return ec;
    }

    public void deleteEmailChange(String key) throws Exception {
        Connection conn = null;
        try {
            conn = ds.getConnection();
            PreparedStatement pst = conn.prepareStatement("delete from emailchange where changeKey = ? or expires > now()");
            pst.setString(1, key);
            pst.executeUpdate();
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
    }

    public boolean isEmailAvailable(String email, long userId) throws Exception {
        Connection conn = null;
        boolean available = true;
        try {
            conn = ds.getConnection();
            PreparedStatement pst = conn.prepareStatement("select * from users where lower(email) = ? and id != ?");
            pst.setString(1, email.toLowerCase());
            pst.setLong(2, userId);
            ResultSet rs = pst.executeQuery();
            available = !rs.next();
            pst.close();

            if (available) {
                pst = conn.prepareStatement("select * from emailchange where lower(email) = ? and userId != ? and expires > now()");
                pst.setString(1, email.toLowerCase());
                pst.setLong(2, userId);
                rs = pst.executeQuery();
                available = !rs.next();
            }

        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return available;
    }

    public void sendVerificationEmail(User user) throws Exception {
        Properties props = new Properties();
        props.put("firstName", user.getFirstName());
        props.put("lastName", user.getLastName());
        props.put("email", user.getEmail());
        props.put("url", Config.getString("url") + "/users/v.jsp?key=" + user.getVerifyKey());
        MailMessage msg = new MailMessage(props, "applied.vm", user.getEmail(), "Please verify your email",false);

        try {
            msg.send();
        }
        catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    public void sendPasswordResetEmail(User user, String key) throws Exception {
        Properties props = new Properties();
        props.put("firstName", user.getFirstName());
        props.put("lastName", user.getLastName());
        props.put("email", user.getEmail());
        props.put("url", Config.getString("url") + "/users/reset.jsp?key=" + key);
        MailMessage msg = new MailMessage(props, "pwreset.vm", user.getEmail(), "Password reset request",false);

        try {
            msg.send();
        }
        catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    public void logout(HttpSession session, HttpServletResponse response) throws Exception {
        User u = (User) session.getAttribute("user");
        if (u != null) {
            resetRememberKey(u.getId());
            /*System.out.println("UserMap.userMap.size() "+UserMap.userMap.size());
            
            User userFromMap= UserMap.userMap.get(u.getEmail());
            System.out.println(" userFromMap in logout "+userFromMap);
            if(userFromMap!=null){
         	   System.out.println("Removing user "+u.getEmail());
         	   UserMap.userMap.remove(u.getEmail());
         	  UserMap.sessionMap.remove(u.getEmail());
            }*/
        }
      
        session.invalidate();
        Cookie c = new Cookie("remember", "");
        c.setMaxAge(0);
        c.setPath("/");
        c.setDomain(Config.getString("cookieDomain"));
        response.addCookie(c);

    }

    public void logoutFromExpiry(HttpSession session, HttpServletResponse response) throws Exception {
        User u = (User) session.getAttribute("user");
        if (u != null) {
            resetRememberKey(u.getId());
           // System.out.println("UserMap.userMap.size() "+UserMap.userMap.size());
            
          /*  User userFromMap= UserMap.userMap.get(u.getEmail());
            System.out.println(" userFromMap in logout "+userFromMap);
            if(userFromMap!=null){
         	   System.out.println("Removing user "+u.getEmail());
         	   UserMap.userMap.remove(u.getEmail());
         	  UserMap.sessionMap.remove(u.getEmail());
         	 
            }*/
        }
      
        session.invalidate();
        Cookie c = new Cookie("remember", "");
        c.setMaxAge(0);
        c.setPath("/");
        c.setDomain(Config.getString("cookieDomain"));
        response.addCookie(c);

    }

    public List<User> getUsersForCompany(long companyId) throws Exception {
        Connection conn = ds.getConnection();
        LinkedList<User> users = new LinkedList<User>();
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("select * from users where id in (select user_id from company_owners where company = " + companyId + ")");
            while (rs.next()) {
                User u = createUser(rs);
                users.add(u);
            }
        }
        finally {
            conn.close();
        }
        return users;

    }

    public List<String> getAllEmail() throws Exception {
        Connection conn = ds.getConnection();
        LinkedList<String> email = new LinkedList<String>();
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("select email from users where email is not null and email != ''");
            while (rs.next()) {
                email.add(rs.getString("email"));
            }
        }
        finally {
            conn.close();
        }
        return email;
    }
    
    public int getTutorialProgressForUser(long userId) throws Exception{
        Connection conn = null;
        try {
            conn = ds.getConnection();
            PreparedStatement ps = conn.prepareStatement("select tutorial_progress from user_settings where userid = ?");
            ps.setLong(1, userId);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt("tutorial_progress");
            }
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return TUTORIAL_NOT_AVAILABLE;
    }
    
    public void setTutorialProgressForUser(long userId, int tutorialProgress, boolean newUser) throws Exception{
        Connection conn = null;
        try {
            conn = ds.getConnection();
            PreparedStatement pst = (newUser)? conn.prepareStatement("insert into user_settings(tutorial_progress, userid) values(?, ?)") : conn.prepareStatement("update user_settings set tutorial_progress = ? where userid = ?");
            pst.setInt(1, tutorialProgress);
            pst.setLong(2, userId);
            pst.executeUpdate();
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
}
