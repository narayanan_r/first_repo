package au.com.ourbodycorp.model.managers;

import au.com.ourbodycorp.Util;
import au.com.ourbodycorp.model.Program;
import au.com.ourbodycorp.model.ProgramMessage;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import javax.net.ssl.HttpsURLConnection;

/**
 *
 * @author rustyshelf
 */
public class ProgramMessageManager extends ManagerBase {

    public List<ProgramMessage> listMessages(long programId, int limit, int offset) throws Exception {
        LinkedList<ProgramMessage> messages = new LinkedList<ProgramMessage>();
        Connection conn = ds.getConnection();
        ProgramMessage message;
        try {
            String sql = "select * from program_messages where program_id = ?";

            sql += " order by created desc";
            if (limit > 0) {
                sql += " limit " + limit;
            }
            if (offset > 0) {
                sql += " offset " + offset;
            }
            PreparedStatement pst = conn.prepareStatement(sql);
            pst.setLong(1, programId);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                message = new ProgramMessage();
                messages.add(message);
                message.setId(rs.getLong("id"));
                message.setBody(rs.getString("body"));
                message.setProgramId(rs.getLong("program_id"));
                message.setCreatedDate(rs.getTimestamp("created"));
                message.setHeadline(rs.getString("headline"));
                message.setLink(rs.getString("link"));
                message.setPhotoId(rs.getLong("photo"));
            }
            rs.close();
            pst.close();
        }
        finally {
            conn.close();
        }

        return messages;
    }

    public ProgramMessage save(ProgramMessage message) throws Exception {
        String columns[] = {"id", "program_id", "created", "headline", "body", "link", "photo"};
        Connection conn = ds.getConnection();
        try {
            PreparedStatement pst;
            if (message.getId() == 0) {
                message.setId(getSequence("programMessageId", conn));
                pst = conn.prepareStatement(createInsert("program_messages", columns));
            }
            else {
                pst = conn.prepareStatement(createUpdate("program_messages", columns, "id = ?"));
                pst.setLong(columns.length + 1, message.getId());
            }
            int i = 0;
            pst.setLong(++i, message.getId());
            pst.setLong(++i, message.getProgramId());
            pst.setTimestamp(++i, new Timestamp(message.getCreatedDate().getTime()));
            pst.setString(++i, message.getHeadline());
            pst.setString(++i, message.getBody());
            pst.setString(++i, message.getLink());
            if (message.getPhotoId() > 0) {
                pst.setLong(++i, message.getPhotoId());
            }
            else {
                pst.setNull(++i, Types.BIGINT);
            }
            pst.executeUpdate();
            pst.close();
        }
        finally {
            conn.close();
        }

        return message;
    }

    public void deleteMessage(long id) throws Exception {
        Connection conn = ds.getConnection();
        try {
            Statement st = conn.createStatement();
            st.execute("delete from program_messages where id = " + id);
            st.close();
        }
        finally {
            conn.close();
        }
    }

    public ProgramMessage getMessage(long messageId) throws Exception {
        Connection conn = ds.getConnection();
        ProgramMessage message = null;
        try {
            PreparedStatement pst = conn.prepareStatement("select * from program_messages where id = ?");
            pst.setLong(1, messageId);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                message = new ProgramMessage();
                message.setId(rs.getLong("id"));
                message.setBody(rs.getString("body"));
                message.setProgramId(rs.getLong("program_id"));
                message.setCreatedDate(rs.getTimestamp("created"));
                message.setHeadline(rs.getString("headline"));
                message.setLink(rs.getString("link"));
                message.setPhotoId(rs.getLong("photo"));
            }
            rs.close();
            pst.close();
        }
        finally {
            conn.close();
        }
        return message;
    }
    
    public void send(ProgramMessage message) throws Exception {
        Program progam = new LibraryManager().getProgram(message.getProgramId());
        
        OutputStream outputStream = null;
        HttpsURLConnection connection = null;
        BufferedWriter writer = null;
        try {
            URL pushUrl = new URL("https://api.parse.com/1/push");
            connection = (HttpsURLConnection) pushUrl.openConnection();
            
            connection.setRequestProperty("X-Parse-Application-Id", "8aZhlEK7gJdVdTNv4IPHljCxtDlucfNkHsELmT62");
            connection.setRequestProperty("X-Parse-REST-API-Key", "k7hkrEqJsHyG8l4EVA5hy9l5P9919BKjEmzWGVm8");  
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(15000);
            connection.setReadTimeout(15000);
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);
            connection.setRequestMethod("POST");

            
            LinkedHashMap<String, Object> jsonMap = new LinkedHashMap<String, Object>();
            ArrayList channels = new ArrayList();
            jsonMap.put("channels", channels);
            channels.add("Program-" + progam.getCode().toUpperCase());
            
            LinkedHashMap<String, Object> data = new LinkedHashMap<String, Object>();
            jsonMap.put("data", data);
            data.put("alert", message.getHeadline());
            data.put("badge", "Increment");
            data.put("sound", "notif.aif");
            data.put("action", "com.fiizio.fiiziolive");
            data.put("msgid", message.getId());
            
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            String jsonPostStr = gson.toJson(jsonMap);
            
            outputStream = connection.getOutputStream();
            writer = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
            writer.write(jsonPostStr);
           // System.out.println("String:" + jsonPostStr);
            writer.close();
            outputStream.close();
            if (connection.getResponseCode() != 200){
                System.out.println("Response Message:" + connection.getResponseMessage() + " with code " + connection.getResponseCode());
                throw new IOException("Push sending failed");
            }
            
           // System.out.println("Sent push message");
        }
        catch (Exception e){
            e.printStackTrace();
            throw new IOException("Push sending failed");
        }
        finally {
            Util.close(writer);
            Util.close(outputStream);
            Util.close(connection);
        }
    }
}
