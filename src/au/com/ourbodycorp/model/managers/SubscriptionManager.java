package au.com.ourbodycorp.model.managers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;

import au.com.ourbodycorp.model.Corporation;
import au.com.ourbodycorp.model.PaymentSubscription;
import au.com.ourbodycorp.model.Subscription;

public class SubscriptionManager extends ManagerBase {

	public List<Corporation> getAllClinics() throws Exception {
    	List<Corporation> clinicList = new ArrayList<Corporation>();
        Connection conn = null;
        try {
            conn = ds.getConnection();
            PreparedStatement pst = conn.prepareStatement("select id,name from corporation");
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
            	Corporation corporation = new Corporation();
            	corporation.setId(rs.getLong("id"));
            	corporation.setName(rs.getString("name"));
            	CorporationManager cm = new CorporationManager();
            	int subLocCnt = cm.getSubLocationCount(corporation.getId());
            	String subLocCntWithId = corporation.getId()+"-"+subLocCnt;
            	corporation.setSubLocCntWithId(subLocCntWithId);
            	clinicList.add(corporation);
            }
            rs.close();
            pst.close();
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return clinicList;
    }
	
	public Subscription save(Subscription subscription) throws Exception {
        String columns[] = {"id", "corpid", "subscription", "subscription_period", "subscription_period_type", "subscription_clinic_amount", "subscription_patient_amount", "sharesubscription", "creator", "sharesubscriptionid", "active","planineffectivedate","currency_type"};
        Connection conn = null;
        try {
            conn = ds.getConnection();
            conn.setAutoCommit(false);
            PreparedStatement pst;
            if (subscription.getSubscriptionId() == 0) {
            	subscription.setSubscriptionId(getSequence("subscriptionid"));
                pst = conn.prepareStatement(createInsert("subscription", columns));
            }
            else {
                pst = conn.prepareStatement(createUpdate("subscription", columns, "id = ?"));
                pst.setLong(columns.length + 1, subscription.getSubscriptionId());
            }
            int i=0;
            pst.setLong(++i, subscription.getSubscriptionId());
            pst.setLong(++i, subscription.getCorporation());
            pst.setString(++i, subscription.getSubscription());
            pst.setLong(++i, subscription.getSubscriptionPeriod());
            pst.setString(++i, subscription.getSubPeriodType());
            pst.setFloat(++i, subscription.getSubscriptionClinicAmt());
            pst.setFloat(++i, subscription.getSubscriptionPatientAmt());
            pst.setBoolean(++i, subscription.isShareSubscription());
            pst.setLong(++i, subscription.getCreator());
            if(subscription.getSharesubscriptionid()>0){
            	pst.setLong(++i, subscription.getSharesubscriptionid());
            }else{
            	subscription.setSharesubscriptionid(subscription.getSubscriptionId());
            	pst.setLong(++i, subscription.getSharesubscriptionid());
            }
            pst.setBoolean(++i, subscription.isActive());
            if(subscription.getPlanineffectivedate()!=null){
            	pst.setTimestamp(++i, new Timestamp(subscription.getPlanineffectivedate().getTime()));
            }else{
            	pst.setTimestamp(++i, null);
            }
            pst.setString(++i, subscription.getCurrencyType());
            pst.executeUpdate();
            pst.close();
            conn.commit();
        }
        finally {
            if (conn != null) {
                conn.setAutoCommit(true);
                conn.close();
            }
        }
        return subscription;
    }
	
	public Subscription CreateSubscription(ResultSet rs) throws SQLException {
		Subscription s=new Subscription();
		s.setSubscriptionId(rs.getLong("id"));
		s.setCorporation(rs.getInt("corpid"));
		s.setSubscription(rs.getString("subscription"));
		s.setSubscriptionPeriod(rs.getInt("subscription_period"));
		s.setSubPeriodType(rs.getString("subscription_period_type"));
		s.setSubscriptionClinicAmt(rs.getFloat("subscription_clinic_amount"));
		s.setSubscriptionPatientAmt(rs.getFloat("subscription_patient_amount"));
		s.setCreator(rs.getInt("creator"));
		s.setSharesubscriptionid(rs.getLong("sharesubscriptionid"));
		s.setShareSubscription(rs.getBoolean("sharesubscription"));
		s.setActive(rs.getBoolean("active"));
		s.setPlanineffectivedate(rs.getTimestamp("planineffectivedate"));
		s.setCreated((rs.getTimestamp("created")));
		s.setCurrencyType(rs.getString("currency_type"));
		return s;
	}
	
	public String getCorpName(long id) throws SQLException
	{
		String name = null;
		Connection conn = null;
        try {
            conn = ds.getConnection();
            PreparedStatement pst = conn.prepareStatement("select name from corporation where id=?");
            pst.setLong(1, id);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
            	name=rs.getString("name");
            }
            rs.close();
            pst.close();
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
		
		return name;
		
	}
	public List<Subscription> getAllSubscription(int offset, int limit) throws Exception {
    	List<Subscription> subscriptionList = new ArrayList<Subscription>();
        Connection conn = null;
        try {
            conn = ds.getConnection();
            PreparedStatement pst = conn.prepareStatement("select * from subscription order by id desc offset ? limit ?");
            pst.setInt(1, offset);
            pst.setInt(2, limit);
            SubscriptionManager sm = new SubscriptionManager();
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
            	
            	Subscription s=sm.CreateSubscription(rs);
            	
            	subscriptionList.add(s);
            }
            rs.close();
            pst.close();
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return subscriptionList;
    }
	public List<Subscription> getSearchSubscription(String query,int offset, int limit) throws Exception {
    	List<Subscription> subscriptionList = new ArrayList<Subscription>();
        Connection conn = null;
        try {
            conn = ds.getConnection();
            PreparedStatement pst = conn.prepareStatement("select * from subscription where (lower(subscription) like lower(?) or corpid in (select id from corporation where lower(name) like lower(?))) order by id desc offset ? limit ?");
            pst.setString(1, query.toLowerCase() + "%");
            pst.setString(2, query.toLowerCase() + "%");
            pst.setInt(3, offset);
            pst.setInt(4, limit);
            SubscriptionManager sm = new SubscriptionManager();
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
            	Subscription s=sm.CreateSubscription(rs);
            	subscriptionList.add(s);
            }
            rs.close();
            pst.close();
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return subscriptionList;
    }
	public void deleteSubscription(Long id) throws Exception {
        Connection conn = null;
        try {
            conn = ds.getConnection();
            PreparedStatement pst = conn.prepareStatement("delete from subscription where id= ?");
            pst.setLong(1, id);
            pst.executeUpdate();
            pst.close();
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
    }
	public Subscription getSubscriptions(long subscriptionId) throws Exception {
        Connection conn = null;
        Subscription subscriptions = new Subscription();
        try {
            conn = ds.getConnection();
            PreparedStatement pst = conn.prepareStatement("select * from subscription where id= ?");
            pst.setLong(1, subscriptionId);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
            	subscriptions = CreateSubscription(rs);
            }
            rs.close();
            pst.close();
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return subscriptions;
    }
	public List<Subscription> getSubscriptionWithShareidList(long shareid,Long corpid) throws Exception {
    	List<Subscription> subscriptionList = new ArrayList<Subscription>();
        Connection conn = null;
        try {
            conn = ds.getConnection();
            PreparedStatement pst = conn.prepareStatement("select * from subscription where sharesubscriptionid = ? and id <> ? and corpid=?");
            pst.setLong(1, shareid);
            pst.setLong(2, shareid);
            pst.setLong(3, corpid);
            SubscriptionManager sm = new SubscriptionManager();
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
            	Subscription s=sm.CreateSubscription(rs);
            	subscriptionList.add(s);
            }
            rs.close();
            pst.close();
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return subscriptionList;
    }
	
	public List<Subscription> getSubscriptionWithShareid(long shareid) throws Exception {
    	List<Subscription> subscriptionList = new ArrayList<Subscription>();
        Connection conn = null;
        try {
            conn = ds.getConnection();
            PreparedStatement pst = conn.prepareStatement("select * from subscription where sharesubscriptionid = ? and id <> ?");
            pst.setLong(1, shareid);
            pst.setLong(2, shareid);
            SubscriptionManager sm = new SubscriptionManager();
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
            	Subscription s=sm.CreateSubscription(rs);
            	subscriptionList.add(s);
            }
            rs.close();
            pst.close();
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return subscriptionList;
    }
	
	public int getSubscripedPatientCnt(long subscriptionId) throws Exception {
        Connection conn = null;
        int subPatientCnt = 0;
        try {
            conn = ds.getConnection();
            PreparedStatement pst = conn.prepareStatement("select count(*) as cnt from patient where subscription_id = ?");
            pst.setLong(1, subscriptionId);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
            	subPatientCnt = rs.getInt("cnt");
            }
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return subPatientCnt;
    }
	
	public int countSubscription() throws Exception {
        Connection conn = null;
        int cntSubscription = 0;
        try {
            conn = ds.getConnection();
            PreparedStatement pst = conn.prepareStatement("select count(*) as cnt from subscription");
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
            	cntSubscription = rs.getInt("cnt");
            }
            rs.close();
            pst.close();
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return cntSubscription;
    }
	public int countSearchSubscription(String strSearch) throws Exception {
        Connection conn = null;
        int cntSubscription = 0;
        try {
            conn = ds.getConnection();
            PreparedStatement pst = conn.prepareStatement("select count(*) as cnt from subscription where  (lower(subscription) like lower(?) or corpid in (select id from corporation where lower(name) like lower(?)))");
            pst.setString(1, strSearch.toLowerCase() + "%");
            pst.setString(2, strSearch.toLowerCase() + "%");
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
            	cntSubscription = rs.getInt("cnt");
            }
            rs.close();
            pst.close();
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return cntSubscription;
    }
	public List<Subscription> getSubcriptionNames(long corpid,long patientId) throws Exception {
        List<Subscription> subscriptionList = new ArrayList<Subscription>();
        Connection conn = null;
        try {
            conn = ds.getConnection();
            PreparedStatement pst = conn.prepareStatement("select id,subscription from subscription where corpid=? and active=true");
            pst.setLong(1,corpid);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                Subscription s = new Subscription();
                s.setSubscriptionId(rs.getLong("id"));
                s.setSubscription(rs.getString("subscription"));
                subscriptionList.add(s);
            }
            rs.close();
            pst.close();
            pst = conn.prepareStatement("select p.subscriptiondate,s.subscription_period,s.subscription_period_type,s.id,s.subscription from patient p,subscription s where s.id=p.subscription_id and s.active=false and p.id=?");
            pst.setLong(1, patientId);
            rs = pst.executeQuery();
 		   	if (rs.next()) {
 			   int  period =rs.getInt("subscription_period");
 			   String type =rs.getString("subscription_period_type");
 			   DateTime now =new DateTime();
 			   DateTime subscriptionDate=new DateTime(rs.getTimestamp("subscriptiondate"));
 			   DateTime expired=null;
 			  if(type.equalsIgnoreCase("day")){
 			      expired = subscriptionDate.plusDays((period));
 			  }else if(type.equalsIgnoreCase("week")){
 				  expired = subscriptionDate.plusWeeks(period);
 			  }else if(type.equalsIgnoreCase("month")){
 				  expired = subscriptionDate.plusMonths(period);
 			  }else if(type.equalsIgnoreCase("year")){
 				  expired = subscriptionDate.plusYears(period);
 			  }
 			  if(now.toLocalDate().isBefore(expired.toLocalDate())){
 				 Subscription s = new Subscription();
                 s.setSubscriptionId(rs.getLong("id"));
                 s.setSubscription(rs.getString("subscription"));
                 subscriptionList.add(s);
 			  }
 		   	}
	 	      rs.close();
	 	      pst.close();
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return subscriptionList;
    }
 
    public long getPatientSubscriptionId(long id) throws Exception {
        long subId=0;
        Connection conn = null;
        try {
            conn = ds.getConnection();
            PreparedStatement pst = conn.prepareStatement("select subscription_id from patient where id=?");
            pst.setLong(1, id);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                subId=rs.getLong("subscription_id");
            }
            rs.close();
            pst.close();
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
        return subId;
    }
    
    public void updatePatientSubExpiryDate(long patientId, String email)throws Exception{
    	LibraryManager libraryManager = new LibraryManager();
    	PaymentSubscription ps = new PaymentSubscription();
    	try{
    		ps = libraryManager.findPaymentSubscriptionById(patientId);
    		if(ps != null){
				Connection connection = ds.getConnection();
				PreparedStatement pst = connection.prepareStatement("update patient set subscription_id = 1 where id = ?");
				pst.setLong(1,patientId);
				pst.executeUpdate();
				pst.close();
				pst = connection.prepareStatement("update payment_subscription set sub_id = 1 where id = ?");
				pst.setLong(1,ps.getId());
				pst.executeUpdate();
				pst.close();
				connection.close();
    		}
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    }
    public boolean checkSubscriptionExists(String subscription,String corpid,long id)throws Exception{
    	 Connection conn = null;
    	 Long corpId=Long.parseLong((corpid));
    	 boolean stat=false;
    	 try{
    		conn = ds.getConnection();
    		PreparedStatement pst =null;
    		if(id>0){
    			pst = conn.prepareStatement("select subscription from subscription where corpid=? and subscription=? and id <> ?");
    			pst.setLong(3, id);
    		}else{
    			pst = conn.prepareStatement("select subscription from subscription where corpid=? and subscription=?");
    	 	}
            pst.setLong(1, corpId);
            pst.setString(2,subscription );
            ResultSet rs = pst.executeQuery();
            if(rs.next()) {
            	stat=true;	
            	
            }
            rs.close();
            pst.close();
            conn.close();
    		}
	    	finally {
	            if (conn != null) {
	                conn.close();
	            }
	        }
    	return stat;
    }
}
 