package au.com.ourbodycorp.model.managers;

import au.com.ourbodycorp.model.Country;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

public class CountryManager extends ManagerBase {
  private Country createCountry(ResultSet rs) throws Exception {
    Country country = new Country();

    country.setIso(rs.getString("iso"));
    country.setName(rs.getString("name"));
    country.setDisplayName(rs.getString("printable_name"));
    country.setIso3(rs.getString("iso3"));
    country.setNum(rs.getInt("numcode"));
    country.setDial(rs.getString("dial"));

    return country;
  }

  public List<Country> listCountries() throws Exception {
    List<Country> countries = new LinkedList<Country>();
    Connection conn = null;
    try {
      conn = ds.getConnection();
      Statement st = conn.createStatement();
      ResultSet rs = st.executeQuery("select * from country order by printable_name asc");
      while (rs.next()) {
        countries.add(createCountry(rs));
      }
    } finally {
      if (conn != null) {
        conn.close();
      }
    }

    return countries;
  }

  public Country getCountry(String iso) throws Exception {
    Country c = null;
    Connection conn = null;
    try {
      conn = ds.getConnection();
      PreparedStatement st = conn.prepareStatement("select * from country where iso = ?");
      st.setString(1, iso);
      ResultSet rs = st.executeQuery();
      if (rs.next()) {
        c = createCountry(rs);
      }
    } finally {
      if (conn != null) {
        conn.close();
      }
    }
    return c;
  }
}
