package au.com.ourbodycorp.model.managers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.List;
import java.util.Properties;
import java.util.UUID;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sun.org.apache.regexp.internal.recompile;

import au.com.ourbodycorp.Config;
import au.com.ourbodycorp.MailMessage;
import au.com.ourbodycorp.Util;
import au.com.ourbodycorp.model.Patient;
import au.com.ourbodycorp.model.User;

public class PatientManager  {
	public Patient updatePatient(ResultSet rs) throws SQLException {
        Patient u = new Patient ();
        u.setId(rs.getLong("id"));
        u.setCorpId(rs.getLong("corp_id"));
        u.setReference(rs.getString("reference"));
        u.setFirstName(rs.getString("fname"));
        u.setLastName(rs.getString("lname"));
        u.setEmail(rs.getString("email"));
        u.setPhone(rs.getString("phone"));
        u.setCreated(rs.getDate("created"));
        u.setCreator(rs.getLong("creator"));
        u.setCliniko_id(rs.getLong("cliniko_id"));
        u.setNew_patient(rs.getString("new_patient"));
        u.setCheck_patient_exist(rs.getString("check_patient_exist"));
        u.setPassword(rs.getString("password"));
        return u;
    }
	/*public long getPatientId(String email) throws Exception {
		long id = 0;
		Connection conn = Util.getConnection();
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			if (email != null) {
				statement = conn.prepareStatement("select corp_id from patient where email = ? ");
				statement.setString(1, email);
				resultSet = statement.executeQuery();
				while (resultSet.next()) {
					id = resultSet.getInt(1);
					
				}
				statement = conn.prepareStatement("select id from programs where corp_id=?");
				statement.setLong(1, id);
				resultSet = statement.executeQuery();
				while (resultSet.next()) {
					id = resultSet.getLong(1);
					
				}
			}
			

		} finally {
			if (conn != null)
				conn.close();
			if (statement != null)
				statement.close();
			if (resultSet != null)
				resultSet.close();

		}

		return id;

	}
*/	public long getProgramId(String email) throws Exception {
		long id = 0;
		Connection conn = Util.getConnection();
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			if (email != null) {
				statement = conn.prepareStatement("select p.id from patient pa, programs p where pa.id=p.patient and  pa.email=?");
				statement.setString(1, email);
				resultSet = statement.executeQuery();
				while (resultSet.next()) {
					id = resultSet.getLong("id");
					
				}
			}
			

		} finally {
			if (conn != null)
				conn.close();
			if (statement != null)
				statement.close();
			if (resultSet != null)
				resultSet.close();

		}

		return id;

	}


	public boolean validate(String username, String password) throws Exception {
		Boolean status = false;

		Connection conn = Util.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			if (password != null && username != null) {
				pst = conn.prepareStatement("select * from patient where email = ? and password = ?");
				pst.setString(1, username);
				pst.setString(2, Util.encryptPassword(password));
				rs = pst.executeQuery();
				status = rs.next();
			}

		} finally {
			if (conn != null)
				conn.close();
			if (pst != null)
				pst.close();
			if (rs != null)
				rs.close();

		}

		return status;
	}
	 public Patient getUserByEmail(String email) throws Exception
	 {
		 Patient patient=null;
		 Connection conn = Util.getConnection();
			PreparedStatement pst = null;
			ResultSet rs = null;
			try {
				if (email!= null) {
					pst = conn.prepareStatement("select * from patient where lower(email) = ?");
					pst.setString(1, email.toLowerCase());
					rs = pst.executeQuery();
					if (rs.next()) {
						patient = updatePatient(rs);
		            }
				}
		 
	         }
			finally {
				if (conn != null)
					conn.close();
				if (pst != null)
					pst.close();
				if (rs != null)
					rs.close();

			}
			return patient;
   }
	 public String startPasswordReset(long patientid) throws Exception 
	 {
		 String key = UUID.randomUUID().toString();
		 Connection conn = Util.getConnection();
		 	PreparedStatement pst1 = null;
			PreparedStatement pst2 = null;
		
	        try {
	        	pst1 = conn.prepareStatement("DELETE FROM pwreset_patient WHERE patientid=?");
	        	pst1.setLong(1, patientid);
	            pst1.executeUpdate();
	            pst2 = conn.prepareStatement("insert into pwreset_patient (patientid, resetKey) values (?, ?)");
	            pst2.setLong(1, patientid);
	            pst2.setString(2, key);
	            pst2.executeUpdate();
	        }
	        finally {
	            if (conn != null) {
	                conn.close();
	            }
	        }
	        return key;
	    }
	 
	 public void sendPasswordResetEmail(Patient patient, String key) throws Exception {
	        Properties props = new Properties();
	        props.put("fname", patient.getFirstName());
	        props.put("lname", patient.getLastName());
	        props.put("email", patient.getEmail());
	        props.put("url", Config.getString("url") + "/corp/patients/patientreset.jsp?key="+key);
	        MailMessage msg = new MailMessage(props, "pwreset_patient.vm", patient.getEmail(), "Password reset request",false);
	        try {
	            msg.send();
	        }
	        catch (Exception e) {
	            e.printStackTrace();
	            throw e;
	        }
	    }
	    public Patient getPatientByKey(String key) throws Exception {
	    	Patient p = null;

	        Connection conn = Util.getConnection();

	        try {
	        	
				PreparedStatement ps = conn.prepareStatement("select patientid from pwreset_patient where resetKey = ? and expires > now()");
	            ps.setString(1, key);
	            ResultSet rs = ps.executeQuery();
	            if(rs.next()){
	            Long id=rs.getLong("patientid");
	            PreparedStatement ps1 = conn.prepareStatement("select * from patient where id = ?");
	            ps1.setLong(1, id);
	            ResultSet rs1 = ps1.executeQuery();
	            if(rs1.next()){
	            	p =updatePatient(rs1);
	            }
	            }
	           
	        }
	        finally {
	            if (conn != null) {
	                conn.close();
	            }
	        }
	        return p;
	    }
	    public Patient findUserByKey(String key) throws Exception {
	    	Patient patient = null;

	        Connection conn = Util.getConnection();

	        try {
	            PreparedStatement ps = conn.prepareStatement("select patientid from pwreset_patient where resetKey = ? and expires > now())");
	            ps.setString(1, key);
	            ResultSet rs = ps.executeQuery();
	            if (rs.next()) {
	            	patient =updatePatient(rs);
	            }
	        }
	        finally {
	            if (conn != null) {
	                conn.close();
	            }
	        }
	        return patient;
	    }
	 
	   public void deletePasswordReset(long patientid) throws Exception {
		   Connection conn = Util.getConnection();
		   PreparedStatement pst = null;
	        try {
	            pst = conn.prepareStatement("delete from pwreset_patient where patientid = ?");
	            pst.setLong(1, patientid);
	            pst.execute();
	        }
	        finally {
	            if (conn != null) {
	                conn.close();
	            }
	        }
	    }
	   public void savePatient(String password,String email) throws Exception {
		   Connection conn = Util.getConnection();
		   PreparedStatement pst = null;
		   try {
	            pst = conn.prepareStatement("update patient set password=? where email=?");
	            pst.setString(1, password);
	            pst.setString(2, email);
	            pst.execute();
	        }
	        finally {
	            if (conn != null) {
	                conn.close();
	            }
	        }
	   }
	   
	   public void logout(HttpSession session, HttpServletResponse response) throws Exception {
		    Patient u = (Patient) session.getAttribute("patient");
		    session.invalidate();
	        Cookie c = new Cookie("remember", "");
	        c.setMaxAge(0);
	        c.setPath("/");
	        c.setDomain(Config.getString("cookieDomain"));
	        response.addCookie(c);

	    }
	   public String success(long patientid,String key)throws Exception{
		   String status=null;
		   Connection conn = Util.getConnection();
		   PreparedStatement pst = null;
		   try {
	            pst = conn.prepareStatement("select * from  pwreset_patient where patientid=? and  resetkey=?");
	            pst.setLong(1, patientid);
	            pst.setString(2, key);
	            pst.execute();
	            status="Success";
	        }
	        finally {
	            if (conn != null) {
	                conn.close();
	            }
	        }
		   
		   return status;
	   }
	   
}
