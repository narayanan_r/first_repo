package au.com.ourbodycorp.model.managers;

import au.com.ourbodycorp.model.ScheduledMessage;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author rustyshelf
 */
public class ScheduledMessageManager extends ManagerBase {

    public List<ScheduledMessage> listMessages(long corpId, int limit, int offset) throws Exception {
        LinkedList<ScheduledMessage> messages = new LinkedList<ScheduledMessage>();
        Connection conn = ds.getConnection();
        ScheduledMessage message;
        try {
            String sql = "select * from scheduled_messages where corp_id = ?";

            sql += " order by show_after_days asc";
            if (limit > 0) {
                sql += " limit " + limit;
            }
            if (offset > 0) {
                sql += " offset " + offset;
            }
            PreparedStatement pst = conn.prepareStatement(sql);
            pst.setLong(1, corpId);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                message = new ScheduledMessage();
                messages.add(message);
                message.setId(rs.getLong("id"));
                message.setBody(rs.getString("body"));
                message.setCorpId(rs.getLong("corp_id"));
                message.setCreatedDate(rs.getTimestamp("created"));
                message.setHeadline(rs.getString("headline"));
                message.setLink(rs.getString("link"));
                message.setPhotoId(rs.getLong("photo"));
                message.setShowAfterDays(rs.getInt("show_after_days"));
            }
            rs.close();
            pst.close();
        }
        finally {
            conn.close();
        }

        return messages;
    }

    public ScheduledMessage save(ScheduledMessage message) throws Exception {
        String columns[] = {"id", "corp_id", "created", "show_after_days", "headline", "body", "link", "photo"};
        Connection conn = ds.getConnection();
        try {
            PreparedStatement pst;
            if (message.getId() == 0) {
                message.setId(getSequence("scheduledMessageId", conn));
                pst = conn.prepareStatement(createInsert("scheduled_messages", columns));
            }
            else {
                pst = conn.prepareStatement(createUpdate("scheduled_messages", columns, "id = ?"));
                pst.setLong(columns.length + 1, message.getId());
            }
            int i = 0;
            pst.setLong(++i, message.getId());
            pst.setLong(++i, message.getCorpId());
            pst.setTimestamp(++i, new Timestamp(message.getCreatedDate().getTime()));
            pst.setInt(++i, message.getShowAfterDays());
            pst.setString(++i, message.getHeadline());
            pst.setString(++i, message.getBody());
            pst.setString(++i, message.getLink());
            if (message.getPhotoId() > 0) {
                pst.setLong(++i, message.getPhotoId());
            }
            else {
                pst.setNull(++i, Types.BIGINT);
            }
            pst.executeUpdate();
            pst.close();
        }
        finally {
            conn.close();
        }

        return message;
    }

    public void deleteMessage(long id) throws Exception {
        Connection conn = ds.getConnection();
        try {
            Statement st = conn.createStatement();
            st.execute("delete from scheduled_messages where id = " + id);
            st.close();
        }
        finally {
            conn.close();
        }
    }

    public ScheduledMessage getMessage(long messageId) throws Exception {
        Connection conn = ds.getConnection();
        ScheduledMessage message = null;
        try {
            PreparedStatement pst = conn.prepareStatement("select * from scheduled_messages where id = ?");
            pst.setLong(1, messageId);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                message = new ScheduledMessage();
                message.setId(rs.getLong("id"));
                message.setBody(rs.getString("body"));
                message.setCorpId(rs.getLong("corp_id"));
                message.setCreatedDate(rs.getTimestamp("created"));
                message.setHeadline(rs.getString("headline"));
                message.setLink(rs.getString("link"));
                message.setPhotoId(rs.getLong("photo"));
                message.setShowAfterDays(rs.getInt("show_after_days"));
            }
            rs.close();
            pst.close();
        }
        finally {
            conn.close();
        }
        return message;
    }
}
