package au.com.ourbodycorp.model.managers;

import au.com.ourbodycorp.model.FlickrPhoto;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;

public class PhotoSetManager extends ManagerBase {
  public void save(String name, List<FlickrPhoto> photos) throws Exception {
    Connection conn = null;
    try {
      conn = ds.getConnection();
      conn.setAutoCommit(false);

      PreparedStatement pst = conn.prepareStatement("delete from photosets where name = ?");
      pst.setString(1, name);
      pst.executeUpdate();
      String[] columns = {"name", "seq", "id", "caption", "link", "thumbUrl", "fullUrl"};
      pst = conn.prepareStatement(createInsert("photosets", columns));

      int j = 1;
      for (FlickrPhoto photo : photos) {
        int i = 0;
        pst.setString(++i, name);
        pst.setLong(++i, j++);
        pst.setString(++i, photo.getId());
        pst.setString(++i, photo.getCaption());
        pst.setString(++i, photo.getLink());
        pst.setString(++i, photo.getThumbUrl());
        pst.setString(++i, photo.getFullUrl());
        pst.executeUpdate();
      }

      conn.commit();
    } finally {
      if (conn != null) {
        conn.setAutoCommit(true);
        conn.close();
      }
    }
  }

  public List<FlickrPhoto> get(String name) throws Exception {
    LinkedList<FlickrPhoto> photos = new LinkedList<FlickrPhoto>();
    Connection conn = null;
    try {
      conn = ds.getConnection();
      PreparedStatement pst = conn.prepareStatement("select * from photosets where name = ? order by seq");
      pst.setString(1, name);
      ResultSet rs = pst.executeQuery();
      while (rs.next()) {
        FlickrPhoto p = new FlickrPhoto();
        p.setFullUrl(rs.getString("fullUrl"));
        p.setId(rs.getString("id"));
        p.setCaption(rs.getString("caption"));
        p.setLink(rs.getString("link"));
        p.setThumbUrl(rs.getString("thumbUrl"));

        photos.add(p);
      }
    } finally {
      if (conn != null) {
        conn.close();
      }
    }
    return (photos.size() > 0) ? photos : null;
  }
}
