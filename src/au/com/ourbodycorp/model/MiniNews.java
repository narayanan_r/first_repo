package au.com.ourbodycorp.model;

import java.util.Date;

public class MiniNews {
  private long id;
  private long corpId;
  private Date created;
  private Date pushed;
  private String status;
  private String headline;
  private String body;
  private String link;
  private long photoId;
  private long shareid;
  private boolean sharelocation;
  private Attachment attachment;
  
  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public long getCorpId() {
    return corpId;
  }

  public void setCorpId(long corpId) {
    this.corpId = corpId;
  }

  public Date getCreated() {
    return created;
  }

  public void setCreated(Date created) {
    this.created = created;
  }

  public Date getPushed() {
    return pushed;
  }

  public void setPushed(Date pushed) {
    this.pushed = pushed;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getHeadline() {
    return headline;
  }

  public void setHeadline(String headline) {
    this.headline = headline;
  }

  public String getBody() {
    return body;
  }

  public void setBody(String body) {
    this.body = body;
  }

  public String getLink() {
    return link;
  }

  public void setLink(String link) {
    this.link = link;
  }

  public long getPhotoId() {
    return photoId;
  }

  public void setPhotoId(long photoId) {
    this.photoId = photoId;
  }

public boolean isSharelocation() {
	return sharelocation;
}

public void setSharelocation(boolean sharelocation) {
	this.sharelocation = sharelocation;
}

public Attachment getAttachment() {
	return attachment;
}

public void setAttachment(Attachment attachment) {
	this.attachment = attachment;
}

public long getShareid() {
	return shareid;
}

public void setShareid(long shareid) {
	this.shareid = shareid;
}
}

