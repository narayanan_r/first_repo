package au.com.ourbodycorp.model;

import java.util.LinkedList;

public class MyInfo {

    private long id;
    private long corpId;
    private String headline;
    private String body;
    private long photoId;
    private String link;
    private long shareid;
    private boolean sharelocation;
    private Attachment attachment;
    private LinkedList<Exercise.BodyPart> bodyParts;
    private LinkedList<Exercise.Type> types;

    public LinkedList<Exercise.BodyPart> getBodyParts() {
        return bodyParts;
    }

    public void setBodyParts(LinkedList<Exercise.BodyPart> bodyParts) {
        this.bodyParts = bodyParts;
    }

    public LinkedList<Exercise.Type> getTypes() {
        return types;
    }

    public void setTypes(LinkedList<Exercise.Type> types) {
        this.types = types;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getCorpId() {
        return corpId;
    }

    public void setCorpId(long corpId) {
        this.corpId = corpId;
    }

    public String getHeadline() {
        return headline;
    }

    public void setHeadline(String headline) {
        this.headline = headline;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public long getPhotoId() {
        return photoId;
    }

    public void setPhotoId(long photoId) {
        this.photoId = photoId;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public Attachment getAttachment() {
        return attachment;
    }

    public void setAttachment(Attachment attachment) {
        this.attachment = attachment;
    }

	public boolean isSharelocation() {
		return sharelocation;
	}

	public void setSharelocation(boolean sharelocation) {
		this.sharelocation = sharelocation;
	}

	public long getShareid() {
		return shareid;
	}

	public void setShareid(long shareid) {
		this.shareid = shareid;
	}
}
