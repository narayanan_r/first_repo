package au.com.ourbodycorp.model;

import java.util.Date;

public class CartDetails {
  private long id;
  private long plan;
  private String type;
  private long corp_id;
  private long company;
  private long userid;
  private Date created;
  
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getPlan() {
		return plan;
	}
	public void setPlan(long plan) {
		this.plan = plan;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public long getCorp_id() {
		return corp_id;
	}
	public void setCorp_id(long corp_id) {
		this.corp_id = corp_id;
	}
	public long getCompany() {
		return company;
	}
	public void setCompany(long company) {
		this.company = company;
	}
	public long getUserid() {
		return userid;
	}
	public void setUserid(long userid) {
		this.userid = userid;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
  
}

