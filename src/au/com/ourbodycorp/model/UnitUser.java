package au.com.ourbodycorp.model;

import java.util.Date;

public class UnitUser {

  public enum Type {
    ownerocc ("Owner/Occupier"),
    owner ("Owner"),
    tenant ("Tenant");

    private String description;

    Type(String description) {
      this.description = description;
    }

    public String getDescription() {
      return description;
    }
  }

  private Unit unit;
  private User user;
  private Type type;
  private Date since;
  private Date until;
  private boolean current;
  private boolean preferredAddress;

  public Unit getUnit() {
    return unit;
  }

  public void setUnit(Unit unit) {
    this.unit = unit;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public Type getType() {
    return type;
  }

  public void setType(Type type) {
    this.type = type;
  }

  public Date getSince() {
    return since;
  }

  public void setSince(Date since) {
    this.since = since;
  }

  public Date getUntil() {
    return until;
  }

  public void setUntil(Date until) {
    this.until = until;
  }

  public boolean isCurrent() {
    return current;
  }

  public void setCurrent(boolean current) {
    this.current = current;
  }

  public boolean isPreferredAddress() {
    return preferredAddress;
  }

  public void setPreferredAddress(boolean preferredAddress) {
    this.preferredAddress = preferredAddress;
  }
}
