/*
 * Developed and maintained by SAPOL IS&T Applications Branch.
 */
package au.com.ourbodycorp.model;

/**
 * Constants.
 *
 * @author PD98191
 * @version 1.0.0
 */
public class Constants {
    
    public static boolean TESTING_MODE = false;

    public static int SPECIAL_USER_ID_1 = 13; //Triston
//    public static int SPECIAL_USER_ID_2 = 292; //Shawn
    
  public enum State {
    ACT ("Australian Capital Territory"),
    NSW ("New South Wales"),
    NT  ("Northern Territory"),
    QLD ("Queensland"),
    SA  ("South Australia"),
    TAS ("Tasmania"),
    VIC ("Victoria"),
    WA  ("Western Australia");

    private String fullName;
    private State(String fullName) {
      this.fullName = fullName;
    }

    public String getFullName() {
      return fullName;
    }
  }
  
  public enum State_US {
    AL ("Alabama"),
    AK ("Alaska"),
    AZ ("Arizona"),
    AR ("Arkansas"),
    CA ("California"),
    CO ("Colorado"),
    CT ("Connecticut"),
    DE ("Delaware"),
    DC ("District of Columbia"),
    FL ("Florida"),
    GA ("Georgia"),
    HI ("Hawaii"),
    ID ("Idaho"),
    IL ("Illinois"),
    IN ("Indiana"),
    IA ("Iowa"),
    KS ("Kansas"),
    KY ("Kentucky"),
    LA ("Louisiana"),
    ME ("Maine"),
    MD ("Maryland"),
    MA ("Massachusetts"),
    MI ("Michigan"),
    MN ("Minnesota"),
    MS ("Mississippi"),
    MO ("Missouri"),
    MT ("Montana"),
    NE ("Nebraska"),
    NV ("Nevada"),
    NH ("New Hampshire"),
    NJ ("New Jersey"),
    NM ("New Mexico"),
    NY ("New York"),
    NC ("North Carolina"),
    ND ("North Dakota"),
    OH ("Ohio"),
    OK ("Oklahoma"),
    OR ("Oregon"),
    PA ("Pennsylvania"),
    RI ("Rhode Island"),
    SC ("South Carolina"),
    SD ("South Dakota"),
    TN ("Tennessee"),
    TX ("Texas"),
    UT ("Utah"),
    VT ("Vermont"),
    VA ("Virginia"),
    WA ("Washington"),
    WV ("West Virginia"),
    WI ("Wisconsin"),
    WY ("Wyoming");

    private String fullName;
    
    private State_US(String fullName) {
      this.fullName = fullName;
    }

    public String getFullName() {
      return fullName;
    }
  }

  public enum LoginReason {
    admin ("Please log in using an admin account"),
    login ("Please log in first");

    private String text;

    LoginReason(String text) {
      this.text = text;
    }

    public String getText() {
      return text;
    }
  }
}
