package au.com.ourbodycorp.model;

public class PatientGroup {
    
    private long id;
    private String groupName;
    private Long corporationId;
    
    public String getGroupName(){
        return groupName;
    }
    
    public void setGroupName(String groupName){
        this.groupName = groupName;
    }
    
    public Long getCorporationId(){
        return corporationId;
    }
    
    public void setCorporationId(Long corporationId){
        this.corporationId = corporationId;
    }

    public void setId(long id) {
        this.id = id;
    }
    
    public long getId(){
        return id;
    }
    
    public String getUserName() {
        return groupName.trim().toLowerCase().replaceAll("[^a-z0-9]+", "");
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 19 * hash + (int) (this.id ^ (this.id >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        
        if (getClass() != obj.getClass()) {
            return false;
        }
        
        final PatientGroup other = (PatientGroup) obj;
        
        if (this.id != other.id) {
            return false;
        }
        
        return true;
    }

}
