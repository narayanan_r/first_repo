package au.com.ourbodycorp.model;

import java.util.Date;

public class Spam {
  private long id;
  private String email;
  private String title;
  private String firstName;
  private String lastName;
  private String address1;
  private String address2;
  private String suburb;
  private String state;
  private String postcode;
  private String phone;
  private Date subscribed;
  private Date updated;
  private boolean optInEmail;
  private boolean optInPost;
  private String country;
  private long confirm;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getAddress1() {
    return address1;
  }

  public void setAddress1(String address1) {
    this.address1 = address1;
  }

  public String getAddress2() {
    return address2;
  }

  public void setAddress2(String address2) {
    this.address2 = address2;
  }

  public String getSuburb() {
    return suburb;
  }

  public void setSuburb(String suburb) {
    this.suburb = suburb;
  }

  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public String getPostcode() {
    return postcode;
  }

  public void setPostcode(String postcode) {
    this.postcode = postcode;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public Date getSubscribed() {
    return subscribed;
  }

  public void setSubscribed(Date subscribed) {
    this.subscribed = subscribed;
  }

  public Date getUpdated() {
    return updated;
  }

  public void setUpdated(Date updated) {
    this.updated = updated;
  }

  public boolean isOptInEmail() {
    return optInEmail;
  }

  public void setOptInEmail(boolean optInEmail) {
    this.optInEmail = optInEmail;
  }

  public boolean isOptInPost() {
    return optInPost;
  }

  public void setOptInPost(boolean optInPost) {
    this.optInPost = optInPost;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public long getConfirm() {
    return confirm;
  }

  public void setConfirm(long confirm) {
    this.confirm = confirm;
  }
}
