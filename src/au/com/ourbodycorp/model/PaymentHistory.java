package au.com.ourbodycorp.model;

import java.util.Date;

public class PaymentHistory {
  private long id;
  private String access_code;
  private String response;
  private java.math.BigDecimal total_amount;
  private boolean trans_status;
  private String invoice_number;
  private long trans_id;
  private long corp_id;
  private long company;
  private long userid;
  private Date created;
  
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	public boolean isTrans_status() {
		return trans_status;
	}
	public void setTrans_status(boolean trans_status) {
		this.trans_status = trans_status;
	}
	public long getCorp_id() {
		return corp_id;
	}
	public void setCorp_id(long corp_id) {
		this.corp_id = corp_id;
	}
	public long getCompany() {
		return company;
	}
	public void setCompany(long company) {
		this.company = company;
	}
	public long getUserid() {
		return userid;
	}
	public void setUserid(long userid) {
		this.userid = userid;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	public String getInvoice_number() {
		return invoice_number;
	}
	public void setInvoice_number(String invoice_number) {
		this.invoice_number = invoice_number;
	}
	public String getAccess_code() {
		return access_code;
	}
	public void setAccess_code(String access_code) {
		this.access_code = access_code;
	}
	public long getTrans_id() {
		return trans_id;
	}
	public void setTrans_id(long trans_id) {
		this.trans_id = trans_id;
	}
	public java.math.BigDecimal getTotal_amount() {
		return total_amount;
	}
	public void setTotal_amount(java.math.BigDecimal total_amount) {
		this.total_amount = total_amount;
	}
  
}

