package au.com.ourbodycorp.model;

import java.util.Date;

public class PlanPrice {
  private long id;
  private String plan_name;
  private long cents_per_sms;
  private long max_users;
  private long max_locations;
  private boolean practice_logo;
  private boolean push_notification;
  private String currency_type;
  private String invoice_number;
  private java.math.BigDecimal tax;
  private java.math.BigDecimal price;
  private String type;
  private long creator;
  private Date created;
  
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getPlan_name() {
		return plan_name;
	}
	public void setPlan_name(String plan_name) {
		this.plan_name = plan_name;
	}
	public long getCents_per_sms() {
		return cents_per_sms;
	}
	public void setCents_per_sms(long cents_per_sms) {
		this.cents_per_sms = cents_per_sms;
	}
	public long getMax_users() {
		return max_users;
	}
	public void setMax_users(long max_users) {
		this.max_users = max_users;
	}
	public long getMax_locations() {
		return max_locations;
	}
	public void setMax_locations(long max_locations) {
		this.max_locations = max_locations;
	}
	public boolean isPractice_logo() {
		return practice_logo;
	}
	public void setPractice_logo(boolean practice_logo) {
		this.practice_logo = practice_logo;
	}
	public boolean isPush_notification() {
		return push_notification;
	}
	public void setPush_notification(boolean push_notification) {
		this.push_notification = push_notification;
	}
	public String getCurrency_type() {
		return currency_type;
	}
	public void setCurrency_type(String currency_type) {
		this.currency_type = currency_type;
	}
	public java.math.BigDecimal getTax() {
		return tax;
	}
	public void setTax(java.math.BigDecimal tax) {
		this.tax = tax;
	}
	public java.math.BigDecimal getPrice() {
		return price;
	}
	public void setPrice(java.math.BigDecimal price) {
		this.price = price;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public long getCreator() {
		return creator;
	}
	public void setCreator(long creator) {
		this.creator = creator;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	public String getInvoice_number() {
		return invoice_number;
	}
	public void setInvoice_number(String invoice_number) {
		this.invoice_number = invoice_number;
	}
  
}

