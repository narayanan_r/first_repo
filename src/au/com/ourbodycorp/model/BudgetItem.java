package au.com.ourbodycorp.model;

import java.math.BigDecimal;

public class BudgetItem {
  private long id;
  private int year;
  private String type;
  private long groupId;
  private String groupName;
  private String title;
  private BigDecimal planned;
  private BigDecimal balance;

  public BudgetItem() {
  }

  public BudgetItem(long id, int year, String type, long groupId, String title, BigDecimal planned, BigDecimal balance) {
    this.id = id;
    this.year = year;
    this.type = type;
    this.groupId = groupId;
    this.title = title;
    this.planned = planned;
    this.balance = balance;
  }

  public BudgetItem(long id, int year, String type, long groupId, String title) {
    this.id = id;
    this.year = year;
    this.type = type;
    this.groupId = groupId;
    this.title = title;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public int getYear() {
    return year;
  }

  public void setYear(int year) {
    this.year = year;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public long getGroupId() {
    return groupId;
  }

  public void setGroupId(long groupId) {
    this.groupId = groupId;
  }

  public String getGroupName() {
    return groupName;
  }

  public void setGroupName(String groupName) {
    this.groupName = groupName;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public BigDecimal getPlanned() {
    return planned;
  }

  public void setPlanned(BigDecimal planned) {
    this.planned = planned;
  }

  public BigDecimal getBalance() {
    return balance;
  }

  public void setBalance(BigDecimal balance) {
    this.balance = balance;
  }
}
