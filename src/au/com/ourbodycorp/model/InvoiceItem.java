package au.com.ourbodycorp.model;

import java.math.BigDecimal;

/**
 * Created by IntelliJ IDEA.
 * User: bas
 * Date: 21/10/11
 * Time: 2:12 PM
 * To change this template use File | Settings | File Templates.
 */
public class InvoiceItem {
  private long id;
  private long invoiceId;
  private int sequence;
  private float quantity;
  private BigDecimal price;
  private String item;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public long getInvoiceId() {
    return invoiceId;
  }

  public void setInvoiceId(long invoiceId) {
    this.invoiceId = invoiceId;
  }

  public int getSequence() {
    return sequence;
  }

  public void setSequence(int sequence) {
    this.sequence = sequence;
  }

  public float getQuantity() {
    return quantity;
  }

  public void setQuantity(float quantity) {
    this.quantity = quantity;
  }

  public BigDecimal getPrice() {
    return price;
  }

  public void setPrice(BigDecimal price) {
    this.price = price;
  }

  public String getItem() {
    return item;
  }

  public void setItem(String item) {
    this.item = item;
  }

  public BigDecimal getLineTotal() {
    return price.multiply(new BigDecimal(quantity));
  }
}
