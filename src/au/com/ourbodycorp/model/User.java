package au.com.ourbodycorp.model;

import au.com.ourbodycorp.Util;
import au.com.ourbodycorp.model.managers.UserManager;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class User {

  public enum Status {applied, authorised, rejected, deleted, disabled}

  public enum Type {
    staff ("Staff"),
    member ("Site member");


    private String description;
    private Type(String description) {
      this.description = description;
    }

    public String getDescription() {
      return description;
    }
  }

  public static String[] titles = {"Mr.", "Mrs.", "Ms", "Miss", "Dr"};

  private long id = 0;
  private String name;
  private String rememberKey;
  private String verifyKey;
  private String title;
  private String firstName;
  private String lastName;
  private String email;
  private String password;
  private String address1;
  private String address2;
  private String suburb;
  private String state;
  private String postcode;
  private String country;
  private String countryName;
  private String phone;
  private String mobile;
  private Type type;
  private String about;
  private Date registered = new Date();
  private Date lastSeen = new Date();
  private Status status;
  private long attachmentId = 0;
  private boolean admin = false;
  private Attachment attachment;
  private List<String> roles;
  private boolean rolesChanged;
  private long lastCorp;
  private boolean disclaimed;
  private int applicationId;
  private String clinikoAPIKey;

  public static String[] getTitles() {
    return titles;
  }

  public static void setTitles(String[] titles) {
    User.titles = titles;
  }

  public boolean isDisclaimed() {
    return disclaimed;
  }

  public void setDisclaimed(boolean disclaimed) {
    this.disclaimed = disclaimed;
  }

  public long getLastCorp() {
    return lastCorp;
  }

  public void setLastCorp(long lastCorp) {
    this.lastCorp = lastCorp;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    if (email != null) {
      this.email = email.toLowerCase();
    } else {
      this.email = email;
    }
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
      this.password = password;
  }

  public boolean comparePassword(String password) throws Exception {
    return this.password.equals(Util.encryptPassword(password));
  }

  public String getAddress1() {
    return address1;
  }

  public void setAddress1(String address1) {
    this.address1 = address1;
  }

  public String getAddress2() {
    return address2;
  }

  public void setAddress2(String address2) {
    this.address2 = address2;
  }

  public String getSuburb() {
    return suburb;
  }

  public void setSuburb(String suburb) {
    this.suburb = suburb;
  }

  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public String getPostcode() {
    return postcode;
  }

  public void setPostcode(String postcode) {
    this.postcode = postcode;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public String getCountryName() {
    return countryName;
  }

  public void setCountryName(String countryName) {
    this.countryName = countryName;
  }

  public String getMobile() {
    return mobile;
  }

  public void setMobile(String mobile) {
    this.mobile = mobile;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public Type getType() {
    return type;
  }

  public void setType(Type type) {
    this.type = type;
  }

  public String getAbout() {
    return about;
  }

  public void setAbout(String about) {
    this.about = about;
  }

  public Date getRegistered() {
    return registered;
  }

  public void setRegistered(Date registered) {
    this.registered = registered;
  }

  public Status getStatus() {
    return status;
  }

  public void setStatus(Status status) {
    this.status = status;
  }

  public long getAttachmentId() {
    return attachmentId;
  }

  public void setAttachmentId(long attachmentId) {
    this.attachmentId = attachmentId;
  }

  public boolean isAdmin() {
    return admin;
  }

  public void setAdmin(boolean admin) {
    this.admin = admin;
  }

  public String getRememberKey() {
    return rememberKey;
  }

  public void setRememberKey(String rememberKey) {
    this.rememberKey = rememberKey;
  }

  public String getVerifyKey() {
    return verifyKey;
  }

  public void setVerifyKey(String verifyKey) {
    this.verifyKey = verifyKey;
  }

  public Date getLastSeen() {
    return lastSeen;
  }

  public void setLastSeen(Date lastSeen) {
    this.lastSeen = lastSeen;
  }

  public Attachment getAttachment() {
    return attachment;
  }

  public void setAttachment(Attachment attachment) {
    this.attachment = attachment;
  }

  public String getFullName() {
    return firstName + " " + lastName;
  }


  public void addRole(String role) throws Exception {
    if (roles == null && id > 0) {
      getRoles();
    }
    if (roles != null && !hasRole(role)) {
      roles.add(role);
      rolesChanged = true;
    }
  }

  public void removeRole(String role) throws Exception {
    if (roles == null && id > 0) {
      getRoles();
    }
    if (roles != null) {
      roles.remove(role);
      roles.add(role);
      rolesChanged = true;
    }
  }

  @SuppressWarnings("unchecked")
  public List<String> getRoles() throws Exception {
    if (roles == null && id > 0) {
      UserManager um = new UserManager();
      roles = (LinkedList) um.getRolesForUser(this.id);
    }
    return roles;
  }

  public boolean hasRole(String role) throws Exception {
    if (roles == null && id > 0) {
      getRoles();
    }
    return roles != null && roles.contains(role);
  }

  public boolean isRolesChanged() {
    return rolesChanged;
  }

  public void setRoles(List<String> roles) {
    rolesChanged = true;
    this.roles = roles;
  }

  public int getApplicationId() {
	return applicationId;
  }
	
  public void setApplicationId(int applicationId) {
    this.applicationId = applicationId;
  }

public String getClinikoAPIKey() {
	return clinikoAPIKey;
}

public void setClinikoAPIKey(String clinikoAPIKey) {
	this.clinikoAPIKey = clinikoAPIKey;
}
}
