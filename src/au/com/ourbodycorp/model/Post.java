package au.com.ourbodycorp.model;

import au.com.ourbodycorp.model.managers.PostManager;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;


public class Post implements Comparable<Post> {
  public enum PostType {
    page ("Page", "Pages"),
    blog ("Blog", "Blogs"),
    gallery ("Gallery", "Galleries");

    private String title;
    private String plural;

    PostType(String title, String plural) {
      this.title = title;
      this.plural = plural;
    }

    public String getTitle() {
      return title;
    }

    public String getPlural() {
      return plural;
    }
  }


  public enum Status {draft, published, deleted, obsolete}
  public enum Format {text, html}

  private long versionId;
  private long id;
  private long parent;
  private long seqNo;
  private long blogId;
  private Status status;
  private long userId;
  private String author;
  private PostType type;
  private Date posted;
  private Date updated;
  private Date start;
  private Date end;
  private String shortCode;
  private String title;
  private String excerpt;
  private String body;
  private Format format;
  private boolean external;
  private boolean gallery;
  private boolean allowComments;
  private long bannerId;
  private long iconId;
  private Attachment icon;
  private Attachment banner;
  private int commentCount;
  private long draftVersion;
  private int depth;
  private long blogLink;
  private String bannerBackground;


  private boolean urlChanged;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public long getParent() {
    return parent;
  }

  public void setParent(long parent) {
    this.parent = parent;
  }

  public Status getStatus() {
    return status;
  }

  public void setStatus(Status status) {
    this.status = status;
  }

  public long getUserId() {
    return userId;
  }

  public void setUserId(long userId) {
    this.userId = userId;
  }

  public String getAuthor() {
    return author;
  }

  public void setAuthor(String author) {
    this.author = author;
  }

  public PostType getType() {
    return type;
  }

  public void setType(PostType type) {
    this.type = type;
  }

  public Date getPosted() {
    return posted;
  }

  public void setPosted(Date posted) {
    this.posted = posted;
  }

  public Date getUpdated() {
    return updated;
  }

  public void setUpdated(Date updated) {
    this.updated = updated;
  }

  public Date getStart() {
    return start;
  }

  public void setStart(Date start) {
    this.start = start;
  }

  public Date getEnd() {
    return end;
  }

  public void setEnd(Date end) {
    this.end = end;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getShortCode() {
    return shortCode;
  }

  public void setShortCode(String shortCode) {
    String old = this.shortCode;
    this.shortCode = (shortCode != null && shortCode.length() >0) ? shortCode : null;
    urlChanged = ((old == null && this.shortCode != null) || (old != null && this.shortCode == null) || (old != null && !old.equals(this.shortCode)));
  }

  public String getExcerpt() {
    return excerpt;
  }

  public void setExcerpt(String excerpt) {
    this.excerpt = (excerpt != null && excerpt.length() > 0) ? excerpt : null;
  }

  public String getBody() {
    if (body != null) {
      body = body.replaceAll("lightbox\\[0\\]", "lightbox");
    }
    return body;
  }

  public void setBody(String body) {
    this.body = body;
  }

  public Format getFormat() {
    return format;
  }

  public void setFormat(Format format) {
    this.format = format;
  }

  public boolean isExternal() {
    return external;
  }

  public void setExternal(boolean external) {
    this.external = external;
  }

  public boolean isGallery() {
    return gallery;
  }

  public void setGallery(boolean gallery) {
    this.gallery = gallery;
  }

  public boolean isAllowComments() {
    return allowComments;
  }

  public void setAllowComments(boolean allowComments) {
    this.allowComments = allowComments;
  }

  public Attachment getIcon() {
    return icon;
  }

  public void setIcon(Attachment icon) {
    this.icon = icon;
  }

  public Attachment getBanner() {
    return banner;
  }

  public void setBanner(Attachment banner) {
    this.banner = banner;
  }

  public int getCommentCount() {
    return commentCount;
  }

  public void setCommentCount(int commentCount) {
    this.commentCount = commentCount;
  }

  public long getVersionId() {
    return versionId;
  }

  public void setVersionId(long versionId) {
    this.versionId = versionId;
  }

  public long getSeqNo() {
    return seqNo;
  }

  public void setSeqNo(long seqNo) {
    this.seqNo = seqNo;
  }

  public long getBlogId() {
    return blogId;
  }

  public void setBlogId(long blogId) {
    this.blogId = blogId;
  }

  public long getBannerId() {
    return bannerId;
  }

  public void setBannerId(long bannerId) {
    this.bannerId = bannerId;
  }

  public long getIconId() {
    return iconId;
  }

  public void setIconId(long iconId) {
    this.iconId = iconId;
  }

  public long getBlogLink() {
    return blogLink;
  }

  public void setBlogLink(long blogLink) {
    this.blogLink = blogLink;
  }

  public Post[] getHierarchy() throws Exception {
    if (this.getShortCode() == null || this.getShortCode().trim().length() == 0) {
      return null;
    }

    PostManager pm = new PostManager();
    LinkedList<Post> elements = new LinkedList<Post>();
    Post parent = this;
    while (true) {
      elements.add(parent);
      if (parent.getParent() > 0) {
        long pId = parent.getParent();
        parent = pm.getLivePost(pId);
        if (parent == null) {
          parent = pm.getDraftPost(pId);
        }
        if (parent == null) {
          break;
        }
      } else {
        break;
      }
    }
    Post[] ea = new Post[elements.size()];
    for (int i = 0; i < ea.length; i++) {
      ea[i] = elements.get(ea.length -1 - i);
    }
    return ea;
  }

  public String[] getPathElements() throws Exception {

    if (this.getShortCode() == null || this.getShortCode().trim().length() == 0) {
      return null;
    }

    PostManager pm = new PostManager();
    LinkedList<String> elements = new LinkedList<String>();
    Post parent = this;
    while (true) {
      elements.add(parent.getShortCode());
      if (parent.getParent() > 0) {
        long pId = parent.getParent();
        parent = pm.getLivePost(pId);
        if (parent == null) {
          parent = pm.getDraftPost(pId);
        }
        if (parent == null) {
          break;
        }
      } else {
        break;
      }
    }
    String[] ea = new String[elements.size()];
    for (int i = 0; i < ea.length; i++) {
      ea[i] = elements.get(ea.length -1 - i);
    }
    return ea;
  }

  public String getUrl() throws Exception {
    String[] pathElements = getPathElements();
    String url = null;
    if (pathElements != null) {
      StringBuilder sb = new StringBuilder("/");
      for (String pe : pathElements) {
        sb.append(pe).append("/");
      }
      url = sb.toString();
    }
    return url;
  }

  public String getPreviewUrl() {
    if (blogId > 0) {
      return "/admin/posts/preview?blogId=" + versionId;
    } else {
      return "/admin/posts/preview?versionId=" + versionId;
    }
  }

  public String getUrl(boolean skipLast) throws Exception {
    if (!skipLast) {
      return getUrl();
    }
    String[] pathElements = getPathElements();
    if (pathElements ==null || pathElements.length <= 1) {
      return null;
    }

    pathElements = Arrays.copyOfRange(pathElements, 0, pathElements.length - 1);

    String url = null;
    if (pathElements != null) {
      StringBuilder sb = new StringBuilder("/");
      for (String pe : pathElements) {
        sb.append(pe).append("/");
      }
      url = sb.toString();
    }
    return url;
  }

  public String getBlogUrl(Post anchorPoint) throws Exception {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy'/'MM");
    return String.format("%s%s/%s.html", anchorPoint.getUrl(), sdf.format(posted), shortCode);
  }

  public boolean isUrlChanged() {
    return urlChanged;
  }

  public long getDraftVersion() {
    return draftVersion;
  }

  public void setDraftVersion(long draftVersion) {
    this.draftVersion = draftVersion;
  }

  public int compareTo(Post post) {
    if (type == PostType.page) {
      if (post.seqNo == 0 || seqNo == 0) {
        return title.toLowerCase().compareTo(post.title.toLowerCase());
      } else {
        if (post.seqNo > seqNo) {
          return -1;
        } else if (post.seqNo < seqNo) {
          return 1;
        } else {
          return 0;
        }
      }
    } else {
      if (posted.before(post.getPosted())) {
        return 1;
      } else if (posted.after(post.getPosted())) {
        return -1;
      } else {
        return 0;
      }
    }
  }

  public int getDepth() {
    return depth;
  }

  public void setDepth(int depth) {
    this.depth = depth;
  }

  public String getBannerBackground() {
    return bannerBackground;
  }

  public void setBannerBackground(String bannerBackground) {
    this.bannerBackground = bannerBackground;
  }
}
