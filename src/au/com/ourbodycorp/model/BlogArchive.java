package au.com.ourbodycorp.model;

import java.text.SimpleDateFormat;
import java.util.Date;

public class BlogArchive {
  private Date month;
  private int count;

  public BlogArchive(Date month, int count) {
    this.month = month;
    this.count = count;
  }

  public Date getMonth() {
    return month;
  }

  public int getCount() {
    return count;
  }

  public String getUrl(Post anchorPoint) throws Exception {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy'/'MM");
    return String.format("%s%s/", anchorPoint.getUrl(), sdf.format(month));
  }
}
