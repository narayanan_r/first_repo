package au.com.ourbodycorp.model;

public class BudgetGroup {
  private long id;
  private long corpId;
  private int seq;
  private String title;

  public BudgetGroup() {
  }

  public BudgetGroup(long id, long corpId, int seq, String title) {
    this.id = id;
    this.corpId = corpId;
    this.seq = seq;
    this.title = title;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public long getCorpId() {
    return corpId;
  }

  public void setCorpId(long corpId) {
    this.corpId = corpId;
  }

  public int getSeq() {
    return seq;
  }

  public void setSeq(int seq) {
    this.seq = seq;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }
}
