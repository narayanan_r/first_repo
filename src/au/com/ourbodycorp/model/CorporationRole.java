package au.com.ourbodycorp.model;

public class CorporationRole {

  public enum Role {
    Admin ("AD"),
    TeamMember ("TM"),
    Treasurer ("TR"),
    President ("PR"),
    Secretary ("ST"),
    Owner ("OW"),
    Tenant ("TN"),
    Manager ("MG"),
    Auditor ("AU")
    ;
    private String code;

    Role(String code) {
      this.code = code;
    }

    public String getCode() {
      return code;
    }
  }

  private String id;
  private String name;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public boolean equals(Object o) {
    if (o instanceof CorporationRole && o != null && ((CorporationRole) o).getId() == this.getId()) {
      return true;
    } else if (o instanceof String) {
      return ((String) o).equalsIgnoreCase(this.getId());
    } else {
      return false;
    }
  }
}
