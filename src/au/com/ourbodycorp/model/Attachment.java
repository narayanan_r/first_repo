package au.com.ourbodycorp.model;

import au.com.ourbodycorp.Dimension;
import au.com.ourbodycorp.model.managers.AttachmentManager;

import java.util.Date;

public class Attachment {

  public enum ImageSize {

    thumb (134, 134, true, "134 x 134 cropped"),
    f100 (100, 100, false, "Fits in 100 x 100"),
    s100 (100, 100, true, "100 x 100 cropped"),
    f146 (146, 146, false, "Fits in 146 x 146"),
    f150 (150, 150, false, "Fits in 150 x 150"),
    f300 (300, 300, false, "Fits in 300 x 300"),
    f320 (320, 320, false, "Fits in 320 x 320"),
    logo (640, 332, false, "Fits in 640 x 332"),
    weblogo (250, 130, false, "Fits in 250 x 130"),
    f640 (640, 640, false, "Fits in 640 x 640"),
    f800 (800, 800, false, "Fits in 800 x 800"),
    org (0, 0, false, "Original size"),
    banner (990, 300, true, "Banner cropped to 990 x 300", ImageSize.LANDSCAPE),
    icon (232, 120, true, "Icon cropped to 232 x 120", ImageSize.LANDSCAPE),
    bannerPreview (198, 60, true, "Banner PREVIEW cropped to 198 x 60", ImageSize.LANDSCAPE),
    iconPreview (116, 60, true, "Icon PREVIEW cropped to 116 x 60", ImageSize.LANDSCAPE);

    public static final int ORIENTATION_ANY = 0;
    public static final int PORTRAIT = 1;
    public static final int LANDSCAPE = 2;


    private int width;
    private int height;
    private String description;
    private boolean cropped;
    private int orientation = ORIENTATION_ANY;

    ImageSize(int width, int height, boolean cropped, String description) {
      this.width = width;
      this.height = height;
      this.cropped = cropped;
      this.description = description;
    }

    ImageSize(int width, int height, boolean cropped, String description, int orientation) {
      this.width = width;
      this.height = height;
      this.cropped = cropped;
      this.description = description;
      this.orientation = orientation;
    }

    public static ImageSize[] getAllowedSizes() {
      return new ImageSize[] {f100, s100, f320, f640, f800, icon, org};
    }

    public int getWidth() {
      return width;
    }

    public void setWidth(int width) {
      this.width = width;
    }

    public int getHeight() {
      return height;
    }

    public void setHeight(int height) {
      this.height = height;
    }

    public boolean isCropped() {
      return cropped;
    }

    public void setCropped(boolean cropped) {
      this.cropped = cropped;
    }

    public String getDescription() {
      return description;
    }

    public void setDescription(String description) {
      this.description = description;
    }

    public int getOrientation() {
      return orientation;
    }

    public void setOrientation(int orientation) {
      this.orientation = orientation;
    }
  }


  public enum AttachmentType {
    image (AttachmentManager.FOLDER_IMAGE),
    file (AttachmentManager.FOLDER_FILE),
    media (AttachmentManager.FOLDER_MEDIA);

    private String folderName;

    AttachmentType(String folderName) {
      this.folderName = folderName;
    }

    public String getFolderName() {
      return folderName;
    }
  }

  private long id;
  private long folderId;
  private long version;
  private String filename;
  private String mimeType;
  private String caption;
  private Date uploaded = new Date();
  private Date updated = new Date();
  private long userId;
  private byte[] data;
  private int width;
  private int height;
  private long size;
  private AttachmentType type;
  private boolean display;
  private String link;
  private boolean active;

  public boolean isDisplay() {
    return display;
  }

  public void setDisplay(boolean display) {
    this.display = display;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public long getVersion() {
    return version;
  }

  public void setVersion(long version) {
    this.version = version;
  }

  public String getFilename() {
    if (filename.contains("/")) {
      String[] parts = filename.split("/");
      return parts[parts.length - 1];
    } else {
      return filename;
    }
  }

  public String getFullFileName() {
    return filename;
  }

  public void setFilename(String filename) {
    this.filename = (filename != null && filename.trim().length() > 0) ? filename : null;
  }

  public String getMimeType() {
    return mimeType;
  }

  public void setMimeType(String mimeType) {
    this.mimeType = mimeType;
  }

  public String getCaption() {
    return caption;
  }

  public void setCaption(String caption) {
    this.caption = caption;
  }

  public Date getUploaded() {
    return uploaded;
  }

  public void setUploaded(Date uploaded) {
    this.uploaded = uploaded;
  }

  public Date getUpdated() {
    return updated;
  }

  public void setUpdated(Date updated) {
    this.updated = updated;
  }

  public long getUserId() {
    return userId;
  }

  public void setUserId(long userId) {
    this.userId = userId;
  }

  public byte[] getData() {
    return data;
  }

  public void setData(byte[] data) {
    this.data = data;
  }

  public int getWidth() {
    return width;
  }

  public void setWidth(int width) {
    this.width = width;
  }

  public int getHeight() {
    return height;
  }

  public void setHeight(int height) {
    this.height = height;
  }


  public long getSize() {
    return size;
  }

  public void setSize(long size) {
    this.size = size;
  }

  public String getUrl() {
    return "/attachment/" + id + "/" + getFilename().replaceAll("[^A-Za-z0-9\\._\\-]+", "");
  }

  public String getImageUrl(ImageSize size) {
    return "/image/" + id + "." + version + "/" + size + "/" + getFilename().replaceAll("[^A-Za-z0-9\\._\\-]+", "");
  }

  public AttachmentType getType() {
    return type;
  }

  public void setType(AttachmentType type) {
    this.type = type;
  }

  public String getLink() {
    return link;
  }

  public void setLink(String link) {
    this.link = link;
  }

  public long getFolderId() {
    return folderId;
  }

  public boolean isActive() {
    return active;
  }

  public void setActive(boolean active) {
    this.active = active;
  }

  public void setFolderId(long folderId) {
    this.folderId = folderId;
  }

  public String getExtension() {
    String ext = null;
    if (filename != null) {
      int idx = filename.lastIndexOf(".");
      if (idx > -1 && idx < filename.length() - 1) {
        ext = filename.substring(idx + 1);
      }
    }
    return ext;
  }

  public Dimension estimateSize(ImageSize size) {
    if (type != Attachment.AttachmentType.image || width == 0 || height == 0) {
      return null;
    } else {
      Dimension d = new Dimension(width, height);
      if (!size.isCropped() && width <= size.width && height <= size.height) {
        // We don't up size
        d.setWidth(width);
        d.setHeight(height);
      } else if (size != ImageSize.org) {
        d.setWidth(size.getWidth());
        d.setHeight(size.getWidth());
        if (!size.isCropped() && width != height) {
          if (width > height) {
            float y = ((float) size.getWidth() / (float) width) * (float) height;
            d.setHeight(Math.round(y));
          } else {
            float x = ((float) size.getWidth() / (float) height) * (float) width;
            d.setWidth(Math.round(x));
          }
        }
      }
      return d;
    }
  }
}
