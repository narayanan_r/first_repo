package au.com.ourbodycorp.model;

import au.com.ourbodycorp.Config;
import au.com.ourbodycorp.model.managers.CorporationManager;

import java.util.Date;
import java.util.Map;
import java.util.TimeZone;

public class Corporation {

    public enum Type {

        strata("Strata Plan"),
        companyTitle("Company Title"),
        community("Community Association"),
        other("Other");
        private String description;

        Type(String description) {
            this.description = description;
        }

        public String getDescription() {
            return description;
        }
    }
    private long id;
    private Type type;
    private String regNum;
    private String name;
    private String slug;
    private String address1;
    private String address2;
    private String state;
    private String postcode;
    private String suburb;
    private String country;
    private String countryName;
    private Date created;
    private int financialYear;
    private int startOfFinancialYear;
    private TimeZone timeZone;
    private String locked;
    private long logoId;
    private long companyId;
    private int maxPatients;
    private int maxPractitioners;
    private String subType;
    private Date expiry;
    private int smsCount;
    private String cliniko_apikey;
    private long session_timeout;
    private String subLocCntWithId;
    private boolean mainLocation;

    public boolean isMainLocation() {
		return mainLocation;
	}

	public void setMainLocation(boolean mainLocation) {
		this.mainLocation = mainLocation;
	}

	public String getSubLocCntWithId() {
		return subLocCntWithId;
	}

	public void setSubLocCntWithId(String subLocCntWithId) {
		this.subLocCntWithId = subLocCntWithId;
	}

	public boolean isTrial() {
        return (subType != null && subType.equals("trial"));
    }

    public boolean isExpiredTrial() {
        return (isTrial() && expiry != null && expiry.before(new Date()));
    }

    public Date getExpiry() {
        return expiry;
    }

    public void setExpiry(Date expiry) {
        this.expiry = expiry;
    }

    public String getSubType() {
        return subType;
    }

    public void setSubType(String subType) {
        if (subType != null) {
            if ("trial".equalsIgnoreCase(subType)) {
                setMaxPatients(0);
                setMaxPractitioners(0);
            }
            else if ("solo".equalsIgnoreCase(subType)) {
                setMaxPatients(150);
                setMaxPractitioners(1);
            }
            else if ("small".equalsIgnoreCase(subType)) {
                setMaxPatients(500);
                setMaxPractitioners(5);
            }
            else if ("large".equalsIgnoreCase(subType)) {
                setMaxPatients(0);
                setMaxPractitioners(10);
            }
            else if ("unlimited".equalsIgnoreCase(subType)) {
                setMaxPatients(0);
                setMaxPractitioners(0);
            }
        }
        this.subType = subType;
    }

    public int getMaxPatients() {
        return maxPatients;
    }

    public void setMaxPatients(int maxPatients) {
        this.maxPatients = maxPatients;
    }

    public long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(long companyId) {
        this.companyId = companyId;
    }

    public long getLogoId() {
        return logoId;
    }

    public void setLogoId(long logoId) {
        this.logoId = logoId;
    }
    private Map<String, CorpMeta> meta;

    public String getLocked() {
        return locked;
    }

    public void setLocked(String locked) {
        this.locked = locked;
    }

    public TimeZone getTimeZone() {
        if (timeZone == null) {
            return TimeZone.getDefault();
        }
        return timeZone;
    }

    public void setTimeZone(TimeZone timeZone) {
        this.timeZone = timeZone;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getRegNum() {
        return regNum;
    }

    public void setRegNum(String regNum) {
        this.regNum = regNum;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getSuburb() {
        return suburb;
    }

    public void setSuburb(String suburb) {
        this.suburb = suburb;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public int getFinancialYear() {
        return financialYear;
    }

    public void setFinancialYear(int financialYear) {
        this.financialYear = financialYear;
    }

    public int getStartOfFinancialYear() {
        return startOfFinancialYear;
    }

    public void setStartOfFinancialYear(int startOfFinancialYear) {
        this.startOfFinancialYear = startOfFinancialYear;
    }

    public int getMaxPractitioners() {
        return maxPractitioners;
    }

    public void setMaxPractitioners(int maxPractitioners) {
        this.maxPractitioners = maxPractitioners;
    }

    public int getSmsCount() {
        return smsCount;
    }

    public void setSmsCount(int smsCount) {
        this.smsCount = smsCount;
    }

    @Override
    public boolean equals(Object o) {
        return (o instanceof Corporation && o != null && ((Corporation) o).getId() == this.getId());
    }

    public String getHost() throws Exception {
        String url = this.getSlug() + "." + Config.getString("baseDomain");
//        String url = Config.getString("baseDomain");
        if (Config.getBoolean("https", false)) {
            return "https://" + url;
        }
        else {
            return "http://" + url;
        }

    }

    public String getUrl() throws Exception {
        String url = this.getSlug() + "." + Config.getString("baseDomain") + "/corp/";
        //url = "localhost:8080/corp/index.jsp";
        if (Config.getBoolean("https", false)) {
            return "https://" + url;
        }
        else {
            return "http://" + url;
        }

    }

    public Map<String, CorpMeta> getMeta() throws Exception {
        if (meta == null) {
            CorporationManager cm = new CorporationManager();
            meta = cm.getCorpMeta(id);
        }
        return meta;
    }

    public String getMetaString(String key) throws Exception {
        return getMetaString(key, null);
    }

    public String getMetaString(String key, String defaultValue) throws Exception {
        if (meta == null) {
            getMeta();
        }
        CorpMeta value = meta.get(key);
        if (value != null) {
            return value.getStringValue();
        }
        else {
            return defaultValue;
        }
    }

    public boolean isLocked() {
        return (locked != null && locked.trim().length() > 0);
    }
    
    public boolean isSmsEnabledCountry(){
        if (country == null) return false;
        
        if (country.equalsIgnoreCase("CA") || country.equalsIgnoreCase("US")) return false;
        
        return true;
    }
    
    public boolean isPushAllowed(){
        if (subType == null) return false;
        
        return true;
    }

	public String getCliniko_apikey() {
		return cliniko_apikey;
	}

	public void setCliniko_apikey(String cliniko_apikey) {
		this.cliniko_apikey = cliniko_apikey;
	}

	public long getSession_timeout() {
		return session_timeout;
	}

	public void setSession_timeout(long session_timeout) {
		this.session_timeout = session_timeout;
	}

}
