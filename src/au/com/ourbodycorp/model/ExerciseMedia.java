package au.com.ourbodycorp.model;

public class ExerciseMedia {
  private long id;
  private long exercise;
  private int seq;
  private String type;
  private byte[] data;
  private String url;
  private String caption;
  private String name;
  private boolean key;
  private int version;

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public long getExercise() {
    return exercise;
  }

  public void setExercise(long exercise) {
    this.exercise = exercise;
  }

  public int getSeq() {
    return seq;
  }

  public void setSeq(int seq) {
    this.seq = seq;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public byte[] getData() {
    return data;
  }

  public void setData(byte[] data) {
    this.data = data;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public String getCaption() {
    return caption;
  }

  public void setCaption(String caption) {
    this.caption = caption;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public boolean isKey() {
    return key;
  }

  public void setKey(boolean key) {
    this.key = key;
  }

  public String getCacheName(int size) {
    return (id / 1000) + "/" + id + "." + version + "@" + size + ".jpg";
  }

}
