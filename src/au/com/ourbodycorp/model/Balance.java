package au.com.ourbodycorp.model;

import java.math.BigDecimal;

public class Balance {
  private long accountId;
  private int year;
  private BigDecimal opening;
  private BigDecimal closing;
  private Budget budget;

  public long getAccountId() {
    return accountId;
  }

  public void setAccountId(long accountId) {
    this.accountId = accountId;
  }

  public int getYear() {
    return year;
  }

  public void setYear(int year) {
    this.year = year;
  }

  public BigDecimal getOpening() {
    return opening;
  }

  public void setOpening(BigDecimal opening) {
    this.opening = opening;
  }

  public BigDecimal getClosing() {
    return closing;
  }

  public void setClosing(BigDecimal closing) {
    this.closing = closing;
  }

  public Budget getBudget() {
    return budget;
  }

  public void setBudget(Budget budget) {
    this.budget = budget;
  }
}
