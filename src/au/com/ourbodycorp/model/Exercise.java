package au.com.ourbodycorp.model;

import java.util.Date;
import java.util.LinkedList;

public class Exercise {

  public static final String TIMED_EXERCISE = "timed";
  public static final String REP_EXERCISE = "rep";

  public enum Type {
    strength("Strength"),
    selfRelease("Release"),
    stretches("Stretch"),
    neutralMob("Neutral Mob."),
    selfMob("Self Mob."),
    postOp("Post Op."),
    posture("Posture"),
    proprioception("Proprioception"),
    cardio("Cardio"),
    agility("Agility"),
    others("Others")
    ;
    private String value;

    Type(String name) {
      this.value = name;
    }

    public String getValue() {
      return value;
    }
  }

  public enum BodyPart {
    core("Core"),
    headNeck("Head and Neck"),
    shoulder("Shoulder"),
    upperBody("Upper Body"),
    lowerBody("Lower Body"),
    upperBack("Upper Back"),
    lowerBack("Lower Back"),
    upperLimb("Upper Limb"),
    lowerLimb("Lower Limb"),
    elbow("Elbow"),
    wristHand("Wrist and Hand"),
    hipPelvis("Hip and Pelvis"),
    knee("Knee"),
    footAnkle("Foot and Ankle")
    ;
    private String value;

    BodyPart(String name) {
      this.value = name;
    }

    public String getValue() {
      return value;
    }
  }

  private long id;
  private long category;
  private long corpId;
  private String name;
  private String[] equipment;
  private int repTime;
  private int reps;
  private int restTime;
  private int setRestTime;
  private String description;
  private Date created;
  private long creator;
  private Long keyPhotoId;
  private int keyPhotoVersion;
  private LinkedList<BodyPart> bodyParts;
  private LinkedList<Type> types;
  private String type;
  private boolean showSides;
  private boolean shareLocation;
  private boolean hideExercise;
  private long shareid;
  private int sets;
  private int holds;
  private int rest;
  private String side;
  private int repsperseconds;
  private int weight;
  
  
  
public int getWeight() {
	return weight;
}

public void setWeight(int weight) {
	this.weight = weight;
}

public int getSets() {
	return sets;
}

public void setSets(int sets) {
	this.sets = sets;
}

public int getHolds() {
	return holds;
}

public void setHolds(int holds) {
	this.holds = holds;
}

public int getRest() {
	return rest;
}

public void setRest(int rest) {
	this.rest = rest;
}

public String getSide() {
	return side;
}

public void setSide(String side) {
	this.side = side;
}

public boolean isShowSides() {
    return showSides;
  }

  public void setShowSides(boolean showSides) {
    this.showSides = showSides;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public int getKeyPhotoVersion() {
    return keyPhotoVersion;
  }

  public void setKeyPhotoVersion(int keyPhotoVersion) {
    this.keyPhotoVersion = keyPhotoVersion;
  }

  public LinkedList<BodyPart> getBodyParts() {
    return bodyParts;
  }

  public void setBodyParts(LinkedList<BodyPart> bodyParts) {
    this.bodyParts = bodyParts;
  }

  public LinkedList<Type> getTypes() {
    return types;
  }

  public void setTypes(LinkedList<Type> types) {
    this.types = types;
  }

  public Long getKeyPhotoId() {
    return keyPhotoId;
  }

  public void setKeyPhotoId(Long keyPhotoId) {
    this.keyPhotoId = keyPhotoId;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public long getCategory() {
    return category;
  }

  public void setCategory(long category) {
    this.category = category;
  }

  public long getCorpId() {
    return corpId;
  }

  public void setCorpId(long corpId) {
    this.corpId = corpId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String[] getEquipment() {
    return equipment;
  }

  public void setEquipment(String[] equipment) {
    this.equipment = equipment;
  }

  public int getRepTime() {
    return repTime;
  }

  public void setRepTime(int repTime) {
    this.repTime = repTime;
  }

  public int getReps() {
    return reps;
  }

  public void setReps(int reps) {
    this.reps = reps;
  }

  public int getRestTime() {
    return restTime;
  }

  public void setRestTime(int restTime) {
    this.restTime = restTime;
  }

  public int getSetRestTime() {
    return setRestTime;
  }

  public void setSetRestTime(int setRestTime) {
    this.setRestTime = setRestTime;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Date getCreated() {
    return created;
  }

  public void setCreated(Date created) {
    this.created = created;
  }

  public long getCreator() {
    return creator;
  }

  public void setCreator(long creator) {
    this.creator = creator;
  }

public boolean isShareLocation() {
	return shareLocation;
}

public void setShareLocation(boolean shareLocation) {
	this.shareLocation = shareLocation;
}

public boolean isHideExercise() {
	return hideExercise;
}

public void setHideExercise(boolean hideExercise) {
	this.hideExercise = hideExercise;
}

public long getShareid() {
	return shareid;
}

public void setShareid(long shareid) {
	this.shareid = shareid;
}

public int getRepsperseconds() {
	return repsperseconds;
}

public void setRepsperseconds(int repsperseconds) {
	this.repsperseconds = repsperseconds;
}

}
