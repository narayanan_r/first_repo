package au.com.ourbodycorp.model;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class CorporationUser {
  private User user;
  private Corporation corporation;
  private CorporationRole role;
  private Date regDate;
  private long inviterId;
  private int selectedFY;
  private List<CorporationRole> roles =  new LinkedList<CorporationRole>();

  public int getSelectedFY() {
    return selectedFY;
  }

  public void setSelectedFY(int selectedFY) {
    this.selectedFY = selectedFY;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public Corporation getCorporation() {
    return corporation;
  }

  public void setCorporation(Corporation corporation) {
    this.corporation = corporation;
  }

  public CorporationRole getRole() {
    return role;
  }

  public void setRole(CorporationRole role) {
    this.role = role;
  }

  public Date getRegDate() {
    return regDate;
  }

  public void setRegDate(Date regDate) {
    this.regDate = regDate;
  }

  public long getInviterId() {
    return inviterId;
  }

  public void setInviterId(long inviterId) {
    this.inviterId = inviterId;
  }

  public List<CorporationRole> getRoles() {
    return roles;
  }

  public void setRoles(List<CorporationRole> roles) {
    this.roles = roles;
  }

  public void addRole(CorporationRole role) {
    roles.add(role);
  }

  public boolean hasRole(CorporationRole.Role... roles) {
    for (CorporationRole role : this.roles) {
      for (CorporationRole.Role r : roles) {
        if (role.getId().equalsIgnoreCase(r.getCode())) return true;
      }
    }
    return false;
  }

  public boolean isTreasurer() {
    return (hasRole(CorporationRole.Role.Treasurer, CorporationRole.Role.Admin, CorporationRole.Role.Manager));
  }

  public boolean isExecutive() {
    return (hasRole(CorporationRole.Role.Secretary, CorporationRole.Role.Admin, CorporationRole.Role.President, CorporationRole.Role.Auditor, CorporationRole.Role.Manager, CorporationRole.Role.Treasurer));
  }
}
