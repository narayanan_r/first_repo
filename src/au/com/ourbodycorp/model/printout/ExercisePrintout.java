package au.com.ourbodycorp.model.printout;


public interface ExercisePrintout
{
    //saves the printout to file in the directory specified by path. The name 
    //of the file is stored in the first element of the name array. The build 
    //status of the printout is returned in the first element of the status 
    //array. Ignore the build status if the method return ERR_NONE. The build 
    //status is not specified when this method return ERR_PARAM.
    //
    //@returns: ERR_PARAM if path, name and/or status is null, or the length of 
    //the name and/or status arrays is 0; ERR_INVALID if there were errors when 
    //building the document (in which case, status[0] specifies the build error); 
    //ERR_NONE on successful execution   
    ERR_T save(String path, String name[], int status[]);
}
