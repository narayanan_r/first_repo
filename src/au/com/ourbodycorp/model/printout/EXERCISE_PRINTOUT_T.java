package au.com.ourbodycorp.model.printout;


//enum for specifying the type of printout to produce (currently just
//pdf printouts implemented). Add more enums vals as more types of 
//printouts are implemented
public enum EXERCISE_PRINTOUT_T
{
    PDF_PRINTOUT(0x01);


    private int val;  //underlying enum val


    //constructor
    EXERCISE_PRINTOUT_T(int val)
    {
        this.val = val;
    }
}
