package au.com.ourbodycorp.model.printout;


import java.util.List;
import au.com.ourbodycorp.model.Patient;
import au.com.ourbodycorp.model.Program;
import au.com.ourbodycorp.model.Corporation;
import au.com.ourbodycorp.model.PatientGroup;
import au.com.ourbodycorp.model.ProgramExercise;


interface ExercisePrintoutFactory
{
    public ExercisePrintout createExercisePrintout(Corporation practice,
                                                   Patient patient,
                                                   PatientGroup group,
                                                   Program program,
                                                   List<ProgramExercise> exercises,
                                                   boolean encryptFile);
}
