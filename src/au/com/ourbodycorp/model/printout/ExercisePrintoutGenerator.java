package au.com.ourbodycorp.model.printout;

import java.util.List;
import au.com.ourbodycorp.model.Patient;
import au.com.ourbodycorp.model.Program;
import au.com.ourbodycorp.model.Corporation;
import au.com.ourbodycorp.model.PatientGroup;
import au.com.ourbodycorp.model.ProgramExercise;

public class ExercisePrintoutGenerator {
	
	

    private ExercisePrintoutFactory factory;  //creates new instance of printout

    public ExercisePrintoutGenerator() {
        //by default set printout type to pdf - ignore return value
        setPrintoutType(EXERCISE_PRINTOUT_T.PDF_PRINTOUT);
    }

    public ExercisePrintoutGenerator(EXERCISE_PRINTOUT_T type) {
        setPrintoutType(type);
    }

    public ERR_T generateExercisePrintout(Corporation practice,
            Patient patient,
            PatientGroup group,
            Program program,
            List<ProgramExercise> exercises,
            boolean encryptFile,
            String path,
            String name[],
            int status[]) {
        ERR_T retval = ERR_T.ERR_PARAM;

        if ((practice != null) && (patient != null || group != null) && (program != null)
                && (exercises != null) && (exercises.size() > 0) && (path != null)
                && (name != null) && (name.length > 0) && (status != null)
                && (status.length > 0)) {
        	
        	System.out.println("=======>ExercisePrintoutGenerator=======>");
        	
            retval = ERR_T.ERR_INVALID;

            if (factory != null) {  //else myself
            	
            	System.out.println("=======>factory !=null=======>");
            	
            	System.out.println("=======>practice=======>"+practice);
            	System.out.println("=======>patient=======>"+patient);
            	System.out.println("=======>group=======>"+group);
            	System.out.println("=======>program=======>"+program);
            	System.out.println("=======>exercises=======>"+exercises);
            	System.out.println("=======>encryptFile=======>"+encryptFile);
            	
            	
            	
                ExercisePrintout printout =
                        factory.createExercisePrintout(practice, patient, group, program,
                        exercises, encryptFile);

                if (printout != null) {  //else myself
                    //now save printout
                	System.out.println("=======>printout!=null=======>");
                	System.out.println("print out save=========>"+path);
                    retval = printout.save(path, name, status);
                }else
                	System.out.println("printout==null");
                	
            }else
            	System.out.println("factory== null=====>");
        }

        return retval;
    }

    public ERR_T setPrintoutType(EXERCISE_PRINTOUT_T type) {
        ERR_T retval = ERR_T.ERR_UNSUPPORTED;
        factory = null;

        switch (type) {
            case PDF_PRINTOUT:
                factory = new PDFExercisePrintoutFactory();
                retval = ERR_T.ERR_NONE;
                break;
            //add new types below this line

            default:
                //unsupported type
                break;
        }

        return retval;
    }
}
