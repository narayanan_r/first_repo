package au.com.ourbodycorp.model.printout;


public enum ERR_T
{
    ERR_NONE(0x01),
    ERR_INVALID(0x02),
    ERR_UNSUPPORTED(0x03),
    ERR_PARAM(0x04),
    ERR_SHORT_BUFF(0x05);


    private int val;


    ERR_T(int val)
    {
        this.val = val;
    }
}
