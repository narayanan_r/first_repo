package au.com.ourbodycorp.model.printout; //this is a stupid package name but 
//for now we are stuck with it

import au.com.ourbodycorp.MailMessage;
import au.com.ourbodycorp.Util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import java.util.LinkedList;
import java.util.Properties;
import java.util.Vector;
import java.io.StringWriter;
import java.io.File;
import java.io.PrintWriter;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import au.com.ourbodycorp.model.Patient;
import au.com.ourbodycorp.model.User;
import au.com.ourbodycorp.model.MyInfo;
import au.com.ourbodycorp.model.Program;
import au.com.ourbodycorp.model.Corporation;
import au.com.ourbodycorp.model.ProgramExercise;
import au.com.ourbodycorp.model.ExerciseMedia;
import au.com.ourbodycorp.model.Exercise;
import au.com.ourbodycorp.model.Attachment;
import au.com.ourbodycorp.model.Constants;
import au.com.ourbodycorp.model.managers.AttachmentManager;
import au.com.ourbodycorp.model.managers.LibraryManager;
import au.com.ourbodycorp.model.managers.ManagerBase;
import au.com.ourbodycorp.model.managers.UserManager;
import au.com.ourbodycorp.model.PatientGroup;
import au.com.ourbodycorp.model.ProgramNote;
import javax.imageio.ImageReadParam;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDJpeg;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;

public class PDFExercisePrintout extends ManagerBase implements ExercisePrintout {

	//myself
   /* private static final String NO_KEY_PHOTO_PATH = Constants.TESTING_MODE ? "/Users/rustyshelf/netbeans_workspace/fiizio_images/no_key_photo.jpg" : "/opt/bitnami/apache-tomcat/webapps/ROOT/images/no_key_photo.jpg";
    private static final String FIIZIO_APP_LOGO_PATH = Constants.TESTING_MODE ? "/Users/rustyshelf/netbeans_workspace/fiizio_images/fiizioapp.jpg" : "/opt/bitnami/apache-tomcat/webapps/ROOT/images/fiizioapp.jpg";
	private static final String IMAGE_CORRUPTED = Constants.TESTING_MODE ? "/Users/rustyshelf/netbeans_workspace/fiizio_images/image_corrupted.jpg" : "/opt/bitnami/apache-tomcat/webapps/ROOT/images/image_corrupted.jpg";*/

	private static final String NO_KEY_PHOTO_PATH = Constants.TESTING_MODE ? "/Users/rustyshelf/netbeans_workspace/fiizio_images/no_key_photo.jpg" : "E:/Images/fiizio/images/no_key_photo.jpg";
	private static final String IMAGE_CORRUPTED = Constants.TESTING_MODE ? "/Users/rustyshelf/netbeans_workspace/fiizio_images/no_key_photo.jpg" : "E:/Images/fiizio/images/image_corrupted.jpg";
    private static final String FIIZIO_APP_LOGO_PATH = Constants.TESTING_MODE ? "/Users/rustyshelf/netbeans_workspace/fiizio_images/fiizioapp.jpg" : "E:/Images/fiizio/images/fiizioapp.jpg";

    private static final PDFont MAJOR_HEADING_FONT = PDType1Font.HELVETICA_BOLD;
    private static final PDFont MINOR_HEADING_FONT = PDType1Font.HELVETICA_BOLD;
    private static final PDFont DEFAULT_FONT = PDType1Font.HELVETICA;
    private static final float MAJOR_HEADING_LINE_HEIGHT = 5;
    private static final int MAJOR_HEADING_FONT_SIZE = 7;
    private static final int MINOR_HEADING_FONT_SIZE = 9;
    private static final int DEFAULT_FONT_SIZE = 9;
    private static final float DEFAULT_LINE_HEIGHT = 11;
    private static final float PAGE_WIDTH = PDPage.PAGE_SIZE_A4.getWidth();
    private static final float PAGE_HEIGHT = PDPage.PAGE_SIZE_A4.getHeight();
    
    private static final float INTERNAL_PAGE_MARGIN = 25;
    private static final float INTERNAL_PAGE_MARGIN_EXTRAINFOPAGES = 36;
    private static final float INTERNAL_VERT_PAD = 10;
    private static final float INTERNAL_HORZ_PAD = 10;
    private static final float PRACTICE_LOGO_HEIGHT = 50;
    private static final float PRACTICE_LOGO_Y_POS =
            PAGE_HEIGHT - PRACTICE_LOGO_HEIGHT - INTERNAL_PAGE_MARGIN;
    private static final float FIIZIO_APP_LOGO_HEIGHT = 50;
    private static final float FIIZIO_APP_LOGO_Y_POS =
            PAGE_HEIGHT - FIIZIO_APP_LOGO_HEIGHT - INTERNAL_PAGE_MARGIN;
    private static final float EXERCISE_IMG_WIDTH = 125;
    private static final float EXERCISE_IMG_X_POS = INTERNAL_PAGE_MARGIN;
    private static final float EXERCISE_IMG_TOP_Y_POS =
            PAGE_HEIGHT - PRACTICE_LOGO_HEIGHT - INTERNAL_PAGE_MARGIN
            - MAJOR_HEADING_LINE_HEIGHT - 4 * DEFAULT_LINE_HEIGHT
            - 3 * INTERNAL_VERT_PAD;
    private static final float QUICK_NOTES_Y_OFFSET =
            PAGE_HEIGHT - PRACTICE_LOGO_HEIGHT - INTERNAL_PAGE_MARGIN
            - MAJOR_HEADING_LINE_HEIGHT - 4 * DEFAULT_LINE_HEIGHT - INTERNAL_VERT_PAD;
    private static final float XTRA_INFO_IMG_MAX_WIDTH = EXERCISE_IMG_WIDTH;
    private static String EXERCISE_DETAILS_NUM_REPS_HEADING = "Reps: ";
    private static String EXERCISE_DETAILS_REP_TIME_HEADING = "Time for 1 Rep: ";
    private static String EXERCISE_DETAILS_HOLD_TIME_HEADING = "Exercise hold time: ";
    private static String EXERCISE_DETAILS_REP_REST_TIME_HEADING =
            "Rest time between reps: ";
    private static String EXERCISE_DETAILS_SET_REST_TIME_HEADING =
            "Rest time between sets: ";
    private static String EXERCISE_DETAILS_NUM_SETS_HEADING = "Sets: ";
    private static String EXERCISE_DETAILS_SIDE_HEADING = "Side: ";
    private static String EXERCISE_DETAILS_REST_TIME_HEADING = "Rest: ";
    public static String errorMsg;   //last error string (for debugging program)
    //error status codes
    public static final int NO_KEY_PHOTO_READ_FAILED = 1;
    public static final int PRACTICE_LOGO_IMG_CREATION_FAILED = 2;
    public static final int FIIZIO_LOGO_READ_FAILED = 3;
    public static final int PRACTICE_LOGO_DRAW_FAILED = 4;
    public static final int FIIZIO_LOGO_DRAW_FAILED = 5;
    public static final int PROGRAM_DETAILS_TEXT_RENDER_FAILED = 6;
    public static final int EXERCISE_IMG_RENDER_FAILED = 7;
    public static final int IMG_CAPTION_TEXT_RENDER_FAILED = 8;
    public static final int XTRA_INFO_TXT_RENDER_FAILED = 9;
    public static final int NULL_SPLITLINE_STRING_VAL = 10;
    public static final int QUICK_NOTES_TEXT_RENDER_FAILED = 11;
    public static final int PRACTICE_LOGO_XTRA_INFO_DRAW_FAILED = 12;
    public static final int FIIZIO_LOGO_XTRA_INFO_DRAW_FAILED = 13;
    public static final int EXERCISE_IMG_CREATION_FAILED = 14;
    public static final int XTRA_INFO_IMG_DRAW_FAILED = 15;
    public static final int PDF_DOC_SAVE_FAILED = 16;
    public static final int XTRA_INFO_IMG_CREATION_FAILED = 17;
    public static final int INVALID_PRINTOUT_SAVE_PATH = 18;
    public static final int PDF_DOC_CREATION_FAILED = 19;
    public static final int INVALID_CONSTRUCTOR_PARAMS = 20;
    public static final int MANAGER_CREATION_FAILED = 21;
    public static final int EXERCISE_DESCR_SPLIT_LINES_FAILURE = 22;
    public static final int IMG_DESCR_TEXT_RENDER_FAILED = 23;
    public static final int EXERCISE_DETAILS_TEXT_RENDER_FAILED = 24;
    private Patient programPatient;
    private PatientGroup programGroup;
    private PDDocument doc;  //reference to pdf doc
    private User creator;   //exercise program creator
    private int buildStatus;
    private PDPageContentStream pdfstream;
    private boolean buildSuccess;
    private long programId;
    private Corporation practice;
    private LibraryManager libManager;
    private long creatorId;
     
    
    public PDFExercisePrintout(Corporation practice, Patient patient, PatientGroup group, Program program, List<ProgramExercise> exercises, boolean encryptFile) {
    	System.out.println("PDFExercisePrintout=========>");
        buildStatus = INVALID_CONSTRUCTOR_PARAMS;
        if ((practice != null) && (patient != null || group != null) && (program != null) && (exercises != null) && (exercises.size() > 0)) {
            programId = program.getId();
            this.practice = practice;
            UserManager userManager = null;
            this.programPatient = patient;
            this.programGroup = group;
            buildSuccess = false;
  

            try {
                creatorId = program.getCreator();
                buildStatus = MANAGER_CREATION_FAILED;
                userManager = new UserManager();
                libManager = new LibraryManager();
                creator = userManager.getUser(creatorId);

                //create new pdf document
                buildStatus = PDF_DOC_CREATION_FAILED;
                doc = new PDDocument();

                if (encryptFile && (creator != null)) {
                    //create buffer to hold passphrase
                    StringBuffer buffer = new StringBuffer();
                    String firstname = creator.getFirstName();

                    if (firstname != null) {
                        for (int indx = 0; indx < firstname.length(); indx++) {
                            char ch = firstname.charAt(indx);

                            if (indx == 0) {
                                ch = Character.toUpperCase(ch);
                            }

                            buffer.append(ch);
                        }

                        String lastname = creator.getLastName();

                        if (lastname != null) {
                            for (int indx = 0; indx < lastname.length(); indx++) {
                                char ch = lastname.charAt(indx);

                                if (indx == 0) {
                                    ch = Character.toUpperCase(ch);
                                }

                                buffer.append(ch);
                            }

                            String passphrase = buffer.toString();

                            //mark the document as encrypted (note: it is not 
                            //actually encrypted until it is saved)
                            doc.encrypt(passphrase, passphrase);
                        }
                    }
                }

                PDJpeg practiceLogo[] = {null}; //practice logo 
                PDJpeg fiizioLogo[] = {null}; //fiizio logo

                //lists for storing exercise info, extra info, and
                //images for extra info
                List<MyInfo> xtraInfo =
                        libManager.listInfoForProgram(programId);
                List<PDJpeg> exerciseKeyPhotos = new LinkedList<PDJpeg>();
                List<PDJpeg> xtraInfoImgs = new LinkedList<PDJpeg>();

                //create all the images (must do this first before adding
                //any image)
                createImages(practice, exercises, xtraInfo, practiceLogo,
                        fiizioLogo, exerciseKeyPhotos, xtraInfoImgs);
                float yOffset = PAGE_HEIGHT - INTERNAL_PAGE_MARGIN;
                PDPage page = new PDPage(PDPage.PAGE_SIZE_A4);
                doc.addPage(page);
                pdfstream = new PDPageContentStream(doc, page, true, false);

                //add images to doc, along with program details and
                //quick notes
                addImages(practiceLogo[0], fiizioLogo[0]);
                addProgramDetails(program);
                yOffset = addQuickNotes(QUICK_NOTES_Y_OFFSET);

                for (int indx = 0; indx < exercises.size(); indx++) {
                    if (exercises.get(indx) != null) {
                        yOffset = addExercise(exercises.get(indx), exerciseKeyPhotos.get(indx), yOffset);
                    }
                }

                //now add extra info sheets to printout
                for (int indx = 0; indx < xtraInfo.size(); indx++) {
                    MyInfo info = xtraInfo.get(indx);

                    if (info != null) {
                        String text = info.getBody();
                        text = text.replaceAll("(<(p|P)>|<(BR|br)(/)?>)", "\n");
                        text = text.replaceAll("&nbsp;", " ");
                        text = text.replaceAll("<(.*?)>", "");
                        text = text.replaceAll("\n(\\s)*\n", "\n");
                        String title = info.getHeadline();
                        PDJpeg xtraInfoImg = xtraInfoImgs.get(indx);

                        if ((text != null) && (info != null)
                                && (title != null) && (xtraInfoImg != null)) {
                            addExtraInfoPages(practiceLogo[0], fiizioLogo[0],
                                    xtraInfoImg, title, text);
                        }
                    }
                }

                pdfstream.close();

                //all done - and no exceptions thrown so build must have been
                //successful
                buildSuccess = true;
            }
            catch (Exception e) {
                e.printStackTrace();
                errorMsg = getStackTrace(e);
            }
        }
    }

    public ERR_T save(String path, String name[], int status[]) {
        ERR_T retval = ERR_T.ERR_PARAM;
        
        if ((path != null) && (name != null) && (name.length > 0) && (status != null) && (status.length > 0)) {
            retval = ERR_T.ERR_INVALID;

            if (buildSuccess && (programPatient != null || programGroup != null) && (doc != null)) {
                try {
                	//String path1 = "C:/MyPhysio-Eclipse/Fiizio/WebContent/images/";
                	
                	
                	//String path1 = "D:/My project/fiizionew/fizzio/WebContent/images";
                	
                	String path1 = "E:/Images/fiizio/images/pdf_chk/";
                	//myself
                	//String path1 = "/opt/bitnami/apache-tomcat/webapps/ROOT/printouts/";
                    buildStatus = INVALID_PRINTOUT_SAVE_PATH;
                    File dir = new File(path1);

                    if (dir.exists()) {
                        StringBuffer buffer = new StringBuffer("program_printout_");
                        if (programPatient != null){
                            buffer.append(programPatient.getFirstName().replaceAll("[^\\w\\s]",""));
                            buffer.append('_');
                            buffer.append(programPatient.getLastName().replaceAll("[^\\w\\s]",""));
                            buffer.append('_');
                            buffer.append(programPatient.getId());
                        }
                        else {
                            buffer.append(programGroup.getGroupName().replaceAll(" ","_"));
                            buffer.append('_');
                            buffer.append(programGroup.getId());
                        }
                        buffer.append(programId);
                        buffer.append(".pdf");
                        String fileName = buffer.toString();
                        buffer = new StringBuffer(path1);

                        if (path1.charAt(path1.length() - 1) != '/') {
                            buffer.append('/');
                        }

                        buffer.append(fileName);
                        doc.save(buffer.toString());
                        name[0] = fileName;
                        doc.close();
                        retval = ERR_T.ERR_NONE;
                    }
                }
                catch (Exception e) {
                    buildStatus = PDF_DOC_SAVE_FAILED;
                    errorMsg = getStackTrace(e);
                     e.printStackTrace();
                }
            }

            status[0] = buildStatus;
        }

        return retval;
    }

    //one must call this method before a stream is created otherwise the images
    //are not rendered by the pdf library - WTF !!!!
    //preconditions: doc not null, exercises not null, practiceLogo not null
    //practiceLogo length > 0, fiizioLogo not null, fiizioLogo length > 0
    //practice not null
    //postconditions: practiceLogo contains either: the practice logo
    //if the logo was successfully read from file, or the 'no key value' image
    //if the logo was not successfully read from file. fiiziologo contains
    //the fiizio logo 
    private void createImages(Corporation practice, List<ProgramExercise> exercises, List<MyInfo> xtraInfo, PDJpeg practiceLogo[], PDJpeg fiizioLogo[], List<PDJpeg> exerciseImgs, List<PDJpeg> xtraInfoImgs) throws Exception {
        byte buff[] = null;
        byte practiceLogoData[] = null;
        byte practiceLogoCorrupt[] = null;
        
        AttachmentManager attachManager = new AttachmentManager();
        buildStatus = NO_KEY_PHOTO_READ_FAILED;
        //by default, load no key photo bytes for image
        FileInputStream instream = new FileInputStream(NO_KEY_PHOTO_PATH);
        int len = instream.available(); //get estimate of file size

        if (len > 0) {
            buff = new byte[len];
            instream.read(buff);  //read bytes from file - buff contains
            //default image
            practiceLogoData = buff;
        }

        instream.close();
        Attachment attachment = attachManager.getAttachment(practice.getLogoId(), true);

        if ((attachment != null) && (attachment.getData() != null)
                && (attachment.getData().length > 0)) {
            //use the practice logo instead
            practiceLogoData = attachment.getData();
        }

        buildStatus = PRACTICE_LOGO_IMG_CREATION_FAILED;
        BufferedImage buffImg;
        if(practiceLogoData.length != 0){
	        buffImg =
	                ImageIO.read(new ByteArrayInputStream(practiceLogoData));
	        practiceLogo[0] = new PDJpeg(doc, buffImg);
    	}else{
        	//by default, load no key photo bytes for image
            FileInputStream instreamNoImage = new FileInputStream(IMAGE_CORRUPTED);
            int lenNoImage = instreamNoImage.available(); //get estimate of file size

            if (lenNoImage > 0) {
                buff = new byte[lenNoImage];
                instreamNoImage.read(buff);  //read bytes from file - buff contains
                //default image
                practiceLogoCorrupt = buff;
            }
            buffImg =
	                ImageIO.read(new ByteArrayInputStream(practiceLogoCorrupt));
	        practiceLogo[0] = new PDJpeg(doc, buffImg);
            instreamNoImage.close();
            
            String email = "";
            String phone = "";
            
            Connection conn = null;
            conn = ds.getConnection();
            PreparedStatement pst = conn.prepareStatement("select email,phone from company where id = ?");
            pst.setLong(1, practice.getCompanyId());
            ResultSet rs1 = pst.executeQuery();
            if(rs1.next()){ 
            	email = rs1.getString("email");
            	phone = rs1.getString("phone");
            }
            
            Properties props = new Properties();
            props.put("reason", "Please reupload the practice logo.");
            props.put("imagename", practice.getName());
            props.put("email", email);
            props.put("phone", phone);
            props.put("practicename", practice.getName());
            MailMessage msg = new MailMessage(props, "image_corrupted.vm", "shagul001@gmail.com","PDF Image Corrupted",true);
//            MailMessage msg = new MailMessage(props, "image_corrupted.vm", "admin@myphysioapp.com.au","PDF Image Corrupted");
            
            try {
                msg.send();
            }
            catch (Exception e) {
//	                e.printStackTrace();
//	                throw e;
            }
        }

        //read fiizio logo
        buildStatus = FIIZIO_LOGO_READ_FAILED;
        buffImg = ImageIO.read(new FileInputStream(FIIZIO_APP_LOGO_PATH));
        fiizioLogo[0] = new PDJpeg(doc, buffImg);

        //read images for each exercise
        buildStatus = EXERCISE_IMG_CREATION_FAILED;
        byte exerciseImgData[] = null;
        ImageReader jpgReader = ImageIO.getImageReadersBySuffix("jpg").next();
        ImageIO.setUseCache(false);
        ImageReadParam imageReadingParams = new ImageReadParam();
        
        for (int indx = 0; indx < exercises.size(); indx++) {
            ProgramExercise exercise = exercises.get(indx);
            exerciseImgData = buff;  //set to default image bytes
            imageReadingParams.setSourceSubsampling(1, 1, 0, 0);
            
            if (exercise != null) {
                try {
                    ExerciseMedia media = libManager.getExerciseMedia(exercise.getKeyPhotoId(), true);
                    
                    if (media != null) {
                        exerciseImgData = media.getData();
                        imageReadingParams.setSourceSubsampling(4, 4, 0, 0);

                    }
                }
                catch (Exception e) {
                    errorMsg = getStackTrace(e);
                }
            }
            ByteArrayInputStream byteStream;
            if(exerciseImgData.length != 0){
            	 byteStream = new ByteArrayInputStream(exerciseImgData);
            	 ImageInputStream imageInputStream = null;
                 try{
                     imageInputStream = ImageIO.createImageInputStream(byteStream);
                     jpgReader.setInput(imageInputStream, false);
                     buffImg = jpgReader.read(0, imageReadingParams);
                     exerciseImgs.add(new PDJpeg(doc, buffImg));
                 }
                 catch (javax.imageio.IIOException readException){
                     //if we can't read the above image the fast way, try the slow way instead
                     buffImg = ImageIO.read(new ByteArrayInputStream(exerciseImgData));
                     exerciseImgs.add(new PDJpeg(doc, buffImg));
                 }
                 catch(Exception e){
                     e.printStackTrace();
                 }
                 finally{
                     if (imageInputStream != null) imageInputStream.close();
                 }
            }else{
            	//by default, load no key photo bytes for image
                FileInputStream instreamNoImage = new FileInputStream(IMAGE_CORRUPTED);
                int lenNoImage = instreamNoImage.available(); //get estimate of file size

                if (lenNoImage > 0) {
                    buff = new byte[lenNoImage];
                    instreamNoImage.read(buff);  //read bytes from file - buff contains
                    //default image
                    practiceLogoCorrupt = buff;
                }
                byteStream = new ByteArrayInputStream(practiceLogoCorrupt);
                ImageInputStream imageInputStream = null;
                try{
                    imageInputStream = ImageIO.createImageInputStream(byteStream);
                    jpgReader.setInput(imageInputStream, false);
                    buffImg = ImageIO.read(new ByteArrayInputStream(practiceLogoCorrupt));
                    exerciseImgs.add(new PDJpeg(doc, buffImg));
                }
                catch (javax.imageio.IIOException readException){
                    //if we can't read the above image the fast way, try the slow way instead
                    buffImg = ImageIO.read(new ByteArrayInputStream(practiceLogoCorrupt));
                    exerciseImgs.add(new PDJpeg(doc, buffImg));
                }
                catch(Exception e){
                    e.printStackTrace();
                }
                finally{
                    if (imageInputStream != null) imageInputStream.close();
                }
                instreamNoImage.close();
                
                String email = "";
                String phone = "";
                
                Connection conn = null;
                conn = ds.getConnection();
                PreparedStatement pst = conn.prepareStatement("select email,phone from company where id = ?");
                pst.setLong(1, practice.getCompanyId());
                ResultSet rs1 = pst.executeQuery();
                if(rs1.next()){ 
                	email = rs1.getString("email");
                	phone = rs1.getString("phone");
                }
                
                Properties props = new Properties();
                props.put("reason", "Please reupload the exercise image.");
                props.put("imagename", exercise.getName());
                props.put("email", email);
                props.put("phone", phone);
                props.put("practicename", practice.getName());
                MailMessage msg = new MailMessage(props, "image_corrupted.vm", "shagul001@gmail.com","PDF Image Corrupted",true);
//                MailMessage msg = new MailMessage(props, "image_corrupted.vm", "admin@myphysioapp.com.au","PDF Image Corrupted");
                try {
                    msg.send();
                }
                catch (Exception e) {
//    	                e.printStackTrace();
//    	                throw e;
                }
            }
           
            
        }

        //create images for xtra info pages
        buildStatus = XTRA_INFO_IMG_CREATION_FAILED;
        byte infoImgData[] = null;
        for (int indx = 0; indx < xtraInfo.size(); indx++) {
            MyInfo info = xtraInfo.get(indx);
            infoImgData = buff;

            if (info != null) {
                attachment = attachManager.getAttachment(info.getPhotoId(), true);

                if (attachment != null) {
                    //use the exercise image
                    if (attachment.getData() != null) {
                        infoImgData = attachment.getData();
                    }
                }
            }

            if(infoImgData.length != 0){
	            buffImg = ImageIO.read(new ByteArrayInputStream(infoImgData));
	            xtraInfoImgs.add(new PDJpeg(doc, buffImg));
            }else{
            	//by default, load no key photo bytes for image
                FileInputStream instreamNoImage = new FileInputStream(IMAGE_CORRUPTED);
                int lenNoImage = instreamNoImage.available(); //get estimate of file size

                if (lenNoImage > 0) {
                    buff = new byte[lenNoImage];
                    instreamNoImage.read(buff);  //read bytes from file - buff contains
                    //default image
                    practiceLogoCorrupt = buff;
                }
            	buffImg = ImageIO.read(new ByteArrayInputStream(practiceLogoCorrupt));
                xtraInfoImgs.add(new PDJpeg(doc, buffImg));
                instreamNoImage.close();
                
                String email = "";
                String phone = "";
                
                Connection conn = null;
                conn = ds.getConnection();
                PreparedStatement pst = conn.prepareStatement("select email,phone from company where id = ?");
                pst.setLong(1, practice.getCompanyId());
                ResultSet rs1 = pst.executeQuery();
                if(rs1.next()){ 
                	email = rs1.getString("email");
                	phone = rs1.getString("phone");
                }
                
                Properties props = new Properties();
                props.put("reason", "Please reupload the reading info image.");
                props.put("imagename", info.getHeadline());
                props.put("email", email);
                props.put("phone", phone);
                props.put("practicename", practice.getName());
                MailMessage msg = new MailMessage(props, "image_corrupted.vm", "shagul001@gmail.com","PDF Image Corrupted",true);
//              MailMessage msg = new MailMessage(props, "image_corrupted.vm", "admin@myphysioapp.com.au","PDF Image Corrupted");
                try {
                    msg.send();
                }
                catch (Exception e) {
//    	                e.printStackTrace();
//    	                throw e;
                }
                
            }
        }
    }

    //preconditions: doc not null, practiceLogo not null, fiizioLogo not null,
    //pdfstream not null
    //exerciseImg not null, quickNotes not null, quickNotes.length > 0
    private void addImages(PDJpeg practiceLogo,
            PDJpeg fiizioLogo) throws Exception {
        float aspectRatio =
                ((float) practiceLogo.getWidth()) / practiceLogo.getHeight();
        float imgWidth = PRACTICE_LOGO_HEIGHT * aspectRatio;

        //one has to create the images BEFORE creating the stream - WTF!!!
        buildStatus = PRACTICE_LOGO_DRAW_FAILED;
        pdfstream.drawXObject(fiizioLogo, INTERNAL_PAGE_MARGIN-5,
                PRACTICE_LOGO_Y_POS-2, imgWidth+80, PRACTICE_LOGO_HEIGHT+15);

        aspectRatio =
                ((float) fiizioLogo.getWidth()) / fiizioLogo.getHeight();
        imgWidth = FIIZIO_APP_LOGO_HEIGHT * aspectRatio;
        float xpos = PAGE_WIDTH - INTERNAL_PAGE_MARGIN - imgWidth;
        buildStatus = FIIZIO_LOGO_DRAW_FAILED;
        pdfstream.drawXObject(practiceLogo, xpos-25, FIIZIO_APP_LOGO_Y_POS, imgWidth+50,
                FIIZIO_APP_LOGO_HEIGHT+10);
    }

    //preconditions: program not null, pdfstream not null
    //This method assumes that all program details will fit onto current pdf page
    //(i.e. it does not check if text will fit onto current page)
    private void addProgramDetails(Program program) throws Exception {
        buildStatus = PROGRAM_DETAILS_TEXT_RENDER_FAILED;
        float yOffset = PAGE_HEIGHT - INTERNAL_PAGE_MARGIN - PRACTICE_LOGO_HEIGHT - INTERNAL_VERT_PAD;
        yOffset -= MAJOR_HEADING_LINE_HEIGHT;
        yOffset -= DEFAULT_LINE_HEIGHT;
        
        //display patient name
        pdfstream.beginText();
        pdfstream.moveTextPositionByAmount(INTERNAL_PAGE_MARGIN, yOffset);
        pdfstream.setFont(MINOR_HEADING_FONT, MINOR_HEADING_FONT_SIZE);
        pdfstream.drawString((programPatient == null)? "Group: " : "Patient: ");
        pdfstream.endText();
        pdfstream.beginText();
        pdfstream.setFont(DEFAULT_FONT, DEFAULT_FONT_SIZE);
        pdfstream.moveTextPositionByAmount(INTERNAL_PAGE_MARGIN
                + MINOR_HEADING_FONT.getStringWidth("Patient: ") / 1000
                * MINOR_HEADING_FONT_SIZE, yOffset);

        if (programPatient != null){
            if (programPatient.getFullName() != null) {
                pdfstream.drawString(programPatient.getFullName());
            }
        }
        else {
            if (programGroup.getGroupName() != null) {
                pdfstream.drawString(programGroup.getGroupName());
            }
        }

        pdfstream.endText();

        //display frequency
        yOffset -= DEFAULT_LINE_HEIGHT;
        pdfstream.beginText();
        pdfstream.moveTextPositionByAmount(INTERNAL_PAGE_MARGIN, yOffset);
        pdfstream.setFont(MINOR_HEADING_FONT, MINOR_HEADING_FONT_SIZE);
        pdfstream.drawString("Frequency: ");
        pdfstream.endText();
        pdfstream.beginText();
        pdfstream.moveTextPositionByAmount(INTERNAL_PAGE_MARGIN
                + MINOR_HEADING_FONT.getStringWidth("Frequency: ") / 1000
                * MINOR_HEADING_FONT_SIZE, yOffset);
        pdfstream.setFont(DEFAULT_FONT, DEFAULT_FONT_SIZE);

        //convert interval to pretty output string
        StringBuilder builder = new StringBuilder();
        int interval = program.getInterval();

        switch (program.getIntervalType()) {
            case perDay:
                if (interval == 1) {
                    builder.append("Once per day");
                }
                else {
                    builder.append(interval);
                    builder.append(" times per day");
                }
                break;
            case perWeek:
                if (interval == 1) {
                    builder.append("Once per week");
                }
                else {
                    builder.append(interval);
                    builder.append(" times per week");
                }
                break;
            case hours:
                builder.append("every ");
                builder.append(interval);
                builder.append(" hours");
                break;
            default:
                builder.append("Unknown");
                break;
        }

        pdfstream.drawString(builder.toString());
        pdfstream.endText();

        yOffset -= DEFAULT_LINE_HEIGHT;
        
        //display creator if not null
        if (creator != null) {
            pdfstream.beginText();
            pdfstream.moveTextPositionByAmount(INTERNAL_PAGE_MARGIN, yOffset);
            pdfstream.setFont(MINOR_HEADING_FONT, MINOR_HEADING_FONT_SIZE);
            pdfstream.drawString("Creator: ");
            pdfstream.endText();
            pdfstream.beginText();
            pdfstream.moveTextPositionByAmount(INTERNAL_PAGE_MARGIN
                    + MINOR_HEADING_FONT.getStringWidth("Creator: ") / 1000
                    * MINOR_HEADING_FONT_SIZE, yOffset);
            pdfstream.setFont(DEFAULT_FONT, DEFAULT_FONT_SIZE);

            if (creator.getFullName() != null) {
                pdfstream.drawString(creator.getFullName());
            }

            yOffset -= DEFAULT_LINE_HEIGHT;
            pdfstream.endText();
        }

        //display creator if not null
        if (practice != null) {
            //line it up with the patients tag
            float rightAlignYOffset = PAGE_HEIGHT - INTERNAL_PAGE_MARGIN - PRACTICE_LOGO_HEIGHT - INTERNAL_VERT_PAD - DEFAULT_LINE_HEIGHT - MAJOR_HEADING_LINE_HEIGHT;
            float rightHandSide = PAGE_WIDTH - INTERNAL_PAGE_MARGIN;
            
            //practice name
            String practiceName = practice.getName();
            pdfstream.beginText();
            pdfstream.moveTextPositionByAmount(rightHandSide - (DEFAULT_FONT.getStringWidth(practiceName) / 1000 * DEFAULT_FONT_SIZE), rightAlignYOffset);
            pdfstream.setFont(DEFAULT_FONT, DEFAULT_FONT_SIZE);
            pdfstream.drawString(practiceName);
            pdfstream.endText();
            rightAlignYOffset -= DEFAULT_LINE_HEIGHT;
            
            //address
            String practiceAddress = practice.getAddress1();
            if (practice.getAddress2() != null && practice.getAddress2().trim().length() > 0){
                practiceAddress += ", " + practice.getAddress2();
            }
            pdfstream.beginText();
            pdfstream.moveTextPositionByAmount(rightHandSide - (DEFAULT_FONT.getStringWidth(practiceAddress) / 1000 * DEFAULT_FONT_SIZE), rightAlignYOffset);
            pdfstream.setFont(DEFAULT_FONT, DEFAULT_FONT_SIZE);
            pdfstream.drawString(practiceAddress);
            pdfstream.endText();
            rightAlignYOffset -= DEFAULT_LINE_HEIGHT;
            
            //postcode, state, etc
            String statePostcode = practice.getSuburb() + " " + practice.getState() + " " + practice.getPostcode();
            if (statePostcode != null && statePostcode.trim().length() > 0){
                pdfstream.beginText();
                pdfstream.moveTextPositionByAmount(rightHandSide - (DEFAULT_FONT.getStringWidth(statePostcode) / 1000 * DEFAULT_FONT_SIZE), rightAlignYOffset);
                pdfstream.setFont(DEFAULT_FONT, DEFAULT_FONT_SIZE);
                pdfstream.drawString(statePostcode);
                pdfstream.endText();
                rightAlignYOffset -= DEFAULT_LINE_HEIGHT;
            }
        
            //phone number
            String phone = Util.notNull(practice.getMetaString("phone"));
            if (phone != null && phone.trim().length() > 0){
                pdfstream.beginText();
                pdfstream.moveTextPositionByAmount(rightHandSide - (DEFAULT_FONT.getStringWidth(phone) / 1000 * DEFAULT_FONT_SIZE), rightAlignYOffset);
                pdfstream.setFont(DEFAULT_FONT, DEFAULT_FONT_SIZE);
                pdfstream.drawString(phone);
                pdfstream.endText();
                rightAlignYOffset -= DEFAULT_LINE_HEIGHT;
            }
        }

        
        //display date created
        pdfstream.beginText();
        pdfstream.moveTextPositionByAmount(INTERNAL_PAGE_MARGIN, yOffset);
        pdfstream.setFont(MINOR_HEADING_FONT, MINOR_HEADING_FONT_SIZE);
        pdfstream.drawString("Created: ");
        pdfstream.endText();
        pdfstream.beginText();
        pdfstream.moveTextPositionByAmount(INTERNAL_PAGE_MARGIN
                + MINOR_HEADING_FONT.getStringWidth("Created: ") / 1000
                * MINOR_HEADING_FONT_SIZE, yOffset);
        pdfstream.setFont(DEFAULT_FONT, DEFAULT_FONT_SIZE);

        if (program.getCreated() != null) {
            pdfstream.drawString(program.getCreated().toString());
        }

        yOffset -= INTERNAL_VERT_PAD;
        pdfstream.endText();

        //draw horizontal separation line
        pdfstream.drawLine(INTERNAL_PAGE_MARGIN, yOffset,
                PAGE_WIDTH - INTERNAL_PAGE_MARGIN, yOffset);
    }

    //preconditions: exercise not null, exerciseImg not null pdfstream not null,
    //doc not null
    //this method will add new pages to pdf doc and re-create pdfstream as required
    private float addExercise(ProgramExercise exercise,
            PDJpeg exerciseImg,
            float yOffset) throws Exception {
        StringBuffer buff = new StringBuffer();
        float newOffset = 0; 
        
        yOffset = verifyRemainingPageSpace(yOffset, INTERNAL_VERT_PAD);
        yOffset -= INTERNAL_VERT_PAD;
        pdfstream.drawLine(INTERNAL_PAGE_MARGIN, yOffset+5,
                PAGE_WIDTH - INTERNAL_PAGE_MARGIN, yOffset+5);
        yOffset = verifyRemainingPageSpace(yOffset, INTERNAL_VERT_PAD);
        yOffset -= INTERNAL_VERT_PAD;

        buildStatus = EXERCISE_IMG_RENDER_FAILED;
        float aspectRatio =
                ((float) exerciseImg.getWidth()) / exerciseImg.getHeight();
        
        float imgHeight = 0;
        if (exercise.getName().startsWith("Rest")) {
        	imgHeight = 100;
        }else{
        	imgHeight = 75;
        }
        //verify there is enough room for image and its name on the page
        	yOffset =
                    verifyRemainingPageSpace(yOffset, imgHeight + DEFAULT_LINE_HEIGHT);
        
        float initYOffset = yOffset;
        if (exercise.getName().startsWith("Rest")) {
        	yOffset -= 0;
        }else{
        	yOffset -= imgHeight;
        }
        if (!exercise.getName().startsWith("Rest")) {
        pdfstream.drawXObject(exerciseImg, EXERCISE_IMG_X_POS, yOffset,
                EXERCISE_IMG_WIDTH, imgHeight);
        }
        yOffset -= DEFAULT_LINE_HEIGHT;
        
        float imageyOffset = yOffset;
        //add exercise name below img
        if (exercise.getName().startsWith("Rest")) {
        	if (exercise.getName() != null) {
        		buildStatus = IMG_CAPTION_TEXT_RENDER_FAILED;
        		pdfstream.beginText();
        		pdfstream.setFont(DEFAULT_FONT, DEFAULT_FONT_SIZE);
        		pdfstream.moveTextPositionByAmount(INTERNAL_PAGE_MARGIN, yOffset+7);
        		pdfstream.drawString(exercise.getName());
        		pdfstream.endText();
        	}
        }else{
        	if (exercise.getName() != null) {
        		buildStatus = IMG_CAPTION_TEXT_RENDER_FAILED;
        		pdfstream.beginText();
        		pdfstream.setFont(DEFAULT_FONT, DEFAULT_FONT_SIZE);
        		pdfstream.moveTextPositionByAmount(INTERNAL_PAGE_MARGIN, yOffset+90);
        		pdfstream.drawString(exercise.getName());
        		pdfstream.endText();
        	}
        }
        
        if (!exercise.getName().startsWith("Rest")) {
        //store the y coord of the bottom of the text below the image
        float imgTextBottomYOffset = yOffset;
        int numPages = doc.getNumberOfPages();
        float xpos =
                EXERCISE_IMG_WIDTH + INTERNAL_HORZ_PAD + INTERNAL_PAGE_MARGIN;

        //add exercise details to right of image
        buildStatus = EXERCISE_DETAILS_TEXT_RENDER_FAILED;

        //reset yOffset to top image then move down 1 line for start of text
        yOffset = initYOffset;
        yOffset = verifyRemainingPageSpace(yOffset, DEFAULT_LINE_HEIGHT);
        yOffset -= DEFAULT_LINE_HEIGHT;
        pdfstream.beginText();
        pdfstream.moveTextPositionByAmount(xpos, yOffset);
        pdfstream.setFont(MINOR_HEADING_FONT, MINOR_HEADING_FONT_SIZE);
        pdfstream.drawString(EXERCISE_DETAILS_NUM_REPS_HEADING);
        pdfstream.endText();
        pdfstream.beginText();
        pdfstream.moveTextPositionByAmount(xpos
                + MINOR_HEADING_FONT.getStringWidth(EXERCISE_DETAILS_NUM_REPS_HEADING)
                / 1000 * MINOR_HEADING_FONT_SIZE, yOffset);
        pdfstream.setFont(DEFAULT_FONT, DEFAULT_FONT_SIZE);
        pdfstream.drawString(String.valueOf(exercise.getReps()));
        pdfstream.endText();
        Exercise ex = null;

        //catch any exceptions when getting associated exercise object rather than
        //pass this exception up the call stack (failure to obtain a ref to associated
        //exercise object shouldn't prevent the printout from being rendered)
        try {
            ex = libManager.getExercise(exercise.getExerciseId());
        }
        catch (Exception e) {
        }

        if (ex != null) {
            boolean isRepExercise =
                    ((ex.getType() != null)
                    && Exercise.REP_EXERCISE.equals(ex.getType()));

            if ((exercise.getHolds() > 0) && !isRepExercise) {
                yOffset = verifyRemainingPageSpace(yOffset, DEFAULT_LINE_HEIGHT);
                yOffset -= DEFAULT_LINE_HEIGHT;
                newOffset = yOffset;
                pdfstream.beginText();
                pdfstream.moveTextPositionByAmount(xpos, yOffset);
                pdfstream.setFont(MINOR_HEADING_FONT, MINOR_HEADING_FONT_SIZE);
                pdfstream.drawString(EXERCISE_DETAILS_HOLD_TIME_HEADING);
                pdfstream.endText();

                pdfstream.beginText();
                pdfstream.moveTextPositionByAmount(xpos
                        + MINOR_HEADING_FONT.getStringWidth(
                        EXERCISE_DETAILS_HOLD_TIME_HEADING) / 1000
                        * MINOR_HEADING_FONT_SIZE, yOffset);
                pdfstream.setFont(DEFAULT_FONT, DEFAULT_FONT_SIZE);
                buff.append(exercise.getHolds());
                buff.append(" seconds");
                pdfstream.drawString(buff.toString());
                pdfstream.endText();
            }

            if (isRepExercise) {
                if (exercise.getHolds() > 0) {
                    yOffset = verifyRemainingPageSpace(yOffset, DEFAULT_LINE_HEIGHT);
                    yOffset -= DEFAULT_LINE_HEIGHT;
                    pdfstream.beginText();
                    pdfstream.moveTextPositionByAmount(xpos, yOffset);
                    pdfstream.setFont(MINOR_HEADING_FONT, MINOR_HEADING_FONT_SIZE);
                    pdfstream.drawString(EXERCISE_DETAILS_REP_TIME_HEADING);
                    pdfstream.endText();

                    pdfstream.beginText();
                    pdfstream.moveTextPositionByAmount(xpos
                            + MINOR_HEADING_FONT.getStringWidth(
                            EXERCISE_DETAILS_REP_TIME_HEADING) / 1000
                            * MINOR_HEADING_FONT_SIZE, yOffset);
                    pdfstream.setFont(DEFAULT_FONT, DEFAULT_FONT_SIZE);
                    buff.append(exercise.getHolds());
                    buff.append(" seconds");
                    pdfstream.drawString(buff.toString());
                    pdfstream.endText();
                }

                if ((exercise.getReps() > 1) && (ex.getRestTime() > 0)) {
                    yOffset =
                            verifyRemainingPageSpace(yOffset, DEFAULT_LINE_HEIGHT);
                    yOffset -= DEFAULT_LINE_HEIGHT;
                    pdfstream.beginText();
                    pdfstream.moveTextPositionByAmount(xpos, yOffset);
                    pdfstream.setFont(MINOR_HEADING_FONT, MINOR_HEADING_FONT_SIZE);
                    pdfstream.drawString(EXERCISE_DETAILS_REP_REST_TIME_HEADING);
                    pdfstream.endText();

                    pdfstream.beginText();
                    pdfstream.moveTextPositionByAmount(xpos
                            + MINOR_HEADING_FONT.getStringWidth(
                            EXERCISE_DETAILS_REP_REST_TIME_HEADING) / 1000
                            * MINOR_HEADING_FONT_SIZE, yOffset);
                    pdfstream.setFont(DEFAULT_FONT, DEFAULT_FONT_SIZE);
                    buff.append(ex.getRestTime());
                    buff.append(" seconds");
                    pdfstream.drawString(buff.toString());
                    pdfstream.endText();
                }

                if ((exercise.getSets() > 1) && (ex.getSetRestTime() > 0)) {
                    yOffset =
                            verifyRemainingPageSpace(yOffset, DEFAULT_LINE_HEIGHT);
                    yOffset -= DEFAULT_LINE_HEIGHT;
                    pdfstream.beginText();
                    pdfstream.moveTextPositionByAmount(xpos, yOffset);
                    pdfstream.setFont(MINOR_HEADING_FONT, MINOR_HEADING_FONT_SIZE);
                    pdfstream.drawString(EXERCISE_DETAILS_SET_REST_TIME_HEADING);
                    pdfstream.endText();

                    pdfstream.beginText();
                    pdfstream.moveTextPositionByAmount(xpos
                            + MINOR_HEADING_FONT.getStringWidth(
                            EXERCISE_DETAILS_SET_REST_TIME_HEADING) / 1000
                            * MINOR_HEADING_FONT_SIZE, yOffset);
                    pdfstream.setFont(DEFAULT_FONT, DEFAULT_FONT_SIZE);
                    buff.append(ex.getSetRestTime());
                    buff.append(" seconds");
                    pdfstream.drawString(buff.toString());
                    pdfstream.endText();
                }
            }
        }

        yOffset = verifyRemainingPageSpace(yOffset, DEFAULT_LINE_HEIGHT);
        yOffset -= DEFAULT_LINE_HEIGHT;
        pdfstream.beginText();
        pdfstream.moveTextPositionByAmount(xpos, yOffset);
        pdfstream.setFont(MINOR_HEADING_FONT, MINOR_HEADING_FONT_SIZE);
        pdfstream.drawString(EXERCISE_DETAILS_NUM_SETS_HEADING);
        pdfstream.endText();
        pdfstream.beginText();
        pdfstream.moveTextPositionByAmount(xpos
                + MINOR_HEADING_FONT.getStringWidth(
                EXERCISE_DETAILS_NUM_SETS_HEADING) / 1000
                * MINOR_HEADING_FONT_SIZE, yOffset);
        pdfstream.setFont(DEFAULT_FONT, DEFAULT_FONT_SIZE);
        pdfstream.drawString(String.valueOf(exercise.getSets()));
        pdfstream.endText();

        if (exercise.getSide() != null) {
            yOffset = verifyRemainingPageSpace(yOffset, DEFAULT_LINE_HEIGHT);
            yOffset -= DEFAULT_LINE_HEIGHT;
            pdfstream.beginText();
            pdfstream.moveTextPositionByAmount(xpos, yOffset);
            pdfstream.setFont(MINOR_HEADING_FONT, MINOR_HEADING_FONT_SIZE);
            pdfstream.drawString(EXERCISE_DETAILS_SIDE_HEADING);
            pdfstream.endText();
            pdfstream.beginText();
            pdfstream.moveTextPositionByAmount(xpos
                    + MINOR_HEADING_FONT.getStringWidth(
                    EXERCISE_DETAILS_SIDE_HEADING) / 1000
                    * MINOR_HEADING_FONT_SIZE, yOffset);
            pdfstream.setFont(DEFAULT_FONT, DEFAULT_FONT_SIZE);
            pdfstream.drawString(exercise.getSide());
            pdfstream.endText();
        }

        yOffset = verifyRemainingPageSpace(yOffset, DEFAULT_LINE_HEIGHT);
        yOffset -= DEFAULT_LINE_HEIGHT;
        pdfstream.beginText();
        pdfstream.moveTextPositionByAmount(xpos, yOffset);
        pdfstream.setFont(MINOR_HEADING_FONT, MINOR_HEADING_FONT_SIZE);
        pdfstream.drawString(EXERCISE_DETAILS_REST_TIME_HEADING);
        pdfstream.endText();
        pdfstream.beginText();
        pdfstream.moveTextPositionByAmount(xpos
                + MINOR_HEADING_FONT.getStringWidth(
                EXERCISE_DETAILS_REST_TIME_HEADING) / 1000
                * MINOR_HEADING_FONT_SIZE, yOffset);
        pdfstream.setFont(DEFAULT_FONT, DEFAULT_FONT_SIZE);
        buff = new StringBuffer();
        buff.append(exercise.getRest());
        buff.append(" seconds between sets");
        pdfstream.drawString(buff.toString());
        pdfstream.endText();

        //if the number of pages has increased as a result of adding text to the
        //right of the image then use the current yOffset - otherwise, use the 
        //lowest yOffset - if the bottom of the caption text below image is lower 
        //than yOffset then reset yOffset to the bottom of the text
        if ((doc.getNumberOfPages() == numPages)
                && (imgTextBottomYOffset < yOffset)) {
            yOffset = imgTextBottomYOffset;
        }

        yOffset = verifyRemainingPageSpace(yOffset+41, INTERNAL_VERT_PAD);
        yOffset -= INTERNAL_VERT_PAD;

        String captions[] = {"None"};
        buildStatus = IMG_DESCR_TEXT_RENDER_FAILED;
        List<ExerciseMedia> exerciseMedia =
                libManager.getExerciseMedia(exercise.getExerciseId());

        if ((exerciseMedia != null) && (exerciseMedia.size() > 0)) {
            buff = new StringBuffer();
            int counter = 0;

            for (int indx = 0; indx < exerciseMedia.size(); indx++) {
                ExerciseMedia media = exerciseMedia.get(indx);

                if (media != null) {
                    String caption = media.getCaption();

                    if ((caption != null) && isValidCaption(caption)) {
                        buff.append(++counter);
                        buff.append(". ");
                        buff.append(caption);
                        buff.append(".\n");
                    }
                }
            }

            String str = buff.toString();
            captions = str.split("\n");
        }
        
        yOffset = verifyRemainingPageSpace(yOffset, DEFAULT_LINE_HEIGHT);
        yOffset -= DEFAULT_LINE_HEIGHT;
        pdfstream.beginText();
        pdfstream.moveTextPositionByAmount(xpos
                + MINOR_HEADING_FONT.getStringWidth(
                EXERCISE_DETAILS_REST_TIME_HEADING) / 180
                * MINOR_HEADING_FONT_SIZE, yOffset+56);
        pdfstream.setFont(MINOR_HEADING_FONT, MINOR_HEADING_FONT_SIZE);
        pdfstream.drawString("Description: ");
        buildStatus = EXERCISE_DESCR_SPLIT_LINES_FAILURE;
        captions =
                splitLines(captions, DEFAULT_FONT, 500 * (PAGE_WIDTH
                - 2 * INTERNAL_PAGE_MARGIN) / DEFAULT_FONT_SIZE);
        pdfstream.endText();

        if (captions != null) {
            for (int indx = 0; indx < captions.length; indx++) {
                if (captions[indx] != null) {
                    yOffset = 
                               verifyRemainingPageSpace(yOffset, DEFAULT_LINE_HEIGHT);
                    yOffset -= DEFAULT_LINE_HEIGHT;
                    
                    pdfstream.beginText();
                    pdfstream.setFont(DEFAULT_FONT, DEFAULT_FONT_SIZE);
//                    pdfstream.moveTextPositionByAmount(INTERNAL_PAGE_MARGIN,
//                            yOffset);
                    pdfstream.moveTextPositionByAmount(xpos
                            + MINOR_HEADING_FONT.getStringWidth(
                            EXERCISE_DETAILS_REST_TIME_HEADING) / 180
                            * MINOR_HEADING_FONT_SIZE, yOffset+55);
                    pdfstream.drawString(captions[indx]);
                    pdfstream.endText();
                }
            }
        }
        if(captions.length > 6){
        	yOffset = yOffset+50;
        }else{
        	yOffset = imageyOffset;
        	if(yOffset < 50){
        		yOffset = yOffset+755;
        	}
        }
        }
        
        return yOffset;
    }

    //this method verifies there is sufficient space on the current page
    //to add content. If there is sufficient space then the current 
    //y offset is returned. If there is insufficient space then this 
    //method creates a new page and sets the y offset to the top of the page
    //preconditions: pdfstream not null, incr >= 0
    private float verifyRemainingPageSpace(float yOffset,
            float incr) throws Exception {
        if ((yOffset - incr) < 38) {
            //create new page, add to document and create new stream
            pdfstream.close();
            PDPage page = new PDPage(PDPage.PAGE_SIZE_A4);
            doc.addPage(page);
            pdfstream = new PDPageContentStream(doc, page, true, false);

            //reset offset to top of page
            yOffset = PAGE_HEIGHT - 65;
        }

        return yOffset;
    }

    //this method splits each line in the input array to fit the specified width
    //preconditions: lines not null, lines.length > 0, font not null,
    //width in text units, NOT pixels 
    private String[] splitLines(String lines[],
            PDFont font,
            float width) throws Exception {
        String retval[] = null;
        Vector<String> wrkspce = new Vector<String>();

        for (int indx = 0; indx < lines.length; indx++) {
            if (lines[indx] != null) {
                wrkspce.addAll(splitLine(lines[indx], font, width));
            }
        }

        if (wrkspce.size() > 0) {
            retval = new String[wrkspce.size()];
            wrkspce.toArray(retval);
        }

        return retval;
    }

    //this method splits the line to fit the specified width. This method returns
    //the (potentially) split lines in an array containing 1 or more elements
    //preconditions: line not null, font not null
    private Vector<String> splitLine(String line,
            PDFont font,
            float width) throws Exception {
        Vector<String> wrkspce = new Vector<String>();

        //base case: line is shorter than width
        if (font.getStringWidth(line) <= width) {
            wrkspce.add(line);
        }
        else {
            //general case: split line into left and right parts, where left is
            //longest string less than width; then add left to wrkspce and 
            //recursively call splitLine on right string

            buildStatus = NULL_SPLITLINE_STRING_VAL;
            int splitIndx = line.length();

            //keep looping while we haven't reach the start of the string, the 
            //string is still longer that width or the string is shorter than
            //width but we haven't found a delim char (whitespace) yet
            while ((splitIndx > 0)
                    && ((font.getStringWidth(line.substring(0, splitIndx)) > width)
                    || ((font.getStringWidth(line.substring(0, splitIndx)) <= width)
                    && (line.charAt(splitIndx) != ' ')))) {
                splitIndx--;
            }

            if (splitIndx > 0) {
                String left = line.substring(0, splitIndx);
                wrkspce.add(left);
                String right = line.substring(splitIndx + 1);
                Vector<String> lines = splitLine(right, font, width);

                if ((lines != null) && (lines.size() > 0)) {
                    wrkspce.addAll(lines);
                }
            }
        }

        return wrkspce;
    }

    //preconditions: pdfstream not null, libManager not null
    private float addQuickNotes(float yOffset) throws Exception {
        buildStatus = QUICK_NOTES_TEXT_RENDER_FAILED;
        yOffset -= INTERNAL_VERT_PAD;
        String lines[] = {"None"};

        List<ProgramNote> notes = libManager.getNotes(programId);
        Vector<String> wrkspce = new Vector<String>();

        if ((notes != null) && (notes.size() > 0)) {
            for (int indx = 0; indx < notes.size(); indx++) {
                if (notes.get(indx) != null) {
                    String text = notes.get(indx).getNote();

                    if (text != null) {
                        lines = text.split("\n");

                        for (int offset = 0; offset < lines.length; offset++) {
                            wrkspce.add(lines[offset]);
                        }
                    }
                }
            }

            if (wrkspce.size() > 0) {
                lines = new String[wrkspce.size()];
                wrkspce.toArray(lines);
            }
        }

        yOffset = verifyRemainingPageSpace(yOffset, DEFAULT_LINE_HEIGHT);
        yOffset -= DEFAULT_LINE_HEIGHT;
        pdfstream.beginText();
        pdfstream.moveTextPositionByAmount(INTERNAL_PAGE_MARGIN, yOffset);
        pdfstream.setFont(MINOR_HEADING_FONT, MINOR_HEADING_FONT_SIZE);
        pdfstream.drawString("Quick Notes: ");
        lines = splitLines(lines, DEFAULT_FONT, 1000 * (PAGE_WIDTH
                - 2 * INTERNAL_PAGE_MARGIN) / DEFAULT_FONT_SIZE);
        pdfstream.endText();

        if (lines != null) {
            for (int indx = 0; indx < lines.length; indx++) {
                yOffset = verifyRemainingPageSpace(yOffset, DEFAULT_LINE_HEIGHT);
                yOffset -= DEFAULT_LINE_HEIGHT;
                pdfstream.beginText();
                pdfstream.setFont(DEFAULT_FONT, DEFAULT_FONT_SIZE);
                pdfstream.moveTextPositionByAmount(INTERNAL_PAGE_MARGIN, yOffset);
                pdfstream.drawString(lines[indx]);
                pdfstream.endText();
            }
        }

        return yOffset;
    }

    //title not null, info not null, practiceLogo not null, pdfstream not null 
    //fiizioLogo not null, img not null
    private void addExtraInfoPages(PDJpeg practiceLogo,
            PDJpeg fiizioLogo,
            PDJpeg img,
            String title,
            String info) throws Exception {
        String lines[] = {"None"};
        String wrkspce[] = info.split("\n");

        if ((wrkspce != null) && (wrkspce.length > 0)) {
            lines = wrkspce;
        }

        String remainingText = null;

        //create new page for extra info
        pdfstream.close();
        PDPage page = new PDPage(PDPage.PAGE_SIZE_A4);
        doc.addPage(page);
        pdfstream = new PDPageContentStream(doc, page, true, false);

        //add practice and myphsioapp logos
        float yOffset = PAGE_HEIGHT - INTERNAL_PAGE_MARGIN_EXTRAINFOPAGES;
        float aspectRatio =
                ((float) practiceLogo.getWidth()) / practiceLogo.getHeight();
        float imgWidth = PRACTICE_LOGO_HEIGHT * aspectRatio;
        
        buildStatus = PRACTICE_LOGO_XTRA_INFO_DRAW_FAILED;
        pdfstream.drawXObject(fiizioLogo, INTERNAL_PAGE_MARGIN-5,
                PRACTICE_LOGO_Y_POS-2, imgWidth, PRACTICE_LOGO_HEIGHT+15);

        aspectRatio =
                ((float) fiizioLogo.getWidth()) / fiizioLogo.getHeight();
        imgWidth = FIIZIO_APP_LOGO_HEIGHT * aspectRatio;
        float xpos = PAGE_WIDTH - INTERNAL_PAGE_MARGIN - imgWidth;
        buildStatus = FIIZIO_LOGO_XTRA_INFO_DRAW_FAILED;
        pdfstream.drawXObject(practiceLogo, xpos-25, FIIZIO_APP_LOGO_Y_POS,
                imgWidth+25, FIIZIO_APP_LOGO_HEIGHT+5);

        //draw the title
        buildStatus = XTRA_INFO_TXT_RENDER_FAILED;
        pdfstream.beginText();
        pdfstream.setFont(MAJOR_HEADING_FONT, MAJOR_HEADING_FONT_SIZE);
        //move down by the height of the practice logo plus the title height
        yOffset -= (PRACTICE_LOGO_HEIGHT + INTERNAL_VERT_PAD
                + MAJOR_HEADING_LINE_HEIGHT);
        pdfstream.moveTextPositionByAmount(INTERNAL_PAGE_MARGIN, yOffset);
        pdfstream.drawString(title);
        pdfstream.endText();

        if (img != null) {
            //render the image related to the page content - we need to 
            //dynamically set its height so that it is an integer number of lines (to 
            //ensure even spacing between lines on the page)
            buildStatus = XTRA_INFO_IMG_DRAW_FAILED;
            aspectRatio = ((float) img.getWidth()) / img.getHeight();
            int imgHeightInTextLines =
                    (int) Math.floor((XTRA_INFO_IMG_MAX_WIDTH / aspectRatio)
                    / DEFAULT_LINE_HEIGHT);

            //check that num text lines is valid - if not then set it to a valid
            //value
            imgHeightInTextLines =
                    imgHeightInTextLines < 1 ? 1 : imgHeightInTextLines;
            float imgHeight = imgHeightInTextLines * DEFAULT_LINE_HEIGHT;
            imgHeightInTextLines++;  //increment to add some white space below img
            imgWidth = imgHeight * aspectRatio;
            yOffset -= INTERNAL_VERT_PAD;
            pdfstream.drawXObject(img, INTERNAL_PAGE_MARGIN, yOffset - imgHeight,
                    imgWidth, imgHeight);
            float remainingWidth =
                    PAGE_WIDTH - 2 * INTERNAL_PAGE_MARGIN - imgWidth - INTERNAL_HORZ_PAD;
            lines =
                    splitLines(lines, DEFAULT_FONT, 1000 * remainingWidth / DEFAULT_FONT_SIZE);

            //it is fair to assume that the text to the right of the image will fit
            //on the current page so no need to check that it fits - if it doesn't 
            //then the image cannot fit on the page (in which case, all bets are 
            //off)

            if (lines != null) {
                buildStatus = XTRA_INFO_TXT_RENDER_FAILED;
                int numShortLinesToRender =
                        lines.length >= imgHeightInTextLines ? imgHeightInTextLines
                        : lines.length;

                if (imgHeightInTextLines < lines.length) {
                    //keep just the text after the last line of text to the right of 
                    //the image
                    remainingText =
                            info.substring(info.indexOf(lines[imgHeightInTextLines]));
                }

                //draw text to the right of the image
                for (int indx = 0; indx < numShortLinesToRender; indx++) {
                    if (lines[indx] != null) {
                        yOffset -= DEFAULT_LINE_HEIGHT;
                        pdfstream.beginText();
                        pdfstream.setFont(DEFAULT_FONT, DEFAULT_FONT_SIZE);
                        pdfstream.moveTextPositionByAmount(INTERNAL_PAGE_MARGIN
                                + imgWidth + INTERNAL_HORZ_PAD, yOffset);
                        pdfstream.drawString(lines[indx]);
                        pdfstream.endText();
                    }
                }
            }
        }

        if (remainingText != null) {
            //the remaining text fills in the entire width of the page (less 
            //the internal margins on both sides) - so we need to re-split the 
            //remaining text to fit - all of this text may not fit on the page 
            //so we need to check if each line will fit before we add it
            lines = remainingText.split("\n");

            //resplit lines using the entire page width
            lines = splitLines(lines, DEFAULT_FONT, 1000 * (PAGE_WIDTH
                    - 2 * INTERNAL_PAGE_MARGIN) / DEFAULT_FONT_SIZE);

            if (lines != null) {
                buildStatus = XTRA_INFO_TXT_RENDER_FAILED;

                //draw each line below the image
                for (int indx = 0; indx < lines.length; indx++) {
                    if (lines[indx] != null) {
                        yOffset =
                                verifyRemainingPageSpace(yOffset, DEFAULT_LINE_HEIGHT);
                        yOffset -= DEFAULT_LINE_HEIGHT;
                        pdfstream.beginText();
                        pdfstream.setFont(DEFAULT_FONT, DEFAULT_FONT_SIZE);
                        pdfstream.moveTextPositionByAmount(INTERNAL_PAGE_MARGIN,
                                yOffset);
                        pdfstream.drawString(lines[indx]);
                        pdfstream.endText();
                    }
                }
            }
        }
    }

    //classifies validity of caption
    //precondition: caption not null
    private boolean isValidCaption(String caption) {
        boolean retval = false;
        String words[] = caption.split(" ");

        if ((words != null) && (words.length > 0)) {
            for (int indx = 0; indx < words.length; indx++) {
                //a valid sentence will contain at least one word that
                //has more than one char (try writing a sensible sentence 
                //where this is not true) so once we have a word of length
                //> 1 we have a valid sentence. Note: we do not consider
                //the semantics of what the sentence is saying.
                if (words[indx].length() > 1) {
                    retval = true;
                    break;
                }
            }
        }

        return retval;
    }

    //returns a string containing the exception stack trace 
    //precondition: e not null
    private String getStackTrace(Exception e) {
        StringWriter sw = new StringWriter();
        e.printStackTrace(new PrintWriter(sw));
        return sw.toString();
    }
}
