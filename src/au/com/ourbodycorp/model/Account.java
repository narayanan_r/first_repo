package au.com.ourbodycorp.model;

public class Account implements Comparable {

  public enum Type {
    std("Standard"),
    cr("Credit"),
    db("Debit"),
    unt("Unit"),
    grp("Group"),
    mbr("Member"),
    bnk("Bank"),
    bdg("Budget Item");

    private String description;

    Type(String description) {
      this.description = description;
    }

    public String getDescription() {
      return description;
    }
  }

  private long corpId;
  private long id;
  private long parent;
  private Type type;
  private String name;
  private Balance balance;
  private int depth;
  private long unitId;

  public Account() {
  }

  public Account(long id, long corpId, long parent, Type type, String name) {
    this.corpId = corpId;
    this.id = id;
    this.parent = parent;
    this.type = type;
    this.name = name;
  }

  public Account(long id, long corpId, long parent, Type type, String name, Long unitId) {
    this.id = id;
    this.corpId = corpId;
    this.parent = parent;
    this.type = type;
    this.name = name;
    this.unitId = unitId;
  }

  public long getUnitId() {
    return unitId;
  }

  public void setUnitId(long unitId) {
    this.unitId = unitId;
  }

  public long getCorpId() {
    return corpId;
  }

  public void setCorpId(long corpId) {
    this.corpId = corpId;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public long getParent() {
    return parent;
  }

  public void setParent(long parent) {
    this.parent = parent;
  }

  public Type getType() {
    return type;
  }

  public void setType(Type type) {
    this.type = type;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Balance getBalance() {
    return balance;
  }

  public void setBalance(Balance balance) {
    this.balance = balance;
  }

  public int getDepth() {
    return depth;
  }

  public void setDepth(int depth) {
    this.depth = depth;
  }

  public int compareTo(Object o) {
    if (o instanceof Account) {
      Account a = (Account) o;
      return this.name.toLowerCase().compareTo(a.name.toLowerCase());
    }
    return 0;
  }
}
