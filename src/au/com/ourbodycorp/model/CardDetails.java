package au.com.ourbodycorp.model;

import java.util.Date;

public class CardDetails {
  private long id;
  private long tokencustomerid;
  private String accountno;
  private long month;
  private long year;
  private String card_type;
  private String name_on_card;
  private long corp_id;
  private long company;
  private long userid;
  private Date created;
  
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getAccountno() {
		return accountno;
	}
	public void setAccountno(String accountno) {
		this.accountno = accountno;
	}
	public long getMonth() {
		return month;
	}
	public void setMonth(long month) {
		this.month = month;
	}
	public long getYear() {
		return year;
	}
	public void setYear(long year) {
		this.year = year;
	}
	public String getCard_type() {
		return card_type;
	}
	public void setCard_type(String card_type) {
		this.card_type = card_type;
	}
	public String getName_on_card() {
		return name_on_card;
	}
	public void setName_on_card(String name_on_card) {
		this.name_on_card = name_on_card;
	}
	public long getCorp_id() {
		return corp_id;
	}
	public void setCorp_id(long corp_id) {
		this.corp_id = corp_id;
	}
	public long getCompany() {
		return company;
	}
	public void setCompany(long company) {
		this.company = company;
	}
	public long getUserid() {
		return userid;
	}
	public void setUserid(long userid) {
		this.userid = userid;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	public long getTokencustomerid() {
		return tokencustomerid;
	}
	public void setTokencustomerid(long tokencustomerid) {
		this.tokencustomerid = tokencustomerid;
	}
  
}

