package au.com.ourbodycorp.model;

import java.util.Date;
import java.util.List;

public class Company {
  private long id;
  private String name;
  private String contact;
  private String email;
  private String address1;
  private String address2;
  private String suburb;
  private String state;
  private String postcode;
  private String phone;
  private List<User> owners;
  private String country;
  private int maxCorp = 1;
  private int corpCount;
  private Date expiry;

  public Date getExpiry() {
    return expiry;
  }

  public void setExpiry(Date expiry) {
    this.expiry = expiry;
  }

  public int getMaxCorp() {
    return maxCorp;
  }

  public void setMaxCorp(int maxCorp) {
    this.maxCorp = maxCorp;
  }

  public int getCorpCount() {
    return corpCount;
  }

  public void setCorpCount(int corpCount) {
    this.corpCount = corpCount;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getContact() {
    return contact;
  }

  public void setContact(String contact) {
    this.contact = contact;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getAddress1() {
    return address1;
  }

  public void setAddress1(String address1) {
    this.address1 = address1;
  }

  public String getAddress2() {
    return address2;
  }

  public void setAddress2(String address2) {
    this.address2 = address2;
  }

  public String getSuburb() {
    return suburb;
  }

  public void setSuburb(String suburb) {
    this.suburb = suburb;
  }

  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public String getPostcode() {
    return postcode;
  }

  public void setPostcode(String postcode) {
    this.postcode = postcode;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public List<User> getOwners() {
    return owners;
  }

  public void setOwners(List<User> owners) {
    this.owners = owners;
  }
}
