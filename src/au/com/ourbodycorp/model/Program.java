package au.com.ourbodycorp.model;

import java.util.Date;

public class Program {

  public enum IntervalType {
    perDay("Times Per Day"),
    perWeek("Times Per Week"),
    hours("Every x hours")
    ;
    private String title;

    IntervalType(String title) {
      this.title = title;
    }

    public String getTitle() {
      return title;
    }
  }

  private long id;
  private long corpId;
  private long patientId;
  private long groupId;
  private String name;
  private String symptom;
  private String code;
  private IntervalType intervalType;
  private int interval;
  private Date created;
  private long creator;
  private Date retrieved;
  private String firstName;
  private String lastName;
  private Date lastUpdated;
  private boolean sharelocation;
  private boolean shareprogram;
  private boolean sharegroup;
  private long shareid;

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public long getCorpId() {
    return corpId;
  }

  public void setCorpId(long corpId) {
    this.corpId = corpId;
  }

  public long getPatientId() {
    return patientId;
  }

  public void setPatientId(long patientId) {
    this.patientId = patientId;
  }
  
  public long getGroupId() {
    return groupId;
  }

  public void setGroupId(long groupId) {
    this.groupId = groupId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSymptom() {
    return symptom;
  }

  public void setSymptom(String symptom) {
    this.symptom = symptom;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public IntervalType getIntervalType() {
    return intervalType;
  }

  public void setIntervalType(IntervalType intervalType) {
    this.intervalType = intervalType;
  }

  public void setIntervalType(String intervalType) {
    if (intervalType != null && intervalType.length() > 0) {
      setIntervalType(IntervalType.valueOf(intervalType));
    } else {
      this.intervalType = null;
    }
  }

  public int getInterval() {
    return interval;
  }

  public void setInterval(int interval) {
    this.interval = interval;
  }

  public Date getCreated() {
    return created;
  }

  public void setCreated(Date created) {
    this.created = created;
  }

  public long getCreator() {
    return creator;
  }

  public void setCreator(long creator) {
    this.creator = creator;
  }

  public Date getRetrieved() {
    return retrieved;
  }

  public void setRetrieved(Date retrieved) {
    this.retrieved = retrieved;
  }
  
  public void setLastUpdated(Date lastUpdated){
      this.lastUpdated = lastUpdated;
  }
  
  public Date getLastUpdated(){
      //since this field was added later in the project, in a lot of cases it might be null, so give the created date
      if (lastUpdated == null) return created;
      
      return lastUpdated;
  }

public boolean isSharelocation() {
	return sharelocation;
}

public void setSharelocation(boolean sharelocation) {
	this.sharelocation = sharelocation;
}

public boolean isShareprogram() {
	return shareprogram;
}

public void setShareprogram(boolean shareprogram) {
	this.shareprogram = shareprogram;
}

public boolean isSharegroup() {
	return sharegroup;
}

public void setSharegroup(boolean sharegroup) {
	this.sharegroup = sharegroup;
}

public long getShareid() {
	return shareid;
}

public void setShareid(long shareid) {
	this.shareid = shareid;
}

}
