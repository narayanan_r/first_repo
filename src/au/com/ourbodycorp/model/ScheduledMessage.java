package au.com.ourbodycorp.model;

import java.util.Date;


public class ScheduledMessage {
    
    private long id;
    private long corpId;
    private Date createdDate;
    private String headline;
    private String body;
    private String link;
    private long photoId;
    private int showAfterDays;
   
    public long getId(){
        return id;
    }
    
    public void setId(long id){
        this.id = id;
    }
    
    public String getHeadline(){
        return headline;
    }
    
    public void setHeadline(String headline){
        this.headline = headline;
    }
    
    public Date getCreatedDate(){
        return createdDate;
    }
    
    public void setCreatedDate(Date createdDate){
        this.createdDate = createdDate;
    }

    public long getCorpId() {
        return corpId;
    }

    public void setCorpId(long corpId) {
        this.corpId = corpId;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public long getPhotoId() {
        return photoId;
    }

    public void setPhotoId(long photoId) {
        this.photoId = photoId;
    }

    public int getShowAfterDays() {
        return showAfterDays;
    }

    public void setShowAfterDays(int showAfterDays) {
        this.showAfterDays = showAfterDays;
    }
   
}
