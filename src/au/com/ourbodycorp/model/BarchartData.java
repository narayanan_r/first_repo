
package au.com.ourbodycorp.model;

import java.util.Date;

public class BarchartData {
	private Long id;
	  private Integer duration;
	  Boolean finished;
	  private Integer lastProgram;
	  private Integer patientId;
	  private Integer programId;
	  Date dateDone;
	  private String programFreqType;
	  private Integer programFreqValue;
	  private String painLevel;
	  private Integer totalduration;
	  private Integer adherncescore;
	  private Integer frequencyscore;
	  private String title;
	  private String message;
	  private String note;
	  private Float painlevlAvg;
	  private Integer freqAvg;
	  private Integer adhenceAvg;
	  private Integer sumfreq;;
	  
	  
	  
	  
	  
	
	
	
	public Integer getSumfreq() {
		return sumfreq;
	}
	public void setSumfreq(Integer sumfreq) {
		this.sumfreq = sumfreq;
	}
	public Float getPainlevlAvg() {
		return painlevlAvg;
	}
	public void setPainlevlAvg(Float painlevlAvg) {
		this.painlevlAvg = painlevlAvg;
	}
	public Integer getFreqAvg() {
		return freqAvg;
	}
	public void setFreqAvg(Integer freqAvg) {
		this.freqAvg = freqAvg;
	}
	public Integer getAdhenceAvg() {
		return adhenceAvg;
	}
	public void setAdhenceAvg(Integer adhenceAvg) {
		this.adhenceAvg = adhenceAvg;
	}
	public String getNote() {
			return note;
		}
		public void setNote(String note) {
			this.note = note;
		}
	public Integer getFrequencyscore() {
		return frequencyscore;
	}
	public void setFrequencyscore(Integer frequencyscore) {
		this.frequencyscore = frequencyscore;
	}
	public String getProgramFreqType() {
		return programFreqType;
	}
	public void setProgramFreqType(String programFreqType) {
		this.programFreqType = programFreqType;
	}
	public Integer getProgramFreqValue() {
		return programFreqValue;
	}
	public void setProgramFreqValue(Integer programFreqValue) {
		this.programFreqValue = programFreqValue;
	}
	public Integer getAdherncescore() {
		return adherncescore;
	}
	public void setAdherncescore(Integer adherncescore) {
		this.adherncescore = adherncescore;
	}
	public Integer getTotalduration() {
		return totalduration;
	}
	public void setTotalduration(Integer totalduration) {
		this.totalduration = totalduration;
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Integer getDuration() {
		return duration;
	}
	public void setDuration(Integer duration) {
		this.duration = duration;
	}
	public Boolean getFinished() {
		return finished;
	}
	public void setFinished(Boolean finished) {
		this.finished = finished;
	}
	public Integer getLastProgram() {
		return lastProgram;
	}
	public void setLastProgram(Integer lastProgram) {
		this.lastProgram = lastProgram;
	}
	public Integer getPatientId() {
		return patientId;
	}
	public void setPatientId(Integer patientId) {
		this.patientId = patientId;
	}
	public Integer getProgramId() {
		return programId;
	}
	public void setProgramId(Integer programId) {
		this.programId = programId;
	}
	public Date getDateDone() {
		return dateDone;
	}
	public void setDateDone(Date dateDone) {
		this.dateDone = dateDone;
	}
	public String getPainLevel() {
		return painLevel;
	}
	public void setPainLevel(String painLevel) {
		this.painLevel = painLevel;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	  
	
	

}
