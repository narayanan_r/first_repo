package au.com.ourbodycorp.model;

public class Subscriber {
  private long memberId;
  private String email;
  private String unsubKey;
  private String firstName;

  public long getMemberId() {
    return memberId;
  }

  public void setMemberId(long memberId) {
    this.memberId = memberId;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getUnsubKey() {
    return unsubKey;
  }

  public void setUnsubKey(String unsubKey) {
    this.unsubKey = unsubKey;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }
}
