package au.com.ourbodycorp.model;

import java.util.Date;

public class CancelSubscription {
  private long id;
  private long rebill_customer_id;
  private long rebill_id;
  private Date order_date;
  private Date canceled_date;
  private String reason;
  private long corp_id;
  private long company;
  private long userid;
  private Date created;
  
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getRebill_customer_id() {
		return rebill_customer_id;
	}
	public void setRebill_customer_id(long rebill_customer_id) {
		this.rebill_customer_id = rebill_customer_id;
	}
	public long getRebill_id() {
		return rebill_id;
	}
	public void setRebill_id(long rebill_id) {
		this.rebill_id = rebill_id;
	}
	public Date getOrder_date() {
		return order_date;
	}
	public void setOrder_date(Date order_date) {
		this.order_date = order_date;
	}
	public Date getCanceled_date() {
		return canceled_date;
	}
	public void setCanceled_date(Date canceled_date) {
		this.canceled_date = canceled_date;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public long getCorp_id() {
		return corp_id;
	}
	public void setCorp_id(long corp_id) {
		this.corp_id = corp_id;
	}
	public long getCompany() {
		return company;
	}
	public void setCompany(long company) {
		this.company = company;
	}
	public long getUserid() {
		return userid;
	}
	public void setUserid(long userid) {
		this.userid = userid;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	
  
}

