package au.com.ourbodycorp.model;


import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class Unit {
  private long id;
  private Corporation corporation;
  private String name;
  private String address1;
  private String address2;
  private String state;
  private String postcode;
  private String country;
  private String countryName;
  private String suburb;
  private Date added;
  private List<UnitUser> owners = new LinkedList<UnitUser>();
  private String notes;
  private int uev;

  public String getNotes() {
    return notes;
  }

  public void setNotes(String notes) {
    this.notes = notes;
  }

  public int getUev() {
    return uev;
  }

  public void setUev(int uev) {
    this.uev = uev;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public Corporation getCorporation() {
    return corporation;
  }

  public void setCorporation(Corporation corporation) {
    this.corporation = corporation;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getAddress1() {
    return address1;
  }

  public void setAddress1(String address1) {
    this.address1 = address1;
  }

  public String getAddress2() {
    return address2;
  }

  public void setAddress2(String address2) {
    this.address2 = address2;
  }

  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public String getPostcode() {
    return postcode;
  }

  public void setPostcode(String postcode) {
    this.postcode = postcode;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public String getCountryName() {
    return countryName;
  }

  public void setCountryName(String countryName) {
    this.countryName = countryName;
  }

  public String getSuburb() {
    return suburb;
  }

  public void setSuburb(String suburb) {
    this.suburb = suburb;
  }

  public Date getAdded() {
    return added;
  }

  public void setAdded(Date added) {
    this.added = added;
  }

  public List<UnitUser> getOwners() {
    return owners;
  }

  public void setOwners(List<UnitUser> owners) {
    this.owners = owners;
  }

  public void addOwner(UnitUser user) {
    owners.add(user);
  }
}
