package au.com.ourbodycorp.model;

import java.util.Date;

public class Invitation {
  private String id;
  private String name;
  private Date sent;
  private Date expires;
  private Date accepted;
  private long acceptor;
  private String email;
  private long inviterId;
  private long corpId;
  private String roles[];
  private long units[];

  private User inviter;
  private Corporation corporation;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Date getSent() {
    return sent;
  }

  public void setSent(Date sent) {
    this.sent = sent;
  }

  public Date getExpires() {
    return expires;
  }

  public void setExpires(Date expires) {
    this.expires = expires;
  }

  public Date getAccepted() {
    return accepted;
  }

  public void setAccepted(Date accepted) {
    this.accepted = accepted;
  }

  public long getAcceptor() {
    return acceptor;
  }

  public void setAcceptor(long acceptor) {
    this.acceptor = acceptor;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public long getInviterId() {
    if (this.inviter != null) {
      return this.inviter.getId();
    } else {
      return inviterId;
    }
  }

  public void setInviterId(long inviterId) {
    this.inviterId = inviterId;
  }

  public long getCorpId() {
    if (this.corporation != null) {
      return corporation.getId();
    } else {
      return corpId;
    }
  }

  public void setCorpId(long corpId) {
    this.corpId = corpId;
  }

  public String[] getRoles() {
    return roles;
  }

  public void setRoles(String[] roles) {
    this.roles = roles;
  }

  public long[] getUnits() {
    return units;
  }

  public void setUnits(long[] units) {
    this.units = units;
  }

  public User getInviter() {
    return inviter;
  }

  public void setInviter(User inviter) {
    this.inviter = inviter;
  }

  public Corporation getCorporation() {
    return corporation;
  }

  public void setCorporation(Corporation corporation) {
    this.corporation = corporation;
  }
}
