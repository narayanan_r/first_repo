package au.com.ourbodycorp.model;

import java.util.Date;

public class Subscription {
	
	private long subscriptionId;
	private String subscription;
	private long subscriptionPeriod;
	private String subPeriodType;
	private long corporation;
	private float subscriptionClinicAmt;
	private float subscriptionPatientAmt;
	private boolean shareSubscription;
	private long creator;
	private long sharesubscriptionid;
	private boolean active;
	private Date planineffectivedate;
	private Date created;
	private String currencyType;
	
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public long getSharesubscriptionid() {
		return sharesubscriptionid;
	}
	public void setSharesubscriptionid(long sharesubscriptionid) {
		this.sharesubscriptionid = sharesubscriptionid;
	}
	public long getCreator() {
		return creator;
	}
	public void setCreator(long creator) {
		this.creator = creator;
	}
	public long getSubscriptionId() {
		return subscriptionId;
	}
	public void setSubscriptionId(long subscriptionId) {
		this.subscriptionId = subscriptionId;
	}
	public String getSubscription() {
		return subscription;
	}
	public void setSubscription(String subscription) {
		this.subscription = subscription;
	}
	public long getSubscriptionPeriod() {
		return subscriptionPeriod;
	}
	public void setSubscriptionPeriod(long subscriptionPeriod) {
		this.subscriptionPeriod = subscriptionPeriod;
	}
	public long getCorporation() {
		return corporation;
	}
	public void setCorporation(long corporation) {
		this.corporation = corporation;
	}
	public String getSubPeriodType() {
		return subPeriodType;
	}
	public void setSubPeriodType(String subPeriodType) {
		this.subPeriodType = subPeriodType;
	}
	public float getSubscriptionClinicAmt() {
		return subscriptionClinicAmt;
	}
	public void setSubscriptionClinicAmt(float subscriptionClinicAmt) {
		this.subscriptionClinicAmt = subscriptionClinicAmt;
	}
	public float getSubscriptionPatientAmt() {
		return subscriptionPatientAmt;
	}
	public void setSubscriptionPatientAmt(float subscriptionPatientAmt) {
		this.subscriptionPatientAmt = subscriptionPatientAmt;
	}
	public boolean isShareSubscription() {
		return shareSubscription;
	}
	public void setShareSubscription(boolean shareSubscription) {
		this.shareSubscription = shareSubscription;
	}
	public Date getPlanineffectivedate() {
		return planineffectivedate;
	}
	public void setPlanineffectivedate(Date planineffectivedate) {
		this.planineffectivedate = planineffectivedate;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	public String getCurrencyType() {
		return currencyType;
	}
	public void setCurrencyType(String currencyType) {
		this.currencyType = currencyType;
	}

}
