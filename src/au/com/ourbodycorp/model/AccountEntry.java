package au.com.ourbodycorp.model;

import java.math.BigDecimal;
import java.util.Date;

public class AccountEntry {
  private long id;
  private long accountId;
  private int year;
  private Date entryDate;
  private Date effectiveDate;
  private String description;
  private BigDecimal amount;
  private long budgetItem;

  public long getBudgetItem() {
    return budgetItem;
  }

  public void setBudgetItem(long budgetItem) {
    this.budgetItem = budgetItem;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public long getAccountId() {
    return accountId;
  }

  public void setAccountId(long accountId) {
    this.accountId = accountId;
  }

  public int getYear() {
    return year;
  }

  public void setYear(int year) {
    this.year = year;
  }

  public Date getEntryDate() {
    return entryDate;
  }

  public void setEntryDate(Date entryDate) {
    this.entryDate = entryDate;
  }

  public Date getEffectiveDate() {
    return effectiveDate;
  }

  public void setEffectiveDate(Date effectiveDate) {
    this.effectiveDate = effectiveDate;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public BigDecimal getAmount() {
    return amount;
  }

  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }
}
