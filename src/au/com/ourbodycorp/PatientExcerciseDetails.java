package au.com.ourbodycorp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;

import au.com.ourbodycorp.model.ProgramExercise;
import au.com.ourbodycorp.model.managers.LibraryManager;

public class PatientExcerciseDetails  extends HttpServlet {

  /**
  * 
  */
 private static final long serialVersionUID = 1L;

 protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	 LibraryManager lm=new LibraryManager();
	 
	  StringBuffer jb = new StringBuffer();
	  String line = null;
	  try {
	    BufferedReader reader = request.getReader();
	    while ((line = reader.readLine()) != null)
	      jb.append(line);
	  } catch (Exception e) { /*report an error*/ }

	  try {
	    JSONObject jsonObject = new JSONObject(jb.toString());
	
	    String exerciseId = jsonObject.getString("exerciseId");
    	String sets = jsonObject.getString("sets");
    	String reps = jsonObject.getString("reps");
    	String time = jsonObject.getString("time");
    	String repTime = jsonObject.getString("repTime");
    	String setRestTime = jsonObject.getString("setRestTime");
    	String seq = jsonObject.getString("seq");
    	String programId = jsonObject.getString("programId");
    	String maxSets = jsonObject.getString("maxSets");
    	String maxReps = jsonObject.getString("maxReps");
    	ProgramExercise p= lm.updateProgramExercise(exerciseId,sets,reps,time,repTime,setRestTime,seq,programId,maxSets,maxReps);
    	response.setStatus(200);
 	    response.setContentType("application/json");
 	    PrintWriter out = response.getWriter();
 		out.print(new Gson().toJson(p));
 		out.flush();
	  } catch (JSONException e) {
	    // crash and burn
	    throw new IOException("Error parsing JSON request string");
	  } 
	  catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	}
 
 
}