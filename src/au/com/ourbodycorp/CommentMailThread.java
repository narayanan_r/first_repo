package au.com.ourbodycorp;

import au.com.ourbodycorp.model.Comment;
import au.com.ourbodycorp.model.Post;
import au.com.ourbodycorp.model.Subscriber;
import au.com.ourbodycorp.model.User;
import au.com.ourbodycorp.model.managers.PostManager;
import au.com.ourbodycorp.model.managers.UserManager;

import java.util.List;
import java.util.Properties;

public class CommentMailThread extends Thread {

  Comment comment;

  public CommentMailThread(Comment comment) {
    this.comment = comment;
  }

  @Override
  public void run() {
    try {
      PostManager pm = new PostManager();
      UserManager mm = new UserManager();
      Post p = pm.getPost(comment.getPostId());
      List<Subscriber> subs = pm.getSubscribers(p.getId());
      User m = mm.getUser(comment.getAuthor());
      Properties props = new Properties();
      props.put("title", p.getTitle());
      props.put("author", m.getFirstName() + " " + m.getLastName());
      props.put("comment", comment.getComment());
      props.put("url", Config.getString("url") + p.getUrl());
      for (Subscriber sub : subs) {
        if (sub.getMemberId() == comment.getAuthor()) {
          continue;
        }
        props.put("firstName", sub.getFirstName());
        props.put("unsub", Config.getString("url") + "/comment?unsub=" + sub.getUnsubKey());
        MailMessage msg = new MailMessage(props, "comment.vm", sub.getEmail(), "New comment for \"" + p.getTitle() + "\"",true);
        try {
          msg.send();
        } catch (Exception e) {
          System.out.println("ERROR sending comment email to " + sub.getEmail());
        }
      }

    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
