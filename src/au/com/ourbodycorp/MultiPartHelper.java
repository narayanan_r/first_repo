package au.com.ourbodycorp;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class MultiPartHelper {
  private HashMap<String, LinkedList<Object>> values;

  public MultiPartHelper(HttpServletRequest request) throws Exception {
    parse(request);
  }

  private void parse(HttpServletRequest request) throws Exception {

    values = new HashMap<String, LinkedList<Object>>();
    FileItemFactory factory = new DiskFileItemFactory();
    ServletFileUpload upload = new ServletFileUpload(factory);
    List /* FileItem */ items = upload.parseRequest(request);

    for (Object i : items) {
      FileItem item = (FileItem) i;
      LinkedList<Object> list = values.get(item.getFieldName());
      if (list == null) {
        list = new LinkedList<Object>();
        values.put(item.getFieldName(), list);
      }
      if (item.isFormField()) {
        list.add(item.getString());
      } else {
        list.add(item);
      }
    }
  }

  public String getParameter(String name) {
    LinkedList<Object> list = values.get(name);
    if (list != null) {
      return (String) list.getFirst();
    } else {
      return null;
    }
  }

  public FileItem getFileItem(String name) {
    LinkedList<Object> list = values.get(name);
    if (list != null) {
      return (FileItem) list.getFirst();
    } else {
      return null;
    }
  }
}
