package au.com.ourbodycorp;

import au.com.ourbodycorp.model.Company;
import au.com.ourbodycorp.model.Corporation;
import au.com.ourbodycorp.model.CorporationUser;
import au.com.ourbodycorp.model.Patient;
import au.com.ourbodycorp.model.Program;
import au.com.ourbodycorp.model.User;
import au.com.ourbodycorp.model.managers.CorporationManager;
import au.com.ourbodycorp.model.managers.LibraryManager;
import au.com.ourbodycorp.model.managers.UserManager;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class ExportCorporationsServlet extends HttpServlet{

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            response.setHeader("Content-Disposition", "attachment; filename=\"practices.csv\"");
            response.setContentType("text/csv");
            
            //find the required corporations
            CorporationManager cm = new CorporationManager();
            UserManager userManager = new UserManager();
            String type = Util.notNull(request.getParameter("type"), "all");
            List<Corporation> corporations;
            if (type.equalsIgnoreCase("all")) {
                corporations = cm.getAllCorporations();
            } 
            else if (type.equalsIgnoreCase("currentTrial")) {
                corporations = cm.getAllActiveTrialCorporations();
            } 
            else if (type.equalsIgnoreCase("expiredTrial")) {
                corporations = cm.getExpiredCorporations();
            }
            else{
                corporations = cm.getCorporationsOfType(type);
            }
            
            //output as CSV file
            SimpleDateFormat sdf = new SimpleDateFormat("d MMM yy");
            LibraryManager libraryManager = new LibraryManager();
            response.getOutputStream().println("Name,Type,Created,Contact,Phone,Email,Address1,Address2,Suburb,State,Postcode,Country,Trial progress,Program downloaded");
            for (Corporation corp : corporations){
                response.getOutputStream().print("\""+corp.getName() + "\",");
                response.getOutputStream().print(corp.getSubType() + ",");
                response.getOutputStream().print((corp.getCreated()== null)? "," : sdf.format(corp.getCreated()) + ",");
                
                Company company = libraryManager.getCompany(corp.getCompanyId());
                response.getOutputStream().print("\""+ company.getContact()+ "\",");
                response.getOutputStream().print("\""+ company.getPhone() + "\",");
                response.getOutputStream().print("\""+ company.getEmail()+ "\",");
                response.getOutputStream().print("\""+ company.getAddress1() + "\",");
                response.getOutputStream().print("\""+ company.getAddress2() + "\",");
                response.getOutputStream().print("\""+ company.getSuburb()+ "\",");
                response.getOutputStream().print("\""+ company.getState()+ "\",");
                response.getOutputStream().print("\""+ company.getPostcode()+ "\",");
                response.getOutputStream().print("\""+ company.getCountry()+ "\",");
                
                List<CorporationUser> users = cm.getUsersForCorporation(corp.getId());
                User firstUser = (users == null || users.size() == 0)? null : users.get(0).getUser(); 
                int tutorialProgress = (firstUser == null)? UserManager.TUTORIAL_NOT_DONE :  userManager.getTutorialProgressForUser(firstUser.getId()); 
                String progress = "N/A";
                if (UserManager.TUTORIAL_NOT_DONE == tutorialProgress){
                    progress = "Tutorial Not Started";
                }
                else if (UserManager.TUTORIAL_WENT_TO_PATIENT_PAGE == tutorialProgress){
                    progress = "Tutorial Part 2";
                }
                else if (UserManager.TUTORIAL_WENT_TO_PROGRAM_PAGE == tutorialProgress){
                    progress = "Tutorial Part 3";
                }
                else if (UserManager.TUTORIAL_FINISHED == tutorialProgress){
                    progress = "Tutorial Finished";
                }
                response.getOutputStream().print(progress + ",");

                String programDownloadDate = "Never";
                List<Patient> patients = libraryManager.getPatients(corp.getId(), 0, 1);
                Patient firstPatient = (patients != null && patients.size() > 0)? patients.get(0) : null;
                if (firstPatient != null){
                     List<Program> programs = libraryManager.getPrograms(firstPatient.getId());
                     if (programs != null && programs.size() > 0){
                         Program firstProgram = programs.get(0);
                         if (firstProgram.getRetrieved() != null){
                             programDownloadDate = sdf.format(firstProgram.getRetrieved());
                         }
                     }
                }
                response.getOutputStream().print(programDownloadDate + "");
                
                response.getOutputStream().println();
            }
        }
        catch (Exception e) {
            throw new ServletException(e);
        }
    }
}
