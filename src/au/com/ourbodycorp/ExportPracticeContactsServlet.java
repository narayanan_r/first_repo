/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package au.com.ourbodycorp;

import au.com.ourbodycorp.model.Company;
import au.com.ourbodycorp.model.Corporation;
import au.com.ourbodycorp.model.CorporationUser;
import au.com.ourbodycorp.model.managers.CorporationManager;
import au.com.ourbodycorp.model.managers.LibraryManager;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ExportPracticeContactsServlet extends HttpServlet{

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            response.setHeader("Content-Disposition", "attachment; filename=\"practice_contacts.csv\"");
            response.setContentType("text/csv");
            
            LibraryManager libraryManager = new LibraryManager();
            List<Company> companies = libraryManager.listCompanies(0, 0);
            
            CorporationManager corporationManager = new CorporationManager();

            //output as CSV file
            SimpleDateFormat sdf = new SimpleDateFormat("d MMM yy");
            response.getOutputStream().println("Name,Email,Practice");
            for (Company company : companies){
                List<Corporation> corporationsInCompany = corporationManager.getCorporationsForCompany(company.getId());
                for (Corporation corp : corporationsInCompany){
                    //primary contact - seems like these are counted as users too, so it only doubles up if you print them as well
//                    response.getOutputStream().print("\""+ company.getContact()+ "\",");
//                    response.getOutputStream().print("\""+ company.getEmail()+ "\",");
//                    response.getOutputStream().print("\""+ corp.getName() + "\",");
//                    response.getOutputStream().println();
                    
                    List<CorporationUser> users = corporationManager.getUsersForCorporation(corp.getId());
                    if (users == null) continue;

                    for (CorporationUser user : users){
                        response.getOutputStream().print("\""+ user.getUser().getFullName() + "\",");
                        response.getOutputStream().print("\""+ user.getUser().getEmail()+ "\",");
                        response.getOutputStream().print("\""+ corp.getName() + "\",");
                        response.getOutputStream().println();
                    }
                }
                        
                
            }
        }
        catch (Exception e) {
            throw new ServletException(e);
        }
    }
}
