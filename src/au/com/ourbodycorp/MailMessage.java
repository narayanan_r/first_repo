package au.com.ourbodycorp;

import au.com.ourbodycorp.model.Constants;
import java.io.StringWriter;
import java.net.URL;
import java.util.Date;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.URLDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.exception.ParseErrorException;
import org.apache.velocity.exception.ResourceNotFoundException;

public class MailMessage {
	private Properties vars;
	private String template;
	private String recipient;
	private String subject;
	private String from;
	private  String support; 
	private boolean info;

	public MailMessage(Properties vars, String template, String recipient,
			String subject, String from, boolean info) {
		this.vars = vars;
		this.template = template;
		this.recipient = recipient;
		this.subject = subject;
		this.from = from;
		this.info=info;
	}

	public MailMessage(Properties vars, String template, String recipient,
			String subject, boolean info) {
		this.vars = vars;
		this.template = template;
		this.recipient = recipient;
		this.subject = subject;
		this.info=info;
	}

	public void send() {
		
		System.out.println("INSIDE SEND MAIL METHOD========>");
		final String username = "info@fiizio.com";
		final String password = "FZFT1234";

		Properties props = new Properties();
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.host", "smtp.mailgun.org");
		props.put("mail.smtp.port", "2525");

		Session session = Session.getInstance(props,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
					//return new PasswordAuthentication(username, password);
						 return new PasswordAuthentication("postmaster@fiizio.com", "c21a1f0bd3ef1c19172bc04eabb51a2e");
					}
				});
		Message m = null;
		try {
			m = new MimeMessage(session);
			if (this.from != null) {
				if(info)
				{
				m.setFrom(new InternetAddress(this.from));
				}
				else 
				{
					m.setFrom(new InternetAddress(this.from));
				}
			} else {
				if(info)
				{
					m.setFrom(new InternetAddress(Config
							.getString("email.admin.from")));
				}
				else 
				{
					m.setFrom(new InternetAddress(Config
							.getString("email.support.from")));
				}
				
			}
			m.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(this.recipient, false));
			m.setSubject(this.subject);
			m.setSentDate(new Date());
		} catch (AddressException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		}

		VelocityContext ctx = new VelocityContext();
		for (Object key : this.vars.keySet()) {
			String name = (String) key;
			ctx.put(name, this.vars.getProperty(name));
		}
		Template tpl;
		try {
			tpl = Velocity.getTemplate(this.template);
			StringWriter sw = new StringWriter();
			tpl.merge(ctx, sw);

			String body = sw.toString();
			if (body.contains("<html>")) {
				m.setContent(sw.toString(), "text/html; charset=\"UTF-8\"");
			} else {
				m.setContent(sw.toString(), "text/plain; charset=\"UTF-8\"");
			}
			if (Constants.TESTING_MODE) {
				System.out.println("Would have sent " + this.recipient
						+ " this message:\n" + sw.toString());
			} else {
				Transport.send(m);
			}
		} catch (ResourceNotFoundException e) {
			e.printStackTrace();
		} catch (ParseErrorException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void sendWithAttachment(String url, String filename)
			throws Exception {
		
		System.out.println("INSIDE sendWithAttachment MAIL METHOD========>");
	//	final String username = "info@fiizio.com";
	//	final String password = "FZFT1234";

		Properties props = new Properties();
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.host", "smtp.mailgun.org");
		props.put("mail.smtp.port", "2525");

		Session session = Session.getInstance(props,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						//return new PasswordAuthentication(username, password);
						 return new PasswordAuthentication("postmaster@fiizio.com", "c21a1f0bd3ef1c19172bc04eabb51a2e");
					}
				});

		Message m = new MimeMessage(session);
		if (this.from != null) {
			m.setFrom(new InternetAddress(this.from));
		} else {
			m.setFrom(new InternetAddress(Config.getString("email.admin.from")));
		}
		m.setRecipients(Message.RecipientType.TO,
				InternetAddress.parse(this.recipient, false));
		m.setSubject(this.subject);
		m.setSentDate(new Date());

		VelocityContext ctx = new VelocityContext();
		for (Object key : this.vars.keySet()) {
			String name = (String) key;
			ctx.put(name, this.vars.getProperty(name));
		}
		Template tpl = Velocity.getTemplate(this.template);
		StringWriter sw = new StringWriter();
		tpl.merge(ctx, sw);

		Multipart mp = new MimeMultipart();
		BodyPart bp = new MimeBodyPart();

		String body = sw.toString();
		if (body.contains("<html>")) {
			bp.setContent(sw.toString(), "text/html; charset=\"UTF-8\"");
		} else {
			bp.setContent(sw.toString(), "text/plain; charset=\"UTF-8\"");
		}
		mp.addBodyPart(bp);

		bp = new MimeBodyPart();
		DataSource source = new URLDataSource(new URL(url));
		bp.setDataHandler(new DataHandler(source));
		bp.setFileName(filename);
		mp.addBodyPart(bp);

		m.setContent(mp);

		Transport.send(m);
	}
}
