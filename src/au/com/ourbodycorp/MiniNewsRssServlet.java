package au.com.ourbodycorp;

import au.com.ourbodycorp.model.Corporation;
import au.com.ourbodycorp.model.MiniNews;
import au.com.ourbodycorp.model.managers.CorporationManager;
import au.com.ourbodycorp.model.managers.MiniNewsManager;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

public class MiniNewsRssServlet extends HttpServlet {
  SimpleDateFormat df = new SimpleDateFormat("EEE', 'dd' 'MMM' 'yyyy' 'HH:mm:ss' 'Z", Locale.US);

  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {



    if (request.getParameter("corp") == null) {
      response.sendError(400, "No corp specified");
      return;
    }

    try {

      CorporationManager cm = new CorporationManager();
      Corporation corp = cm.getCorporation(request.getParameter("corp"));
      if (corp == null) {
        response.sendError(404, "No such corp");
        return;
      }

      MiniNewsManager mn = new MiniNewsManager();
      List<MiniNews> posts =  mn.listNews(corp.getId(), 20, 0, "published");


      Document doc = DocumentHelper.createDocument();
      Element root = doc.addElement("rss");
      root.addAttribute("version", "2.0");
      Element channel = root.addElement("channel");
      channel.addElement("title").addText(corp.getName());
      channel.addElement("link").addText(corp.getMetaString("www"));

      for (MiniNews post : posts) {
        Element item = channel.addElement("item");
        item.addElement("title").addText(post.getHeadline());
        item.addElement("link").addText(Config.getString("url") + "/news.jsp?id=" + post.getId());
        item.addElement("guid").addText(Config.getString("url") + "/news.jsp?id=" + post.getId());
        item.addElement("pubDate").addText(df.format(post.getCreated()));
      }

      StringWriter sw = new StringWriter();
      OutputFormat format = OutputFormat.createPrettyPrint();
      XMLWriter writer = new XMLWriter(sw, format);
      writer.write(doc);

      byte[] bytes = sw.toString().getBytes("UTF-8");

      response.setContentType("application/rss+xml");
      response.setContentLength(bytes.length);
      response.getOutputStream().write(bytes);

    } catch (Exception e) {
      throw new ServletException(e);
    }
  }
}
