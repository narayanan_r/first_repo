package au.com.ourbodycorp;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import au.com.ourbodycorp.model.managers.LibraryManager;

public class MobileSubscription  extends HttpServlet {

  /**
  * 
  */
 private static final long serialVersionUID = 1L;

 /*protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

     try {
       String email = request.getParameter("username");
       if (StringUtils.isBlank(email)) {
         response.sendError(400, "No username provided");
         return;
       }
       LibraryManager lm = new LibraryManager();
       List<String> status=lm.checkPatinetSubscriptionIsVaild(email);
       LinkedHashMap<String, Object> main = new LinkedHashMap<String, Object>();
       String	user="false";
       String	subscription="false";
       String	title="";
       String	message="";
       
       if(status!=null && (status.get(0).equals("true"))){
    	   user="true"; //user set as true
    	   if(status.get(1).equals("0")||status.get(1).equals("1")||status.get(2).equals("false")){
    		   if(status.get(1).equals("0")){
    			   title="You don�t have a valid subscription for using the Fiizio App";
    			   message="Please contact your clinic to subscribe.";
    		   }else{
    			   title="Your Fiizio subscription has expired";
    			   message="Please contact your clinic to renew your subscription.";
    		   }
    	   }else{
    		   subscription="true"; //subscription set as true
    	   }
       }
       main.put("user",user);
	   main.put("subscription",subscription);
	   main.put("title", title);
	   main.put("message", message);
       Gson gson = new GsonBuilder().setPrettyPrinting().create();
       String json = gson.toJson(main);
       response.setContentType("application/json;charset=utf-8");
       byte[] out = json.getBytes("UTF-8");
       response.setContentLength(out.length);
       response.getOutputStream().write(out);
     } catch (Exception e) {
       throw new ServletException(e);
     }
   }*/
 
 protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

     try {
       String email = request.getParameter("username");
       if (StringUtils.isBlank(email)) {
         response.sendError(400, "No username provided");
         return;
       }
       LibraryManager lm = new LibraryManager();
       Map<String,Boolean> map=lm.checkPatientPaymentSubscriptionIsVaild(email);
       LinkedHashMap<String, Object> main = new LinkedHashMap<String, Object>();
       String	user="false";
       String	subscription="false";
       String	title="No  Subscription";
       String	message="No Subscription";
       
       if(map.get("user")!=null){
       boolean userVal=map.get("user");
       if(userVal){
    	   user="true";
       }
       }
       if(map.get("subscription")!=null){
       boolean subscriptionVal=map.get("subscription");
       if(subscriptionVal){
		   subscription="true"; //subscription set as true
		   title="Your in valid subscription period";
		   message="Your in valid subscription period once your subscription expired please subscribe mobile app";
	   }
       }
       if(map.get("NoSubscription")!=null){
       boolean noSubVal=map.get("NoSubscription");
       if(noSubVal){
    	   subscription="false";
		   title="No Subscription";
		   message="No Subscription";
	   }
       }
      /* if(map.get("Expired")!=null){
       boolean expiredVal=map.get("Expired");
       if(expiredVal){
    	   subscription="false";
		   title="No Active Subscription";
		   message="Your subscription plan has expired.To keep using Fiizio, please choose a plan to reactivate .";
	   }
       }*/
       if(map.get("TrailExpired")!=null){
           boolean expiredVal=map.get("TrailExpired");
           if(expiredVal){
        	   subscription="false";
    		   title="Your Free�Trial�Has Expired";
    		   message="You don�t have any active subscription. To keep using Fiizio, please choose a subscription plan below to reactivate the service";
    	   }
           }
       if(map.get("oneMonthExpired")!=null){
           boolean expiredVal=map.get("oneMonthExpired");
           if(expiredVal){
        	   subscription="false";
        	   title="Your 1 Month Period Has Expired";
    		   message="You don�t have any active subscription. To keep using Fiizio, please choose a subscription plan below to reactivate the service";
    	   }
           }
       
       if(map.get("threeMonthExpired")!=null){
           boolean expiredVal=map.get("threeMonthExpired");
           if(expiredVal){
        	   subscription="false";
        	   title="Your 3 Months Period Has Expired";
    		   message="You don�t have any active subscription. To keep using Fiizio, please choose a subscription plan below to reactivate the service";
    	   }
           }
       if(map.get("sixMonthExpired")!=null){
           boolean expiredVal=map.get("threeMonthExpired");
           if(expiredVal){
        	   subscription="false";
        	   title="Your 6 Months Period Has Expired";
    		   message="You don�t have any active subscription. To keep using Fiizio, please choose a subscription plan below to reactivate the service";
    	   }
           }
       
       if(user=="false"){
    	   subscription="false";
		   title="No Subscription";
		   message="No Subscription";
	   }
       main.put("user",user);
	   main.put("subscription",subscription);
	   main.put("title", title);
	   main.put("message", message);
       Gson gson = new GsonBuilder().setPrettyPrinting().create();
       String json = gson.toJson(main);
       response.setContentType("application/json;charset=utf-8");
       byte[] out = json.getBytes("UTF-8");
       response.setContentLength(out.length);
       response.getOutputStream().write(out);
     } catch (Exception e) {
       throw new ServletException(e);
     }
   }
}