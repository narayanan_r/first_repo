package au.com.ourbodycorp;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class SecureUsersFilter implements Filter {
  public void destroy() {
  }

  public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain) throws ServletException, IOException {
    try {
      HttpServletRequest req = (HttpServletRequest) servletRequest;
      HttpServletResponse resp = (HttpServletResponse) servletResponse;
      if (Util.makeSecure(req, resp)) {
        return;
      }
    } catch (Exception e) {
      throw new ServletException(e);
    }
    chain.doFilter(servletRequest, servletResponse);
  }

  public void init(FilterConfig config) throws ServletException {

  }

}
