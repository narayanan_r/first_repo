package au.com.ourbodycorp;

import au.com.ourbodycorp.model.managers.UserManager;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: bas
 * Date: 17/07/12
 * Time: 9:54 AM
 * To change this template use File | Settings | File Templates.
 */
public class ExportEmailServlet extends HttpServlet {
  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

  }

  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    try {
      response.setHeader("Content-Disposition", "attachment; filename=\"all_email.csv\"");
      response.setContentType("text/csv");
      UserManager um = new UserManager();
      List<String> email = um.getAllEmail();
      for (String s : email) {
          response.getOutputStream().println(s);
      }
    } catch (Exception e) {
      throw new ServletException(e);
    }
  }
}
