package au.com.ourbodycorp;

import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;



public class SMSBroadcastClient {

  public String sendSimple(String rcpt, String sender, String message) throws Exception {
    String gs = String.format("http://www.smsbroadcast.com.au/api.php");
    HttpClient hc = new DefaultHttpClient();
    HttpPost post = new HttpPost(gs);
    ResponseHandler<String> rh = new BasicResponseHandler();

    List <NameValuePair> nvps = new ArrayList <NameValuePair>();
    nvps.add(new BasicNameValuePair("username", Config.getString("sms.user")));
    nvps.add(new BasicNameValuePair("password", Config.getString("sms.password")));
    nvps.add(new BasicNameValuePair("from", sender));
    nvps.add(new BasicNameValuePair("to", rcpt));
    nvps.add(new BasicNameValuePair("message", message));
    post.setEntity(new UrlEncodedFormEntity(nvps, "UTF-8"));

    String body = hc.execute(post, rh);
    return body;
  }
}
