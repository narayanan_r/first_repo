//package au.com.ourbodycorp;
//
//
//import javax.naming.InitialContext;
//import javax.sql.DataSource;
//import java.sql.Connection;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.sql.Statement;
//import java.util.Properties;
//import org.postgresql.ds.*;
//
//public class Config {
//    private static Config ourInstance = new Config();
//    private Properties props = new Properties();
//
//  public static Config getInstance() {
//        return ourInstance;
//    }
//
//    private Config() {
//        try {
//            InitialContext ic = new InitialContext();
////            DEPLOYMENT VERSION
////            (DataSource) ic.lookup("jdbc/WebDS"); 
//            
////            LOCAL DEV VERSION
//            PGPoolingDataSource ds = new PGPoolingDataSource();
//            ds.setDataSourceName("MyChiro");
//            ds.setServerName("localhost");
//            ds.setDatabaseName("mychiro");
//            ds.setUser("postgres");
//            ds.setPassword("testing");
//            ds.setMaxConnections(10);
//            
//            Connection conn = ds.getConnection();
//            Statement st = conn.createStatement();
//            ResultSet rs = st.executeQuery("select * from config");
//            while (rs.next()) {
//                props.put(rs.getString("name"), rs.getString("value"));
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//  public void setString(String key, String value) throws Exception {
//    Connection conn = Util.getConnection();
//    try {
//      PreparedStatement pst;
//      if (props.containsKey(key)) {
//        pst = conn.prepareStatement("update config set value =? where name = ?");
//      } else {
//        pst = conn.prepareStatement("insert into config (value, name) values (?, ?)");
//      }
//      pst.setString(1, key);
//      pst.setString(2, value);
//      pst.execute();
//    } finally {
//      conn.close();
//      reload();
//    }
//  }
//
//    public void reload() throws Exception {
//      Properties props = new Properties();
//      InitialContext ic = new InitialContext();
//      DataSource ds = (DataSource) ic.lookup("jdbc/WebDS");
//      Connection conn = ds.getConnection();
//      Statement st = conn.createStatement();
//      ResultSet rs = st.executeQuery("select * from config");
//      while (rs.next()) {
//          props.put(rs.getString("name"), rs.getString("value"));
//      }
//      this.props = props;
//    }
//
//    public static String getString(String key) {
//        Config c = Config.getInstance();
//        return (String) c.props.getProperty(key);
//    }
//
//    public static String getString(String key, String defaultValue) {
//        Config c = Config.getInstance();
//        return (String) c.props.getProperty(key, defaultValue);
//    }
//
//    public static int getInt(String key, int defaultValue) {
//        Config c = Config.getInstance();
//        return Integer.parseInt(c.props.getProperty(key, Integer.toString(defaultValue)).trim());
//    }
//
//    public static boolean getBoolean(String key, boolean defaultValue) {
//        Config c = Config.getInstance();
//        return Boolean.parseBoolean(c.props.getProperty(key, Boolean.toString(defaultValue)).trim());
//    }
//
//  public static String getMimeType(String extension) {
//    if (extension == null || extension.trim().length() == 0) {
//      return "application/octet-stream";
//    } else {
//      return getString("mime." + extension, "application/octet-stream");
//    }
//  }
//
//  public static String getUrl() {
//    return (Config.getBoolean("https", false)) ? Config.getString("url.secure") : Config.getString("url");
//  }
//
//}