package au.com.ourbodycorp;

import au.com.ourbodycorp.model.Constants;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.naming.InitialContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.TimeZone;
import javax.net.ssl.HttpsURLConnection;

import org.json.JSONObject;

public class Util {

    public static String formValue(String value) {
        return (value == null) ? "" : value.replaceAll("\"", "&quot;");
    }

    public static String notNull(String value) {
        return (value != null) ? value.trim() : "";
    }

    public static String notNull(String value, String def) {
        return (value != null) ? value.trim() : ((def != null) ? def : "");
    }

    public static String textAreaValue(String value) {
        return (value == null) ? "" : value.replaceAll(">", "&gt;").replaceAll("<", "&lt;");
    }

    public static String loginUrl(Constants.LoginReason reason, HttpServletRequest request) throws Exception {
        String url = (Constants.TESTING_MODE) ? "http" : "https://";
        url += request.getHeader("host");
        url += request.getRequestURI();

        return loginUrl(reason, url, request.getQueryString(), false);
    }

    public static String loginUrl(Constants.LoginReason reason, String url, String query) throws Exception {
        return loginUrl(reason, url, query, false);
    }

    public static String loginUrl(Constants.LoginReason reason, String url, String query, boolean includeHost) throws Exception {
        StringBuilder sb = new StringBuilder();

        if (includeHost) {
            sb.append(Config.getString("url"));
        }

        sb.append("/users/login.jsp");

        if (reason != null | url != null) {
            sb.append("?");
        }

        if (reason != null) {
            sb.append("reason=").append(reason.name());
        }

        if (url != null) {
            if (query != null && query.trim().length() > 0) {
                url += "?" + query;
            }
            if (reason != null) {
                sb.append("&");
            }
            sb.append("return=").append(URLEncoder.encode(url, "UTF-8"));
        }
        return sb.toString();

    }

    public static String safeInput(String source, boolean failOnHtml) throws Exception {
        if (source == null) {
            return null;
        }
        String escaped = textAreaValue(source);
        if (failOnHtml && !escaped.equals(source)) {
            throw new InvalidInputException();
        }
        else {
            return escaped;
        }
    }

    public static String formMissing(LinkedList<String> fields, String field) {
        return (fields != null && fields.contains(field)) ? "formmissing" : "";
    }

    public static String encryptPassword(String password) throws Exception {
        MessageDigest sha = MessageDigest.getInstance("MD5");
        byte[] tmp = password.getBytes();
        sha.update(tmp);
        return Util.convertToHex(sha.digest());
    }

    private static String convertToHex(byte[] data) {
        StringBuffer buf = new StringBuffer();
        for (int i = 0; i < data.length; i++) {
            int halfbyte = (data[i] >>> 4) & 0x0F;
            int two_halfs = 0;
            do {
                if ((0 <= halfbyte) && (halfbyte <= 9)) {
                    buf.append((char) ('0' + halfbyte));
                }
                else {
                    buf.append((char) ('a' + (halfbyte - 10)));
                }
                halfbyte = data[i] & 0x0F;
            }
            while (two_halfs++ < 1);
        }
        return buf.toString();
    }

    public static Connection getConnection() throws Exception {
        InitialContext ic = new InitialContext();
        DataSource ds = (DataSource) ic.lookup("java:comp/env/jdbc/WebDS");
        return ds.getConnection();
    }

    public static long getSequence(String name, Connection conn) throws Exception {
        long id;

        PreparedStatement ps = conn.prepareStatement("select nextval(?) as id");
        ps.setString(1, name);
        ResultSet rs = ps.executeQuery();
        if (rs.next()) {
            id = rs.getLong("id");
        }
        else {
            throw new Exception("No sequence value");
        }
        rs.close();
        ps.close();
        return id;
    }

    public static String randomString(int length) {
        String values = "ABCDEFGHJKLMNPQRTUVWXYZ23456789";
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < length; i++) {
            char c = values.charAt((int) (Math.random() * values.length()));
            sb.append(c);
        }
        return sb.toString();
    }
    
    public static String randomNumeric(int length) {
        String values = "0123456789";
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < length; i++) {
            char c = values.charAt((int) (Math.random() * values.length()));
            sb.append(c);
        }
        return sb.toString();
    }


    public static TimeZone getDefaultTimeZone(String country, String state) {
        TimeZone tz = TimeZone.getTimeZone("Australia/Sydney");

        if (country.equals("AU")) {
            if (state.equals("ACT")) {
                tz = TimeZone.getTimeZone("Australia/ACT");
            }
            else if (state.equals("NSW")) {
                tz = TimeZone.getTimeZone("Australia/Sydney");
            }
            else if (state.equals("NT")) {
                tz = TimeZone.getTimeZone("Australia/Darwin");
            }
            else if (state.equals("QLD")) {
                tz = TimeZone.getTimeZone("Australia/Brisbane");
            }
            else if (state.equals("SA")) {
                tz = TimeZone.getTimeZone("Australia/Adelaide");
            }
            else if (state.equals("TAS")) {
                tz = TimeZone.getTimeZone("Australia/Hobart");
            }
            else if (state.equals("VIC")) {
                tz = TimeZone.getTimeZone("Australia/Melbourne");
            }
            else if (state.equals("WA")) {
                tz = TimeZone.getTimeZone("Australia/Perth");
            }
        } else if ( country.equals("US")) {
            tz = TimeZone.getTimeZone("America/New_York");
        }

        return tz;
    }

    public static boolean isValidEmail(String email) {
        if (email == null) {
            return false;
        }
        boolean result = true;
        try {
            InternetAddress emailAddr = new InternetAddress(email);
            String[] parts = email.split("@");
            result = parts.length == 2 && parts[0].length() > 0 && parts[1].length() > 0;
        }
        catch (AddressException ex) {
            result = false;
        }
        return result;
    }
    
    public static String convertToValidPhoneNumber(String phoneNumber, String countryCode){
        if (phoneNumber == null || phoneNumber.length() < 6) return null;
        
        String validPhone = phoneNumber.replaceAll(" +", "");
        if (validPhone.startsWith("0")) {
            validPhone = Util.notNull(countryCode, "61") + validPhone.substring(1);
        } 
        else if (validPhone.startsWith("+")) {
            validPhone = validPhone.substring(1);
        }
        
        return validPhone;
    }
    
    public static void redirectTo(String relativeUrl, HttpServletRequest request, HttpServletResponse response) throws Exception {
        if (Config.getBoolean("https", false)){
            //since jetty runs as http, while apache is https we use this method to change redirects to stick https on the front
            StringBuffer currentLocation = request.getRequestURL();
            if (currentLocation != null){
                String modifiedLocation = currentLocation.toString().replaceAll("http:", "https:");
                int lastSlashIndex = modifiedLocation.lastIndexOf("/");
                modifiedLocation = modifiedLocation.substring(0, lastSlashIndex + 1);
                response.sendRedirect(modifiedLocation + relativeUrl);
                
                return;
            }
        }
        
        response.sendRedirect(relativeUrl);
    }

    public static boolean makeSecure(HttpServletRequest req, HttpServletResponse res) throws Exception {
        return false;
//        StringBuffer currentUrl = req.getRequestURL();
//        if (!Config.getBoolean("https", false) || currentUrl.toString().startsWith("https")) return false; //already secure
//
//        String url = req.getRequestURI();
//        if (req.getQueryString() != null) {
//          url += "?" + req.getQueryString();
//        }
//        res.sendRedirect(Config.getString("url.secure") + url);
//        
//        return true;
    }
    
    
    public static void close(Writer writer) {
        if (writer == null) return;
        
        try{
            writer.close();
        }
        catch (Exception e){}
    }

    public static void close(OutputStream outputStream) {
        if (outputStream == null) return;
        
        try{
            outputStream.close();
        }
        catch (Exception e){}
    }
    
    public static void close(HttpsURLConnection connection) {
        if (connection == null) return;
        
        try{
            connection.disconnect();
        }
        catch (Exception e){}
    }
    
    /*public static void allActivitiesLogs(String act_name, long act_user_id, String act_corp_loc, String act_page) throws IOException {
    	String urlParameters = "act_name="+act_name+"&act_user_id="+act_user_id+"&act_corp_loc="+act_corp_loc+"&act_page="+act_page+"";
	 	URL url = new URL("http://nodeapps.colanonline.net:3002/saveLogActivity");
	 	URLConnection conn = url.openConnection();

	 	conn.setDoOutput(true);

	 	OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());

	 	writer.write(urlParameters);
	 	writer.flush();

	 	String line;
	 	BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));

		while ((line = reader.readLine()) != null) {
//	 	    System.out.println(line);
	 	}
	 	writer.close();
	 	reader.close(); 
    }*/
    public static void allActivitiesLogs2(String act_type,String act_activity_page,String act_corp_id,String act_corp_name, String act_corp_loc, String act_user_id, String act_user_name, String act_patient_id, String act_patient_name,String act_flag,String act_from, String act_to, String act_desc) throws IOException {
    	System.out.println("=======>inside sub method======> start");
    	String urlParameters = "act_type="+act_type+"&act_activity_page="+act_activity_page+"&act_corp_id="+act_corp_id+"&act_corp_name="+act_corp_name+"&act_corp_loc="+act_corp_loc+"&act_user_id="+act_user_id+"&act_user_name="+act_user_name+"&act_patient_id="+act_patient_id+"&act_patient_name="+act_patient_name+"&act_flag="+act_flag+"&act_from="+act_from+"&act_to="+act_to+"&act_desc="+act_desc;
	 	//URL url = new URL("http://192.168.2.22:81/saveLogActivity"); 
    //	URL url = new URL("http://japps.fiizio.com:81/saveLogActivity");
    	
    	//myself
    	URL url = new URL("http://fiiziosoft.com:81/saveLogActivity");

	 	URLConnection conn = url.openConnection();

	 	conn.setDoOutput(true);

	 	OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());

	 	writer.write(urlParameters);
	 	writer.flush();

	 	String line;
	 	BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));

		while ((line = reader.readLine()) != null) {
 	    System.out.println(line);
	 	}
	 	writer.close();
	 	reader.close(); 
	 	
	 	
	 	System.out.println("=======>inside sub method======> end ");
    }
}