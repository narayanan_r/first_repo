package au.com.ourbodycorp;

import au.com.ourbodycorp.model.ExerciseMedia;
import au.com.ourbodycorp.model.managers.LibraryManager;
import org.im4java.core.ConvertCmd;
import org.im4java.core.IMOperation;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.UUID;

public class MediaServlet extends HttpServlet {
  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    long id = 0;
    int size = 0;
    try {
      id = Long.parseLong(req.getParameter("id"));
    } catch (Exception e) {
      resp.sendError(404, "Invalid media ID " + req.getParameter("id"));
      return;
    }
    try {
      size = Integer.parseInt(req.getParameter("width"));
    } catch (Exception e ) {
      // don't care
    }
    LibraryManager lm = new LibraryManager();
    try {
      ExerciseMedia em = lm.getExerciseMedia(id, false);
      if (em == null) {
        resp.sendError(404, "Media not found");
        return;
      }
      resp.setHeader("Cache-Control", "max-age=7740000, public");
      resp.setHeader("Expires", null);
      if (em.getType().equals("photo")) {
        resp.setContentType("image/jpeg");
//        if(Config.getString("https").equals("true"))
//        	doPhoto(em, size, resp);
//        else
          	doPhotoDevMode(em, size, resp);
        return;
      } else {
        em = lm.getExerciseMedia(em.getId(), true);
        resp.setContentType("video/x-m4v");
      }
      resp.setHeader("Accept-Ranges", "bytes");
      if (req.getHeader("Range") != null) {
        String range = req.getHeader("Range").replaceAll("bytes=", "").trim();

        String[] parts = range.split("-");

        int end = em.getData().length - 1;
        if (parts.length == 2 && parts[1].length() > 0) {
          end = Integer.parseInt(parts[1]);
        }

        int start = 0;
        if (parts[0].length() == 0) {
          start = em.getData().length - 1 - end;
        } else {
          start = Integer.parseInt(parts[0]);
        }

        resp.setStatus(206);
        String rv =  String.format("bytes %d-%d/%d", start, end, em.getData().length);
        int length = end + 1 - start;
        resp.setContentLength(length);
        resp.setHeader("Content-Range", rv);

        resp.getOutputStream().write(em.getData(), start, length);

      } else {
        resp.setContentLength(em.getData().length);
        resp.getOutputStream().write(em.getData());
      }


    } catch (Exception e) {
      throw new ServletException(e);
    }
  }

  @Override
  protected void doHead(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    super.doHead(req, resp);    //To change body of overridden methods use File | Settings | File Templates.
  }

  private void doPhoto(ExerciseMedia em, int size, HttpServletResponse resp) throws Exception {
    LibraryManager lm = new LibraryManager();

    String cache = Config.getString("cache.image");
    if (!cache.endsWith("/")) cache += "/";

    String file = em.getCacheName(size);

    File cf = new File(cache + file);
    if (cf.exists()) {
      returnFile(cf, resp);
    } else if (size == 0) {
      em = lm.getExerciseMedia(em.getId(), true);
      resp.setContentLength(em.getData().length);
      resp.getOutputStream().write(em.getData());
    } else {
      File org= null;
      try {
        em = lm.getExerciseMedia(em.getId(), true);
        org = File.createTempFile("physio-" + em.getId() + UUID.randomUUID().toString(), "jpg");
        FileOutputStream fos = new FileOutputStream(org);
        fos.write(em.getData());
        fos.close();

        File cd = new File(cache + file.split("/")[0]);
        if (!cd.exists()) {
          cd.mkdir();
        }

        ConvertCmd cmd = new ConvertCmd();
        IMOperation op = new IMOperation();
        op.addImage(org.getAbsolutePath());
        op.thumbnail(size);
        op.strip();
        op.addImage(cf.getAbsolutePath());
        cmd.run(op);

        returnFile(cf, resp);

      } finally {
        if (org != null)
          org.delete();
        }
      }

  }
  
  private void doPhotoDevMode(ExerciseMedia em, int size, HttpServletResponse resp) throws Exception {
	    LibraryManager lm = new LibraryManager();

	    String cache = Config.getString("cache.image");
	    if (!cache.endsWith("/")) cache += "/";

	    String file = em.getCacheName(size);

	    File cf = new File(cache + file);
	    if (cf.exists()) {
	      returnFile(cf, resp);
	    } else if (size == 0) {
	      em = lm.getExerciseMedia(em.getId(), true);
	      resp.setContentLength(em.getData().length);
	      resp.getOutputStream().write(em.getData());
	    } else {
	      File org= null;
	      try {
	        em = lm.getExerciseMedia(em.getId(), true);
	        org = File.createTempFile("physio-" + em.getId() + UUID.randomUUID().toString(), "jpg");
	        FileOutputStream fos = new FileOutputStream(org);
	        fos.write(em.getData());
	        
	        //Dev Mode
	        writeFile(cf.getPath(), em.getData());
	        //end
	        
	        fos.close();

	        File cd = new File(cache + file.split("/")[0]);
	        if (!cd.exists()) {
	          cd.mkdir();
	        }

	      //Live Mode
//	        ConvertCmd cmd = new ConvertCmd();
//	        IMOperation op = new IMOperation();
//	        op.addImage(org.getAbsolutePath());
//	        op.thumbnail(size);
//	        op.strip();
//	        op.addImage(cf.getAbsolutePath());
//	        cmd.run(op);

	        returnFile(cf, resp);

	      } finally {
	        if (org != null)
	          org.delete();
	        }
	      }

	  }

  private void writeFile(String name, byte[] data) throws IOException {
	    File file = new File(name);
	    FileOutputStream fos = new FileOutputStream(file);
	    fos.write(data);
	    fos.close();
	  }
  
  private void returnFile(File file, HttpServletResponse resp) throws Exception {
    FileInputStream fis = null;
      try {
        fis = new FileInputStream(file);
        resp.setContentLength((int) file.length());

        byte[] bytes = new byte[10*1024];
        int read = 0;
        do {
          read = fis.read(bytes);
          if (read > -1)
            resp.getOutputStream().write(bytes, 0, read);
        } while (read > -1);
      } finally {
        if (fis != null)
          fis.close();
      }
  }

}
