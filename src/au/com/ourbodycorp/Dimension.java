package au.com.ourbodycorp;

/**
 * Created by IntelliJ IDEA.
 * User: bas
 * Date: 25/01/11
 * Time: 1:49 PM
 * To change this template use File | Settings | File Templates.
 */
public class Dimension {
  private int width;
  private int height;

  public Dimension() {
  }

  public Dimension(int width, int height) {
    this.width = width;
    this.height = height;
  }

  public int getWidth() {
    return width;
  }

  public void setWidth(int width) {
    this.width = width;
  }

  public int getHeight() {
    return height;
  }

  public void setHeight(int height) {
    this.height = height;
  }
}
