package au.com.ourbodycorp;

import au.com.ourbodycorp.model.Comment;
import au.com.ourbodycorp.model.Constants;
import au.com.ourbodycorp.model.Post;
import au.com.ourbodycorp.model.User;
import au.com.ourbodycorp.model.managers.PostManager;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CommentServlet extends HttpServlet {
  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    try {
      String url = (req.getParameter("url") != null) ? req.getParameter("url") : "/";
      String comment = (req.getParameter("comment") != null && req.getParameter("comment").trim().length() > 0) ? req.getParameter("comment") : null;
      User m = (User) req.getSession().getAttribute("member");
      if (m == null) {
        // Only logged in members can comment
        resp.sendRedirect("/members/login.jsp?reason=" + Constants.LoginReason.login + "&return=" + url);
      } else if (comment == null) {
        resp.sendRedirect(url);
      } else {
        PostManager pm = new PostManager();
        Post p = pm.getPost(Long.parseLong(req.getParameter("postId")));
        if (p != null && p.isAllowComments()) {
          Comment c = new Comment();
          c.setAuthor(m.getId());
          c.setPostId(p.getId());
          c.setComment(comment.replaceAll("&", "&amp;").replaceAll("<", "&lt;").replaceAll(">", "&gt;"));
          pm.saveComment(c);
          if (!pm.isSubscribed(p.getId(), m.getId())) {
            pm.addSubscription(p.getId(), m.getId());
          }
          CommentMailThread cmt = new CommentMailThread(c);
          cmt.setDaemon(false);
          cmt.start();
        }
        resp.sendRedirect(url);
      }
    } catch (Exception e) {
      throw new ServletException(e);
    }
  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    try {
      User m = (User) req.getSession().getAttribute("member");
      PostManager pm = new PostManager();
      if (req.getParameter("subscribe") != null) {
        if (m == null) {
          resp.sendRedirect("/members/login.jsp?reason=" + Constants.LoginReason.login);
        } else {
          long id = Long.parseLong(req.getParameter("subscribe"));
          Post p = pm.getPost(id);
          if (p != null) {
            if (!pm.isSubscribed(id, m.getId())) {
              pm.addSubscription(id, m.getId());
            }
            resp.sendRedirect(p.getUrl());
          } else {
            resp.sendRedirect("/");
          }
        }
      } else if (req.getParameter("u") != null) {
        if (m == null) {
          resp.sendRedirect("/members/login.jsp?reason=" + Constants.LoginReason.login);
        } else {
          long id = Long.parseLong(req.getParameter("u"));
          Post p = pm.getPost(id);
          if (p != null) {
            pm.deleteSubscription(id, m.getId());
            resp.sendRedirect(p.getUrl());
          } else {
            resp.sendRedirect("/");
          }
        }
      } else if (req.getParameter("unsub") != null) {
        pm.deleteSubscription(req.getParameter("unsub"));
        req.getRequestDispatcher("/WEB-INF/includes/unsub.jsp").forward(req, resp);
      }
    } catch (Exception e) {
      throw new ServletException(e);
    }
  }
}
