package au.com.ourbodycorp;

import au.com.ourbodycorp.model.User;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.net.URLEncoder;

public class AdminFilter implements Filter {

    public void destroy() {
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;
        resp.setHeader("Cache-Control", "no-cache");

        HttpSession session = req.getSession();
        User u = (User) session.getAttribute("user");
        try {
            if (u == null || !(u.hasRole("admin") || u.hasRole("author"))) {
                String returnUrl = Config.getString("baseDomain");
                if (Config.getBoolean("https", false)) {
                    returnUrl = "https://" + returnUrl;
                }
                else {
                    returnUrl = "http://" + returnUrl;
                }
                    
                if (req.getMethod().equalsIgnoreCase("get")) {
                    String uri = req.getRequestURI();
                    String params = req.getQueryString();
                    if (params != null && params.trim().length() > 0) {
                        uri += "?" + params;
                    }
                    resp.sendRedirect(returnUrl + "/users/login.jsp?reason=admin&return=" + URLEncoder.encode(uri, "UTF-8"));
                }
                else {
                    resp.sendRedirect(returnUrl + "/users/login.jsp?reason=admin");
                }
            }
            else {
                chain.doFilter(request, response);
            }
        }
        catch (Exception e) {
            throw new ServletException(e);
        }
    }

    public void init(FilterConfig config) throws ServletException {
    }
}
