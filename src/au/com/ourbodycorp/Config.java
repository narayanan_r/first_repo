package au.com.ourbodycorp;


import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.postgresql.ds.PGPoolingDataSource;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Hashtable;
import java.util.Properties;
import java.util.Set;

public class Config {
	 private static final Logger LOG = Logger.getLogger(Config.class);
    private static Config ourInstance = new Config();
    private Properties props = new Properties();
   

  public static Config getInstance() {
        return ourInstance;
    }
  
 /* public void checkDBConn(){
	  LOG.info("Inside checkDBConn");
	  InitialContext ic;
	try {
		ic = new InitialContext();
		 LOG.info("Before Getting  DataSource");
		 DataSource ds = (DataSource) ic.lookup("java:comp/env/jdbc/WebDS"); 
		 LOG.info("After Getting  DataSource "+ds);
		  Connection conn = ds.getConnection();
		  LOG.info("After Getting  Connection "+conn);
	      Statement st = conn.createStatement();
	      LOG.info("After Getting  Statement "+st);
	      ResultSet rs = st.executeQuery("select * from config");
	      LOG.info("After Getting  ResultSet "+rs);
	      while (rs.next()) {
		      LOG.info(rs.getString("name")+" "+rs.getString("value"));
	         // props.put(rs.getString("name"), rs.getString("value"));
	      }
	} catch (Exception e) {
		LOG.error("Exception while getting connection", e);
	}
	 
	  
  }*/

    private Config() {
    	LOG.info("Inside Config.java");
        try {
//            DEPLOYMENT VERSION
          InitialContext ic = new InitialContext();
          Context env = (Context) ic.lookup("java:/comp/env/");
          
          Hashtable<?, ?> outTable = env.getEnvironment();
          Set<?> keys = outTable.keySet();
          for(Object key: keys){
              System.out.println("Value of "+key+" is: "+outTable.get(key));
          }
          LOG.info("Before getting datasoucre Config.java");
            DataSource ds = (DataSource) ic.lookup("java:comp/env/jdbc/WebDS"); 
            LOG.info("After getting Datasource Config.java ds="+ds);
    //        LOCAL DEV VERSION
         /*  PGPoolingDataSource ds = new PGPoolingDataSource();
           ds.setDataSourceName("postgres");
           ds.setServerName("localhost");
            ds.setDatabaseName("fiizio");
           ds.setUser("fiizio");
           ds.setPassword("fiizio");
           ds.setMaxConnections(10);*/
            LOG.info("Inside Config.java");
            Connection conn = ds.getConnection();
            LOG.info("After connection in config.java file");
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("select * from config");
            while (rs.next()) {
            	LOG.info("InCongig file  "+rs.getString("name")+" "+rs.getString("value"));
                props.put(rs.getString("name"), rs.getString("value"));
            }
        } catch (Exception e) {
        	LOG.error("Exception while getting connection", e);
        }
    }

  public void setString(String key, String value) throws Exception {
    Connection conn = Util.getConnection();
    try {
      PreparedStatement pst;
      if (props.containsKey(key)) {
        pst = conn.prepareStatement("update config set value =? where name = ?");
      } else {
        pst = conn.prepareStatement("insert into config (value, name) values (?, ?)");
      }
      pst.setString(1, key);
      pst.setString(2, value);
      pst.execute();
    } finally {
      conn.close();
      reload();
    }
  }

    public void reload() throws Exception {
      Properties props = new Properties();
      InitialContext ic = new InitialContext();
      DataSource ds = (DataSource) ic.lookup("jdbc/WebDS");
      Connection conn = ds.getConnection();
      Statement st = conn.createStatement();
      ResultSet rs = st.executeQuery("select * from config");
      while (rs.next()) {
          props.put(rs.getString("name"), rs.getString("value"));
      }
      this.props = props;
    }

    public static String getString(String key) {
        Config c = Config.getInstance();
        return (String) c.props.getProperty(key);
    }

    public static String getString(String key, String defaultValue) {
        Config c = Config.getInstance();
        return (String) c.props.getProperty(key, defaultValue);
    }

    public static int getInt(String key, int defaultValue) {
        Config c = Config.getInstance();
        return Integer.parseInt(c.props.getProperty(key, Integer.toString(defaultValue)).trim());
    }

    public static boolean getBoolean(String key, boolean defaultValue) {
        Config c = Config.getInstance();
        return Boolean.parseBoolean(c.props.getProperty(key, Boolean.toString(defaultValue)).trim());
    }

  public static String getMimeType(String extension) {
    if (extension == null || extension.trim().length() == 0) {
      return "application/octet-stream";
    } else {
      return getString("mime." + extension, "application/octet-stream");
    }
  }

  public static String getUrl() {
    return (Config.getBoolean("https", false)) ? Config.getString("url.secure") : Config.getString("url");
  }

}