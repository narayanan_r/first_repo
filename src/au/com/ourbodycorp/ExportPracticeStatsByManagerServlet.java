package au.com.ourbodycorp;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
    
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import au.com.ourbodycorp.model.Corporation;
import au.com.ourbodycorp.model.CorporationUser;
import au.com.ourbodycorp.model.User;
import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
    
public class ExportPracticeStatsByManagerServlet extends HttpServlet
{
   /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

public void doGet(HttpServletRequest request, 
    HttpServletResponse response)
      throws ServletException, IOException
   {
    OutputStream out = null;
    try
    {
    	
     response.setContentType("application/vnd.ms-excel");
 
     response.setHeader("Content-Disposition", "attachment; filename=Practice_Statistics.xls");
 
     WritableWorkbook workbook = Workbook.createWorkbook(response.getOutputStream());
     WritableSheet sheet = workbook.createSheet("Practice Statistics", 0);
     
     // Create cell font and format
     WritableFont cellFont1 = new WritableFont(WritableFont.TIMES, 11);
     cellFont1.setColour(Colour.BLUE);
     cellFont1.setBoldStyle(WritableFont.BOLD);
     
     WritableCellFormat cellFormat1 = new WritableCellFormat(cellFont1);
     cellFormat1.setBackground(Colour.LIGHT_ORANGE);
     cellFormat1.setBorder(Border.ALL, BorderLineStyle.THIN);
     cellFormat1.setAlignment(Alignment.CENTRE);
     
     CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");

     Corporation corp = cu.getCorporation();
     
     User u = cu.getUser();
     
     Connection conn = Util.getConnection();
     
//     PreparedStatement pst = conn.prepareStatement("SELECT COALESCE(to_char(T.date, 'DD-Mon-YYYY at HH24:MI'), 'Not Created') as date,T.firstname,T.lastname,T.fname,T.lname,T.programname,T.downloaded  FROM (select pt.created as date,u.firstname,u.lastname,pt.fname,pt.lname,COALESCE(NULLIF(p.name  ,''),'No Program Name') as programname ,COALESCE(to_char(p.retrieved, 'DD-Mon-YYYY at HH24:MI'), 'Never') as downloaded from corporation c join patient pt on pt.corp_id = c.id join users u on u.id = pt.creator left join programs p on p.patient = pt.id where c.id = ? GROUP BY PT.CREATED ,u.firstname,u.lastname,pt.fname,pt.lname,COALESCE(NULLIF(p.name  ,''),'No Program Name') ,COALESCE(to_char(p.retrieved, 'DD-Mon-YYYY at HH24:MI'), 'Never') order by PT.CREATED DESC)T");
     PreparedStatement pst = conn.prepareStatement("SELECT COALESCE(to_char(T.date, 'DD-Mon-YYYY at HH24:MI'), 'Not Yet Created') as date,T.firstname,T.lastname,T.fname,T.lname,T.programname,T.downloaded  FROM (select p.created as date, u.firstname, u.lastname, pt.fname, pt.lname, COALESCE(NULLIF(p.name  ,''),'Program Not Yet Created') as programname , COALESCE(to_char(p.retrieved, 'DD-Mon-YYYY at HH24:MI'), 'Never') as downloaded from corporation c join patient pt on pt.corp_id = c.id join users u on u.id = pt.creator left join programs p on p.patient = pt.id where c.id = ? order by P.CREATED DESC)T");
     pst.setLong(1, corp.getId());
     ResultSet rs = pst.executeQuery();
     
     int col = 0;
     int widthInChars = 20;
     sheet.setColumnView(col, widthInChars);
     sheet.addCell(new Label(col, 0, "Date", cellFormat1));   
     
     col = 1;
     widthInChars = 30;
     sheet.mergeCells(1, 0, 2, 0);
     sheet.setColumnView(col, widthInChars);
     sheet.addCell(new Label(col, 0, "Team Members", cellFormat1));  
     
     col = 3;
     widthInChars = 30;
     sheet.mergeCells(3, 0, 4, 0);
     sheet.setColumnView(col, widthInChars);
     sheet.addCell(new Label(col, 0, "Client Name", cellFormat1));  
     
     col = 5;
     widthInChars = 50;
     sheet.setColumnView(col, widthInChars);
     sheet.addCell(new Label(col, 0, "Program Name", cellFormat1));  
     
     col = 6;
     widthInChars = 20;
     sheet.setColumnView(col, widthInChars);
     sheet.addCell(new Label(col, 0, "Download Date", cellFormat1));  
     
     
     //Set cell width in CHARS
     int rowCount = 1;
     // Create cell font and format
     WritableFont cellFont = new WritableFont(WritableFont.TIMES, 11);
     cellFont.setColour(Colour.BLACK);
     
     WritableCellFormat cellFormat = new WritableCellFormat(cellFont);
     cellFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
     while (rs.next()) {
	
     col = 0;
     widthInChars = 20;
     sheet.setColumnView(col, widthInChars);
     sheet.addCell(new Label(col, rowCount, rs.getString("date"), cellFormat));   
     
     col = 1;
     widthInChars = 15;
     sheet.setColumnView(col, widthInChars);
     sheet.addCell(new Label(col, rowCount, rs.getString("firstname"), cellFormat));  
     
     col = 2;
     widthInChars = 15;
     sheet.setColumnView(col, widthInChars);
     sheet.addCell(new Label(col, rowCount, rs.getString("lastname"), cellFormat));  
     
     col = 3;
     widthInChars = 15;
     sheet.setColumnView(col, widthInChars);
     sheet.addCell(new Label(col, rowCount, rs.getString("fname"), cellFormat));  
     
     col = 4;
     widthInChars = 15;
     sheet.setColumnView(col, widthInChars);
     sheet.addCell(new Label(col, rowCount, rs.getString("lname"), cellFormat));  
     
     col = 5;
     widthInChars = 50;
     sheet.setColumnView(col, widthInChars);
     sheet.addCell(new Label(col, rowCount, rs.getString("programname"), cellFormat));  
     
     col = 6;
     widthInChars = 20;
     sheet.setColumnView(col, widthInChars);
     sheet.addCell(new Label(col, rowCount, rs.getString("downloaded"), cellFormat)); 
     
     rowCount = rowCount + 1;
     }
     //Writes out the data held in this workbook in Excel format
     workbook.write(); 
     
     //Close and free allocated memory 
     workbook.close(); 
     pst.close();
     conn.close();
     
     String type="Reports";
     String activity_page="ExportPracticeStatsByManager";
 	String corp_id=String.valueOf(corp.getId());
 	String corp_name=corp.getName();
 	String corp_loc=corp.getSuburb();
 	String user_id=String.valueOf(u.getId());
 	String user_name=u.getFullName();
 	String invitor_id="---";
 	String invitor_name="---";
 	String flag="display";
 	String from="---";
 	String to="---";
 	String desc="Downloaded Practice Stats Report";
     try{
     	
     	Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
     }catch(Exception e){
     	
     }
     
   /*  try{
     	Util.allActivitiesLogs("Reports", u.getId(), corp.getName(), "Downloaded Practice Stats Report");
     }catch(Exception e){
     	
     }*/
    } catch (Exception e)
    {
     throw new ServletException("Exception in Excel Practise Statistics Servlet", e);
    } finally
    {
     if (out != null)
      out.close();
    }
   }
  }