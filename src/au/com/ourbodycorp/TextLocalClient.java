package au.com.ourbodycorp;

import au.com.ourbodycorp.model.Constants;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

public class TextLocalClient {

    public String sendSimple(String rcpt, String sender, String message) throws Exception {
        if (rcpt == null || rcpt.trim().length() == 0) return "ERROR";
        
        //trim off any excess commas
        while (rcpt.endsWith(",") && rcpt.length() > 0){
            rcpt = rcpt.substring(0, rcpt.length() - 1);
        }
        
        //in testing mode just output to the console
        if (Constants.TESTING_MODE){
            System.out.println("Would have sent to " + rcpt);
            
            return "HELLO";
        }
        
        String gs = String.format("http://www.txtlocal.com/sendsmspost.php");
        HttpClient hc = new DefaultHttpClient();
        HttpPost post = new HttpPost(gs);
        ResponseHandler<String> rh = new BasicResponseHandler();

        List<NameValuePair> nvps = new ArrayList<NameValuePair>();
        nvps.add(new BasicNameValuePair("uname", Config.getString("sms.user")));
        nvps.add(new BasicNameValuePair("pword", Config.getString("sms.password")));
        nvps.add(new BasicNameValuePair("from", sender));
        nvps.add(new BasicNameValuePair("selectednums", rcpt));
        nvps.add(new BasicNameValuePair("message", message));



        post.setEntity(new UrlEncodedFormEntity(nvps, "UTF-8"));

        String body = hc.execute(post, rh);
        return body;
    }
}
