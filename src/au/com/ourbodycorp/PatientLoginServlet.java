package au.com.ourbodycorp;

import au.com.ourbodycorp.model.*;
import au.com.ourbodycorp.model.managers.CorporationManager;
import au.com.ourbodycorp.model.managers.LibraryManager;
import au.com.ourbodycorp.model.managers.UserManager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

public class PatientLoginServlet extends HttpServlet{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {
        	String username = request.getParameter("username");
            String password = request.getParameter("password");
                
            if (username == null || username.trim().length() == 0) {
                response.sendError(400, "No Username provided");
                return;
            }
            
            if (password == null || password.trim().length() == 0) {
                response.sendError(400, "No Password provided");
                return;
            }
            
            Connection conn = Util.getConnection();
            
            if(username != null){
            	username=username.toLowerCase();
            	PreparedStatement pst = conn.prepareStatement("select * from patient where email = ?");
                pst.setString(1, username);
                ResultSet rs = pst.executeQuery();
                if (!rs.next()) {
                	response.sendError(403, "The user does not exist.");
                    return;
                }
                rs.close();
                pst.close();
            	
            } 
            
            if(password != null){
            	username=username.toLowerCase();
            	PreparedStatement pst = conn.prepareStatement("select * from patient where email = ? and password = ?");
                pst.setString(1, username);
                pst.setString(2, Util.encryptPassword(password));
                ResultSet rs = pst.executeQuery();
                if (!rs.next()) {
                	response.sendError(403, "Password you entered wrong.");
                    return;
                }
                rs.close();
                pst.close();
            }
            
            conn.close();
            
            LibraryManager lm = new LibraryManager();
            CorporationManager cm = new CorporationManager();
            
            if(lm.getPatientExist(username,Util.encryptPassword(password))){
//            	System.out.println("Login Success...");
            }else{
            	response.sendError(403, "The email or password you entered wrong.");
                return;
            }
            
            
            LinkedHashMap<String, Object> main = new LinkedHashMap<String, Object>();
            main.put("baseUrl", Config.getString("url"));

            
            List<Program> pCodes = lm.getProgramCodes(username);
            
            if(!pCodes.isEmpty()){
            	Corporation corp = cm.getCorporation(pCodes.get(0).getCorpId());
            	if(corp.getSession_timeout() == 0){
            		main.put("sessionTimeout", 20);
            	}else{
            		main.put("sessionTimeout", corp.getSession_timeout());
            	}
            }else{
            	main.put("sessionTimeout", 20);
            }
            
            
            //add program codes
            LinkedList<LinkedHashMap<String, Object>> programCodes = new LinkedList<LinkedHashMap<String, Object>>();
            main.put("programCodes", programCodes);
            for (Program pCode : pCodes) {
                LinkedHashMap<String, Object> itemMap = new LinkedHashMap<String, Object>();
                itemMap.put("id", pCode.getCode());
                programCodes.add(itemMap);
            }
                   
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            String json = gson.toJson(main);
            
            response.setContentType("application/json;charset=utf-8");
            byte[] out = json.getBytes("UTF-8");
            response.setContentLength(out.length);
            response.getOutputStream().write(out);
        
        

        }
        catch (Exception e) {
            throw new ServletException(e);
        }
    }
}
