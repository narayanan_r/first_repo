package au.com.ourbodycorp;

import au.com.ourbodycorp.model.*;
import au.com.ourbodycorp.model.managers.PatientManager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedHashMap;

public class PatientForgotServlet extends HttpServlet{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {
        	String username = request.getParameter("username");
            if (username == null || username.trim().length() == 0) {
                response.sendError(400, "No Username provided");
                return;
            }
            Connection conn = Util.getConnection();
            
            if(username != null){
            		 PreparedStatement pst = conn.prepareStatement("select * from patient where email = ?");
            		 pst.setString(1, username);
            		 ResultSet rs = pst.executeQuery();
            		 if (!rs.next()) {
                	 response.sendError(403, "The user does not exist.");
                     return;
            		 }else{
                     LinkedHashMap<String, Object> main = new LinkedHashMap<String, Object>();
                     PatientManager pm = new PatientManager();
                     Patient p=pm.getUserByEmail(username);
                     String key=pm.startPasswordReset(p.getId());
                     pm.sendPasswordResetEmail(p, key);
                     main.put("success", pm.success(p.getId(), key));
                     Gson gson = new GsonBuilder().setPrettyPrinting().create();
                     String json = gson.toJson(main);
                     response.setContentType("application/json;charset=utf-8");
                     byte[] out = json.getBytes("UTF-8");
                     response.setContentLength(out.length);
                     response.getOutputStream().write(out);
                     }
                     rs.close();
                     pst.close();
                     conn.close();            	
            	} 
           	}
        catch (Exception e) {
            throw new ServletException(e);
        }
    }
}
