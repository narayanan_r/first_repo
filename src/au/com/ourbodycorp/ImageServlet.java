package au.com.ourbodycorp;

import au.com.ourbodycorp.model.Attachment;
import au.com.ourbodycorp.model.managers.AttachmentManager;
import org.im4java.core.CommandException;
import org.im4java.core.ConvertCmd;
import org.im4java.core.IMOperation;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.UUID;

public class ImageServlet extends HttpServlet {

  private String tmp;

  public ImageServlet() {
    String tmp = System.getProperty("java.io.tmpdir");

    if (tmp.endsWith(File.separator)) {
      this.tmp = tmp;
    } else {
      this.tmp = tmp + File.separator;
    }
  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    headOrGet(req, resp);
  }

  @Override
  protected void doHead(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    headOrGet(req, resp);
  }

  @Override
  protected long getLastModified(HttpServletRequest req) {
    try {
    String parts[] = req.getRequestURI().substring(1).split("/");
    if (parts.length != 4) {
      return -1;
    }

    long id = 0;
    if (parts[1].contains(".")) {
      id = Long.parseLong(parts[1].split("\\.")[0]);
    } else {
      id = Long.parseLong(parts[1]);
    }

    Attachment.ImageSize size = Attachment.ImageSize.valueOf(parts[2]);
    String ext = parts[3].substring(parts[3].lastIndexOf("."));

    AttachmentManager am = new AttachmentManager();
    Attachment a = am.getAttachment(id, false);
    if (a == null) {
      return -1;
    }
    return a.getUpdated().getTime();
    } catch (Exception e) {
//      e.printStackTrace();
      return -1;
    }
  }

  private void headOrGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    boolean headOnly = req.getMethod().equalsIgnoreCase("head");

    try {
      String parts[] = req.getRequestURI().substring(1).split("/");
      if (parts.length != 4) {
        resp.sendError(404, "Unrecognised URL: " + req.getRequestURI());
        return;
      }

      long id = 0;
      long version = 0;
      if (parts[1].contains(".")) {
        id = Long.parseLong(parts[1].split("\\.")[0]);
        version = Long.parseLong(parts[1].split("\\.")[1]);
      } else {
        id = Long.parseLong(parts[1]);
      }


      Attachment.ImageSize size = Attachment.ImageSize.valueOf(parts[2]);
      String ext = parts[3].substring(parts[3].lastIndexOf(".") + 1).toLowerCase();

      AttachmentManager am = new AttachmentManager();

      String cacheDir = Config.getString("cache.image");
      File cacheFile = null;
      File file = null;
      if (cacheDir != null) {
        cacheFile = new File(cacheDir);
        if (cacheFile.exists()) {
          file = new File(cacheFile, id + "." + version + "-" + size.name() + "." + ext);
        }
      }

      Calendar c = Calendar.getInstance();
      c.add(Calendar.MONTH, 3);
      resp.setDateHeader("Expires", c.getTime().getTime());
      resp.addHeader("Cache-Control", "public");

      if (file != null && file.exists()) {
        resp.setContentLength((int) file.length());
        resp.setStatus(200);
        resp.setDateHeader("Last-Modified", file.lastModified());
        resp.setContentType(Config.getMimeType(ext));
        if (!headOnly) {
          FileInputStream is = new FileInputStream(file);
          byte[] data = new byte[(int) file.length()];
          int read = 0;
          while (read < data.length) {
            read += is.read(data, read, data.length - read);
          }
          is.close();
          resp.getOutputStream().write(data);
        }
      } else {
        Attachment a = am.getAttachment(id,  true);
        if (a == null) {
          resp.sendError(404);
          return;
        }
        if (a.getType() != Attachment.AttachmentType.image) {
          resp.sendError(404);
          return;
        }
        resp.setStatus(200);
        resp.setDateHeader("Last-Modified", a.getUpdated().getTime());
        resp.setContentType(Config.getMimeType(a.getExtension()));

        if (size == Attachment.ImageSize.org) {
          resp.setContentLength(a.getData().length);
          resp.getOutputStream().write(a.getData());
        } else {
        	byte[] data;
//        	if(Config.getString("https").equals("true"))
//          data = getResized(a, size);
//        	else
          data = getResizedDevMode(a, size, file);
          
          if (data != null && file != null) {
            writeFile(file.getAbsolutePath(), data);
          }
          resp.setContentLength(data.length);
          if (!headOnly) {
            resp.getOutputStream().write(data);
          }
        }
      }
    } catch (IOException e) {
      throw e;
    } catch (Exception e) {
      throw new ServletException(e);
    }
  }

  private byte[] getResized(Attachment a, Attachment.ImageSize size) throws Exception {
    String tmpId = UUID.randomUUID().toString();
    String ext = a.getFilename().substring(a.getFilename().lastIndexOf("."));
    String srcName = tmp + tmpId + "-src." + ext;
    String outName = tmp + tmpId + "-out.jpg";
    byte[] data = null;
    writeFile(srcName, a.getData());
    writeFile(outName, a.getData());
    BufferedImage img = ImageIO.read(new File(srcName));
    int height = img.getHeight();
    int width = img.getWidth();

    ConvertCmd cmd = new ConvertCmd();
    IMOperation op = new IMOperation();
    op.addImage(srcName);
    if (size.isCropped()) {
      double sourceAspect = (double) width / (double) height;
      double targetAspect = (double) size.getWidth() / (double) size.getHeight();
      double targetHeight = (double) size.getHeight();
      double targetWidth = (double) size.getWidth();
      double sourceHeight = (double) height;
      double sourceWidth = (double) width;
      op.thumbnail(size.getWidth(), size.getHeight(), "^");
      if (sourceAspect == targetAspect) {
        // already correct aspect ratio;
      } else if (size.getWidth() == size.getHeight()) {
        //square
        if (height == width) {
          // already square;
        } else if (height > width) {
          int y = (int) (((sourceHeight / (sourceWidth / targetWidth)) - targetHeight) / 2.0);
          op.crop(size.getWidth(),size.getHeight(),0, y);
        } else {
          int x = (int) (((sourceWidth / (sourceHeight / targetHeight)) - targetWidth) / 2.0);
          op.crop(size.getWidth(),size.getHeight(),x, 0);
        }
      } else if ((size.getOrientation() == Attachment.ImageSize.LANDSCAPE && targetAspect > sourceAspect)
          || (size.getOrientation() == Attachment.ImageSize.PORTRAIT && targetAspect < sourceAspect)) {
        int y = (int) (((sourceHeight / (sourceWidth / targetWidth)) - targetHeight) / 2.0);
        op.crop(size.getWidth(),size.getHeight(),0, y);
      } else if ((size.getOrientation() == Attachment.ImageSize.LANDSCAPE && targetAspect < sourceAspect)
          || (size.getOrientation() == Attachment.ImageSize.PORTRAIT && targetAspect > sourceAspect)) {
        int x = (int) (((sourceWidth / (sourceHeight / targetHeight)) - targetWidth) / 2.0);
        op.crop(size.getWidth(),size.getHeight(),x, 0);
      }
    } else if (width > size.getWidth() || height > size.getHeight()) {
      //only resize if larger than size.
      op.resize(size.getWidth(), size.getHeight());
    }
    op.strip();
    op.addImage(outName);
    try {
      cmd.run(op);
    } catch (CommandException e) {
      for (String error : e.getErrorText()) {
        System.err.println("error line: " + error);
      }
      throw new Exception(e);
    }
    deleteFile(srcName);
    data = readFile(outName);
    deleteFile(outName);

    return data;
  }
  
  private byte[] getResizedDevMode(Attachment a, Attachment.ImageSize size, File file) throws Exception {
	    String tmpId = UUID.randomUUID().toString();
	    String ext = a.getFilename().substring(a.getFilename().lastIndexOf("."));
	    
	    String filepath = file.getPath();
	    
	    String srcName = tmp + tmpId + "-src." + ext;
	    String outName = tmp + tmpId + "-out.jpg";
	    byte[] data = null;
	    writeFile(filepath, a.getData());
	    writeFile(srcName, a.getData());
	    writeFile(outName, a.getData());
	    BufferedImage img = ImageIO.read(new File(srcName));
	    int height = img.getHeight();
	    int width = img.getWidth();

//	    ConvertCmd cmd = new ConvertCmd();
	    IMOperation op = new IMOperation();
	    op.addImage(srcName);
	    if (size.isCropped()) {
	      double sourceAspect = (double) width / (double) height;
	      double targetAspect = (double) size.getWidth() / (double) size.getHeight();
	      double targetHeight = (double) size.getHeight();
	      double targetWidth = (double) size.getWidth();
	      double sourceHeight = (double) height;
	      double sourceWidth = (double) width;
	      op.thumbnail(size.getWidth(), size.getHeight(), "^");
	      if (sourceAspect == targetAspect) {
	        // already correct aspect ratio;
	      } else if (size.getWidth() == size.getHeight()) {
	        //square
	        if (height == width) {
	          // already square;
	        } else if (height > width) {
	          int y = (int) (((sourceHeight / (sourceWidth / targetWidth)) - targetHeight) / 2.0);
	          op.crop(size.getWidth(),size.getHeight(),0, y);
	        } else {
	          int x = (int) (((sourceWidth / (sourceHeight / targetHeight)) - targetWidth) / 2.0);
	          op.crop(size.getWidth(),size.getHeight(),x, 0);
	        }
	      } else if ((size.getOrientation() == Attachment.ImageSize.LANDSCAPE && targetAspect > sourceAspect)
	          || (size.getOrientation() == Attachment.ImageSize.PORTRAIT && targetAspect < sourceAspect)) {
	        int y = (int) (((sourceHeight / (sourceWidth / targetWidth)) - targetHeight) / 2.0);
	        op.crop(size.getWidth(),size.getHeight(),0, y);
	      } else if ((size.getOrientation() == Attachment.ImageSize.LANDSCAPE && targetAspect < sourceAspect)
	          || (size.getOrientation() == Attachment.ImageSize.PORTRAIT && targetAspect > sourceAspect)) {
	        int x = (int) (((sourceWidth / (sourceHeight / targetHeight)) - targetWidth) / 2.0);
	        op.crop(size.getWidth(),size.getHeight(),x, 0);
	      }
	    } else if (width > size.getWidth() || height > size.getHeight()) {
	      //only resize if larger than size.
	      op.resize(size.getWidth(), size.getHeight());
	    }
	    op.strip();
	    op.addImage(outName);
//	    try {
//	      cmd.run(op);
//	    } catch (CommandException e) {
//	      for (String error : e.getErrorText()) {
//	        System.err.println("error line: " + error);
//	      }
//	      throw new Exception(e);
//	    }
	    deleteFile(srcName);
	    data = readFile(filepath);
	    deleteFile(outName);

	    return data;
	  }

  private byte[] readFile(String name) throws IOException {
    File file = new File(name);
    byte[] data = new byte[( int ) file.length()];
    FileInputStream fis = new FileInputStream(file);
    int read = 0;
    while (read < data.length) {
      read += fis.read(data, read, data.length - read);
    }
    fis.close();
    return data;
  }

  private void writeFile(String name, byte[] data) throws IOException {
    File file = new File(name);
    FileOutputStream fos = new FileOutputStream(file);
    fos.write(data);
    fos.close();
  }

  private void deleteFile(String name) {
    File file = new File(name);
    file.delete();
  }

}
