package au.com.ourbodycorp;

import au.com.ourbodycorp.model.User;
import au.com.ourbodycorp.model.managers.LibraryManager;
import au.com.ourbodycorp.model.managers.UserManager;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.Date;

public class LoginFilter implements Filter {
	
	
	
  public void init(FilterConfig filterConfig) throws ServletException {

  }

  public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

    try {
    	
      HttpServletRequest req = (HttpServletRequest) servletRequest;
      HttpServletResponse resp = (HttpServletResponse) servletResponse;
      resp.setHeader("Cache-Control", "private");
      HttpSession session = req.getSession();
     
      /*   User user= (User) session.getAttribute("user");
    String sessionIdFromMap=UserMap.sessionMap.get(session.getId());
    System.out.println("session.getId() IN Login Filter "+session.getId());
     if(user!=null){
    	 System.out.println("UserMap.sessionMap.containsKey "+!UserMap.sessionMap.containsKey(user.getEmail()));
     }
		if(user!=null && sessionIdFromMap==null && !UserMap.sessionMap.containsKey(user.getEmail())){
			UserMap.userMap.put(user.getEmail(), user);
			UserMap.sessionMap.put(user.getEmail(),session.getId());
			System.out.println("Added user for session id "+session.getId());
		}
		*/
      if (session.isNew()) {
        Cookie[] cookies = req.getCookies();
        if (cookies != null) {
          for (Cookie cookie : cookies) {
            if (!cookie.getName().equals("remember")) {
              UserManager mm = new UserManager();
              User m = mm.getUserByRememberKey(cookie.getValue());
              if (m != null && m.getStatus() == User.Status.authorised) {
                LibraryManager lm = new LibraryManager();
                session.setAttribute("hasCompanies", lm.hasCompanies(m.getId()));
                session.setAttribute("user", m);
                m.setLastSeen(new Date());
                mm.saveUser(m);
               
              }
            }
          }
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }

    filterChain.doFilter(servletRequest, servletResponse);
  }

  public void destroy() {

  }
}
