package au.com.ourbodycorp;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import au.com.ourbodycorp.model.CorporationUser;
import au.com.ourbodycorp.model.Patient;
import au.com.ourbodycorp.model.managers.LibraryManager;
import au.com.ourbodycorp.model.managers.UserManager;
 
@SuppressWarnings("serial")
public class ImportCliniko extends HttpServlet{
	CorporationUser cu = null;
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		cu = (CorporationUser) request.getAttribute("corpUser");
		try{
			String key = (new UserManager().getUserByCorpid(cu.getCorporation().getId())).getClinikoAPIKey();
			getAndParseJSON(getDetailsFromCliniko(key),cu.getCorporation().getId(),cu.getUser().getId());
		} catch (Exception e){
			e.printStackTrace(); 
		}
		response.sendRedirect("/corp/patients/");
	}
	public String getDetailsFromCliniko(String key) throws Exception{
		CloseableHttpClient httpclient = HttpClients.createDefault();
        try {
            HttpGet httpget = new HttpGet("https://api.cliniko.com/v1/patients");
            String userPassword = key + ":" + "";
	        String encodedString = Base64.encodeBase64String(userPassword.getBytes());
	        httpget.addHeader("Authorization", "Basic " + encodedString);
	        httpget.addHeader("Accept", "application/json");

            // Create a custom response handler
            ResponseHandler<String> responseHandler = new ResponseHandler<String>() {

                @Override
                public String handleResponse(
                        final HttpResponse response) throws ClientProtocolException, IOException {
                    int status = response.getStatusLine().getStatusCode();
                    if (status >= 200 && status < 300) {
                        HttpEntity entity = response.getEntity();
                        return entity != null ? EntityUtils.toString(entity) : null;
                    } else {
                        throw new ClientProtocolException("Unexpected response status: " + status);
                    }
                }

            };
            return httpclient.execute(httpget, responseHandler);
            } finally {
            httpclient.close();
        }
     
	}
	public void getAndParseJSON(String response, long corp_id, long userid) throws Exception {
		Connection conn = Util.getConnection();
		PreparedStatement pst = null;
    	try {
    		JSONArray jsonPatArr = new JSONObject(response).optJSONArray("patients");
    		for (int k=0; k<jsonPatArr.length(); k++) {
    			JSONObject jsonObject = jsonPatArr.getJSONObject(k);
    			Patient p = new Patient();
    			JSONArray phone = jsonObject.getJSONArray("patient_phone_numbers");
    	        pst = conn.prepareStatement("select id,fname,lname,email,phone from patient where cliniko_id= ? and corp_id= ?");
    	        pst.setLong(1, jsonObject.getLong("id"));
    	        pst.setLong(2, corp_id);
    	        ResultSet rs = pst.executeQuery();
    	        if(rs.next()){ 
    	        	if(!rs.getString("fname").equals(jsonObject.getString("first_name"))){
    	        		p.setCreated(new Date());
                    	p.setCreator(userid);
                    	p.setCorpId(corp_id);
                    	p.setId(rs.getLong("id"));
                    	p.setFirstName(jsonObject.getString("first_name"));
                    	p.setLastName(jsonObject.getString("last_name"));
                    	if (jsonObject.optString("email") != null){
                    		p.setEmail(jsonObject.optString("email").toLowerCase());
                    	} else {
                    		p.setEmail("");
                    	}
                    	p.setReference("Cliniko");
                    	if (phone.length()!=0){
                    		p.setPhone(phone.getJSONObject(0).getString("number"));
                    	} else {
                    		p.setPhone("");
                    	}
                    	p.setNew_patient("Updated_Patient");
                    	p.setCliniko_id(jsonObject.getLong("id"));
                    	p.setCheck_patient_exist("Check");
                    	try {
                    		new LibraryManager().save(p);
                    	} catch (Exception e) {
                    		e.printStackTrace();
                    	}
    	        		
    	        	} else if(!rs.getString("lname").equals(jsonObject.getString("last_name"))){
    	        		p.setCreated(new Date());
                    	p.setCreator(userid);
                    	p.setCorpId(corp_id);
                    	p.setId(rs.getLong("id"));
                    	p.setFirstName(jsonObject.getString("first_name"));
                    	p.setLastName(jsonObject.getString("last_name"));
                    	if (jsonObject.optString("email") != null){
                    		p.setEmail(jsonObject.optString("email").toLowerCase());
                    	} else {
                    		p.setEmail("");
                    	}
                    	p.setReference("Cliniko");
                    	if (phone.length()!=0){
                    		p.setPhone(phone.getJSONObject(0).getString("number"));
                    	} else {
                    		p.setPhone("");
                    	}
                    	p.setNew_patient("Updated_Patient");
                    	p.setCliniko_id(jsonObject.getLong("id"));
                    	p.setCheck_patient_exist("Check");
                    	try {
                    		new LibraryManager().save(p);
                    	} catch (Exception e) {
                    		e.printStackTrace();
                    	}
    	        	} else if(!rs.getString("email").equals(jsonObject.optString("email").toLowerCase())){
    	        		p.setCreated(new Date());
                    	p.setCreator(userid);
                    	p.setCorpId(corp_id);
                    	p.setId(rs.getLong("id"));
                    	p.setFirstName(jsonObject.getString("first_name"));
                    	p.setLastName(jsonObject.getString("last_name"));
                    	if (jsonObject.optString("email") != null){
                    		p.setEmail(jsonObject.optString("email").toLowerCase());
                    	} else {
                    		p.setEmail("");
                    	}
                    	p.setReference("Cliniko");
                    	if (phone.length()!=0){
                    		p.setPhone(phone.getJSONObject(0).getString("number"));
                    	} else {
                    		p.setPhone("");
                    	}
                    	p.setNew_patient("Updated_Patient");
                    	p.setCliniko_id(jsonObject.getLong("id"));
                    	p.setCheck_patient_exist("Check");
                    	try {
                    		new LibraryManager().save(p);
                    	} catch (Exception e) {
                    		e.printStackTrace();
                    	}
    	        	} else if(phone.length() == 0){
    	        		if(!rs.getString("phone").equals("")){
	        			p.setCreated(new Date());
	                	p.setCreator(userid);
	                	p.setCorpId(corp_id);
	                	p.setId(rs.getLong("id"));
	                	p.setFirstName(jsonObject.getString("first_name"));
	                	p.setLastName(jsonObject.getString("last_name"));
	                	if (jsonObject.optString("email") != null){
	                		p.setEmail(jsonObject.optString("email").toLowerCase());
	                	} else {
	                		p.setEmail("");
	                	}
	                	p.setReference("Cliniko");
	                	if (phone.length()!=0){
	                		p.setPhone(phone.getJSONObject(0).getString("number"));
	                	} else {
	                		p.setPhone("");
	                	}
	                	p.setNew_patient("Updated_Patient");
	                	p.setCliniko_id(jsonObject.getLong("id"));
	                	p.setCheck_patient_exist("Check");
	                	try {
	                		new LibraryManager().save(p);
	                	} catch (Exception e) {
//	                		e.printStackTrace();
	                	}
    	        		}
	        		} else if(phone.length()!=0){
    	        		if(!rs.getString("phone").equals(phone.optJSONObject(0).getString("number"))){
    	        			p.setCreated(new Date());
    	                	p.setCreator(userid);
    	                	p.setCorpId(corp_id);
    	                	p.setId(rs.getLong("id"));
    	                	p.setFirstName(jsonObject.getString("first_name"));
    	                	p.setLastName(jsonObject.getString("last_name"));
    	                	if (jsonObject.optString("email") != null){
    	                		p.setEmail(jsonObject.optString("email").toLowerCase());
    	                	} else {
    	                		p.setEmail("");
    	                	}
    	                	p.setReference("Cliniko");
    	                	if (phone.length()!=0){
    	                		p.setPhone(phone.getJSONObject(0).getString("number"));
    	                	} else {
    	                		p.setPhone("");
    	                	}
    	                	p.setNew_patient("Updated_Patient");
    	                	p.setCliniko_id(jsonObject.getLong("id"));
    	                	p.setCheck_patient_exist("Check");
    	                	try {
    	                		new LibraryManager().save(p);
    	                	} catch (Exception e) {
    	                		e.printStackTrace();
    	                	}
    	        		} 
    	        	}
    				
    	        } else {
    				try{
    					p.setCreated(new Date());
                    	p.setCreator(userid);
                    	p.setCorpId(corp_id);
                    	p.setId(0);
                    	p.setFirstName(jsonObject.getString("first_name"));
                    	p.setLastName(jsonObject.getString("last_name"));
                    	if (jsonObject.optString("email") != null){
                    		p.setEmail(jsonObject.optString("email").toLowerCase());
                    	} else {
                    		p.setEmail("");
                    	}
                    	p.setReference("Cliniko");
                    	if (phone.length()!=0){
                    		p.setPhone(phone.getJSONObject(0).getString("number"));
                    	} else {
                    		p.setPhone("");
                    	}
                    	p.setNew_patient("New_Patient");
                    	p.setCliniko_id(jsonObject.getLong("id"));
                    	p.setCheck_patient_exist("Check");
                    	try {
                    		new LibraryManager().save(p);
                    	} catch (Exception e) {
                    		e.printStackTrace();
                    	}	
    				} catch (Exception e){
    					e.printStackTrace();
    				}
    			}
    		}

    	} catch (Exception ex) {
    		ex.printStackTrace();
    	} finally {
            if (conn != null) {
            	pst.close();
                conn.close();
            }
        }
    }
}
