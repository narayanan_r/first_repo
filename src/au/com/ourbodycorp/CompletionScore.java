package au.com.ourbodycorp;

import au.com.ourbodycorp.model.BarchartData;
import au.com.ourbodycorp.model.Patient;
import au.com.ourbodycorp.model.Program;
import au.com.ourbodycorp.model.managers.CorporationManager;
import au.com.ourbodycorp.model.managers.LibraryManager;
import au.com.ourbodycorp.model.managers.UserManager;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.IOException;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CompletionScore
  extends HttpServlet
{
  private static final long serialVersionUID = 1L;
  
  public CompletionScore() {}
  
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException
  {
    try
    {
      String programId = request.getParameter("programId");
      String patientId = request.getParameter("patientId");
      String fromDate = request.getParameter("fromDate");
      String toDate = request.getParameter("toDate");
      if (programId == null) {
        response.sendError(400, "No programId provided");
        return;
      }
      if (patientId == null) {
        response.sendError(400, "No patientId provided");
        return;
      }
      LibraryManager lm = new LibraryManager();
      UserManager um = new UserManager();
      CorporationManager cm = new CorporationManager();
      Program program = lm.getProgram(Integer.parseInt(programId));
      Patient patient = lm.getPatient(Integer.parseInt(patientId));
      if (program == null) {
        response.sendError(404, "Unknown program ID");
      }
      if (patient == null) {
        response.sendError(404, "Unknown program ID");
      }
      
      SimpleDateFormat dateformet = new SimpleDateFormat("dd/mm/yy");
      Date startDate = dateformet.parse(fromDate);
      Date endDate = dateformet.parse(toDate);
      System.out.println(startDate);
      System.out.println(endDate);
      if (!fromDate.equals(dateformet.format(startDate))) {
        startDate = null;
        System.out.println(startDate);
      }
      if (!toDate.equals(dateformet.format(endDate))) {
        endDate = null;
        System.out.println(endDate);
      }
      System.out.println(endDate);
      if ((startDate == null) && (endDate == null))
      {
        response.sendError(400, "Invalid date format");
        return;
      }
      ArrayList<BarchartData> pe = lm.saveTotalDuration(Integer.parseInt(programId), dateformet.format(startDate), dateformet.format(endDate));
      ArrayList<BarchartData> scoresAvg = lm.scoreAvg(Integer.parseInt(programId), dateformet.format(startDate), dateformet.format(endDate));
      ArrayList<BarchartData> painLevelAvg = lm.painLevelAvg(Integer.parseInt(programId), dateformet.format(startDate), dateformet.format(endDate));
      LinkedHashMap<String, Object> main = new LinkedHashMap();
      LinkedList<LinkedHashMap<String, Object>> scoreList = new LinkedList();
      
      main.put("CompletionScore", scoreList);
      SimpleDateFormat formatter; for (BarchartData adherence : scoresAvg) {
        formatter = new SimpleDateFormat("dd/MM/yyyy");
        LinkedHashMap<String, Object> score = new LinkedHashMap();
        score.put("Program Id", programId);
        score.put("Patient Id", patientId);
        score.put("Adherence score", adherence.getAdhenceAvg());
        score.put("Frequency score", adherence.getFreqAvg());
        score.put("Frequency per day", adherence.getProgramFreqValue());
        score.put("dateDone", adherence.getDateDone());
        scoreList.add(score);
        System.out.println("adhere avg::::" + adherence.getAdhenceAvg());
        System.out.println("frq avg::::" + adherence.getProgramFreqValue());
        System.out.println("dateDone avg::::" + adherence.getDateDone());
      }
      LinkedList<LinkedHashMap<String, Object>> painLevelAvgList = new LinkedList();
      main.put("Pain Level List", painLevelAvgList);
      for (BarchartData pain : painLevelAvg) {
        SimpleDateFormat formatters = new SimpleDateFormat("dd/MM/yyyy");
        LinkedHashMap<String, Object> painLevel = new LinkedHashMap();
        painLevel.put("Pain level score", pain.getPainlevlAvg());
        painLevel.put("dateDone", pain.getDateDone());
        painLevelAvgList.add(painLevel);
      }
      LinkedList<LinkedHashMap<String, Object>> notesList = new LinkedList();
      main.put("Notes List", notesList);
      for (BarchartData note : pe) {
        SimpleDateFormat formatters = new SimpleDateFormat("dd/MM/yyyy");
        LinkedHashMap<String, Object> notes = new LinkedHashMap();
        notes.put("Program Id", programId);
        notes.put("Patient Id", patientId);
        notes.put("Note", note.getNote());
        notes.put("dateDone", note.getDateDone());
        notesList.add(notes);
      }
      
      Gson gson = new GsonBuilder().disableHtmlEscaping().setPrettyPrinting().create();
      String json = gson.toJson(main);
      response.setContentType("application/json");
      byte[] out = json.getBytes();
      response.setContentLength(out.length);
      response.getOutputStream().write(out);
    }
    catch (Exception localException) {}
  }
}
