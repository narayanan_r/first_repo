/*
 * Developed and maintained by SAPOL IS&T Applications Branch.
 */
package au.com.ourbodycorp;

/**
 * InvalidInputException.
 *
 * @author PD98191
 * @version 1.0.0
 */
public class InvalidInputException extends Exception {
  public InvalidInputException() {
  }

  public InvalidInputException(String s) {
    super(s);
  }

  public InvalidInputException(String s, Throwable throwable) {
    super(s, throwable);
  }

  public InvalidInputException(Throwable throwable) {
    super(throwable);
  }
}
