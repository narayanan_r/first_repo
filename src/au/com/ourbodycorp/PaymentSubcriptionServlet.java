package au.com.ourbodycorp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;

import au.com.ourbodycorp.model.PaymentSubscription;
import au.com.ourbodycorp.model.managers.LibraryManager;

public class PaymentSubcriptionServlet extends HttpServlet   {

	/**
	  * 
	  */
	 private static final long serialVersionUID = 1L;

	 protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		 LibraryManager lm=new LibraryManager();
		 
		  StringBuffer jb = new StringBuffer();
		  String line = null;
		  try {
		    BufferedReader reader = request.getReader();
		    while ((line = reader.readLine()) != null)
		      jb.append(line);
		  } catch (Exception e) { /*report an error*/ }

		  try {
		 //   JSONObject jsonString=  HTTP.toJSONObject(jb.toString());
		    JSONObject jsonObject = new JSONObject(jb.toString());
		
		    String totalAmount = jsonObject.getString("totalAmount");
	    	String subscriptionid = jsonObject.getString("subscriptionId");
	    	String transactionId = jsonObject.getString("transactionId");
		    String transStatus = jsonObject.getString("transStatus");
			String email = jsonObject.getString("email");
			String corpId = null;
			if(jb.toString().contains("corpId")){
				corpId = jsonObject.getString("corpId");
			}
			if (totalAmount != null && subscriptionid != null && transactionId != null && transStatus != null
					&&  email != null) {				
	    	PaymentSubscription p= lm.savePaymentSubscription(totalAmount,subscriptionid,transactionId,transStatus,email, corpId);	
	    	if(p!=null){
	    	response.setStatus(200);
	 	    response.setContentType("application/json");
	 	    PrintWriter out = response.getWriter();
	 		out.print(new Gson().toJson(p));
	 		out.flush();
	    	}else{
	    		response.setStatus(500);
		 	    response.setContentType("application/json");
		 	    PrintWriter out = response.getWriter();
		 		out.print(new Gson().toJson("Internal Server error"));
		 		out.flush();
	    	}
			}
		  } catch (JSONException e) {
		    // crash and burn
		    throw new IOException("Error parsing JSON request string");
		  } 
		  catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
}
