package au.com.ourbodycorp;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ibm.icu.math.BigDecimal;

public class HTML {

  public static String submitButton(String name) {
    return "<div class=\"form-actions\">\n" +
        "            <button name=\"submit\" type=\"submit\" value=\"" + name + "\" class=\"btn btn-primary\">" + name + "</button>\n" +
        "          </div>";
  }

  public static String saveAndClose() {
    return "<div class=\"form-actions\">\n" +
        "            <button name=\"submit\" type=\"submit\" value=\"Save\" class=\"btn btn-primary\">Save</button>\n" +
        "            <button name=\"submit\" type=\"submit\" value=\"Save and Close\" class=\"btn btn-primary\">Save and Close</button>\n" +
        "          </div>";
  }

  public static String saveCancel() {
    return "<div class=\"form-actions\">\n" +
        "            <button name=\"submit\" type=\"submit\" value=\"Save\" class=\"btn btn-primary\">Save</button>\n" +
        "            <button name=\"submit\" type=\"submit\" value=\"Cancel\" class=\"btn\">Cancel</button>\n" +
        "          </div>";
  }

  public static String confirmCancel() {
    return "<div class=\"form-actions\">\n" +
        "            <button name=\"submit\" type=\"submit\" value=\"Confirm\" class=\"btn btn-primary\">Confirm</button>\n" +
        "            <button name=\"submit\" type=\"submit\" value=\"Cancel\" class=\"btn\">Cancel</button>\n" +
        "          </div>";
  }
  
  public static String loginAndClose() {
	    return "<div class=\"form-actions\">\n" +
	        "            <button name=\"submit\" type=\"submit\" onclick=\"Login();\" value=\"Login\" class=\"btn btn-primary\">Return To Fiizio App Login</button>\n" +
	        "            <button name=\"submit\" type=\"submit\" onclick=\"Exit();\" value=\"Exit\" class=\"btn\">Exit and Close This Session</button>\n" +
	        "          </div>";
	  }

  public static String textInput(String name, String label, String size, String maxlen, String value) {
    return HTML.textInput(name, label, size, maxlen, value, null, false);
  }
  public static String textInput(String name, String label, String size, String maxlen, String value, String help, boolean isError) {
    String error = "";
    if (isError) {
      error = " error";
    }
    String s =  String.format("<div class=\"control-group\"><label class=\"control-label\" for=\"%s\">%s</label><div class=\"controls\"><input id=\"%s\" class=\"input-xlarge%s\" type=\"text\" size=\"%s\" maxlength=\"%s\" name=\"%s\" value=\"%s\"/>",
        name, label, name, error, size, maxlen, name, Util.formValue(value));
    if (help != null) {
      s += "<span class=\"help-inline\">" + Util.notNull(help) + "</span>";
    }
    s += "</div></div>";
    return s;
  }

  public static String textInput(String name, String label, String size, String maxlen, String value, String help, Map<String, String> errors) {
    String error = "";
    if (errors != null && errors.containsKey(name)) {
      error = " error";
    }
    String s =  String.format("<div class=\"control-group%s\"><label for=\"%s\" class=\"control-label\">%s</label><div class=\"controls\"><input class=\"input-xlarge%s\" id=\"%s\" type=\"text\" size=\"%s\" maxlength=\"%s\" name=\"%s\" value=\"%s\"/>",
        error, name, label, error, name, size, maxlen, name, Util.formValue(value));
    if (help != null) {
      s += "<span class=\"help-inline\">" + Util.notNull(help) + "</span>";
    } else if (errors != null && errors.containsKey(name)) {
      s += "<span class=\"help-inline\">" + Util.notNull(errors.get(name)) + "</span>";
    }
    s += "</div></div>";
    return s;
  }
  
  public static String textNameAlphabetsInput(String name, String label, String size, String maxlen, String value, String help, Map<String, String> errors, String isAlphabets) {
	    String error = "";
	    if (errors != null && errors.containsKey(name)) {
	      error = " error";
	    }
	    String s =  String.format("<div class=\"control-group%s\" ><label for=\"%s\" class=\"control-label\">%s</label><div class=\"controls\"><input class=\"input-xlarge%s\" id=\"%s\" type=\"text\" size=\"%s\" maxlength=\"%s\" name=\"%s\" value=\"%s\" onkeypress=\"%s\"/>",
	        error, name, label, error, name, size, maxlen, name, Util.formValue(value), isAlphabets);
	    if (help != null) {
	      s += "<span class=\"help-inline\">" + Util.notNull(help) + "</span>";
	    } else if (errors != null && errors.containsKey(name)) {
	      s += "<span class=\"help-inline\">" + Util.notNull(errors.get(name)) + "</span>";
	    }
	    s += "</div></div>";
	    return s;
	  }
  
  public static String textNumericInput(String name, String label, String size, String maxlen, String value, String help, Map<String, String> errors, String isAlphabets) {
	    String error = "";
	    if (errors != null && errors.containsKey(name)) {
	      error = " error";
	    }
	    String s =  String.format("<div class=\"control-group%s\" ><label for=\"%s\" class=\"control-label\">%s</label><div class=\"controls\"><input class=\"input-xlarge%s\" id=\"%s\" type=\"text\" size=\"%s\" maxlength=\"%s\" name=\"%s\" value=\"%s\" onkeypress=\"%s\"/>",
	        error, name, label, error, name, size, maxlen, name, Util.formValue(value), isAlphabets);
	    if (help != null) {
	      s += "<span class=\"help-inline\">" + Util.notNull(help) + "</span>";
	    } else if (errors != null && errors.containsKey(name)) {
	      s += "<span class=\"help-inline\">" + Util.notNull(errors.get(name)) + "</span>";
	    }
	    s += "</div></div>";
	    return s;
	  }
  
  public static String textInputNumeric(String name, String label, String size, String maxlen, String value, String help, boolean isError, String isnumeric) {
	    String error = "";
	    if (isError) {
	      error = " error";
	    }
	    String s =  String.format("<div class=\"control-group\"><label class=\"control-label\" for=\"%s\">%s</label><div class=\"controls\"><input id=\"%s\" class=\"input-xlarge%s\" type=\"text\" size=\"%s\" maxlength=\"%s\" name=\"%s\" value=\"%s\" onkeypress=\"%s\"/>",
	        name, label, name, error, size, maxlen, name, Util.formValue(value), isnumeric);
	    if (help != null) {
	      s += "<span class=\"help-inline\">" + Util.notNull(help) + "</span>";
	    }
	    s += "</div></div>";
	    return s;
	  }
  
  public static String textInputNumericBigDec(String name, String label, String size, String maxlen, java.math.BigDecimal value, String help, boolean isError, String isnumeric) {
	    String error = "";
	    if (isError) {
	      error = " error";
	    }
	    String s =  String.format("<div class=\"control-group\"><label class=\"control-label\" for=\"%s\">%s</label><div class=\"controls\"><input id=\"%s\" class=\"input-xlarge%s\" type=\"text\" size=\"%s\" maxlength=\"%s\" name=\"%s\" value=\"%s\" onkeypress=\"%s\"/>",
	        name, label, name, error, size, maxlen, name, value, isnumeric);
	    if (help != null) {
	      s += "<span class=\"help-inline\">" + Util.notNull(help) + "</span>";
	    }
	    s += "</div></div>";
	    return s;
	  }
  
  public static String textBillingCardName(String name, String label, String size, String maxlen, String value, String help, Map<String, String> errors, String placeholder, String isalphabets) {
	    String error = "";
	    if (errors != null && errors.containsKey(name)) {
	      error = " error";
	    }
	    String s =  String.format("<div class=\"control-group%s\"><label for=\"%s\" class=\"control-label\">%s</label><div class=\"controls\"><input class=\"input-xlarge%s\" id=\"%s\" type=\"text\" size=\"%s\" maxlength=\"%s\" name=\"%s\" value=\"%s\" placeholder=\"%s\"  onkeypress=\"%s\" autocomplete=\"off\"/>",
	        error, name, label, error, name, size, maxlen, name, Util.formValue(value), placeholder, isalphabets);
	    if (help != null) {
	      s += "<span class=\"help-inline\">" + Util.notNull(help) + "</span>";
	    } else if (errors != null && errors.containsKey(name)) {
	      s += "<span class=\"help-inline\">" + Util.notNull(errors.get(name)) + "</span>";
	    }
	    s += "</div></div>";
	    return s;
	  }
  
  public static String textBillingCardInput(String name, String label, String size, String maxlen, String value, String help, Map<String, String> errors, String placeholder, String onchange, String isnumeric) {
	    String error = "";
	    if (errors != null && errors.containsKey(name)) {
	      error = " error";
	    }
	    String s =  String.format("<div class=\"control-group%s\" style=\"width: 460px; margin: 0; float: left;\"><label for=\"%s\" class=\"control-label\">%s</label><div class=\"controls\"><input class=\"input-xlarge%s\" id=\"%s\" type=\"text\" size=\"%s\" maxlength=\"%s\" name=\"%s\" value=\"%s\" placeholder=\"%s\" onchange=\"%s\" onkeypress=\"%s\" autocomplete=\"off\"/>",
	        error, name, label, error, name, size, maxlen, name, Util.formValue(value), placeholder, onchange, isnumeric);
	    if (help != null) {
	      s += "<span class=\"help-inline\">" + Util.notNull(help) + "</span>";
	    } else if (errors != null && errors.containsKey(name)) {
	      s += "<span class=\"help-inline\">" + Util.notNull(errors.get(name)) + "</span>";
	    }
	    s += "</div></div>";
	    return s;
	  }
  
  public static String textBillingCardMMInput(String name, String label, String size, String maxlen, String value, String help, Map<String, String> errors, String placeholder, String isnumeric) {
	    String error = "";
	    if (errors != null && errors.containsKey(name)) {
	      error = " error";
	    }
	    String s =  String.format("<div class=\"control-group%s\" style=\"float: left; margin: 0; width: 130px; padding: 0px 0px 20px;\"><label for=\"%s\" class=\"control-label\" style=\"text-align: left; width: 50px;\">%s</label><div class=\"controls\" style=\"margin-left: 29px;\"><input class=\"input-mini%s\" id=\"%s\" type=\"text\" size=\"%s\" maxlength=\"%s\" name=\"%s\" value=\"%s\" placeholder=\"%s\" onkeypress=\"%s\" autocomplete=\"off\"/>",
	        error, name, label, error, name, size, maxlen, name, Util.formValue(value), placeholder, isnumeric);
	    if (help != null) {
	      s += "<span class=\"help-inline\">" + Util.notNull(help) + "</span>";
	    } else if (errors != null && errors.containsKey(name)) {
	      s += "<span class=\"help-inline\">" + Util.notNull(errors.get(name)) + "</span>";
	    }
	    s += "</div></div>";
	    return s;
	  }
  
  public static String textBillingCardYYInput(String name, String label, String size, String maxlen, String value, String help, Map<String, String> errors, String placeholder, String isnumeric) {
	    String error = "";
	    if (errors != null && errors.containsKey(name)) {
	      error = " error";
	    }
	    String s =  String.format("<div class=\"control-group%s\" style=\"margin-bottom: 0px;\"><label for=\"%s\" class=\"control-label\" style=\"display: none;\">%s</label><div class=\"controls\" style=\"margin-left: 0px; padding: 0 0 20px;\"><input class=\"input-mini%s\" id=\"%s\" type=\"text\" size=\"%s\" maxlength=\"%s\" name=\"%s\" value=\"%s\" placeholder=\"%s\" onkeypress=\"%s\" autocomplete=\"off\"/>",
	        error, name, label, error, name, size, maxlen, name, Util.formValue(value), placeholder, isnumeric);
	    if (help != null) {
	      s += "<span class=\"help-inline\">" + Util.notNull(help) + "</span>";
	    } else if (errors != null && errors.containsKey(name)) {
	      s += "<span class=\"help-inline\">" + Util.notNull(errors.get(name)) + "</span>";
	    }
	    s += "</div></div>";
	    return s;
	  }
  
  public static String textBillingCardCVVInput(String name, String label, String size, String maxlen, String value, String help, Map<String, String> errors, String placeholder, String isnumeric) {
	    String error = "";
	    if (errors != null && errors.containsKey(name)) {
	      error = " error";
	    }
	    String s =  String.format("<div class=\"control-group%s\" style=\"margin: 0; width: 495px; padding: 0px 0px 20px;\"><label for=\"%s\" class=\"control-label\" style=\"width:80px;\">%s</label><div class=\"controls\" style=\"margin-left: 0px;\"><input class=\"input-mini%s\" id=\"%s\" type=\"text\" size=\"%s\" maxlength=\"%s\" name=\"%s\" value=\"%s\" placeholder=\"%s\" onkeypress=\"%s\" autocomplete=\"off\"/>",
	        error, name, label, error, name, size, maxlen, name, Util.formValue(value), placeholder, isnumeric);
	    if (help != null) {
	      s += "<span class=\"help-inline\">" + Util.notNull(help) + "</span>";
	    } else if (errors != null && errors.containsKey(name)) {
	      s += "<span class=\"help-inline\">" + Util.notNull(errors.get(name)) + "</span>";
	    }
	    s += "</div></div>";
	    return s;
	  }
  
  public static String textBillingSelectedCardCVVInput(String name, String label, String size, String maxlen, String value, String help, Map<String, String> errors, String placeholder, String isnumeric) {
	    String error = "";
	    if (errors != null && errors.containsKey(name)) {
	      error = " error";
	    }
	    String s =  String.format("<div class=\"control-group%s\"><label for=\"%s\" class=\"control-label\" style=\"display: none;\">%s</label><div class=\"controls\"><input class=\"input-mini%s\" id=\"%s\" type=\"text\" size=\"%s\" maxlength=\"%s\" name=\"%s\" value=\"%s\" placeholder=\"%s\" onkeypress=\"%s\" autocomplete=\"off\"/>",
	        error, name, label, error, name, size, maxlen, name, Util.formValue(value), placeholder, isnumeric);
	    if (help != null) {
	      s += "<span class=\"help-inline\">" + Util.notNull(help) + "</span>";
	    } else if (errors != null && errors.containsKey(name)) {
	      s += "<span class=\"help-inline\">" + Util.notNull(errors.get(name)) + "</span>";
	    }
	    s += "</div></div>";
	    return s;
	  }

  public static String textArea(String name, String label, String value, int rows) {
    return textArea(name, label, value, "input-xlarge", rows);
  }

  public static String textArea(String name, String label, String value, String size, int rows) {
    return "<div class=\"control-group\">\n" +
        "            <label class=\"control-label\" for=\"" + name + "\">" + label + "</label>\n" +
        "            <div class=\"controls\">\n" +
        "              <textarea name=\"" + name + "\" class=\"" + size + "\" id=\"" + name + "\" rows=\"" + rows + "\">" +Util.textAreaValue(value) + "</textarea>\n" +
        "            </div>\n" +
        "          </div>";
  }
  
  public static String textArea(String name, String label, String value, int rows, String placeholder) {
    return textArea(name, label, value, "input-xlarge", rows, placeholder);
  }
  
  public static String textArea(String name, String label, String value, String size, int rows, String placeholder) {
    return "<div class=\"control-group\">\n" +
        "            <label class=\"control-label\" for=\"" + name + "\">" + label + "</label>\n" +
        "            <div class=\"controls\">\n" +
        "              <textarea name=\"" + name + "\" class=\"" + size + "\" id=\"" + name + "\" rows=\"" + rows + "\" placeholder=\""+placeholder+"\">" +Util.textAreaValue(value) + "</textarea>\n" +
        "            </div>\n" +
        "          </div>";
  }

  public static String passwordInput(String name, String label, String size, String maxlen, String value, String help) {
    return passwordInput(name, label, size,maxlen, value, help, null);
  }

  public static String passwordInput(String name, String label, String size, String maxlen, String value, String help, Map<String, String> errors) {
    String error = "";
    if (errors != null && errors.containsKey(name)) {
      error = " error";
    }
    String s =  String.format("<div class=\"control-group%s\"><label class=\"control-label\" for=\"%s\">%s</label><div class=\"controls\"><input id=\"%s\" class=\"input-xlarge%s\" type=\"password\" size=\"%s\" maxlength=\"%s\" name=\"%s\" value=\"%s\"/>",
        error, name, label, name, error, size, maxlen, name, Util.formValue(value));
    if (help != null) {
      s += "<span class=\"help-inline\">" + Util.notNull(help) + "</span>";
    } else if (errors != null && errors.containsKey(name)) {
      s += "<span class=\"help-inline\">" + Util.notNull(errors.get(name)) + "</span>";
    }
    s += "</div></div>";
    return s;
  }

  public static String select(String name, String label, Map<String, String> options, String selected, String helpText, Map<String, String> errors, boolean startBlank) {
    String[] values = new String[options.size()];
    String[] opts = new String[options.size()];
    int i = 0;
    for (Map.Entry<String, String> entry : options.entrySet()) {
      values[i] = entry.getKey();
      opts[i] = entry.getValue();
      i++;
    }
    return select(name, label, values, opts, selected, helpText, errors, startBlank);
  }

  public static String selectMini(String name, String label, String values[], String options[], String selected, String helpText, Map<String, String> errors, boolean startBlank) {
    boolean alreadySelected = false;
    boolean isError = (errors != null && errors.containsKey(name));
    StringBuilder sb = new StringBuilder();
    if (isError) {
      sb.append("<div class=\"control-group error\">");
    } else {
      sb.append("<div class=\"control-group\">");
    }
    sb.append("<label class=\"control-label\">").append(label).append("</label>");
    sb.append("<div class=\"controls\"><select name=\"").append(name).append("\"");
    sb.append(" class=\"input-mini");
    if (isError) {
      sb.append(" error");
    }
    sb.append("\"");
    sb.append(">");
    if (startBlank && selected != null && !selected.equals("")) {
      sb.append("<option value=\"\"></option>");
    } else if (startBlank) {
      sb.append("<option value=\"\" selected></option>");
      alreadySelected = true;
    }
    for (int i = 0; i < values.length; i++) {
      if (options[i].equals("*-")) {
        sb.append("</optgroup>");
      } else if (options[i].startsWith("*")) {
        sb.append("<optgroup label=\"").append(options[i].substring(1)).append("\">");
      } else {
        sb.append("<option value=\"").append(Util.formValue(values[i])).append("\"");
        if (!alreadySelected && values[i].equals(selected)) {
          sb.append(" selected");
          alreadySelected = true;
        }
        sb.append(">").append(options[i]).append("</option>");
      }
    }

    sb.append("</select>");
    if (helpText != null) {
      sb.append("<span class=\"help-inline\">").append(helpText).append("</span>");
    } else if (isError) {
      sb.append("<span class=\"help-inline\">").append(errors.get(name)).append("</span>");
    }
    sb.append("</div></div>");

    return sb.toString();
  }
  
  public static String selectMinis(String name, String label, String values[], String options[], String selected, String helpText, Map<String, String> errors, boolean startBlank) {
	    boolean alreadySelected = false;
	    boolean isError = (errors != null && errors.containsKey(name));
	    StringBuilder sb = new StringBuilder();
	    if (isError) {
	      sb.append("<div class=\"control-group error\">");
	    } else {
	      sb.append("<div style=\"margin-left: 53px;\"  class=\"control-group left-align\">");
	    }
	    
	    sb.append("<label class=\"control-label \">").append(label).append("</label>");
	    sb.append("<div class=\"controls\"><select style=\"margin-left: 10px;\" name=\"").append(name).append("\"");
	    sb.append(" class=\"input-mini");
	    if (isError) {
	      sb.append(" error");
	    }
	    sb.append("\"");
	    sb.append(">");
	    if (startBlank && selected != null && !selected.equals("")) {
	      sb.append("<option value=\"\"></option>");
	    } else if (startBlank) {
	      sb.append("<option value=\"\" selected></option>");
	      alreadySelected = true;
	    }
	    for (int i = 0; i < values.length; i++) {
	      if (options[i].equals("*-")) {
	        sb.append("</optgroup>");
	      } else if (options[i].startsWith("*")) {
	        sb.append("<optgroup label=\"").append(options[i].substring(1)).append("\">");
	      } else {
	        sb.append("<option value=\"").append(Util.formValue(values[i])).append("\"");
	        if (!alreadySelected && values[i].equals(selected)) {
	          sb.append(" selected");
	          alreadySelected = true;
	        }
	        sb.append(">").append(options[i]).append("</option>");
	      }
	    }

	    sb.append("</select>");
	    if (helpText != null) {
	      sb.append("<span class=\"help-inline\">").append(helpText).append("</span>");
	    } else if (isError) {
	      sb.append("<span class=\"help-inline\">").append(errors.get(name)).append("</span>");
	    }
	    sb.append("</div></div>");

	    return sb.toString();
	  }
  
  public static String selectCardTypeMini(String name, String label, String values[], String options[], String selected, String helpText, Map<String, String> errors, boolean startBlank) {
	    boolean alreadySelected = false;
	    boolean isError = (errors != null && errors.containsKey(name));
	    StringBuilder sb = new StringBuilder();
	    if (isError) {
	      sb.append("<div class=\"control-group error\">");
	    } else {
	      sb.append("<div class=\"control-group\" style=\"width: 270px; float: left;\">");
	    }
	    sb.append("<label class=\"control-label\">").append(label).append("</label>");
	    sb.append("<div class=\"controls\" style=\"width: 260px;\"><select id=\"billing-cardtype\" style=\"width:100px;\" name=\"").append(name).append("\"");
	    sb.append(" class=\"input-mini");
	    if (isError) {
	      sb.append(" error");
	    }
	    sb.append("\"");
	    sb.append(">");
	    if (startBlank && selected != null && !selected.equals("")) {
	      sb.append("<option value=\"\"></option>");
	    } else if (startBlank) {
	      sb.append("<option value=\"\" selected></option>");
	      alreadySelected = true;
	    }
	    for (int i = 0; i < values.length; i++) {
	      if (options[i].equals("*-")) {
	        sb.append("</optgroup>");
	      } else if (options[i].startsWith("*")) {
	        sb.append("<optgroup label=\"").append(options[i].substring(1)).append("\">");
	      } else {
	        sb.append("<option value=\"").append(Util.formValue(values[i])).append("\"");
	        if (!alreadySelected && values[i].equals(selected)) {
	          sb.append(" selected");
	          alreadySelected = true;
	        }
	        sb.append(">").append(options[i]).append("</option>");
	      }
	    }

	    sb.append("</select>");
	    if (helpText != null) {
	      sb.append("<span class=\"help-inline\">").append(helpText).append("</span>");
	    } else if (isError) {
	      sb.append("<span class=\"help-inline\">").append(errors.get(name)).append("</span>");
	    }
	    sb.append("</div></div>");

	    return sb.toString();
	  }

  public static String select(String name, String label, String values[], String options[], String selected, String helpText, Map<String, String> errors, boolean startBlank) {
    boolean alreadySelected = false;
    boolean isError = (errors != null && errors.containsKey(name));
    StringBuilder sb = new StringBuilder();
    if (isError) {
      sb.append("<div class=\"control-group error\">");
    } else {
      sb.append("<div class=\"control-group\">");
    }
    sb.append("<label class=\"control-label\">").append(label).append("</label>");
    sb.append("<div class=\"controls\"><select name=\"").append(name).append("\"");
    if (isError) {
      sb.append(" class=\"error\"");
    }
    sb.append("class=\"input-xlarge\">");
    if (startBlank && selected != null && !selected.equals("")) {
      sb.append("<option value=\"\"></option>");
    } else if (startBlank) {
      sb.append("<option value=\"\" selected></option>");
      alreadySelected = true;
    }
    for (int i = 0; i < values.length; i++) {
    	if(options[i]!=null&&values[i]!=null){
    		 if (options[i].equals("*-")) {
    		      } else if (options[i].startsWith("*")) {
    		        sb.append("<optgroup label=\"").append(options[i].substring(1)).append("\">");
    		      } else {
    		        sb.append("<option value=\"").append(Util.formValue(values[i])).append("\"");
    		        if (!alreadySelected && values[i].equals(selected)) {
    		          sb.append(" selected");
    		          alreadySelected = true;
    		        }
    		        sb.append(">").append(options[i]).append("</option>");
    		      }		
    	}
     
    }

    sb.append("</select>");
    if (helpText != null) {
      sb.append("<span class=\"help-inline\">").append(helpText).append("</span>");
    } else if (isError) {
      sb.append("<span class=\"help-inline\">").append(errors.get(name)).append("</span>");
    }
    sb.append("</div></div>");

    return sb.toString();
  }

  public static String checkboxList(String label, List<CheckboxListItem> items, String help) {
    StringBuilder sb = new StringBuilder();

    sb.append("<div class=\"control-group\">");
    sb.append("<label for=\"optionsCheckboxes\" class=\"control-label\">").append(label).append("</label>");
    sb.append("<div class=\"controls\">");
    for (CheckboxListItem item : items) {
      sb.append("<label class=\"checkbox\">");
      sb.append("<input type=\"checkbox\" name=\"").append(item.getName())
          .append("\" value=\"").append(Util.formValue(item.getValue())).append("\"");
      if (item.selected) {
        sb.append(" checked");
      }
      sb.append(">").append(item.getLabel()).append("</label>");
    }
    if (help != null) {
      sb.append("<span class=\"help-block\">")
          .append(help).append("</span>");
    }
    sb.append("</div></div>");
    return sb.toString();
  }

  public static class CheckboxListItem {
    private String label;
    private String value;
    private String name;
    private boolean selected;

    public CheckboxListItem(String label, String value, String name, boolean selected) {
      this.label = label;
      this.value = value;
      this.name = name;
      this.selected = selected;
    }

    public String getLabel() {
      return label;
    }

    public String getValue() {
      return value;
    }

    public String getName() {
      return name;
    }
  }

  public static String pages(List<String> pages, long selected) {
    if (selected > pages.size() || pages.size() ==  1) return "";
    selected = selected - 1;
    long total = pages.size();
    long start = 0;
    long end = 20;
    long first = -1;
    long last = -1;
    long eitherSide = 4;


    start = selected - eitherSide;
    if (start < 0) start = 0;

    end = start + (eitherSide * 2);
    if (end > total -1) end = total - 1;

    start = end - eitherSide * 2;
    if (start < 0) start = 0;



    if (start > 0) {
      first = 0;
    }
    if (end < total -1) {
      last = total - 1;
    }

    StringBuilder sb = new StringBuilder();
    sb.append("<div class=\"pagination\"><ul>\n");
    if (selected > 0) {
      sb.append("<li class=\"prev\"><a href=\"").append(pages.get((int) selected - 1)).append("\">&larr; Previous</a></li>\n");
    } else {
      sb.append("<li class=\"prev disabled\"><a href=\"#\">&larr; Previous</a></li>\n");
    }
    if (first > -1) {
      sb.append("<li><a href=\"").append(pages.get(0)).append("\">First</a></li>\n");
    }
    int i = -1;
    for (String page : pages) {
      i++;
      if (i < start) continue;
      if (i > end) break;
      if (i != selected) {
        sb.append("<li>");
      } else {
        sb.append("<li class=\"active\">");
      }
      sb.append("<a href=\"").append(pages.get(i)).append("\">").append(i+1).append("</a></li>\n");
    }
    if (last > -1) {
      sb.append("<li><a href=\"").append(pages.get((int) last)).append("\">Last</a></li>\n");
    }

    if (selected < total - 1) {
      sb.append("<li class=\"next\"><a href=\"").append(pages.get((int) selected + 1)).append("\">Next &rarr;</a></li>\n");
    } else {
      sb.append("<li class=\"next disabled\"><a href=\"#\">Next &rarr;</a></li>\n");
    }

    sb.append("</ul></div>\n");
    return sb.toString();
  }

  public static String helpText(String field, HashMap<String, String> errorFields) {
    if (errorFields.containsKey(field)) {
      return "<span class=\"help-inline\">"+ errorFields.get(field) + "</span>";
    } else {
      return "";
    }
  }

  public static String fileInput(String name, String label, String helpText) {
    return "<div class=\"control-group\">\n" +
        "            <label class=\"control-label\" for=\"" + name + "\">" + label + "</label>\n" +
        "            <div class=\"controls\">\n" +
        "              <input class=\"input-file\" id=\"" + name + "\" type=\"file\" name=\"" + name + "\">\n" +
        ((helpText != null) ? "<p class=\"help-block\">" + helpText + "</p>" : "") +
        "            </div>\n" +
        "          </div>";

  }

}
