package au.com.ourbodycorp;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import au.com.ourbodycorp.model.Corporation;
import au.com.ourbodycorp.model.CorporationUser;
import au.com.ourbodycorp.model.Exercise;
import au.com.ourbodycorp.model.User;
import au.com.ourbodycorp.model.managers.LibraryManager;

public class CreateExercise extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String name = Util.notNull(req.getParameter("name"));
		String repsvalue = req.getParameter("reps");
		String side = Util.notNull(req.getParameter("side"));
		String repsperseconds = req.getParameter("repsperseconds");
		String sets = req.getParameter("sets");
		String rest = req.getParameter("rest");
		String type = Util.notNull(req.getParameter("type"));

		System.out.println("name : " + name);
		System.out.println("repsvalue : " + repsvalue);
		System.out.println("sidevalue : " + side);
		System.out.println("repsperseconds : " + repsperseconds);
		System.out.println("sets : " + sets);
		System.out.println("rest : " + rest);
		System.out.println("type : " + type);

		CorporationUser cu = (CorporationUser) req.getAttribute("corpUser");
		User u = cu.getUser();
		Exercise e = null;
		long uId = 0;
		LibraryManager lm = new LibraryManager();
		Corporation corp = cu.getCorporation();
		String title = "Create Exercise";
		try {
			e = new Exercise();
			e.setCreator(u.getId());
			e.setCorpId(corp.getId());
			e.setName(name);
			e.setType(type);
			e.setReps(Integer.parseInt(repsvalue));
			e.setHolds(Integer.parseInt(repsperseconds));
			e.setRest(Integer.parseInt(rest));
			e.setSide(Util.safeInput(side, true));
			e.setShareid(uId);
			lm.save(e, corp.getCompanyId());
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			System.out.println(e1);
		}
		if (req.getParameter("submit").contains("Close")) {
			String bodyPart = "";
			if (e.getBodyParts() != null && e.getBodyParts().size() > 0) {
				bodyPart = e.getBodyParts().getFirst().name();
			}

			String type2 = "";
			if (e.getTypes() != null && e.getTypes().size() > 0) {
				type2 = e.getTypes().getFirst().name();
			}

			resp.sendRedirect("/corp/library/?bp=" + bodyPart + "&type=" + type2);
		} else {
			resp.sendRedirect("/corp/library/create_excercise.jsp?id=" + e.getId());
		}

	}

}
