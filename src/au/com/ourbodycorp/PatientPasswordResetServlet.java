package au.com.ourbodycorp;

import au.com.ourbodycorp.model.managers.LibraryManager;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedHashMap;
import java.util.Properties;
import java.util.UUID;

public class PatientPasswordResetServlet extends HttpServlet{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {
        	String username = request.getParameter("username");
                
            if (username == null || username.trim().length() == 0) {
                response.sendError(400, "No Username provided");
                return;
            }
            
            LibraryManager lm = new LibraryManager();
            LinkedHashMap<String, Object> main = new LinkedHashMap<String, Object>();
            
            if(lm.getPatientEmailExist(username)){
//            	System.out.println("Login Success...");
            	String key = UUID.randomUUID().toString();
    		    Connection conn = Util.getConnection();
    		    PreparedStatement pst = conn.prepareStatement("update patient set password = ? where email = ?");
                pst.setString(1, key);
                pst.setString(2, username);
                pst.executeUpdate();
                pst.close();
                PreparedStatement pst1 = conn.prepareStatement("select fname, lname from patient where email = ?");
                pst1.setString(1, username);
                ResultSet rs = pst1.executeQuery();
                Properties props = new Properties();
                if(rs.next()){
                	props.put("firstName", rs.getString("fname"));
                	props.put("lastName", rs.getString("lname"));
                }
    	        props.put("email", username);
    	        String url = null;
    	        if(Config.getString("https").equals("true")){
    	        	url = Config.getString("url.secure");
    	        }else{
    	        	url = Config.getString("url");
    	        }
    	        props.put("url", url + "/users/password_reset.jsp?key=" + key);
    	        MailMessage msg = new MailMessage(props, "password_reset.vm", username, "Password reset request",false);
    	        pst1.close();
                conn.close();
    	        try {
    	            msg.send();
    	        }
    	        catch (Exception e) {
    	        	
    	        }
    	        main.put("result", "Password reset email sent");
            }else{
            	main.put("result", "This user does not exist");
            }
            
            
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            String json = gson.toJson(main);
            
            response.setContentType("application/json;charset=utf-8");
            byte[] out = json.getBytes("UTF-8");
            response.setContentLength(out.length);
            response.getOutputStream().write(out);
        
        

        }
        catch (Exception e) {
            throw new ServletException(e);
        }
    }
}
