package au.com.ourbodycorp;

import au.com.ourbodycorp.model.Blog;
import au.com.ourbodycorp.model.Post;
import au.com.ourbodycorp.model.managers.PostManager;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

public class RssServlet extends HttpServlet {
  SimpleDateFormat df = new SimpleDateFormat("EEE', 'dd' 'MMM' 'yyyy' 'HH:mm:ss' 'Z", Locale.US);

  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    String[] parts = request.getRequestURI().split("/");
    String file = parts[parts.length - 1];
    int index = file.indexOf(".");
    String slug = (index > 0) ? file.substring(0, index) : file;

    try {
      PostManager pm = new PostManager();
      Blog blog = pm.getBlog(slug);
      if (blog == null) {
        response.sendError(404);
        return;
      }
      Post anchor = pm.getLivePost(blog.getPostId());

      List<Post> posts = pm.getBlogPosts(blog.getId(), 100);


      Document doc = DocumentHelper.createDocument();
      Element root = doc.addElement("rss");
      root.addAttribute("version", "2.0");
      Element channel = root.addElement("channel");
      channel.addElement("title").addText(blog.getName());
      channel.addElement("link").addText(Config.getString("url") + anchor.getUrl());
      channel.addElement("description").addText(blog.getTagline());

      for (Post post : posts) {
        Element item = channel.addElement("item");
        item.addElement("title").addText(post.getTitle());
        item.addElement("link").addText(Config.getString("url") + post.getBlogUrl(anchor));
        item.addElement("guid").addText(Config.getString("url") + post.getBlogUrl(anchor));
        item.addElement("pubDate").addText(df.format(post.getPosted()));
        item.addElement("description").addText(post.getExcerpt());
      }

      StringWriter sw = new StringWriter();
      OutputFormat format = OutputFormat.createPrettyPrint();
      XMLWriter writer = new XMLWriter(sw, format);
      writer.write(doc);

      byte[] bytes = sw.toString().getBytes("UTF-8");

      response.setContentType("application/rss+xml");
      response.setContentLength(bytes.length);
      response.getOutputStream().write(bytes);

    } catch (Exception e) {
      throw new ServletException(e);
    }
  }
}
