package au.com.ourbodycorp;

import java.io.*;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.Date;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import au.com.ourbodycorp.model.Corporation;
import au.com.ourbodycorp.model.CorporationUser;
import au.com.ourbodycorp.model.Exercise;
import au.com.ourbodycorp.model.Program;
import au.com.ourbodycorp.model.ProgramExercise;
import au.com.ourbodycorp.model.User;
import au.com.ourbodycorp.model.managers.LibraryManager;

import java.util.ArrayList;
import java.util.List;

// Extend HttpServlet class
public class AddExercise extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {		
		CorporationUser cu = (CorporationUser) req.getAttribute("corpUser");
	    User u = cu.getUser();
	    Corporation corp = cu.getCorporation();
		String exerciseIds = req.getParameter("excerciseid");
		String programidstr = req.getParameter("pgid");
		long programid = Long.valueOf(programidstr);
		String[] exId = exerciseIds.split(",");
		Long[] exerciseId = new Long[exId.length];
		for (int i = 0; i < exId.length; i++) {
			exerciseId[i] = Long.valueOf(exId[i]);
		}

		List<Long> exerciseIdList = new ArrayList<Long>();
		for (Long excerid : exerciseId) {

			exerciseIdList.add(excerid);
		}
		LibraryManager lm = new LibraryManager();
		Program program = null;
		try {
			program = lm.getProgram(programid);
		
		for (Long Eid : exerciseIdList) {
			Exercise ex = lm.getExercise(Eid);
			ProgramExercise pe = new ProgramExercise();
		    pe.setExerciseId(ex.getId());
			pe.setProgramId(Eid);
			pe.setName(ex.getName());
			pe.setDescription(ex.getDescription());
			pe.setType("exercise");
			pe.setSequence(lm.nextExerciseSequence(Eid));
			pe.setBase(ex.getType());
			pe.setShowSides(ex.isShowSides());
			pe.setSets(ex.getSets());
			pe.setReps(ex.getReps());
			pe.setHolds(ex.getHolds());
			pe.setRest(ex.getRest());			
			pe.setSide(ex.getSide());
			pe.setDescription(ex.getDescription());
			pe.setProgramId(programid);
			pe = lm.save(pe);
		} 
		
		program.setLastUpdated(new Date());
		lm.save(program);
		}catch (Exception e) {
			e.printStackTrace();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		long p=program.getId();
		//System.out.println("program id"+p);
		resp.sendRedirect("/corp/programs/edit.jsp?id="+p);
	}
}
