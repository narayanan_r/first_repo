package au.com.ourbodycorp;

import au.com.ourbodycorp.model.Company;
import au.com.ourbodycorp.model.Corporation;
import au.com.ourbodycorp.model.CorporationUser;
import au.com.ourbodycorp.model.Patient;
import au.com.ourbodycorp.model.Program;
import au.com.ourbodycorp.model.User;
import au.com.ourbodycorp.model.managers.CorporationManager;
import au.com.ourbodycorp.model.managers.LibraryManager;
import au.com.ourbodycorp.model.managers.UserManager;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ExportPracticesServlet extends HttpServlet{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            response.setHeader("Content-Disposition", "attachment; filename=\"practice_details.csv\"");
            response.setContentType("text/csv");
            
            LibraryManager libraryManager = new LibraryManager();
            List<Company> companies = libraryManager.listCompanies(0, 0);
            
            CorporationManager corporationManager = new CorporationManager();
            UserManager userManager = new UserManager();

            //output as CSV file
            SimpleDateFormat sdf = new SimpleDateFormat("d MMM yy");
            response.getOutputStream().println("Sign up date,Practice name,Contact name,Contact phone number,Contact email,Plan type,Number of users,Total patients,Trial progress,Program downloaded,Country");
            for (Company company : companies){
                List<Corporation> corporationsInCompany = corporationManager.getCorporationsForCompany(company.getId());
                for (Corporation corp : corporationsInCompany){
                    response.getOutputStream().print((corp.getCreated()== null)? "-" : sdf.format(corp.getCreated()) + ",");
                    response.getOutputStream().print("\""+ corp.getName() + "\",");
                    response.getOutputStream().print("\""+ company.getContact()+ "\",");
                    response.getOutputStream().print("\""+ company.getPhone() + "\",");
                    response.getOutputStream().print("\""+ company.getEmail()+ "\",");
                    response.getOutputStream().print(corp.getSubType() + ",");
                    response.getOutputStream().print(libraryManager.countOfPractitioners(corp.getId()) + ",");
                    response.getOutputStream().print(libraryManager.countPatients(corp.getId()) + ",");
                    
                    List<CorporationUser> users = corporationManager.getUsersForCorporation(corp.getId());
                    User firstUser = (users == null || users.size() == 0)? null : users.get(0).getUser(); 
                    int tutorialProgress = (firstUser == null)? UserManager.TUTORIAL_NOT_DONE :  userManager.getTutorialProgressForUser(firstUser.getId()); 
                    String progress = "N/A";
                    if (UserManager.TUTORIAL_NOT_DONE == tutorialProgress){
                        progress = "Tutorial Not Started";
                    }
                    else if (UserManager.TUTORIAL_WENT_TO_PATIENT_PAGE == tutorialProgress){
                        progress = "Tutorial Part 2";
                    }
                    else if (UserManager.TUTORIAL_WENT_TO_PROGRAM_PAGE == tutorialProgress){
                        progress = "Tutorial Part 3";
                    }
                    else if (UserManager.TUTORIAL_FINISHED == tutorialProgress){
                        progress = "Tutorial Finished";
                    }
                    response.getOutputStream().print(progress + ",");
                    
                    String programDownloadDate = "Never";
                    List<Patient> patients = libraryManager.getPatients(corp.getId(), 0, 1);
                    Patient firstPatient = (patients != null && patients.size() > 0)? patients.get(0) : null;
                    if (firstPatient != null){
                         List<Program> programs = libraryManager.getPrograms(firstPatient.getId());
                         if (programs != null && programs.size() > 0){
                             Program firstProgram = programs.get(0);
                             if (firstProgram.getRetrieved() != null){
                                 programDownloadDate = sdf.format(firstProgram.getRetrieved());
                             }
                         }
                    }
                    response.getOutputStream().print(programDownloadDate + ",");
                    
                    response.getOutputStream().print(corp.getCountry());
                    response.getOutputStream().println();
                }
                        
                
            }
        }
        catch (Exception e) {
            throw new ServletException(e);
        }
    }
}
