package au.com.ourbodycorp;

import au.com.ourbodycorp.model.Post;
import au.com.ourbodycorp.model.managers.PostManager;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * ${NAME}.
 *
 * @author PD98191
 * @version 1.0.0
 */
public class PostServlet extends HttpServlet {
  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    doGet(request, response);
  }

  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    String uri = request.getRequestURI();
    int id = 0;
    String[] parts = uri.split("/");

    if (uri.startsWith("/go/") && parts.length == 3) {
      try {
        id = Integer.parseInt(parts[2].trim());
      } catch (NumberFormatException e) {
        // must be short code
      }
        try {
            PostManager pm = new PostManager();
            Post p = null;
          if (id > 0) {
            p = pm.getPost(id);
          } else {
            p = pm.getPost(parts[2].trim());
          }
            if (p != null) {
                response.sendRedirect(p.getUrl());
                return;
            }
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    if (parts.length == 3) {
      try {
        id = Integer.parseInt(parts[2].split("-")[0]);
      } catch (Exception e) {
        // Illegal ID passed, pretend the page doesn't exist at all
        response.sendError(404);
        return;
      }
    }

    Post.PostType type = Post.PostType.valueOf(parts[1]);
    if (type != null) {
      String servlet = type.name();
      if (id == 0) {
        servlet += "_index";
      }
      request.getRequestDispatcher("/WEB-INF/servlets/" + servlet + ".jsp?id=" +id).forward(request, response);
    } else {
      response.sendError(404);
    }
    
  }
}
