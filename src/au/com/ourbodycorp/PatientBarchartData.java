package au.com.ourbodycorp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;

import au.com.ourbodycorp.model.BarchartData;
import au.com.ourbodycorp.model.ExerciseMedia;
import au.com.ourbodycorp.model.Program;
import au.com.ourbodycorp.model.ProgramExercise;
import au.com.ourbodycorp.model.managers.CorporationManager;
import au.com.ourbodycorp.model.managers.LibraryManager;
import au.com.ourbodycorp.model.managers.UserManager;

public class PatientBarchartData extends HttpServlet {

	 private static final long serialVersionUID = 1L;

	 protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		 LibraryManager lm=new LibraryManager();
		 
		  StringBuffer jb = new StringBuffer();
		  String line = null;
		  try {
		    BufferedReader reader = request.getReader();
		    while ((line = reader.readLine()) != null)
		      jb.append(line);
		  } catch (Exception e) { /*report an error*/ }

		  try {
		    JSONObject jsonObject = new JSONObject(jb.toString());
	    	String duration = jsonObject.getString("duration");
	    	Boolean finished = jsonObject.getBoolean("finished");
	    	String lastProgram = jsonObject.getString("lastProgramId");
	    	String patientId = jsonObject.getString("patientId");
	    	String programId = jsonObject.getString("programId");
	    	String programFreqType = jsonObject.getString("programFreqType");
	    	String painLevel = jsonObject.getString("painLevel");
	    	String programFreqValue = jsonObject.getString("programFreqValue");
	    	String PrgmDoneDate = jsonObject.getString("dateDone");
	    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    	String note=jsonObject.getString("note");
	    	
	    	System.out.println("duration::::::"+duration);
	    	System.out.println("finished::::::"+finished);
	    	System.out.println("lastProgram::::::"+lastProgram);
	    	System.out.println("patientId::::::"+patientId);
	    	System.out.println("programId::::::"+programId);
	    	System.out.println("programFreqType::::::"+programFreqType);
	    	System.out.println("programFreqValue::::::"+programFreqValue);
	    	System.out.println("painLevel::::::"+painLevel);
	    	System.out.println("PrgmDoneDate::::::"+PrgmDoneDate);
	    	Date dateDone = sdf.parse(PrgmDoneDate);
	    	System.out.println("Befor save date entry");
	    	BarchartData barchart= lm.saveProgressReportData(duration,finished,lastProgram,patientId,programId,dateDone,programFreqType,programFreqValue,painLevel,note);
	    	System.out.println("after save date entry");
	    	response.setStatus(200);
	 	    response.setContentType("application/json");
	 	    PrintWriter out = response.getWriter();
	 		out.print(new Gson().toJson(barchart));
	 		out.flush();
	  } catch (JSONException e) {
		    // crash and burn
		    throw new IOException("Error parsing JSON request string");
		    
		  } 
		  catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
	 /*
		protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			try {
				
				String programId = request.getParameter("id");
	            String patientId = request.getParameter("patientid");
	            if (programId==null) {
	                response.sendError(400, "No programId provided");
	                return;
	            }
	            if (patientId==null) {
	                response.sendError(400, "No patientId provided");
	                return;
	            }
  
	            LibraryManager lm = new LibraryManager();
	            UserManager um = new UserManager();
	            CorporationManager cm = new CorporationManager();
	            LinkedHashMap<String, Object> main = new LinkedHashMap<String, Object>();
	        	ArrayList<BarchartData> pe = lm.getBarchartData(programId,patientId );     
	            LinkedHashMap<String, Object> scores = new LinkedHashMap<String, Object>();   
	        	 for (BarchartData score : pe) {
	        		 scores.put("Adherence Score",score.getAdherncescore());
	        		 scores.put("Frequecy score",score.getFrequencyscore());
	        	 }
				main.put("Progress report", scores);
			} catch (Exception e) {
				// TODO: handle exception
			}
		}*/
	}