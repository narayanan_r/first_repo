package au.com.ourbodycorp;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import au.com.ourbodycorp.model.BarchartData;
import au.com.ourbodycorp.model.Patient;
import au.com.ourbodycorp.model.Program;
import au.com.ourbodycorp.model.managers.CorporationManager;
import au.com.ourbodycorp.model.managers.LibraryManager;
import au.com.ourbodycorp.model.managers.UserManager;

/**
 * Servlet implementation class LastBarChartData
 */
public class LastBarChartData extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	
		try
	    {
	      String programId = request.getParameter("programId");
	      String patientId = request.getParameter("patientId");
	      String fromDate = request.getParameter("fromDate");
	      String toDate = request.getParameter("toDate");
	      if (programId == null) {
	        response.sendError(400, "No programId provided");
	        return;
	      }
	      if (patientId == null) {
	        response.sendError(400, "No patientId provided");
	        return;
	      }
	      LibraryManager lm = new LibraryManager();
	      UserManager um = new UserManager();
	      CorporationManager cm = new CorporationManager();
	      Program program = lm.getProgram(Integer.parseInt(programId));
	      Patient patient = lm.getPatient(Integer.parseInt(patientId));
	      if (program == null) {
	        response.sendError(404, "Unknown program ID");
	      }
	      if (patient == null) {
	        response.sendError(404, "Unknown program ID");
	      }
	      
	      SimpleDateFormat dateformet = new SimpleDateFormat("dd/mm/yyyy");
	      Date startDate = dateformet.parse(fromDate);
	      Date endDate = dateformet.parse(toDate);
	      System.out.println(startDate);
	      System.out.println(endDate);
	      if (!fromDate.equals(dateformet.format(startDate))) {
	        startDate = null;
	        System.out.println(startDate);
	      }
	      if (!toDate.equals(dateformet.format(endDate))) {
	        endDate = null;
	        System.out.println(endDate);
	      }
	      System.out.println(endDate);
	      if ((startDate == null) && (endDate == null))
	      {
	        response.sendError(400, "Invalid date format");
	        return;
	      }
	      
	      BarchartData lastbarchartdata = lm.sumOfData(Integer.parseInt(programId), dateformet.format(startDate), dateformet.format(endDate));
	      System.out.println("after list");
	      LinkedHashMap<String, Object> main = new LinkedHashMap();
	      SimpleDateFormat formatter;
	      
	        formatter = new SimpleDateFormat("dd/MM/yyyy");
	        LinkedHashMap<String, Object> score = new LinkedHashMap();
	        score.put("Program Id", programId);
	        score.put("Patient Id", patientId);
	        score.put("Duration", lastbarchartdata.getDuration());
	        score.put("Frequency per day", lastbarchartdata.getProgramFreqValue());
	        score.put("Date Done", lastbarchartdata.getDateDone());
	        main.put("lastBarchartData", score);
	      Gson gson = new GsonBuilder().disableHtmlEscaping().setPrettyPrinting().create();
	      String json = gson.toJson(main);
	      response.setContentType("application/json");
	      byte[] out = json.getBytes();
	      response.setContentLength(out.length);
	      response.getOutputStream().write(out);
	    }
	    catch (Exception localException) {}
	  }
	
	}


