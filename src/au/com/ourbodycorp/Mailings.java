package au.com.ourbodycorp;

import au.com.ourbodycorp.model.Spam;
import au.com.ourbodycorp.model.managers.SpamManager;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Properties;


public class Mailings extends HttpServlet {
  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    String type = request.getParameter("type");

    String title = (request.getParameter("title") != null && request.getParameter("title").trim().length() > 0) ? request.getParameter("title").trim() : null;
    String fname = (request.getParameter("fname") != null && request.getParameter("fname").trim().length() > 0) ? request.getParameter("fname").trim() : null;
    String lname = (request.getParameter("lname") != null && request.getParameter("lname").trim().length() > 0) ? request.getParameter("lname").trim() : null;
    String address1 = (request.getParameter("address1") != null && request.getParameter("address1").trim().length() > 0) ? request.getParameter("address1").trim() : null;
    String address2 = (request.getParameter("address2") != null && request.getParameter("address2").trim().length() > 0) ? request.getParameter("address2").trim() : null;
    String suburb = (request.getParameter("suburb") != null && request.getParameter("suburb").trim().length() > 0) ? request.getParameter("suburb").trim() : null;
    String state = (request.getParameter("state") != null && request.getParameter("state").trim().length() > 0) ? request.getParameter("state").trim() : null;
    String freeState = (request.getParameter("freestate") != null && request.getParameter("freestate").trim().length() > 0) ? request.getParameter("freestate").trim() : null;
    String country = (request.getParameter("country") != null && request.getParameter("country").trim().length() > 0) ? request.getParameter("country").trim() : "Australia";
    String postcode = (request.getParameter("postcode") != null && request.getParameter("postcode").trim().length() > 0) ? request.getParameter("postcode").trim() : null;
    String email = (request.getParameter("email") != null && request.getParameter("email").trim().length() > 0) ? request.getParameter("email").trim() : null;

    if (country != null && !country.equalsIgnoreCase("australia")) {
        state = freeState;
    }

    Spam spam = new Spam();
    spam.setEmail(email);
    spam.setFirstName(fname);
    spam.setLastName(lname);
    spam.setTitle(title);
    spam.setAddress1(address1);
    spam.setAddress2(address2);
    spam.setSuburb(suburb);
    spam.setCountry(country);
    spam.setPostcode(postcode);
    spam.setState(state);

    if (email != null && email.indexOf("@") > 0) {
      spam.setOptInEmail(true);
    }

    String formUrl = null;
    String doneUrl = "/contact/enews/email/";

    LinkedList<String> missingFields = new LinkedList<String>();
    if (spam.getFirstName() == null || spam.getFirstName().length() == 0) missingFields.add("fname");
    if (spam.getLastName() == null || spam.getLastName().length() == 0) missingFields.add("lname");

    if (type.equals("enews")) {
      formUrl = "/contact/enews/";
      doneUrl = "/contact/enews/email/";
      if (spam.getEmail() == null || email.indexOf("@") == 0) missingFields.add("email");
    } else {
      spam.setOptInPost(true);
      formUrl = "/contact/program/";
      if (spam.getAddress1() == null || spam.getAddress1().length() == 0) missingFields.add("address1");
      if (spam.getSuburb() == null || spam.getSuburb().length() == 0) missingFields.add("suburb");
    }

    if (spam.getEmail() != null) {
      String p = "\\A[^@><\"]+@.+\\.[a-z]+\\z";
      if (!spam.getEmail().toLowerCase().matches(p)) {
        missingFields.add("email");
      }
    }

    if (missingFields.size() > 0) {
      request.getSession().setAttribute("spam", spam);
      request.getSession().setAttribute("missingFields", missingFields);
      response.sendRedirect(formUrl);
    } else {
      request.getSession().removeAttribute("spam");
      request.getSession().removeAttribute("missingFields");
      SpamManager sm = new SpamManager();
      try {
        if (spam.isOptInEmail()) {
          sm.save(spam);
          try {
            emailConfirm(spam);
          } catch (Exception ae) {
            missingFields.add("email");
            request.getSession().setAttribute("missingFields", missingFields);
            request.getSession().setAttribute("spam", spam);
            response.sendRedirect(formUrl);
            return;
          }
          if (type.equals("program")) {
            doneUrl = "/contact/program/program_email/";
          }
        } else {
          sm.submitSpam(spam);
          doneUrl = "/contact/program/thanks/";
        }
      } catch (Exception e) {
        throw new ServletException(e);
      }
      response.sendRedirect(doneUrl);
    }

  }

  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    response.sendRedirect("/");
  }

  private void emailConfirm(Spam spam)  throws Exception {
    String url = Config.getString("url") + "/mailings/confirm.jsp?i=" + spam.getId() + "&c=" + spam.getConfirm();

    Properties props = new Properties();
    props.setProperty("fname", Util.notNull(spam.getFirstName()));
    props.setProperty("lname", Util.notNull(spam.getLastName()));
    props.setProperty("url", url);

    MailMessage msg = new MailMessage(props, "enews_confirm.vm", spam.getEmail(), "Please confirm your subscription", Config.getString("email.from"),true);
    msg.send();
  }

}
