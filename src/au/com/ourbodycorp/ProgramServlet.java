package au.com.ourbodycorp;

import au.com.ourbodycorp.model.*;
import au.com.ourbodycorp.model.managers.AttachmentManager;
import au.com.ourbodycorp.model.managers.CorporationManager;
import au.com.ourbodycorp.model.managers.LibraryManager;
import au.com.ourbodycorp.model.managers.UserManager;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

public class ProgramServlet extends HttpServlet {


  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    try {
      String id = request.getParameter("id");
      String username = request.getParameter("username");
      if (id == null || id.trim().length() == 0) {
        response.sendError(400, "No ID provided");
        return;
      }

      LibraryManager lm = new LibraryManager();
      UserManager um = new UserManager();
      CorporationManager cm = new CorporationManager();

      Program p = lm.getProgram(id.toUpperCase());

      if (p == null) {
        response.sendError(404, "Unknown program ID");
        return;
      }

      try {
        p.setRetrieved(new Date());
        lm.saveJSON(p);
      } 
      catch (Exception e) {
        e.printStackTrace();
        //ignore
      }

      List<ProgramExercise> pe = lm.getProgramExercises(p.getId());
      Patient programPatient = lm.getPatient(p.getPatientId());
      PatientGroup programGroup = lm.getPatientGroup(p.getGroupId());

      /*if (username != null) {
        if (!username.trim().equalsIgnoreCase(patient.getUserName())) {
          response.sendError(403);
          return;
        }
      } */

      User physio = um.getUser(p.getCreator());
      Corporation corp = cm.getCorporation(p.getCorpId());

      SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
      sdf.setTimeZone(corp.getTimeZone());

      LinkedHashMap<String, Object> main = new LinkedHashMap<String, Object>();
      main.put("baseUrl", Config.getString("url"));

      String www = Util.notNull(corp.getMetaString("www", "http://www.fiizio.com")).toLowerCase();
      if (www.length() > 0 && !(www.startsWith("http://") || www.startsWith("https://"))) {
        www = "http://" + www;
      }

      String logo = Config.getString("url") + "/images/default-logo.png";
      String logoOrg = Config.getString("url") + "/images/default-logo.png";
      if (corp.getLogoId() > 0) {
        AttachmentManager am = new AttachmentManager();
        Attachment a = am.getAttachment(corp.getLogoId(), false);
        if (a != null) {
          logo = Config.getString("url") + a.getImageUrl(Attachment.ImageSize.logo);
          logoOrg = Config.getString("url") + a.getUrl();
        }
      }

      LinkedHashMap<String, Object> practice = new LinkedHashMap<String, Object>();
      main.put("practice", practice);
      practice.put("name", corp.getName());
      practice.put("slug", corp.getSlug());
      practice.put("www", www);
      practice.put("email", corp.getMetaString("email", "info@fiizio.com"));
      practice.put("phone", corp.getMetaString("phone", "0404835033"));
      practice.put("phone2", corp.getMetaString("phone2"));
      practice.put("hours", corp.getMetaString("hours", "Mon-Friday 8am-5pm"));
      practice.put("bio", corp.getMetaString("bio", "Example Bio. This is where will add your practice information"));
      practice.put("logo", logo);
      practice.put("logo_org", logoOrg);
      practice.put("news", "/mininews.rss?corp=" + corp.getSlug());

      LinkedHashMap<String, String> address = new LinkedHashMap<String, String>();
      practice.put("address", address);
      address.put("line1", corp.getAddress1());
      address.put("line2", corp.getAddress2());
      address.put("suburb", corp.getSuburb());
      address.put("state", corp.getState());
      address.put("postcode", corp.getPostcode());

      LinkedHashMap<String, Object> program = new LinkedHashMap<String, Object>();
      main.put("program", program);
      program.put("name", p.getName());
      program.put("symptom", p.getSymptom());
      program.put("id", p.getCode());
      program.put("programid", p.getId());
      if (programPatient != null) program.put("patient", programPatient.getFirstName());
      if (programGroup != null) program.put("group", programGroup.getGroupName());
      program.put("date", sdf.format(p.getCreated()));
      program.put("updatedDate", sdf.format(p.getLastUpdated()));
      LinkedHashMap<String, String> physioMap = new LinkedHashMap<String, String>();
      physioMap.put("title", physio.getTitle());
      physioMap.put("firstName", physio.getFirstName());
      physioMap.put("lastName", physio.getLastName());
      program.put("physio", physioMap);
      program.put("frequency", p.getInterval());
      program.put("interval", p.getIntervalType().name());


      LinkedList<LinkedHashMap<String,Object>> exercises = new LinkedList<LinkedHashMap<String, Object>>();
      program.put("exercises", exercises);
      LinkedHashMap<String, Object> previous = null;
      for (ProgramExercise e : pe) {
        if (e.getType().equals("rest")) {
          if (previous != null) {
            previous.put("restTime", e.getRest());
          }
        }
        LinkedHashMap<String, Object> ex = new LinkedHashMap<String, Object>();
        previous = ex;
        exercises.add(ex);
        ex.put("exerciseId", e.getExerciseId());
        ex.put("name", e.getName());
        ex.put("Seq", e.getSequence());
        ex.put("base", e.getBase());
        ex.put("isolated", e.getV1SafeSide());
        ex.put("sets", (e.getType().equals("rest")) ? 1 : e.getSets());
        ex.put("reps", (e.getType().equals("rest")) ? 0 : e.getReps());
        if (e.getBase() != null && e.getBase().equals(Exercise.REP_EXERCISE)) {
          ex.put("time", "0");
          ex.put("repTime", e.getHolds());
        } else {
          ex.put("time", (e.getType().equals("rest")) ? e.getRest() : e.getHolds());
          ex.put("repTime", "0");
        }
        ex.put("setRestTime", (e.getType().equals("rest")) ? 0 : e.getRest());
        ex.put("equipment", "");
        ex.put("keyFrame", 0);

        List<ExerciseMedia> media = lm.getExerciseMedia(e.getExerciseId());
        int ii = 0;
        LinkedList<LinkedHashMap<String,Object>> images = new LinkedList<LinkedHashMap<String, Object>>();
        ex.put("images", images);
        if (e.getType().equals("rest")) {
          LinkedHashMap<String, Object> image = new LinkedHashMap<String, Object>();
          images.add(image);
          image.put("url", "/images/rest.jpg");
          image.put("instruction", "");
          ex.put("keyFrame", 0);

        } else {
          for (ExerciseMedia em : media) {
            if (em.getType().equals("photo")) {
              LinkedHashMap<String, Object> image = new LinkedHashMap<String, Object>();
              images.add(image);
              image.put("url", "/media?id="+em.getId());
              image.put("instruction", em.getCaption());
              if (em.isKey()) {
                ex.put("keyFrame", ii);
              }
              ii++;
            } else {
              ex.put("videoUrl", "/media?id=" + em.getId());
            }
          }
        }
      }

      List<MyInfo> myInfos = lm.listInfoForProgram(p.getId());
      if (myInfos.size() > 0) {
        LinkedList<LinkedHashMap<String, String>> infos = new LinkedList<LinkedHashMap<String, String>>();
        for (MyInfo myInfo : myInfos) {
          LinkedHashMap<String, String> info = new LinkedHashMap<String, String>();
          info.put("title", myInfo.getHeadline());
          info.put("url", "/info.jsp?id=" + myInfo.getId());
          infos.add(info);
        }
        main.put("info", infos);
      }

      List<ProgramNote> notes = lm.getNotes(p.getId());
      if (notes.size() > 0) {
        LinkedList<LinkedHashMap<String, String>> n = new LinkedList<LinkedHashMap<String, String>>();
        for (ProgramNote note : notes) {
          LinkedHashMap<String, String> info = new LinkedHashMap<String, String>();
          info.put("date", sdf.format(note.getDate()));
          info.put("note", note.getNote());
          n.add(info);
        }
        main.put("notes", n);
      }


      Gson gson = new GsonBuilder().setPrettyPrinting().create();

      String json = gson.toJson(main);

      response.setContentType("application/json;charset=utf-8");
      byte[] out = json.getBytes("UTF-8");
      response.setContentLength(out.length);
      response.getOutputStream().write(out);
    } catch (Exception e) {
      throw new ServletException(e);
    }
  }
}