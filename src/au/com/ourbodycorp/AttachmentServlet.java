package au.com.ourbodycorp;

import au.com.ourbodycorp.model.Attachment;
import au.com.ourbodycorp.model.managers.AttachmentManager;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.Calendar;

public class AttachmentServlet extends HttpServlet {

  public AttachmentServlet() {

  }

  protected void doHead(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    headOrGet(req, resp);
  }

  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    headOrGet(request, response);
  }

  private void headOrGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    String[] parts = request.getRequestURI().substring(1).split("/");
    if (parts.length < 2) {
      response.sendError(404);
      return;
    }
    long id;

    try {
      id = Long.parseLong(parts[1]);
    } catch (NumberFormatException e) {
      response.sendError(404);
      return;
    }

    AttachmentManager am = new AttachmentManager();

    boolean full = (request.getMethod().equalsIgnoreCase("get"));


    try {
      Attachment a = am.getAttachment(id, full);
      if (a == null) {
        response.sendError(404);
        return;
      }
      byte[] data;

      data = a.getData();

      if (full) {
        if (data == null || data.length == 0) {
          File f = new File("/static", a.getFullFileName());
          response.sendRedirect(f.getCanonicalPath());
          return;
        }
      }

      Calendar c = Calendar.getInstance();
      c.add(Calendar.MONTH, 3);
      response.addDateHeader("Last-Modified", a.getUpdated().getTime());
      response.setDateHeader("Expires", c.getTime().getTime());
      response.addHeader("Cache-Control", "public");
      response.setContentType(Config.getMimeType(a.getExtension()));
      response.setContentLength((data != null) ? data.length : (int) a.getSize());
      if (full) {
        response.getOutputStream().write(data);
      }
    } catch (Exception e) {
      throw new ServletException(e);
    }
  }
}
