package au.com.ourbodycorp;

import au.com.ourbodycorp.model.*;
import au.com.ourbodycorp.model.managers.CorporationManager;
import au.com.ourbodycorp.model.managers.UserManager;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import sun.rmi.runtime.Log;
import sun.util.logging.resources.logging;

import java.io.IOException;
import java.util.List;

public class HostnameFilter implements Filter {
	

    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
    	
        
        HttpServletRequest r = (HttpServletRequest) req;
        HttpServletResponse res = (HttpServletResponse) resp;

        if (r.getHeader("host") == null || r.getHeader("host").trim().length() == 0) {
            res.sendError(400, "HTTP/1.1 Host Header Required");
            return;
        }
        
        if (r.getRequestURI().startsWith("/program.json")) {
            chain.doFilter(req, resp);
        }
        
//        String[] parts = null;
//		
//        if(r.getSession().getAttribute("getSlugUrl") != null){
//        	parts = r.getSession().getAttribute("getSlugUrl").toString().split("\\.");
//        }else{
//        	parts = r.getHeader("host").split("\\.");
//        }
        
        String parts[] = r.getHeader("host").split("\\.");
      
        // Ignore www or equal to start of base domain.
        if (parts[0].equalsIgnoreCase("www")) {
            res.sendRedirect(Config.getString("url"));
            
            return;
        }
        else if (parts[0].equalsIgnoreCase(Config.getString("baseDomain").split("\\.")[0])) {
        	
            chain.doFilter(req, resp);
            return;
        }
       
       try {
            CorporationManager cm = new CorporationManager();
            
            Corporation c = cm.getCorporation(parts[0]);
            
            if (c != null) {
//                if (c.getLocked() != null || c.isExpiredTrial()) {
//                    res.sendRedirect(Config.getString("url") + "/locked.jsp?slug=" + parts[0]);
//                    return;
//                }
//                else if(cm.hasTrialExpired(c.getId())){
//                    res.sendRedirect(Config.getString("url") + "/locked.jsp?slug=" + parts[0]);
//                    return;
//                }
                
                UserManager mm = new UserManager();
                User u = mm.getUserByCorpid(c.getId());
             //   User u = (User) r.getSession().getAttribute("user");
         /*      User userFromMap= UserMap.userMap.get(u.getEmail());
               String sessionId=UserMap.sessionMap.get(u.getEmail());
              System.out.println("host name filter session id from session map:"+sessionId);
              
              
               if(userFromMap!=null){
                r.getSession().setAttribute("user",userFromMap);
              
               }else{
            	 User improperUser=  (User) r.getSession().getAttribute("user");
            	 System.out.println("Removing improper user from sessio map "+improperUser);
            	 if(improperUser!=null){
            	 UserMap.sessionMap.remove(improperUser.getEmail());
            
            	   r.getSession().invalidate();
            	              	
            	 }
            	 String returnUrl = String.valueOf(c.getSlug()) + "." + Config.getString((String)"baseDomain") + r.getRequestURI();
                 returnUrl = Config.getBoolean((String)"https", (boolean)false) ? "https://" + returnUrl : "http://" + returnUrl;
                 res.sendRedirect(Util.loginUrl((Constants.LoginReason)Constants.LoginReason.login, (String)returnUrl, (String)r.getQueryString(), (boolean)true));
                 return;
               }*/

             
                
                if (Constants.TESTING_MODE && u == null){
                    u = new UserManager().getUser(13);
                }
                List<CorporationRole> roles = null;
                if (u != null) {
                    roles = cm.getRolesForUserInCorporation(u.getId(), c.getId());
                    if (u.hasRole("admin")) {
                        CorporationRole ar = new CorporationRole();
                        ar.setId("AD");
                        ar.setName("Admin");
                        if (!roles.contains(ar)) {
                            roles.add(ar);
                        }
                    }
                }
                if (u == null || roles.size() == 0) {
                    String returnUrl = String.valueOf(c.getSlug()) + "." + Config.getString((String)"baseDomain") + r.getRequestURI();
                    returnUrl = Config.getBoolean((String)"https", (boolean)false) ? "https://" + returnUrl : "http://" + returnUrl;
                    res.sendRedirect(Util.loginUrl((Constants.LoginReason)Constants.LoginReason.login, (String)returnUrl, (String)r.getQueryString(), (boolean)true));
                    return;
                }
                CorporationUser cu = new CorporationUser();
                cu.setRoles(roles);
                cu.setUser(u);
                cu.setCorporation(c);
                r.setAttribute("corpUser", cu);
                r.setAttribute("corp", c);
                r.setAttribute("roles", roles);

                if (u.getLastCorp() != c.getId()) {
                    u.setLastCorp(c.getId());
                    UserManager um = new UserManager();
                    um.saveUser(u);
                }

                cu.setSelectedFY(c.getFinancialYear());
                try {
                    String sfy = (String) r.getSession().getAttribute("fy-" + c.getId());
                    cu.setSelectedFY(Integer.parseInt(sfy));
                }
                catch (Exception e) {
                    // don't bother.
                }
               
            }
            else {
            	System.out.println("REDIRECTING TO: " + Config.getString("url"));
                res.sendRedirect(Config.getString("url"));
                return;
            }
        }
        catch (Exception e) {
            throw new ServletException(e);
        }

        chain.doFilter(req, resp);
        
    }

    public void init(FilterConfig config) throws ServletException {
    }
}
