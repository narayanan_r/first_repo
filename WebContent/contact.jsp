<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Our Body Corporate</title>

<link href="/images/app/favicon.png" rel="shortcut icon">

<link href="css/reset.css" rel="stylesheet" type="text/css" />
<link href="css/960_12_col.css" rel="stylesheet" type="text/css" />
<link href="css/layout.css" rel="stylesheet" type="text/css" />
<link href="css/museo.css" rel="stylesheet" type="text/css" />
<link href="css/typography.css" rel="stylesheet" type="text/css" />

<!--[if IE]>
	<link href="css/ie.css" rel="stylesheet" type="text/css" />
<![endif]-->

<script src="js/jquery-1.7.1.min.js" type="text/javascript" ></script>
<script src="js/jquery.easing.1.3.js" type="text/javascript" ></script>
<script src="js/jquery.backgroundPosition.js" type="text/javascript" ></script>
<script src="js/site.js" type="text/javascript" ></script>

</head>

<body class="nojquery">


<div id="header">
	<div id="header_content" class="container_12">
    	<a href="index.jsp" class="grid_4"><img src="images/logo.gif" alt="Our Body Corporate" width="292" height="112" border="0" /></a>
        <div class="grid_8">
        	
            <div id="top">
            	<a href="#" class="signin">sign-in</a>
                <form id="search" method="get" action="#">
                	<div class="label">
                        <label for="searchtext">Search</label>
                        <input type="text" accesskey="s" id="searchtext" name="q" autocomplete="off">
                    </div>
                    <input type="hidden" id="search-section" name="sec" value="global">
               	</form>
                
                <div id="search-icon"></div>
                
            </div>
        	
            <!-- NAVIGATION START -->
            
        	<ul id="nav">
            	<li><a href="index.jsp">About Us</a></li>
                <li><a href="subpage.jsp">How it Works</a></li>
                <li><a href="stratachat.jsp">StrataChat</a></li>
                <li><a href="contact.jsp" class="selected">Contact Us</a></li>
            </ul>
            
            <!-- NAVIGATION END -->
            
            <a href="#" title="Follow us on Facebook" class="social facebook"></a>
            
        </div>        
    </div>
</div>

<div id="header_shadow"></div>

<div id="sub_promo">
	<div class="container_12">
    	<div class="grid_7 prefix_1">
    	<p>Test Drive Now - FREE 8 Week Trial</p>
        </div>
        <div class="grid_3 suffix_1">
            <a class="bigButton"  href="#">Try it for free</a>
        </div>
    </div>
    <div id="jaggy"></div>
</div>

<div id="content"  class="container_12">
	
    <!-- INTRODUCTION START -->

	<div class="intro clearfix">
        <div class="grid_12 introText sub">
        	<h2>Contact details</h2>
            <p><strong>Our Body Corp Pty Ltd</strong></p>
            <p>GPO Box 1948<br />
            Adelaide<br />
            South Australia 5000</p>
            <p>tel: 1800 OURBODYCORP</p>
            <p>e: <a href="mailto:info@ourbodycorp.com.au">info@ourbodycorp.com.au</a></p><br />
            <p>abn: 73 149 576 330</p>
      </div>
    </div>
    
    
  <!-- INTRODUCTION END -->

    
</div>


<div id="call2act">
	<div class="container_12">
    	<div class="grid_7">
            <h2>Have a Question?</h2>
            <p>Feel free to contact us at <a href="mailto:info@ourbodycorp.com.au">info@ourbodycorp.com.au</a> <br />
              or phone us on<strong> 1800 OURBODYCORP</strong></p>
        </div>
       	<div class="grid_5" >
        	<a class="bigButton" href="#">Try it for free</a>
        </div>        
    </div>
</div>

<div id="footer">
    <div class="container_12">
  		<div class="grid_7">
        	<p><strong>Disclaimer:</strong> Our Body Corp Pty Ltd will not be liable for any loss or damage including, without limitation, direct or indirect loss or damage purported to result from the contents of this program.</p>
        </div>
        <div class="grid_3 prefix_2 copyr">
        	<p>Copyright &copy; 2012<br />
			Our Body Corp Pty Ltd. All rights reserved.</p>
        </div>
    </div>
</div>

</body>
</html>