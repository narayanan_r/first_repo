<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.Spam" %>
<%@ page import="java.util.LinkedList" %>
<%
    Spam spam = (Spam) session.getAttribute("spam");
    if (spam == null) {
        spam = new Spam();
    }
    LinkedList<String> missingFields = (LinkedList<String>) session.getAttribute("missingFields");

%>
<form id="programform" name="programform" method="post" action="/mailings/submit">
    <input type="hidden" name="type" value="enews"/>

    <%
        if (missingFields != null && missingFields.size() >0) {
    %>
    <div class="formleft">&nbsp</div>
    <div class="formright"><h3>Please complete all fields</h3></div>
    <%
        }
    %>

    <div class="formleft formreq"><label for="fname">First Name</label></div>
    <div class="formright"><input class="<%=Util.formMissing(missingFields, "fname")%>" type="text" name="fname" id="fname" maxlength="50" value="<%=Util.formValue(spam.getFirstName())%>"/></div>

    <div class="formleft formreq"><label for="lname">Last Name</label></div>
    <div class="formright"><input class="<%=Util.formMissing(missingFields, "lname")%>" type="text" name="lname" id="lname" maxlength="50" value="<%=Util.formValue(spam.getLastName())%>"/></div>


    <div class="formleft formreq"><label for="email">Email</label></div>
    <div class="formright"><input class="<%=Util.formMissing(missingFields, "email")%>" type="text" name="email" id="email" maxlength="100" value="<%=Util.formValue(spam.getEmail())%>"/></div>

    <div class="formleft">&nbsp;</div>
    <div class="formright"><input type="submit" name="submit" id="submit" value="Sign Up" class="arrow-button" /> </div>


</form>
<script type="text/javascript">
    doCountry();
</script>
