<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.Country" %>
<%@ page import="au.com.ourbodycorp.model.Spam" %>
<%@ page import="au.com.ourbodycorp.model.managers.CountryManager" %>
<%@ page import="java.util.LinkedList" %>
<%@ page import="java.util.List" %>
<%
    Spam spam = (Spam) session.getAttribute("spam");
    if (spam == null) {
        spam = new Spam();
        spam.setCountry("Australia");
    }
    LinkedList<String> missingFields = (LinkedList<String>) session.getAttribute("missingFields");

    CountryManager cm = new CountryManager();
    String[][] states = {{"", ""}, {"ACT", "Australian Capital Territory"}, {"NSW", "New South Wales"}, {"NT", "Northern Territory"}, {"QLD", "Queensland"}, {"SA", "South Australia"}, {"TAS", "Tasmania"}, {"VIC", "Victoria"}, {"WA", "Western Australia"}};
    List<Country> countries = cm.listCountries();
%>
<script type="text/javascript">
    function doCountry() {

        var country = document.forms.programform.country.options[document.forms.programform.country.selectedIndex].value;
        if (country == 'Australia') {
            document.getElementById("stateright2").style.display = 'none';
            document.getElementById("stateleft2").style.display = 'none';
            document.getElementById("stateright1").style.display = 'inline';
            document.getElementById("stateleft1").style.display = 'inline';
        } else {
            document.getElementById("stateright1").style.display = 'none';
            document.getElementById("stateleft1").style.display = 'none';
            document.getElementById("stateright2").style.display = 'inline';
            document.getElementById("stateleft2").style.display = 'inline';
        }
    }

</script>
<form id="programform" name="programform" method="post" action="/mailings/submit">
    <input type="hidden" name="type" value="program"/>

    <div class="formleft">&nbsp</div>
    <div class="formright"><h3>Fields marked in bold are required</h3></div>
    <%
        if (missingFields != null && missingFields.size() >0) {
    %>
    <div class="formleft">&nbsp</div>
    <div class="formright"><h3>Please complete all required fields</h3></div>
    <%
        }
    %>

    <div class="formleft"><label for="title">Title (Mr/Mrs/etc)</label></div>
    <div class="formright"><input type="text" name="title" id="title" maxlength="10" value="<%=Util.formValue(spam.getTitle())%>"/></div>

    <div class="formleft formreq"><label for="fname">First Name</label></div>
    <div class="formright"><input class="<%=Util.formMissing(missingFields, "fname")%>" type="text" name="fname" id="fname" maxlength="50" value="<%=Util.formValue(spam.getFirstName())%>"/></div>

    <div class="formleft formreq"><label for="lname">Last Name</label></div>
    <div class="formright"><input class="<%=Util.formMissing(missingFields, "lname")%>" type="text" name="lname" id="lname" maxlength="50" value="<%=Util.formValue(spam.getLastName())%>"/></div>

    <div class="formleft formreq"><label for="address1">Address line 1</label></div>
    <div class="formright"><input class="<%=Util.formMissing(missingFields, "address1")%>" type="text" name="address1" id="address1" maxlength="50" value="<%=Util.formValue(spam.getAddress1())%>" /></div>

    <div class="formleft"><label for="address2">Address line 2</label></div>
    <div class="formright"><input type="text" name="address2" id="address2" maxlength="50" value="<%=Util.formValue(spam.getAddress2())%>" /></div>

    <div class="formleft formreq"><label for="suburb">Suburb / City</label></div>
    <div class="formright"><input class="<%=Util.formMissing(missingFields, "suburb")%>" type="text" name="suburb" id="suburb" maxlength="50" value="<%=Util.formValue(spam.getSuburb())%>"/></div>

    <div class="formleft" style="display: none" id="stateright1"><label for="state">State</label></div>
    <div class="formright" style="display: none" id="stateleft1">
        <select name="state" id="state">
            <%
                for (String [] s : states) {
            %>
            <option value="<%=s[0]%>"<%=(spam.getState() != null && spam.getState().equals(s[0])) ? " selected":""%>><%=s[1]%></option>
            <%
                }
            %>
        </select>
    </div>

    <div class="formleft" style="display: none" id="stateright2"><label for="state2">State / Province</label></div>
    <div class="formright" style="display: none" id="stateleft2">
        <input type="text" name="freestate" id="state2" maxlength="50" value="<%=Util.formValue(spam.getState())%>"/>
    </div>

    <div class="formleft formreq"><label for="country">Country</label></div>
    <div class="formright" id="countrydic">
        <select name="country" id="country" onchange="doCountry();">
            <option value=""></option>
            <%
                for (Country c : countries) {
            %>
            <option value="<%=c.getDisplayName()%>"<%=(spam.getCountry() != null && spam.getCountry().equals(c.getDisplayName())) ? " selected":""%>><%=c.getDisplayName()%></option>
            <%
                }
            %>
        </select>
    </div>

    <div class="formleft"><label for="postcode">Zip / Postcode</label></div>
    <div class="formright"><input type="text" name="postcode" id="postcode" maxlength="10" value="<%=Util.formValue(spam.getPostcode())%>"/></div>

    <div class="formleft"><label for="email">Email</label></div>
    <div class="formright"><input class="<%=Util.formMissing(missingFields, "email")%>" type="text" name="email" id="email" maxlength="100" value="<%=Util.formValue(spam.getEmail())%>"/> Optional, sign up for enews</div>

    <div class="formleft">&nbsp;</div>
    <div class="formright"><input type="submit" name="submit" id="submit" value="Request Program" class="arrow-button" /> </div>


</form>
<script type="text/javascript">
    doCountry();
</script>
