<%
    String uri = request.getRequestURI();
    if (uri.endsWith("/")) uri = uri.substring(0, uri.length() - 1);
%>
<% if (uri.endsWith("contact/enews")) {
%>
<jsp:include page="/WEB-INF/includes/forms/enews.jsp" />
<%
} else if (uri.endsWith("contact/program")) {
%>
<jsp:include page="/WEB-INF/includes/forms/program.jsp" />
<%
}
%>