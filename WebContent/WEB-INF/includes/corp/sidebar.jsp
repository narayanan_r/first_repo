<%@page import="org.bouncycastle.asn1.ocsp.Request"%>
<%@ page import="au.com.ourbodycorp.model.CorporationRole" %>
<%@page import="au.com.ourbodycorp.model.managers.UserManager"%>
<%@ page import="au.com.ourbodycorp.Config" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%@ page import="au.com.ourbodycorp.model.managers.AttachmentManager" %>
<%@ page import="au.com.ourbodycorp.model.managers.CorporationManager" %>
<%@ page import="au.com.ourbodycorp.model.managers.UserManager" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="au.com.ourbodycorp.model.managers.MiniNewsManager" %>
<%@ page import="org.joda.time.DateTime" %>
<%@ page import="org.joda.time.Days" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.*" %>
<%@ page import="java.sql.*" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%!
    String isActive(String key, String uri) {
        return (uri.startsWith(key)) ? " class=\"active\"" : "";
    }

	CorporationManager cm = new CorporationManager();
	
%>
<ul class="nav nav-list">
    <li class="nav-header">
    <%=corp.getName()%>
    </li>
 <%
 Connection conn = Util.getConnection();
 PreparedStatement pst = conn.prepareStatement("select * from patient where reference = 'Cliniko' and corp_id = ? and (new_patient = 'New_Patient' or new_patient = 'Updated_Patient')");
 pst.setLong(1, corp.getId());
 ResultSet rs = pst.executeQuery();
 if(rs.next()){ 
	 session.setAttribute("cliniko_patient", "True");
 }else{
	 session.setAttribute("cliniko_patient", null);
 }
 pst.close();
 conn.close();
if(cm.isExpired(corp.getId())){ %>
<% if (corp.getSubType() != null) {if (cu.hasRole(CorporationRole.Role.Admin)) {%>	
	<li<%=isActive("/corp/subscription/", request.getRequestURI())%> id="subscription_side"><a href="/corp/subscription/"><i class="icon-shopping-cart"></i> Subscription</a></li>
<% }} %>
<%}else{ %>
<li<%=(request.getRequestURI().equalsIgnoreCase("/corp/")) ? " class=\"active\"" : ""%> id="dashboard_side"><a href="/corp/"><i class="icon-home"></i> Dashboardxxxxxxz</a></li>
<% if (cu.hasRole(CorporationRole.Role.Admin)) {%>
    <li<%=isActive("/corp/people/", request.getRequestURI())%> id="members_side"><a href="/corp/people/"><i class="icon-user"></i> Team Membersxxxxxxxzzz</a></li>
<%}%>
<%if(session.getAttribute("cliniko_patient") != null){ %>	
	<%if(session.getAttribute("patient_side") != null){ %> 
	    <li<%=isActive("/corp/patients/", "/corp/patients/")%> id="patients_side"><a href="/corp/patients/"><img width="14" border="0" height="14" style="margin: 0 2px -2px 0;" src="/images/heart.gif"> Patients</a></li>
	<%}else{%>
		<li<%=isActive("/corp/patients/", request.getRequestURI())%> id="patients_side"><a href="/corp/patients/"><img width="14" border="0" height="14" style="margin: 0 2px -2px 0;" src="/images/heart.gif"> Patients</a></li>
	<%}%> 
<%}else{ %>
	<%if(session.getAttribute("patient_side") != null){ %> 
    	<li<%=isActive("/corp/patients/", "/corp/patients/")%> id="patients_side"><a href="/corp/patients/"><i class="icon-heart"></i> Patients</a></li>
	<%}else{%>
		<li<%=isActive("/corp/patients/", request.getRequestURI())%> id="patients_side"><a href="/corp/patients/"><i class="icon-heart"></i> Patients</a></li>
	<%}%>
<%} %>
    <li<%=isActive("/corp/groups/", request.getRequestURI())%> id="groups_side"><a href="/corp/groups/"><i class="icon-list-alt"></i> Patient Groups</a></li>
<%if(session.getAttribute("patient_side") != null){ %>    
    <li<%=isActive("/corp/programs/", "")%> id="templates_side"><a href="/corp/programs/templates.jsp"><i class="icon-file"></i> Templates</a></li>
<%}else{%>
 	<li<%=isActive("/corp/programs/", request.getRequestURI())%> id="templates_side"><a href="/corp/programs/templates.jsp"><i class="icon-file"></i> Templates</a></li>
<%}%>
<% if (cu.hasRole(CorporationRole.Role.Admin)) {%>  
    <li<%=isActive("/corp/news/", request.getRequestURI())%> id="news_side"><a href="/corp/news/"><i class="icon-star"></i> News</a></li>
<%}%>
    <li<%=isActive("/corp/library/", request.getRequestURI())%> id="exercises_side"><a href="/corp/library/"><i class="icon-book"></i> My Exercises</a></li>
    <li<%=isActive("/corp/myinfo/", request.getRequestURI())%> id="articles_side"><a href="/corp/myinfo/"><i class="icon-info-sign"></i> My Articles</a></li>
<%-- <%if(corp.getCliniko_apikey() != null){ %>	
	<li<%=isActive("/corp/integrations/", request.getRequestURI())%> id="integrations_side"><a href="/corp/integrations/"><img width="20" border="0" height="20" style="margin: 0px 0px -6px -4px;" src="/images/shuffle.png"> My Integrations</a></li>
<%}else{ %>
	<li<%=isActive("/corp/integrations/", request.getRequestURI())%> id="integrations_side"><a href="/corp/integrations/"><img width="20" border="0" height="20" style="margin: 0px 0px -6px -4px;" src="/images/integration.gif"> My Integrations</a></li>
<%} %> --%>
<% if (cu.hasRole(CorporationRole.Role.Admin)) {%>
    <li<%=isActive("/corp/admin/", request.getRequestURI())%> id="settings_side"><a href="/corp/admin/"><i class="icon-cog"></i> Settings</a></li>
<% } %>
	<li<%=isActive("/corp/reports/", request.getRequestURI())%> id="reports_side"><a href="/corp/reports/"><i class="icon-list-alt"></i> Reports</a></li>
<%-- <% if (corp.getSubType() != null) {if (cu.hasRole(CorporationRole.Role.Admin)) {%>	 --%>
	<%-- <li<%=isActive("/corp/subscription/", request.getRequestURI())%> id="subscription_side"><a href="/corp/subscription/"><i class="icon-shopping-cart"></i> Subscription</a></li> --%>
<%-- <% }} %> --%>
<%} %>
</ul>

<%
    if (user.getRoles().contains("admin")) {
%>
<ul class="nav nav-list">
<li class="nav-header">
Site Management
</li>
    <li<%=(request.getRequestURI().equalsIgnoreCase("/admin/")) ? " class=\"active\"" : ""%>><a href="<%=Config.getUrl()%>/admin/"><i class="icon-cog"></i> Admin</a></li>
    <li<%=isActive("/admin/library/", request.getRequestURI())%>><a href="<%=Config.getUrl()%>/admin/library/"><i class="icon-book"></i> Library</a></li>
    <li<%=isActive("/admin/myinfo/", request.getRequestURI())%>><a href="<%=Config.getUrl()%>/admin/myinfo/"><i class="icon-info-sign"></i> My Info</a></li>
    <li<%=isActive("/admin/clients/", request.getRequestURI())%>><a href="<%=Config.getUrl()%>/admin/clients/"><i class="icon-th-list"></i> Clients</a></li>
    <li<%=isActive("/admin/planprice/", request.getRequestURI())%>><a href="<%=Config.getUrl()%>/admin/planprice/"><i class="icon-shopping-cart"></i> Plans & Price</a></li>
    <li<%=isActive("/admin/promotion/", request.getRequestURI())%>><a href="<%=Config.getUrl()%>/admin/promotion/"><i class="icon-gift"></i> Promotion</a></li>
    <li<%=isActive("/admin/subscription/index.jsp", request.getRequestURI())%>><a href="<%=Config.getUrl()%>/admin/subscription/index.jsp"><i class="icon-shopping-cart"></i> Manage Subscription Plans</a></li>
    <li<%=isActive("/admin/log/log.jsp", request.getRequestURI())%>><a href="<%=Config.getUrl()%>/admin/log/log.jsp" target="_blank"><i class="icon-pencil"></i> Log</a></li>
   
</ul>
<%
    }
%>
<%session.setAttribute("patient_side", null); %>

