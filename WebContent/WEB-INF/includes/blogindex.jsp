<%@ page import="au.com.ourbodycorp.Dimension" %>
<%@ page import="au.com.ourbodycorp.model.Attachment" %>
<%@ page import="au.com.ourbodycorp.model.Blog" %>
<%
    Post p = (Post) request.getAttribute("post");
    long id = p.getId();
    Post[] crumbs = (Post[]) request.getAttribute("crumbs");
    long topMenuId = crumbs[0].getId();

    Blog blog = (Blog) request.getAttribute("blog");

    List<Post> posts = (List<Post>) request.getAttribute("indexPosts");
    SimpleDateFormat sdf = new SimpleDateFormat("d/M/yyyy");
    Attachment.ImageSize size = Attachment.ImageSize.icon;
%>
<html>
<head><title><%=p.getTitle()%>
</title>
<link rel="alternate" type="application/rss+xml" href="/rss/<%=blog.getSlug()%>.rss" />
</head>
<body>

<h2><%=p.getTitle()%></h2>

<div class="box-2thirds">
    <hr class="top"/>
    <div class="content">

        <ul class="news-list">
            <%
                for (Post post : posts) {
                    int width = 232;
                    int height = 120;
                    Dimension d = null;
                    if (post.getIcon() != null) {
                        d = post.getIcon().estimateSize(size);
                        if (d != null) {
                            width = d.getWidth();
                            height = d.getHeight();
                        }
                    }

            %>
            <li<%=(post == posts.get(posts.size() -1)) ? " class=\"last\"" : ""%>>
                <% if (post.getIcon() != null) {%>
                <a href="<%=post.getBlogUrl(p)%>"><img src="<%=post.getIcon().getImageUrl(size)%>" width="<%=width%>" height="<%=height%>"/></a>
                <% } %>

                <h3><a href="<%=post.getBlogUrl(p)%>"><%=post.getTitle()%></a></h3>

                <p class="date"><%=sdf.format(post.getPosted())%></p>

                <p><%=post.getExcerpt()%></p>
            </li>
            <%
                }
            %>

        </ul>

        <%@include file="share.jsp" %>

    </div>

</div>


<jsp:include page="sidenav.jsp"/>

<%@include file="blogarchivedropdown.jsp"%>


</body>
</html>