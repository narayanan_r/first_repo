<%@ page import="au.com.ourbodycorp.model.Attachment" %>
<%@ page import="au.com.ourbodycorp.model.managers.AttachmentManager" %>
<%@ page import="java.util.List" %>
<%
    long versionId = Long.parseLong(request.getParameter("versionId"));
    AttachmentManager am = new AttachmentManager();
    List<Attachment> attachments = am.getAttachmentsForPost(versionId, Attachment.AttachmentType.file, 0, 0, true);
    if (attachments != null && attachments.size() > 0) {
%>
<div class="box-1third">
    <hr class="top"/>
    <div class="download">
        <!--START DYNAMIC DOWNLOAD ITEMS-->
        <%
            for (Attachment a : attachments) {

        %>
        <a href="<%=a.getUrl()%>"><img src="/images/template/file-<%=a.getExtension()%>.png" alt="Download this as a <%=a.getExtension()%>" width="48" height="48" border="0" class="file-icon"/></a>
        <p><a href="<%=a.getUrl()%>"><%=(a.getCaption() != null) ? a.getCaption() : a.getFilename()%></a><br/>
        <%=a.getSize() / 1024%> kB</p>
        <%=(a != attachments.get(attachments.size() - 1)) ? "<hr/>" : ""%>
        <%
            }
        %>

        <!--END DYNAMIC DOWNLOAD ITEMS-->
    </div>
</div>
<% } %>
