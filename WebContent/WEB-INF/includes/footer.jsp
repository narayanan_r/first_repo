<div id="footer">
    <div class="content">
        <div id="enews">
            <h3>sign up for enews</h3>

            <form id="enews" name="enews" method="post" action="/mailings/submit">
                <input type="hidden" name="type" value="enews"/>
                <p><label for="fname">First name</label><input type="text" name="fname" value="" id="fname"
                                                               autocomplete="off"></p>

                <p><label for="lname">Last name</label><input type="text" name="lname" value="" id="lname"
                                                              autocomplete="off"></p>

                <p><label for="email">Your email address</label><input type="text" name="email" value="" id="email"
                                                                       autocomplete="off"></p>
                <input type="submit" value="sign up" class="arrow-button">
            </form>

        </div>

        <div id="twitter">
            <h3>live tweets</h3>

            <div class="tweets"></div>
            <a href="http://twitter.com/adelaidefest" target="_blank" class="arrow-link">view more</a>
        </div>

        <div id="footer-links">
            <h3>follow us...</h3>
            <a href="http://www.facebook.com/adelaidefestival" target="_blank"><img src="/images/footer/facebook_32.png"
                                                                                    width="32" height="32"
                                                                                    alt="Follow us on Facebook"/></a>
            <a href="http://twitter.com/adelaidefest" target="_blank"><img src="/images/footer/twitter_32.png"
                                                                           width="32" height="32"
                                                                           alt="Follow us on Twitter"/></a>
            <a href="http://www.youtube.com/user/FestivalofArts " target="_blank"><img
                    src="/images/footer/youtube_32.png" width="32" height="32" alt="Youtube Channel"/></a>
            <a href="http://www.flickr.com/photos/adelaidefestival/" target="_blank"><img
                    src="/images/footer/flickr_32.png" width="32" height="32" alt="Follow us on Flickr"/></a>
            <hr/>
            <a href="/contact/program/"><img src="/images/footer/program.gif" width="186" height="25"
                                                 alt="Request a program"/></a>
            <a href="http://2010.adelaidefestival.com.au/servlet/Web?s=2290869&action=changePage&pageID=867309274"><img src="/images/footer/adfest-2010.gif" width="186"
                                                                height="25"
                                                                alt="Link to the Adelaide Festival 2010 website"/></a>
        </div>

        <p id="disclaimer" class="sub-footer">&copy; 2011 Adelaide Festival Corporation | <a href="#">Privacy Policy</a>
        </p>

        <p id="mango" class="sub-footer">Design by <a href="http://www.mangochutney.com.au" target="_blank">Mango
            Chutney</a> | Implemented by <a href="http://www.formicary.net.au" target="_blank">Formicary Australia</a>
        </p>
    </div>
</div>