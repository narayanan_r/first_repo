<%@ page import="au.com.ourbodycorp.Config" %>
<%@ page import="au.com.ourbodycorp.model.Attachment" %>
<%@ page import="au.com.ourbodycorp.model.Post" %>
<%
    Post p = (Post) request.getAttribute("post");
    long id = p.getId();

    Attachment banner = p.getBanner();
    String bannerUrl = "/images/header/flight.jpg";
    String bannerAlt = p.getTitle();
    String bannerLink = null;
    if (banner != null) {
        bannerUrl = banner.getImageUrl(Attachment.ImageSize.banner);
        if (banner.getCaption() != null && banner.getCaption().trim().length() > 0) {
            bannerAlt = banner.getCaption();
        }
        bannerLink = banner.getLink();
    }
    request.setAttribute("bannerUrl", bannerUrl);
    request.setAttribute("bannerAlt", bannerAlt);
    request.setAttribute("bannerbg", p.getBannerBackground());
    request.setAttribute("bannerLink", bannerLink);
%>
<html>
<head><title><%=p.getTitle()%>
</title></head>
<body>
<h2><%=p.getTitle()%></h2>

<div class="box-2thirds">
    <hr class="top"/>
    <div class="content">
        <!--START DYNAMIC SUBPAGE CONTENT-->
        <% if (p.getExcerpt() != null) { %>
        <p class="intro"><%=p.getExcerpt()%></p>
        <% } %>
        <%=p.getBody()%>
        <%@include file="forms/include.jsp"%>
        <!--END DYNAMIC SUBPAGE CONTENT-->

        <%@include file="share.jsp"%>
        <% if (p.isAllowComments()) {
            String disqusId = "adlfest_" + p.getId();
            String permalink = Config.getString("url") + p.getBlogUrl(p);
        %>
        <%@include file="disqus.jsp"%>
        <%}%>
    </div>

</div>

<jsp:include page="sidenav.jsp"/>
<jsp:include page="attachments.jsp">
    <jsp:param name="versionId" value="<%=p.getVersionId()%>"/>
</jsp:include>


</body>
</html>