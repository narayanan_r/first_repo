<%@ include file="/WEB-INF/includes/Analytics.jsp"%>
<%@ page import="au.com.ourbodycorp.model.Constants"%>
<%@ page import="au.com.ourbodycorp.Config" %>
<%@ page import="au.com.ourbodycorp.model.Corporation" %>
<%@ page import="au.com.ourbodycorp.model.User" %>
<%@ page import="au.com.ourbodycorp.model.managers.CorporationManager" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="java.util.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.CorporationUser" %>
<%@ page import="au.com.ourbodycorp.model.CorporationRole" %>
<%@ page import="java.util.List" %>
<%
    User u = (User) session.getAttribute("user");
    Corporation c = (Corporation) request.getAttribute("corp");
    CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");
    List<Corporation> corps = null;
	 if (u != null) {
        CorporationManager cm = new CorporationManager();
        corps = cm.getCorporations(u.getId());
        LibraryManager lm = new LibraryManager();
        session.setAttribute("hasCompanies", lm.hasCompanies(u.getId())); 
    }
    Connection conn = Util.getConnection();
%>

<html>
<head>
<script type="text/javascript">
var action = 1;
function showorhidehome() {
    if ( action == 1 ) {
    	$("#details").hide();
    	$("#home").show();
    	action2 = 1;
        action = 2;
    } else {
    	$("#details").hide();
    	$("#home").hide();
        action = 1;
    }
}
var action2 = 1; 
function showorhidedetails() {
	if ( action2 == 1 ) {
    	$("#home").hide();
    	$("#details").show();
    	action = 1;
        action2 = 2;
    } else {
    	$("#home").hide();
    	$("#details").hide();
        action2 = 1;
    }
}

$('html').click(function (e) {
    if (e.target.id == 'home') {
    } else {
    	$("#home").hide();
    	 action = 1;
    }
});

$('html').click(function (e) {
    if (e.target.id == 'home') {
    } else {
    	$("#details").hide();
    	 action2 = 1;
    }
});


function viewCart(plan,type) {
	<%session.setAttribute("discountError", null);
	session.setAttribute("Expired", null);
	session.setAttribute("Used", null);
	
	if (cu != null) {
  	Corporation corp1 = cu.getCorporation();
    PreparedStatement pst1;
    pst1 = conn.prepareStatement("select plan,type from cart_details where corp_id = ?");
    pst1.setLong(1, corp1.getId());
    ResultSet rs7 = pst1.executeQuery();
    if(rs7.next()){ 
    	session.setAttribute("Cart_Error", "Error");
  	}else{
  		session.setAttribute("Cart_Error", "No_Error");
  	} 
    pst1.close();
	}%>
	window.location.replace("/corp/subscription/cart.jsp?plan="+plan+"&type="+type+"&cart=true");
}


</script>
<script type="text/javascript">
	function slugUrl(slugUrl) {
		$.ajax({
	        url: '<%=Config.getString("url")%>/corp/slugurl_changer.jsp?slugUrl='+slugUrl+'',
	        type: 'POST',
	        success: function(data) {
	      	   window.location = '<%=Config.getString("url")%>/corp/';
	        }
	      });
	}
</script>

<style type="text/css">
.itemCount {
    background: #db6e00 none repeat scroll 0 0;
    border-radius: 2px;
    color: #fff;
    font-size: 11px;
    height: 16px;
    line-height: 16px;
    padding: 0 3px;
    position: absolute;
    right: -2px;
    top: 3px;
}
.icon-envelope {
	animation-name: example;
	animation-duration: .5s;
	animation-iteration-count: infinite;
	animation-timing-function: linear;
}

@-webkit-keyframes example {
    0% { -webkit-transform: scaleY(1.0); }
   50% { -webkit-transform: scaleY(1.5); }
   100% { -webkit-transform: scaleY(1.0); } 
} 

</style>
</head>
<div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container-fluid">
         <div style="float: left; padding:  0; margin: 0px 25px 0px 10px">
          <img src="/images/app/fiizio_logo.png" width="180" height="50" style="">
             </div>

          <ul class="nav">

          </ul>
          <%
              if (u != null) {
          %>

          <ul class="nav">
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" onclick="showorhidehome();"><i class="icon-home icon-white"></i> <%=(c != null) ? c.getName() : "Select Practice"%>
<%-- 			  <a href="#" class="dropdown-toggle" data-toggle="dropdown" onclick="showorhidehome();"><i class="icon-home icon-white"></i><%if(request.getRequestURI().toString().startsWith("/users")){%><%="Select Practice"%><%}else{%><%= c.getName()%><%}%> --%>
              <b class="caret"></b>
              </a>
              <ul class="dropdown-menu" id="home" style="display: none;">
                <% if (corps != null && corps.size() > 0) {
                    for (Corporation corp : corps) {
                    	if(!request.getRequestURI().toString().startsWith("/users")){
                    		if (c != null && c.equals(corp)) {
                                continue;
                            }
                    	}
                  %>
                <%-- <%if (Config.getBoolean("https", false)) {%>
                    <li><a style="cursor: pointer;" onclick="slugUrl('<%=corp.getSlug()%>')"><%=corp.getName()%></a></li>
        		<%}else {%>
        			<li><a style="cursor: pointer;" onclick="slugUrl('<%=corp.getSlug()%>')"><%=corp.getName()%></a></li>
        		<%}%> --%>
                  <li><a href="<%=corp.getUrl()%>"><%=corp.getName()%></a></li>
                  <%
                    }
                %>

                  <%
                      }
                  %>
                  <%
                      boolean hasCompanies = (Boolean) session.getAttribute("hasCompanies");
                      if (hasCompanies) {
                          %>
                  <li class="divider"></li>
                  <% if (u.getId() == Constants.SPECIAL_USER_ID_1){ %>
                      <li><a href="<%=Config.getUrl()%>/users/corp.jsp">New practice...</a></li>
                  <% } %>
                  <%
                      }
                  %>
              </ul>
            </li>
          </ul>
            <%
                }
            %>
            <ul class="nav">
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" onclick="showorhidedetails();"><i class="icon-user icon-white"></i> <%=(u != null) ? u.getFirstName() + " " + u.getLastName() : "My Account"%>
                  <b class="caret"></b></a>
              <ul class="dropdown-menu" id="details" style="display: none;">
                  <%
                      if (u != null) {
                  %>
                  <li><a href="<%=Config.getUrl()%>/users/me.jsp">My Details</a></li>
                  <li><a href="<%=Config.getUrl()%>/users/passwd.jsp">Change Password</a></li>
                <li class="divider"></li>
                <li><a href="<%=Config.getUrl()%>/users/logout.jsp">Log Out</a></li>
                  <%
                      } else {
                  %>
                <li><a href="<%=Config.getUrl()%>/users/register.jsp?from=trial">Register</a></li>
                <li><a href="<%=Config.getUrl()%>/users/login.jsp">Log in</a></li>
                  <%
                      }
                  %>
              </ul>
            </li>

              
          <ul class="nav">
            <li>
              <a href="http://fiizio.com/home/contact/" target="_blank"><i class="icon-book icon-white"></i> Help</a>
            </li>
          </ul>  
         </ul>
             
         
           <ul class="nav" style="margin-top: 8px;">  
                  <%  
                       if (cu != null && u!=null) {
                    	   List<String> lst=null;
                           PreparedStatement pst2 = conn.prepareStatement("select id from invitations where email = ? and acceptor isNull order by sent desc" );
                              pst2.setString(1, u.getEmail());
                              ResultSet rs2=pst2.executeQuery();
                              if(rs2.next()){
                             
                            	%> <li id="bl"><a href="/corp/people/pending.jsp" ><i class="icon-envelope icon-white" ></i></a></li><%
                              }
                             pst2.close();
                            
                   %>
                 <%--  <a href="<%=Config.getUrl()%>/users/me.jsp">My Details</a> --%>
                    
                  <%
                     
                      }
                  %>
         
            
          </ul>  
       <%--    <%
          if (cu != null) {
        	  Corporation corp = cu.getCorporation();
//           if (corp.getSubType() != null) {
//           if (cu.hasRole(CorporationRole.Role.Admin)) {
          %>	 --%>
   <%--        <ul class="nav">
            <li>
              <%
	      		PreparedStatement pst;
	            pst = conn.prepareStatement("select plan,type from cart_details where corp_id = ?");
	            pst.setLong(1, corp.getId());
	            ResultSet rs = pst.executeQuery();
	            if(rs.next()){ %>
              <a style="cursor: pointer;" onclick="viewCart(<%=rs.getLong("plan")%>,'<%=rs.getString("type")%>');"><i class="icon-shopping-cart icon-white"></i>
              		<span class="itemCount">1</span>
              </a>
              	<%}else{ %>
              	<a href="/corp/subscription/"><i class="icon-shopping-cart icon-white"></i></a>
              	<%} 
	            pst.close();
				}%>
            </li>
          </ul>  --%>   
<%--           <%}} %> --%>
          <div>
          <div class="brand" style="padding:  0; margin: 10px 15px 0px 15px; float: right;">
              <img src="/images/app/fiizio_tagline.png" width="150" height="26">
          </div>

          </div>
        </div>
    </div><!-- /topbar -->
    </div>
    <%conn.close(); %>
</html>