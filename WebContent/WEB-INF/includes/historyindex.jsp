<%@ page import="au.com.ourbodycorp.model.Attachment" %>
<%@ page import="au.com.ourbodycorp.model.Post" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.List" %>
<%
    Post p = (Post) request.getAttribute("post");
    long id = p.getId();
    Post[] crumbs = (Post[]) request.getAttribute("crumbs");
    long topMenuId = crumbs[0].getId();

    List<Post> posts = (List<Post>) request.getAttribute("indexPosts");
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
    Attachment.ImageSize size = Attachment.ImageSize.icon;

    //for (int i = 0; i < 20; i++) {
    //    posts.add(posts.get(0));
    //}
%>
<html>
<head><title><%=p.getTitle()%>
</title></head>
<body>

<h2><%=p.getTitle()%>
</h2>

<div class="box-2thirds">
    <hr class="top"/>
    <div class="content">

        <% if (p.getExcerpt() != null && p.getExcerpt().length() > 0) {%>
        <h3><%=p.getExcerpt()%></h3>
        <%}%>

        <%
            for (Post post : posts) {
                if (post.getIcon() == null) continue;
                String url = post.getBlogUrl(p);
        %>
        <div style="text-align: center; margin: 0 0 10px 0; float:left;">
            <a href="<%=url%>"><img src="<%=post.getIcon().getImageUrl(Attachment.ImageSize.thumb)%>" width="134" height="134"/></a>
            <br/>
            <a class="arrow" href="<%=url%>"><%=sdf.format(post.getPosted())%></a>

        </div>
        <%
            }
        %>
        <%@include file="share.jsp" %>

    </div>

</div>


<jsp:include page="sidenav.jsp"/>



</body>
</html>