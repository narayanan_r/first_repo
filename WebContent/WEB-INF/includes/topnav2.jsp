<%@ include file="/WEB-INF/includes/Analytics.jsp"%>
<%@ page import="au.com.ourbodycorp.model.Constants"%>
<%@ page import="au.com.ourbodycorp.Config" %>
<%@ page import="au.com.ourbodycorp.model.Corporation" %>
<%@ page import="au.com.ourbodycorp.model.User" %>
<%@ page import="au.com.ourbodycorp.model.managers.CorporationManager" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="java.util.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.CorporationUser" %>
<%@ page import="au.com.ourbodycorp.model.CorporationRole" %>
<%@ page import="java.util.List" %>
<%
    User u = (User) session.getAttribute("user");
    Corporation c = (Corporation) request.getAttribute("corp");
    CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");
    List<Corporation> corps = null;
    if (u != null) {
        CorporationManager cm = new CorporationManager();
        corps = cm.getCorporations(u.getId());
        LibraryManager lm = new LibraryManager();
        session.setAttribute("hasCompanies", lm.hasCompanies(u.getId())); 
    }
    Connection conn = Util.getConnection();
%>

<html>
<head>
<script type="text/javascript">
var action = 1;
function showorhidehome() {
    if ( action == 1 ) {
    	$("#details").hide();
    	$("#home").show();
    	action2 = 1;
        action = 2;
    } else {
    	$("#details").hide();
    	$("#home").hide();
        action = 1;
    }
}
var action2 = 1; 
function showorhidedetails() {
	if ( action2 == 1 ) {
    	$("#home").hide();
    	$("#details").show();
    	action = 1;
        action2 = 2;
    } else {
    	$("#home").hide();
    	$("#details").hide();
        action2 = 1;
    }
}

$('html').click(function (e) {
    if (e.target.id == 'home') {
    } else {
    	$("#home").hide();
    	 action = 1;
    }
});

$('html').click(function (e) {
    if (e.target.id == 'home') {
    } else {
    	$("#details").hide();
    	 action2 = 1;
    }
});

function viewCart(plan,type) {
	<%session.setAttribute("discountError", null);
	session.setAttribute("Expired", null);
	session.setAttribute("Used", null);
	
	if (cu != null) {
  	Corporation corp1 = cu.getCorporation();
    PreparedStatement pst1;
    pst1 = conn.prepareStatement("select plan,type from cart_details where corp_id = ?");
    pst1.setLong(1, corp1.getId());
    ResultSet rs7 = pst1.executeQuery();
    if(rs7.next()){ 
    	session.setAttribute("Cart_Error", "Error");
  	}else{
  		session.setAttribute("Cart_Error", "No_Error");
  	} 
    pst1.close();
	}%>
	window.location.replace("/corp/subscription/cart.jsp?plan="+plan+"&type="+type+"&cart=true");
}
</script>
<script type="text/javascript">
	function slugUrl(slugUrl) {
		$.ajax({
	        url: '<%=Config.getString("url")%>/corp/slugurl_changer.jsp?slugUrl='+slugUrl+'',
	        type: 'POST',
	        success: function(data) {
	      	   window.location = '<%=Config.getString("url")%>/corp/';
	        }
	      });
	}
</script>
<style type="text/css">
.itemCount {
    background: #db6e00 none repeat scroll 0 0;
    border-radius: 2px;
    color: #fff;
    font-size: 11px;
    height: 16px;
    line-height: 16px;
    padding: 0 3px;
    position: absolute;
    right: -2px;
    top: 3px;
}
</style>
</head>
<div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container-fluid">
         <div style="float: left; padding:  0; margin: 5px 25px 0px 10px">
          <img src="/images/app/fiizio_logo.png" width="123" height="27" style="">
             </div>

          <ul class="nav">

          </ul>
         

              
          <ul class="nav">
            <li>
              <a href="http://fiizio.com/home/contact/" target="_blank"><i class="icon-book icon-white"></i> Help</a>
            </li>
          </ul>    
          <%
          if (cu != null) {
        	  Corporation corp = cu.getCorporation();
//           if (corp.getSubType() != null) {
//           if (cu.hasRole(CorporationRole.Role.Admin)) {
          %>	
          <ul class="nav">
            <li>
              <%
	      		PreparedStatement pst;
	            pst = conn.prepareStatement("select plan,type from cart_details where corp_id = ?");
	            pst.setLong(1, corp.getId());
	            ResultSet rs = pst.executeQuery();
	            if(rs.next()){ %>
              <a style="cursor: pointer;" onclick="viewCart(<%=rs.getLong("plan")%>,'<%=rs.getString("type")%>');"><i class="icon-shopping-cart icon-white"></i>
              		<span class="itemCount">1</span>
              </a>
              	<%}else{ %>
              	<a href="/corp/subscription/"><i class="icon-shopping-cart icon-white"></i></a>
              	<%} 
	            pst.close();
				}%>
            </li>
          </ul>    
<%--           <%}} %> --%>
          <div>
          <div class="brand" style="padding:  0; margin: 5px 15px 0px 10px; float: right;">
              <img src="/images/app/fiizio_tagline.png" width="150" height="26">
          </div>

          </div>
        </div>
    </div><!-- /topbar -->
    </div>
    <%conn.close(); %>
</html>