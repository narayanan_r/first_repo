<%@ page import="au.com.ourbodycorp.model.Comment" %>
<%@ page import="au.com.ourbodycorp.model.User" %>
<%@ page import="au.com.ourbodycorp.model.managers.PostManager" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    boolean allow = (request.getParameter("allow") != null && Boolean.parseBoolean(request.getParameter("allow")));
    long postId = Long.parseLong(request.getParameter("postId"));
    PostManager pm = new PostManager();
    List<Comment> comments = pm.getComments(postId);
    SimpleDateFormat sdf = new SimpleDateFormat("d MMM yyyy '@' H:mm");
    User m = (User) session.getAttribute("user");

    boolean subscribed = (m != null && pm.isSubscribed(postId, m.getId()));
%>
<%
    if (comments.size() > 0) {
%>
<h3>Comments</h3>
<%
    for (Comment c : comments) {
%>
<div style="margin-bottom: 15px;">
    <div class="comment_header">
        <div class="comment_header_left"><a href="/members/detail.jsp?id=<%=c.getAuthor()%>" class="no_underline"><%=c.getAuthorName()%></a></div>
        <div class="comment_header_right"><%=sdf.format(c.getPosted())%></div>
    </div>
    <div class="comment"><%=c.getComment()%></div>
</div>
<%
    }
}
    if (allow && m != null) {
%>
<h3>Add a comment:</h3>
<form action="/comment" method="post">
    <input type="hidden" name="postId" value="<%=postId%>"/>
    <input type="hidden" name="url" value="<%=request.getParameter("url")%>"/>
    <textarea name="comment" rows="5" cols="60"></textarea><br/>
    <input type="submit" value="submit"/>
</form>
<%
    } else if (allow) {
%>
<p>Please <a href="/members/login.jsp?return=<%=request.getParameter("url")%>">log in</a> to leave a comment.</p>
<%
    } else if (comments.size() > 0) {
%>
<p>Comments for this post have been closed.</p>
<%
    }
%>
<%
    if (subscribed) {
%>
<p>You are subscribed to be notified of new comments by email. <a href="/comment?u=<%=postId%>">Unsubscribe</a>.</p>
<%
    } else if (m != null) {
%>
<p><a href="/comment?subscribe=<%=postId%>">Subscribe</a> to be notified of new comments by email.</p>
<%
    }
%> 