<%@ page import="au.com.ourbodycorp.model.Attachment" %>
<%@ page import="au.com.ourbodycorp.model.Blog" %>
<%@ page import="au.com.ourbodycorp.model.Post" %>
<%@ page import="au.com.ourbodycorp.model.User" %>
<%@ page import="au.com.ourbodycorp.model.managers.PostManager" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.List" %><%
    PostManager pm = new PostManager();
    User user = (User) session.getAttribute("user");
    Post p = (Post) request.getAttribute("post");
    long id = p.getId();
    Post[] crumbs = (Post[]) request.getAttribute("crumbs");
    long topMenuId = crumbs[0].getId();

    Blog blog = pm.getBlog(p.getBlogLink());
    request.setAttribute("blog", blog);
    Post blogPost = null;
    List<Post> archivePosts = null;
    List<Post> indexPosts = null;

    String uri = (request.getRequestURI().endsWith("/")) ? request.getRequestURI().substring(0, request.getRequestURI().length() -1) : request.getRequestURI();
    String[] parts = uri.split("/");
    if (request.getParameter("blogId") != null && user != null && user.hasRole("author")) {
        //preview
        blogPost = pm.getPost(Long.parseLong(request.getParameter("blogId")));
        request.setAttribute("blogPost", blogPost);
    } else if (request.getRequestURI().endsWith(".html")) {
        String name = parts[parts.length - 1];
        String slug = name.substring(0, name.lastIndexOf("."));
        int month = Integer.parseInt(parts[parts.length - 2]);
        int year = Integer.parseInt(parts[parts.length - 3]);
        blogPost = pm.getBlogPost(p.getBlogLink(), year, month, slug);
        request.setAttribute("blogPost", blogPost);
    } else if (request.getRequestURI().matches("^.+/[0-9]{4}/[0-9]{2}/?$")) {
        int month = Integer.parseInt(parts[parts.length - 1]);
        int year = Integer.parseInt(parts[parts.length - 2]);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date start = sdf.parse(String.format("%04d-%02d-01 00:00:00", year, month));
        Calendar cal = Calendar.getInstance();
        cal.setTime(start);
        cal.add(Calendar.MONTH, 1);
        Date end = cal.getTime();
        archivePosts = pm.getBlogPosts(p.getBlogLink(), start, end);
        request.setAttribute("archivePosts", archivePosts);
    } else {
        indexPosts = pm.getBlogPosts(p.getBlogLink(), blog.getIndexCount());
        request.setAttribute("indexPosts", indexPosts);
    }

    // Default banner
    Attachment banner = p.getBanner();
    String bannerUrl = "/images/header/flight.jpg";
    String bannerAlt = p.getTitle();
    if (banner != null) {
        bannerUrl = banner.getImageUrl(Attachment.ImageSize.banner);
        if (banner.getCaption() != null && banner.getCaption().trim().length() > 0) {
            bannerAlt = banner.getCaption();
        }
    }
    request.setAttribute("bannerUrl", bannerUrl);
    request.setAttribute("bannerAlt", bannerAlt);
    request.setAttribute("bannerbg", (blogPost != null) ? blogPost.getBannerBackground() : p.getBannerBackground());
%>
<% if (blogPost != null) {%>
<jsp:include page="<%=blog.getEntryJsp()%>"/>
<% } else if (archivePosts != null && archivePosts.size() > 0) {%>
<jsp:include page="<%=blog.getArchiveJsp()%>"/>
<% } else if (indexPosts != null && indexPosts.size() > 0) {%>
<jsp:include page="<%=blog.getIndexJsp()%>"/>
<%
    } else {
        response.sendError(404);
        return;
    }
%>

