<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.Attachment" %>
<%@ page import="au.com.ourbodycorp.model.FlickrPhoto" %>
<%@ page import="au.com.ourbodycorp.model.Post" %>
<%@ page import="au.com.ourbodycorp.model.managers.PhotoSetManager" %>
<%@ page import="com.aetrion.flickr.Flickr" %>
<%@ page import="com.aetrion.flickr.REST" %>
<%@ page import="com.aetrion.flickr.photos.*" %>
<%@ page import="com.aetrion.flickr.photosets.Photoset" %>
<%@ page import="com.aetrion.flickr.photosets.Photosets" %>
<%@ page import="com.aetrion.flickr.photosets.PhotosetsInterface" %>
<%@ page import="java.util.Collection" %>
<%@ page import="java.util.LinkedList" %>
<%@ page import="java.util.List" %>
<%
    Post p = (Post) request.getAttribute("post");
    long id = p.getId();

    Attachment banner = p.getBanner();
    String bannerUrl = "/images/header/flight.jpg";
    String bannerAlt = p.getTitle();
    if (banner != null) {
        bannerUrl = banner.getImageUrl(Attachment.ImageSize.banner);
        if (banner.getCaption() != null && banner.getCaption().trim().length() > 0) {
            bannerAlt = banner.getCaption();
        }
    }
    request.setAttribute("bannerUrl", bannerUrl);
    request.setAttribute("bannerAlt", bannerAlt);
    request.setAttribute("bannerbg", p.getBannerBackground());

    PhotoSetManager psm = new PhotoSetManager();

    List<FlickrPhoto> photos = psm.get(p.getTitle());


    if (photos == null || request.getParameter("reload") != null) {
        Flickr f = new Flickr("249ef289248e25027e9e9e59c5664adc", "38db7e71c3f22e44", new REST());

        PhotoList pl = null;
        PhotosInterface pi = f.getPhotosInterface();

        if (p.getShortCode().equals("gallery")) {
            pl = f.getPeopleInterface().getPublicPhotos("25206892@N07", Extras.ALL_EXTRAS, 64, 1);
        } else {
            PhotosetsInterface si = f.getPhotosetsInterface();
            Photosets sets = si.getList("25206892@N07");
            Collection sc = sets.getPhotosets();
            for (Object o : sc) {
                Photoset set = (Photoset) o;
                if (set.getTitle().equalsIgnoreCase(p.getTitle())) {
                    pl = si.getPhotos(set.getId(), Extras.ALL_EXTRAS, Flickr.PRIVACY_LEVEL_PUBLIC, 999, 1);

                    break;
                }
            }
        }

        photos = new LinkedList<FlickrPhoto>();
        if (pl != null) {
            for (int i = 0; i < pl.getPerPage(); i++) {
                Photo photo;
                try {
                    photo = (Photo) pl.get(i);
                } catch (Exception e) {
                    // End of the road
                    break;
                }

                FlickrPhoto fp = new FlickrPhoto();
                fp.setCaption(photo.getTitle());
                fp.setId(photo.getId());
                fp.setThumbUrl(photo.getSmallSquareUrl());

                Size[] sizes = {photo.getLargeSize(), photo.getMediumSize(), photo.getThumbnailSize()};
                for (Size size : sizes) {
                    if (size != null) {
                        fp.setFullUrl(size.getSource());
                        break;
                    }
                }
                fp.setLink(photo.getUrl());
                /*
                Collection sizes = pi.getSizes(photo.getId());
                for (Object size : sizes) {
                    Size s = (Size) size;
                    if (s.getLabel() == Size.SQUARE) {
                        fp.setThumbUrl(s.getSource());
                    } else if (s.getLabel() == Size.LARGE) {
                        fp.setLink(s.getUrl());
                        fp.setFullUrl(s.getSource());
                    }
                }
                */
                photos.add(fp);

            }
        }
        psm.save(p.getTitle(), photos);
    }

%>
<html>
<head><title><%=p.getTitle()%>
</title></head>
<body>
<h2><%=p.getTitle()%>
</h2>

<div class="box-2thirds">
    <hr class="top"/>
    <div class="content">
        <!--START DYNAMIC SUBPAGE CONTENT-->
        <% if (p.getExcerpt() != null) { %>
        <p class="intro"><%=p.getExcerpt()%>
        </p>
        <p>
        <% } %>

        <%
            if (photos.size() > 0) {
            for (FlickrPhoto photo : photos) {
        %>
        <a href="<%=photo.getFullUrl()%>" rel="lightbox" title="<%=Util.formValue(photo.getCaption())%>"><img src="<%=photo.getThumbUrl()%>" width="75" height="75" title="<%=Util.formValue(photo.getCaption())%>" alt="<%=Util.formValue(photo.getCaption())%>"/></a>
        <%
            }
            } else {
        %>
        No set named "<%=p.getTitle()%>" on Flickr.
        <%
            }
        %>
        </p>
        <% if (p.getShortCode().equals("gallery")) {
        %>
        <p style="clear: both;">More Adelaide Festival Photos on <a href="http://www.flickr.com/photos/adelaidefestival/">Flickr</a>.</p>
        <%
        }%>
        <!--END DYNAMIC SUBPAGE CONTENT-->

        <%@include file="share.jsp" %>

    </div>

</div>

<jsp:include page="sidenav.jsp"/>
<jsp:include page="attachments.jsp">
    <jsp:param name="versionId" value="<%=p.getVersionId()%>"/>
</jsp:include>


</body>
</html>
