<%
      if (errorFields.size() > 0) {
%>
<div class="alert alert-error" id="inputError">
      <p>There were some problems with your input, please check the form for details.</p>
      <%
          if (errorFields.containsKey("safeInput")) {
              %>
      <p><%=errorFields.get("safeInput")%></p>
      <%
          }
      %>

    <%
          if (errorFields.containsKey("general")) {
              %>
      <p><%=errorFields.get("general")%></p>
      <%
          }
      %>
  </div>
  <%
      }
  %>