<%@ page import="au.com.ourbodycorp.Config" %>
<%@ page import="au.com.ourbodycorp.model.Attachment" %>
<%@ page import="au.com.ourbodycorp.model.Post" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%

    Post p = (Post) request.getAttribute("post");
    Post blogPost = (Post) request.getAttribute("blogPost");
    long id = p.getId();

    Attachment banner = blogPost.getBanner();
    if (banner == null) {
        banner = p.getBanner();
    }
    String bannerUrl = "/images/header/flight.jpg";
    String bannerAlt = p.getTitle();
    if (banner != null) {
        bannerUrl = banner.getImageUrl(Attachment.ImageSize.banner);
        if (banner.getCaption() != null && banner.getCaption().trim().length() > 0) {
            bannerAlt = banner.getCaption();
        }
    }
    request.setAttribute("bannerUrl", bannerUrl);
    request.setAttribute("bannerAlt", bannerAlt);

    Post[] crumbs = (Post[]) request.getAttribute("crumbs");
    Post[] newCrumbs = new Post[crumbs.length + 1];
    for (int i = 0; i < crumbs.length; i++) {
        newCrumbs[i] = crumbs[i];
    }
    newCrumbs[crumbs.length] = blogPost;
    request.setAttribute("crumbs", newCrumbs);
    SimpleDateFormat sdf = new SimpleDateFormat("d/M/yyyy");
%>
<html>
<head><title><%=blogPost.getTitle()%> - <%=p.getTitle()%></title></head>
<body>
<h2><%=blogPost.getTitle()%></h2>

<div class="box-2thirds">
    <hr class="top"/>
    <div class="content">
        <!--START DYNAMIC SUBPAGE CONTENT-->
        <p class="date published"><%=sdf.format(blogPost.getPosted())%></p>
        <% if (blogPost.getExcerpt() != null) { %>
        <p class="intro"><%=blogPost.getExcerpt()%></p>
        <% } %>
        </p>
        <%=blogPost.getBody()%>

        <!--END DYNAMIC SUBPAGE CONTENT-->

        <%@include file="share.jsp"%>
        <% if (blogPost.isAllowComments()) {
            String disqusId = "adlfest_" + blogPost.getId();
            String permalink = Config.getString("url") + blogPost.getBlogUrl(p);
        %>
        <%@include file="disqus.jsp"%>
        <%}%>

    </div>

</div>


<jsp:include page="sidenav.jsp"/>
<jsp:include page="attachments.jsp">
    <jsp:param name="versionId" value="<%=blogPost.getVersionId()%>"/>
</jsp:include>



</body>
</html>