<%@ page import="au.com.ourbodycorp.model.Post" %>
<%@ page import="au.com.ourbodycorp.model.managers.PostManager" %>
<%@ page import="java.util.List" %>
<%
    PostManager pm = new PostManager();
    Post[] crumbs = (Post[]) request.getAttribute("crumbs");

    Post post = null;
    if (crumbs != null) {
        post = (crumbs.length > 2) ? crumbs[1] : (Post) request.getAttribute("post");
    }

    long id = (post != null) ? post.getId() : 0;
    long parentId = 0;
    if (post != null) {
        parentId = (post.getParent() > 0) ? post.getParent() : post.getId();
    }
    long topMenuId = 0;

    if (crumbs != null) {
        topMenuId = crumbs[0].getId();
    }

    List<Post> top = pm.getLivePosts(Post.PostType.page, 0, false);
    request.setAttribute("topPosts", top);
    List<Post> children = null;
    if (parentId > 0) {
         children = pm.getLivePosts(Post.PostType.page, parentId, false);
    }
%>
<div class="box-1third clearfix">
    <hr class="top"/>
    <ul id="sub-nav">
        <%
            for (Post p : top) {
                String active = "";
                String childrenHtml = "";
                if (p.getId() == topMenuId) {
                    if (p.getId() == id) {
                        active = " class=\"active\"";
                    }
                    StringBuilder sb = new StringBuilder();
                    sb.append("\n<ul>\n");
                    for (Post child : children) {
                        String cls = "";

                        if (child == children.get(children.size() - 1)) {
                            cls += "last";
                        }

                        if (child.getId() == id) {
                            cls += " active";
                        }


                        if (cls.length() > 0) {
                            sb.append("\t<li class=\"").append(cls).append("\">");
                        } else {
                            sb.append("\t<li>");
                        }
                        sb.append("<a href=\"").append(child.getUrl()).append("\">")
                                .append(child.getTitle().toLowerCase()).append("</a></li>\n");
                    }
                    sb.append("</ul>\n");
                    childrenHtml = sb.toString();
                }
                if (p == top.get(top.size() - 1)) {
                    if (active.length() > 0) {
                        active += " ";
                    }
                    active += "last";
                }
        %>
        <li<%=active%>><a href="<%=p.getUrl()%>"><%=p.getTitle().toLowerCase()%>
        </a></li>
        <%=childrenHtml%>
        <%
            }
        %>

    </ul>
</div>