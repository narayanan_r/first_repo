<%
   int bci = 1;
%>

<div class="row">
    <div class="span9">
        <ul class="breadcrumb">
            <%
                String bcLink;
                for (String[] bcCrumb : breadcrumbs) {
                    if (bcCrumb[0] == null) {
                        bcLink = bcCrumb[1];
                    } else {
                        bcLink = "<a href=\"" + bcCrumb[0] + "\">" + bcCrumb[1] + "</a>";
                    }
                    String bcActive = "";
                    if (bci < breadcrumbs.length) bcLink += " <span class=\"divider\">/</span>";
                    else if (bci == breadcrumbs.length) bcActive = "  class=\"active\"";
                    bci++;
            %>
            <li<%=bcActive%>><%=bcLink%></li>
            <%
                }
            %>
        </ul>
    </div>
</div>