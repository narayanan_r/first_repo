<%@ include file="/WEB-INF/includes/Analytics.jsp"%>
<%@ page import="au.com.ourbodycorp.model.Constants"%>
<%@ page import="au.com.ourbodycorp.Config" %>
<%@ page import="au.com.ourbodycorp.model.Corporation" %>
<%@ page import="au.com.ourbodycorp.model.User" %>
<%@ page import="au.com.ourbodycorp.model.managers.CorporationManager" %>
<%@ page import="java.util.List" %>
<%
    User u = (User) session.getAttribute("user");
    List<Corporation> corps = null;
    if (u != null) {
        CorporationManager cm = new CorporationManager();
        corps = cm.getCorporations(u.getId());
    }
%>

<script type="text/javascript">
	function slugUrl(slugUrl) {
		$.ajax({
	        url: '<%=Config.getString("url")%>/corp/slugurl_changer.jsp?slugUrl='+slugUrl+'',
	        type: 'POST',
	        success: function(data) {
	      	   window.location = '<%=Config.getString("url")%>/corp/';
	        }
	      });
	}
</script>
<div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container-fluid">
          <a class="brand" href="<%=Config.getUrl()%>"><%=Config.getString("site.name")%> Admin</a>
          <ul class="nav">
          </ul>
          <%
              if (u != null) {
          %>

          <ul class="nav">
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-home icon-white"></i> Select Practice
              <b class="caret"></b>
              </a>
              <ul class="dropdown-menu">
                <% if (corps != null && corps.size() > 0) {
                    for (Corporation corp : corps) {
                  %>
                  <%-- <%if (Config.getBoolean("https", false)) {%>
                    <li><a style="cursor: pointer;" onclick="slugUrl('<%=corp.getSlug()%>')"><%=corp.getName()%></a></li>
        		<%}else {%>
        			<li><a style="cursor: pointer;" onclick="slugUrl('<%=corp.getSlug()%>')"><%=corp.getName()%></a></li>
        		<%}%> --%>
                  <li><a href="<%=corp.getUrl()%>"><%=corp.getName()%></a></li>
                  <%
                    }
                %>
                  <li class="divider"></li>
                  <%
                      }
                  %>
                  <% if (u.getId() == Constants.SPECIAL_USER_ID_1){ %>
                      <li><a href="<%=Config.getUrl()%>/users/corp.jsp">New practice...</a></li>
                  <% } %>
              </ul>
            </li>
          </ul>
            <%
                }
            %>
            <ul class="nav">
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-user icon-white"></i> <%=(u != null) ? u.getFirstName() + " " + u.getLastName() : "My Account"%>
                  <b class="caret"></b></a>
              <ul class="dropdown-menu">
                  <%
                      if (u != null) {
                  %>
                  <li><a href="<%=Config.getUrl()%>/users/me.jsp">My Details</a></li>
                  <li><a href="<%=Config.getUrl()%>/users/passwd.jsp">Change Password</a></li>
                <li class="divider"></li>
                <li><a href="<%=Config.getUrl()%>/users/logout.jsp">Log Out</a></li>
                  <%
                      } else {
                  %>
                <li><a href="<%=Config.getUrl()%>/users/register.jsp">Register</a></li>
                <li><a href="<%=Config.getUrl()%>/users/login.jsp">Log in</a></li>
                  <%
                      }
                  %>
              </ul>
            </li>
          </ul>

        </div>
    </div><!-- /topbar -->
    </div>
