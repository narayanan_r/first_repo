<%@page import="au.com.ourbodycorp.Config"%>
<%!
    String isActive(String key, String uri) {
        return (uri.matches(key)) ? " class=\"active\"" : "";
    }
%>
<ul class="nav nav-list">
<li class="nav-header">
Site Management
</li>
    <li<%=(request.getRequestURI().equalsIgnoreCase("/admin/")) ? " class=\"active\"" : ""%>><a href="<%=Config.getUrl()%>/admin/"><i class="icon-cog"></i> Admin</a></li>
    <li<%=isActive("/admin/library/", request.getRequestURI())%>><a href="<%=Config.getUrl()%>/admin/library/"><i class="icon-book"></i> Library</a></li>
    <li<%=isActive("/admin/myinfo/", request.getRequestURI())%>><a href="<%=Config.getUrl()%>/admin/myinfo/"><i class="icon-info-sign"></i> My Info</a></li>
    <li<%=isActive("/admin/clients/", request.getRequestURI())%>><a href="<%=Config.getUrl()%>/admin/clients/"><i class="icon-th-list"></i> Clients</a></li>
    <li<%=isActive("/admin/planprice/", request.getRequestURI())%>><a href="<%=Config.getUrl()%>/admin/planprice/"><i class="icon-shopping-cart"></i> Plans & Price</a></li>
    <li<%=isActive("/admin/promotion/", request.getRequestURI())%>><a href="<%=Config.getUrl()%>/admin/promotion/"><i class="icon-gift"></i> Promotion</a></li>
    <li<%=isActive("/admin/subscription/index.jsp", request.getRequestURI())%>><a href="<%=Config.getUrl()%>/admin/subscription/index.jsp"><i class="icon-shopping-cart"></i> Manage Subscription Plans</a></li>
    <li<%=isActive("/admin/log/log.jsp", request.getRequestURI())%>><a href="<%=Config.getUrl()%>/admin/log/log.jsp" target="_blank"><i class="icon-pencil"></i> Log</a></li>
    
</ul>



