<%@ page import="au.com.ourbodycorp.model.BlogArchive" %>
<%@ page import="au.com.ourbodycorp.model.Post" %>
<%@ page import="au.com.ourbodycorp.model.managers.PostManager" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.List" %>
<%
    PostManager pm = new PostManager();
    List<BlogArchive> months = pm.getArchiveMonths(p.getBlogLink());
    SimpleDateFormat monthFormat = new SimpleDateFormat("MMMM yyyy");

    List<Post> archivePosts = (List<Post>) request.getAttribute("archivePosts");
    String selected = "";
    if (archivePosts != null) {
        selected = monthFormat.format(archivePosts.get(0).getPosted());
    }

%>
<%
    if (months.size() > 1) {
%>
<div class="box-1third">
    <hr class="top"/>
    <div class="download">
        <h3><%=p.getTitle().toLowerCase()%> archive:</h3>

        <form name="archive" action="#">
            <select name="month" onchange="location = this.options[this.selectedIndex].value;" style="width: 220px;">
                <option value="#">Select Year and Month:</option>
                <%
                    for (BlogArchive month : months) {
                        String title = String.format("%s (%d)", monthFormat.format(month.getMonth()), month.getCount());
                        boolean check = selected.equals(monthFormat.format(month.getMonth()));
                %>
                <option value="<%=month.getUrl(p)%>"<%=(check) ? " selected" : ""%>><%=title%>
                </option>
                <%
                    }
                %>
            </select>
        </form>
    <div style="margin-top: 15px;">
    <a href="/rss/<%=blog.getSlug()%>.rss"><img src="/images/feed-icon-14x14.png" width="14" height="14"/></a>
    <a href="/rss/<%=blog.getSlug()%>.rss">Subscribe to RSS News Feed</a>
    </div>
    </div>
</div>

<%
    }
%>
