<%@ page import="au.com.ourbodycorp.Config" %>
<%@ page import="au.com.ourbodycorp.MailMessage" %>
<%@ page import="au.com.ourbodycorp.model.User" %>
<%@ page import="java.io.PrintWriter" %>
<%@ page import="java.io.StringWriter" %>
<%@ page import="java.util.Enumeration" %>
<%@ page import="java.util.Properties" %>
<%@ page isErrorPage="true" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String url = (String) request.getAttribute("javax.servlet.forward.request_uri");
    String query = (String) request.getAttribute("javax.servlet.forward.query_string");
    User u = (User) session.getAttribute("user");

    StringWriter sw = new StringWriter();
    PrintWriter pw = new PrintWriter(sw);
    exception.printStackTrace(pw);

    StringBuilder sb = new StringBuilder();
    if (u != null) {
        sb.append("User: ").append(u.getName()).append(" (").append(u.getEmail()).append(")\n");
    }
    sb.append(request.getMethod().toUpperCase()).append(": ").append(url).append("\n");
    sb.append("Query: ").append(query).append("\n\n");
    sb.append("Headers:\n");
    Enumeration headers = request.getHeaderNames();
    while (headers.hasMoreElements()) {
        String name = (String) headers.nextElement();
        sb.append(name).append(": ");
        Enumeration values = request.getHeaders(name);
        while (values.hasMoreElements()) {
            String value = (String) values.nextElement();
            sb.append("{").append(value).append("}");
        }
        sb.append("\n");
    }


    sb.append("\nException: ").append(exception.getMessage()).append("\n");
    sb.append(sw.toString());
    sb.append("\n\n");
    sb.append("Variables:\n");
    Enumeration params = request.getParameterNames();
    while (params.hasMoreElements()) {
        String name = (String) params.nextElement();
        sb.append(name).append(": ");
        String[] values = request.getParameterValues(name);
        for (String value : values) {
            sb.append("{").append(value).append("}");
        }
        sb.append("\n");

    }

    boolean doEmail = false;
    String adminEmail = "chris@fiizio.com";
    try {
        adminEmail = Config.getString("email.admin", adminEmail);
    } catch (Exception e) {
        //yup, really dead.
    }

    if (Config.getBoolean("error.email", false)) {
        doEmail = true;
        try {
            Properties p = new Properties();
            p.put("body", sb.toString());
            /* MailMessage message = new MailMessage(p, "exception.vm", adminEmail, "Exception on " + url);
            message.send();  */
        } catch (Exception ee) {
            System.err.println("Failed sending error email to admin");
//            ee.printStackTrace();
        }
    }

%>
<% if (doEmail) {
%>
<%@include file="/WEB-INF/includes/error.jsp"%>
<%
} else {
%>
<html>
<head><title>500 Internal Server Error</title></head>
<body>
<h1>500 Internal Server Error</h1>
<pre>
<%=sb.toString()%>
</pre>
</body>
</html>
<%
}
%>
