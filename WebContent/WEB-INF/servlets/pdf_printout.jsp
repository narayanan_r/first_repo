<%@page import="au.com.ourbodycorp.model.managers.CorporationManager"%>
<%@ page import="java.util.List" %>
<%@ page import="au.com.ourbodycorp.Config" %>
<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="au.com.ourbodycorp.model.printout.ERR_T" %>
<%@ page import="au.com.ourbodycorp.model.printout.ExercisePrintoutGenerator" %>
<%@ page import="au.com.ourbodycorp.model.printout.PDFExercisePrintout" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.TextLocalClient" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    //the printout directory path specifies where to save the printout in the
    //web server. If root directory of the web server changes then printout 
    //dir path will need to change accordingly
    final String PRINTOUT_DIR_PATH = Constants.TESTING_MODE ? "/Users/rustyshelf/netbeans_workspace/physio_pdfs" : "/opt/tomcat/fiizio_webapps/ROOT/printouts/";
    final int NULL_PROGRAM_INSTANCE = -1;

    ExercisePrintoutGenerator gen = new ExercisePrintoutGenerator();
    String wrkspce[] = {null};   //create buffer to hold filename
    int buildStatus[] = {NULL_PROGRAM_INSTANCE};
    ERR_T status = ERR_T.ERR_INVALID;
    String path = null;
    String error = null;

    //the below is intended to run without authentication, so that a user can generate printouts for their exercise
    long id = Long.parseLong(request.getParameter("id")); //the program id
    LibraryManager lm = new LibraryManager();
    CorporationManager corporationManager = new CorporationManager();
    Program program = lm.getProgram(id);
    String userName=(String) session.getAttribute("patient_session");
    if(userName==null){
     	response.sendRedirect("/corp/patients/patientlogin.jsp");
    }
    if (program != null) {
        Patient patient = lm.getPatient(program.getPatientId());
        PatientGroup group = lm.getPatientGroup(program.getGroupId());
        List<ProgramExercise> exercises = lm.getProgramExercises(id);
        Corporation practice = corporationManager.getCorporation(program.getCorpId());
        status = gen.generateExercisePrintout(practice, patient, group, program, exercises, false, PRINTOUT_DIR_PATH, wrkspce, buildStatus);
    }

    if (status == ERR_T.ERR_NONE) {
        //created printout
        StringBuffer buffer = new StringBuffer("/printouts/");
        buffer.append(wrkspce[0]);
        path = buffer.toString();   //store the path to the printout
        response.sendRedirect(path);
        return;
    }
    else {
        //failed to create printout
        error =
                "Error creating exercise program printout (Err type = " + status
                + ", status code = " + buildStatus[0] + ")";
    }
%>
<html>
    <head>
        <style>
            a:hover {background-color:red;}
        </style>
        <title>Exercise Program Printout
        </title>
    </head>
    <body>
        <%@include file="/WEB-INF/includes/formErrorSimple.jsp"%>
        <%
            if (status == ERR_T.ERR_NONE) {
        %>
        <div class="row">
            <div class="span9">
                <p>
                    Generating program printout. The printout will automatically appear once it 
                    is generated. If the printout does not appear then <a href=<%=path%> >click here</a> 
                    to view it.
                </p>
            </div>
        </div>
        <%
            }
        %>
    </body>
</html>
