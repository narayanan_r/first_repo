<%@page import="au.com.ourbodycorp.model.PatientGroup"%>
<%@ page import="au.com.ourbodycorp.model.Patient" %>
<%@ page import="au.com.ourbodycorp.model.Program" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="au.com.ourbodycorp.model.managers.UserManager" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String parts[] = request.getRequestURI().split("/");
    if (parts == null || parts.length == 0) {
        response.sendError(400, "No Program ID provided");
        return;
    }
    LibraryManager lm = new LibraryManager();
    Program p = lm.getProgram(parts[parts.length - 1]);
    if (p == null) {
        response.sendError(404, "program not found");
        return;
    }
    Patient patient = lm.getPatient(p.getPatientId());
    PatientGroup group = lm.getPatientGroup(p.getGroupId());
    
    String username = (patient != null)? patient.getUserName() : group.getUserName();
    String applicationName = (new UserManager().getUser(p.getCreator()).getApplicationId())==1?"M3Clinic App":"Fiizio App";
    String appUrl = "fiizio://add/" + username +"/" + p.getCode().toLowerCase();
    String pdfUrl = "/pdf/pdf_printout.jsp?id=" + p.getId();
%>
<html>
<head>
    <title>Add Program</title>
    <style type="text/css">
        body {
            /*width: 320px;*/
            margin: 0 0 0 0;
            padding: 0;
            background-color: #ededed;
            text-align: center;
        }
    </style>
    
    <meta name="apple-itunes-app" content="app-id=526273899, app-argument=<%=appUrl%>">
</head>
<body>
    <div style="background-color: #ffffff; border-bottom: #dadada solid 1px; margin-bottom: 20px; width:100%;">
    <%if(applicationName.equals("M3Clinic App")){ %>
    	<img src="/images/M3ClinicAppLogo320x80.jpg" alt="FiizioApp" width="320" height="80">
    <%}else{ %>
        <img src="/images/app/logo-320-app.png" alt="FiizioApp" width="320" height="80">
    <%} %>
    </div>
    
    <div style="background-color: #f7f7f7; padding: 10px; margin: 0 auto; width: 320px;">
        If you have <%=applicationName%> installed:</br>
        <a href="<%=appUrl%>" class="btn" style="margin-top: 5px; margin-bottom: 20px">Download the exercise program</a>
        <br/>
        If you want to view it on your computer:<br/>
        <a href="<%=pdfUrl%>" class="btn" style="margin-top: 5px">Download PDF version</a>
    </div>
    
    <div style="margin-top: 30px;">
        <p><%=applicationName%> is available for iOS and Android:</p>
        <%if(applicationName.equals("M3Clinic App")){ %>
        <a href="#">
            <img src="/images/app_store.png" alt="Available on the App Store" width="150">
        </a>
        <a href="#" style="margin-left: 10px;">
            <img src="/images/play_store.png" alt="Available on the Google Play Store" width="150">
        </a>
        <%}else{ %>
         <a href="#">
            <img src="/images/app_store.png" alt="Available on the App Store" width="150">
        </a>
        <a href="#" style="margin-left: 10px;">
            <img src="/images/play_store.png" alt="Available on the Google Play Store" width="150">
        </a>
        <%} %>
    </div>
</body>
</html>