<%@ page import="au.com.ourbodycorp.Config" %>
<%@ page import="au.com.ourbodycorp.model.Attachment" %>
<%@ page import="au.com.ourbodycorp.model.MyInfo" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    long id = 0;
    try {
        id = Long.parseLong(request.getParameter("id"));
    }
    catch (Exception e) {
        response.sendError(400, "No ID provided");
        return;
    }
    
    if(request.getAttribute("corpUser") != null){
    CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");
    User u = (User) session.getAttribute("user");
    Corporation corp = cu.getCorporation();
    
    String type="Patients";
    String activity_page="info.jsp";
	String corp_id=String.valueOf(corp.getId());
	String corp_name=corp.getName();
	String corp_loc=corp.getSuburb();
	String user_id=null;
	String user_name=null;
	if(u!=null){
	user_id=String.valueOf(u.getId());
	user_name=u.getFullName();
	}else{
	user_id="---";
	user_name="--";
	}
	String invitor_id="---";
	String invitor_name="---";
	String flag="view";
	String from="---";
	String to="---";
	String desc="View Articles Info by user";
    /* try
    {		
     Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
    }
    catch(Exception e)
    {
    } */
   /*  try{
    	Util.allActivitiesLogs("Patients", u.getId(), corp.getName(), "View Articles Info");
    }catch(Exception e){
    	
    } */
    }else{
    	User u = (User) session.getAttribute("user");
    	
    	    String type="My Info";
    	    String activity_page="info.jsp";
    		String corp_id="No Corporation";
    		String corp_name="----";
    		String corp_loc="---";
    		String user_id=null;
    		String user_name=null;
    		if(u!=null){
    		user_id=String.valueOf(u.getId());
    		user_name=u.getFullName();
    		}else{
    		user_id="---";
        	user_name="--";
    		}
    		String invitor_id="---";
    		String invitor_name="---";
    		String flag="view";
    		String from="---";
    		String to="---";
    		String desc="View My Info Article Image by Fiizio System Admin";
    	   /*  try
    	    {		
    	     Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
    	    }
    	    catch(Exception e)
    	    {
    	    }	 */
    	
    	
    	/* try{
    		Util.allActivitiesLogs("My Info", u.getId(), "Fiizio System Admin", "View My Info Article Image");
        }catch(Exception e){
        	
        } */
    }

    LibraryManager lm = new LibraryManager();
    MyInfo n = lm.getInfo(id);
    if (n == null) {
        response.sendError(404);
        return;
    }


    Attachment a = n.getAttachment();

%>

<html>
    <head><title><%=n.getHeadline()%></title>
        <base href="<%=Config.getString("url")%>" />
    </head>
    <body>
        <div class="row">
            <div class="span9">
                <div class="page-header">
                    <h1><%=n.getHeadline()%></h1>
                </div>
                <div class="row">
                    <div class="span9">
                        <% if (a != null) {%>
                        <img src="<%=a.getImageUrl(Attachment.ImageSize.f320)%>">
                        <% }%>
                        <p>
                            <%=n.getBody().replaceAll("\n", "<br>")%>
                        </p>
                        <% if (n.getLink() != null && n.getLink().trim().length() > 0) {
                                String url = n.getLink();
                                if (!url.startsWith("http://") && !url.startsWith("https://")) {
                                    url = "http://" + url;
                                }
                        %>
                        <p>
                            <a href="<%=url%>" class="btn" target="_balnk">More Info</a>
                        </p>
                        <% }%>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>