<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.Corporation" %>
<%@ page import="au.com.ourbodycorp.model.managers.CorporationManager" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String slug = Util.notNull(request.getParameter("slug"));
    CorporationManager cm = new CorporationManager();
    Corporation c = cm.getCorporation(slug);
    if (c == null) {
        response.sendRedirect("/");
        return;
    }

    boolean addLink = false;
    if (c.isExpiredTrial() || cm.hasTrialExpired(c.getId())) {
        c.setLocked("Your trial period has expired.");
        addLink = true;
    }

%>
<html>
<head><title>Account Locked</title></head>
<body>
<div class="page-header">
    <h1>Account locked</h1>
</div>
<div class="row">
    <div class="span4">
        <p>
            <%=c.getName()%> has been locked, because:
        </p>
        <p class="well"> <%=c.getLocked()%>
        <% if (addLink){ %>
            <br/><br/>To continue using Fiizio please <a href="http://localhost:8071/corp/subscription/">check our pricing page</a> and sign up for a plan. We hope to see you again soon.</p>
        <% } else { %>
        </p><p> Please contact support to resolve the issue. </p>
        <% } %>
    </div>
</div>

</body>
</html>