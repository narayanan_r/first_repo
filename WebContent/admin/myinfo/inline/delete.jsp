<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.MyInfo" %>
<%@ page import="au.com.ourbodycorp.model.managers.AttachmentManager" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="java.net.URLEncoder" %>
<%@ page import="au.com.ourbodycorp.Config" %>
<%@ page import="au.com.ourbodycorp.model.User" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    long id = Long.parseLong(request.getParameter("id"));
    boolean confirm = (request.getParameter("confirm") != null && Boolean.parseBoolean(request.getParameter("confirm")));
    String submit = Util.notNull(request.getParameter("submit"));
    LibraryManager lm = new LibraryManager();
    User u = (User) session.getAttribute("user");
   
    MyInfo e = lm.getInfo(id);

    if (e == null) {
        response.sendError(404);
        return;
    }

    if (submit.equalsIgnoreCase("cancel")) {
    	if(Config.getString("https").equals("true")){
    		response.sendRedirect("https://"+request.getHeader("host")+"/close.jsp");
    	}else{
			response.sendRedirect("http://"+request.getHeader("host")+"/close.jsp");
		}
        return;
    } else if (confirm) {
    	    String type="My Info";
    	    String activity_page="admin/myinfo/inline/delete.jsp";
    	    String corp_id="---";
        	String corp_name="---";
        	String corp_loc="---";
        	String user_id=String.valueOf(u.getId());
        	String user_name="Fiizio Admin";
    		String invitor_id=String.valueOf(e.getId());
    		String invitor_name="---";
    		String flag="new";
    		String from="Delete Info Article from UserInfo";
    		String to="ArticleName:"+e.getHeadline()+";Body:"+e.getBody();
    	    try{
    	    	//Util.allActivitiesLogs("My Info", u.getId(), "Fiizio System Admin", "Delete My Info Article");
        		Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,"Delete My Info Article By Fiizio System Admin:"+u.getFullName());
    	    }catch(Exception ee){
    	    	
    	    }
        lm.deleteInfo(id);
        if (e.getAttachment() != null) {
            AttachmentManager am = new AttachmentManager();
            am.delete(e.getAttachment().getId());
        }
        if(Config.getString("https").equals("true")){
    		response.sendRedirect("https://"+request.getHeader("host")+"/close.jsp?redir=" + URLEncoder.encode("/admin/myinfo/", "UTF-8"));
    	}else{
			response.sendRedirect("http://"+request.getHeader("host")+"/close.jsp?redir=" + URLEncoder.encode("/admin/myinfo/", "UTF-8"));
		}
        return;
    }

%>
<html>
<head><title>Delete Article</title></head>
<body>
<div class="page-header">
    <h1>Delete Article <small><%=Util.textAreaValue(e.getHeadline())%></small></h1>
</div>
<div class="row">
    <div class="span6">
        <p>Are you sure you want to delete this article? It will be removed from any program it is used in.</p>
        <form action="delete.jsp" method="POST">
            <input type="hidden" name="id" value="<%=id%>">
            <input type="hidden" name="confirm" value="true">
            <%=HTML.confirmCancel()%>
        </form>
    </div>
</div>
</body>
</html>