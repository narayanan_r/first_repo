<%@ page import="au.com.ourbodycorp.Config" %>
<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.MultiPartHelper" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.Attachment" %>
<%@ page import="au.com.ourbodycorp.model.MyInfo" %>
<%@ page import="au.com.ourbodycorp.model.managers.AttachmentManager" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="org.apache.commons.fileupload.FileItem" %>
<%@ page import="org.apache.commons.fileupload.servlet.ServletFileUpload" %>
<%@ page import="java.util.Date" %>
<%@ page import="au.com.ourbodycorp.model.User" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String titlePrefix = "Add";

    String[][] breadcrumbs = {{"/admin/", "Admin"}, {"/admin/myinfo/", "MyInfo"}, {null, "Edit"}};

    LibraryManager lm = new LibraryManager();

    long id = 0;
    try {
        id = Long.parseLong(request.getParameter("id"));
    } catch (Exception e) {
        // is ok
    }

    String headline = Util.notNull(request.getParameter("headline"));
    String body = Util.notNull(request.getParameter("body"));
    
    MultiPartHelper mp = null;
    if (ServletFileUpload.isMultipartContent(request)) {
        mp = new MultiPartHelper(request);
        id = Long.parseLong(mp.getParameter("id"));

        headline = Util.notNull(mp.getParameter("headline"));
        body = Util.notNull(mp.getParameter("body"));
    }

    User u = (User) session.getAttribute("user");
    String type="My Info";
    String activity_page="admin/myinfo/edit.jsp";
    String corp_id="---";
	String corp_name="---";
	String corp_loc="---";
	String user_id=String.valueOf(u.getId());
	String user_name="Fiizio Admin";
	String flag="edit";
	String from="";
	String to="";
    MyInfo info = null;
    if (id > 0) {
    	 try{
        	//Util.allActivitiesLogs("My Info", u.getId(), "Fiizio System Admin", "Edit My Info Article");
    		Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,"---","---",flag,from,to,"Edit My Info Article By Fiizio System Admin:"+u.getFullName());
        }catch(Exception ee){
        } 
        info = lm.getInfo(id);
        if (info == null) {
            response.sendError(404, "no such info item");
            return;
        }
        titlePrefix = "Edit";
    }else{
    	try{
        	//Util.allActivitiesLogs("My Info", u.getId(), "Fiizio System Admin", "Edit My Info Article");
    		Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,"---","---",flag,from,to,"Add My Info Article By Fiizio System Admin:"+u.getFullName());
        }catch(Exception ee){
        } 
    }

    AttachmentManager am = new AttachmentManager();
    Attachment a = null;


    String error = null;

    if (request.getMethod().equalsIgnoreCase("post")) {
        if (mp.getParameter("submit").equals("Cancel")) {
            response.sendRedirect("/admin/myinfo/");
            return;
        }

        if (headline.trim().length() == 0) {
            error = "Please enter a headline";
        } else if (body.trim().length() == 0) {
            error = "Please enter some body text";
        }
        if(info != null){
        	try{
            	//Util.allActivitiesLogs("My Info", u.getId(), "Fiizio System Admin", "Edit My Info Article");
        		Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,"---","---",flag,from,to,"Edit My Info Article By Fiizio System Admin:"+u.getFullName());
            }catch(Exception ee){
            }
        	
        }else{
        	try{
            	//Util.allActivitiesLogs("My Info", u.getId(), "Fiizio System Admin", "Edit My Info Article");
        		Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,"---","---",flag,from,to,"Add My Info Article By Fiizio System Admin:"+u.getFullName());
            }catch(Exception ee){
            }
        }


        headline = Util.safeInput(headline, false);


        FileItem photo = mp.getFileItem("photo");
        if (photo != null && photo.getSize() > 0) {
            if (!photo.getName().toLowerCase().endsWith(".jpg")) {
                error = "Photos must be uploaded as .jpg file";
            }
        }

        if (error == null) {
            if (info == null) {
                info = new MyInfo();
                info.setCorpId(0);
            }

            if (photo != null && photo.getSize() > 0) {

                if (info.getPhotoId() > 0) {
                    a = am.getAttachment(info.getPhotoId(), false);
                } else {
                    a = new Attachment();
                    a.setUploaded(new Date());
                }
                a.setVersion(a.getVersion() + 1);
                a.setType(Attachment.AttachmentType.image);
                a.setFilename(photo.getName());
                a.setUpdated(new Date());
                a.setMimeType(photo.getContentType());
                a.setActive(true);
                a.setSize(photo.getSize());
                a.setData(photo.get());
                am.saveAttachment(a);
                info.setPhotoId(a.getId());
                info.setAttachment(a);
            }

            info.setHeadline(headline);
            info.setBody(body);
            lm.save(info);

            if (mp.getParameter("submit").contains("Close")) {
                response.sendRedirect("/admin/myinfo/");
                return;
            }
            id = info.getId();
        }

    } else {
        if (info != null) {
            headline = info.getHeadline();
            body = info.getBody();
        }
    }

    if (info != null) {
      if (info.getPhotoId() > 0) {
                a = info.getAttachment();
            }
        }

%>
<html>
<head>
    <title><%=titlePrefix%> Info Article</title>

    <script type="text/javascript" src="/tiny_mce/tiny_mce.js"></script>
    <script type="text/javascript">
    tinyMCE.init({
        // General options
        mode : "exact",
        elements: "body",
        theme : "advanced",
        plugins : "autolink,lists,spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",

        // Theme options
        theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,formatselect,fontselect,fontsizeselect",
        theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,bullist,numlist,|,outdent,indent,blockquote,|,insertdate,inserttime,preview,|,forecolor,backcolor,|,hr,removeformat,visualaid,|,sub,sup",
        theme_advanced_buttons3 : "",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_statusbar_location : "bottom",
        theme_advanced_resizing : false,

        // Skin options
        skin : "o2k7",
        skin_variant : "silver",

        // Example content CSS (should be your site CSS)
        content_css : "css/example.css",

        // Drop lists for link/image/media/template dialogs
        template_external_list_url : "js/template_list.js",
        external_link_list_url : "js/link_list.js",
        external_image_list_url : "js/image_list.js",
        media_external_list_url : "js/media_list.js"

    });
</script>


</head>
<body>
<div class="page-header">
    <h1>My Info <small><%=titlePrefix%> Article</small></h1>
</div>
<%@include file="/WEB-INF/includes/breadcrumbs.jsp"%>
<%@include file="/WEB-INF/includes/formErrorSimple.jsp"%>
<div class="row">
    <div class="span9">
        <form action="edit.jsp" method="post" class="form-horizontal" enctype="multipart/form-data">
            <input type="hidden" name="id" value="<%=id%>">
            <%=HTML.textInput("headline", "Headline", "50", "100", headline)%>
            <%=HTML.textArea("body", "Body", body, "input-xxlarge", 15)%>
            <%=HTML.fileInput("photo", ((a != null) ? "Update Photo" : "Add Photo"), null)%>
            <%
                if (a != null) {
            %>
            <div class="control-group">
            <label class="control-label" for="textarea">&nbsp;</label>
            <div class="controls">
              <img width="200" height="150" src="<%=a.getImageUrl(Attachment.ImageSize.f320)%>">
            </div>
          </div>
            <%
                }
            %>
            <%--
            <div class="control-group"><label for="Media" class="control-label">Body Parts and Types</label>
                <div class="controls">
                <% if (id == 0) {%>
                Select types and body parts after saving
                <% } else { %>
                    <% if (info.getBodyParts() != null && info.getBodyParts().size() > 0) {%>
                    <b>Body Parts:</b>
                    <%
                        int i = 0;
                        for (Exercise.BodyPart bp : info.getBodyParts()) {
                    %>
                    <%=bp.getValue()%><%=(++i < info.getBodyParts().size()) ? "," : ""%>
                    <%
                        }
                    %>
                    <% } %>
                    <% if (info.getTypes() != null && info.getTypes().size() > 0) {%>
                    <br><b>Exercise Types:</b>
                    <%
                        int i = 0;
                        for (Exercise.Type type : info.getTypes()) {
                    %>
                    <%=type.getValue()%><%=(++i < info.getTypes().size()) ? "," : ""%>
                    <%
                        }
                    %>
                    <% } %>
                    <br>
                <a href="inline/bodyparts.jsp?id=<%=id%>" class="btn fancybox btn-primary">Edit Body Parts</a>
                <% } %>
            </div>

            </div>
            --%>
            <% if (id != 0) {%>
            <div class="control-group" style="margin-top: 15px;">
                <label for="Delete" class="control-label">&nbsp;</label>
                <div class="controls">


                <a href="<%=Config.getUrl()%>/info.jsp?id=<%=id%>" class="btn fancybox">Preview</a>
                <a href="inline/delete.jsp?id=<%=id%>" class="btn btn-danger fancybox">Delete Article</a>

            </div>
            </div>
                <% } %>
            <%=HTML.saveAndClose()%>
        </form>
    </div>
</div>
</body>
</html>