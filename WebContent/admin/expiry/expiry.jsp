<%@page import="au.com.ourbodycorp.model.Program"%>
<%@page import="au.com.ourbodycorp.model.Patient"%>
<%@page import="au.com.ourbodycorp.model.User"%>
<%@page import="au.com.ourbodycorp.model.CorporationUser"%>
<%@page import="au.com.ourbodycorp.model.managers.UserManager"%>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.Corporation" %>
<%@ page import="au.com.ourbodycorp.model.managers.CorporationManager" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.List" %>
<%
    LibraryManager lm = new LibraryManager();
    UserManager userManager = new UserManager();
    CorporationManager cm = new CorporationManager();
    User u = (User) session.getAttribute("user");
    String activity_page="admin/expiry/expiry.jsp";
    String corp_id="---";
	String corp_name="---";
	String corp_loc="---";
	String user_id=String.valueOf(u.getId());
	String user_name="Fiizio Admin";
	String invitor_id="";
	String invitor_name="---";
	String flag="search";
	String from="---";
	String to="---";
    try{
    	//Util.allActivitiesLogs("Admin", u.getId(), "Fiizio System Admin", "View Practice Reports");
		Util.allActivitiesLogs2("Admin",activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,"View Practice Reports By Fiizio System Admin:"+u.getFullName());
    }catch(Exception e){
    	
    } 
    String type = Util.notNull(request.getParameter("type"), "all");

    List<Corporation> corporations;
    boolean showExpiry = false;
    if (type.equalsIgnoreCase("all")) {
        corporations = cm.getAllCorporations();
    } 
    else if (type.equalsIgnoreCase("currentTrial")) {
        corporations = cm.getAllActiveTrialCorporations();
    } 
    else if (type.equalsIgnoreCase("expiredTrial")) {
        corporations = cm.getExpiredCorporations();
    }
    else{
//        showExpiry = false;
        corporations = cm.getCorporationsOfType(type);
    }
    SimpleDateFormat sdf = new SimpleDateFormat("d MMM yy");
%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head><title>Practice Report</title></head>
<body>
<div class="page-header">
    <h1>Practice Report</h1>
</div>
<div class="row">
    <div class="span9">
        <ul class="nav nav-tabs">
            <li class="<%=(type.equalsIgnoreCase("all")) ? "active" : ""%>"><a href="expiry.jsp">All</a></li>
            <li class="<%=(type.equalsIgnoreCase("currentTrial")) ? "active" : ""%>"><a href="expiry.jsp?type=currentTrial">Current Trial</a></li>
            <li class="<%=(type.equalsIgnoreCase("expiredTrial")) ? "active" : ""%>"><a href="expiry.jsp?type=expiredTrial">Expired Trial</a></li>
            <li class="<%=(type.equalsIgnoreCase("solo")) ? "active" : ""%>"><a href="expiry.jsp?type=solo">Solo</a></li>
            <li class="<%=(type.equalsIgnoreCase("small")) ? "active" : ""%>"><a href="expiry.jsp?type=small">Small</a></li>
            <li class="<%=(type.equalsIgnoreCase("large")) ? "active" : ""%>"><a href="expiry.jsp?type=large">Large</a></li>
            <li class="<%=(type.equalsIgnoreCase("unlimited")) ? "active" : ""%>"><a href="expiry.jsp?type=unlimited">Unlimited</a></li>
        </ul>
    </div>
</div>
<div class="row">
    <div class="span9">
        <table class="table table-zebra-stripes">
            <thead>
            <tr>
                <th>Name</th>
                <th style="text-align: center">Type</th>
                <th style="text-align: center"><%=(showExpiry)? "Expires" : "Signed Up"%></th>
                <th style="text-align: center">Trial Progress</th>
                <th style="text-align: center">Program Downloaded</th>
            </tr>
            </thead>
            <tbody>
            <%
                for (Corporation corp : corporations) {
            %>
            <tr>
                <td class="span6"><a href="../clients/corporation.jsp?id=<%=corp.getId()%>"><%=corp.getName()%></a></td>
                <td class="" style="text-align: center"><%=corp.getSubType()%></td>
                <% if (showExpiry){ %>
                    <td class="" style="text-align: center"><%=(corp.getExpiry() == null)? "-" : sdf.format(corp.getExpiry())%></td>
                <% } else { %>
                    <td class="" style="text-align: center"><%=(corp.getCreated()== null)? "-" : sdf.format(corp.getCreated())%></td>
                <% } %>
                <% 
                    List<CorporationUser> users = cm.getUsersForCorporation(corp.getId());
                    User firstUser = (users == null || users.size() == 0)? null : users.get(0).getUser(); 
                    int tutorialProgress = (firstUser == null)? UserManager.TUTORIAL_NOT_DONE :  userManager.getTutorialProgressForUser(firstUser.getId()); 
                    String progress = "N/A";
                    if (UserManager.TUTORIAL_NOT_DONE == tutorialProgress){
                        progress = "Tutorial Not Started";
                    }
                    else if (UserManager.TUTORIAL_WENT_TO_PATIENT_PAGE == tutorialProgress){
                        progress = "Tutorial Part 2";
                    }
                    else if (UserManager.TUTORIAL_WENT_TO_PROGRAM_PAGE == tutorialProgress){
                        progress = "Tutorial Part 3";
                    }
                    else if (UserManager.TUTORIAL_FINISHED == tutorialProgress){
                        progress = "Tutorial Finished";
                    }
                %>
                <td class="" style="text-align: center"><%=progress%></td>
                <% 
                    
                    String programDownloadDate = "Never";
                    List<Patient> patients = lm.getPatients(corp.getId(), 0, 1);
                    Patient firstPatient = (patients != null && patients.size() > 0)? patients.get(0) : null;
                    if (firstPatient != null){
                         List<Program> programs = lm.getPrograms(firstPatient.getId());
                         if (programs != null && programs.size() > 0){
                             Program firstProgram = programs.get(0);
                             if (firstProgram.getRetrieved() != null){
                                 programDownloadDate = sdf.format(firstProgram.getRetrieved());
                             }
                         }
                    }
                   
                    
                %>
                <td class="" style="text-align: center"><%=programDownloadDate%></td>
            </tr>
            <%
                }
            %>
            </tbody>
        </table>
    </div>
</div>
<a href="/admin/export/corporations.csv?type=<%=type%>">Export to CSV</a>
</body>
</html>