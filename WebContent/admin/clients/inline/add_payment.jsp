<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.Payment" %>
<%@ page import="au.com.ourbodycorp.model.managers.CorporationManager" %>
<%@ page import="java.math.BigDecimal" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="au.com.ourbodycorp.model.CorpInvoice" %>
<%@ page import="au.com.ourbodycorp.model.Corporation" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="java.lang.ref.ReferenceQueue" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    long invoiceId = Long.parseLong(Util.notNull(request.getParameter("invoice"), "0"));
    long corpId = Long.parseLong(Util.notNull(request.getParameter("corpId"), "0"));
    long id = Long.parseLong(Util.notNull(request.getParameter("id"), "0"));
    String date = Util.notNull(request.getParameter("date"));
    String source = Util.notNull(request.getParameter("source"));
    String amount = Util.notNull(request.getParameter("amount"));
    String reference = Util.notNull(request.getParameter("ref"));
    String txn = Util.notNull(request.getParameter("txn"));
    String email = Util.notNull(request.getParameter("email"));
    String fname = Util.notNull(request.getParameter("fname"));
    String lname = Util.notNull(request.getParameter("lname"));
    String newExpiry = Util.notNull(request.getParameter("newExpiry"));

    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

    CorporationManager cm = new CorporationManager();

    CorpInvoice ci = null;
    if (invoiceId > 0) {
        ci = cm.getCorpInvoice(invoiceId);
        if (ci == null) {
            response.sendError(404);
            return;
        } else {
            corpId = ci.getCorpId();
        }
    }

    Corporation corp = cm.getCorporation(corpId);

    String title = "Add";

    Payment p = null;
    if (id > 0) {
        p = cm.getPayment(id);
        if (p == null) {
            response.sendError(404);
            return;
        }
    }

    if (request.getMethod().equalsIgnoreCase("post")) {
        if (request.getParameter("submit").equals("Cancel")) {
            response.sendRedirect("/close.jsp");
            return;
        }
        if (p == null) {
            p = new Payment();
            if (newExpiry.length() > 0) {
                p.setNewExpiry(sdf.parse(newExpiry));
            }
        }
        p.setAmount(new BigDecimal(Double.parseDouble(amount)));
        p.setDate(sdf.parse(date));
        p.setCorpId(corpId);
        p.setSource(source);
        p.setCurrency("AUD");
        p.setReference(reference);
        p.setTransactionId(txn);
        p.setEmail(email);
        p.setFirstName(fname);
        p.setLastName(lname);

        if (p.getId() == 0) {
            cm.save(p, p.getNewExpiry());
        } else {
            cm.save(p, null);
        }

        if (corp.getMaxUnits() == 0) {
            corp.setMaxUnits(cm.getUnitsForCorp(corp.getId()).size());
            cm.save(corp, 0);
        }

        response.sendRedirect("/close.jsp?reload=true");
        return;
    } else if (p != null) {
        title = "Edit";
        corpId = p.getCorpId();
        date = sdf.format(p.getDate());
        source = p.getSource();
        amount = Double.toString(p.getAmount().doubleValue());
        reference = p.getReference();
        txn = p.getTransactionId();
        email = p.getEmail();
        fname = p.getEmail();
        lname = p.getEmail();
        if (p.getNewExpiry() != null) {
            newExpiry = sdf.format(p.getNewExpiry());
        }
    } else {
        date = sdf.format(new Date());
    }

    if (request.getMethod().equalsIgnoreCase("get") && ci != null) {
        if (corp.getExpiry() == null) {
            corp.setExpiry(new Date());
        }
        Calendar cal = Calendar.getInstance();
        if (corp.getExpiry().before(new Date())) {
            cal.setTime(new Date());
        } else {
            cal.setTime(corp.getExpiry());
        }
        cal.add(Calendar.YEAR, 1);
        newExpiry = sdf.format(cal.getTime());

        amount = Double.toString(ci.getAmount().doubleValue());
    }

    String[] sources = {"EFT", "Cheque", "PayPal", "Cash"};
%>
<html>
<head>
    <title><%=title%> Payment</title>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            $("#date").datepicker({
                dateFormat: "dd/mm/yy",
                changeMonth: true,
			    changeYear: true
            });
            $("#ndate").datepicker({
                dateFormat: "dd/mm/yy",
                changeMonth: true,
			    changeYear: true
            });
        });
    </script>
</head>
<body>
<div class="page-header">
    <h1><%=title%> Payment <small><%=corp.getName()%></small></h1>
</div>
<div class="row">
    <div class="span6">
        <form action="add_payment.jsp" method="post" class="form-horizontal">
            <input type="hidden" name="invoice" value="<%=invoiceId%>">
            <input type="hidden" name="id" value="<%=id%>">
            <input type="hidden" name="corpId" value="<%=corpId%>">
            <div class="control-group"><label for="date" class="control-label">Payment Date</label><div class="controls"><input class="input" id="date" type="text" size="15" maxlen="15" name="date" value="<%=date%>"></div></div>
            <%=HTML.select("source", "Source", sources, sources, source, null, null, true)%>
            <%=HTML.textInput("amount", "Amount", "30", "30", amount)%>
            <%=HTML.textInput("ref", "Reference", "30", "30", reference)%>
            <%=HTML.textInput("txn", "Transaction ID", "30", "30", txn)%>
            <%=HTML.textInput("email", "Email", "30", "30", email)%>
            <%=HTML.textInput("fname", "First Name", "30", "30", fname)%>
            <%=HTML.textInput("lname", "Last Name", "30", "30", lname)%>
            <div class="control-group"><label for="ndate" class="control-label">New Expiry</label><div class="controls"><input class="input" id="ndate" type="text" size="15" maxlen="15" name="newExpiry" value="<%=newExpiry%>" <%=(p != null) ? "disabled" : ""%>></div></div>
            <%=HTML.saveCancel()%>
        </form>
    </div>
</div>
</body>
</html>