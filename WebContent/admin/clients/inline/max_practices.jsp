<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.Company" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="au.com.ourbodycorp.Config" %>
<%@ page import="au.com.ourbodycorp.model.User" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    LibraryManager lm = new LibraryManager();
    Company company = lm.getCompany(Long.parseLong(request.getParameter("id")));
    String max = Util.notNull(request.getParameter("max"), "0").trim();
    String max_paractices =String.valueOf(company.getMaxCorp());
    User uu = (User) session.getAttribute("user");
    
    if (max.length() == 0) {
        max = "0";
    }

    if (request.getMethod().equalsIgnoreCase("post")) {
        if (request.getParameter("submit").equals("Cancel")) {
        	if(Config.getString("https").equals("true")){
        		response.sendRedirect("https://"+request.getHeader("host")+"/close.jsp");
        	}else{
				response.sendRedirect("http://"+request.getHeader("host")+"/close.jsp");
			}
            return;
        }

        try {
            company.setMaxCorp(Integer.parseInt(max.trim()));
            lm.save(company);
            String type="Clients";
            String activity_page="admin/clients/inline/max_practices.jsp";
            String corp_id="---";
        	String corp_name="---";
        	String corp_loc="---";
        	String user_id=String.valueOf(uu.getId());
        	String user_name="Fiizio Admin";
        	String invitor_id=String.valueOf(company.getId());
        	String invitor_name=company.getName();
        	String flag="update";
        	String from=max_paractices;
        	String to=max;
        	String desc="Upadating Maximum practices to "+company.getName()+" as "+max+" By Fiizio System Admin:"+uu.getFullName();
            try{
        	//	Util.allActivitiesLogs("Clients", uu.getId(), "Fiizio System Admin", "Maximum practices");
        		Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
        	}catch(Exception ee){
        		
        	}
            if(Config.getString("https").equals("true")){
                response.sendRedirect("https://"+request.getHeader("host")+"/close.jsp?reload=true");
        	}else{
                response.sendRedirect("http://"+request.getHeader("host")+"/close.jsp?reload=true");
        	}
            return;
        } catch (NumberFormatException nfe) {
            //nothing.
        }

    } else {
        max = Integer.toString(company.getMaxCorp());
    }
%>
<html>
<head><title>Number of Practices</title></head>
<body>
<div class="page-header">
    <h1>Maximum practices <small><%=company.getName()%></small></h1>
</div>
<div class="row">
    <div class="span6">
        <form method="post" action="max_practices.jsp" class="form-horizontal">
            <input type="hidden" name="id" value="<%=company.getId()%>">
        <%=HTML.textInput("max", "Max Practices", "30", "30", max, "Blank or 0 for unlimited", false)%>
        <%=HTML.saveCancel()%>
    </form>
    </div>
</div>


</body>
</html>