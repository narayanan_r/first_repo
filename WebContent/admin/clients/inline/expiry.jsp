<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.Corporation" %>
<%@ page import="au.com.ourbodycorp.model.managers.CorporationManager" %>
<%@ page import="java.text.ParseException" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%

    CorporationManager cm = new CorporationManager();
    Corporation corp = cm.getCorporation(Long.parseLong(request.getParameter("id")));
    String dateString = Util.notNull(request.getParameter("date")).trim();
    String subType = Util.notNull(request.getParameter("type"), "trial");

    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy");

    String error = null;
    if (request.getMethod().equalsIgnoreCase("post")) {
        if (request.getParameter("submit").equals("Cancel")) {
            response.sendRedirect("/close.jsp");
            return;
        }


        Date date = null;
        if (dateString.length() == 0) {
            error = "Please enter a date";
        } else {
            try {
                date = sdf.parse(dateString);
            } catch (ParseException pe) {
                error = "Please enter a valid date";
            }
        }

        if (error == null) {
            corp.setExpiry(date);
            corp.setSubType(subType);
            cm.save(corp,  0);
            response.sendRedirect("/close.jsp?reload=true");
            return;
        }


    } else {
        dateString = (corp.getExpiry() != null) ? sdf.format(corp.getExpiry()) : "";
        subType = Util.notNull(corp.getSubType(), "trial");
    }
    String[] typeValues = {"trial", "paid", "free"};
    String[] typeLabels = {"Trial", "Paid", "Free"};
%>
<html>
<head>
    <title>Expiry Date</title>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            $("#date").datepicker({
                dateFormat: "dd/mm/yy",
                changeMonth: true,
			    changeYear: true
            });
        });
    </script>
    <script type="text/javascript" src="/js/jquery.colorbox-min.js"></script>
</head>
<body>
<div class="page-header">
    <h1>Expiry Date <small><%=corp.getName()%></small></h1>
</div>
<div class="row">
    <div class="span6">
        <form method="post" action="expiry.jsp" class="form-horizontal">
            <input type="hidden" name="id" value="<%=corp.getId()%>">
        <div class="control-group"><label for="date" class="control-label">Expiry Date</label><div class="controls"><input class="input" id="date" type="text" size="15" maxlen="15" name="date" value="<%=dateString%>"></div></div>
            <%=HTML.select("type", "Subscription Type", typeValues, typeLabels, subType, null, null, false)%>
        <%=HTML.saveCancel()%>
    </form>
    </div>
</div>


</body>
</html>