<%@ page import="au.com.ourbodycorp.HTML" %>
<%@page import="au.com.ourbodycorp.model.Corporation"%>
<%@ page import="au.com.ourbodycorp.model.Company" %>
<%@page import="au.com.ourbodycorp.model.managers.CorporationManager"%>
<%@ page import="au.com.ourbodycorp.model.User" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="au.com.ourbodycorp.model.managers.UserManager" %>
<%@ page import="au.com.ourbodycorp.Config" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    LibraryManager lm = new LibraryManager();
    UserManager um = new UserManager();
    Company company = lm.getCompany(Long.parseLong(request.getParameter("id")));
    User uu = (User) session.getAttribute("user");
    CorporationManager cm=new CorporationManager();
    User u = um.getUser(Long.parseLong(request.getParameter("user")));
    String error = null;
    if (request.getMethod().equalsIgnoreCase("post")) {
        if (request.getParameter("submit").equals("Cancel")) {
        	if(Config.getString("https").equals("true")){
        		response.sendRedirect("https://"+request.getHeader("host")+"/close.jsp");
        	}else{
				response.sendRedirect("http://"+request.getHeader("host")+"/close.jsp");
			}
            return;
        }
        String type="Clients";
        String activity_page="admin/clients/inline/delete_owner.jsp";
        String corp_id="---";
    	String corp_name="---";
    	String corp_loc="---";
    	String user_id=String.valueOf(uu.getId());
    	String user_name="Fiizio Admin";
    	String invitor_id=String.valueOf(u.getId());
    	String invitor_name=u.getFullName();
    	String flag="delete";
    	String from="---";
    	String to="---";
    	String desc="Delete Owner From Corporation By Fiizio System Admin:"+uu.getFullName();
        try{
    		//Util.allActivitiesLogs("Clients", uu.getId(), "Fiizio System Admin", "Delete Owner");
    		Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
    	}catch(Exception ee){
    		
    	}

        lm.deleteOwnerFromCompany(company.getId(), u.getId());
        

        if(Config.getString("https").equals("true")){
            response.sendRedirect("https://"+request.getHeader("host")+"/close.jsp?reload=true");
    	}else{
            response.sendRedirect("http://"+request.getHeader("host")+"/close.jsp?reload=true");
    	}
        return;
    }
%>
<html>
<head><title>Remove Owner</title></head>
<body>
<div class="page-header">
    <h1>Remove Owner <small><%=company.getName()%></small></h1>
</div>
<%@include file="/WEB-INF/includes/formErrorSimple.jsp"%>
<div class="row">
    <div class="span6">
        <p>
            Remove <b><%=u.getFullName()%></b> from the owners list of <%=company.getName()%>. Are you Sure?
        </p>
        <form method="post" action="delete_owner.jsp" class="form-horizontal">
            <input type="hidden" name="id" value="<%=company.getId()%>">
            <input type="hidden" name="user" value="<%=u.getId()%>">

        <%=HTML.confirmCancel()%>
    </form>
    </div>
</div>


</body>
</html>