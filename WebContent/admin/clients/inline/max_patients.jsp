<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.Corporation" %>
<%@ page import="au.com.ourbodycorp.model.managers.CorporationManager" %>
<%@ page import="au.com.ourbodycorp.Config" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%

    CorporationManager cm = new CorporationManager();
    Corporation corp = cm.getCorporation(Long.parseLong(request.getParameter("id")));
    String max = Util.notNull(request.getParameter("max"), "0").trim();

    if (max.length() == 0) {
        max = "0";
    }

    if (request.getMethod().equalsIgnoreCase("post")) {
        if (request.getParameter("submit").equals("Cancel")) {
        	if(Config.getString("https").equals("true")){
        		response.sendRedirect("https://"+request.getHeader("host")+"/close.jsp");
        	}else{
				response.sendRedirect("http://"+request.getHeader("host")+"/close.jsp");
			}
            return;
        }

        try {
            corp.setMaxPatients(Integer.parseInt(max.trim()));
            cm.save(corp,  0);
            if(Config.getString("https").equals("true")){
                response.sendRedirect("https://"+request.getHeader("host")+"/close.jsp?reload=true");
        	}else{
                response.sendRedirect("http://"+request.getHeader("host")+"/close.jsp?reload=true");
        	}
            return;
        } catch (NumberFormatException nfe) {
            //nothing.
        }

    } else {
        max = Integer.toString(corp.getMaxPatients());
    }
%>
<html>
<head><title>Number of Patients</title></head>
<body>
<div class="page-header">
    <h1>Maximum patients <small><%=corp.getName()%></small></h1>
</div>
<div class="row">
    <div class="span6">
        <form method="post" action="max_patients.jsp" class="form-horizontal">
            <input type="hidden" name="id" value="<%=corp.getId()%>">
        <%=HTML.textInput("max", "Max Patiends", "30", "30", max, "Blank or 0 for unlimited", false)%>
        <%=HTML.saveCancel()%>
    </form>
    </div>
</div>


</body>
</html>