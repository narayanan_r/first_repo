<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.Corporation" %>
<%@ page import="au.com.ourbodycorp.model.managers.CorporationManager" %>
<%@ page import="java.text.ParseException" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="au.com.ourbodycorp.Config" %>
<%@ page import="au.com.ourbodycorp.model.User" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%

    CorporationManager cm = new CorporationManager();
    Corporation corp = cm.getCorporation(Long.parseLong(request.getParameter("id")));
    String subType = Util.notNull(request.getParameter("subType"), "trial");
    String subscriptionType = corp.getSubType();
    User uu = (User) session.getAttribute("user");
    
    if (request.getMethod().equalsIgnoreCase("post")) {
        if (request.getParameter("submit").equals("Cancel")) {
        	if(Config.getString("https").equals("true")){
        		response.sendRedirect("https://"+request.getHeader("host")+"/close.jsp");
        	}else{
				response.sendRedirect("http://"+request.getHeader("host")+"/close.jsp");
			}
            return;
        }

        corp.setSubType(subType);
        cm.save(corp,  0);
        String type="Clients";
        String activity_page="admin/clients/inline/subscription.jsp";
        String corp_id="---";
    	String corp_name="---";
    	String corp_loc="---";
    	String user_id=String.valueOf(uu.getId());
    	String user_name="Fiizio Admin";
    	String invitor_id="---";
    	String invitor_name="---";
    	String flag="update";
    	String from=subscriptionType;
    	String to=subType;
    	String desc="View Subscription Type  to "+corp.getName()+" as "+subType+" By Fiizio System Admin:"+uu.getFullName();
        /* try{
        	//Util.allActivitiesLogs("Clients", uu.getId(), "Fiizio System Admin", "View Subscription Type");
    		Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
        }catch(Exception ee){
        	
        } */
        if(Config.getString("https").equals("true")){
            response.sendRedirect("https://"+request.getHeader("host")+"/close.jsp?reload=true");
    	}else{
            response.sendRedirect("http://"+request.getHeader("host")+"/close.jsp?reload=true");
    	}
        
        return;
    } 
    else {

        subType = Util.notNull(corp.getSubType(), "trial");
    }
    String[] typeValues = {"trial", "solo", "small", "large", "unlimited"};
    String[] typeLabels = {"Trial", "Solo Practitioner", "Small Practice", "Large Practice", "Unlimited"};
%>
<html>
<head>
    <title>Subscription Type</title>
    <script type="text/javascript" src="/js/jquery.colorbox-min.js"></script>
</head>
<body>
<div class="page-header">
    <h1>Subscription Type <small><%=corp.getName()%></small></h1>
</div>
<div class="row">
    <div class="span6">
        <form method="post" action="subscription.jsp" class="form-horizontal">
            <input type="hidden" name="id" value="<%=corp.getId()%>">
            
            <%=HTML.select("subType", "Subscription Type", typeValues, typeLabels, subType, null, null, false)%>
        
            <%=HTML.saveCancel()%>
        </form>
    </div>
</div>


</body>
</html>