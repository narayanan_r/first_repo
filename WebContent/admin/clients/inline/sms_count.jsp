<%@page import="au.com.ourbodycorp.model.Corporation"%>
<%@page import="au.com.ourbodycorp.model.managers.CorporationManager"%>
<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.Company" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="au.com.ourbodycorp.Config" %>
<%@ page import="au.com.ourbodycorp.model.User" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    CorporationManager cm = new CorporationManager();
    Corporation corp = cm.getCorporation(Long.parseLong(request.getParameter("id")));
    User uu = (User) session.getAttribute("user");
    String sms_C = request.getParameter("sms_count");
    String sms_R = String.valueOf(corp.getSmsCount());
	if (corp == null){
    	if(Config.getString("https").equals("true")){
            response.sendRedirect("https://"+request.getHeader("host")+"/close.jsp?reload=true");
    	}else{
            response.sendRedirect("http://"+request.getHeader("host")+"/close.jsp?reload=true");
    	}
        return;
    }
 
    if (request.getMethod().equalsIgnoreCase("post")) {
        if (request.getParameter("submit").equals("Cancel")) {
        	if(Config.getString("https").equals("true")){
        		response.sendRedirect("https://"+request.getHeader("host")+"/close.jsp");
        	}else{
				response.sendRedirect("http://"+request.getHeader("host")+"/close.jsp");
			}
            return;
        }

        try {
            String newSmsCount = request.getParameter("sms_count");
            corp.setSmsCount(Integer.parseInt(newSmsCount.trim()));
            cm.save(corp, 0);
            String type="Clients";
            String activity_page="admin/clients/inline/sms_count.jsp";
            String corp_id="---";
        	String corp_name="---";
        	String corp_loc="---";
        	String user_id=String.valueOf(uu.getId());
        	String user_name="Fiizio Admin";
        	String invitor_id=String.valueOf(corp.getId());
        	String invitor_name="---";
        	String flag="update";
        	String from=sms_R;
        	String to=sms_C;
        	String desc="Upadating SMS Remaining  to "+corp.getName()+" as "+sms_C+" By Fiizio System Admin:"+uu.getFullName();
            try{
            	//Util.allActivitiesLogs("Clients", uu.getId(), "Fiizio System Admin", "SMS Remaining");
        		Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
            }catch(Exception ee){
            	
            }
            if(Config.getString("https").equals("true")){
                response.sendRedirect("https://"+request.getHeader("host")+"/close.jsp?reload=true");
        	}else{
                response.sendRedirect("http://"+request.getHeader("host")+"/close.jsp?reload=true");
        	}
            return;
            
        } 
        catch (NumberFormatException nfe) {
            //nothing.
        }
    }
    
     
%>
<html>
<head><title>Number of SMS Remaining</title></head>
<body>
<div class="page-header">
    <h1>SMS Remaining <small><%=corp.getName()%></small></h1>
</div>
<div class="row">
    <div class="span6">
        <form method="post" action="sms_count.jsp" class="form-horizontal">
            <input type="hidden" name="id" value="<%=corp.getId()%>">
        <%=HTML.textInput("sms_count", "SMS Remaining", "10", "10", ""+corp.getSmsCount(), "Change how many SMS are left for this corporation", false)%>
        <%=HTML.saveCancel()%>
    </form>
    </div>
</div>


</body>
</html>