<%@page import="au.com.ourbodycorp.model.Corporation"%>
<%@page import="au.com.ourbodycorp.model.managers.CorporationManager"%>
<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.Company" %>
<%@ page import="au.com.ourbodycorp.model.User" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="au.com.ourbodycorp.model.managers.UserManager" %>
<%@ page import="au.com.ourbodycorp.Config" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    LibraryManager lm = new LibraryManager();
    Company company = lm.getCompany(Long.parseLong(request.getParameter("id")));
    String email = Util.notNull(request.getParameter("email")).trim();
    User uu = (User) session.getAttribute("user");
    CorporationManager cm=new CorporationManager();
    UserManager um=new UserManager();
    /* try{
		//Util.allActivitiesLogs("Clients", uu.getId(), "Fiizio System Admin", "Add Owner");
		Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
	}catch(Exception ee){
		
	} */
    String error = null;

    if (request.getMethod().equalsIgnoreCase("post")) {
        if (request.getParameter("submit").equals("Cancel")) {
        	if(Config.getString("https").equals("true")){
        		response.sendRedirect("https://"+request.getHeader("host")+"/close.jsp");
        	}else{
				response.sendRedirect("http://"+request.getHeader("host")+"/close.jsp");
			}
            return;
        }
        User u = um.getUserByEmail(email);
        if (u != null) {
        	String type="Clients";
            String activity_page="admin/clients/inline/add_owner.jsp";
        	String corp_id="---";
        	String corp_name="---";
        	String corp_loc="---";
        	String user_id=String.valueOf(uu.getId());
        	String user_name="Fiizio Admin";
        	String invitor_id=String.valueOf(u.getId());
        	String invitor_name=u.getFullName();
        	String flag="invite";
        	String from="---";
        	String to="---";
        	String desc="Add Owner "+email+" into Corporation By Fiizio System Admin:"+uu.getFullName();
            try{
        		//Util.allActivitiesLogs("Clients", uu.getId(), "Fiizio System Admin", "Add Owner");
        		Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
        	}catch(Exception ee){
        		
        	}
            if (!lm.isCompanyOwner(company.getId(), u.getId())) {
                lm.addOwnerToCompany(company.getId(), u.getId());
            }
            if(Config.getString("https").equals("true")){
                response.sendRedirect("https://"+request.getHeader("host")+"/close.jsp?reload=true");
        	}else{
                response.sendRedirect("http://"+request.getHeader("host")+"/close.jsp?reload=true");
        	}
            return;
        } else {
            error = "User not found. Check the email and try again.";
        }
        

        

    }
%>
<html>
<head><title>Add Owner</title></head>
<body>
<div class="page-header">
    <h1>Add Owner <small><%=company.getName()%></small></h1>
</div>
<%@include file="/WEB-INF/includes/formErrorSimple.jsp"%>
<div class="row">
    <div class="span6">
        <form method="post" action="add_owner.jsp" class="form-horizontal">
            <input type="hidden" name="id" value="<%=company.getId()%>">
        <%=HTML.textInput("email", "User Email", "30", "30", email, null, false)%>
        <%=HTML.saveCancel()%>
    </form>
    </div>
</div>


</body>
</html>