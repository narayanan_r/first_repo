<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.Corporation" %>
<%@ page import="au.com.ourbodycorp.model.managers.CorporationManager" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%

    CorporationManager cm = new CorporationManager();
    Corporation corp = cm.getCorporation(Long.parseLong(request.getParameter("id")));
    String max = Util.notNull(request.getParameter("max"), "0").trim();

    if (max.length() == 0) {
        max = "0";
    }

    if (request.getMethod().equalsIgnoreCase("post")) {
        if (request.getParameter("submit").equals("Cancel")) {
            response.sendRedirect("/close.jsp");
            return;
        }

        try {
            corp.setMaxUnits(Integer.parseInt(max.trim()));
            cm.save(corp,  0);
            response.sendRedirect("/close.jsp?reload=true");
            return;
        } catch (NumberFormatException nfe) {
            //nothing.
        }

    } else {
        max = Integer.toString(corp.getMaxUnits());
    }
%>
<html>
<head><title>Number of Units</title></head>
<body>
<div class="page-header">
    <h1>Maximum Units <small><%=corp.getName()%></small></h1>
</div>
<div class="row">
    <div class="span6">
        <form method="post" action="max_units.jsp" class="form-horizontal">
            <input type="hidden" name="id" value="<%=corp.getId()%>">
        <%=HTML.textInput("max", "Max Units", "30", "30", max, "Blank or 0 for unlimited", false)%>
        <%=HTML.saveCancel()%>
    </form>
    </div>
</div>


</body>
</html>