<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.Corporation" %>
<%@ page import="au.com.ourbodycorp.model.managers.CorporationManager" %>
<%@ page import="au.com.ourbodycorp.Config" %>
<%@ page import="au.com.ourbodycorp.model.User" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%

    CorporationManager cm = new CorporationManager();
    Corporation corp = cm.getCorporation(Long.parseLong(request.getParameter("id")));
    String reason = Util.notNull(request.getParameter("reason"), "").trim();
    User uu = (User) session.getAttribute("user");
    
    if (reason.length() == 0) {
        reason = null;
    }

    if (request.getMethod().equalsIgnoreCase("post")) {
        if (request.getParameter("submit").equals("Cancel")) {
        	if(Config.getString("https").equals("true")){
        		response.sendRedirect("https://"+request.getHeader("host")+"/close.jsp");
        	}else{
				response.sendRedirect("http://"+request.getHeader("host")+"/close.jsp");
			}
            return;
        }


        corp.setLocked(reason);
        cm.save(corp,  0);
        String type="Clients";
        String activity_page="admin/clients/inline/lock.jsp";
        String corp_id="---";
    	String corp_name="---";
    	String corp_loc="---";
    	String user_id=String.valueOf(uu.getId());
    	String user_name="Fiizio Admin";
    	String invitor_id="---";
    	String invitor_name="---";
    	String flag="lock";
    	String from="---";
    	String to="---";
    	String desc="Lock/Unlock Practice to "+corp.getName()+" for "+reason+" By Fiizio System Admin:"+uu.getFullName();
        try{
//        	Util.allActivitiesLogs("Clients", uu.getId(), "Fiizio System Admin", "Lock Practice");
    		Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
        }catch(Exception ee){
        	
        }
        if(Config.getString("https").equals("true")){
            response.sendRedirect("https://"+request.getHeader("host")+"/close.jsp?reload=true");
    	}else{
            response.sendRedirect("http://"+request.getHeader("host")+"/close.jsp?reload=true");
    	}
        return;

    } else {
        reason = corp.getLocked();
    }
%>
<html>
<head><title>Lock Practice</title></head>
<body>
<div class="page-header">
    <h1>Lock Practice <small><%=corp.getName()%></small></h1>
</div>
<div class="row">
    <div class="span6">
        <form method="post" action="lock.jsp" class="form-horizontal">
            <input type="hidden" name="id" value="<%=corp.getId()%>">
        <%=HTML.textArea("reason", "Lock Reason", reason, 7)%>
        <p>Remove all text from reason to unlock.</p>
        <%=HTML.saveCancel()%>
    </form>
    </div>
</div>


</body>
</html>