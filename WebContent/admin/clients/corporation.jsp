<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.Company" %>
<%@ page import="au.com.ourbodycorp.model.Corporation" %>
<%@ page import="au.com.ourbodycorp.model.SMSStat" %>
<%@ page import="au.com.ourbodycorp.model.managers.CorporationManager" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.List" %>
<%@ page import="au.com.ourbodycorp.Config" %>
<%@ page import="au.com.ourbodycorp.model.User" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    CorporationManager cm = new CorporationManager();
    LibraryManager lm = new LibraryManager();
    Corporation c=cm.getCorporation(Long.parseLong(request.getParameter("id")));
    User u = (User) session.getAttribute("user");
    String type="Clients";
    String activity_page="admin/clients/corporation.jsp";
    String corp_id="---";
	String corp_name="---";
	String corp_loc="---";
	String user_id=String.valueOf(u.getId());
	String user_name="Fiizio Admin";
	String invitor_id="---";
	String invitor_name="---";
	String flag="search";
	String from="---";
	String to="---";
     try{
    	//Util.allActivitiesLogs("Clients", u.getId(), "Fiizio System Admin", "View Practice Details");
		Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,"View Practice Details in "+c.getName()+" By Fiizio System Admin:"+u.getFullName());

    }catch(Exception ee){
    	
    } 
    Corporation corporation = cm.getCorporation(Long.parseLong(request.getParameter("id")));
    Company company = lm.getCompany(corporation.getCompanyId());
    
    String[][] breadcrumbs = {{"/admin/clients/", "Clients"},  {"company.jsp?id=" + company.getId(), company.getName()}, {"", corporation.getName()}};
    SimpleDateFormat sdf = new SimpleDateFormat("d MMM yyy");

    List<SMSStat> sms = lm.getSMSUageByMonth(corporation.getId(), 12);

    int patients = lm.simpleCount("select count(*) as num from patient where corp_id = " + corporation.getId());
    int programs = lm.simpleCount("select count(*) as num from programs where corp_id = " + corporation.getId());
    int users = lm.simpleCount("select count(distinct userid) as num from corp_users where corpid = " + corporation.getId());

%>
<html>
<head><title><%=corporation.getName()%></title>
<script type="text/javascript">
	function slugUrl(slugUrl) {
		$.ajax({
	        url: '<%=Config.getString("url")%>/corp/slugurl_changer.jsp?slugUrl='+slugUrl+'',
	        type: 'POST',
	        success: function(data) {
	      	   window.location = '<%=Config.getString("url")%>/corp/';
	        }
	      });
	}
</script>
</head>
<body>
<div class="page-header">
    <h1>Practice <small><%=corporation.getName()%></small></h1>
</div>
<%@include file="/WEB-INF/includes/breadcrumbs.jsp"%>
<div class="row">
    <div class="span3 well">
        <h3><%=corporation.getName()%></h3>

        <address>
            <%=corporation.getAddress1()%><br>
            <%=(Util.notNull(corporation.getAddress2()).length() > 0) ? corporation.getAddress2() + "<br>" : ""%>
            <%=corporation.getSuburb()%> <%=corporation.getState()%> <%=corporation.getPostcode()%><br>
            <%=corporation.getCountryName()%>
        </address>
        <p>
            <b>Subscription Type:</b> <a href="inline/subscription.jsp?id=<%=corporation.getId()%>" class="fancybox"><%=(corporation.getSubType() != null) ? corporation.getSubType() : "trial"%></a>
        </p>
        <p>
            <b>Remaining SMS:</b> <%=corporation.getSmsCount()%> <a href="inline/sms_count.jsp?id=<%=corporation.getId()%>" class="fancybox btn btn-mini btn-primary">change</a>
        </p>
        <p>
            <a href="<%=corporation.getUrl()%>" class="btn btn-mini">Go to Practice Site</a>
<%-- 			<a style="cursor: pointer;" onclick="slugUrl('<%=corporation.getSlug()%>')" class="btn btn-mini">Go to Practice Site</a> --%>
            
        </p>
        <p>
            <a href="inline/lock.jsp?id=<%=corporation.getId()%>" class="btn btn-mini fancybox btn-danger"><%=(corporation.isLocked()) ? "Unlock" : "Lock"%> Practice</a>
        </p>
    </div>
    <div class="span2">
        <h3>Statistics</h3>
        <table class="table">
            <thead>
            <tr>
                <th>Item</th>
                <th>Count</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Staff Members</td>
                <td><%=users%></td>
            </tr>
            <tr>
                <td>Patients</td>
                <td><%=patients%></td>
            </tr>
            <tr>
                <td>Programs</td>
                <td><%=programs%></td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="span2">
        <h3>SMS Usage</h3>
        <table class="table">
            <thead>
            <tr>
                <th>Month</th>
                <th>Count</th>
            </tr>
            </thead>
            <tbody>
            <%
                for (SMSStat sm : sms) {
                    %>
            <tr>
                <td><%=sm.getMonth()%></td>
                <td><%=sm.getCount()%></td>
            </tr>
            <%
                }
            %>
            </tbody>
        </table>
    </div>

</div>
</body>
</html>