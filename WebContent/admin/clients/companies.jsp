<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.Company" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="java.util.List" %>
<%@ page import="au.com.ourbodycorp.model.User" %>
<%
    LibraryManager lm = new LibraryManager();
	User u = (User) session.getAttribute("user");

    String q = request.getParameter("q");
    boolean search = false;
    List<Company> companies;
    String type="Clients";
    String activity_page="admin/clients/companies.jsp";
    String corp_id="---";
	String corp_name="---";
	String corp_loc="---";
	String user_id=String.valueOf(u.getId());
	String user_name="Fiizio Admin";
	String invitor_id="---";
	String invitor_name="---";
	String flag="search";
	String from="---";
	String to="---";
    if (q != null && q.length() > 0) {
    	 try{
    		//Util.allActivitiesLogs("Clients", u.getId(), "Fiizio System Admin", "Search Clients Details");
			Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,"Search Clients Details By Fiizio System Admin:"+u.getFullName());
    	}catch(Exception ee){
    		
    	} 
        search = true;
        companies = lm.searchCompanies(q);
    } else {
    	try{
    		//Util.allActivitiesLogs("Clients", u.getId(), "Fiizio System Admin", "View Clients Details");
			Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,"View Clients Details By Fiizio System Admin:"+u.getFullName());
    	}catch(Exception ee){
    		
    	} 
        companies = lm.listCompanies(0, 0);
    }

%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head><title>Companies - Clients</title>
<link rel="stylesheet" href="/css/jquery.dataTables.min.css" /> 

<script src="/js/jquery.dataTables.min.js"></script> 
 
<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
</head>

<body>
<div class="page-header">
    <h1>Clients</h1>
</div>

<div class="row">
    <div class="span6"><h2><%=(search) ? "Search Results" : "Alphabetical Listing"%></h2></div>
    <%-- <div class="span3">

        <form class="form-search" action="companies.jsp" method="get">
            <input type="text" name="q" value="<%=Util.formValue(q)%>" class="input-medium search-query">
            <button type="submit" class="btn">Search</button>
        </form>

    </div> --%>
</div>
<div class="row">
    <div class="span9">
        <table id="example" class="table table-zebra-stripes">
            <thead>
            <tr>
                <th>Name</th>
                <th>Contact</th>
                <th>Phone</th>
                <th style="text-align: center">State</th>
                <th style="text-align: center">Practices</th>
            </tr>
            </thead>
            <tbody>
            <%
                for (Company company : companies) {
            %>
            <tr>
                <td class="span3"><a href="company.jsp?id=<%=company.getId()%>"><%=company.getName()%></a></td>
                <td class="span3"><a href="mailto:<%=company.getEmail()%>"><%=company.getContact()%></a></td>
                <td class="span1"><%=Util.notNull(company.getPhone(), "&nbsp;")%></td>
                <td class="span1" style="text-align: center"><%=company.getState()%></td>
                <td class="span1" style="text-align: center"><%=company.getCorpCount()%></td>
            </tr>
            <%
                }
            %>
            </tbody>
        </table>
    </div>
</div>

</body>
</html>