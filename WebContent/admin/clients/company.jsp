<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.Company" %>
<%@ page import="au.com.ourbodycorp.model.Corporation" %>
<%@ page import="au.com.ourbodycorp.model.User" %>
<%@ page import="au.com.ourbodycorp.model.managers.CorporationManager" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="au.com.ourbodycorp.model.managers.UserManager" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    CorporationManager cm = new CorporationManager();
    LibraryManager lm = new LibraryManager();

    Company company = lm.getCompany(Long.parseLong(request.getParameter("id")));
    List<Corporation> corporations = cm.getCorporationsForCompany(company.getId());

    String[][] breadcrumbs = {{"/admin/clients/", "Clients"},  {"" + company.getId(), company.getName()}};
    SimpleDateFormat sdf = new SimpleDateFormat("d MMM yyyy");

    UserManager um = new UserManager();
    List<User> owners = um.getUsersForCompany(company.getId());

%>
<html>
<head><title><%=company.getName()%></title></head>
<body>
<div class="page-header">
    <h1>Company <small><%=company.getName()%></small></h1>
</div>
<%@include file="/WEB-INF/includes/breadcrumbs.jsp"%>
<div class="row">
    <div class="span3">
        <div class="well">
            <h3><%=company.getName()%></h3>
            <p>
                <b><a href="mailto:<%=company.getEmail()%>"><%=company.getContact()%></a></b><br>
                <%=(Util.notNull(company.getPhone()).length() > 0) ? company.getPhone() + "<br>" : "" %>
            </p>

            <address>
                <%=company.getAddress1()%><br>
                <%=(Util.notNull(company.getAddress2()).length() > 0) ? company.getAddress2() + "<br>" : ""%>
                <%=Util.notNull(company.getSuburb())%> <%=Util.notNull(company.getState())%> <%=Util.notNull(company.getPostcode())%>
            </address>
            <p>
                <b>Maximum Practices:</b> <%=(company.getMaxCorp() > 0) ? company.getMaxCorp() : "unlimited"%> <a href="inline/max_practices.jsp?id=<%=company.getId()%>" class="fancybox btn btn-mini btn-primary">change</a>
            </p>
        </div>
        <div class="margin-top:25px;">
            <h3>Company Owners</h3>
            <table class="table">
                <tbody>
                <%
                for (User owner : owners) {
            %>
                <tr>
                    <td><a href="mailto:<%=owner.getEmail()%>"><%=owner.getFullName()%></a></td>
                    <td><a href="inline/delete_owner.jsp?id=<%=company.getId()%>&user=<%=owner.getId()%>" class="fancybox"><i class="icon-trash"></i></a></td>
                </tr>
            <%
                }
            %>
                </tbody>
            </table>
            <a href="inline/add_owner.jsp?id=<%=company.getId()%>" class="btn btn-primary btn-mini fancybox">Add Owner</a>
        </div>
    </div>
    <div class="span6">
        <table class="table table-zebra-striped">
            <thead>
            <tr>
                <th>Name</th>
                <th>Location</th>
                <th>Max Patients</th>
                <th>Expires</th>
            </tr>
            </thead>
            <tbody>
            <%
                for (Corporation c : corporations) {
            %>
            <tr>
                <td class="span3"><% if (c.isLocked()) { %><i class="icon-exclamation-sign"></i> <% } %><a href="corporation.jsp?id=<%=c.getId()%>"><%=c.getName()%></a><%=(c.isTrial()) ? " (trial)" : ""%></td>
                <td class=""><%=c.getSuburb()%>, <%=Util.notNull(c.getState()).length() > 0 ? c.getState() + "," : ""%> <%=c.getCountry()%></td>
                <td class="" style="text-align: center"><%=(c.getMaxPatients() > 0) ? c.getMaxPatients() : "unlimited"%></td>
                <td class=""><%=(c.getExpiry() != null) ? sdf.format(c.getExpiry()) : ""%></td>
            </tr>
            <%
                }
            %>
            </tbody>
        </table>
    </div>
</div>
</body>
</html>