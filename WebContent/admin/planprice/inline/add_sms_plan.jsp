<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.PlanPrice" %>
<%@ page import="au.com.ourbodycorp.model.User" %>
<%@ page import="java.math.*" %>
<%@ page import="java.util.Date" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="java.lang.ref.ReferenceQueue" %>
<%@ page import="au.com.ourbodycorp.Config" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%

	User u = (User) session.getAttribute("user");

// 	long planid = Long.parseLong(Util.notNull(request.getParameter("id"), "0"));
	
    String plan_name = Util.notNull(request.getParameter("plan_name"));
	String cents_per_sms = Util.notNull(request.getParameter("cents_per_sms"));
    String currency_type = Util.notNull(request.getParameter("currency_type"));
    BigDecimal tax = new BigDecimal("0.00");
    BigDecimal price = new BigDecimal("0.00");
    BigDecimal tax1 = new BigDecimal("0.00");
    BigDecimal price1 = new BigDecimal("0.00");
    if(plan_name != ""){
    	if(request.getParameter("tax") != ""){
	    	tax = new BigDecimal (request.getParameter("tax"));
	    	tax1 = new BigDecimal("0.0000");
    	}else{
    		tax1 = new BigDecimal("0.00");
    	}
    	if(request.getParameter("price") != ""){
	    	price = new BigDecimal (request.getParameter("price"));
	    	price1 = new BigDecimal("0.0000");
    	}else{
    		price1 = new BigDecimal("0.00");
    	}
    }

    String title = "Add";
    String error = null;

    if (request.getMethod().equalsIgnoreCase("post")) {
        if (request.getParameter("submit").equals("Cancel")) {
        	if(Config.getString("https").equals("true")){
        		response.sendRedirect(Config.getString("url.secure")+"/close.jsp");
        	}else{
				response.sendRedirect(Config.getString("url")+"/close.jsp");
			}
            return;
        }
        if (plan_name.trim().length() == 0) {
        	error = "Please enter a plan name";
        } else if(cents_per_sms.trim().length() == 0){
        	error = "Please enter a cents per sms";
        } else if(tax.equals(tax1)){
        	error = "Please enter a tax";
        } else if(price.equals(price1)){
        	error = "Please enter a price";
        }
        
        PlanPrice pp;
        LibraryManager lm = new LibraryManager();
        String code = null;
        if (error == null) {
        	do {
     	        code = Util.randomNumeric(6);
     	        pp = lm.getInvoiceNumber(code);
     	    }
     	    while (pp != null);
        	pp = new PlanPrice();
        	pp.setPlan_name(plan_name);
        	pp.setCents_per_sms(Long.parseLong(cents_per_sms));
        	pp.setCurrency_type(currency_type);
        	pp.setTax(tax);
        	pp.setPrice(price);
        	pp.setType("SMS");
        	pp.setInvoice_number("SMS "+code);
        	pp.setCreator(u.getId());
        	pp.setCreated(new Date());
        	lm.savePlanPriceDetails(pp);
        	String type="Plan & Price";
        	String activity_page="admin/planprice/inline/add_sms_plan.jsp";
        	String corp_id="---";
        	String corp_name="---";
        	String corp_loc="---";
        	String user_id=String.valueOf(u.getId());
        	String user_name="Fiizio Admin";
        	String invitor_id=String.valueOf(pp.getId());
        	String invitor_name="---";
        	String flag="add";
        	String from="---";
        	String to="---";
        	try{
        		//Util.allActivitiesLogs("Plan &amp; Price", u.getId(), "Fiizio System Admin", "Add New SMS Plan");
            	Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,"Add New SMS Plan By Fiizio System Admin:"+u.getFullName());
        	}catch(Exception ee){
        		
        	}
        	if(Config.getString("https").equals("true")){
        		response.sendRedirect(Config.getString("url.secure")+"/close.jsp?reload=true");
        	}else{
        		response.sendRedirect(Config.getString("url")+"/close.jsp?reload=true");
        	}
            return;
        }

    } 
    String currencyTypeChoices[] = {"AUD", "USD", "Euro", "Pounds"};
    String currencyTypeChoicesVal[] = {"AUD", "USD", "Euro", "Pounds"};

    String[] sources = {"EFT", "Cheque", "PayPal", "Cash"};
%>
<html>
<head>
    <title><%=title%> Plan</title>
    <script type="text/javascript">
    function isNumeric(evt) {
    	evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    };
    function isNumericWithDot(evt) {
    	var theEvent = evt || window.event;
        var key = theEvent.keyCode || theEvent.which;
        key = String.fromCharCode(key);
        var regex = /^[0-9.,\b]+$/;
        if (!regex.test(key)) {
            theEvent.returnValue = false;
            if (theEvent.preventDefault) theEvent.preventDefault();
        };
    };
    </script>
    <style type="text/css">
    .input-mini{
    	width: 78px;
    }
    </style>
</head>
<body>
<%@include file="/WEB-INF/includes/formErrorSimple.jsp"%>
<div class="page-header">
    <h1><%=title%> Plan</h1>
</div>
<div class="row">
    <div class="span6">
        <form action="add_sms_plan.jsp" method="post" class="form-horizontal">
            <%=HTML.textInput("plan_name", "Plan Name", "50", "50", plan_name)%>
            <%=HTML.textInput("cents_per_sms", "Cents Per SMS", "10", "10", cents_per_sms)%>
            <%=HTML.selectMini("currency_type", "Currency Type", currencyTypeChoicesVal, currencyTypeChoices, currency_type, null, null, false)%>         
            <%=HTML.textInputNumericBigDec("tax", "Tax", "13", "13", tax, null, false, "return isNumericWithDot(event);")%>
            <%=HTML.textInputNumericBigDec("price", "Price", "13", "13", price, null, false, "return isNumericWithDot(event);")%>
            <%=HTML.saveCancel()%>
        </form>
    </div>
</div>
</body>
</html>