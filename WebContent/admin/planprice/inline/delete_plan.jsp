<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="java.util.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="au.com.ourbodycorp.Config" %>
<%@ page import="au.com.ourbodycorp.model.PlanPrice" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="java.util.Date" %>
<%@ page import="au.com.ourbodycorp.model.User" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
	
	long planid = Long.parseLong(Util.notNull(request.getParameter("id"), "0"));
User u = (User) session.getAttribute("user");

	if (request.getMethod().equalsIgnoreCase("post")) {
	    if (request.getParameter("submit").startsWith("Cancel")) {
	    	if(Config.getString("https").equals("true")){
        		response.sendRedirect(Config.getString("url.secure")+"/close.jsp");
        	}else{
				response.sendRedirect(Config.getString("url")+"/close.jsp");
			}
	        return;
	    }
	    
	LibraryManager lm = new LibraryManager();
	PlanPrice pp = null;
	pp = lm.getPlanPrice(planid);
	String type="Plan & Price";
	String activity_page="admin/planprice/inline/delete_plan.jsp";
	String corp_id="---";
	String corp_name="---";
	String corp_loc="---";
	String user_id=String.valueOf(u.getId());
	String user_name="Fiizio Admin";
	String invitor_id=String.valueOf(pp.getId());
	String invitor_name="---";
	String flag="delete";
	String from="---";
	String to="---";
    try{
    	//Util.allActivitiesLogs("Plan &amp; Price", u.getId(), "Fiizio System Admin", "Delete Plan");
    	Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,"Delete Plan By Fiizio System Admin:"+u.getFullName());
    }catch(Exception ee){
    	
    }  

	
	
    Connection conn = Util.getConnection();
	PreparedStatement pst;
    pst = conn.prepareStatement("delete from plan_price where id = ?");
    pst.setLong(1, planid);
    pst.executeUpdate();
    pst.close();
	conn.close();
	
	pp.setPlan_name(pp.getPlan_name());
	pp.setMax_users(pp.getMax_users());
	pp.setMax_locations(pp.getMax_locations());
	pp.setPractice_logo(pp.isPractice_logo());
	pp.setPush_notification(pp.isPush_notification());
	pp.setCurrency_type(pp.getCurrency_type());
	pp.setTax(pp.getTax());
	pp.setPrice(pp.getPrice());
	pp.setType(pp.getType());
	pp.setInvoice_number(pp.getInvoice_number());
	pp.setCreator(pp.getCreator());
	pp.setCreated(new Date());
	lm.savePlanPriceHistoryDetails(pp);
	
	if(Config.getString("https").equals("true")){
		response.sendRedirect(Config.getString("url.secure")+"/close.jsp?reload=true");
	}else{
		response.sendRedirect(Config.getString("url")+"/close.jsp?reload=true");
	}
	return;
	}
	
%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head><title>Delete Plan</title>
</head>
<body>
<div class="page-header">
    <h1>Delete Plan</h1>
</div>
<div class="row">
    <div class="span6">
        <p>
            Delete Plan. Are you sure?
        </p>
        <form action="delete_plan.jsp" method="post">
        	<input type="hidden" name="id" value="<%=planid%>">
            <%=HTML.confirmCancel()%>
        </form>
    </div>
</div>
</body>
</html>
