<%@page import="au.com.ourbodycorp.Config"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="au.com.ourbodycorp.model.User" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%
User u = (User) session.getAttribute("user");
String type="Plan & Price";
String activity_page="admin/planprice/index.jsp";
String corp_id="";
String corp_name="---";
String corp_loc="---";
String user_id=String.valueOf(u.getId());
String user_name=u.getFullName();
String invitor_id="";
String invitor_name="---";
String flag="view";
String from="---";
String to="---";
try{
	//Util.allActivitiesLogs("Plan &amp; Price", u.getId(), "Fiizio System Admin", "View Plan &amp; Price");
	Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,"View Plan Price By Fiizio System Admin");
}catch(Exception ee){
	
} 
%>
<html>
<head><title>Admin Home</title></head>
<body>
<div class="page-header">
    <h1>Fiizio System Admin</h1>
</div>
<div class="row">
    <div class="span2 well">
        <h3>Upgrade Plans</h3>
        <a href="<%=Config.getUrl()%>/admin/planprice/pricing.jsp">Upgrade Plan Details</a>
    </div>
    <div class="span2 well">
        <h3>SMS Plans</h3>
        <a href="<%=Config.getUrl()%>/admin/planprice/pricing_sms.jsp">SMS Plan Details</a>
    </div>
</div>
</body>
</html>