<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%@ page import="au.com.ourbodycorp.model.managers.CorporationManager" %>
<%@ page import="java.util.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="au.com.ourbodycorp.model.managers.CountryManager" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
User u = (User) session.getAttribute("user");
String type="Plan & Price";
String activity_page="admin/planprice/pricing_sms.jsp";
String corp_id="---";
String corp_name="---";
String corp_loc="---";
String user_id=String.valueOf(u.getId());
String user_name="Fiizio Admin";
String invitor_id="";
String invitor_name="---";
String flag="view";
String from="---";
String to="---";
try{
	//Util.allActivitiesLogs("Plan &amp; Price", u.getId(), "Fiizio System Admin", "View SMS Plan &amp; Price Details");
	Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,"View SMS Plan and Price Details By Fiizio System Admin:"+u.getFullName());
}catch(Exception ee){
	
}
	String[][] breadcrumbs = {{"/admin/", "Admin"}, {"/admin/planprice/", "Plan&Price"}, {null, "Plan Details"}};

%>
<html>
<head><title>Plan Details</title>
<style type="text/css">
.info-data {
    padding: 0 0 25px;
}
/* .info-data table { */
/*     width: 772px; */
/* } */
.info-data table form {
    padding: 15px 0 0;
}
.info-data td {
    padding: 0 5px;
    text-align: center;
    vertical-align: middle;
}
.info-data td.col01 {
    width: 152px;
}
.info-data td.col02 {
    width: 172px;
}
.info-data td.col03 {
    width: 146px;
}
.info-data td.col04 {
    width: 147px;
}
.info-data .row01 td {
    color: #fff;
    font-size: 20px;
    font-weight: bold;
    height: 90px;
    line-height: 25px;
}
.info-data .row01 .col01 {
    background: #db9700 none repeat scroll 0 0;
    color: #000;
    font-size: 33px;
    font-weight: bold;
    line-height: 36px;
}
.info-data .row01 .col02 {
    background: #db9100 none repeat scroll 0 0;
    border-bottom-color: #000;
    border-right-color: #000;
    border-top-color: #000;
}
.info-data .row01 .col03 {
    background: #db8700 none repeat scroll 0 0;
    border-bottom-color: #000;
    border-right-color: #000;
    border-top-color: #000;
}
.info-data .row01 .col04 {
    background: #db7a00 none repeat scroll 0 0;
    border-bottom-color: #000;
    border-right-color: #000;
    border-top-color: #000;
}
.info-data .row01 .col05 {
    background: #db7200 none repeat scroll 0 0;
    border-bottom-color: #000;
    border-right-color: #000;
    border-top-color: #000;
}
.info-data .row01 .col06 {
    background: #db6500 none repeat scroll 0 0;
    border-bottom-color: #000;
    border-right-color: #000;
    border-top-color: #000;
}
.info-data .row02 td {
/*     background: #3A3A3A none repeat scroll 0 0; */
    font-size: 16px;
    height: 88px;
    line-height: 19px;
}
.info-data .row02 td.nobg {
    background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
}
.info-data .row02 td.col01 {
    font-weight: bold;
}
.info-data .row03 td {
    color: #000;
    font-size: 24px;
    font-weight: bold;
    height: 45px;
    line-height: 29px;
    padding: 5px 0;
}
.info-data .row03 .col01 {
    background: #db9700 none repeat scroll 0 0;
    font-size: 16px;
    line-height: 16px;
}
.tick {
    font-size: 30px;
}
.info-data .row03 .col02 {
    background: #db9100 none repeat scroll 0 0;
    color: #000;
}
.info-data .row03 .col03 {
    background: #db8700 none repeat scroll 0 0;
}
.info-data .row03 .col04 {
    background: #db7a00 none repeat scroll 0 0;
}
.info-data .row03 .col05 {
    background: #db7200 none repeat scroll 0 0;
}
.info-data .row03 .col06 {
    background: #db6500 none repeat scroll 0 0;
}
.btn-click, #content .btn-click, input[type="submit"], input[type="reset"], input[type="button"], button, .txt-area .entry a.btn-click {
    background: #db6e00 none repeat scroll 0 0;
    border: 1px solid #000;
    border-radius: 5px;
    color: #fff;
    display: inline-block;
    font-size: 17px;
    height: 40px;
    line-height: 40px;
    padding: 0 24px;
    position: relative;
    text-decoration: none;
    vertical-align: top;
    text-shadow: 0 0 0 rgba(255, 255, 255, 0.75) !important;
    margin: 15px 0;
}
.btn-click:hover, .btn-login:hover, input[type="submit"]:hover, input[type="reset"]:hover, input[type="button"]:hover, button:hover, #content .btn-click:hover, .txt-area .entry a.btn-click:hover {
    background: #ed9944 none repeat scroll 0 0;
    text-decoration: none;
    color: #fff;
    text-shadow: 0 0 0 rgba(255, 255, 255, 0.75) !important;
}
</style>
</head>
<body>


  <div class="page-header">
    <h1>Plan Details</h1>
  </div>
  <%@include file="/WEB-INF/includes/breadcrumbs.jsp"%>
    <fieldset>
  		<a style="margin: 0px 0px 10px;" href="inline/add_sms_plan.jsp" class="btn btn-primary fancybox"><i class="icon-plus icon-white"></i> Add Plan</a>
		</fieldset>
  <div class="row">
<div class="span9" style="padding: 10px;">
  <form class="form-horizontal" method="post">
  <fieldset>
		<div class="info-data">
<table border="1" align="center">
<tbody>
<tr class="row01">
<td class="col01">Plans</td>
 <%
	            int i = 2;
              	Connection conn = Util.getConnection();
	      		PreparedStatement pst;
	            pst = conn.prepareStatement("select plan_name from plan_price where type = ? order by price");
	            pst.setString(1, "SMS");
	            ResultSet rs = pst.executeQuery();
	            while(rs.next()){ %>
              		<td class="col0<%=i%>"><%=rs.getString("plan_name")%></td>
              	<%i = i + 1;} %>
</tr>
<tr class="row02">
<td class="col01">Cents per sms</td>
<%	
 				i = 2;
	            pst = conn.prepareStatement("select cents_per_sms from plan_price where type = ? order by price");
	            pst.setString(1, "SMS");
	            ResultSet rs1 = pst.executeQuery();
	            while(rs1.next()){ %>
              		<td class="col0<%=i%>">
              			<%=rs1.getLong("cents_per_sms")%> cents per sms
              		</td>
              	<%i = i + 1;} %>
</tr>
<tr class="row02">
<td class="col01">Price</td>
<%	
 				i = 2;
	            pst = conn.prepareStatement("select currency_type,price from plan_price where type = ? order by price");
	            pst.setString(1, "SMS");
	            ResultSet rs2 = pst.executeQuery();
	            while(rs2.next()){ %>
              		<td class="col0<%=i%>">
              		<%if(rs2.getString("currency_type").equals("AUD")){%>
              			$<%=rs2.getBigDecimal("price")%>
              		<%}else if(rs2.getString("currency_type").equals("USD")){%>
              			$<%=rs2.getBigDecimal("price")%>
              		<%}else if(rs2.getString("currency_type").equals("Euro")){%>
              			$<%=rs2.getBigDecimal("price")%>
              		<%}else if(rs2.getString("currency_type").equals("Pounds")){%>
              			$<%=rs2.getBigDecimal("price")%>
              		<%}%>
              		</td>
              	<%i = i + 1;} %>
</tr>
<tr>
<td></td>
<%	
	            pst = conn.prepareStatement("select id from plan_price where type = ? order by price");
	            pst.setString(1, "SMS");
	            ResultSet rs3 = pst.executeQuery();
	            while(rs3.next()){ %>
              		<td>
              		<p style="margin: 20px 0;">
              		<a class="btn btn-primary btn-mini fancybox" href="inline/edit_sms_plan.jsp?id=<%=rs3.getString("id")%>" style="padding: 7px 10px; font-size: 13px; margin: 10px 0;">
						<i class="icon icon-edit icon-white"></i>
						Edit
					</a>
					<a class="btn btn-danger btn-mini fancybox" href="inline/delete_plan.jsp?id=<%=rs3.getString("id")%>" style="padding: 7px 10px; font-size: 13px;">
						<i class="icon icon-trash icon-white"></i>
						Delete
					</a>
					</p>
              		</td>
              	<%}
	            pst.close();
              	conn.close();
              	%>
</tr>
</tbody>
</table>
</div>
  </fieldset>
  </form>
  </div>
  </div>
</body>
</html>