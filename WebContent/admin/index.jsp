<%@page import="au.com.ourbodycorp.Config"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="au.com.ourbodycorp.model.User" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%
User u = (User) session.getAttribute("user");
String activity_page="admin/index.jsp";
String corp_id="---";
String corp_name="---";
String corp_loc="---";
String user_id=String.valueOf(u.getId());
String user_name="Fiizio Admin";
String invitor_id="---";
String invitor_name="---";
String flag="view";
String from="---";
String to="---";
 try{
	//Util.allActivitiesLogs("Admin", u.getId(), "Fiizio System Admin", "View Client Reports");
	Util.allActivitiesLogs2("Admin",activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,"View Client Reports By Fiizio System Admin:"+u.getFullName());
}catch(Exception ee){

} 
%>
<html>
<head><title>Admin Home</title></head>
<body>
<div class="page-header">
    <h1>Fiizio System Admin</h1>
</div>
<div class="row">
    <div class="span2 well">
        <h3>Export</h3>
        <a href="/admin/export/practice_details.csv">Practice Details</a><br />
        <a href="/admin/export/practice_contacts.csv">Practice Contact Information</a>
    </div>
    <div class="span2 well">
        <h3>Reports</h3>
        <a href="<%=Config.getUrl()%>/admin/expiry/expiry.jsp">Practices Report</a>
    </div>
</div>
</body>
</html>