<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.Exercise" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="java.net.URLEncoder" %>
<%@ page import="au.com.ourbodycorp.model.User" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    long id = Long.parseLong(request.getParameter("id"));
    boolean confirm = (request.getParameter("confirm") != null && Boolean.parseBoolean(request.getParameter("confirm")));
    String submit = Util.notNull(request.getParameter("submit"));
    LibraryManager lm = new LibraryManager();
    Exercise e = lm.getExercise(id);
    User u = (User) session.getAttribute("user");
    String type="Library";
    String activity_page="admin/library/inline/delete.jsp";
    String corp_id="---";
	String corp_name="---";
	String corp_loc="---";
	String user_id=String.valueOf(u.getId());
	String user_name="Fiizio Admin";
	String invitor_id=String.valueOf(e.getId());
	String invitor_name="---";
	String flag="delete";
	String from="---";
	String to="---";
	
    

    if (e == null) {
        response.sendError(404);
    }

    if (submit.equalsIgnoreCase("cancel")) {
        response.sendRedirect("/close.jsp");
        return;
    } else if (confirm) {
    	try{
    		//Util.allActivitiesLogs("Library", u.getId(), "Fiizio System Admin", "Delete Exercise");
    		Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,"Delete Exercise from Library By Fiizio System Admin:"+u.getFullName());

    	}catch(Exception ee){
    		
    	}
        lm.deleteExercise(id);
        response.sendRedirect("/close.jsp?redir=" + URLEncoder.encode("/admin/library/", "UTF-8"));
        return;
    }

%>
<html>
<head><title>Delete Exercise</title></head>
<body>
<div class="page-header">
    <h1>Delete Exercise <small><%=Util.textAreaValue(e.getName())%></small></h1>
</div>
<div class="row">
    <div class="span6">
        <p>Are you sure you want to delete this exercise? It will be removed from any program it is used in.</p>
        <form action="delete.jsp" method="POST">
            <input type="hidden" name="id" value="<%=id%>">
            <input type="hidden" name="confirm" value="true">
            <%=HTML.confirmCancel()%>
        </form>
    </div>
</div>
</body>
</html>