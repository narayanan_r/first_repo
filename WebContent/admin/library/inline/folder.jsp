<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.PreparedStatement" %>
<%@ page import="java.sql.Types" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    long parent = Long.parseLong(request.getParameter("parent"));
    String name = Util.notNull(request.getParameter("name"));

    String error = null;

    if (request.getMethod().equalsIgnoreCase("post")) {
        if (name.trim().length() == 0) {
            error = "Please enter a folder name";
        } else {
            Connection conn = Util.getConnection();
            try {
                PreparedStatement pst = conn.prepareStatement("insert into exercise_category (id, name, parent) values (nextval('exerciseCatId'),?,?)");
                pst.setString(1, name);
                if (parent > 0) {
                    pst.setLong(2, parent);
                } else {
                    pst.setNull(2, Types.BIGINT);
                }
                pst.execute();
                response.sendRedirect("/close.jsp?reload=true");
                return;
            } finally {
                conn.close();
            }
        }
    }


%>
<html>
<head><title>Add Folder</title></head>
<body>
<div class="page-header">
    <h1>Add Folder</h1>
</div>
<div class="row">
    <div class="span4">
        <%@include file="/WEB-INF/includes/formErrorSimple.jsp"%>
        <form action="folder.jsp" method="post">
            <input type="hidden" name="parent" value="<%=parent%>">
            <%=HTML.textInput("name", "New Folder Name", "30", "30", name)%>
            <%=HTML.submitButton("Create Folder")%>
        </form>
    </div>
</div>
</body>
</html>