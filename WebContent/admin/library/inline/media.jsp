<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.Exercise" %>
<%@ page import="au.com.ourbodycorp.model.ExerciseMedia" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.List" %>
<%@ page import="au.com.ourbodycorp.model.User" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    long id = Long.parseLong(request.getParameter("id"));
    LibraryManager lm = new LibraryManager();
    Exercise e = lm.getExercise(id);
    User u = (User) session.getAttribute("user");
    String type="Library";
    String activity_page="admin/library/inline/media.jsp";
    String corp_id="---";
	String corp_name="---";
	String corp_loc="---";
	String user_id=String.valueOf(u.getId());
	String user_name="Fiizio Admin";
	String invitor_id="";
	String invitor_name="---";
	String flag="view";
	String from="---";
	String to="---";
    try{
	 	//Util.allActivitiesLogs("Library", u.getId(), "Fiizio System Admin", "View Exercise Media");
		Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,"View Exercise Media from Library By Fiizio System Admin:"+u.getFullName());

	}catch(Exception ee){
		
	}  
    List<ExerciseMedia> em = lm.getExerciseMedia(id);
    HashMap<Long, Long> next = new HashMap<Long, Long>();
    long prev = 0;
    for (ExerciseMedia media : em) {
        next.put(prev, media.getId());
        prev = media.getId();
    }
%>
<html>
<head><title>Media</title></head>
<body>
<div class="page-header">
    <h1>Media <small><%=e.getName()%></small></h1>
</div>
<div class="row">
    <div class="span6">
        <a href="add_media.jsp?id=<%=id%>" class="btn btn-primary">Add Media</a>
    </div>
</div>
<div class="row" style="margin-top:15px;">
    <div class="span6">
        <table class="table table-zebra-striped">
            <%
                prev = 0;
                for (ExerciseMedia media : em) {
                    String name = media.getCaption();
                    if (name == null || name.length() == 0) {
                        name = media.getName();
                    }
                    if (name == null || name.length() == 0) {
                        name = media.getType();
                    }
                    String preview;
                    if (media.getType().equals("video")) {
                        preview = "/images/video_75_56.gif";
                    } else {
                        preview = "/media?id=" + media.getId() + "&width=75&v=" + media.getVersion();
                    }
            %>
            <tr>
                <td><%=media.getSeq()%></td>
                <%--
                <td>
                    <% if (media.getType().equals("photo")) {%><a href="/media?id=<%=media.getId()%>" target="_blank"><img src="/media?id=<%=media.getId()%>" width="75"></a><%
                } else {
                    %>
                    <a href="/media?id=<%=media.getId()%>" target="_blank">Download</a>
                    <%
                }
                %>
                </td>

                <td><a href="/media?id=<%=media.getId()%>" target="_blank"><i class="<%=(media.getType().equals("video")) ? "icon-film" : "icon-picture"%>"></i></a> <a href="delete_media.jsp?id=<%=media.getId()%>" ><i class="icon-trash"></i></a> <a href="add_media.jsp?media=<%=media.getId()%>&id=<%=id%>"><%=Util.textAreaValue(name)%></a><% if (media.isKey()) {%> <i class="icon-ok"></i><% } %></td>
                --%>
                <td style="width:85px;"><a href="view.jsp?id=<%=media.getId()%>"><img src="<%=preview%>" width="75"></a></td>
                <td><a href="delete_media.jsp?id=<%=media.getId()%>" ><i class="icon-trash"></i></a></td>
                    <td style="width:270px;"><a href="add_media.jsp?media=<%=media.getId()%>&id=<%=id%>"><%=Util.textAreaValue(name)%></a><% if (media.isKey()) {%> <i class="icon-ok"></i><% } %></td>
                <td>
                    <a href="swap.jsp?id=<%=id%>&id1=<%=media.getId()%>&id2=<%=prev%>"><i class="icon-arrow-up<%=(prev == 0) ? " icon-white" : ""%>"></i></a>
                    <% if (next.get(media.getId()) != null) {%>
                    <a href="swap.jsp?id=<%=id%>&id1=<%=media.getId()%>&id2=<%=next.get(media.getId())%>"><i class="icon-arrow-down"></i></a>
                    <% } %>

                </td>
                <td>

                </td>
            </tr>
            <%
                    prev = media.getId();
                }
            %>
        </table>
    </div>
</div>


</body>
</html>