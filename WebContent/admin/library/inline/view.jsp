<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.ExerciseMedia" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="au.com.ourbodycorp.model.User" %>
<%
    long id = Long.parseLong(request.getParameter("id"));
LibraryManager lm = new LibraryManager();
ExerciseMedia em = lm.getExerciseMedia(id, false);
User u = (User) session.getAttribute("user");
String type="Library";
String activity_page="admin/library/inline/view.jsp";
String corp_id="---";
String corp_name="---";
String corp_loc="---";
String user_id=String.valueOf(u.getId());
String user_name="Fiizio Admin";
String invitor_id="";
String invitor_name="---";
String flag="view";
String from="---";
String to="---";
try{
	//Util.allActivitiesLogs("Library", u.getId(), "Fiizio System Admin", "View Exercise Media Image");
	Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,"View Exercise Media Image from Library By Fiizio System Admin:"+u.getFullName());
}catch(Exception ee){
	
} 
    boolean edit = (request.getParameter("edit") != null) && Boolean.parseBoolean(request.getParameter("edit"));
    if (em == null) {
        response.sendError(404);
        return;
    }
%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head><title>Preview</title></head>
<body>
<div class="page-header">
    <h1>Preview</h1>
</div>
<div class="row">
    <div class="span6">
        <% if (em.getType().equals("photo")) {
        %>
        <a href="/media?id=<%=id%>" target="_blank"><img src="/media?id=<%=id%>&width=480&v=<%=em.getVersion()%>"></a>
        <%
        } else {
        %>
        <video width="480" height=320 controls>
            <source src="/media?id=<%=id%>" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"'>
        </video>
        <%
            }
        %>
    </div>
</div>
<div class="row">
    <div class="span6">
        <p><%=Util.textAreaValue(em.getCaption())%></p>
    </div>
</div>
<div class="row">
    <div class="span6">
        <div class="form-actions">
            <% if (edit) {
            %>
            <a href="add_media.jsp?id=<%=em.getExercise()%>&media=<%=em.getId()%>&close=true" class="btn">Edit</a>
            <a href="/close.jsp" class="btn btn-primary">Close</a>
            <%
            } else {
            %>
            <a href="media.jsp?id=<%=em.getExercise()%>" class="btn btn-primary">Back</a>
            <%
            }%>

          </div>
    </div>
</div>
</body>
</html>