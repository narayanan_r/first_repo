<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.Exercise" %>
<%@ page import="au.com.ourbodycorp.model.ExerciseMedia" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="au.com.ourbodycorp.model.User" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    long id = Long.parseLong(request.getParameter("id"));
    boolean confirm = (request.getParameter("confirm") != null && Boolean.parseBoolean(request.getParameter("confirm")));
    String submit = Util.notNull(request.getParameter("submit"));
    LibraryManager lm = new LibraryManager();
    ExerciseMedia em = lm.getExerciseMedia(id, false);
    Exercise e = lm.getExercise(em.getExercise());
    User u = (User) session.getAttribute("user");
    String type="Library";
    String activity_page="admin/library/inline/delete_media.jsp";
    String corp_id="---";
	String corp_name="---";
	String corp_loc="---";
	String user_id=String.valueOf(u.getId());
	String user_name="Fiizio Admin";
	String invitor_id=String.valueOf(e.getId());
	String invitor_name="---";
	String flag="delete";
	String from="---";
	String to="---";
    	
    if (submit.equalsIgnoreCase("cancel")) {
        response.sendRedirect("media.jsp?id=" + e.getId());
        return;
    } else if (confirm) {
    	try{
    		//Util.allActivitiesLogs("Library", u.getId(), "Fiizio System Admin", "Delete Exercise Media");
    		Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,"Delete Exercise Media from Library By Fiizio System Admin:"+u.getFullName());

    	}catch(Exception ee){
    		
    	}
        lm.deleteExerciseMedia(em.getId());
        response.sendRedirect("media.jsp?id=" + e.getId());
        return;
    }

    String url = "/images/movie_150_100.gif";
    if (em.getType().equalsIgnoreCase("photo")) {
        url = "/media?id=" + em.getId() + "&width=150";
    }
%>
<html>
<head><title>Delete Media</title></head>
<body>
<div class="page-header">
    <h1>Delete Media <small><%=Util.textAreaValue(e.getName())%></small></h1>
</div>
<div class="row">
    <div class="span6">
        <h2>Confirm delete of media</h2>
        <img src="<%=url%>" width="150">
        <form action="delete_media.jsp" method="POST">
            <input type="hidden" name="id" value="<%=id%>">
            <input type="hidden" name="confirm" value="true">
            <%=HTML.confirmCancel()%>
        </form>
    </div>
</div>
</body>
</html>