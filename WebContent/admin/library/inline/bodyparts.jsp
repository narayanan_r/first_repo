<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.Exercise" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.PreparedStatement" %>
<%@ page import="au.com.ourbodycorp.Config" %>
<%@ page import="au.com.ourbodycorp.model.User" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    long id = Long.parseLong(request.getParameter("id"));
    LibraryManager lm = new LibraryManager();
    Exercise e = lm.getExercise(id);
    User u = (User) session.getAttribute("user");
    
    String error = null;
    if (request.getMethod().equalsIgnoreCase("post")) {
    	String activity_page="admin/library/inline/bodyparts.jsp";
    	String corp_id="---";
    	String corp_name="---";
    	String corp_loc="---";
    	String user_id=String.valueOf(u.getId());
    	String user_name="Fiizio Admin";
    	String invitor_id=String.valueOf(e.getId());
    	String invitor_name="---";
    	String flag="add";
    	String from="---";
    	String to="---";
        try{
    		//Util.allActivitiesLogs("Library", u.getId(), "Fiizio System Admin", "Add Exercise Types");
    		Util.allActivitiesLogs2("Library",activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,"Adding Exercise Types to Library By Fiizio System Admin:"+u.getFullName());
    	}catch(Exception ee){
    		
    	}
        if (request.getParameter("submit").equals("Cancel")) {
        	if(Config.getString("https").equals("true")){
        		response.sendRedirect("https://"+request.getHeader("host")+"/close.jsp");
        	}else{
				response.sendRedirect("http://"+request.getHeader("host")+"/close.jsp");
			}
            return;
        }

        if (request.getParameterValues("bp") == null || request.getParameterValues("type") == null) {
            error = "select at least 1 body port and 1 type";
        } else {

            Connection conn = Util.getConnection();
            try {
                conn.setAutoCommit(false);
                PreparedStatement pst = conn.prepareStatement("delete from bodyparts where exercise = ?");
                pst.setLong(1, e.getId());
                pst.executeUpdate();
                pst.close();

                pst = conn.prepareStatement("delete from exercise_types where exercise = ?");
                pst.setLong(1, e.getId());
                pst.executeUpdate();
                pst.close();

                pst = conn.prepareStatement("insert into bodyparts (exercise, bodypart) values (?,?)");
                for (String bp : request.getParameterValues("bp")) {
                    try {
                        Exercise.BodyPart b = Exercise.BodyPart.valueOf(bp);
                    } catch (IllegalArgumentException ex) {
                        continue;
                    }
                    pst.setLong(1, e.getId());
                    pst.setString(2, bp);
                    pst.execute();
                }
                pst.close();

                pst = conn.prepareStatement("insert into exercise_types (exercise, type) values (?,?)");
                for (String type : request.getParameterValues("type")) {
                    try {
                        Exercise.Type t = Exercise.Type.valueOf(type);
                    } catch (IllegalArgumentException ex) {
                        continue;
                    }
                    pst.setLong(1, e.getId());
                    pst.setString(2, type);
                    pst.execute();
                }
                pst.close();

                conn.commit();

                if(Config.getString("https").equals("true")){
                    response.sendRedirect("https://"+request.getHeader("host")+"/close.jsp?reload=true");
            	}else{
                    response.sendRedirect("http://"+request.getHeader("host")+"/close.jsp?reload=true");
            	}
            } finally {
                conn.setAutoCommit(true);
                conn.close();
            }
        }
    }

%>
<html>
<head><title>Select body parts and exercise types</title></head>
<body>
<div class="page-header"><h1>Body parts and exercise types</h1></div>
<%@include file="/WEB-INF/includes/formErrorSimple.jsp"%>

<div class="row">
    <form action="bodyparts.jsp" method="post">

    <input type="hidden" name="id" value="<%=id%>">

        <div class="span3">
            <h2>Body Parts</h2>
            <%
                for (Exercise.BodyPart bp : Exercise.BodyPart.values()) {
            %>
                <label class="checkbox">
                <input type="checkbox" name="bp" value="<%=bp.name()%>" <%=e.getBodyParts().contains(bp) ? "checked" : ""%>> <%=bp.getValue()%><br>
                </label>
            <%
                }
            %>
        </div>
        <div class="span3">
            <h2>Exercise Types</h2>
            <%
                for (Exercise.Type type : Exercise.Type.values()) {
            %>
                <label class="checkbox">
                <input type="checkbox" name="type" value="<%=type.name()%>" <%=e.getTypes().contains(type) ? "checked" : ""%>> <%=type.getValue()%><br>
                </label>
            <%
                }
            %>
        </div>
</div>
    <div class="row">
        <div class="span6">
            <%=HTML.saveCancel()%>
        </div>
           </div>
</form>

</body>
</html>