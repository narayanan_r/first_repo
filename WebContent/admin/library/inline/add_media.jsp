<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.Exercise" %>
<%@ page import="au.com.ourbodycorp.model.ExerciseMedia" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="org.apache.commons.fileupload.FileItem" %>
<%@ page import="org.apache.commons.fileupload.FileItemFactory" %>
<%@ page import="org.apache.commons.fileupload.disk.DiskFileItemFactory" %>
<%@ page import="org.apache.commons.fileupload.servlet.ServletFileUpload" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.List" %>
<%@ page import="au.com.ourbodycorp.Config" %>
<%@ page import="au.com.ourbodycorp.model.User" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    long id = Long.parseLong(request.getParameter("id"));
    long media = Long.parseLong(Util.notNull(request.getParameter("media"),"0"));
    boolean key = false;
    boolean close = (request.getParameter("close") != null) && Boolean.parseBoolean(request.getParameter("close"));

    LibraryManager lm = new LibraryManager();
    Exercise e = lm.getExercise(id);
    User u = (User) session.getAttribute("user");
    ExerciseMedia oem = null;
    String title = "Add";
    String type="Library";
    String activity_page="admin/library/inline/add_media.jsp";
    String corp_id="---";
	String corp_name="---";
	String corp_loc="---";
	String user_id=String.valueOf(u.getId());
	String user_name="Fiizio Admin";
	String invitor_id=String.valueOf(e.getId());
	String invitor_name="---";
	String flag="ExerciseMedia";
	String from="---";
	String to="---";
    if (media > 0) {
    	
        title = "Edit";
        oem = lm.getExerciseMedia(media, false);
    }else{
    	 try{
    		//Util.allActivitiesLogs("Library", u.getId(), "Fiizio System Admin", "Add Exercise Media");
    		Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,"Add Exercise Media By Fiizio System Admin");

    	}catch(Exception ee){
    		
    	} 
    }

    String name = (oem != null) ? oem.getName() : null;
    String caption = (oem != null) ? oem.getCaption() : null;
    HashMap<String, String> errorFields = new HashMap<String, String>();


    FileItem file = null;
    session.setAttribute("filenotselected", null);
    session.setAttribute("fileformaterror", null);
    
    if (request.getMethod().equalsIgnoreCase("post")) {
        HashMap<String, String> fields = new HashMap<String, String>();
        FileItemFactory factory = new DiskFileItemFactory();
        ServletFileUpload upload = new ServletFileUpload(factory);
        List<FileItem> items = upload.parseRequest(request);
        for (FileItem item : items) {
            if (item.isFormField()) {
                fields.put(item.getFieldName(), item.getString());
            } else {
                if (item.getName() != null && item.getName().trim().length() > 0)
                    file = item;
            }
        }

        key = fields.get("key") != null && Boolean.parseBoolean(fields.get("key"));

        if (fields.get("submit") != null && fields.get("submit").contains("Cancel")) {
        	if(Config.getString("https").equals("true")){
                response.sendRedirect("https://"+request.getHeader("host")+"/admin/library/inline/media.jsp?id=" + id);
        	}else{
                response.sendRedirect("http://"+request.getHeader("host")+"/admin/library/inline/media.jsp?id=" + id);
        	}
            return;
        }

        name = fields.get("name");
        caption = fields.get("caption");

        if (oem == null && (file == null || file.getSize() == 0)) {
            errorFields.put("file", "No file selected");
            session.setAttribute("filenotselected", "No file selected");
        } else if (file != null && !file.getName().endsWith(".m4v") && !file.getName().endsWith(".jpg")) {
            errorFields.put("file", "Only .m4v and .jpg files allowed");
            session.setAttribute("filenotselected", null);
            session.setAttribute("fileformaterror", "Only .m4v and .jpg files allowed");
        } else if (caption == null || caption.trim().length() == 0) {
            errorFields.put("caption", "Enter a caption");
            session.setAttribute("filenotselected", null);
            session.setAttribute("fileformaterror", null);
        }
        /*if (name == null || name.trim().length() == 0) {
            errorFields.put("name", "Enter a name");
        }*/

        if (errorFields.size() == 0) {
        	session.setAttribute("filenotselected", null);
            session.setAttribute("fileformaterror", null);
            ExerciseMedia em;
            if (oem == null) {
            	try{
            		//Util.allActivitiesLogs("Library", u.getId(), "Fiizio System Admin", "Add Exercise Media");
            		Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,"---","---","create","","","Add Exercise Media By Fiizio System Admin:"+u.getFullName());

            	}catch(Exception ee){
            		
            	}
                em = new ExerciseMedia();
                em.setSeq(lm.nextExerciseMediaSequence(id));
            } else {
            	try{
            		//Util.allActivitiesLogs("Library", u.getId(), "Fiizio System Admin", "Edit Exercise Media");
            		Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,"Edit Exercise Media By Fiizio System Admin:"+u.getFullName());
            	}catch(Exception ee){
            		
            	}
                em = oem;
            }
            em.setExercise(id);

            if (file != null) {
                em.setType((file.getName().endsWith(".m4v")) ? "video" : "photo");
                em.setData(file.get());
                em.setVersion(em.getVersion() + 1);
            }
            em.setCaption(caption);
            em.setName(name);

            lm.save(em);
            

            if (key) {
                lm.setKeyPhoto(id, em.getId());
            }

            if (close) {
            	if(Config.getString("https").equals("true")){
                    response.sendRedirect("https://"+request.getHeader("host")+"/admin/library/inline/view.jsp?edit=true&id=" + em.getId());
            	}else{
                    response.sendRedirect("http://"+request.getHeader("host")+"/admin/library/inline/view.jsp?edit=true&id=" + em.getId());
            	}
            } else {
            	if(Config.getString("https").equals("true")){
                    response.sendRedirect("https://"+request.getHeader("host")+"/admin/library/inline/media.jsp?id=" + id);
            	}else{
                    response.sendRedirect("http://"+request.getHeader("host")+"/admin/library/inline/media.jsp?id=" + id);
            	}
            }
            return;
        }

    } else {
        if (oem != null) {
            key = oem.isKey();
        }
    }

%>
<html>
<head><title><%=title%> Media</title></head>
<body>
<div class="page-header">
    <h1><%=title%> Media <small><%=e.getName()%></small></h1>
</div>
<div class="row">
    <div class="span6">
        <form action="add_media.jsp?id=<%=id%>&media=<%=media%>&close=<%=close%>" method="post" enctype="multipart/form-data" class="form-horizontal">
            <%if(session.getAttribute("filenotselected") != null){ %>
          <div class="control-group error">
			<label class="control-label" for="fileInput">File</label>
			<div class="controls">
			<input id="fileInput" class="input-file" type="file" name="file">
			<small>.jpg or .m4v only</small>
			<span class="help-inline">No file selected</span>
			</div>
		  </div>
          <%}else if(session.getAttribute("fileformaterror") != null){ %>
          <div class="control-group error">
			<label class="control-label" for="fileInput">File</label>
			<div class="controls">
			<input id="fileInput" class="input-file" type="file" name="file">
			<small>.jpg or .m4v only</small>
			<span class="help-inline">Only .m4v and .jpg files allowed</span>
			</div>
		 </div>
		 <%}else{ %>
		 <div class="control-group">
            <label class="control-label" for="fileInput">File</label>
            <div class="controls">
              <input class="input-file" name="file" id="fileInput" type="file">
                <small>.jpg or .m4v only</small>
            </div>
          </div>
		 <%} %>
            <%--=HTML.textInput("name", "Name", "30", "30", name, null, errorFields)--%>
            <%=HTML.textInput("caption", "Caption", "2000", "2000", caption, null, errorFields)%>
            <div class="control-group">
            <label class="control-label" for="optionsCheckbox">Key Photo</label>
            <div class="controls">
              <label class="checkbox">
                <input type="checkbox" id="optionsCheckbox" name="key" value="true" <%=(key) ? "checked" : ""%>>
                Set this to be the key photo
              </label>
            </div>
          </div>
            <%=HTML.saveCancel()%>
        </form>
    </div>
</div>
</body>
</html>