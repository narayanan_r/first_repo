<%@page import="org.zefer.html.doc.u"%>
<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.Exercise" %>
<%@ page import="au.com.ourbodycorp.model.ExerciseMedia" %>
<%@ page import="au.com.ourbodycorp.model.User" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.LinkedHashMap" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%

    long id = Long.parseLong(Util.notNull(request.getParameter("id"), "0"));
    String name = Util.notNull(request.getParameter("name"));
    String description = Util.notNull(request.getParameter("description"));
    String type = Util.notNull(request.getParameter("type"));
    boolean showSides = request.getParameter("showSides") != null && Boolean.parseBoolean(request.getParameter("showSides"));
    String repsperseconds = request.getParameter("repsperseconds") != null ? request.getParameter("repsperseconds") : "0";
	String sets = request.getParameter("sets") != null ? request.getParameter("sets") : "0";
	String rest = request.getParameter("rest") != null ? request.getParameter("rest") : "0";
	String repsvalue = request.getParameter("reps") != null ? request.getParameter("reps") : "0";
	String holdtime = request.getParameter("holdtime") != null ? request.getParameter("holdtime") : "0";
	String weight = request.getParameter("weight") != null ? request.getParameter("weight") : "0";
	String error = null;
	String sideChoices[];
	String side = Util.notNull(request.getParameter("side"));
	String scRep[] = {"Left", "Right", "Both", "Alternating"};
	String scTime[] = {"Left", "Right", "Both"};
	sideChoices = scRep;
    LibraryManager lm = new LibraryManager();
    Exercise e = null;
    String title = "Create Exercise";
    User u = (User) session.getAttribute("user");
    String activity_page="admin/library/exercise.jsp";
    String corp_id="---";
	String corp_name="---";
	String corp_loc="---";
	String user_id=String.valueOf(u.getId());
	String user_name="Fiizio Admin";
   
    if (id > 0) {
    	
        title = "Edit Exercise";
        e = lm.getExercise(id);
        if (e == null) {
            response.sendError(404, "Exercise not found");
            return;
        }
        if (request.getMethod().equalsIgnoreCase("post")) {
        String invitor_id=String.valueOf(e.getId());
        String invitor_name="---";
        String flag="update";
        String from="Name:"+e.getName()+";Description:"+e.getDescription()+";";
        String to="Name:"+request.getParameter("name")+";Description:"+request.getParameter("description")+";";
        try{
    		//Util.allActivitiesLogs("Library", u.getId(), "Fiizio System Admin", "Edit Exercise");
    		Util.allActivitiesLogs2("Library",activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,"Edit Exercise "+e.getName()+" By Fiizio System Admin:"+u.getFullName());
    	}catch(Exception ee){
    		
    	}
        }
    }/* else{
    	try{
    		Util.allActivitiesLogs("Library", u.getId(), "Fiizio System Admin", "Create Exercise");
    	}catch(Exception ee){
    		
    	}
    } */

    HashMap<String, String> errorFields = new HashMap<String, String>();


    if (request.getMethod().equalsIgnoreCase("get") && id > 0) {
        name = e.getName();
        description = e.getDescription();
        type = e.getType();
		side=e.getSide();
 		rest=Integer.toString(e.getRest());
 		System.out.println("rest at get time"+rest);
		repsperseconds=Integer.toString(e.getRepsperseconds());
		repsvalue=Integer.toString(e.getReps());
		sets=Integer.toString(e.getSets());
		holdtime=Integer.toString(e.getHolds());
		showSides = e.isShowSides();
		System.out.println("showSides"+e.getSide());
		sets = Integer.toString(e.getSets());
		weight=Integer.toString(e.getWeight());
    } else if (request.getMethod().equalsIgnoreCase("post")) {
        if (name.trim().length() == 0) {
            errorFields.put("name", "Please enter a name");
        }
		//if(e == null){
        if (errorFields.size() == 0) {
            if (e == null) {
                e = new Exercise();
                e.setCreator(u.getId());
            }
            e.setName(name);
            e.setType(type);
            e.setDescription(description);
            e.setShowSides(showSides);
            if(type.equals(Exercise.REP_EXERCISE)){
            	e.setReps(Integer.parseInt(repsvalue));
            	e.setHolds(Integer.parseInt(repsperseconds));
            }else{
            	e.setReps(0);
            	e.setHolds(0);
            }
            if(type.equals(Exercise.TIMED_EXERCISE)){
				e.setHolds(Integer.parseInt(holdtime));
            }
			e.setSets(Integer.parseInt(sets));
			e.setRest(Integer.parseInt(rest));
			e.setSide(Util.safeInput(side, true));
			e.setWeight(Integer.parseInt(weight));
            lm.save(e,0,"true");
            String invitor_id=String.valueOf(e.getId());
            String invitor_name="---";
            String flag="add";
            String from="---";
            String to="---";
            try{
        		//Util.allActivitiesLogs("Library", u.getId(), "Fiizio System Admin", "Create Exercise");
        		Util.allActivitiesLogs2("Library",activity_page,corp_id,corp_name,corp_loc,user_id,user_name,String.valueOf(e.getId()),invitor_name,"create","","","Create Exercise "+e.getName()+" By Fiizio System Admin:"+u.getFullName());
        	}catch(Exception ee){
        		
        	}
            if (request.getParameter("submit").contains("Close")) {
                String bodyPart = "";
                if (e.getBodyParts() != null && e.getBodyParts().size() > 0) {
                    bodyPart = e.getBodyParts().getFirst().name();
                }

                String type2 = "";
                if (e.getTypes() != null && e.getTypes().size() > 0) {
                    type2 = e.getTypes().getFirst().name();
                }

                response.sendRedirect("/admin/library/?bp=" + bodyPart + "&type=" + type2);
            } else {
                response.sendRedirect("admin_exercise.jsp?id=" + e.getId());
            }
            return;
        }
    }

    LinkedHashMap<String, String> bodyParts = new LinkedHashMap<String, String>();
    for (Exercise.BodyPart b : Exercise.BodyPart.values()) {
        bodyParts.put(b.name(), b.getValue());
    }
    LinkedHashMap<String, String> types = new LinkedHashMap<String, String>();
    for (Exercise.Type b : Exercise.Type.values()) {
        types.put(b.name(), b.getValue());
    }

    String[][] breadcrumbs = {{"/admin/", "Admin"}, {"/admin/library/", "Library"}, {"", title}};

%>
<html>
<head><title><%=title%></title>
<link rel="stylesheet" href="/css/rSlider.min.css" type='text/css'>
<style>
   body {
       font-family: Arial, Helvetica, sans-serif;
       margin: 0;
       padding: 0 0 50px;
       color: #333;
       font-size: 14px;
   }
   p {
       margin: 0;
   }
   .container {
       width: 80%;
       margin: 70px auto;
   }
   .slider-container {
       width: 100%;
       max-width: 800px;
       margin: 0 auto 50px;
   }
</style></head>
<body onload="setExcerciseType();">
<div class="page-header">
    <h1><%=title%></h1>
</div>
<%@include file="/WEB-INF/includes/breadcrumbs.jsp"%>
<form action="admin_exercise.jsp" method="post" class="form-horizontal">
<div class="row">
    <div class="span6">
        <%@include file="/WEB-INF/includes/formError.jsp"%>

            <input type="hidden" name="id" value="<%=id%>">


            <%=HTML.textInput("name","Name", "100", "100", name, null, errorFields)%>
            <%=HTML.textArea("description", "Description", description, 5)%>
        <div class="control-group">
            <label class="control-label" for="type">This exercise is</label>
            <div class="controls">
              <select id="type" name="type" onchange="setExcerciseType();">
                <option value="<%=Exercise.TIMED_EXERCISE%>" <%=(type.equals(Exercise.TIMED_EXERCISE)) ? "selected" : ""%>>Time based</option>
                  <option value="<%=Exercise.REP_EXERCISE%>" <%=(type.equals(Exercise.REP_EXERCISE)) ? "selected" : ""%>>Rep based</option>
              </select>
            </div>
          </div>
          <div class="control-group" >
						<label class="control-label" for="type">Weight</label>

						<div class="controls">
								<input
									type="range" 
									onchange="rangevalue.value=value" id="weight"
									name="weight" min="0" max="250"value="<%=weight%>"/>
						</div>
					</div>
          
			         <div>
                 <div class="control-group" id="repsDiv" style="display: none;">
						<label class="control-label" for="type">Reps</label>

						<div class="controls">
								 <input
									type="range" 
									onchange="rangevalue.value=value" id="reps"
									name="reps" value="<%=repsvalue%>"/>
						</div>
					</div>

					<div class="control-group" id="repsPerSecDiv" style="display: none;">
						<label class="control-label" for="type">Time for 1 rep per
							seconds</label>

						<div class="controls">
								<input
									type="range" 
									onchange="rangevalue.value=value" id="repsperseconds"
									name="repsperseconds"  value="<%=repsperseconds%>"/>
						</div>
					</div>
					<div class="control-group" id="holdTimeDiv" style="display: none;">
						<label class="control-label" for="type">Hold Time</label>

						<div class="controls">
								<input
									type="range" 
									onchange="rangevalue.value=value" id="holdtime"
									name="holdtime" min="5" max="200" value="<%=holdtime%>"/>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="type">Side</label>
						<div class="controls">
							<table>
								<tr>
									<td><label for="radio1">Left&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><input class="side" type="radio" id="radio1" name="side" value="Left"/></td>
									<td><label for="radio2">Right&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><input class="side" type="radio" id="radio2" name="side" value="Right"/></td>
									<td><label for="radio3">Both&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><input class="side" type="radio" id="radio3" name="side" value="Both"/></td>
									<td><label for="radio3">Alternating&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><input class="side" type="radio" id="radio4" name="side" value="Alternating"/></td>	
								</tr>
							</table>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="type">Sets</label>
						<div class="controls">
						<input
									type="range" 
									onchange="rangevalue.value=value" id="sets"
									name="sets"  value="<%=sets%>"/>
									</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="type">Rest</label>

						<div class="controls">
								 <input
									type="range"
									onchange="rangevalue.value=value" id="rest"
									name="rest"  value="<%=rest%>" onload=""/>
						</div>
					</div>

					<div class="control-group">
						<label class="control-label" for="optionsCheckbox">Show
							Sides</label>
						<div class="controls">
							<label class="checkbox"> <input type="checkbox"
								id="optionsCheckbox" name="showSides" value="true"
								<%=(showSides) ? " checked" : ""%>> Display the
								left/right/both selector
							</label>
						</div>
					</div>
            <div class="control-group"><label for="Media" class="control-label">Tabs</label>
                <div class="controls">
                <% if (id == 0) {%>
                Select types and body parts after saving
                <% } else { %>
                    <% if (e.getBodyParts() != null && e.getBodyParts().size() > 0) {%>
                    <b>Body Parts:</b>
                    <%
                        int i = 0;
                        for (Exercise.BodyPart bp : e.getBodyParts()) {
                    %>
                    <%=bp.getValue()%><%=(++i < e.getBodyParts().size()) ? "," : ""%>
                    <%
                        }
                    %>
                    <% } %>
                    <% if (e.getTypes() != null && e.getTypes().size() > 0) {%>
                    <br><b>Exercise Types:</b>
                    <%
                        int i = 0;
                        for (Exercise.Type t : e.getTypes()) {
                    %>
                    <%=t.getValue()%><%=(++i < e.getTypes().size()) ? "," : ""%>
                    <%
                        }
                    %>
                    <% } %>
                    <br>
                <a href="inline/bodyparts.jsp?id=<%=id%>" class="btn fancybox" style="margin-top: 15px;">Edit Tabs</a>
                <% } %>
            </div>
            </div>
            <div class="control-group" style="margin-top: 15px;"><label for="Media" class="control-label">&nbsp;</label>
                <div class="controls">
                <% if (id == 0) {%>
                Save exercise before adding media
                <% } else { %>
                <a id="editmedia" href="inline/media.jsp?id=<%=id%>" class="btn btn-primary fancybox">Edit Media</a>
                <% } %>
            </div>
            </div>
                <% if (id != 0) {%>
            <div class="control-group" style="margin-top: 15px;">
                <label for="Delete" class="control-label">&nbsp;</label>
                <div class="controls">


                <a href="inline/delete.jsp?id=<%=id%>" class="btn btn-danger fancybox">Delete Exercise</a>

            </div>
            </div>
                <% } %>

    </div>

    <div class="span4">
        <ul class="thumbnails">
            <%

                if (id != 0) {
                    int i = 0;
                    List<ExerciseMedia> exerciseMediaList = lm.getExerciseMedia(id);
                    for (ExerciseMedia em : exerciseMediaList) {
                        i++;
                        String url = "/images/movie_150_100.gif";
                        if (em.getType().equalsIgnoreCase("photo")) {
                            url = "/media?id=" + em.getId() + "&width=150&v=" + em.getVersion();
                        }
                            String caption = em.getCaption();
                            if (caption == null || caption.length() == 0) {
                                caption = em.getName();
                            }

            %>
            <li>
          <div class="thumbnail" style="float: left; position: relative;">
            <a href="inline/view.jsp?id=<%=em.getId()%>&edit=true" class="fancybox"><img src="<%=url%>" width="150" alt="<%=Util.formValue(caption)%>"></a>
              <div style="position: absolute; top:5px; left:5px; z-index: 999; background-color: black; opacity: 0.6;font-weight: bold; padding: 3px; color: white;"><%=i%></div>
          </div>
        </li>
            <%
                }
                }
            %>
        </ul>
    </div>
    </div>
    <div class="row">
        <div class="span9">
            <%=HTML.saveAndClose()%>
        </div>
    </div>

        </form>
        	<script type="text/javascript" src="/js/rSlider.min.js"></script>
        
        <script type="text/javascript">
        
        $(document).ready(function() {
			$("#editmedia").fancybox({
				maxWidth : 580,
				height : 400,
				autoSize : false,
				closeClick : false,
				openEffect : 'none',
				closeEffect : 'none',
				type : 'iframe',
				'afterClose' : function() {
					window.location.reload();
				},
			});
		});
		
		function setExcerciseType(){
			var excerciseType = document.getElementById("type").value;
			if(excerciseType == "timed"){
				document.getElementById("repsDiv").style.display = 'none';
				document.getElementById("repsPerSecDiv").style.display = 'none';
				document.getElementById("holdTimeDiv").style.display = 'block';
			} else if(excerciseType == "rep"){
				document.getElementById("repsDiv").style.display = 'block';
				document.getElementById("repsPerSecDiv").style.display = 'block';
				document.getElementById("holdTimeDiv").style.display = 'none';
			} else {
				document.getElementById("repsDiv").style.display = 'none';
				document.getElementById("repsPerSecDiv").style.display = 'none';
				document.getElementById("holdTimeDiv").style.display = 'none';
			}
		}
		(function () {
            'use strict';

            var init = function () {    
            	document.getElementById("repsDiv").style.display = 'block';
				document.getElementById("repsPerSecDiv").style.display = 'block';
				document.getElementById("holdTimeDiv").style.display = 'block';
                var sets = new rSlider({
                    target: '#sets',
                    values: [1, 2, 3, 4, 5, 6],
                    range: false,
                    set: [<%=sets%>],
                    tooltip: false,
                    onChange: function (vals) {
                        console.log(vals);
                    }
                
                });
                
                
                var rest = new rSlider({
                    target: '#rest',
                    values: [0, 10, 20, 30, 60],
                    range: false,
                    set: [<%=rest%>],
                    tooltip: false,
                    onChange: function (vals) {
                        console.log(vals);
                    }
                
                });
                
                var reps = new rSlider({
                    target: '#reps',
                    values: [1,2,3,4,5,6,7,8,9,10,11,12,15,20,25,30, 40],
                    range: false,
                    set: [<%=repsvalue%>],
                    tooltip: false,
                    onChange: function (vals) {
                        console.log(vals);
                    }
                
                });
                
                var repsperseconds = new rSlider({
                    target: '#repsperseconds',
                    values: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
                    range: false,
                    set: [<%=repsperseconds%>],
                    tooltip: false,
                    onChange: function (vals) {
                        console.log(vals);
                    }
                
                });
                
                var holdtime = new rSlider({
                    target: '#holdtime',
                    values: [5, 10, 15, 20, 30, 40, 60, 90, 120, 180],
                    range: false,
                    set: [<%=holdtime%>],
                    tooltip: false,
                    onChange: function (vals) {
                        console.log(vals);
                    }
                
                });
                var weight = new rSlider({
                    target: '#weight',
                    values: [0, 50, 70, 100, 120, 150, 170, 200, 220,250],
                    range: false,
                    set: [<%=weight%>],
                    tooltip: false,
                    onChange: function (vals) {
                        console.log(vals);
                    }
                });
                setExcerciseType();	
            };
            window.onload = init;
        })();
		
				function setSide(){
					var side = '<%=side%>';
					if(side == null || side == ""){
						document.getElementById("radio1").checked = true;
					}else{
						setCheckedValueOfRadioButtonGroup(side);
					}
				}
				function setCheckedValueOfRadioButtonGroup(vValue) {
				    var radios = document.getElementsByName("side");
				    for (var j = 0; j < radios.length; j++) {
				        if (radios[j].value == vValue) {
				            radios[j].checked = true;
				            break;
				        }
				    }
				}
				
				$(document).ready(function () {
					setSide();
				});

		
	</script>
</body>
</html>