<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.Exercise" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="java.util.List" %>
<%@ page import="au.com.ourbodycorp.model.User" %>
<%

    String type = Util.notNull(request.getParameter("type"));
    String bodyPart = Util.notNull(request.getParameter("bp"));
    User u = (User) session.getAttribute("user");
    String activity_page="admin/library/index.jsp";
    String corp_id="---";
	String corp_name="---";
	String corp_loc="---";
	String user_id=String.valueOf(u.getId());
	String user_name="Fiizio Admin";
    String invitor_id="";
    String invitor_name="---";
    String flag="view";
    String from="---";
    String to="---";
    /* try{
    	//Util.allActivitiesLogs("Library", u.getId(), "Fiizio System Admin", "View Exercise Library");
    	Util.allActivitiesLogs2("Libarary",activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,"View Exercise By Fiizio System Admin:"+u.getFullName());
    }catch(Exception e){
    	
    } */

    String[][] breadcrumbs = {{"/admin/", "Admin"}, {"/admin/library/", "Library"}, {null, "Exercises"}};

    LibraryManager lm = new LibraryManager();

    Exercise.BodyPart bp = null;
    Exercise.Type tp = null;
    try {
        bp = Exercise.BodyPart.valueOf(bodyPart);
    } catch (Exception e) {
        bp = null;
    }

    try {
        tp = Exercise.Type.valueOf(type);
    } catch (Exception e) {
        tp = null;
    }

    List<Exercise> exercises = lm.getExercises(bp, tp, -1);



%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head><title>Exercise Library</title></head>
<body>
<div class="page-header">
    <h1>Exercise Library</h1>
</div>
<%@include file="/WEB-INF/includes/breadcrumbs.jsp"%>

<div class="row">
    <div class="span9">

        <a href="exercise.jsp?type=<%=type%>&bp=<%=bodyPart%>" class="btn btn-primary"><i class="icon-plus icon-white"></i> Add Exercise</a>
    </div>
</div>
<div class="row" style="margin-top: 10px;">
    <div class="span8 offset1">
        <ul class="nav nav-tabs" style="margin-bottom: 0;">
            <li <%=(type.isEmpty()) ? "class=\"active\"" : ""%>>
                <a href="?type=&bp=<%=bodyPart%>">All Types</a>
            </li>
            <%
                for (Exercise.Type et : Exercise.Type.values()) {
            %>
            <li <%=(type.equals(et.name())) ? "class=\"active\"" : ""%>>
                <a href="?type=<%=et.name()%>&bp=<%=bodyPart%>"><%=et.getValue()%></a>
            </li>
            <%
                }
            %>
        </ul>
    </div>
</div>
<div class="row">
    <div class="span1">
        <div class="tabs-left">
            <ul class="nav nav-tabs">
            <li <%=(bodyPart.isEmpty()) ? "class=\"active\"" : ""%>>
                <a href="?type=<%=type%>&bp=">All Parts</a>
            </li>
            <%
                for (Exercise.BodyPart b : Exercise.BodyPart.values()) {
            %>
            <li <%=(bodyPart.equals(b.name())) ? "class=\"active\"" : ""%>>
                <a href="?type=<%=type%>&bp=<%=b.name()%>"><%=b.getValue()%></a>
            </li>
            <%
                }
            %>
        </ul>
        </div>
    </div>
    <div class="span8" style="padding: 10px;">
        <ul class="thumbnails">
            <%
                for (Exercise exercise : exercises) {
                    String url = "/images/photo_150_100.gif";
                    if (exercise.getKeyPhotoId() > 0) {
                        url = "/media?id=" + exercise.getKeyPhotoId() + "&width=150&v=" + exercise.getKeyPhotoVersion();
                    }
            %>
            <li class="span2">
          <div class="thumbnail">
              <img src="<%=url%>" width="150" alt="">
            <div class="caption">
              <h5 style="height: 35px; overflow: hidden;"><a href="exercise.jsp?id=<%=exercise.getId()%>"><%=exercise.getName()%></a></h5>
            </div>
          </div>
        </li>
            <%
                }
            %>


      </ul>
    </div>
</div>
</body>
</html>