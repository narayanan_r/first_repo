<%@page import="org.zefer.html.doc.u"%>
<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.Exercise" %>
<%@ page import="au.com.ourbodycorp.model.ExerciseMedia" %>
<%@ page import="au.com.ourbodycorp.model.User" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.LinkedHashMap" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%

    long id = Long.parseLong(Util.notNull(request.getParameter("id"), "0"));
    String name = Util.notNull(request.getParameter("name"));
    String description = Util.notNull(request.getParameter("description"));
    String type = Util.notNull(request.getParameter("type"));
    boolean showSides = request.getParameter("showSides") != null && Boolean.parseBoolean(request.getParameter("showSides"));
    LibraryManager lm = new LibraryManager();
    Exercise e = null;
    String title = "Create Exercise";
    User u = (User) session.getAttribute("user");
    String activity_page="admin/library/exercise.jsp";
    String corp_id="---";
	String corp_name="---";
	String corp_loc="---";
	String user_id=String.valueOf(u.getId());
	String user_name="Fiizio Admin";
   
    if (id > 0) {
    	
        title = "Edit Exercise";
        e = lm.getExercise(id);
        if (e == null) {
            response.sendError(404, "Exercise not found");
            return;
        }
        if (request.getMethod().equalsIgnoreCase("post")) {
        String invitor_id=String.valueOf(e.getId());
        String invitor_name="---";
        String flag="update";
        String from="Name:"+e.getName()+";Description:"+e.getDescription()+";";
        String to="Name:"+request.getParameter("name")+";Description:"+request.getParameter("description")+";";
        try{
    		//Util.allActivitiesLogs("Library", u.getId(), "Fiizio System Admin", "Edit Exercise");
    		Util.allActivitiesLogs2("Library",activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,"Edit Exercise "+e.getName()+" By Fiizio System Admin:"+u.getFullName());
    	}catch(Exception ee){
    		
    	}
        if (request.getParameter("submit").contains("Close")) {
            String bodyPart = "";
            if (e.getBodyParts() != null && e.getBodyParts().size() > 0) {
                bodyPart = e.getBodyParts().getFirst().name();
            }

            String type2 = "";
            if (e.getTypes() != null && e.getTypes().size() > 0) {
                type2 = e.getTypes().getFirst().name();
            }

            response.sendRedirect("/admin/library/?bp=" + bodyPart + "&type=" + type2);
        } else {
            response.sendRedirect("exercise.jsp?id=" + e.getId());
        }
        }
    }/* else{
    	try{
    		Util.allActivitiesLogs("Library", u.getId(), "Fiizio System Admin", "Create Exercise");
    	}catch(Exception ee){
    		
    	}
    } */

    HashMap<String, String> errorFields = new HashMap<String, String>();


    if (request.getMethod().equalsIgnoreCase("get") && id > 0) {
        name = e.getName();
        description = e.getDescription();
        type = e.getType();
        showSides = e.isShowSides();
    } else if (request.getMethod().equalsIgnoreCase("post")) {
        if (name.trim().length() == 0) {
            errorFields.put("name", "Please enter a name");
        }
		if(e == null){
        if (errorFields.size() == 0) {
            if (e == null) {
                e = new Exercise();
                e.setCreator(u.getId());
            }
            e.setName(name);
            e.setType(type);
            e.setDescription(description);
            e.setShowSides(showSides);
            lm.save(e,0,"true");
            String invitor_id=String.valueOf(e.getId());
            String invitor_name="---";
            String flag="add";
            String from="---";
            String to="---";
            try{
        		//Util.allActivitiesLogs("Library", u.getId(), "Fiizio System Admin", "Create Exercise");
        		Util.allActivitiesLogs2("Library",activity_page,corp_id,corp_name,corp_loc,user_id,user_name,String.valueOf(e.getId()),invitor_name,"create","","","Create Exercise "+e.getName()+" By Fiizio System Admin:"+u.getFullName());
        	}catch(Exception ee){
        		
        	}
            if (request.getParameter("submit").contains("Close")) {
                String bodyPart = "";
                if (e.getBodyParts() != null && e.getBodyParts().size() > 0) {
                    bodyPart = e.getBodyParts().getFirst().name();
                }

                String type2 = "";
                if (e.getTypes() != null && e.getTypes().size() > 0) {
                    type2 = e.getTypes().getFirst().name();
                }

                response.sendRedirect("/admin/library/?bp=" + bodyPart + "&type=" + type2);
            } else {
                response.sendRedirect("exercise.jsp?id=" + e.getId());
            }
            return;
        }
    }
    }

    LinkedHashMap<String, String> bodyParts = new LinkedHashMap<String, String>();
    for (Exercise.BodyPart b : Exercise.BodyPart.values()) {
        bodyParts.put(b.name(), b.getValue());
    }
    LinkedHashMap<String, String> types = new LinkedHashMap<String, String>();
    for (Exercise.Type b : Exercise.Type.values()) {
        types.put(b.name(), b.getValue());
    }

    String[][] breadcrumbs = {{"/admin/", "Admin"}, {"/admin/library/", "Library"}, {"", title}};

%>
<html>
<head><title><%=title%></title></head>
<body>
<div class="page-header">
    <h1><%=title%></h1>
</div>
<%@include file="/WEB-INF/includes/breadcrumbs.jsp"%>
<form action="exercise.jsp" method="post" class="form-horizontal">
<div class="row">

    <div class="span5">
        <%@include file="/WEB-INF/includes/formError.jsp"%>

            <input type="hidden" name="id" value="<%=id%>">


            <%=HTML.textInput("name","Name", "100", "100", name, null, errorFields)%>
            <%=HTML.textArea("description", "Description", description, 5)%>
        <div class="control-group">
            <label class="control-label" for="type">This exercise is</label>
            <div class="controls">
              <select id="type" name="type">
                <option value="<%=Exercise.TIMED_EXERCISE%>" <%=(type.equals(Exercise.TIMED_EXERCISE)) ? "selected" : ""%>>Time based</option>
                  <option value="<%=Exercise.REP_EXERCISE%>" <%=(type.equals(Exercise.REP_EXERCISE)) ? "selected" : ""%>>Rep based</option>
              </select>
            </div>
          </div>
          <div class="control-group">
            <label class="control-label" for="optionsCheckbox">Show Sides</label>
            <div class="controls">
              <label class="checkbox">
                <input type="checkbox" id="optionsCheckbox" name="showSides" value="true"<%=(showSides) ? " checked": ""%>>
                Display the left/right/both selector
              </label>
            </div>
          </div>

            <div class="control-group"><label for="Media" class="control-label">Tabs</label>
                <div class="controls">
                <% if (id == 0) {%>
                Select types and body parts after saving
                <% } else { %>
                    <% if (e.getBodyParts() != null && e.getBodyParts().size() > 0) {%>
                    <b>Body Parts:</b>
                    <%
                        int i = 0;
                        for (Exercise.BodyPart bp : e.getBodyParts()) {
                    %>
                    <%=bp.getValue()%><%=(++i < e.getBodyParts().size()) ? "," : ""%>
                    <%
                        }
                    %>
                    <% } %>
                    <% if (e.getTypes() != null && e.getTypes().size() > 0) {%>
                    <br><b>Exercise Types:</b>
                    <%
                        int i = 0;
                        for (Exercise.Type t : e.getTypes()) {
                    %>
                    <%=t.getValue()%><%=(++i < e.getTypes().size()) ? "," : ""%>
                    <%
                        }
                    %>
                    <% } %>
                    <br>
                <a href="inline/bodyparts.jsp?id=<%=id%>" class="btn fancybox" style="margin-top: 15px;">Edit Tabs</a>
                <% } %>
            </div>
            </div>
            <div class="control-group" style="margin-top: 15px;"><label for="Media" class="control-label">&nbsp;</label>
                <div class="controls">
                <% if (id == 0) {%>
                Save exercise before adding media
                <% } else { %>
                <a href="inline/media.jsp?id=<%=id%>" class="btn btn-primary fancybox">Edit Media</a>
                <% } %>
            </div>
            </div>
                <% if (id != 0) {%>
            <div class="control-group" style="margin-top: 15px;">
                <label for="Delete" class="control-label">&nbsp;</label>
                <div class="controls">


                <a href="inline/delete.jsp?id=<%=id%>" class="btn btn-danger fancybox">Delete Exercise</a>

            </div>
            </div>
                <% } %>

    </div>

    <div class="span4">
        <ul class="thumbnails">
            <%

                if (id != 0) {
                    int i = 0;
                    List<ExerciseMedia> exerciseMediaList = lm.getExerciseMedia(id);
                    for (ExerciseMedia em : exerciseMediaList) {
                        i++;
                        String url = "/images/movie_150_100.gif";
                        if (em.getType().equalsIgnoreCase("photo")) {
                            url = "/media?id=" + em.getId() + "&width=150&v=" + em.getVersion();
                        }
                            String caption = em.getCaption();
                            if (caption == null || caption.length() == 0) {
                                caption = em.getName();
                            }

            %>
            <li>
          <div class="thumbnail" style="float: left; position: relative;">
            <a href="inline/view.jsp?id=<%=em.getId()%>&edit=true" class="fancybox"><img src="<%=url%>" width="150" alt="<%=Util.formValue(caption)%>"></a>
              <div style="position: absolute; top:5px; left:5px; z-index: 999; background-color: black; opacity: 0.6;font-weight: bold; padding: 3px; color: white;"><%=i%></div>
          </div>
        </li>
            <%
                }
                }
            %>
        </ul>
    </div>
    </div>
    <div class="row">
        <div class="span9">
            <%=HTML.saveAndClose()%>
        </div>
    </div>

        </form>

</body>
</html>