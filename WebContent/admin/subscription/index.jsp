<%@page import="org.joda.time.DateTime"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="au.com.ourbodycorp.HTML"%>
<%@page import="java.util.LinkedList"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="au.com.ourbodycorp.Util"%>
<%@page import="au.com.ourbodycorp.model.Subscription"%>
<%@page import="java.util.List"%>
<%@page import="au.com.ourbodycorp.model.managers.SubscriptionManager"%>
<%
    String activity_page="admin/subscription/index.jsp";
    String[][] breadcrumbs = {{"/admin/", "Admin"}, {"/admin/subscription/", "Subscription"} , {null,"Subscription Plans"}};
    SubscriptionManager sm = new SubscriptionManager();
    String	index=(String) session.getAttribute("subscription");
    session.removeAttribute("subscription");
    int currentPage = 1;
    try {
        currentPage = Integer.parseInt(request.getParameter("page"));
    } catch (NumberFormatException nfe) {
    }
    String query = (request.getParameter("query") != null && request.getParameter("query").trim().length() > 0) ? request.getParameter("query") : null;
    int perPage = 10;
    int count = 0;
    if (query != null) {
        count = sm.countSearchSubscription(query);
    } else {
        count = sm.countSubscription();
    }
    int pages = count / perPage;
    if (count % perPage > 0) pages++;

    if (currentPage > pages + 1) {
        currentPage = pages;
    }
    LinkedList<String> pageList = new LinkedList<String>();
    for (int i = 1; i <= pages; i++) {
        if (query != null) {
            pageList.add("?query=" + query + "&page=" + i);
        } else {
            pageList.add("?page=" + i);
        }

    }
    List<Subscription> subcriptionList;
    if (query != null) {
    	subcriptionList = sm.getSearchSubscription(query,(currentPage - 1) * perPage, perPage);
    } else {
    	subcriptionList = sm.getAllSubscription((currentPage - 1) * perPage, perPage);
    }
%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head><title>Subscription</title>
<style>
/* Sortable tables */
table.sortable thead {
    background-color:#eee;
    color:#666666;
    font-weight: bold;
    cursor: default;
}
</style>
<script type="text/javascript">
setTimeout(function() {
    $('#subs').fadeOut('fast');
}, 3000);
</script>
<script src="/js/sortable.js">
</script>
</head>
<body>
<div class="page-header">
    <h1>Subscription</h1>
</div>
<%@include file="/WEB-INF/includes/breadcrumbs.jsp"%>
<%if (index != null && StringUtils.equals(index, "true")) {%>
	<div class="row" id="subs">
	    <div class="span9 columns">
	        <div class="alert alert-success">
	      		<p>Successfully saved</p>
	  		</div>
	    </div>
	</div>
<%}%>
<div class="row">
    <div class="span2">
        <a href="subscription.jsp" class="btn btn-primary"><i class="icon-plus icon-white"></i> Add Subscription</a>
    </div>
    <div class="span4">  &nbsp; </div>
	<div class="span3">
        <form class="form-search" method="get" action="">
        <input type="text" class="input-medium search-query" name="query" value="">
        <button type="submit" class="btn">Search</button>
        </form>
    </div>
    </div>
	<div class="row">
        <div class="span9 columns">
        <table id="subscription" class="table table-zebra-stripes sortable">
            <thead>
            <tr>
                <th data-tsorter="input-text">Subscription Plan Name</th>
                <th>Subscription Period</th>
                <th>Clinic</th>
                <th>Clinic Price</th>
                <th>Patient Price</th>
                <th>Currency Type</th>
                <th>Created On</th>
                <th>Status</th>
                <th>Delete</th>
            </tr>
            </thead>
            <tbody>
            <%
            if(subcriptionList!=null && subcriptionList.size()>0)
            {
            	for(Subscription s : subcriptionList)
            	{	
            		String CorpName=sm.getCorpName(s.getCorporation());
            %>
            <tr>
                <td><a href="/admin/subscription/subscription.jsp?id=<%=s.getSubscriptionId()%>"><%=s.getSubscription()%></a></td>
                <td><%=!"0".equals(String.valueOf(s.getSubscriptionPeriod())) ? s.getSubscriptionPeriod() : ""%> 
                <%=(StringUtils.isNotBlank(s.getSubPeriodType())) ? s.getSubPeriodType() : ""%></td>
                <td><%=CorpName%></td>
                <td><%=s.getSubscriptionClinicAmt()%></td>
                <td><%=s.getSubscriptionPatientAmt()%></td>
                <td><%=s.getCurrencyType()%></td>
                <%SimpleDateFormat sdf = new SimpleDateFormat("d MMM yyyy");%>
                <td><%=sdf.format(new DateTime(s.getCreated()).toDate())%></td>
                <td><%=s.isActive() ? "Active" : "InActive"%></td>
                <td><a class="btn btn-danger btn-mini fancybox" href="delete_subscription.jsp?id=<%=s.getSubscriptionId()%>"><i class="icon icon-trash icon-white"></i>
						Delete
					</a>
				</td>
            </tr>
            <%
               }
            }
            %>
            </tbody>
        </table>
        <%=HTML.pages(pageList, currentPage)%>
    </div>
</div>

</body>
</html>