	<%@page import="java.util.Date"%>
<%@page import="org.joda.time.DateTime"%>
<%@page import="au.com.ourbodycorp.model.managers.CorporationManager"%>
<%@page import="au.com.ourbodycorp.model.User"%>
<%@page import="au.com.ourbodycorp.model.Subscription"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="au.com.ourbodycorp.Util"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="au.com.ourbodycorp.model.managers.SubscriptionManager"%>
<%@page import="au.com.ourbodycorp.model.Corporation"%>
<%@page import="java.util.List"%>
<%@page import="java.util.HashMap"%>
<%@page import="au.com.ourbodycorp.HTML"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
	boolean active = false;
	boolean effective = false;
	String	index=(String) session.getAttribute("subscription");
    session.removeAttribute("subscription");
	User u = (User) session.getAttribute("user");
	String activity_page="admin/subscription/subscription.jsp";
    HashMap<String, String> errorFields = new HashMap<String, String>();
    String subscription = request.getParameter("subscription");  
    if(StringUtils.isNotBlank(subscription)){
    	subscription=subscription.trim();
    	}
    String subscriptionPeriod = request.getParameter("subscriptionPeriod");
    String subPeriodType = request.getParameter("subPeriodType");
    String corporation = request.getParameter("corporation");
    String subscriptionClinicAmt = request.getParameter("subscriptionClinicAmt");
    String subscriptionPatientAmt = request.getParameter("subscriptionPatientAmt");
    String currencyType = request.getParameter("currencyType");
    if(request.getParameter("active") != null){
    	active = Boolean.parseBoolean(request.getParameter("active"));
    }
    Subscription sub = null;
    SubscriptionManager sm = new SubscriptionManager();
    CorporationManager cm = new CorporationManager();
    Corporation c=null;
    String subPeriodTypeChoices[] = {"DAY", "WEEK", "MONTH", "YEAR"};
    String subPeriodTypeChoicesVal[] = {"DAY", "WEEK", "MONTH", "YEAR"};
    String subCurrencyTypeChoices[] = {"USD","NIS", "CAD"};
    long id = 0;
    String type="Subscription Plan";
	String corp_id="---";
	String corp_name="---";
	String corp_loc="---";
	String user_id=String.valueOf(u.getId());
	String user_name="Fiizio Admin";
	String invitor_id="---";
	String invitor_name="---";
	String flag="delete";
	String from="---";
	String to="---";
    boolean shareMainLocation = false;
    if(request.getParameter("shareMainLocation") != null){
    	shareMainLocation = Boolean.valueOf(request.getParameter("shareMainLocation"));
    }
    if(request.getParameter("id") != null){
    	id = Long.parseLong(request.getParameter("id"));
    }
    String title = "Create Subscription Plan";
    if(id>0){
    	sub = sm.getSubscriptions(id);
    	if(sub.isActive()==false&&active==false){
    		effective=true;
    	}
    	title = "Edit Subscription Plan";
    	c=cm.getCorporation(sub.getCorporation());
    }else{
    	active=true;
    }
    if (request.getMethod().equalsIgnoreCase("post")) {
    	if(StringUtils.isBlank(subscription)){
    		errorFields.put("subscription", "Please enter Subscription");
    	}
    	if(StringUtils.equals(subscriptionPeriod, "0") || StringUtils.isBlank(subscriptionPeriod) || StringUtils.isBlank(subPeriodType)){
    		errorFields.put("subscriptionPeriod", "Please enter Subscription Period");
    	}
    	if(title != "Edit Subscription Plan"){
    	if(StringUtils.isBlank(corporation)){
    		errorFields.put("corporation", "Please enter Clinic");
    	}
    	}
    	if(StringUtils.isBlank(subscriptionClinicAmt)){
    		errorFields.put("subscriptionClinicAmt", "Please enter Clinic Price");
    	}
    	if(StringUtils.isBlank(subscriptionPatientAmt)){
    		errorFields.put("subscriptionPatientAmt", "Please enter Patient Price");
    	}
    	if(StringUtils.isBlank(currencyType)){
    		errorFields.put("currencyType", "Please enter Currency Type");
    	}
    	if(id>0 ||(errorFields.size() == 0 && StringUtils.isNotBlank(corporation))){
			String cId="0";
			if(id>0){
				cId=String.valueOf(c.getId());
				if(sm.checkSubscriptionExists(subscription,cId,id)){
			         errorFields.put("subscription", "Plan already exists");
			    	}
			}else{
				String[] corporations = corporation.split("-");
				cId=corporations[0];
				if(sm.checkSubscriptionExists(subscription,cId,id)){
			         errorFields.put("subscription", "Plan already exists");
			    }
			}
			if(errorFields.size() == 0 && shareMainLocation){
				List<String> subLocationLst = cm.getSubLocations(Long.parseLong(cId));
	    		if(subLocationLst != null && subLocationLst.size()>0){
	    			List<Subscription> subscriptionList=null;
	    			for(String corpId : subLocationLst){
	    				if(id>0){
	    					subscriptionList=sm.getSubscriptionWithShareidList(sub.getSharesubscriptionid(),Long.parseLong(corpId));
	    					if(subscriptionList.size()>0){
		    					for(Subscription s:subscriptionList){
		    						if(s.getCorporation()==Long.parseLong(corpId)&&sm.checkSubscriptionExists(subscription,corpId,s.getSubscriptionId())){
		   	    			         errorFields.put("subscription", "Plan already exists in sublocation");
		   	    			    	}
		    					}
	    					}else{
			    				if(sm.checkSubscriptionExists(subscription,corpId,id)){
			    			         errorFields.put("subscription", "Plan already exists in sublocation");
			    			    }
			    			}
		    			}else{
		    				if(sm.checkSubscriptionExists(subscription,corpId,id)){
		    			         errorFields.put("subscription", "Plan already exists in sublocation");
		    			    }
		    			}
	    			}
	    		}
			}
		}
    	
    	if (errorFields.size() == 0) {
    		active= Boolean.parseBoolean(request.getParameter("active"));
    		if(sub == null){
    			sub = new Subscription();
    		}
     		if(StringUtils.isNotBlank(corporation)){
    			String[] corporations = corporation.split("-");
    			sub.setCorporation(Long.parseLong(corporations[0]));    			
    		}
    		if(StringUtils.isNotBlank(subscription)){
    			sub.setSubscription(subscription);
    		}
    		if(StringUtils.isNotBlank(subscriptionPeriod)){
    			sub.setSubscriptionPeriod(Long.parseLong(subscriptionPeriod));    			
    		}
    		sub.setSubPeriodType(subPeriodType);
    		if(StringUtils.isNotBlank(subscriptionClinicAmt)){
    			sub.setSubscriptionClinicAmt(Float.parseFloat(subscriptionClinicAmt));
    		}
    		if(StringUtils.isNotBlank(subscriptionPatientAmt)){
    			sub.setSubscriptionPatientAmt(Float.parseFloat(subscriptionPatientAmt));
    		}
    		if(StringUtils.isNotBlank(currencyType)){
    			sub.setCurrencyType(currencyType);
    		}
    		sub.setShareSubscription(shareMainLocation);
    		sub.setActive(active);
    		if(active==true){
    			sub.setPlanineffectivedate(null);
    		}else{
    			if(effective==false){
    				sub.setPlanineffectivedate(new Date());
    			}
    		}
    		if(u != null && u.getId() != 0){
    			sub.setCreator(u.getId());
    		}
    		sub = sm.save(sub);
    		try{
        		//Util.allActivitiesLogs("Plan &amp; Price", u.getId(), "Fiizio System Admin", "Add New Plan");
            	Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,"Add New Plan By Fiizio System Admin:"+u.getFullName());
        	}catch(Exception ee){
        		
        	}
    		id = sub.getSubscriptionId();
    		if(shareMainLocation){
	    		List<String> subLocationLst = cm.getSubLocations(sub.getCorporation());
	    		if(subLocationLst != null && subLocationLst.size()>0){
	    			for(String corpId : subLocationLst){
	    				List<Subscription> shareSubLst = sm.getSubscriptionWithShareidList(id,Long.parseLong(corpId));
	    				if(shareSubLst != null && shareSubLst.size()>0 && title == "Edit Subscription Plan"){
	    					for(Subscription shareSubscription : shareSubLst){
	    						if(StringUtils.isNotBlank(subscription)){
	    							shareSubscription.setSubscription(subscription);
			    	    		}
			    	    		if(StringUtils.isNotBlank(subscriptionPeriod)){
			    	    			shareSubscription.setSubscriptionPeriod(Long.parseLong(subscriptionPeriod));    			
			    	    		}
			    	    		shareSubscription.setSubPeriodType(subPeriodType);
			    	    		if(StringUtils.isNotBlank(subscriptionClinicAmt)){
			    	    			shareSubscription.setSubscriptionClinicAmt(Float.parseFloat(subscriptionClinicAmt));
			    	    		}
			    	    		if(StringUtils.isNotBlank(subscriptionPatientAmt)){
			    	    			shareSubscription.setSubscriptionPatientAmt(Float.parseFloat(subscriptionPatientAmt));
			    	    		}
			    	    		if(StringUtils.isNotBlank(currencyType)){
			    	    			shareSubscription.setCurrencyType(currencyType);
			    	    		}
			    	    		shareSubscription.setShareSubscription(false);
			    	    		if(u != null && u.getId() != 0){
			    	    			shareSubscription.setCreator(u.getId());
			    	    		}
			    	    		shareSubscription.setCorporation(Long.parseLong(corpId));
			    	    		shareSubscription.setSharesubscriptionid(id);
			    	    		shareSubscription.setActive(active);
			    	    		shareSubscription.setPlanineffectivedate(sub.getPlanineffectivedate());
	    						sm.save(shareSubscription);
	    					}
	    				}else{
	    					Subscription shareSub = new Subscription(); 
		    				if(StringUtils.isNotBlank(subscription)){
		    					shareSub.setSubscription(subscription);
		    	    		}
		    	    		if(StringUtils.isNotBlank(subscriptionPeriod)){
		    	    			shareSub.setSubscriptionPeriod(Long.parseLong(subscriptionPeriod));    			
		    	    		}
		    	    		shareSub.setSubPeriodType(subPeriodType);
		    	    		if(StringUtils.isNotBlank(subscriptionClinicAmt)){
		    	    			shareSub.setSubscriptionClinicAmt(Float.parseFloat(subscriptionClinicAmt));
		    	    		}
		    	    		if(StringUtils.isNotBlank(subscriptionPatientAmt)){
		    	    			shareSub.setSubscriptionPatientAmt(Float.parseFloat(subscriptionPatientAmt));
		    	    		}
		    	    		if(StringUtils.isNotBlank(currencyType)){
		    	    			shareSub.setCurrencyType(currencyType);
		    	    		}
		    	    		shareSub.setShareSubscription(false);
		    	    		if(u != null && u.getId() != 0){
		    	    			shareSub.setCreator(u.getId());
		    	    		}
		    	    		shareSub.setCorporation(Long.parseLong(corpId));
		    	    		shareSub.setSharesubscriptionid(id);
		    	    		shareSub.setActive(active);
		    	    		shareSub.setPlanineffectivedate(sub.getPlanineffectivedate());
		    				sm.save(shareSub);
	    				}
	    			}
	    		}
    		}
    		title = "Edit Subscription Plan";
    		if (request.getParameter("submitbtn") != null && request.getParameter("submitbtn").contains("Close")) {
	    		 response.sendRedirect("/admin/subscription/index.jsp");
	    	}else{
	    		if(title == "Create Subscription Plan"){
            	   	response.sendRedirect("subscription.jsp");
            	}else{
            		session.setAttribute("subscription","true") ;
            		response.sendRedirect("subscription.jsp?id=" + sub.getSubscriptionId());
            	}
	    	}
    		session.setAttribute("subscription","true") ;
    		return;
    	}
    }else{
    	if(sub != null){
	    	id = sub.getSubscriptionId();
	    	corporation = String.valueOf(sub.getCorporation());
	    	subscription = sub.getSubscription();
	    	subscriptionPeriod = String.valueOf(sub.getSubscriptionPeriod());
	    	subPeriodType = sub.getSubPeriodType();
	    	subscriptionClinicAmt = String.valueOf(sub.getSubscriptionClinicAmt());
	    	subscriptionPatientAmt = String.valueOf(sub.getSubscriptionPatientAmt());
	    	currencyType = sub.getCurrencyType();
	    	shareMainLocation = sub.isShareSubscription();
	    	active = sub.isActive();
    	}
    }
    String[][] breadcrumbs = {{"/admin/", "Admin"}, {"/admin/subscription/", "Subscription"}, {null,title}};
%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head><title>Subscription</title>
<style type="text/css">
body { font-family: sans-serif; }
#confirmBox
{
    display: none;
    background-color: #eee;
    border-radius: 5px;
    border: 1px solid #aaa;
    position: fixed;
    width: 300px;
    left: 50%;
    margin-left: -150px;
    margin-top: -310px;
    padding: 6px 8px 8px;
    box-sizing: border-box;
    text-align: center;
    
}
#confirmBox .button {
    background-color: #ccc;
    display: inline-block;
    border-radius: 3px;
    border: 1px solid #aaa;
    padding: 2px;
    text-align: center;
    width: 80px;
    cursor: pointer;
}
#confirmBox .button:hover
{
    background-color: #ddd;
}
#confirmBox .message
{
    text-align: left;
    margin-bottom: 8px;
}
#overlay {
    background-color: rgba(0, 0, 0, 0.8);
    z-index: 999;
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    display: none;
}

</style>
<script type="text/javascript">
setTimeout(function() {
    $('#subs').fadeOut('fast');
}, 3000);
	function isNumeric(evt) {
    	evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }
    function isNumericWithDot(evt) {
    	var theEvent = evt || window.event;
        var key = theEvent.keyCode || theEvent.which;
        key = String.fromCharCode(key);
        var regex = /^[0-9.,\b]+$/;
        if (!regex.test(key)) {
            theEvent.returnValue = false;
            if (theEvent.preventDefault) theEvent.preventDefault();
        };
    }
    function subConfirmCancel(e,obj) {
    	var corporation = document.getElementById("corporation").value;
    	var corps = corporation.split("-");
    	var errorfield=false;
    	if(document.getElementById("subscription").value!="" && document.getElementById("subscriptionPeriod").value!="" &&
    	   document.getElementById("subscriptionPeriod").value!="0" && document.getElementById("subPeriodType").value!="" &&
    	   document.getElementById("corporation").value!="" && document.getElementById("subscriptionClinicAmt").value!="" &&
    	   document.getElementById("subscriptionPatientAmt").value!="" && document.getElementById("currencyType").value!=""){
    		errorfield=true;
    	}
    	e.preventDefault();
    	document.getElementById("submitbtn").value = obj.value;
	    if (errorfield==true && corps[1] > 0 ){
	    	doConfirm("Do you want to apply this Subscription to all sublocations?", function yes() {
	    		document.getElementById("shareMainLocation").value = true;
	    		document.getElementById("subForm").submit();
            }, function no() {
            	document.getElementById("shareMainLocation").value = false;
            	document.getElementById("subForm").submit();
            });
	    }else{
	    	document.getElementById("shareMainLocation").value = false;
	    	document.getElementById("subForm").submit();
	    }
    }
    function showorhide(obj){
		if(obj.checked){
			document.getElementById("active").value = true;
		}else{
			document.getElementById("active").value = false;
		}    	
    }
    function doConfirm(msg, yesFn, noFn) {
        var confirmBox = $("#confirmBox");
        confirmBox.find(".message").text(msg);
        confirmBox.find(".yes,.no").unbind().click(function () {
            confirmBox.hide();
        });
        confirmBox.find(".yes").click(yesFn);
        confirmBox.find(".no").click(noFn);
        confirmBox.show();
    }
</script>
<script>
$(":button").click(function() {
    $("#confirmBox").show();
});
$(":button").click(function() {
    $("#confirmBox").show();
});

$("#confirmBox").click(function() {
    $(this).hide();
});
</script>
</head>
<body>
<div class="page-header">
    <h1>Subscription</h1>
</div>
<%@include file="/WEB-INF/includes/breadcrumbs.jsp"%>
<%if (index != null && StringUtils.equals(index, "true")) {%>
	<div class="row" id="subs">
	    <div class="span9 columns">
	        <div class="alert alert-success">
	      		<p>Successfully saved</p>
	  		</div>
	    </div>
	</div>
<%}%>
<%@ include file="/WEB-INF/includes/formError.jsp"%>
<form action="subscription.jsp" method="post" class="form-horizontal" id="subForm" name="subForm">
<div class="row">
    <div class="span5">
    <input type="hidden" name="id" value="<%=id%>">
    <input type="hidden" name="shareMainLocation" id="shareMainLocation">
    <input type="hidden" name="submitbtn" id="submitbtn">
        <%=HTML.textInput("subscription","Subscription Plan Name", "100", "200", subscription, null, errorFields)%>
        <%if (errorFields != null && errorFields.containsKey("subscriptionPeriod")) {%>
        <div class="control-group error" style="margin-top: 15px;"><label class="control-label"> Subscription Period</label>
         	<div class="controls error">
        <%}else{ %>
        <div class="control-group" style="margin-top: 15px;"><label class="control-label"> Subscription Period</label>
            <div class="controls">
        <%} %>
            	<input type="text" class="input-small" maxlength="6" name="subscriptionPeriod" id="subscriptionPeriod" 
            	value="<%=(subscriptionPeriod != null ? subscriptionPeriod : "")%>" onkeypress="return isNumeric(event);"></input>
            	<select name="subPeriodType" id="subPeriodType" class="input-small">
            	<option value="">Select</option>
            	<%for (int i = 0; i < subPeriodTypeChoicesVal.length; i++) {%>
            		<option value="<%=subPeriodTypeChoicesVal[i]%>" <%=subPeriodTypeChoicesVal[i].equals(subPeriodType) ? "selected" : "" %>><%=subPeriodTypeChoicesVal[i]%></option>
            	<%} %>
            	</select> 
            	<%if (errorFields != null && errorFields.containsKey("subscriptionPeriod")) {%>
          			<span class="help-inline"><%=Util.notNull(errorFields.get("subscriptionPeriod"))%></span>
      			<%}%>     
	        </div>
        </div>
        <%if (errorFields != null && errorFields.containsKey("corporation")) {%>
         <div class="control-group error" style="margin-top: 15px;"><label class="control-label"> Clinic</label>
            <div class="controls error">
        <%}else{ %>
        <div class="control-group" style="margin-top: 15px;"><label class="control-label"> Clinic</label>
            <div class="controls">
        <%} %>
         		<select <%if(id>0){%>disabled="disabled"<%}%> id="corporation" name="corporation" class="input-xlarge">
		            <%if(id>0){List<Corporation> clinicList = sm.getAllClinics();
		               if (clinicList != null && clinicList.size() > 0) {
		                     if (clinicList != null && clinicList.size() > 0) {
		                            for (Corporation corp : clinicList) {
		                             String[] subLocCntWithId =null;
		                             String[] corporations = null;
		                             if(corp.getId()==c.getId()){
		                             subLocCntWithId = corp.getSubLocCntWithId().split("-");
		                             if(corporation != null ){corporations = corporation.split("-");}%>
		                             <option value="<%=corp.getSubLocCntWithId()%>" 
		                    <%=(StringUtils.isNotBlank(corporation) && corporations[0].equals(String.valueOf(subLocCntWithId[0]))) ? "selected" : "" %>><%=corp.getName()%></option>
		               <%}}}}}else{%><option value="">---------------------Please Select---------------------</option>
		               <% 
		                List<Corporation> clinicList = sm.getAllClinics();
		               if (clinicList != null && clinicList.size() > 0) {
		                     if (clinicList != null && clinicList.size() > 0) {
		                            for (Corporation corp : clinicList) {
		                             String[] subLocCntWithId = corp.getSubLocCntWithId().split("-");
		                             String[] corporations = null;
		                             if(corporation != null ){corporations = corporation.split("-");}
		                    %>
		                    <option value="<%=corp.getSubLocCntWithId()%>" 
		                    <%=(StringUtils.isNotBlank(corporation) && corporations[0].equals(String.valueOf(subLocCntWithId[0]))) ? "selected" : "" %>><%=corp.getName()%></option>
		               <%}}}}%>
          		</select>
		        <%if (errorFields != null && errorFields.containsKey("corporation")) {%>
      				<span class="help-inline"><%=Util.notNull(errorFields.get("corporation"))%></span>
    			<%}%>
            </div>
        </div>
       <%=HTML.textNumericInput("subscriptionClinicAmt","Clinic Price", "100", "20", subscriptionClinicAmt, null,errorFields,"return isNumericWithDot(event);")%>
       <%=HTML.textNumericInput("subscriptionPatientAmt","Patient Price", "100", "20", subscriptionPatientAmt, null,errorFields,"return isNumericWithDot(event);")%>
         <%if (errorFields != null && errorFields.containsKey("currencyType")) {%>
        <div class="control-group error" style="margin-top: 15px;"><label class="control-label">CurrencyType</label>
         	<div class="controls error">
        <%}else{ %>
        <div class="control-group" style="margin-top: 15px;"><label class="control-label">CurrencyType</label>
            <div class="controls">
        <%} %>
        	<select name="currencyType" id="currencyType" class="input-small">
            	<option value="">Select</option>
            	<%for (int i = 0; i < subCurrencyTypeChoices.length; i++) {%>
            	<option value="<%=subCurrencyTypeChoices[i]%>" <%=subCurrencyTypeChoices[i].equals(currencyType) ? "selected" : "" %>><%=subCurrencyTypeChoices[i]%></option>
            	<%} %>
            	</select> 
            	<%if (errorFields != null && errorFields.containsKey("currencyType")) {%>
          			<span class="help-inline"><%=Util.notNull(errorFields.get("currencyType"))%></span>
      			<%}%>
	        </div>
        </div>   
        <div class="control-group">
			<label class="control-label" for="active">Plan in Effect</label>
			<div class="controls">
				<label class="checkbox"> <input type="checkbox"
					onclick="showorhide(this);" id="active" name="active"
					value="true" <%=(active) ? " checked": ""%>>
					</label>
			</div>
		</div>
    </div>
    </div>
    <div class="row">
        <div class="span9">
            <div class="form-actions">
        		 <input type="button" value="Save" class="btn btn-primary" onclick="subConfirmCancel(event,this);">
        		 <input type="button" value="Save and Close" class="btn btn-primary" onclick="subConfirmCancel(event,this);">
           </div>
        </div>
    </div>
        </form>
<div id="confirmBox">
    <div class="message"></div>
    <span class="btn btn-primary yes">Yes</span>
    <span class="btn btn-primary no">No</span>
</div>
</body>
</html>