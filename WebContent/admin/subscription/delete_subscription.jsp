<%@page import="au.com.ourbodycorp.model.User"%>
<%@page import="au.com.ourbodycorp.model.Subscription"%>
<%@page import="au.com.ourbodycorp.model.managers.SubscriptionManager"%>
<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="java.util.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="au.com.ourbodycorp.Config" %>
<%@ page import="java.util.Date" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
	long subscriptionId = Long.parseLong(Util.notNull(request.getParameter("id"), "0"));
	SubscriptionManager sm = new SubscriptionManager();
	User u = (User) session.getAttribute("user");
	String error = null;
	String type="Subscription Plan";
	String activity_page="admin/subscription/delete_subscription.jsp";
	String corp_id="---";
	String corp_name="---";
	String corp_loc="---";
	String user_id=String.valueOf(u.getId());
	String user_name="Fiizio Admin";
	String invitor_id="---";
	String invitor_name="---";
	String flag="delete";
	String from="---";
	String to="---";
	
	if (request.getMethod().equalsIgnoreCase("post")) {
		
		if (request.getParameter("submit").startsWith("Cancel")) {
	    	if(Config.getString("https").equals("true")){
        		response.sendRedirect(Config.getString("url.secure")+"/close.jsp");
        	}else{
				response.sendRedirect(Config.getString("url")+"/close.jsp");
			}
	        return;
	    }else{
	    	if(subscriptionId > 0)
		    {
	    		Subscription s = sm.getSubscriptions(subscriptionId);
	    		int subPatientCnt = sm.getSubscripedPatientCnt(subscriptionId);
	    		List<Subscription> subLocationsLst = sm.getSubscriptionWithShareid(subscriptionId);
	    		if(s.isShareSubscription() || (subLocationsLst != null && subLocationsLst.size() > 0)){
	    			if(subLocationsLst != null && subLocationsLst.size() > 0){
	    				error = "This Subscription Plan is shared with sublocations.Please delete the sublocations to delete this plan";
	    			}else if(subPatientCnt>0){
	    				error = "Patients were already subscribed to this Plan";
	    			}else{
	    				sm.deleteSubscription(subscriptionId);
	    				try{
	    			    	Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,"Delete Subscription Plan By Fiizio System Admin:"+u.getFullName());
	    			    }catch(Exception ee){
	    			    	
	    			    }  
	    			}
	    		}else{
	    			if(subPatientCnt>0){
	    				error = "Patients were already subscribed to this Plan";
	    			}else{
	    				sm.deleteSubscription(subscriptionId);
	    				try{
	    			    	Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,"Delete Subscription Plan By Fiizio System Admin:"+u.getFullName());
	    			    }catch(Exception ee){
	    			    	
	    			    }  
	    			}
	    		}
		    }
	    }
		if(error == null){
			if(Config.getString("https").equals("true")){
				response.sendRedirect(Config.getString("url.secure")+"/close.jsp?reload=true");
			}else{
				response.sendRedirect(Config.getString("url")+"/close.jsp?reload=true");
			}
			return;
		}
	}
%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head><title>Delete Subscription Plan</title>
</head>
<body>
<div class="page-header">
    <h1>Delete Subscription Plan</h1>
</div>
<%@include file="/WEB-INF/includes/formErrorSimple.jsp"%>
<div class="row">
    <div class="span6">
        <p>
            Delete Subscription Plan. Are you sure?
        </p>
        <form action="delete_subscription.jsp" method="post">
        	<input type="hidden" name="id" value="<%=subscriptionId%>">
            <%=HTML.confirmCancel()%>
        </form>
    </div>
</div>
</body>
</html>
