<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%@ page import="au.com.ourbodycorp.Config" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="au.com.ourbodycorp.model.managers.CorporationManager" %>
<%@ page import="java.util.LinkedList" %>
<%@ page import="java.util.List" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%

	User u = (User) session.getAttribute("user");


    LibraryManager lm = new LibraryManager();
    
	if (u == null) {
	    response.sendRedirect(Config.getString("url"));
	    return;
	}
	String type="Promotion";
	String activity_page="admin/promotion/index.jsp";
	String corp_id="---";
	String corp_name="---";
	String corp_loc="---";
	String user_id=String.valueOf(u.getId());
	String user_name="Fiizio Admin";
	String invitor_id="";
    String invitor_name="---";
    String flag="view";
    String from="---";
    String to="---";
     try{
    	//Util.allActivitiesLogs("Promotion", u.getId(), "Fiizio System Admin", "View Promotion Details");
    	Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,"View Promotion Details By Fiizio System Admin:"+u.getFullName());
	}catch(Exception ee){
	
	} 
	boolean sent = false;
    String sentEmail = null;
    String promoemail = (request.getParameter("promoemail") != null) ? request.getParameter("promoemail").trim().toLowerCase() : "";
    String promocode = (request.getParameter("promocode") != null) ? request.getParameter("promocode").trim().toUpperCase() : "";
    long promoid = Long.parseLong(Util.notNull(request.getParameter("promoid"), "0"));
    
    CorporationManager cm = new CorporationManager();
    
//     if(promoemail != ""){
//     	String company = null;
//     	String validity = null;
//     	String discount = null;
//     	Connection conn = Util.getConnection();
//   		PreparedStatement pst;
//         pst = conn.prepareStatement("select name from company where email = ?");
//         pst.setString(1, promoemail);
//         ResultSet rs = pst.executeQuery();
//         if(rs.next()){ 
//         	company = rs.getString("name").replaceAll("\\s+", "").toLowerCase();
//         }
//         pst = conn.prepareStatement("select to_char(expires, 'DD-Mon-YYYY') as valid, discount from promotion where promo_code = ?");
//         pst.setString(1, promocode);
//         ResultSet rs1 = pst.executeQuery();
//         if(rs1.next()){ 
//         	validity = rs1.getString("valid");
//         	discount = rs1.getString("discount");
//         }
//     	cm.sendPromotion(promoemail, promocode, company, validity, discount);
//     	session.setAttribute("pemail", promoemail);
//     	session.setAttribute("sent", true);
//     	pst.close();
//     	conn.close();
//     	Promotion p;
        
//         	p = new Promotion();
//             p.setId(promoid);
//             p.setSent(new Date());
//         	lm.savePromotionDetails(p);
        	
//     	response.sendRedirect("");
//     	return;
//     }
    
	SimpleDateFormat sdf = new SimpleDateFormat("d MMM yy hh:mm");

    int currentPage = 1;
    try {
        currentPage = Integer.parseInt(request.getParameter("page"));
    } catch (NumberFormatException nfe) {
        // ignore
    }

    String query = (request.getParameter("query") != null && request.getParameter("query").trim().length() > 0) ? request.getParameter("query") : null;

    int perPage = 15;
    int count = 0;

    if (query != null) {
        count = lm.findPromotionsCount(query);
    } else {
        count = lm.countPromotions();
    }


    int pages = count / perPage;
    if (count % perPage > 0) pages++;

    if (currentPage > pages + 1) {
        currentPage = pages;
    }

    List<Promotion> promotions;
    if (query != null) {
    	promotions = lm.getPromotions(query, (currentPage - 1) * perPage, perPage);
    } else {
    	promotions = lm.getPromotions((currentPage - 1) * perPage, perPage);
    }


    String[][] breadcrumbs = {{"/admin/", "Admin"}, {"/admin/promotion/", "Promotion"}};

    if (query != null) {
        breadcrumbs = new String[][]{{"/admin/", "Admin"}, {"/admin/promotion/", "Promotion"}, {null, "Search Results"}};
    }

    LinkedList<String> pageList = new LinkedList<String>();
    for (int i = 1; i <= pages; i++) {
        if (query != null) {
            pageList.add("?query=" + query + "&page=" + i);
        } else {
            pageList.add("?page=" + i);
        }

    }


%>
<html>
<head><title>Promotion</title></head>
<body>
<div class="page-header"><h1>Promotion</h1></div>
<%@include file="/WEB-INF/includes/breadcrumbs.jsp"%>
<div class="row">
<div class="span2">
        <a href="inline/add_promotion.jsp" class="btn btn-primary fancybox"><i class="icon-plus icon-white"></i> Add Promotion</a>
    </div>
    <div class="span4">  &nbsp; </div>
<div class="span3">
        <form class="form-search" method="get" action="/admin/promotion/">
        <input type="text" class="input-medium search-query" name="query" value="<%=Util.formValue(query)%>">
        <button type="submit" class="btn">Search</button>
        </form>
    </div>
    </div>
    <%if (session.getAttribute("sent") != null) {
             if (session.getAttribute("sent").toString().equals("true")) {
                %>
                <div class="alert alert-success">Promotion sent to <%=session.getAttribute("pemail").toString()%>.</div>
                <%
                session.setAttribute("sent", null);
                session.setAttribute("pemail", null);}}
                %>
    <div class="row">
        <div class="span9 columns">
    <table class="table table-zebra-striped">
        <tr>
        <th>Promo Code</th>
        <th>Expires</th>
        <th>Discount(%)</th>
<!--         <th>Email</th> -->
        <th>Sent</th>
        <th>Status</th>
        <th>Send</th>
        <th>Delete</th>
        </tr>
        <tbody>
        <%
            for (Promotion promotion : promotions) {
                String status = "Not Send";
                if (promotion.getSent() != null) {
                    status = "Sent";
                } if (promotion.getAccepted() != null) {
                    status = "<a href=\"/admin/clients/company.jsp?id=" + promotion.getCompany() + "\">Used</a> " + sdf.format(promotion.getAccepted());
                } else if (promotion.getExpires().before(new Date())) {
                    status = "Expired";
                }
        %>
        <tr>
            <td><%=promotion.getPromo_code()%></td>
            <td><%=sdf.format(promotion.getExpires())%></td>
            <td style="text-align:center"><%=promotion.getDiscount()%>%</td>
<%--             <td><%=promotion.getEmail()%></td> --%>
            <td><%if(promotion.getSent() != null){ %><%=sdf.format(promotion.getSent())%><%} %></td>
            <%if(status == "Expired"){ %>
            <td style="color: darkred;"><%=status%></td>
            <%}else{ %>
            <td><%=status%></td>
            <%} %>
            <%if(status == "Sent" || status == "Not Send"){ %>
            <td><a class="btn btn-primary btn-mini fancybox" href="inline/email.jsp?promocode=<%=promotion.getPromo_code()%>&promoid=<%=promotion.getId()%>">
						<i class="icon icon-share-alt icon-white"></i>
						Send
					</a></td>
			<%}else{ %>
			<td><a class="btn btn-primary btn-mini" disabled="disabled">
						<i class="icon icon-share-alt icon-white"></i>
						Send
					</a></td>
			<%} %>
            <td><a class="btn btn-danger btn-mini fancybox" href="inline/delete_promotion.jsp?id=<%=promotion.getId()%>">
						<i class="icon icon-trash icon-white"></i>
						Delete
					</a></td>
        </tr>
        <%
            }
        %>
        </tbody>
    </table>

	<%=HTML.pages(pageList, currentPage)%>
                </div>
    </div>
</body>
</html>