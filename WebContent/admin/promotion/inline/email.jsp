<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@ page import="au.com.ourbodycorp.Config" %>
<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.MailMessage" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="au.com.ourbodycorp.model.managers.CorporationManager" %>
<%@ page import="java.util.Properties" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%

	User u = (User) session.getAttribute("user");

    String email = request.getParameter("email");
    String promocode = (request.getParameter("promocode") != null) ? request.getParameter("promocode").trim() : "";
    long promoid = Long.parseLong(Util.notNull(request.getParameter("promoid"), "0"));

    String error = null;

    if (request.getMethod().equalsIgnoreCase("post")) {
    	if (request.getParameter("submit").equals("Cancel")) {
        	if(Config.getString("https").equals("true")){
        		response.sendRedirect(Config.getString("url.secure")+"/close.jsp");
        	}else{
				response.sendRedirect(Config.getString("url")+"/close.jsp");
			}
            return;
        }
        if (email.length() == 0) {
        	error = "Please enter an email address";
        } else if (!email.matches("[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}")) {
        	error = "Please enter a valid email address";
        }      
        
        
        if (error == null) {
        	CorporationManager cm = new CorporationManager();
        	LibraryManager lm = new LibraryManager();
        	
        	String company = null;
        	String validity = null;
        	String discount = null;
        	Connection conn = Util.getConnection();
      		PreparedStatement pst;
            pst = conn.prepareStatement("select to_char(expires, 'DD-Mon-YYYY') as valid, discount from promotion where promo_code = ?");
            pst.setString(1, promocode);
            ResultSet rs1 = pst.executeQuery();
            if(rs1.next()){ 
            	validity = rs1.getString("valid");
            	discount = rs1.getString("discount");
            }
        	cm.sendPromotion(email, promocode, company, validity, discount);
        	
        	session.setAttribute("pemail", email);
        	session.setAttribute("sent", true);
        	pst.close();
        	conn.close();
        	Promotion p;
            
            	p = new Promotion();
                p.setId(promoid);
                p.setSent(new Date());
                lm.savePromotionDetails(p);
                
                String type="Promotion";
            	String activity_page="admin/promotion/inline/email.jsp";
            	String corp_id="---";
            	String corp_name="---";
            	String corp_loc="---";
            	String user_id=String.valueOf(u.getId());
            	String user_name="Fiizio Admin";
            	String invitor_id=String.valueOf(promoid);
            	String invitor_name="---";
            	String flag="sent";
            	String from="---";
            	String to="---";
            	try{
            		//Util.allActivitiesLogs("Promotion", u.getId(), "Fiizio System Admin", "Email Promotion Code");
                	Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,"Email Promotion Code to "+email+" By Fiizio System Admin:"+u.getFullName());
            	}catch(Exception ee){
            		
            	}
        	
        	if(Config.getString("https").equals("true")){
        		response.sendRedirect(Config.getString("url.secure")+"/close.jsp?reload=true");
        	}else{
        		response.sendRedirect(Config.getString("url")+"/close.jsp?reload=true");
        	}
            return;
        }
    }


%>
<html>
<head><title>Send Email</title>
</head>
<body>
<div class="page-header">
    <h1>Send Email</h1>
</div>
<%@include file="/WEB-INF/includes/formErrorSimple.jsp"%>
<div class="row">
    <div class="span9">
        <form action="email.jsp" method="post" class="form-horizontal">
            <input type="hidden" name="promocode" value="<%=promocode%>">
            <input type="hidden" name="promoid" value="<%=promoid%>">
                   <%=HTML.textInput("email", "Email Address", "100", "20", email)%>
                   <%=HTML.submitButton("Send Email")%>
        </form>
    </div>
</div>
</body>
</html>