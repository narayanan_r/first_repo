<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="java.util.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="au.com.ourbodycorp.Config" %>
<%@ page import="au.com.ourbodycorp.model.User" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
	
	long promotionid = Long.parseLong(Util.notNull(request.getParameter("id"), "0"));
User u = (User) session.getAttribute("user");

	if (request.getMethod().equalsIgnoreCase("post")) {
	    if (request.getParameter("submit").startsWith("Cancel")) {
	    	if(Config.getString("https").equals("true")){
        		response.sendRedirect(Config.getString("url.secure")+"/close.jsp");
        	}else{
				response.sendRedirect(Config.getString("url")+"/close.jsp");
			}
	        return;
	    }
	String type="Promotion";
	String activity_page="admin/promotion/inline/delete_promotion.jsp";
	String corp_id="---";
	String corp_name="---";
	String corp_loc="---";
	String user_id=String.valueOf(u.getId());
	String user_name="Fiizio Admin";
	String invitor_id=String.valueOf(promotionid);
	String invitor_name="---";
	String flag="delete";
	String from="---";
	String to="---";
	try{
		//Util.allActivitiesLogs("Promotion", u.getId(), "Fiizio System Admin", "Delete Promotion");
    	Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,"Delete Promotion By Fiizio System Admin:"+u.getFullName());
	}catch(Exception ee){
		
	}
    Connection conn = Util.getConnection();
	PreparedStatement pst;
    pst = conn.prepareStatement("delete from promotion where id = ?");
    pst.setLong(1, promotionid);
    pst.executeUpdate();
    pst.close();
	conn.close();
	
	if(Config.getString("https").equals("true")){
		response.sendRedirect(Config.getString("url.secure")+"/close.jsp?reload=true");
	}else{
		response.sendRedirect(Config.getString("url")+"/close.jsp?reload=true");
	}
	return;
	}
	
%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head><title>Delete Promotion</title>
</head>
<body>
<div class="page-header">
    <h1>Delete Promotion</h1>
</div>
<div class="row">
    <div class="span6">
        <p>
            Delete Promotion. Are you sure?
        </p>
        <form action="delete_promotion.jsp" method="post">
        	<input type="hidden" name="id" value="<%=promotionid%>">
            <%=HTML.confirmCancel()%>
        </form>
    </div>
</div>
</body>
</html>
