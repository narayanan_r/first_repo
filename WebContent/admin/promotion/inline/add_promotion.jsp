<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.PlanPrice" %>
<%@ page import="au.com.ourbodycorp.model.User" %>
<%@ page import="java.math.*" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="au.com.ourbodycorp.model.Promotion" %>
<%@ page import="java.lang.ref.ReferenceQueue" %>
<%@ page import="au.com.ourbodycorp.Config" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%

	User u = (User) session.getAttribute("user");

    LibraryManager lm = new LibraryManager();
	SimpleDateFormat sf = new SimpleDateFormat("dd/MM/yyyy");
	Promotion p = null;
    String code = null;
    /* if (!request.getMethod().equalsIgnoreCase("post")) {
    do {
        code = Util.randomString(10);
        p = lm.getPromotion(code);
        session.setAttribute("promocodeunique", code);
    }
    while (p != null);
    } */
    
    String promo_code = Util.notNull(request.getParameter("promo_code"));//session.getAttribute("promocodeunique").toString();
	String expires = Util.notNull(request.getParameter("date"));
    String discount = Util.notNull(request.getParameter("discount"));
    String email = (request.getParameter("email") != null) ? request.getParameter("email").trim().toLowerCase() : "";
    Date expiresdate = new Date();
    if(expires != ""){
    	expiresdate = sf.parse(expires);
    }
    String title = "Add";
    String error = null;

    if (request.getMethod().equalsIgnoreCase("post")) {
        if (request.getParameter("submit").equals("Cancel")) {
        	if(Config.getString("https").equals("true")){
        		response.sendRedirect(Config.getString("url.secure")+"/close.jsp");
        	}else{
				response.sendRedirect(Config.getString("url")+"/close.jsp");
			}
            return;
        }
        Connection conn = Util.getConnection();
        PreparedStatement pst = conn.prepareStatement("select promo_code from promotion where promo_code = ?");
        pst.setString(1, promo_code);
        ResultSet rs = pst.executeQuery();
        if(rs.next()){
        	error = "This promo code already exist.";
	   	} 
        else if (promo_code.trim().length() < 9) {
        	error = "Please enter a promo code atleast 10 character";
        } else if (expires.trim().length() == 0) {
        	error = "Please select a expires";
        } else if(discount.trim().length() == 0){
        	error = "Please enter a discount";
        } 
        
        rs.close();
        pst.close();
        conn.close();
        
        /* else if (email.length() == 0) {
        	error = "Please enter an email address";
        } else if (!email.matches("[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}")) {
        	error = "Please enter a valid email address";
        } else if (email.length() != 0){
        	Connection conn = Util.getConnection();
      		PreparedStatement pst;
            pst = conn.prepareStatement("select email from users where email = ?");
            pst.setString(1, email);
            ResultSet rs = pst.executeQuery();
            if(rs.next()){ 
            	error = null;
            }else{
            	error = "Email does not exist";
            }
        	pst.close();
        	conn.close();
        } */
        
        
        
        if (error == null) {
        	p = new Promotion();
            p.setPromo_code(promo_code);
            p.setExpires(expiresdate);
            p.setDiscount(Long.parseLong(discount));
            p.setEmail(email);
            p.setCreator(u.getId());
        	p.setCreated(new Date());
        	lm.savePromotionDetails(p);
        	String type="Promotion";
        	String activity_page="admin/promotion/inline/add_promotion.jsp";
        	String corp_id="---";
        	String corp_name="---";
        	String corp_loc="---";
        	String user_id=String.valueOf(u.getId());
        	String user_name="Fiizio Admin";
        	String invitor_id=String.valueOf(p.getId());
        	String invitor_name="---";
        	String flag="add";
        	String from="---";
        	String to="---";
        	try{
        		//Util.allActivitiesLogs("Promotion", u.getId(), "Fiizio System Admin", "Add New Promotion");
            	Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,"Add New Promotion By Fiizio System Admin:"+u.getFullName());
        	}catch(Exception ee){
        		
        	}
        	if(Config.getString("https").equals("true")){
        		response.sendRedirect(Config.getString("url.secure")+"/close.jsp?reload=true");
        	}else{
        		response.sendRedirect(Config.getString("url")+"/close.jsp?reload=true");
        	}
            return;
        }

    } 
%>
<html>
<head>
    <title><%=title%> Promotion</title>
    <script type="text/javascript">
    $(document).ready(function(){
    	$("#date").datepicker({
    	     dateFormat:"dd/mm/yy",
    	     minDate: 0
    	  });
	});
    function isNumeric(evt) {
    	evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    };
    </script>
    <style type="text/css">
    .input-mini{
    	width: 78px;
    }
    </style>
</head>
<body>
<%@include file="/WEB-INF/includes/formErrorSimple.jsp"%>
<div class="page-header">
    <h1><%=title%> Promotion</h1>
</div>
<div class="row">
    <div class="span6">
        <form action="add_promotion.jsp" method="post" class="form-horizontal">
            <div class="control-group">
            <label class="control-label" for="promo_code">Promo Code</label>
            <div class="controls">
              <label class="promo_code">
           			<input type="text" class="input-xlarge" name="promo_code" id="promo_code" value="<%=promo_code%>" maxlength="20"/>
              </label>
            </div>
            </div>
            <div class="control-group">
            <label class="control-label" for="expires">Expires</label>
            <div class="controls">
              <label class="expires">
           			<input type="text" class="input-xlarge" name="date" id="date" value="<%=expires%>" readonly="readonly"/>
              </label>
            </div>
            </div>
            <%=HTML.textInputNumeric("discount", "Discount(%)", "2", "2", discount, null, false, "return isNumeric(event);")%>
<%--             <%=HTML.textInput("email", "Email", "30", "100", email)%> --%>
            <%=HTML.saveCancel()%>
        </form>
    </div>
</div>
</body>
</html>