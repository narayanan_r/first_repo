// JavaScript Document

$(document).ready(function(){ 

	//fix png transparency for ie 5/6
	$("img").pngfix();
	
	//main navigation rollovers
	$("#nav li a").not($(".active"))
		.css( {backgroundPosition: "50% 40px"} )
		.mouseover(function() {  
                $(this).stop().animate({ backgroundPosition: "50% 31px" }, {duration:400, easing:"easeOutCubic"})
                })
		.mouseout(function() {  
                $(this).stop().animate({ backgroundPosition: "50% 40px" }, {duration:300, easing:"easeInCubic"});
                }); 
	
	/************* footer scripts */
	
	//form label plugin
	$("#enews label").inFieldLabels({ fadeOpacity:0.3, fadeDuration:500, fadeEasing:"easeOutCubic"});		
		
	//twitter feed
	$(".tweets").tweet({
            username: "adelaidefest",
            join_text: "auto",
            count: 2,
            auto_join_text_default: "we said,",
            auto_join_text_ed: "we",
            auto_join_text_ing: "we were",
            auto_join_text_reply: "we replied to",
            auto_join_text_url: "we were checking out",
            loading_text: "loading tweets...",
			refresh_interval:60
        });
	
	//main navigation rollovers
	$("#footer-links a img")
		.mouseover(function() {  
                $(this).stop().animate({ opacity: "0.5" }, {duration:250, easing:"easeOutCubic"})
                })
		.mouseout(function() {  
                $(this).stop().animate({ opacity: "1" }, {duration:500, easing:"easeOutCubic"});
                }); 
				
}); 



