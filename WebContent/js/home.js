// JavaScript Document

$(document).ready(function(){ 
	
	//home page slideshow
	$("ul#slideshow").cycle({
		fx: 'fade',
		pause: 0,
		speed:1500,
		timeout:10000,
		slideResize: 0,
		easeIn: 'easeInOutExpo',
		easeOut: 'easeInOutExpo',
		width: 'auto',
		fit: 0, 
        containerResize: 0,
		random: true
	});
	
	//home page new carousel
	 $("#news").jCarouselLite({
        btnNext: ".next",
        btnPrev: ".back",
		easing:"easeInOutCubic",
		visible:4,
		scroll:4,
		speed:750
    });
	
	//news item rollovers
	$("#news li")
		.mouseover(function() {  
                $(this).find("img").stop().animate({ opacity: "0.6" }, {duration:250, easing:"easeOutCubic"})
                })
		.mouseout(function() {  
                $(this).find("img").stop().animate({ opacity: "1" }, {duration:500, easing:"easeOutCubic"});
                }); 
	
}); 

