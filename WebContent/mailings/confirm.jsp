<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="au.com.ourbodycorp.model.Spam" %>
<%@ page import="au.com.ourbodycorp.model.managers.SpamManager" %><%
    long id = Long.parseLong(request.getParameter("i"));
    long c = Long.parseLong(request.getParameter("c"));
    SpamManager sm = new SpamManager();
    Spam spam = sm.getById(id);
    if (spam != null && (spam.getConfirm() == c || spam.getConfirm() == -1)) {
        if (spam.getConfirm() != -1) {
            sm.submitSpam(spam);
            spam.setConfirm(-1);
            sm.save(spam);
        }
        String url = "/contact/enews/thanks";
        if (spam.isOptInPost()) {
            url = "/contact/program/thanks";
        }
        response.sendRedirect(url);
        return;
    }
%>
<html>
<head><title>Subscription not found</title></head>
<body>
<h2>Subscription not found</h2>

<div class="box-2thirds">
    <hr class="top"/>
    <div class="content">
    <p class="intro">The subscription you are trying to confirm has not been found.</p>
    <p>
        Please try subscribing again.
    </p>
    </div>

</div>

<jsp:include page="/WEB-INF/includes/sidenav.jsp"/>

</body>
</html>