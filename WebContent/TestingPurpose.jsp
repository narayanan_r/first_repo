<html>
  <head>
    <title>Testbox</title>
  </head>  

<body>


<!-- Test Box javascript + html  START ... -->

<style type="text/css">
.input-xlarge {
  width: 270px;
  height: 30px;
}

.blue-border {
    border-color:#1E73BE;
    border-style: solid;
    border-width:5px;
}

#verify {
    width: 20em;  height: 3em;
}
#status {
    color: #ff0000;    
}

</style>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
<!--    

    var emailStatus = ""; // variable containing status of email
    var additionalStatus = "";
    var checkingEmail = false; // variable set to true when we are checking           

    // Kick off the email checker when the user moves out of the email input field
    function call_api() {
      
        /*  $("#email-status").html("");
      
        // get the email address they have entered
        var email = $("#email-test").val();
        
        if(email == '')
            return false;
        
        // Tell the user we are checking ...
        $("#email-status").html("Checking ...");
                    
        // call the api                
        var callUrl = "https://api.emailyoyo.com/api/b/v1?email=" + encodeURIComponent(email) + "&seed="+Math.random();                
        checkingEmail = true;
        
        $.getJSON(callUrl, function(data) {    
            display_result(data);               
        });
                     
        return false;  */ 
       var email = $("#email-test").val();
        /* $.ajax({
            type:"GET",
            url:"https://api.emailyoyo.com/api/b/v1?email=" + encodeURIComponent(email) + "&seed="+Math.random()+"",
            headers:{"origin":"https://emailyoyo.com"},
            success:function(response){
                alert(response);}}); */
       var main_url ="https://api.emailyoyo.com/api/b/v1?email=" + encodeURIComponent(email) + "&seed="+Math.random()+"";var referrer ="https://emailyoyo.com";
       $.ajax({
         url: main_url,
         dataType:"json",
         headers:{'X-Alt-Referer': referrer },
         success:function(data){
           console.log(data);}});

    }
    function display_result(data){
        // update the status        
        var statusMessage = "<b>Result: <span id='status'>" + data['status'] + "</span></b> <br><br><b>Advanced Reason: " + data['additionalStatus'] + "</b>";  

        var emailStatus = data['status'].toUpperCase();      
        $("#email-status").html(statusMessage);
        checkingEmail = false;        
        additionalStatus = data['additionalStatus'];                    
    }

	// Form validation field
    function validateForm() {        
        call_api();        
        return false;
    }
	
-->
</script>


<form onSubmit="return validateForm();">    
    <fieldset class="blue-border">  
        <legend >Enter a email address in the box below to test our validation service.</legend>                  
        <br>
        <div class="control-group">  
            <label class="control-label" for="email-create">Email Address:</label>  
            <div class="controls">  
              <input type="text" class="input-xlarge" id="email-test" name="email" value="" >  
              <p class="help-block" id="email-status"></p>               
            </div>  
        </div>          
        <input type="submit" name="submit" id="verify" value="Click Here To Verify"/> 
    </fieldset>      
</form>  

<!-- ... END Test Box javascript + html -->

</body>
</html>