<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="decorator" uri="http://www.opensymphony.com/sitemesh/decorator" %>
<%@ page import="au.com.ourbodycorp.Config" %>
<html>
<head><title><decorator:title /> - <%=Config.getString("site.name")%></title>
    <decorator:head/>
</head>
<body <decorator:getProperty property="body.onload"  writeEntireProperty="true"/>>


<decorator:body/>

</body>
</html>