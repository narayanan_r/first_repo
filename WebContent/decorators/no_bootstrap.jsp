<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="decorator" uri="http://www.opensymphony.com/sitemesh/decorator" %>
<%@ page import="au.com.ourbodycorp.Config" %>
<html>
<head><title><decorator:title /> - <%=Config.getString("site.name")%></title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
<!--link href="/styles/styles.css" rel="stylesheet" type="text/css" /-->



    <!-- Le styles -->
    <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/bootstrap/css/bootstrap-responsive.min.css">

    <script src="/js/jquery-1.7.1.min.js"></script>
    <script src="/js/jquery-ui.min.js"></script>
    <script src="/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="/fancybox/source/jquery.fancybox.css?v=2.0.5" type="text/css" media="screen" />
    <script type="text/javascript" src="/fancybox/source/jquery.fancybox.pack.js?v=2.0.5"></script>


    <!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <link href="/jquery-ui/css/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet">

    <decorator:head/>
</head>
<body <decorator:getProperty property="body.onload"  writeEntireProperty="true"/>>
<decorator:body/>

</body>
</html>