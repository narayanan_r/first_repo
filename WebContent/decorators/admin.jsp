<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="decorator" uri="http://www.opensymphony.com/sitemesh/decorator" %>
<%@ page import="au.com.ourbodycorp.Config" %>
<html>
<head><title><decorator:title /><%=Config.getString("site.name")%> Admin</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
<!--link href="/styles/styles.css" rel="stylesheet" type="text/css" /-->
	<link href="/images/app/fiizio_favicon.png" rel="shortcut icon" type="image/vnd.microsoft.icon" />


    <!-- Le styles -->
    <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/bootstrap/css/bootstrap-responsive.min.css">
    <link rel="stylesheet" href="/bootstrap/css/jquery.dataTables.min.css" />

    <script src="/js/jquery-1.7.1.min.js"></script>
    <script src="/js/jquery-ui.min.js"></script>
    <script src="/bootstrap/js/bootstrap.min.js"></script>
    <script src="/bootstrap/js/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" href="/fancybox/source/jquery.fancybox.css?v=2.0.5" type="text/css" media="screen" />
    <script type="text/javascript" src="/fancybox/source/jquery.fancybox.pack.js?v=2.0.5"></script>


    <!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <link href="/jquery-ui/css/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet">


    <style type="text/css">
        body {
            padding-top: 50px;
        }
        textarea {
    resize: none;
}
    </style>
    <script type="text/javascript">
        $(document).ready(function() {
		    $(".fancybox").fancybox({
                'type' : 'iframe',
                'width' : 550,
                'height': 400,
                'minHeight' : 400,
                'margin' : new Array(60,10,10,10)
            });
	    });
    </script>
    <script type="text/javascript">
	//Set timeout variables.
	var minutes = 20;
	var milliseconds = minutes * 60000;
	var timoutNow = milliseconds;// Timeout in 25 mins. //Changed for Checking
// 	var logoutUrl = 'http://localhost:8080/users/login.jsp'; // URL to logout page.
	
	var timeoutTimer;
	
	$(document).ready(function() {
		// Start timers.
		    timeoutTimer = setTimeout("IdleTimeout()", timoutNow);
	});
	
	function StartTimers(){
		timeoutTimer = setTimeout("IdleTimeout()", timoutNow);
	}
	
	// Reset timers.
	$(document).ready(function() {
	$('a').click(function(evt) {
        clearTimeout(timeoutTimer);
	    StartTimers();
	    $("#timeout").dialog('close');
    });
	$('button').click(function(evt) {
        clearTimeout(timeoutTimer);
	    StartTimers();
	    $("#timeout").dialog('close');
    });
	});
	function ResetTimers() {
	    clearTimeout(timeoutTimer);
	    StartTimers();
	    $("#timeout").dialog('close');
	}
	
	// Logout the user.
	function IdleTimeout() {
		function disableF5(e) { if ((e.which || e.keyCode) == 116) e.preventDefault(); };
		$(document).on("keydown", disableF5);
// 	    window.location = logoutUrl;
		$.fancybox({
            'width': '45%',
            'height': '63%',
            'autoScale': true,
            'transitionIn': 'fade',
            'transitionOut': 'fade',
            'type': 'iframe',
            'showCloseButton': false,
            "modal": true,
            'hideOnOverlayClick' : true, // prevents closing clicking OUTSIE fancybox
            'hideOnContentClick' : true, // prevents closing clicking INSIDE fancybox
            'enableEscapeButton' : false,  // prevents closing pressing ESCAPE key
            'href': '<%if(Config.getString("https").equals("true")){%><%=Config.getString("url.secure")%><%}else{%><%=Config.getString("url")%><%}%>/users/inline/expiry.jsp',
<%-- 			'href': '<%if(Config.getString("https").equals("true")){%>https://<%=Config.getString("baseDomain")%><%}else{%><%=Config.getString("url")%><%}%>/users/inline/expiry.jsp', --%>
            'helpers'     : { 
                'overlay' : {'closeClick': false }
                }
        });
	}
</script>
    <decorator:head/>
</head>
<body <decorator:getProperty property="body.onload"  writeEntireProperty="true"/>>

    <jsp:include page="/WEB-INF/includes/admin/topnav.jsp"/>

<div class="container-fluid" onkeyup="ResetTimers();">
    <div class="row-fluid">
        <div class="span2">
            <div class="well sidebar-nav">
<%@include file="/WEB-INF/includes/admin/sidebar.jsp"%>
            </div>
        </div>
<div class="span10">
<decorator:body/>
</div>
        </div>
    </div>

</body>
</html>