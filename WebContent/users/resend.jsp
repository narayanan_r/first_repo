<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.model.User" %>
<%@ page import="au.com.ourbodycorp.model.managers.UserManager" %>
<%@ page import="java.util.HashMap" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String email = request.getParameter("email");
    String error = null;
    HashMap<String, String> errorFields = new HashMap<String, String>();
    if (email != null) {
        UserManager um = new UserManager();
        User u = um.getUserByEmail(email);
        if (u != null) {
            if (u.getVerifyKey() != null) {
                um.sendVerificationEmail(u);
                response.sendRedirect("resent.jsp");
                return;
            } else {
                response.sendRedirect("/");
                return;
            }
        } else {
            errorFields.put("email", "User not found");
        }
    }
%>
<html>
<head><title>Resend verification email</title></head>
<body>
<div class="page-header">
    <h1>Resend verification email</h1>
</div>

<div class="row">
    <div class="span4 columns">
        <p>Didn't receive the account activation email?</p>
        <p>
            Be sure to also check you spam or junk mail folder as it may have ended up in there.
        </p>
    </div>
    <div class="span12 columns">
        <%@ include file="/WEB-INF/includes/formError.jsp" %>
        <form action="resend.jsp" method="post">
            <fieldset>
            <%=HTML.textInput("email", "Email Address", "30", "50", email, null, errorFields)%>
                </fieldset>
          <fieldset>

          <div class="clearfix">
            <label id="optionsRadio">&nbsp;</label>
            <div class="input">
              <button type="submit" class="btn primary">Submit</button>
            </div>
              </div>

          </fieldset>
</form>
    </div>
</div>

</body>
</html>