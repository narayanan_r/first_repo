<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.User" %>
<%@ page import="au.com.ourbodycorp.model.managers.UserManager" %>
<%@ page import="java.util.HashMap" %>
<%
    User u = (User) session.getAttribute("user");
    String from="Title: "+u.getTitle()+";First Name: "+u.getFirstName()+";Last Name: "+u.getLastName()+";";

    if (u == null) {
        response.sendRedirect("/close.jsp");
        return;
    }
   /*  try{
    	Util.allActivitiesLogs("My Details", u.getId(), "No Corporation", "Change Name");
    }catch(Exception e){
    	
    } */
    UserManager um = new UserManager();
    u = um.getUser(u.getId());

    String firstName = request.getParameter("firstName");
    String lastName = request.getParameter("lastName");
    String title = request.getParameter("title");

    HashMap<String, String> errorFields = new HashMap<String, String>();
    if (request.getMethod().equalsIgnoreCase("post")) {
        if (Util.notNull(title).length() == 0) {
            errorFields.put("title", "Title required");
        }
        if (Util.notNull(firstName).length() == 0) {
            errorFields.put("firstName", "First name required");
        }
        if (Util.notNull(lastName).length() == 0) {
            errorFields.put("lastName", "Last name required");
        }

        try {
            title = Util.safeInput(title, true);
            firstName = Util.safeInput(firstName, true);
            firstName = Util.safeInput(firstName, true);
        } catch (Exception e) {
            errorFields.put("safeInput", "The characters &gt; and &lt; are not allowed.");
        }

        if (errorFields.size() == 0) {
            u.setTitle(title);
            u.setFirstName(firstName);
            u.setLastName(lastName);
            um.saveUser(u);
            
            String type="My Details";
            String activity_page="users/inline/name.jsp";
        	String corp_id="No Corporation";
        	String corp_name="----";
        	String corp_loc="---";
        	String user_id=String.valueOf(u.getId());
        	String user_name=u.getFullName();
        	String invitor_id="---";
        	String invitor_name="---";
        	String flag="update";
        	String to="Title: "+request.getParameter("title")+";First Name: "+request.getParameter("firstName")+";Last Name: "+request.getParameter("lastName")+";";
        	String desc="Change Name for "+u.getFullName()+" By User";
            try
            {		
             Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
            }
            catch(Exception e)
            {
            }
            session.setAttribute("user", um.getUser(u.getId()));
            response.sendRedirect("/close.jsp?reload=true");
            return;
        }

    } else {
        title = Util.notNull(u.getTitle());
        firstName = u.getFirstName();
        lastName = u.getLastName();
    }
%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head><title>Change Name</title></head>
<body>
<div class="page-header">
    <h1>Change Name</h1>
</div>
<div class="row">
    <div class="span5">
        <%@ include file="/WEB-INF/includes/formError.jsp" %>
        <form action="name.jsp" method="post" class="form-horizontal">
            <fieldset>
            <%=HTML.select("title", "Title", User.titles, User.titles, title, null, errorFields, true)%>
            <%=HTML.textInput("firstName", "First Name", "30", "50", firstName, null, errorFields)%>
          <%=HTML.textInput("lastName", "Last Name", "30", "50", lastName, null, errorFields)%>
                </fieldset>
          <%=HTML.submitButton("submit")%>
</form>
    </div>
</div>


</body>
</html>