<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.Constants" %>
<%@ page import="au.com.ourbodycorp.model.Country" %>
<%@ page import="au.com.ourbodycorp.model.User" %>
<%@ page import="au.com.ourbodycorp.model.managers.CountryManager" %>
<%@ page import="au.com.ourbodycorp.model.managers.UserManager" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.List" %>
<%
    User u = (User) session.getAttribute("user");
    if (u == null) {
        response.sendRedirect("/close.jsp");
        return;
    }
    /* try{
    	//Util.allActivitiesLogs("My Details", u.getId(), "No Corporation", "Change Contact Details");
    }catch(Exception e){
    	
    } */
    Constants.State states[] = Constants.State.values();

    UserManager um = new UserManager();
    u = um.getUser(u.getId());
	String from="Address line 1: "+u.getAddress1()+";Address line 2: "+u.getAddress2()+";City / Suburb: "+u.getSuburb()+";State: "+u.getState()+";Postcode:"+u.getPostcode()+";Country: "+u.getCountry()+";Phone: "+u.getPhone()+";Mobile: "+u.getMobile()+";";
    String address1 = (request.getParameter("address1") != null) ? request.getParameter("address1").trim() : "";
    String address2 = (request.getParameter("address2") != null) ? request.getParameter("address2").trim() : "";
    String suburb = (request.getParameter("suburb") != null) ? request.getParameter("suburb").trim() : "";
    String state = (request.getParameter("state") != null) ? request.getParameter("state").trim() : "";
    String manualState = (request.getParameter("manualState") != null) ? request.getParameter("manualState").trim() : "";
    String postcode = (request.getParameter("postcode") != null) ? request.getParameter("postcode").trim() : "";
    String country = (request.getParameter("country") != null) ? request.getParameter("country").trim() : "AU";
    String phone = (request.getParameter("phone") != null) ? request.getParameter("phone").trim() : "";
    String mobile = (request.getParameter("mobile") != null) ? request.getParameter("mobile").trim() : "";

    if (!country.equals("AU")) {
        state = manualState;
    }

    HashMap<String, String> errorFields = new HashMap<String, String>();
    if (request.getMethod().equalsIgnoreCase("post")) {

        if (country.equals("")) {
            errorFields.put("country", "Please select a country");
        }

        if (country.equals("AU") && (state.length() == 0 || state.equals("XX"))) {
            errorFields.put("state", "Please select an Australian state");
        }

        if (phone.length() < 8 && mobile.length() < 8) {
            errorFields.put("phone", "Please enter at least one phone number");
            errorFields.put("mobile", "Please enter at least one phone number");
        }

        if (address1.length() < 5) {
            errorFields.put("address1", "Please enter an address");
            errorFields.put("address2", "");
        }

        if (suburb.length() < 5) {
            errorFields.put("suburb", "Please enter a city / suburb");
        }

        if (country.equals("AU") && !postcode.matches("[0-9]{4}")) {
            errorFields.put("postcode", "Please enter a valid Australian postcode");
        }

        try {
            address1 = Util.safeInput(address1, true);
            address2 = Util.safeInput(address2, true);
            suburb = Util.safeInput(suburb, true);
            state = Util.safeInput(state, true);
            postcode = Util.safeInput(postcode, true);
            phone = Util.safeInput(phone, true);
            mobile = Util.safeInput(mobile, true);
            manualState = Util.safeInput(manualState, true);
        } catch (Exception e) {
            errorFields.put("safeInput", "The characters &gt; and &lt; are not allowed.");
        }

        if (errorFields.size() == 0) {
            u.setAddress1(address1);
            u.setAddress2(address2);
            u.setSuburb(suburb);
            u.setState(state);
            u.setPostcode(postcode);
            u.setPhone(phone);
            u.setMobile(mobile);
            u.setCountry(country);

            um.saveUser(u);
            session.setAttribute("user", um.getUser(u.getId()));
            
            String type="My Details";
            String activity_page="users/inline/contact.jsp";
        	String corp_id="No Corporation";
        	String corp_name="----";
        	String corp_loc="---";
        	String user_id=String.valueOf(u.getId());
        	String user_name=u.getFullName();
        	String invitor_id="---";
        	String invitor_name="---";
        	String flag="update";
        	String to="Address line 1: "+request.getParameter("address1")+";Address line 2: "+request.getParameter("address2")+";City / Suburb: "+request.getParameter("suburb")+";State: "+request.getParameter("state")+";Postcode:"+request.getParameter("postcode")+";Country: "+request.getParameter("Country")+";Phone: "+request.getParameter("phone")+";Mobile: "+request.getParameter("mobile")+";";
        	String desc="Change Contact Details "+u.getFullName()+" By User";
            try
            {		
             Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
            }
            catch(Exception e)
            {
            }

            response.sendRedirect("/close.jsp?reload=true");
            return;
        }

    } else {
        address1 = Util.notNull(u.getAddress1());
        address2 = Util.notNull(u.getAddress2());
        suburb = Util.notNull(u.getSuburb());
        if (u.getCountry().equals("AU")) {
            state = Util.notNull(u.getState());
            manualState = "";
        } else {
            manualState = Util.notNull(u.getState());
            state = "XX";
        }
        postcode = Util.notNull(u.getPostcode());
        country = Util.notNull(u.getCountry());
        phone = Util.notNull(u.getPhone());
        mobile = Util.notNull(u.getMobile());
    }
    CountryManager cm = new CountryManager();
    List<Country> countries = cm.listCountries();
%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head><title>Change Contact Details</title></head>
<body>
<div class="page-header">
    <h1>Change Contact Details</h1>
  </div>
<div class="row">
    <div class="span6 columns">
        <%@ include file="/WEB-INF/includes/formError.jsp" %>
  <form action="contact.jsp" method="post" class="form-horizontal">
      <fieldset>
          <legend>Your mailing address</legend>
          <%=HTML.textInput("address1", "Address line 1", "30", "50", address1, null, errorFields)%>
          <%=HTML.textInput("address2", "Address line 2", "30", "50", address2, null, errorFields)%>

          <%=HTML.textInput("suburb", "City / Suburb", "30", "50", suburb, null, errorFields)%>

          <%
              Constants.State stateArray[] = Constants.State.values();
              String stateValues[] = new String[stateArray.length + 2];
              String stateLabels[] = new String[stateArray.length + 2];
              for (int i = 0; i < stateArray.length; i++) {
                  stateValues[i] = stateArray[i].name();
                  stateLabels[i] = stateArray[i].getFullName();
              }
              stateValues[stateArray.length] = "";
              stateValues[stateArray.length + 1] = "XX";

              stateLabels[stateArray.length] = "---------------------";
              stateLabels[stateArray.length + 1] = "Not in Australia";

          %>
          <%=HTML.select("state", "Australian State or Territory", stateValues, stateLabels, state, null, errorFields, true)%>

          <%=HTML.textInput("manualState", "Foreign State", "30", "50", manualState, null, errorFields)%>
          <%=HTML.textInput("postcode", "Postcode", "30", "50", postcode, null, errorFields)%>

          <%
              String countryValues[] = new String[countries.size()];
              String countryLabels[] = new String[countries.size()];
              int i = 0;
              for (Country c : countries) {
                  countryValues[i] = c.getIso();
                  countryLabels[i] = c.getDisplayName();
                  i++;
              }
          %>
          <%=HTML.select("country", "Country", countryValues, countryLabels, country, null, errorFields, true)%>
          </fieldset>
          <fieldset>
              <legend>Phone Numbers</legend>
              <%=HTML.textInput("phone", "Home Phone Number", "30", "15", phone, null, errorFields)%>
              <%=HTML.textInput("mobile", "Mobile Phone Number", "30", "15", mobile, null, errorFields)%>
          </fieldset>

          <%=HTML.submitButton("Submit")%>
  </form>
        </div>
      </div>
</body>
</html>