<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.model.managers.UserManager" %>
<%@ page import="au.com.ourbodycorp.Config" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
	UserManager um = new UserManager();
	um.logoutFromExpiry(session, response);
%>
<html>
  <head><title>Session Expired</title>
  <script type="text/javascript">
    <%-- var timoutNow = 15000; // Timeout in 0.25 Secs.
//	var logoutUrl = 'http://localhost:8080/users/login.jsp'; // URL to logout page.
	
	var timeoutTimer;
	
	$(document).ready(function() {
		// Start timers.
		    timeoutTimer = setTimeout("IdleTimeout()", timoutNow);
	});
	
	// Logout the user.
	function IdleTimeout() {
		function disableF5(e) { if ((e.which || e.keyCode) == 116) e.preventDefault(); };
		$(document).on("keydown", disableF5);
		window.parent.location.replace("<%if(Config.getString("https").equals("true")){%><%=Config.getString("url.secure")%><%}else{%><%=Config.getString("url")%><%}%>/users/login.jsp");
	} --%>
	
  function disableF5(e) { if ((e.which || e.keyCode) == 116) e.preventDefault(); };
	$(document).on("keydown", disableF5);
  function Login() {
	  base_url = '<%if(Config.getString("https").equals("true")){%><%=Config.getString("url.secure")%><%}else{%><%=Config.getString("url")%><%}%>/users/login.jsp';
	  window.parent.location.href = base_url;
  }
  function Exit() {
	  base_url = '<%if(Config.getString("https").equals("true")){%><%=Config.getString("url.secure")%><%}else{%><%=Config.getString("url")%><%}%>';
	  window.parent.location.href = base_url;
  }
  </script>
  </head>
  <body>
  <div class="page-header">
    <h1>Session Expired</h1>
  </div>
  <div class="row">
    <div class="span6">
	    <p>For security reasons, you have been automatically logged out of Fiizio App. This will occur after an extended period of inactivity to protect unauthorised access of medical files.</p>
	     <%=HTML.loginAndClose()%>
    </div>
    </div>
  </body>
</html>