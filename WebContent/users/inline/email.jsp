<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.User" %>
<%@ page import="au.com.ourbodycorp.model.managers.UserManager" %>
<%@ page import="java.util.HashMap" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    User u = (User) session.getAttribute("user");
    if (u == null) {
        response.sendRedirect("/close.jsp");
        return;
    }
   /*  try{
    	Util.allActivitiesLogs("My Details", u.getId(), "No Corporation", "Change Email");
    }catch(Exception e){
    	
    } */
    UserManager um = new UserManager();
    u = um.getUser(u.getId());

    String email = request.getParameter("email");
    String email2 = request.getParameter("email2");

    HashMap<String, String> errorFields = new HashMap<String, String>();
	String from="Email: "+u.getEmail()+";";

    if (request.getMethod().equalsIgnoreCase("post")) {
        email = email.toLowerCase();
        email2 = email2.toLowerCase();

        if (!email.matches(email2)) {
            errorFields.put("email", "Email addresses do not match");
            errorFields.put("email2", "");
        }

        if (!email.matches("[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}")) {
            errorFields.put("email", "Please enter a valid email address");
        }

        try {
            email = Util.safeInput(email, true);
        } catch (Exception e) {
            errorFields.put("safeInput", "Characters &gt; and &lt; are not allowed in input.");
        }

        if (errorFields.size() == 0) {
            if (u.getEmail().equalsIgnoreCase(email)) {
                errorFields.put("email", "That's the same email address as you use now!");
            }
        }

        if (errorFields.size() == 0) {
            if (!um.isEmailAvailable(email, u.getId())) {
                errorFields.put("email", "Email address already in use by another user");
            }
        }

        if (errorFields.size() == 0) {
            String key = um.startEmailChange(u.getId(), email);
            um.sendEmailChangeEmail(u, key, email);
            
            String type="My Details";
            String activity_page="users/inline/email.jsp";
        	String corp_id="No Corporation";
        	String corp_name="----";
        	String corp_loc="---";
        	String user_id=String.valueOf(u.getId());
        	String user_name=u.getFullName();
        	String invitor_id="---";
        	String invitor_name="---";
        	String flag="update";
        	String to="Email: "+request.getParameter("email")+";";
        	String desc="Change Email for "+u.getFullName()+" By User";
            try
            {		
             Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
            }
            catch(Exception e)
            {
            }
            Util.redirectTo("email2.jsp", request, response);
            return;
        }
    }

%>
<html>
<head><title>Change Email Address</title></head>
<body>
<div class="page-header">
    <h1>Change Email Address</h1>
</div>

<div class="row">
    <div class="span6">
        <%@ include file="/WEB-INF/includes/formError.jsp" %>
        <form action="email.jsp" method="post" class="form-horizontal">
            <fieldset>
            <%=HTML.textInput("email", "New Email", "30", "50", email, null, errorFields)%>
          <%=HTML.textInput("email2", "Confirm Email", "30", "50", email2, null, errorFields)%>
                </fieldset>
          <%=HTML.submitButton("Submit")%>
</form>
    </div>
</div>
</body>
</html>