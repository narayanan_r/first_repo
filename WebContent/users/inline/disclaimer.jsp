<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    if (request.getMethod().equalsIgnoreCase("post")) {
        if (request.getParameter("submit").equals("accept")) {
            session.setAttribute("disclaimer", "accepted");
        } else {
            session.removeAttribute("disclaimer");
        }
        response.sendRedirect("/close.jsp");
    }
%>
<html>
<head><title>Disclaimer</title></head>
<body>
<div class="page-header">
    <h1>Disclaimer</h1>
</div>
<div class="row">
    <div class="span6">
        <h3>CONDITION OF USE:</h3>
        <p>
This App was designed by FiizioSoft, Inc.(“Fiizio”) as a tool for
health care providers to enhance the treatment of their clients. It can only be activated
by the health care provider after tailoring a program of exercises specifically for the client
as per standard health care practice. Because of this, Fiizio does not endorse the
exercises, information or any programs that are delivered to end users, nor does
Fiizio accept any responsibility for injuries that may arise from the information
provided using the App.
            </p>
        <p>
In using the Fiizio App system to facilitate communication you, as the health professional,
remain fully responsible for the content appropriateness and client safety at all times. You
also agree not to hold Fiizio liable in any way whatsoever for any injury, loss or damage
that a client may suffer  by using this app and, to the full extent permitted by law, you fully
release and indemnify Fiizio for any injury, loss or damage arising out of your use of this
system.
</p>
        <p>
By proceeding with the use of this account,  you acknowledge that you have read,
understood and agreed to this disclaimer.
        </p>
    </div>
</div>
</body>
</html>