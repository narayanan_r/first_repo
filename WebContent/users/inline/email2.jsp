<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head><title>Confirmation email sent</title></head>
<body>
<div class="page-header">
    <h1>Confirmation email sent</h1>
</div>
<div class="row">
    <div class="span8">
        <p>An email has been sent to your new email address with instruction for completing the change of your email
        address.
        </p>
        <p><a href="#" onclick="parent.jQuery.fancybox.close();" class="btn">Close Window</a> </p>
    </div>
</div>

</body>
</html>