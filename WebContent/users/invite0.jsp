<html>
<head><title>Accept Invitation</title></head>
<body>
<div class="page-header">
    <h1>Invitation not found</h1>
</div>
<div class="row">
    <div class="span8 columns">
        <p>
            The invitation you are trying to accept has not been found. Either you sent an invalid (incomplete?)
        link, the invitation has been withdrawn or it has expired.
        </p>
        <p>
            Please contact the administrator of your practice to send you a new invite.
        </p>
    </div>
    <div class="span8 columns">
        &nbsp;
    </div>
</div>
</body>
</html>