<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="au.com.ourbodycorp.model.Corporation" %>
<%@ page import="au.com.ourbodycorp.model.User" %>
<%@ page import="au.com.ourbodycorp.model.managers.CorporationManager" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="java.util.List" %>
<%@ page import="au.com.ourbodycorp.Config" %>
<%@ page import="au.com.ourbodycorp.UserMap" %>
<%
  User u = (User) session.getAttribute("user");
    
    if (u == null) {
        response.sendRedirect("login.jsp");
        return;
    }

    CorporationManager cm = new CorporationManager();
    List<Corporation> corps = cm.getCorporations(u.getId());
    if (corps.size() > 0) {
        Corporation c = corps.get(0);
        if (u.getLastCorp() > 0) {
            for (Corporation corp : corps) {
                if (corp.getId() == u.getLastCorp()) {
                    c = corp;
                    break;
                }
            }
        }
        String type="Login";
        String activity_page="users/properties.jsp";
    	String corp_id=String.valueOf(c.getId());
    	String corp_name=c.getName();
    	String corp_loc=c.getSuburb();
    	String user_id=String.valueOf(u.getId());
    	String user_name=u.getFullName();
    	String invitor_id="---";
    	String invitor_name="---";
    	String flag="login";
    	String from="---";
    	String to="---";
    	String desc="login into Corporation "+c.getName()+" By User:"+u.getFullName();
        try
        {		
         Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
        }
        catch(Exception e)
        {
        }
        //Removed SubDomain
        /* String[] Url = c.getUrl().split("//");
        session.setAttribute("getSlugUrl", Url[1]);
        
        if (Config.getBoolean("https", false)) {
        	response.sendRedirect("https://"+Config.getString("baseDomain") + "/corp/");
        }
        else {
        	response.sendRedirect("http://"+Config.getString("baseDomain") + "/corp/");
        } */
        //End Removed SubDomain
      
      //Added SubDomain
        response.sendRedirect(c.getUrl());
        System.out.println("url::"+c.getUrl());
      //End Added SubDomain
        return;
    } else {
        response.sendRedirect("me.jsp");
    }
%>
