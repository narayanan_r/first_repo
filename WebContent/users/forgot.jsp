<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.model.User" %>
<%@ page import="au.com.ourbodycorp.model.managers.UserManager" %>
<%@ page import="java.util.HashMap" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    UserManager um = new UserManager();
    um.logout(session, response);
    String email = request.getParameter("email");
    HashMap<String, String> errorFields = new HashMap<String, String>();
    if (request.getMethod().equalsIgnoreCase("post")){
        if(email.length() == 0){
        	errorFields.put("email", "Please enter a email");
        } else if (!email.matches("[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}")) {
        	errorFields.put("email", "Please enter a valid email address");
        } else{
        	 User u = um.getUserByEmail(email);
             if (u != null) {
                 String key = um.startPasswordReset(u.getId());
                 um.sendPasswordResetEmail(u, key);
                 response.sendRedirect("pwsent.jsp");
                 return;
             } else {
                 errorFields.put("email", "User not found");
             }
        }
    }
%>
<html>
<head><title>Forgot Password</title></head>
<body>
<div class="page-header">
    <h1>Forgot Password</h1>
</div>

<div class="row">
    <div class="span4">
        <p>We all forget things at times, nothing to worry about.</p>
        <p>
            Just enter the email address you use to log in and click submit.
            You will be sent an email with a link to click on where you will be able to
            choose a new password.
        </p>
        <p>
            If you can't remember you email address, please contact your practice's manager
            and they will be able to tell you.
        </p>
    </div>
    <div class="span5">
        <%@ include file="/WEB-INF/includes/formError.jsp" %>
        <form action="forgot.jsp" method="post" class="form form-vertical">
            <fieldset>
            <%=HTML.textInput("email", "Email Address", "30", "50", email, null, errorFields)%>
                </fieldset>
          <%=HTML.submitButton("Submit")%>
</form>
    </div>
</div>
</body>
</html>