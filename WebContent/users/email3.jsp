<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.model.EmailChange" %>
<%@ page import="au.com.ourbodycorp.model.User" %>
<%@ page import="au.com.ourbodycorp.model.managers.UserManager" %>
<%@ page import="java.util.HashMap" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    UserManager um = new UserManager();
    um.logout(session, response);

    String key = request.getParameter("key");

    if (key == null) {
        response.sendRedirect("email0.jsp");
        return;
    }


    EmailChange ec = um.getEmailForChangeKey(key);

    if (ec == null) {
        response.sendRedirect("email0.jsp");
        return;
    }

    User u = um.getUser(ec.getUserId());

    HashMap<String,String> errorFields = new HashMap<String,String>();

    if (request.getMethod().equalsIgnoreCase("POST")) {
        String pw = request.getParameter("pw");
        if (!u.comparePassword(pw)) {
            errorFields.put("pw","Password incorrect");
        }

        if (errorFields.size() == 0) {
            u.setEmail(ec.getEmail());
            um.saveUser(u);
            um.deleteEmailChange(key);
            response.sendRedirect("email4.jsp");
            return;
        }
    }
%>
<html>
<head><title>Confirm email change</title></head>
<body>

<div class="page-header">
    <h1>Confirm email change</h1>
</div>

<div class="row">
    <div class="span12 columns">
        <%@ include file="/WEB-INF/includes/formError.jsp" %>
        <form action="email3.jsp" method="post">
            <input type="hidden" name="key" value="<%=key%>"/>
            <fieldset>
                <p>To confirm the change of email address from <i><%=u.getEmail()%></i> to <b><%=ec.getEmail()%></b>, please enter your password.</p>
          <%=HTML.passwordInput("pw", "Password", "25", "25", null, null, errorFields)%>
                </fieldset>
          <fieldset>

          <div class="clearfix">
            <label id="optionsRadio">&nbsp;</label>
            <div class="input">
              <button type="submit" class="btn primary">Submit</button>
            </div>
              </div>

          </fieldset>
</form>
    </div>
    <div class="span4 columns">
        &nbsp;
    </div>
</div>
</body>
</html>