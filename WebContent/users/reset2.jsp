<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.User" %>
<%@ page import="au.com.ourbodycorp.model.managers.UserManager" %>
<%@ page import="java.util.HashMap" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    User u = (User) session.getAttribute("resetForUser");
    if (u == null) {
        response.sendRedirect("/");
        return;
    }
    HashMap<String,String> errorFields = new HashMap<String, String>();
    String pw1 = request.getParameter("pw1");
    String pw2 = request.getParameter("pw2");
    if (pw1 != null && pw2 != null) {
    	 	boolean b1=pw1.matches("[a-zA-Z]+");
            boolean b2= pw1.matches("[0-9]+");
            boolean b3= pw1.matches("[^A-Za-z0-9 ]+");
            boolean b4= pw1.matches("[^a-zA-Z]+");
            boolean b5= pw1.matches("[^0-9]+");
            if(b1==true || b2==true || b3==true || b4==true || b5==true){
         	   errorFields.put("pw1", "Your new password must be AlphaNumeric");
            }else if (pw1.length() < 6) {
              errorFields.put("pw1", "Your new password must be at least 6 characters");
              errorFields.put("pw2", "");
          } else if (!pw1.equals(pw2)) {
            errorFields.put("pw1", "New passwords do not match");
            errorFields.put("pw2", "");
        } else {
            UserManager um = new UserManager();
            u.setPassword(Util.encryptPassword(pw1));
            um.saveUser(u);
            um.deletePasswordReset(u.getId());
            response.sendRedirect("reset3.jsp");
            return;
        }
    }
%>
<html>
<head><title>Reset Password</title></head>
<body>

<div class="page-header">
    <h1>Reset Password</h1>
</div>

<div class="row">
    <div class="span9">
        <%@ include file="/WEB-INF/includes/formError.jsp" %>
        <form action="reset2.jsp" method="post" class="form form-horizontal">
            <fieldset>
                <legend>New Password for <%=u.getEmail()%></legend>
          <%=HTML.passwordInput("pw1", "Password", "25", "25", null, null, errorFields)%>
          <%=HTML.passwordInput("pw2", "Confirm Password", "25", "25", null, null, errorFields)%>
                </fieldset>
          <%=HTML.submitButton("Submit")%>
</form>
    </div>
    <div class="span4 columns">
        &nbsp;
    </div>
</div>
</body>
</html>