<%@ page import="au.com.ourbodycorp.model.Corporation" %>
<%@ page import="au.com.ourbodycorp.model.Invitation" %>
<%@ page import="au.com.ourbodycorp.model.User" %>
<%@ page import="au.com.ourbodycorp.model.managers.CorporationManager" %>
<%@ page import="au.com.ourbodycorp.Config" %>
<%@ page import="java.sql.*" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    User u = (User) session.getAttribute("user");
    if (u == null) {
        response.sendRedirect("/");
        return;
    }

    String key = request.getParameter("key");
    Invitation invite;
    CorporationManager cm = new CorporationManager();
    Corporation corp;
    if (key == null) {
        response.sendRedirect("/");
        return;
    } else {
        invite = cm.getInvitation(key);
        if (invite == null) {
            response.sendRedirect("invite0.jsp");
            return;
        } else {
            corp = cm.getCorporation(invite.getCorpId());
            if (invite.getAcceptor() == u.getId()) {
                response.sendRedirect(corp.getUrl());
                return;
            }
        }


    }
    
	   /*  Connection conn = Util.getConnection();
	    PreparedStatement pst = conn.prepareStatement("select userid from user_roles where userid = ?");
	    pst.setLong(1, invite.getInviterId());
	    ResultSet rs = pst.executeQuery();
	    if(rs.next()){
	    	PreparedStatement pst1 = conn.prepareStatement("insert into user_roles values(?,'admin')");
	    	pst1.setLong(1, u.getId());
	    	pst1.execute();
	    	pst1.close();
	    }
	    pst.close();
	    conn.close(); */

    	cm.accept(invite, u);
	    Connection conn = Util.getConnection();
	    PreparedStatement pst = conn.prepareStatement("select corpid from corp_users where userid=? and inviter = ?");
	    pst.setLong(1, u.getId());
	    pst.setLong(2, invite.getInviterId());
	    ResultSet rs = pst.executeQuery();
	    if(rs.next()){
	    	corp=cm.getCorporation(rs.getLong("corpid"));
	    }
	    pst.close();
	    conn.close();
   	 	u.getRoles();
%>
<html>
<head><title>Invitation Accepted</title>
<script type="text/javascript">
	function slugUrl(slugUrl) {
		$.ajax({
	        url: '<%=Config.getString("url")%>/corp/slugurl_changer.jsp?slugUrl='+slugUrl+'',
	        type: 'POST',
	        success: function(data) {
	      	   window.location = '<%=Config.getString("url")%>/corp/';
	        }
	      });
	}
</script>
</head>
<body>
<div class="page-header">
    <h1>Invitation Accepted</h1>
</div>
<div class="row">
    <div class="span8 columns">
        You have accepted your invitation to join <%=invite.getCorporation().getName()%>.
    </div>
    <div class="span8 columns">
    <button class="btn primary" onclick="location.href='<%=corp.getUrl()%>'">Go to <%=invite.getCorporation().getName()%></button>
        <%-- <%if (Config.getBoolean("https", false)) {%>
                    <button class="btn primary" onclick="slugUrl('<%=corp.getSlug()%>')">Go to <%=invite.getCorporation().getName()%></button>
        		<%}else {%>
        			<button class="btn primary" onclick="slugUrl('<%=corp.getSlug()%>')">Go to <%=invite.getCorporation().getName()%></button>
        		<%}%> --%>
    </div>
</div>
</body>
</html>