<%@ page import="au.com.ourbodycorp.model.managers.UserManager" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    UserManager um = new UserManager();
    um.logout(session, response);
%>
<html>
<head><title>Email Changed</title></head>
<body>
<div class="page-header">
    <h1>Your email address has been changed</h1>
</div>
<div class="row">
    <div class="span8 columns">
        You can now <a href="login.jsp">login using your new email address.</a>
    </div>
    <div class="span8 columns">
        &nbsp;
    </div>
</div>

</body>
</html>