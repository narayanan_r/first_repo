<%@ page import="au.com.ourbodycorp.model.managers.UserManager" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    UserManager um = new UserManager();
    um.logout(session, response);
%>
<html>
  <head><title>Password Reset</title></head>
  <body>
  <div class="page-header">
    <h1>Your password has been changed</h1>
</div>
<div class="row">
    <div class="span8">
        You can now <a href="login.jsp">login using your new password.</a>
    </div>
    <div class="span8">
        &nbsp;
    </div>
</div>
  </body>
</html>