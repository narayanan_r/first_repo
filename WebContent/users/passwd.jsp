<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.Constants" %>
<%@ page import="au.com.ourbodycorp.model.User" %>
<%@ page import="au.com.ourbodycorp.model.managers.UserManager" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String pw = request.getParameter("pw");
    String pw1 = request.getParameter("pw1");
    String pw2 = request.getParameter("pw2");

    String error = null;

    User u = (User) session.getAttribute("user");
   /*  try{
    	Util.allActivitiesLogs("Change Password", u.getId(), "No Corporation", "Change Password");
    }catch(Exception e){
    	
    } */
    if (u == null) {
        response.sendRedirect("login.jsp?reason=" + Constants.LoginReason.login + "&return=/users/passwd.jsp");
        return;
    } else if (request.getMethod().equalsIgnoreCase("post") && pw != null && pw1 != null && pw2 != null) {
        UserManager um = new UserManager();
        u = um.getUser(u.getId());
        if (!u.comparePassword(pw)) {
            error = "Old password incorrect";
        } else if (pw1.length() < 5) {
            error = "New password must be at least 5 characters long";
        } else if (!pw1.equals(pw2)) {
            error = "New passwords don't match";
        } else {
            u.setPassword(Util.encryptPassword(pw1));
            um.saveUser(u);
            
            String type="Change Password";
            String activity_page="users/passwd.jsp";
        	String corp_id="No Corporation";
        	String corp_name="----";
        	String corp_loc="---";
        	String user_id=String.valueOf(u.getId());
        	String user_name=u.getFullName();
        	String invitor_id="---";
        	String invitor_name="---";
        	String flag="change";
        	String from="---";
        	String to="---";
        	String desc="Change Password for"+u.getFullName()+" By User";
            try
            {		
             Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
            }
            catch(Exception e)
            {
            }
            
            session.setAttribute("user", u);
            response.sendRedirect("pwchanged.jsp");
            return;
        }

    }
%>
<html>
<head><title>Change Password</title></head>
<body>
<div class="page-header"><h1>Change Password</h1></div>

<%@include file="/WEB-INF/includes/formErrorSimple.jsp"%>
<div class="row">
    <div class="span9">
       <form action="passwd.jsp" method="POST" class="form-horizontal">
           <fieldset>
               <legend>New password for <%=u.getEmail()%></legend>
               <%=HTML.passwordInput("pw", "Current Password", "30", "30", null, "")%>
               <%=HTML.passwordInput("pw1", "New Password", "30", "30", null, "")%>
               <%=HTML.passwordInput("pw2", "Repeat Password", "30", "30", null, "")%>
           </fieldset>
           <%=HTML.submitButton("Submit")%>
       </form>
        <p><a href="forgot.jsp">I forgot my password</a></p>
    </div>
</div>
</body>
</html>