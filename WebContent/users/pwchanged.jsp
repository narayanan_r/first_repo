<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head><title>Password Changed</title></head>
  <body>
  <div class="page-header">
      <h1>Password Changed</h1>
  </div>
  <div class="row">
      <div class="span9">
          <p>
            Your password has been changed. Make sure you remember it for the next time you log in.
           </p>
      </div>
  </div>

  </body>
</html>