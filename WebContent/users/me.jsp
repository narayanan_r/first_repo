<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.Company" %>
<%@ page import="au.com.ourbodycorp.model.Constants" %>
<%@ page import="au.com.ourbodycorp.model.User" %>
<%@ page import="au.com.ourbodycorp.model.Corporation" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="au.com.ourbodycorp.model.managers.CorporationManager" %>
<%@ page import="java.util.List" %>
<%
    User u = (User) session.getAttribute("user");
	
    if (u == null) {
        response.sendRedirect(Util.loginUrl(Constants.LoginReason.login, "/users/me.jsp", null));
        return;
    }
    String type="My Details";
    String activity_page="users/me.jsp";
	String corp_id="No Corporation";
	String corp_name="----";
	String corp_loc="---";
	String user_id=String.valueOf(u.getId());
	String user_name=u.getFullName();
	String invitor_id="---";
	String invitor_name="---";
	String flag="view";
	String from="---";
	String to="---";
	String desc="View My Details of "+u.getFullName()+" By User";
    /* try
    {		
     Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
    }
    catch(Exception e)
    {
    } */
    
    /* try{
    	Util.allActivitiesLogs("My Details", u.getId(), "No Corporation", "View My Details");
    }catch(Exception e){
    	
    } */
    LibraryManager lm = new LibraryManager();
    List<Company> companies = lm.listCompaniesForUser(u.getId());
   
    List<Corporation> corps = null;
    if (u != null) {
        CorporationManager cm = new CorporationManager();
        corps = cm.getCorporations(u.getId());
    }

%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head><title>My Details</title></head>
<body>
<div class="page-header">
    <h1>My Details</h1>
</div>
<div class="row">
    <div class="span3">
      <p>These are your personal details</p>
      <p>Always make sure you details are up to date so you don't miss any important information.</p>
    </div>
    <div class="span6">


    <h3>Name</h3>
    <p><%=Util.notNull(u.getTitle())%> <%=u.getFullName()%> (<a href="inline/name.jsp" class="fancybox">change</a>)</p>
    <h3>Email</h3>
    <p><%=u.getEmail()%> (<a href="inline/email.jsp" class="fancybox">change</a>)</p>
    <h3>Address</h3>
    <address>
        <%=Util.notNull(u.getAddress1())%><br/>
        <%=(u.getAddress2() != null && u.getAddress2().length() > 0) ? u.getAddress2() + "<br/>" : ""%>
        <%=Util.notNull(u.getSuburb())%>, <%=Util.notNull(u.getState())%> <%=Util.notNull(u.getPostcode())%><br/>
        <%=Util.notNull(u.getCountryName())%><br/>
    </address>
    <p>
        <b>Home:</b>  <%=Util.notNull(u.getPhone())%><br/>
        <b>Mobile:</b>  <%=Util.notNull(u.getMobile())%><br/>
    </p>
    <p>
        (<a href="inline/contact.jsp" class="fancybox">change</a>)
    </p>
</div>
    </div>
<%
    if (companies.size() > 0) {
%>
<div class="row">
    <div class="span3">
        <h2>My Companies</h2>
      <p>These are the companies you are an owner of.</p>
      <p>If the quota allows, you can add more locations to these companies.</p>
        <%-- <ul class="">
            <%
                for (Company c : companies) {
            %>
            <li><%=c.getName()%></li>
            <%
                }
            %>
        </ul> --%>
         <ul class="">
                <% if (corps != null && corps.size() > 0) {
                    for (Corporation corp : corps) {
                  %>
                  <li><a href="<%=corp.getUrl()%>"><%=corp.getName()%></a></li>
                  <%
                    }
                }
                %>
            </ul> 
        <p>
            <a href="corp.jsp" class="btn btn-primary btn-small">Add Location</a>
        </p>
    </div>
    <div class="span6">

    </div>
</div>
<%
    }
%>
</body>
</html>