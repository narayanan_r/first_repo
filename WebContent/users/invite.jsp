<%@ page import="au.com.ourbodycorp.Config" %>
<%@ page import="au.com.ourbodycorp.model.Invitation" %>
<%@ page import="au.com.ourbodycorp.model.User" %>
<%@ page import="au.com.ourbodycorp.model.managers.CorporationManager" %>
<%@ page import="java.net.URLEncoder" %>
<%@ page import="java.sql.*" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    User u = (User) session.getAttribute("user");

    String key = request.getParameter("key");
    Invitation invite;
    CorporationManager cm = new CorporationManager();
    if (key == null) {
        response.sendRedirect("/");
        return;
    } else {
        invite = cm.getInvitation(key);
        if (invite == null || invite.getAccepted() != null) {
            response.sendRedirect("invite0.jsp");
            return;
        }else{
        	Connection con = Util.getConnection();
            PreparedStatement pst = con.prepareStatement("select email from users where email = ?");
            pst.setString(1, invite.getEmail());
            ResultSet rs = pst.executeQuery();
            if(rs.next()){
            	session.setAttribute("userAlreadyExist", rs.getString("email"));
            }else{
            	session.setAttribute("userAlreadyExist", null);
            	session.setAttribute("newUser", invite.getEmail());
            }
        }
        
    }
    String returnUrl = URLEncoder.encode("invite.jsp?key=" + key, "UTF-8");
%>
<html>
<head><title>Accept Invitation</title></head>
<body>
<div class="page-header">
    <h1>Accept Invitation</h1>
</div>


<div class="row">
    <div class="span4 columns">
        <h2>Welcome</h2>

        <p>Use your registered email address and password to log in to <%=Config.getString("site.name")%>.</p>

        <p>If you have forgotten your password, <a href="forgot.jsp">click here</a> to reset it.</p>

        <p>New to <%=Config.getString("site.name")%>? <a href="register.jsp?invite=<%=key%>">click here</a> to register.</p>
    </div>
    <div class="span4 columns">
        <p>
            You have been sent an invitation by <strong><%=invite.getInviter().getFullName()%></strong> to
            join <strong><%=invite.getCorporation().getName()%></strong>.
        </p>
        <h2>I already use <%=Config.getString("site.name")%></h2>
        <%
            if (u == null) {
        %>
        <p>
            If you already have an account with <%=Config.getString("site.name")%>, you can log in and
            link <%=invite.getCorporation().getName()%> to your existing user name.
        </p>
        <p>
            <button class="btn primary" onclick="location.href='login.jsp?return=<%=returnUrl%>&loginEmail=<%=invite.getEmail()%>'">Log In</button>
        </p>
        <%if(session.getAttribute("userAlreadyExist") == null){ %>
        <h2>I am new and need to sign up</h2>
        <p>
            Don't have an account yet? Sign up now, it's free and quick. Click on the sign up link below,
            fill in your details and you will be using <%=Config.getString("site.name")%> in no time at all.
        </p>
        <p>
            <button class="btn primary" onclick="location.href='register.jsp?invite=<%=key%>'">Sign Up</button>
        </p>
        <%} %>
        <%
            } else {
        %>
        <p>
            You are logged in as <%=u.getFullName()%>. You can accept the invitation and link it to your account.
            If you are not <%=u.getFullName()%>, log out and click the invitation link again to sign in or register
            as a different user.
        </p>
        <p>
        <%if(!u.getEmail().equals(invite.getEmail())){ %>
        	<button class="btn primary" onclick="location.href='login.jsp?return=<%=returnUrl%>&loginEmail=<%=invite.getEmail()%>'">Please Log In as a <%=invite.getEmail()%></button>
       	<%}else{ %>
            <button class="btn primary" onclick="location.href='accept.jsp?key=<%=key%>'">Accept Invitation</button>
        <%} %>
        </p>
        <%
            }
        %>


    </div>
</div>
</body>
</html>