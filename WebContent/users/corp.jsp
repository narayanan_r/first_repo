<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.managers.CorporationManager" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="java.util.*" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%@ page import="au.com.ourbodycorp.model.managers.CountryManager" %>
<%@ page import="au.com.ourbodycorp.Config" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    User u = (User) session.getAttribute("user");
    if (u == null) {
        response.sendRedirect(Util.loginUrl(Constants.LoginReason.login, "/", null));
        return;
    }
    /* try{
    	Util.allActivitiesLogs("My Details", u.getId(), "No Corporation", "Create Practice");
    }catch(Exception e){
    	
    } */
    String error = null;

    LibraryManager lm = new LibraryManager();
    List<Company> companies = lm.listCompaniesForUser(u.getId());
    if (companies.size() == 0) {
        error = "You are not the owner of any company and can not register a new practice";
    }

    String name = request.getParameter("name");
    String address1 = request.getParameter("address1");
    String address2 = request.getParameter("address2");
    String suburb = request.getParameter("suburb");
    String state = request.getParameter("state");
    String manualState = (request.getParameter("manualState") != null) ? request.getParameter("manualState").trim() : "";
    String postcode = request.getParameter("postcode");
    String timezone = Util.notNull(request.getParameter("timezone"), "Australia/Sydney");
    String company = request.getParameter("company");
    String country = (request.getParameter("country") != null) ? request.getParameter("country").trim() : "AU";

    if (!country.equals("AU")) {
        state = manualState;
    }

    HashMap<String, String> errorFields = new HashMap<String, String>();

    String slug = null;
    if (name != null) {
        slug = name.toLowerCase().replaceAll("[^a-z0-9]", "");
    }

    CorporationManager cm = new CorporationManager();

    if (request.getMethod().equalsIgnoreCase("post")) {

        long companyId = Long.parseLong(company);
        Company comp = lm.getCompany(companyId);
        if (comp == null || !lm.isCompanyOwner(companyId, u.getId())) {
            errorFields.put("company", "Select company");
        } else if (comp.getMaxCorp() <= comp.getCorpCount()) {
            errorFields.put("company", "This company has exceeded its allowed number of practices");
        }

        if (name == null || name.trim().length() < 4) {
            errorFields.put("name", "A name of 4 characters or more is required");
        } else if (cm.getCorporation(slug) != null) {
            errorFields.put("name", "This name is already in use");
        } else if (cm.isReserved(slug)) {
            errorFields.put("name", "This name is not available");
        }

        if (address1 == null || address1.trim().length() < 3) {
            errorFields.put("address1", "An address is required");
            errorFields.put("address2", "");
        }

        if (suburb == null || suburb.trim().length() < 2) {
            errorFields.put("suburb", "A suburb is required");
        }

        if (country.equals("AU") && (postcode == null || !postcode.matches("[0-9]{4}"))) {
            errorFields.put("postcode", "A valid postcode is required");
        }

        if (country.equals("AU") && (state.length() == 0 || state.equals("XX"))) {
            errorFields.put("state", "Please select an Australian state");
        }

        if (timezone == null || timezone.length() == 0) {
            errorFields.put("timezone", "Please select a timezone");
        }

        if (errorFields.size() == 0) {
            Corporation c = new Corporation();
            c.setSlug(slug);
            c.setAddress1(address1);
            c.setAddress2(address2);
            c.setName(name);
            c.setCountry(country);
            c.setPostcode(postcode);
            c.setState(state);
            c.setSuburb(suburb);
            c.setTimeZone(TimeZone.getTimeZone(timezone));
            c.setCompanyId(companyId);
            c.setCountry(country);
            c.setSubType("trial");
            c = cm.save(c, u.getId());
            
            String type="My Details";
            String activity_page="users/corp.jsp";
        	String corp_id=String.valueOf(c.getId());
        	String corp_name=c.getName();
        	String corp_loc=c.getSuburb();
        	String user_id=String.valueOf(u.getId());
        	String user_name=u.getFullName();
        	String invitor_id="---";
        	String invitor_name="---";
        	String flag="new";
        	String from="Create Practice for User:"+u.getFullName();
        	String to="CorporationName"+name+";Address line 1: "+address1+";Address line 2: "+address2+";City / Suburb: "+suburb+";State: "+state+";Postcode:"+postcode+";Country: "+country;
        	String desc="Create Practice for "+u.getFullName()+" By User";
            try
            {		
             Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
            }
            catch(Exception e)
            {
            }
            
            //Removed SubDomain
            /* String[] Url = c.getUrl().split("//");
            session.setAttribute("getSlugUrl", Url[1]);
            if (Config.getBoolean("https", false)) {
            	response.sendRedirect("https://"+Config.getString("baseDomain") + "/corp/?from=new");
            }
            else {
            	response.sendRedirect("http://"+Config.getString("baseDomain") + "/corp/?from=new");
            } */
            //End Removed SubDomain
            
            //Added SubDomain
            response.sendRedirect(c.getUrl() + "?from=new");
            //End Added SubDomain
            return;
        }
    }

    String zones[] = TimeZone.getAvailableIDs();
    Arrays.sort(zones);
    LinkedList<TimeZone> tz = new LinkedList<TimeZone>();
    for (String zone : zones) {
            tz.add(TimeZone.getTimeZone(zone));
    }

    String[] tzValues = new String[tz.size()];
    String[] tzNames = new String[tz.size()];

    int ti = 0;
    for (TimeZone timeZone : tz) {
        tzValues[ti] = timeZone.getID();
        tzNames[ti] = timeZone.getID();//.split("/")[1].replaceAll("_", " ");
        ti++;
    }

    String[] companyValues = null;
    String[] companyLabels = null;
    if (companies.size() > 0) {
        companyValues = new String[companies.size()];
        companyLabels = new String[companies.size()];
        int i = 0;
        for (Company comp : companies) {
            companyValues[i] = Long.toString(comp.getId());
            companyLabels[i] = comp.getName() + " (" + comp.getCorpCount() + "/" + comp.getMaxCorp() + ")";
        }
    } else {
        companyValues = new String[1];
        companyValues[0] = "";
        companyLabels = new String[1];
        companyLabels[0] = "No Companies";
    }

    CountryManager ctm = new CountryManager();
    List<Country> countries = ctm.listCountries();

%>
<html>
<head>
    <title>Create Practice</title>
    <script type="text/javascript">
          function toggleState() {
              var s = document.getElementById("country");
              var au = document.getElementById("state_au");
              var other = document.getElementById("state_other");

              if (s[s.selectedIndex].value == "AU") {
                  au.style.display = "block";
                  other.style.display = "none";
              } else {
                  au.style.display = "none";
                  other.style.display = "block";
              }


          }
    </script>

</head>
<body>


  <div class="page-header">
    <h1>Create Practice</h1>
  </div>
  <div class="row">
    <div class="span3 columns">
      <h2>Let's get Started</h2>
      <p>Enter the basic details for your practice. Once it is created, you can invite others to join and use Fiizio.</p>
    </div>
    <div class="span6 columns">
        <%@include file="/WEB-INF/includes/formErrorSimple.jsp"%>
        <%@ include file="/WEB-INF/includes/formError.jsp" %>
      <form action="corp.jsp" method="post" class="form-horizontal">
          <fieldset>
              <legend>Company this practice is part of</legend>
              <%=HTML.select("company", "Company", companyValues, companyLabels, company, null, errorFields, false)%>
          </fieldset>
          <fieldset>
              <legend>Name</legend>
              <%=HTML.textInput("name","Location Name","30","50",name, "e.g.: Example Physio Adelaide", errorFields)%>
          </fieldset>

        <fieldset>
          <legend>Registered Address</legend>
          <%=HTML.textInput("address1", "Address line 1", "30", "50", address1, null, errorFields)%>
          <%=HTML.textInput("address2", "Address line 2", "30", "50", address2, null, errorFields)%>

          <%=HTML.textInput("suburb", "City / Suburb", "30", "50", suburb, null, errorFields)%>

          <%
              Constants.State stateArray[] = Constants.State.values();
              String stateValues[] = new String[stateArray.length];
              String stateLabels[] = new String[stateArray.length];
              for (int i = 0; i < stateArray.length; i++) {
                  stateValues[i] = stateArray[i].name();
                  stateLabels[i] = stateArray[i].getFullName();
              }


          %>
          <div id="state_au" <%=(!country.equals("AU")) ? "style=\"display:none\"" : ""%>>
            <%=HTML.select("state", "State or Territory", stateValues, stateLabels, state, null, errorFields, true)%>
          </div>

          <div id="state_other" <%=(country.equals("AU")) ? "style=\"display:none\"" : ""%>>
            <%=HTML.textInput("manualState", "State/Province", "30", "50", manualState, null, errorFields)%>
          </div>

          <%=HTML.textInput("postcode", "Postcode", "30", "50", postcode, null, errorFields)%>

            <div class="control-group">
              <label for="country" class="control-label">Country</label>
              <div class="controls">
                  <select id="country" name="country" onchange="toggleState()">
                      <%
                          for (Country c : countries) {
                      %>
                      <option value="<%=c.getIso()%>" <%=(c.getIso().equals(country)) ? "selected" : ""%>><%=c.getDisplayName()%></option>
                      <%
                          }
                      %>
                    </select>
              </div>
          </div>

            <%=HTML.select("timezone", "Timezone", tzValues, tzNames, timezone, null,errorFields, false)%>
          </fieldset>


        <%=HTML.submitButton("Create")%>
      </form>
    </div>
  </div>



</body>
</html>