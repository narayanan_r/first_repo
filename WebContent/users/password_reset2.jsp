<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.User" %>
<%@ page import="au.com.ourbodycorp.model.managers.UserManager" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.sql.*" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    if (session.getAttribute("resetForUser") == null) {
        response.sendRedirect("/");
        return;
    }
    HashMap<String,String> errorFields = new HashMap<String, String>();
    String pw1 = request.getParameter("pw1");
    String pw2 = request.getParameter("pw2");
    if (pw1 != null && pw2 != null) {
        if (pw1.length() < 5) {
            errorFields.put("pw1", "Your new password must be at least 5 characters");
            errorFields.put("pw2", "");
        } else if (!pw1.equals(pw2)) {
            errorFields.put("pw1", "New passwords do not match");
            errorFields.put("pw2", "");
        } else {
            UserManager um = new UserManager();
            
            Connection con1 = Util.getConnection();
		    PreparedStatement pst1 = con1.prepareStatement("update patient set password = ? where id = ?");
            pst1.setString(1, Util.encryptPassword(pw1));
            pst1.setLong(2, Long.parseLong(session.getAttribute("resetForUser").toString()));
            pst1.executeUpdate();
            pst1.close();
            con1.close();
            
            um.logout(session, response);
            response.sendRedirect("password_reset3.jsp");
            return;
        }
    }
%>
<html>
<head><title>Password Reset</title></head>
<body>

<div class="page-header">
    <h1>Password Reset</h1>
</div>

<div class="row">
    <div class="span9">
        <%@ include file="/WEB-INF/includes/formError.jsp" %>
        <form action="password_reset2.jsp" method="post" class="form form-horizontal">
            <fieldset>
                <legend>New Password for <%=session.getAttribute("email").toString()%></legend>
          <%=HTML.passwordInput("pw1", "Password", "25", "25", null, null, errorFields)%>
          <%=HTML.passwordInput("pw2", "Confirm Password", "25", "25", null, null, errorFields)%>
                </fieldset>
          <%=HTML.submitButton("Submit")%>
</form>
    </div>
    <div class="span4 columns">
        &nbsp;
    </div>
</div>
</body>
</html>