<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="au.com.ourbodycorp.HTML" %>

<%
    if (request.getMethod().equalsIgnoreCase("post")) {
        response.sendRedirect("properties.jsp");
        return;
    }
%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    
    <body>
        <div class="page-header" style="margin: 100px 0;">
          <h1>Congratulations!</h1>
        </div>
        
        <div class="row">
          <div class="span4">
            <h4>Your Fiizio App setup is complete and ready to use.</h4>
          </div>

          <div class="span5">
              <form action="registrationSuccess.jsp" method="post" class="form-horizontal">
                  <fieldset>
                      <p>Click through to continue to your Fiizio App Dashboard.</p>
                      <%=HTML.submitButton("Continue")%>
                  </fieldset>
              </form>
          </div>
        </div>
    </body>
</html>
