<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head><title>Verification email resent</title></head>
  <body>
  <div class="page-header">
    <h1>Verification email resent</h1>
</div>
<div class="row">
    <div class="span8 columns">
        An email has been sent to you with instruction for activating your account.
    </div>
    <div class="span8 columns">
        &nbsp;
    </div>
</div>
</body>
</html>