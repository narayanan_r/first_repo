<%@page import="au.com.ourbodycorp.model.managers.MiniNewsManager"%>
<%@ page import="au.com.ourbodycorp.Config" %>
<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%@ page import="au.com.ourbodycorp.model.managers.CorporationManager" %>
<%@ page import="au.com.ourbodycorp.model.managers.CountryManager" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="au.com.ourbodycorp.model.managers.PatientManager" %>
<%@ page import="au.com.ourbodycorp.model.managers.UserManager" %>
<%@ page import="java.util.*" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
System.out.println("registerSecond.jsp=====>");
	String from = Util.notNull(request.getParameter("from"));
	
    if (session.getAttribute("user") != null || session.getAttribute("newUser") == null) {
        UserManager um = new UserManager();
        um.logout(session, response);
        response.sendRedirect("register.jsp?from=" + ((from == null)? "" : from));
        return;
    }

    String zones[] = TimeZone.getAvailableIDs();
    Arrays.sort(zones);
    LinkedList<TimeZone> tz = new LinkedList<TimeZone>();
    
    for (String zone : zones) {
          tz.add(TimeZone.getTimeZone(zone));
	 }
    

    int ti = 0;
    
    ArrayList<String> values = new ArrayList<String>();
    ArrayList<String> names = new ArrayList<String>();
    ArrayList<String> remove = new ArrayList<String>();
    remove.add("US/Aleutian");
    remove.add("US/Arizona");
    remove.add("US/East-Indiana");
    remove.add("US/Indiana-Starke");
    remove.add("US/Michigan");
    remove.add("US/Pacific-New");
    remove.add("US/Samoa");
    remove.add("Canada/East-Saskatchewan");
    remove.add("Canada/Saskatchewan");
    remove.add("Canada/Yukon");
    
    for (TimeZone timeZone : tz) {
		if (timeZone.getID().contains("Canada")
				|| timeZone.getID().contains("US")) {
			values.add(timeZone.getID());
			names.add(timeZone.getID());

			
/* 			tzValues[ti] = timeZone.getID();
			tzNames[ti] = timeZone.getID();
			ti++;
 */			
		}
	}
    names.removeAll(remove);
    values.removeAll(remove);
    
    Object[] tzValues_obj = new String[values.size()];
    Object[] tzNames_obj = new String[names.size()];
    tzValues_obj = values.toArray();
    tzNames_obj =  names.toArray();
    
    String[] tzValues = Arrays.copyOf(tzValues_obj, tzValues_obj.length, String[].class);
    String[] tzNames = Arrays.copyOf(tzNames_obj, tzNames_obj.length, String[].class);

   
    
	String timezone = (request.getParameter("timezone") != null) ? request
			.getParameter("timezone").trim() : "US/Alaska";
	String invite = (String) session.getAttribute("invite");
	
	System.out.println("invite=======>"+invite);
	String address1 = (request.getParameter("address1") != null) ? request
			.getParameter("address1").trim() : "";
	String address2 = (request.getParameter("address2") != null) ? request
			.getParameter("address2").trim() : "";
	String suburb = (request.getParameter("suburb") != null) ? request
			.getParameter("suburb").trim() : "";
	String state = (request.getParameter("state") != null) ? request
			.getParameter("state").trim() : "";
	String manualState = (request.getParameter("manualState") != null) ? request
			.getParameter("manualState").trim() : "";
	String postcode = (request.getParameter("postcode") != null) ? request
			.getParameter("postcode").trim() : "";
	String country = (request.getParameter("country") != null) ? request
			.getParameter("country").trim() : "US";
	String company = (String) session.getAttribute("newCompanyName");
	String hours = Util.notNull(request.getParameter("hours"));

	String pnumber = (request.getParameter("pnumber") != null) ? request
			.getParameter("pnumber").trim() : "";
	;
	String pemail = (request.getParameter("pemail") != null) ? request
			.getParameter("pemail").trim() : "";
	;
	String pwebsite = (request.getParameter("pwebsite") != null) ? request
			.getParameter("pwebsite").trim() : "";

	User newUser = (User) session.getAttribute("newUser");

	String fName = newUser.getFirstName();
	String lName = newUser.getLastName();
	String emailid = newUser.getEmail();

	if (!country.equals("AU")) {
		state = manualState;
	}

	HashMap<String, String> errorFields = new HashMap<String, String>();
	UserManager mm = new UserManager();
	LibraryManager lm = new LibraryManager();
	if (request.getMethod().equalsIgnoreCase("post")) {
		
		System.out.println("TRUE EQUALS IGNORE CASE=====>");
		
		if (country.equals("")) {
			errorFields.put("country", "Please select a country");
		}

		if (country.equals("AU")
				&& (state.length() == 0 || state.equals("XX"))) {
			errorFields.put("state",
					"Please select an Australian state");
		}

		if (!country.equals("AU")
				&& (manualState.length() == 0 || manualState
						.equals("XX"))) {
			errorFields.put("manualState", "Please enter a state");
		}

		if (address1.length() < 5) {
			errorFields.put("address1", "Please enter an address");
		}

		if (suburb.length() < 5) {
			errorFields.put("suburb", "Please enter a city / suburb");
		}

		if (country.equals("AU") && !postcode.matches("[0-9]{4}")) {
			errorFields.put("postcode",
					"Please enter a valid Australian postcode");
		}

		if (!country.equals("AU") && (postcode == "")) {
			errorFields.put("postcode", "Please enter a postcode");
		}

		try {
			address1 = Util.safeInput(address1, true);
			address2 = Util.safeInput(address2, true);
			suburb = Util.safeInput(suburb, true);
			state = Util.safeInput(state, true);
			postcode = Util.safeInput(postcode, true);
			manualState = Util.safeInput(manualState, true);
		} catch (Exception e) {
			errorFields
					.put("safeInput",
							"Characters &gt; and &lt; are not allowed in input.");
		}

		CorporationManager cm = new CorporationManager();

		if (errorFields.size() == 0) {

			newUser.setAddress1(address1);
			newUser.setAddress2(address2);
			newUser.setSuburb(suburb);
			newUser.setState(state);
			newUser.setPostcode(postcode);
			newUser.setType(User.Type.member);
			newUser.setVerifyKey(null);
			newUser.setStatus(User.Status.authorised);
			newUser.setCountry(country);
			newUser.setLastSeen(new Date());
			newUser.setDisclaimed(true);
			//             newUser.setApplicationId(0);
			newUser = mm.saveUser(newUser);

			//this is a new user account, walk them through the tutorial
			mm.setTutorialProgressForUser(newUser.getId(),
					UserManager.TUTORIAL_NOT_DONE, true);

			if (from.equals("trial")) {
				Company co = new Company();
				co.setName(company);
				co.setEmail(newUser.getEmail());
				co.setContact(newUser.getFirstName() + " "
						+ newUser.getLastName());
				co.setAddress1(address1);
				co.setAddress2(address2);
				co.setSuburb(suburb);
				co.setState(state);
				co.setCountry(country);
				co.setPhone(newUser.getMobile());
				co.setPostcode(postcode);
				co.setMaxCorp(1);
				LinkedList<User> owners = new LinkedList<User>();
				owners.add(newUser);
				co.setOwners(owners);
				lm.save(co);

				Corporation c = new Corporation();
				c.setAddress1(address1);
				c.setAddress2(address2);
				c.setCountry(country);
				c.setSuburb(suburb);
				c.setPostcode(postcode);
				c.setName(company);
				c.setState(state);
				c.setCreated(new Date());
				c.setCountry(country);
				c.setTimeZone(TimeZone.getTimeZone(timezone));
				//                 c.setTimeZone(Util.getDefaultTimeZone(country, state));
				String slug = (String) session
						.getAttribute("newCompanySlug");
				c.setSlug(slug);
				c.setCompanyId(co.getId());
				c.setSubType("trial");
				c.setMaxPatients(Config.getInt("trial.patients", 10));
				c = cm.save(c, newUser.getId());
				cm.setCorpMeta(c.getId(), "hours",
						Util.safeInput(hours, false));
				cm.setCorpMeta(c.getId(), "phone",
						Util.safeInput(pnumber, false));
				cm.setCorpMeta(c.getId(), "email",
						Util.safeInput(pemail, false));
				cm.setCorpMeta(c.getId(), "www",
						Util.safeInput(pwebsite, false));

				//set up a sample news item
				MiniNews news = new MiniNews();
				news.setCorpId(c.getId());
				news.setCreated(new Date());

				news.setHeadline("Communication is the key");
				news.setBody("You're holding some very fancy technology in your hand and we are pleased to be one of the first practices in our area to use Fiizio App. It's a specific communication channel between us and you. The goal is to help us support you with better communication, better understanding and better results.\n\n"
						+ "The Fiizio App provides you with clear instructions, photos and videos of the exercises that we prescribe to you during your consultations. It also allows us to send you further reading modules on your condition and self care, so that you are getting the best results with minimum anxiety.\n\n"
						+ "Best of all it fits neatly into you pocket, so you always have the information and our contact details with you wherever you go. You can even share us directly with a friend!");
				news.setStatus("published");
				news.setPhotoId(Constants.TESTING_MODE ? 48 : 236);
				MiniNewsManager newsManager = new MiniNewsManager();
				newsManager.save(news);

				//create a sample patient
				Patient samplePatient = new Patient();
				samplePatient.setCreated(new Date());
				samplePatient.setCreator(newUser.getId());
				samplePatient.setCorpId(c.getId());
				samplePatient.setFirstName(newUser.getFirstName());
				samplePatient.setLastName(newUser.getLastName());
				samplePatient.setEmail(newUser.getEmail());
				samplePatient.setPhone(newUser.getMobile());
				samplePatient.setSubscriptionId(5);
				samplePatient.setIsuserloginfirst(false);
				samplePatient = lm.save(samplePatient);
				PatientManager pm = new PatientManager();
	            pm.savePatient(newUser.getPassword(),samplePatient.getEmail());
				//put that sample patient into a group
				PatientGroup patientGroup = new PatientGroup();
				patientGroup.setCorporationId(c.getId());
				patientGroup.setGroupName("Jets Soccer Club");
				patientGroup = lm.save(patientGroup);
				lm.addPatientToGroup(samplePatient.getId(),
						patientGroup.getId());
				
                  // payment subcription for patient
                  
                PaymentSubscription p= lm.savePaymentSubscription("0","au.com.fiiziosub.6","0","Success",samplePatient.getEmail(),samplePatient.getCorpId()+"");	
                		  
                  
				//give our sample patient a program
				Program sampleProgram = new Program();
				sampleProgram.setCorpId(c.getId());
				sampleProgram.setCreated(new Date());
				sampleProgram.setCreator(newUser.getId());
				sampleProgram.setPatientId(samplePatient.getId());
				sampleProgram.setName("Knee pain");
				sampleProgram.setInterval(2);
				sampleProgram
						.setIntervalType(Program.IntervalType.perDay);
				sampleProgram = lm.save(sampleProgram, "copyProgram");

				//give the sample program a note
				ProgramNote sampleNote = new ProgramNote();
				sampleNote.setDate(new Date());
				sampleNote.setProgramId(sampleProgram.getId());
				sampleNote
						.setNote("No soccer training this Tuesday. Light run on Thursday only and we'll reassess Saturday morning before the game.");
				lm.save(sampleNote);

				//add an info article
				lm.addInfo(sampleProgram.getId(), 5);

				//add some sample exercises
				Exercise ex1 = lm.getExercise(25);
				ProgramExercise pe1 = new ProgramExercise();
				pe1.setExerciseId(ex1.getId());
				pe1.setProgramId(sampleProgram.getId());
				pe1.setName(ex1.getName());
				pe1.setDescription(ex1.getDescription());
				pe1.setType("exercise");
				pe1.setSequence(1);
				pe1.setBase(ex1.getType());
				pe1.setShowSides(ex1.isShowSides());
				pe1.setSets(1);
				pe1.setReps(1);
				pe1.setHolds(15);
				pe1.setRest(0);
				pe1.setSide("Left");
				lm.save(pe1);

				Exercise ex2 = lm.getExercise(165);
				ProgramExercise pe2 = new ProgramExercise();
				pe2.setExerciseId(ex2.getId());
				pe2.setProgramId(sampleProgram.getId());
				pe2.setName(ex2.getName());
				pe2.setDescription(ex2.getDescription());
				pe2.setType("exercise");
				pe2.setSequence(2);
				pe2.setBase(ex2.getType());
				pe2.setShowSides(ex2.isShowSides());
				pe2.setSets(2);
				pe2.setReps(1);
				pe2.setHolds(15);
				pe2.setRest(0);
				pe2.setSide("Both");
				lm.save(pe2);

				Exercise ex3 = lm.getExercise(92);
				ProgramExercise pe3 = new ProgramExercise();
				pe3.setExerciseId(ex3.getId());
				pe3.setProgramId(sampleProgram.getId());
				pe3.setName(ex3.getName());
				pe3.setDescription(ex3.getDescription());
				pe3.setType("exercise");
				pe3.setSequence(3);
				pe3.setBase(ex3.getType());
				pe3.setShowSides(ex3.isShowSides());
				pe3.setSets(1);
				pe3.setReps(4);
				pe3.setHolds(5);
				pe3.setRest(0);
				pe3.setSide("Left");
				lm.save(pe3);
			}
			if (invite != null) {
				Invitation i = cm.getInvitation(invite);
				if (i != null) {
					System.out.println("setting acceptor to: "
							+ newUser.getId());
					i.setAcceptor(newUser.getId());
					cm.save(i);
					Invitation invites;
					if (invite == null) {
						response.sendRedirect("/");
						return;
					} else {
						invites = cm.getInvitation(invite);
					}
					cm.accept(invites, newUser);
				}
			}

			session.setAttribute("user", newUser);
			session.setAttribute("hasCompanies",
					lm.hasCompanies(newUser.getId()));
System.out.println("Before Registration success======>");
			response.sendRedirect("registrationSuccess.jsp");

			return;
		}
	} else if ((from == null || !from.equals("trial"))
			&& invite != null && invite.length() > 0) {
		//         User newUser = (User) session.getAttribute("newUser");

		newUser.setType(User.Type.member);
		newUser.setVerifyKey(null);
		newUser.setStatus(User.Status.authorised);
		newUser.setCountry(country);
		newUser.setLastSeen(new Date());
		newUser.setDisclaimed(true);
		newUser = mm.saveUser(newUser);

		mm.setTutorialProgressForUser(newUser.getId(),
				UserManager.TUTORIAL_NOT_DONE, false);

		CorporationManager cm = new CorporationManager();
		Invitation i = cm.getInvitation(invite);
		if (i != null) {
			i.setAcceptor(newUser.getId());
			cm.save(i);
			cm.addUserToCorp(i.getCorpId(), newUser.getId(),
					i.getRoles());
			Invitation invites;
			if (invite == null) {
				response.sendRedirect("/");
				return;
			} else {
				invites = cm.getInvitation(invite);
			}
			cm.accept(invites, newUser);
		}

		session.setAttribute("user", newUser);
		session.setAttribute("hasCompanies",
				lm.hasCompanies(newUser.getId()));

		response.sendRedirect("registrationSuccess.jsp");
	}
	CountryManager cm = new CountryManager();
	List<Country> countries = cm.listCountries();
%>
<html>
  <head>
      <title>Register<%=(from.equals("trial")) ? " for your free trial" : "" %></title>
      <style type="text/css">
        body {
            background-color: #ededed;
            background-image: none;
        }
    </style>
      <script type="text/javascript">
          function toggleState() {
              var s = document.getElementById("country");
              var au = document.getElementById("state_au");
              var other = document.getElementById("state_other");

              if (s[s.selectedIndex].value == "AU") {
                  au.style.display = "block";
                  other.style.display = "none";
              } else {
                  au.style.display = "none";
                  other.style.display = "block";
              }


          }
      </script>
      <%-- <script type="text/javascript">
      $( document ).ready(function() {
//     	    console.log( "ready!" );
        // your API key is available at
        // https://app.getresponse.com/my_api_key.html
        var api_key = '655fb82fcf9653960ee4f065acbac473';

        // API 2.x URL
        var api_url = 'https://api2.getresponse.com';


            var campaigns = {};

            // find campaign named 'test'
            $.ajax({
                url     : api_url,
                data    : JSON.stringify({
                    'jsonrpc'   : '2.0',
                    'method'    : 'get_campaigns',
                    'params'    : [
                        api_key,
                        {
                            // find by name literally
                            'name' : { 'EQUALS' : 'fiizio_trials' }
                        }
                    ],
                    'id'        : 1
                }),
                type        : 'POST',
                contentType : 'application/json',
                dataType    : 'JSON',
                crossDomain : true,
                async       : false,
                success     : function(response) {
                    // uncomment following line to preview Response
//                     alert(JSON.stringify(response));

                    campaigns = response.result;
                }
            });

            // because there can be only (too much HIGHLANDER movie) one campaign of this name
            // first key is the CAMPAIGN_ID required by next method
            // (this ID is constant and should be cached for future use)
            var CAMPAIGN_ID;
            for(var key in campaigns) {
                CAMPAIGN_ID = key;
                break;
            }

            $.ajax({
                url     : api_url,
                data    : JSON.stringify({
                    'jsonrpc'    : '2.0',
                    'method'    : 'add_contact',
                    'params'    : [
                        api_key,
                        {
                            // identifier of 'test' campaign
                            'campaign'  : CAMPAIGN_ID,

                            // basic info
                            'name'      : '<%=fName+" "+lName%>',
                            'email'     : '<%=emailid%>',
                            'cycle_day' : 0,

                            // custom fields
                            'customs'   : [
                                {
                                    'name'       : 'company',
                                    'content'    : '<%=company%>'
                                }
                            ]
                        }
                    ],
                    'id'        : 2
                }),
                type        : 'POST',
                contentType : 'application/json',
                dataType    : 'JSON',
                crossDomain : true,
                async       : false,
                success     : function(response)
                {
                    // uncomment following line to preview Response
//                     alert(JSON.stringify(response));

//                     alert('Contact added');
                }
            });
            
         // find campaign named 'test'
            $.ajax({
                url     : api_url,
                data    : JSON.stringify({
                    'jsonrpc'   : '2.0',
                    'method'    : 'get_campaigns',
                    'params'    : [
                        api_key,
                        {
                            // find by name literally
                            'name' : { 'EQUALS' : 'fiizio_new_website' }
                        }
                    ],
                    'id'        : 1
                }),
                type        : 'POST',
                contentType : 'application/json',
                dataType    : 'JSON',
                crossDomain : true,
                async       : false,
                success     : function(response) {
                    // uncomment following line to preview Response
//                     alert(JSON.stringify(response));

                    campaigns = response.result;
                }
            });

            // because there can be only (too much HIGHLANDER movie) one campaign of this name
            // first key is the CAMPAIGN_ID required by next method
            // (this ID is constant and should be cached for future use)
            var CAMPAIGN_ID;
            for(var key in campaigns) {
                CAMPAIGN_ID = key;
                break;
            }

            $.ajax({
                url     : api_url,
                data    : JSON.stringify({
                    'jsonrpc'    : '2.0',
                    'method'    : 'add_contact',
                    'params'    : [
                        api_key,
                        {
                            // identifier of 'test' campaign
                            'campaign'  : CAMPAIGN_ID,

                            // basic info
                            'name'      : '<%=fName+" "+lName%>',
                            'email'     : '<%=emailid%>',
                            'cycle_day' : 0,

                            // custom fields
                            'customs'   : [
                                {
                                    'name'       : 'company',
                                    'content'    : '<%=company%>'
                                }
                            ]
                        }
                    ],
                    'id'        : 2
                }),
                type        : 'POST',
                contentType : 'application/json',
                dataType    : 'JSON',
                crossDomain : true,
                async       : false,
                success     : function(response)
                {
                    // uncomment following line to preview Response
//                     alert(JSON.stringify(response));

//                     alert('Contact added');
                }
            });

      });
    </script> --%>
  </head>
  <body>

  <!-- Google Code for To sign up process Conversion Page -->
    <script type="text/javascript">
    /* <![CDATA[ */
        var google_conversion_id = 997830674;
        var google_conversion_language = "en";
        var google_conversion_format = "3";
        var google_conversion_color = "ffffff";
        var google_conversion_label = "rrpNCN64vwYQkuDm2wM";
        var google_conversion_value = 0;
        var google_remarketing_only = false;
        /* ]]> */
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
    </script>
    <noscript>
        <div style="display:inline;">
            <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/997830674/?value=0&amp;label=rrpNCN64vwYQkuDm2wM&amp;guid=ON&amp;script=0"/>
        </div>
    </noscript>
    
    <script type="text/javascript">
        var fb_param = {};
        fb_param.pixel_id = '6013750960789';
        fb_param.value = '0.00';
        fb_param.currency = 'AUD';
        (function(){
          var fpw = document.createElement('script');
          fpw.async = true;
          fpw.src = '//connect.facebook.net/en_US/fp.js';
          var ref = document.getElementsByTagName('script')[0];
          ref.parentNode.insertBefore(fpw, ref);
        })();
    </script>
    <noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/offsite_event.php?id=6013750960789&amp;value=0&amp;currency=AUD" /></noscript>
      
  <div class="page-header">
    <h2>We're almost done, tell us about your practice</h2>
  </div>

  <div class="row">
    <div class="span9 columns">
        <%@ include file="/WEB-INF/includes/formError.jsp" %>
  <form action="registerSecond.jsp" method="post" class="form-horizontal">
      <input type="hidden" name="invite" value="<%=Util.formValue(invite)%>"/>
      <input type="hidden" name="from" value="<%=from%>"/>
      
      <p style="margin-left: 160px; font-weight: bold; margin-bottom: 10px; font-size: 15px;">This information will be used to customize the App:</p>
      <fieldset>
           <div class="control-group">
              <label for="country" class="control-label">Country</label>
              <div class="controls">
                  <select id="country" name="country" onchange="toggleState()">
                      <%
                          for (Country c : countries) {
                      %>
                      <option value="<%=c.getIso()%>" <%=(c.getIso().equals(country)) ? "selected" : ""%>><%=c.getDisplayName()%></option>
                      <%
                          }
                      %>
                    </select> 
              </div>
          </div>
          <%=HTML.select("timezone", "Timezone", tzValues, tzNames, timezone, null,errorFields, false)%>
          <%=HTML.textInput("address1", "Practice address line 1", "30", "50", address1, null, errorFields)%>
          <%=HTML.textInput("address2", "Practice address line 2", "30", "50", address2, null, errorFields)%>
          <%=HTML.textInput("suburb", "City / Suburb", "30", "50", suburb, null, errorFields)%>

          <%
              Constants.State stateArray[] = Constants.State.values();
              String stateValues[] = new String[stateArray.length];
              String stateLabels[] = new String[stateArray.length];
              for (int i = 0; i < stateArray.length; i++) {
                  stateValues[i] = stateArray[i].name();
                  stateLabels[i] = stateArray[i].getFullName();
              }


          %>
          <div id="state_au" <%=(!country.equals("AU")) ? "style=\"display:none\"" : ""%>>
          <%=HTML.select("state", "State or Territory", stateValues, stateLabels, state, null, errorFields, true)%>
          </div>

          <div id="state_other" <%=(country.equals("AU")) ? "style=\"display:none\"" : ""%>>
          <%=HTML.textInput("manualState", "State/Province", "30", "50", manualState, null, errorFields)%>
          </div>
          <%=HTML.textInput("postcode", "Postcode", "30", "50", postcode, null, errorFields)%>
          
          <%=HTML.textInput("pnumber", "Practice phone number:", "30", "50", pnumber, null, errorFields)%>
          <%=HTML.textInput("pemail", "Practice email", "30", "50", pemail, null, errorFields)%>
          <%=HTML.textInput("pwebsite", "Practice website", "30", "50", pwebsite, null, errorFields)%>
          
          <%=HTML.textArea("hours", "Opening Hours", hours, 5, "Mon-Fri 8am-4pm")%>
      </fieldset>
      
      <%=HTML.submitButton("Register")%>
  </form>
        </div>
      </div>

  </body>
</html>