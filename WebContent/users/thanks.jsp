<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head><title>Thanks for registering</title></head>
  <body>

    <div class="page-header">
    <h1>Thank you for registering</h1>
</div>
<div class="row">
    <div class="span8 columns">
        An email has been sent to you with instructions for activating your account.
    </div>
    <div class="span8 columns">
        &nbsp;
    </div>
</div>
  </body>
</html>