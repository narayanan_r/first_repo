<%@ page import="au.com.ourbodycorp.Config" %>
<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.model.Constants" %>
<%@ page import="au.com.ourbodycorp.model.User" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="au.com.ourbodycorp.model.managers.UserManager" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.UUID" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String email = (request.getParameter("email") != null) ? request.getParameter("email").trim().toLowerCase() : "";
    String password = (request.getParameter("password") != null) ? request.getParameter("password").trim() : "";
    boolean remember = (request.getParameter("remember") != null) && Boolean.parseBoolean(request.getParameter("remember"));
    String returnUrl = (request.getParameter("return") != null) ? request.getParameter("return") : "";
    
    boolean doDisclaimer = false;

    String error = null;
    session.setAttribute("getSlugUrl", null);
    UserManager mm = new UserManager();
    User m = mm.getUserByEmailOrUserName(email);
    
    if (request.getMethod().equalsIgnoreCase("post")){
    if(email.length() == 0){
    	error = "Please enter a email";
    } else if (!email.matches("[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}")) {
    	error = "Please enter a valid email address";
    } else if(password.length() == 0){
        error = "Please enter a password";
    } else if(mm.getUserByEmailOrUserName(email) == null){
        error = "This user does not exist";
    } else if(m.comparePassword(password) == false){
        error = "Password incorrect";
    } }
    
    if (request.getMethod().equalsIgnoreCase("post") && email.length() > 0 && password.length() > 0) {
//         UserManager mm = new UserManager();
//         User m = mm.getUserByEmailOrUserName(email);
        if (m != null && m.comparePassword(password)) {

            if (m.getStatus() == User.Status.authorised && m.getVerifyKey() == null && (m.isDisclaimed() || session.getAttribute("disclaimer") != null)) {
                session.setAttribute("user", m);
                m.setLastSeen(new Date());
                m.setDisclaimed(true);
                mm.saveUser(m);
                LibraryManager lm = new LibraryManager();
                session.setAttribute("hasCompanies", lm.hasCompanies(m.getId()));
                if (remember) {
                    if (m.getRememberKey() == null || m.getRememberKey().length() == 0) {
                        m.setRememberKey(UUID.randomUUID().toString());
                        mm.saveUser(m);
                    }
                    Cookie c = new Cookie("remember", m.getRememberKey());
                    c.setMaxAge(60 * 60 * 24 * 365);
                    c.setPath("/");
                    c.setDomain(Config.getString("cookieDomain"));
                    response.addCookie(c);
                }
                if (returnUrl.length() > 0 && !returnUrl.startsWith("http")) {
                    response.sendRedirect(returnUrl);
                } else {
                    response.sendRedirect("properties.jsp");
                }
                return;
            } else {
                if (!m.isDisclaimed()) {
                    doDisclaimer = true;
                    error = "Because you have not accepted the disclaimer yet, you must accept it before proceeding.";
                } else if (m.getStatus() == User.Status.applied || m.getVerifyKey() != null) {
                    error = "Your account has not been verified yet, please check your email for instruction to authorise your account. <a href=\"/users/resend.jsp?email=" + email + "\">click here</a> to send the email again.";
                } else if (m.getStatus() == User.Status.deleted) {
                    error = "Your account has been marked as deleted. Contact us if you wish to become a user again.";
                } else if (m.getStatus() == User.Status.disabled) {
                    error = "Your account has been disabled. Contact us for more information.";
                } else if (m.getStatus() == User.Status.rejected) {
                    error = "Your application has been rejected. Contact us for more information.";
                }
            }
        } /* else {
            error = "Login incorrect";
            email = "";
        } */
    } else {
        String reason = (request.getParameter("reason") != null && request.getParameter("reason").length() > 0) ? request.getParameter("reason") : null;
        if (reason != null) {
            Constants.LoginReason lr = Constants.LoginReason.valueOf(reason);
            error = lr.getText();
        }
        UserManager um = new UserManager();
        um.logout(session, response);
    }
%>
<html>
  <head><title>Login</title></head>
  <body>
  <div class="page-header">
    <h1>Login</h1>
  </div>


  <div class="row">
    <div class="span4">
      <h2>Welcome</h2>
      <p>Use your registered email address and password to log in to <%=Config.getString("site.name")%>.</p>
      <p>If you have forgotten your password, <a href="forgot.jsp">click here</a> to reset it.</p>
    </div>

    <div class="span5">
  <%@include file="/WEB-INF/includes/formErrorSimple.jsp"%>
  <form action="login.jsp" method="post" class="form-horizontal">
      <fieldset>
          <legend>Enter email and password</legend>

      <input type="hidden" name="return" value="<%=returnUrl%>"/>
      <%if(request.getParameter("loginEmail") != null){ %>
      	<div class="control-group">
				<label class="control-label" for="email">Email</label>
			<div class="controls">
				<input id="email" readonly="readonly" class="input-xlarge" type="text" value="<%=request.getParameter("loginEmail")%>" name="email" maxlength="100" size="40">
			</div>
		</div>
      <%}else{ %>
          <%=HTML.textInput("email", "Email", "40", "100", email)%>
      <%} %>
          <%=HTML.passwordInput("password", "Password", "40", "40", null, null)%>

          <div class="control-group">
            <label class="control-label" for="optionsCheckbox">&nbsp;</label>
            <div class="controls">
              <label class="checkbox">
                <input type="checkbox" name="remember" <%=(remember) ? "checked" : ""%> value="true" /> Keep me logged in.
              </label>
            </div>
          </div>
          <%
              if (doDisclaimer) {
          %>
          <div class="control-group">
              <label for="country" class="control-label">Disclaimer</label>
              <div class="controls">
                  <div class="span3 well" style="margin-left: 0;">
                    <p>Before loging in, you must <a href="inline/disclaimer.jsp" class="fancybox">read and accept the disclaimer</a>.</p>
                  </div>
              </div>
          </div>
          <%
              }
          %>

      <%=HTML.submitButton("Submit")%>
          </fieldset>
  </form>
        </div>
      </div>
  </body>
</html>