<%@page import="javax.sound.midi.SysexMessage"%>
<%@ page import="au.com.ourbodycorp.Config" %>
<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%@ page import="au.com.ourbodycorp.model.managers.CorporationManager" %>
<%@ page import="au.com.ourbodycorp.model.managers.CountryManager" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="au.com.ourbodycorp.model.managers.UserManager" %>
<%@ page import="java.util.*" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%

System.out.println("register.jsp page--------->");

    String from = Util.notNull(request.getParameter("from"));
    
    if (session.getAttribute("user") != null) {
    	System.out.println("user !=null==========>");
        UserManager um = new UserManager();
        um.logout(session, response);
        response.sendRedirect("register.jsp?from=" + ((from == null)? "" : from));
        
        return;
    }else
    	System.out.println("user ==null==========>");

    long id = (request.getParameter("id") != null) ? Long.parseLong(request.getParameter("id")) : 0;
    String invite = request.getParameter("invite");
    String title = (request.getParameter("title") != null) ? request.getParameter("title").trim() : "";
    String firstName = (request.getParameter("firstName") != null) ? request.getParameter("firstName").trim() : "";
    String lastName = (request.getParameter("lastName") != null) ? request.getParameter("lastName").trim() : "";
    String email = (request.getParameter("email") != null) ? request.getParameter("email").trim().toLowerCase() : "";
    String password = (request.getParameter("password") != null) ? request.getParameter("password").trim() : "";
    String password2 = (request.getParameter("password2") != null) ? request.getParameter("password2").trim() : "";
    String mobile = (request.getParameter("mobile") != null) ? request.getParameter("mobile").trim() : "";
    String company = Util.notNull(request.getParameter("company"));
    boolean b1=password.matches("[a-zA-Z]+");
    boolean b2= password.matches("[0-9]+");
    boolean b3= password.matches("[^A-Za-z0-9 ]+");
    boolean b4= password.matches("[^a-zA-Z]+");
    boolean b5= password.matches("[^0-9]+");
    String disclaimer = Util.notNull(request.getParameter("disclaimer"));
    if (disclaimer.trim().length() > 0){
        session.setAttribute("disclaimer", "accepted");
    }
    else {
        session.removeAttribute("disclaimer");
    }

    HashMap<String, String> errorFields = new HashMap<String, String>();

    if (request.getMethod().equalsIgnoreCase("post")) {
        if (session.getAttribute("disclaimer") == null) {
            errorFields.put("general", "You must accept the disclaimer.");
        }

        if (title.length() == 0) {
            errorFields.put("title", "Please select a title");
        }

        if (firstName.length() < 2) {
            errorFields.put("firstName", "Please enter a first name");
        }

        if (lastName.length() < 2) {
            errorFields.put("lastName", "Please enter a last name");
        }

        if (email.length() == 0) {
            errorFields.put("email", "Please enter an email address");
            errorFields.put("email2", "");
        } 
        else if (!email.matches("[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}")) {
            errorFields.put("email", "Please enter a valid email address");
        }

        if (mobile.length() < 8) {
            errorFields.put("phone", "Please enter a phone number");
            errorFields.put("mobile", "");
        }
        if(b1==true || b2==true || b3==true || b4==true || b5==true){
        	errorFields.put("password", "Your new password must be AlphaNumeric");
           }
        if (password.length() < 6) {
            password = password2 = "";
            errorFields.put("password", "Password too short (6 character minimum)");
            errorFields.put("password2", "");
        }
        if (!password.equals(password2)) {
            password = password2 = "";
            errorFields.put("password", "Passwords don't match");
            errorFields.put("password2", "");
        }

        UserManager mm = new UserManager();
        if (errorFields.size() == 0) {
            if (mm.getUserByEmail(email) != null) {
                errorFields.put("email", "This email address is already registered!");
            }
        }

        try {
            company = Util.safeInput(company, true);
            title = Util.safeInput(title, true);
            firstName = Util.safeInput(firstName, true);
            lastName = Util.safeInput(lastName, true);
            email = Util.safeInput(email, true);
            mobile = Util.safeInput(mobile, true);
        } 
        catch (Exception e) {
            errorFields.put("safeInput", "Characters &gt; and &lt; are not allowed in input.");
        }

        LibraryManager lm = new LibraryManager();
        CorporationManager cm = new CorporationManager();
        String slug = company.toLowerCase().replaceAll("[^a-z0-9]+", "");
        if (from.equals("trial")) {
            if (company.trim().length() == 0) {
                errorFields.put("company", "Please enter your company name");
            } 
            else {
                if (lm.companyExists(company) || cm.getCorporation(slug) != null) {
                    errorFields.put("company", "This company name already exists. Please modify the company name slightly eg. including your city or suburb name.");
                }
            }
        }

        if (errorFields.size() == 0) {
            User newUser = new User();

            newUser.setTitle(title);
            newUser.setFirstName(firstName);
            newUser.setLastName(lastName);
            newUser.setEmail(email);
            newUser.setPassword(Util.encryptPassword(password));
            newUser.setMobile(mobile);
            newUser.setDisclaimed(true);
            
            session.setAttribute("newUser", newUser);
            session.setAttribute("newCompanyName", company);
            session.setAttribute("newCompanySlug", slug);
            session.setAttribute("invite", invite);
            System.out.println("before registerSecond.jsp=======>");
           response.sendRedirect("registerSecond.jsp?from=" + ((from == null)? "" : from));
            
            //return;
        }
    }
%>
<html>
  <head>
      <title>Register<%=(from.equals("trial")) ? " for your free trial" : "" %></title>
      <style type="text/css">
        body {
            background-color: #ededed;
            background-image: none;
        }
    </style>
   <script type="text/javascript">
	   var company=document.getElementById("compnay").value;
	   company.onkeyup = function(){
		    document.getElementById('name').innerHTML = company.value;
		}
    </style>
  </head>
  <body>
  <div class="page-header">
    <h2>Welcome to Fiizio</h2>
  </div>

  <div style="font-size: 15px; margin-top: -20px; margin-bottom: 20px;">
      You're only moments away from simplifying the way you manage patients and exercises. We just need a few quick details first.
  </div>
  
  <div class="row">
    <div class="span9 columns">
        <%@ include file="/WEB-INF/includes/formError.jsp" %>
  <form action="register.jsp" method="post" class="form-horizontal" onsubmit="return message();" >
      <input type="hidden" name="invite" value="<%=Util.formValue(invite)%>"/>
      <input type="hidden" name="from" value="<%=from%>"/>
      
      <p style="margin-left: 160px; font-weight: bold; margin-bottom: 10px; font-size: 15px; margin-top: 10px;">Tell us about yourself:</p>
      <fieldset>
         <div class="bs-example">
           <div class="alert alert-info fade in" style="width: 250px;position: absolute;margin-left: 450px;">
           <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong id="name"></strong> This Will be Main Location
           </div>
		   </div>
           
          <%
              if (from.equals("trial")) {
          %>
          <%=HTML.textInput("company", "Company Name", "30", "50", company, null, errorFields)%>
          <%
              }
          %>
          <%=HTML.select("title", "Title", User.titles, User.titles, title, null, errorFields, true)%>

          <%=HTML.textInput("firstName", "First Name", "30", "50", firstName, null, errorFields)%>
          <%=HTML.textInput("lastName", "Last Name", "30", "50", lastName, null, errorFields)%>
          <%=HTML.textInput("mobile", "Mobile Phone Number", "30", "15", mobile, null, errorFields)%>
      </fieldset>
      
      <p style="margin-left: 160px; font-weight: bold; margin-bottom: 10px; font-size: 15px; margin-top: 30px;">These details will be used to let you login to our site:</p>
      <fieldset>
      <%if(session.getAttribute("newUser") != null){ %>
      	<div class="control-group">
				<label class="control-label" for="email">Email</label>
			<div class="controls">
				<input id="email" readonly="readonly" class="input-xlarge" type="text" value="<%=session.getAttribute("newUser").toString()%>" name="email" maxlength="100" size="30">
			</div>
		</div>
	  <%}else{ %>
          <%=HTML.textInput("email", "Email", "30", "100", email, null, errorFields)%>
          <%} %>
          </fieldset>
      <fieldset>
          <%=HTML.passwordInput("password", "Password", "30", "15", password, null, errorFields)%>
          <%=HTML.passwordInput("password2", "Confirm Password", "30", "15", password2, null, errorFields)%>
      </fieldset>
      <fieldset>
          <div class="control-group">
              <div class="controls">
                  <div class="span4" style="margin-left: 0;">
                      <label class="checkbox"><input type="checkbox" name="disclaimer" <%= (disclaimer.trim().length() > 0)? "checked" :""%> /> I have read and accept <a href="inline/disclaimer.jsp" class="fancybox">the disclaimer</a></label>
                  </div>
              </div>
          </div>
      </fieldset>
      <div class="form-actions">
       <button name="submit" type="submit" value="continue"  class="btn btn-primary" >continue</button>
       </div>  
        <%--  <%=HTML.submitButton("Continue")%>   --%>
  </form>
        </div>
      </div>

  </body>
</html>