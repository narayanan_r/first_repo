<%@ page import="au.com.ourbodycorp.model.Invitation" %>
<%@ page import="au.com.ourbodycorp.model.User" %>
<%@ page import="au.com.ourbodycorp.model.managers.CorporationManager" %>
<%@ page import="au.com.ourbodycorp.model.managers.UserManager" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    UserManager um = new UserManager();
    um.logout(session, response);
    User u = um.getUserByVerifyKey(request.getParameter("key"));
    if (u != null) {
        u.setVerifyKey(null);
        u.setStatus(User.Status.authorised);
        um.saveUser(u);

        CorporationManager cm = new CorporationManager();
        Invitation invite = cm.getInvitation(u.getId(), true);
        if (invite != null) {
            cm.accept(invite, u);
        }

        response.sendRedirect("verified.jsp");
        return;
    }
%>
<html>
<head><title>Verification Failed</title></head>
<body>
    <div class="page-header">
    <h1>Verification Failed</h1>
</div>
<div class="row">
    <div class="span8">
        The verification key supplied has not been found. It could be incorrect or the account has already been verified.
    </div>
    <div class="span8">
        &nbsp;
    </div>
</div>
</body>
</html>