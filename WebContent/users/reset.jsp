<%@ page import="au.com.ourbodycorp.model.User" %>
<%@ page import="au.com.ourbodycorp.model.managers.UserManager" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    UserManager um = new UserManager();
    User u = um.getUserByPasswordResetKey(request.getParameter("key"));
    if (u != null) {
        session.setAttribute("resetForUser", u);
        response.sendRedirect("reset2.jsp");
        return;
    }
%>
<html>
<head><title>Reset Password</title></head>
<body>
  <div class="page-header">
    <h1>Password reset not found</h1>
</div>
<div class="row">
    <div class="span8 columns">
        The password reset key supplied has not been found. It could be that it has expired,
        in that case, <a href="forgot.jsp">please start the reset process again</a>.
    </div>
    <div class="span8 columns">
        &nbsp;
    </div>
</div>
</body>
</html>