<%@ page import="au.com.ourbodycorp.model.managers.UserManager" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    UserManager um = new UserManager();
    um.logout(session, response);
%>
<html>
  <head><title>Account Verified</title></head>
  <body>

     <div class="page-header">
    <h1>Account Verified</h1>
</div>
<div class="row">
    <div class="span4">
        <p>
      Thank you, your account has now been verified.
  </p>
  <p>
     You may now <a href="login.jsp">login</a> to your account.
  </p>
    </div>
    <div class="span4">
        &nbsp;
    </div>
</div>

  </body>
</html>