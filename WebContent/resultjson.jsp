<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@page import="com.eway.json.bean.GetAccessCodeResultResponse"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>Result Page</title>

<link href="Styles/Site.css" rel="stylesheet" type="text/css" />
<link href="Styles/jquery-ui-1.8.11.custom.css" rel="stylesheet" type="text/css" />
<script src="Scripts/jquery-1.4.4.min.js" type="text/javascript"></script>
<script src="Scripts/jquery-ui-1.8.11.custom.min.js" type="text/javascript"></script>
<script src="Scripts/jquery.ui.datepicker-en-GB.js" type="text/javascript"></script>

</head>
<body>

	<%
		GetAccessCodeResultResponse GetAccessCodeResultResponse = (GetAccessCodeResultResponse) session.getAttribute("GetResponse");
	%>

	<center>
		<div id="outer">
			<div id="toplinks">
				<img alt="eWAY Logo" class="logo" src="Images/companylogo.gif" width="960px" height="65px" />
			</div>
			<div id="main">

				<div id="titlearea">
					<h2>Sample Response</h2>
				</div>


				<div id="maincontent">
					<div class="response">

						<div class="fields">
							<label for="lblAccessCode"> Access Code</label> <label id="lblAccessCode"><%=GetAccessCodeResultResponse.getAccessCode()%></label>
						</div>

						<div class="fields">
							<label for="lblAuthorisationCode"> Authorisation Code</label> <label id="lblAuthorisationCode"><%=(GetAccessCodeResultResponse.getAuthorisationCode() != null && !GetAccessCodeResultResponse.getAuthorisationCode().equals("") ? GetAccessCodeResultResponse.getAuthorisationCode() : "")%></label>
						</div>

						<div class="fields">
							<label for="lblInvoiceNumber"> Invoice Number</label> <label id="lblInvoiceNumber"><%=GetAccessCodeResultResponse.getInvoiceNumber()%></label>
						</div>

						<div class="fields">
							<label for="lblInvoiceReference"> Invoice Reference</label> <label id="lblInvoiceReference"><%=GetAccessCodeResultResponse.getInvoiceReference()%></label>
						</div>
						<%
							int length = GetAccessCodeResultResponse.Options.length;
							for (int i = 0; i < length; i++)
							{
						%>
								<div class="fields">
									<label for="lblOption1"> Option<%=i+1%></label> <label id="lblOption<%=i+1%>"><%=(GetAccessCodeResultResponse.Options[i].getValue() != null && !GetAccessCodeResultResponse.Options[i].getValue().equals("") ? GetAccessCodeResultResponse.Options[i].getValue() : "")%></label>
								</div>
						<%
							}
						%>

						<div class="fields">
							<label for="lblResponseCode"> Response Code</label> <label id="lblResponseCode"><%=GetAccessCodeResultResponse.getResponseCode()%></label>
						</div>

						<div class="fields">
							<label for="lblResponseMessage"> Response Message</label> <label id="lblResponseMessage"><%=GetAccessCodeResultResponse.getResponseMessage()%></label>
						</div>

						<div class="fields">
							<label for="lblTokenCustomerID"> TokenCustomerID </label> <label id="lblTokenCustomerID"><%=(GetAccessCodeResultResponse.getTokenCustomerID() != null && !GetAccessCodeResultResponse.getTokenCustomerID().equals("") ? GetAccessCodeResultResponse.getTokenCustomerID() : "")%></label>
						</div>

						<div class="fields">
							<label for="lblTotalAmount"> Total Amount</label> <label id="lblTotalAmount"><%=GetAccessCodeResultResponse.getTotalAmount()%></label>
						</div>

						<div class="fields">
							<label for="lblTransactionID"> TransactionID</label> <label id="lblTransactionID"><%=GetAccessCodeResultResponse.getTransactionID()%></label>
						</div>

						<div class="fields">
							<label for="lblTransactionStatus"> Transaction Status</label> <label id="lblTransactionStatus"> 
							<%
						 	if (GetAccessCodeResultResponse.getTransactionStatus() != null && GetAccessCodeResultResponse.getTransactionStatus().equalsIgnoreCase("true"))
						 	{
						 		out.print("True");
						 	}
						 	else
						 	{
						 		out.print("False");
						 	}
						 %>
							</label>
						</div>

						<div class="fields">
							<label for="lblBeagleScore"> Beagle Score</label> <label id="lblBeagleScore"><%=GetAccessCodeResultResponse.getBeagleScore()%></label>
						</div>

					</div>
				</div>

				<br /> <br />
				<a href="personalDetail.jsp">[Start Over]</a>

				<div id="maincontentbottom"></div>

			</div>
			<div id="footer"></div>
		</div>
	</center>

</body>
</html>