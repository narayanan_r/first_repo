<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="com.eway.json.bean.CreateAccessCodeResponse"%>
<%@page import="java.util.ResourceBundle"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>Payment</title>

	<link href="Styles/Site.css" rel="stylesheet" type="text/css" />
    <!-- Include for Ajax Calls -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js" type="text/javascript"></script>
    <!-- This is the JSONP script include on the eWAY Rapid API Server - this must be included to use the Rapid API via JSONP -->
   <script type="text/javascript" src="https://api.sandbox.ewaypayments.com/JSONP/v1/js"></script>

</head>
<body>

	<%
		//final ResourceBundle configResource = ResourceBundle.getBundle("config");
		//String redirectURL = configResource.getString("");
    	String totalAmount = (String) session.getAttribute("TotalAmount");
    	String invoiceReference = (String) session.getAttribute("InvoiceReference");
    	CreateAccessCodeResponse CreateAccessCodeResponse = (CreateAccessCodeResponse) session.getAttribute("ResponseText");
    	
    	System.out.println("totalAmount: " + totalAmount);
    	System.out.println("invoiceReference: " + invoiceReference);
    	System.out.println("accessCode: " + CreateAccessCodeResponse.AccessCode);
	%>
	
	<form id="form1" action="<%=CreateAccessCodeResponse.FormActionURL %>" method='post'>
	    <center>
	        <div id="outer">
	            <div id="toplinks">
	                <img alt="eWAY Logo" class="logo" src="Images/merchantlogo.gif" width="926px" height="65px" />
	            </div>
	            <div id="main">
	                <div id="titlearea">
	                    <h2>Sample Merchant Checkout</h2>
	                </div>

                <div id="maincontent">
                    <div class="transactioncustomer">
                        <div class="header first">
                            Customer Address
                        </div>
                        <div class="fields">
                            <label for="lblStreet">Street</label>
                            <label id="lblStreet"><%=CreateAccessCodeResponse.Customer.getStreet1() %></label>
                        </div>
                        <div class="fields">
                            <label for="lblCity">
                                City</label>
                            <label id="lblStreet"><%=CreateAccessCodeResponse.Customer.getCity() %></label>
                        </div>
                        <div class="fields">
                            <label for="lblState">
                                State</label>
                            <label id="lblState"><%=CreateAccessCodeResponse.Customer.getState() %></label>
                        </div>
                        <div class="fields">
                            <label for="lblPostcode">
                                Post Code</label>
                            <label id="lblPostcode"><%=CreateAccessCodeResponse.Customer.getPostalCode() %></label>
                        </div>
                        <div class="fields">
                            <label for="lblCountry">
                                Country</label>
                            <label id="lblCountry"><%=CreateAccessCodeResponse.Customer.getCountry() %></label>
                        </div>
                        <div class="fields">
                            <label for="lblEmail">
                                Email</label>
                            <label id="lblEmail"><%=CreateAccessCodeResponse.Customer.getEmail() %></label>
                        </div>
                        <div class="fields">
                            <label for="lblPhone">
                                Phone</label>
                            <label id="lblPhone"><%=CreateAccessCodeResponse.Customer.getPhone() %></label>
                        </div>
                        <div class="fields">
                            <label for="lblMobile">
                                Mobile</label>
                            <label id="lblMobile"><%=CreateAccessCodeResponse.Customer.getMobile() %></label>
                        </div>
                        <div class="header">
                            Payment Details
                        </div>
                        <div class="fields">
                            <label for="lblAmount">
                                Total Amount</label>
                            <label id="lblAmount"><%=CreateAccessCodeResponse.Payment.getTotalAmount() %></label>
                        </div>
                        <div class="fields">
                            <label for="lblInvoiceReference">
                                Invoice Reference</label>
                            <label id="lblInvoiceReference"><%=invoiceReference %></label>
                        </div>
                    </div>
                    <div class="transactioncard">
                        <div class="header first">
                            Customer Details</div>
                        <div class="fields">
                            <label for="lblTitle">
                                Title</label>
                            <label id="lblTitle"><%=CreateAccessCodeResponse.Customer.getTitle() %></label>
                        </div>
                        <div class="fields">
                            <label for="lblFirstName">
                                First Name</label>
                            <label id="lblFirstName"><%=CreateAccessCodeResponse.Customer.getFirstName() %></label>
                        </div>
                        <div class="fields">
                            <label for="lblLastName">
                                Last Name</label>
                            <label id="lblLastName"><%=CreateAccessCodeResponse.Customer.getLastName() %></label>
                        </div>
                        <div class="fields">
                            <label for="lblCompanyName">
                                Company Name</label>
                            <label id="lblCompanyName"><%=CreateAccessCodeResponse.Customer.getCompanyName() %></label>
                        </div>
                        <div class="fields">
                            <label for="lblJobDescription">
                                Job Description</label>
                            <label id="lblJobDescription"><%=CreateAccessCodeResponse.Customer.getJobDescription() %></label>
                        </div>
                        <div class="header">
                            Card Details
                        </div>
                        <!-- The following fields are the ones that eWAY looks for in the POSTed data when the form is submitted. -->

                        <!-- This field should contain the access code received from eWAY -->
                        <input type='hidden' name='EWAY_ACCESSCODE' value="<%=CreateAccessCodeResponse.AccessCode %>" />
                        
                        <div class="fields">
                            <label for="EWAY_CARDNAME">
                                Card Holder</label>
                            <input type='text' name='EWAY_CARDNAME' id='EWAY_CARDNAME' value="<%=(CreateAccessCodeResponse.Customer.getCardName() != null && !CreateAccessCodeResponse.Customer.getCardName().equals("") ? CreateAccessCodeResponse.Customer.getCardName() : "Test User") %>" />
                        </div>
                        <div class="fields">
                            <label for="EWAY_CARDNUMBER">
                                Card Number</label>
                            <input type='text' name='EWAY_CARDNUMBER' id='EWAY_CARDNUMBER' value="<%=(CreateAccessCodeResponse.Customer.getCardNumber() != null && !CreateAccessCodeResponse.Customer.getCardNumber().equals("") ? CreateAccessCodeResponse.Customer.getCardNumber() : "1111222233334444") %>" />
                        </div>
                        <div class="fields">
                            <label for="EWAY_CARDEXPIRYMONTH">
                                Expiry Date</label>
                            <select id="EWAY_CARDEXPIRYMONTH" name="EWAY_CARDEXPIRYMONTH">
                                <%
                                	int expiryMonth = 1;
                                
                                	if(CreateAccessCodeResponse.Customer.getCardExpiryMonth() != null && !CreateAccessCodeResponse.Customer.getCardExpiryMonth().equals(""))
                                	{
                                		expiryMonth = Integer.parseInt(CreateAccessCodeResponse.Customer.getCardExpiryMonth());
                                	}
                                	else
                                	{
                                		Date date = new Date();
                                		Calendar cal = Calendar.getInstance();
                                		expiryMonth = cal.get(Calendar.MONTH) + 1;
                                	}
                                	
                                	for(int i = 1; i <= 12; i++)
                                	{
                                		out.print("<option value='" + i + "'");
                                		
                                		if(expiryMonth == i)
                                		{	
                                			out.print(" selected='selected'");
                                		}
                                		
                                		out.print(">" + i + "</option>\n");
                                	}
                                %>
                            </select>
                            /
                            <select id="EWAY_CARDEXPIRYYEAR" name="EWAY_CARDEXPIRYYEAR">
                                <%
                                	Calendar cal = Calendar.getInstance();
                                	int currentYear = cal.get(Calendar.YEAR);
                                	int cardExpiryYear = (CreateAccessCodeResponse.Customer.getCardExpiryYear() != null && !CreateAccessCodeResponse.Customer.getCardExpiryYear().equals("") ? Integer.parseInt(CreateAccessCodeResponse.Customer.getCardExpiryYear()) : 0);
                                	int expiryYear = currentYear + 11;
                                	
                                	for(int i = currentYear; i <= expiryYear; i++)
                                	{
                                		out.print("<option value='" + i + "'");
                                		
                                		if(cardExpiryYear == i)
                                		{	
                                			out.print(" selected='selected'");
                                		}
                                		
                                		out.print(">" + i + "</option>\n");
                                	}
                                %>
                            </select>
                        </div>
                        <div class="fields">
                            <label for="EWAY_CARDSTARTMONTH">
                                Valid From Date</label>
                            <select id="EWAY_CARDSTARTMONTH" name="EWAY_CARDSTARTMONTH">
                                <%
                                	int expiryMonth1 = 0;
                                	
                                	if(CreateAccessCodeResponse.Customer.getCardStartMonth() != null && !CreateAccessCodeResponse.Customer.getCardStartMonth().equals(""))
                                	{
                                		expiryMonth1 = Integer.parseInt(CreateAccessCodeResponse.Customer.getCardExpiryMonth());
                                	}
                                	
                                	out.print("<option></option>");
                                	
                                	for(int i = 1; i <= 12; i++)
                                	{
                                		out.print("<option value='" + i + "'");
                                		
                                		if(expiryMonth1 == i)
                                		{	
                                			out.print(" selected='selected'");
                                		}
                                		
                                		out.print(">" + i + "</option>\n");
                                	}
                                %>
                            </select>
                            /
                            <select id="EWAY_CARDSTARTYEAR" name="EWAY_CARDSTARTYEAR">
                            	<%
                                	Calendar cal1 = Calendar.getInstance();
                                	int currentYear1 = cal1.get(Calendar.YEAR);
                                	int expiryYear1 = currentYear1 - 11;
                                	
                                	out.print("<option></option>");
                                	
                                	for(int i = currentYear1; i >= expiryYear1; i--)
                                	{
                                		out.print("<option value='" + i + "'");
                                		
                                		if(CreateAccessCodeResponse.Customer.getCardStartYear() != null && !CreateAccessCodeResponse.Customer.getCardStartYear().equals(""))
                                		{
                                			int cardStartYear = (CreateAccessCodeResponse.Customer.getCardStartYear() != null && !CreateAccessCodeResponse.Customer.getCardStartYear().equals("") ? Integer.parseInt(CreateAccessCodeResponse.Customer.getCardStartYear()) : 0);
	                                		
                                			if(cardStartYear == i)
	                                		{	
	                                			out.print(" selected='selected'");
	                                		}
                                		}
                                		
                                		out.print(">" + i + "</option>\n");
                                	}
                                %>
                            </select>
                        </div>
                        <div class="fields">
                            <label for="EWAY_CARDISSUENUMBER">
                                Issue Number</label>
                            <input type='text' name='EWAY_CARDISSUENUMBER' id='EWAY_CARDISSUENUMBER' value="<%=(CreateAccessCodeResponse.Customer.getCardIssueNumber() != null && !CreateAccessCodeResponse.Customer.getCardIssueNumber().equals("") ? CreateAccessCodeResponse.Customer.getCardNumber() : "22") %>" maxlength="2" style="width:40px;"/> <!-- This field is optional but highly recommended -->
                        </div>
                        <div class="fields">
                            <label for="EWAY_CARDCVN">
                                CVN</label>
                            <input type='text' name='EWAY_CARDCVN' id='EWAY_CARDCVN' value="123" maxlength="4" style="width:40px;"/> <!-- This field is optional but highly recommended -->
                        </div>
                    </div>
                    <div class="paymentbutton">
                        <br />
                        <br />
                        <input type='submit' id="btnSubmit" name='btnSubmit' value="Submit" />
                        <input id="Process" type="button" value="Submit Via Ajax" />
                    </div>
                </div>
                <div id="maincontentbottom">
                </div>
            </div>
        </div>
    </center>
    </form>
    
    <script type="text/javascript">

        $(function () {

            // if something goes wrong, then you should redirect to your result/query page to query eWAY for the status of the request
            var urlToRedirectOnError = "personalDetail.jsp";

            // on button click
            $('#Process').on('click', function () {

                // this is the button - prevent double click
                $(this).attr('disabled', "disabled");

                // call eWAY to process the request
                eWAY.process(document.forms[0], {
                    autoRedirect: false,
                    onComplete: function (data) {
                        // this is a callback to hook into when the requests completes
                        alert('The JSONP request has completed.\r\n\r\nCLick OK to redirect and complete the process');
                        window.location.replace(data.RedirectUrl);
                    },
                    onError: function (e) {
                        // this is a callback you can hook into when an error occurs
                        alert('There was an error processing the request\r\n\r\nClick OK to redirect to your result/query page');
                        window.location.replace(urlToRedirectOnError);
                    },
                    onTimeout: function (e) {
                        // this is a callback you can hook into when the request times out
                        alert('The request has timed out\r\n\r\nClick OK to redirect to your result/query page.');
                        window.location.replace(urlToRedirectOnError);
                    }
                });

            });
        });

    </script>
</body>	


</html>