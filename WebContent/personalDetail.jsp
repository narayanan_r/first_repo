<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.ResourceBundle"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>Personal Detail</title>

	<link href="styles/Site.css" rel="stylesheet" type="text/css" />
	<link href="styles/jquery-ui-1.8.11.custom.css" rel="stylesheet" type="text/css" />
	<script src="Scripts/jquery-1.4.4.min.js" type="text/javascript"></script>
	<script src="Scripts/jquery-ui-1.8.11.custom.min.js" type="text/javascript"></script>
	<script src="Scripts/jquery.ui.datepicker-en-GB.js" type="text/javascript"></script>
	<script type="text/javascript" src="Scripts/tooltip.js"></script>

</head>
<body>

	<%
		final ResourceBundle configResource = ResourceBundle.getBundle("config");
		
		String redirectURL = configResource.getString("redirectURL");
		String method = configResource.getString("requestMethod");
		String format = configResource.getString("requestFormat");
		
		String formActionURL = "";

		if (method.equalsIgnoreCase("POST") && format.equalsIgnoreCase("JSON"))
		{
			formActionURL = "RapidAPIJSONService";
		}
		else if (method.equalsIgnoreCase("POST") && format.equalsIgnoreCase("XML"))
		{
			formActionURL = "RapidAPIXMLService";
		}
		/* else if (method.equalsIgnoreCase("REST") && format.equalsIgnoreCase("XML"))
		{
		}
		else if (method.equalsIgnoreCase("REST") && format.equalsIgnoreCase("JSON"))
		{
		}
		else if (method.equalsIgnoreCase("SOAP"))
		{
		}
		else if(method.equalsIgnoreCase("RPC") && format.equalsIgnoreCase("XML"))
		{
		} */
	%>

	<form method="post" action="<%=formActionURL%>">
		<center>
			<div id="outer">
				<div id="toplinks">
					<img alt="eWAY Logo" class="logo" src="images/companylogo.gif" width="960px" height="65px" />
				</div>
				<div id="main">

					<div id="titlearea">
						<h2>Sample Merchant Page</h2>
					</div>
					
					<%
						if (session != null && session.getAttribute("errors") != null)
						{
							String[] errors = session.getAttribute("errors").toString().split(",");
							String errorDescription = "";
							
							for (int i = 0; i < errors.length; i++)
							{
								errorDescription = configResource.getString(errors[i]);
					%>			
								<div style="width: 100%;">
									<label style="color:red; width: 100%;">
										<%=errors[i] + " : " + errorDescription %>
									</label>
								</div>
					<%			
							}
							
							session.invalidate();
						}
					%>
					
					<div id="maincontent">
					
						<div class="transactioncustomer">
							
							<div class="header first">Request Options</div>
							<div class="fields">
								<label for="txtRedirectURL">Redirect URL</label> <input id="txtRedirectURL" name="txtRedirectURL" type="text" value="<%=redirectURL%>" />
							</div>
							
							<div class="header">Payment Details</div>
							<div class="fields">
								<label for="txtAmount">Amount &nbsp;<img src="images/question.gif" alt="Find out more" id="amountTipOpener" border="0" /></label> <input id="txtAmount" name="txtAmount" type="text" value="100" />
							</div>
							
							<div class="fields">
								<label for="txtCurrencyCode">Currency Code </label> <input id="txtCurrencyCode" name="txtCurrencyCode" type="text" value="" />
							</div>
							
							<div class="fields">
								<label for="txtInvoiceNumber">Invoice Number</label> <input id="txtInvoiceNumber" name="txtInvoiceNumber" type="text" value="Inv 21540" />
							</div>
							
							<div class="fields">
								<label for="txtInvoiceReference">Invoice Reference</label> <input id="txtInvoiceReference" name="txtInvoiceReference" type="text" value="513456" />
							</div>
							
							<div class="fields">
								<label for="txtInvoiceDescription">Invoice Description</label> <input id="txtInvoiceDescription" name="txtInvoiceDescription" type="text" value="Individual Invoice Description" />
							</div>
							
							<div class="header">Custom Fields</div>
							<div class="fields">
								<label for="txtOption1">Option 1</label> <input id="txtOption1" name="txtOption1" type="text" value="Option1" />
							</div>
							
							<div class="fields">
								<label for="txtOption2">Option 2</label> <input id="txtOption2" name="txtOption2" type="text" value="Option2" />
							</div>
							
							<div class="fields">
								<label for="txtOption3">Option 3</label> <input id="txtOption3" name="txtOption3" type="text" value="Option3" />
							</div>
							
							<div class="header">Shipping Information</div>
							<div class="fields">
								<label for="txtOption1">First Name</label> <input id="txtShippingFirstName" name="txtShippingFirstName" type="text" value="John" />
							</div>
							
							<div class="fields">
								<label for="txtOption2">Last Name</label> <input id="txtShippingLastName" name="txtShippingLastName" type="text" value="Doe" />
							</div>
							
							<div class="fields">
								<label for="txtOption3">Street 1</label> <input id="txtShippingStreet1" name="txtShippingStreet1" type="text" value="9/10 St Andrew" />
							</div>
							
							<div class="fields">
								<label for="txtOption3">Street 2</label> <input id="txtShippingStreet2" name="txtShippingStreet2" type="text" value="Square" />
							</div>
							
							<div class="fields">
								<label for="txtOption3">City</label> <input id="txtShippingCity" name="txtShippingCity" type="text" value="Edinburgh" />
							</div>
							
							<div class="fields">
								<label for="txtOption3">State</label> <input id="txtShippingState" name="txtShippingState" type="text" value="" />
							</div>
							
							<div class="fields">
								<label for="txtOption3">Country</label> <input id="txtShippingCountry" name="txtShippingCountry" type="text" value="gb" />
							</div>
							
							<div class="fields">
								<label for="txtOption3">Postal Code</label> <input id="txtShippingPostalCode" name="txtShippingPostalCode" type="text" value="EH2 2AF" />
							</div>
							
							<div class="fields">
								<label for="txtOption3">Email</label> <input id="txtShippingEmail" name="txtShippingEmail" type="text" value="your@email.com" />
							</div>
							
							<div class="fields">
								<label for="txtOption3">Phone</label> <input id="txtShippingPhone" name="txtShippingPhone" type="text" value="0131 208 0321" />
							</div>
							
						</div>
						
						<div class="transactioncard">
						
							<div class="header first">Customer Details</div>
							<div class="fields">
								<label for="txtTokenCustomerID">Token Customer ID &nbsp;<img src="images/question.gif" alt="Find out more" id="tokenCustomerTipOpener" border="0" /></label> <input id="txtTokenCustomerID" name="txtTokenCustomerID" type="text" />
							</div>
							
							<div class="fields">
								<label for="ddlTitle">Title</label> <select id="ddlTitle" name="ddlTitle">
									<option></option>
									<option value="Mr." selected="selected">Mr.</option>
									<option value="Miss">Miss</option>
									<option value="Mrs.">Mrs.</option>
								</select>
							</div>
							
							<div class="fields">
								<label for="txtCustomerRef">Customer Reference</label> <input id="txtCustomerRef" name="txtCustomerRef" type="text" value="A12345" />
							</div>
							
							<div class="fields">
								<label for="txtFirstName">First Name</label> <input id="txtFirstName" name="txtFirstName" type="text" value="John" />
							</div>
							
							<div class="fields">
								<label for="txtLastName">Last Name</label> <input id="txtLastName" name="txtLastName" type="text" value="Doe" />
							</div>
							
							<div class="fields">
								<label for="txtCompanyName">Company Name</label> <input id="txtCompanyName" name="txtCompanyName" type="text" value="WEB ACTIVE" />
							</div>
							
							<div class="fields">
								<label for="txtJobDescription">Job Description</label> <input id="txtJobDescription" name="txtJobDescription" type="text" value="Developer" />
							</div>
							
							<div class="header">Customer Address</div>
							<div class="fields">
								<label for="txtStreet">Street</label> <input id="txtStreet" name="txtStreet" type="text" value="15 Smith St" />
							</div>
							
							<div class="fields">
								<label for="txtCity">City</label> <input id="txtCity" name="txtCity" type="text" value="Phillip" />
							</div>
							
							<div class="fields">
								<label for="txtState">State</label> <input id="txtState" name="txtState" type="text" value="ACT" />
							</div>
							
							<div class="fields">
								<label for="txtPostalcode">Post Code</label> <input id="txtPostalcode" name="txtPostalcode" type="text" value="2602" />
							</div>
							
							<div class="fields">
								<label for="txtCountry">Country</label> <input id="txtCountry" name="txtCountry" type="text" value="au" maxlength="2" />
							</div>
							
							<div class="fields">
								<label for="txtEmail">Email</label> <input id="txtEmail" name="txtEmail" type="text" value="" />
							</div>
							
							<div class="fields">
								<label for="txtPhone">Phone</label> <input id="txtPhone" name="txtPhone" type="text" value="1800 10 10 65" />
							</div>
							
							<div class="fields">
								<label for="txtMobile">Mobile</label> <input id="txtMobile" name="txtMobile" type="text" value="1800 10 10 65" />
							</div>
							
							<div class="fields">
								<label for="txtFax">Fax</label> <input id="txtFax" name="txtFax" type="text" value="02 9852 2244" />
							</div>
							
							<div class="fields">
								<label for="txtUrl">Website</label> <input id="txtUrl" name="txtUrl" type="text" value="http://www.yoursite.com" />
							</div>
							
							<div class="fields">
								<label for="txtComments">Comments</label>
								<textarea id="txtComments" name="txtComments">Some comments here</textarea>
							</div>
							
							<div class="header">Method</div>
							<div class="fields">
								<label for="ddlMethod">Method Type</label> <select id="ddlMethod" name="ddlMethod" style="width: 140px;">
									<option value="ProcessPayment">ProcessPayment</option>
									<option value="CreateTokenCustomer">CreateTokenCustomer</option>
									<option value="UpdateTokenCustomer">UpdateTokenCustomer</option>
									<option value="TokenPayment">TokenPayment</option>
								</select>
							</div>
							
						</div>
						
						<div class="button">
							<br /> <br /> <input type="submit" id="btnSubmit" name="btnSubmit" value="Get Access Code" />
						</div>
						
					</div>
					
					<div id="maincontentbottom"></div>
					<div id="amountTip" style="font-size: 8pt !important">The amount in cents. For example for an amount of $1.00, enter 100</div>
					<div id="tokenCustomerTip" style="font-size: 8pt !important">If this field has a value, the details of an existing customer will be loaded when the request is sent.</div>
					<div id="saveTokenTip" style="font-size: 8pt !important">If this field is checked, the details in the customer fields will be used to either create a new token customer, or (if Token Customer ID has a value) update an existing customer.</div>

				</div>
				
				<div id="footer"></div>
			</div>
		</center>
	</form>
</body>
</html>