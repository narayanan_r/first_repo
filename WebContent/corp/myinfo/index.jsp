<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="java.util.List" %>
<%

    CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");

    User u = cu.getUser();
    Corporation corp = cu.getCorporation();
    
    String type = Util.notNull(request.getParameter("type"));
    String bodyPart = Util.notNull(request.getParameter("bp"));
    boolean exclusive = request.getParameter("exclusive") != null && Boolean.parseBoolean(request.getParameter("exclusive"));

    String[][] breadcrumbs = {{"/corp/", corp.getName()}, {"/corp/myinfo/", "MyInfo Articles"}, {null, "Articles"}};

    LibraryManager lm = new LibraryManager();

    Exercise.BodyPart bp = null;
    Exercise.Type tp = null;
    try {
        bp = Exercise.BodyPart.valueOf(bodyPart);
    } catch (Exception e) {
        bp = null;
    }

    try {
        tp = Exercise.Type.valueOf(type);
    } catch (Exception e) {
        tp = null;
    }
    String activity_page="corp/myinfo/index.jsp";
	String corp_id=String.valueOf(corp.getId());
	String corp_name=corp.getName();
	String corp_loc=corp.getSuburb();
	String user_id=String.valueOf(u.getId());
	String user_name=u.getFullName();
	String invitor_id="";
	String invitor_name="---";
	String flag="view";
    String from="---";
	String to="---";
	 if(exclusive){
    	try{
        	//Util.allActivitiesLogs("My Articles", u.getId(), corp.getName(), "Show Only My Articles");
    		Util.allActivitiesLogs2("My Articles",activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,"Show Only My Articles");
        }catch(Exception ee){
        	
        } 
    }else{
    	 try{
        	//Util.allActivitiesLogs("My Articles", u.getId(), corp.getName(), "View My Info Library");
    		Util.allActivitiesLogs2("My Articles",activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,"View My Info Library");
        }catch(Exception ee){
        	
        } 
    }
    List<MyInfo> infos = lm.listInfo(corp.getId(), bp, tp, exclusive);



%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head><title>My Info Library</title></head>
<body>
<div class="page-header">
    <h1>My Info Library</h1>
</div>
<%@include file="/WEB-INF/includes/breadcrumbs.jsp"%>

<div class="row">
    <div class="span9">

        <form class="form-inline" action="index.jsp" name="ev">
            <input type="hidden" name="bp" value="<%=bodyPart%>">
            <input type="hidden" name="type" value="<%=type%>">
            <a href="edit.jsp?type=<%=type%>&bp=<%=bodyPart%>" class="btn btn-primary" style="margin-right: 25px;"><i class="icon-plus icon-white"></i> Add Article</a>
            <label class="checkbox"><input type="checkbox" name="exclusive" value="true" <%=exclusive?"checked":""%> onchange="document.forms.ev.submit();"> show only my articles</label>
        </form>
    </div>
</div>
<%--
<div class="row" style="margin-top: 10px;">
    <div class="span8 offset1">
        <ul class="nav nav-tabs" style="margin-bottom: 0;">
            <li <%=(type.isEmpty()) ? "class=\"active\"" : ""%>>
                <a href="?type=&bp=<%=bodyPart%>">All Types</a>
            </li>
            <%
                for (Exercise.Type et : Exercise.Type.values()) {
            %>
            <li <%=(type.equals(et.name())) ? "class=\"active\"" : ""%>>
                <a href="?type=<%=et.name()%>&bp=<%=bodyPart%>"><%=et.getValue()%></a>
            </li>
            <%
                }
            %>
        </ul>
    </div>
</div>
--%>
<div class="row">
    <%--
    <div class="span1">
        <div class="tabs-left">
            <ul class="nav nav-tabs">
            <li <%=(bodyPart.isEmpty()) ? "class=\"active\"" : ""%>>
                <a href="?type=<%=type%>&bp=">All Parts</a>
            </li>
            <%
                for (Exercise.BodyPart b : Exercise.BodyPart.values()) {
            %>
            <li <%=(bodyPart.equals(b.name())) ? "class=\"active\"" : ""%>>
                <a href="?type=<%=type%>&bp=<%=b.name()%>"><%=b.getValue()%></a>
            </li>
            <%
                }
            %>
        </ul>
        </div>
    </div>
    --%>
    <div class="span9" style="padding: 10px;">
        <ul class="thumbnails">
            <%
                for (MyInfo info : infos) {
                    String url = "/images/photo_150_100.gif";
                    if (info.getAttachment() != null) {
                        url = info.getAttachment().getImageUrl(Attachment.ImageSize.f150);
                    }
            %>
            <li class="span2">
          <div class="thumbnail">
              <img src="<%=url%>" width="150" alt=""  height="105">
            <div class="caption">
              <h5 style="height: 35px; overflow: hidden;"><a href="edit.jsp?id=<%=info.getId()%>"><%=info.getHeadline()%></a></h5>
            </div>
          </div>
        </li>
            <%
                }
            %>


      </ul>
    </div>
</div>
</body>
</html>