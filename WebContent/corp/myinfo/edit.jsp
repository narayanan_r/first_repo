<%@page import="com.oreilly.servlet.MultipartRequest"%>
<%@ page import="au.com.ourbodycorp.Config" %>
<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.MultiPartHelper" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%@ page import="au.com.ourbodycorp.model.managers.AttachmentManager" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="org.apache.commons.fileupload.FileItem" %>
<%@ page import="org.apache.commons.fileupload.servlet.ServletFileUpload" %>
<%@ page import="au.com.ourbodycorp.model.managers.CorporationManager" %>
<%@ page import="java.util.List" %>
<%@	page import="java.util.ArrayList"%>
<%@ page import="java.util.Date" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@ page import="java.sql.Connection" %>
<%

    CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");
	Corporation c = (Corporation) request.getAttribute("corp");
	boolean showLocation = false;
	String multitype[] = null;
	String multitypeval = null;
	long uId = 0;
	session.setAttribute("uId", 0);
    User u = cu.getUser();
    Corporation corp = cu.getCorporation();

    String titlePrefix = "Add";
    String old=null;


    String[][] breadcrumbs = {{"/corp/", corp.getName()}, {"/corp/myinfo/", "MyInfo Articles"}, {null, "Edit"}};

    LibraryManager lm = new LibraryManager();

    long id = 0;
    try {
        id = Long.parseLong(request.getParameter("id"));
    } catch (Exception e) {
        // is ok
    }

    String headline = Util.notNull(request.getParameter("headline"));
    String body = Util.notNull(request.getParameter("body"));
    String link = Util.notNull(request.getParameter("externalLink"));

    MultiPartHelper mp = null;
    if (ServletFileUpload.isMultipartContent(request)) {
        mp = new MultiPartHelper(request);
        id = Long.parseLong(mp.getParameter("id"));
		showLocation = Boolean.parseBoolean(mp.getParameter("showLocation"));
		if(showLocation == true){
			if(mp.getParameter("multi") != null){
			multitype = mp.getParameter("multi").split(",");
			}
			multitypeval = mp.getParameter("multitype");
			}
        headline = Util.notNull(mp.getParameter("headline"));
        body = Util.notNull(mp.getParameter("body"));
        link = Util.notNull(mp.getParameter("externalLink"));
    }

    MyInfo info = null;
    if (id > 0) {
    	/* try{
        	Util.allActivitiesLogs("My Articles", u.getId(), corp.getName(), "Edit Articles Info");
        }catch(Exception ee){
        	
        } */
        info = lm.getInfo(id);
        old="Headline:"+info.getHeadline()+";Body:"+info.getBody()+";link:"+info.getLink()+";";

        if (info == null) {
            response.sendError(404, "no such info item");
//             response.sendRedirect("/corp/myinfo/");
            return;
        }
        
    	titlePrefix = "Edit";
        if(titlePrefix == "Edit"){
        	Connection conn = Util.getConnection();
    		PreparedStatement pst;
            pst = conn.prepareStatement("select shareid from info where id = ?");
            pst.setLong(1, id);
            ResultSet rs = pst.executeQuery();
            if(rs.next()){
            	session.setAttribute("uId", rs.getLong("shareid"));
            	uId = Long.parseLong(session.getAttribute("uId").toString());
            }
            pst.close();
    		conn.close();
        	Long preId = id;
        	if(preId == id){
        	session.setAttribute("preId", preId);
        	session.setAttribute("prePhotoId", info.getPhotoId());
        	}
        }
    }/* else{
    	try{
        	Util.allActivitiesLogs("My Articles", u.getId(), corp.getName(), "Add Articles Info");
        }catch(Exception ee){
        	
        }
    } */
    if(titlePrefix == "Add"){
    		session.setAttribute("prePhotoId", null);
    }
    boolean isDisabled = (info != null && info.getCorpId() != corp.getId());

    AttachmentManager am = new AttachmentManager();
    Attachment a = null;


    String error = null;

    if (request.getMethod().equalsIgnoreCase("post") && !isDisabled) {
        if (mp.getParameter("submit").equals("Cancel")) {
            response.sendRedirect("/admin/myinfo/");
            return;
        }
        
        if (headline.trim().length() == 0) {
            error = "Please enter a headline";
        } 
        else if (body.trim().length() == 0) {
            error = "Please enter some body text";
        }
        else if(showLocation == true){
        	if(multitypeval == null){
				request.setAttribute("MutiError", "Please select a location");
				error = "Please select a location";
        	}
        }

        headline = Util.safeInput(headline, false);


        FileItem photo = mp.getFileItem("photo");
        if (photo != null && photo.getSize() > 0) {
            if (!photo.getName().toLowerCase().endsWith(".jpg")) {
                error = "Photos must be uploaded as .jpg file";
            }
        }

        if (error == null) {
        	if(multitype != null){
        		for(String companyid : multitype){
        			uId = Long.parseLong(session.getAttribute("uId").toString());
        			List<Corporation> Exerceid = null;
    			    if (u != null && headline != null) {
    			        CorporationManager cm = new CorporationManager();
    			        if(uId!=0)
    			        {
    			    		cm.chkForArticleNameUpdate(uId, headline);	
    			    	}
    			        Exerceid = cm.getExerciseidforArticles(Long.parseLong(companyid),headline,uId,id);
    			    }
    			    if(Long.parseLong(companyid) != c.getId()){
        				if(!Exerceid.isEmpty()){
        					for (Corporation corpuexid : Exerceid) {
        						info.setId(corpuexid.getId());
        					}
        				}else{
        					info = new MyInfo();
						}
        		    	if(session.getAttribute("prePhotoId") != null && Long.parseLong(session.getAttribute("prePhotoId").toString()) > 0){
        		        a = am.getAttachment(Long.parseLong(session.getAttribute("prePhotoId").toString()), false);
			        	info.setPhotoId(a.getId());
			        	info.setAttachment(a);
			        	if (photo != null && photo.getSize() > 0) {
			    	       	a.setUploaded(new Date());
			    	       	a.setVersion(a.getVersion() + 1);
			                a.setType(Attachment.AttachmentType.image);
			                a.setFilename(photo.getName());
			                a.setUpdated(new Date());
			                a.setMimeType(photo.getContentType());
			                a.setActive(true);
			                a.setSize(photo.getSize());
			                a.setData(photo.get());
			                am.saveAttachment(a);
			    	       	}
        		    	}else{
        		    		if(!photo.getName().equals("")){
        		    		a = new Attachment();
        	                a.setUploaded(new Date());
    			        	a.setVersion(a.getVersion() + 1);
    			            a.setType(Attachment.AttachmentType.image);
    			            a.setFilename(photo.getName());
    			            a.setUpdated(new Date());
    			            a.setMimeType(photo.getContentType());
    			            a.setActive(true);
    			            a.setSize(photo.getSize());
    			            a.setData(photo.get());
    			            am.saveAttachment(a);
        		    		info.setPhotoId(a.getId());
    			        	info.setAttachment(a);
    			        	session.setAttribute("prePhotoId",info.getPhotoId());
        		    		}
        		    	}
       	                info.setCorpId(Long.parseLong(companyid));
        	            info.setHeadline(headline);
        	            info.setBody(body);
        	            info.setLink(link);
        	            info.setSharelocation(showLocation);
        	            info.setShareid(uId);
        	            lm.save(info);
        	            Connection conn = Util.getConnection();
                    	if(uId == 0){
        	        		PreparedStatement pst;
        		            pst = conn.prepareStatement("select shareid from info where id = ?");
        		            pst.setLong(1, info.getId());
        		            ResultSet rs = pst.executeQuery();
        		            if(rs.next()){
        		            	session.setAttribute("uId", rs.getLong("shareid"));
        		            }
        		            pst.close();
                		}
                    	conn.close();
    			    }
        			}
        	}
        	if(titlePrefix != "Add"){
        		info.setId(Long.parseLong(session.getAttribute("preId").toString()));
        	}
            if (titlePrefix == "Add") {
            	uId = Long.parseLong(session.getAttribute("uId").toString());
                info = new MyInfo();
            }
        if(session.getAttribute("prePhotoId") != null && Long.parseLong(session.getAttribute("prePhotoId").toString()) > 0){
	        a = am.getAttachment(Long.parseLong(session.getAttribute("prePhotoId").toString()), false);
	       	info.setPhotoId(a.getId());
	       	info.setAttachment(a);
	       	if (photo != null && photo.getSize() > 0) {
	       	a.setUploaded(new Date());
	       	a.setVersion(a.getVersion() + 1);
            a.setType(Attachment.AttachmentType.image);
            a.setFilename(photo.getName());
            a.setUpdated(new Date());
            a.setMimeType(photo.getContentType());
            a.setActive(true);
            a.setSize(photo.getSize());
            a.setData(photo.get());
            am.saveAttachment(a);
	       	}
    	}else{
    	if (photo != null && photo.getSize() > 0) {
            if (info.getPhotoId() > 0) {
                a = am.getAttachment(info.getPhotoId(), false);
            } else {
                a = new Attachment();
                a.setUploaded(new Date());
            }
            a.setVersion(a.getVersion() + 1);
            a.setType(Attachment.AttachmentType.image);
            a.setFilename(photo.getName());
            a.setUpdated(new Date());
            a.setMimeType(photo.getContentType());
            a.setActive(true);
            a.setSize(photo.getSize());
            a.setData(photo.get());
            am.saveAttachment(a);
            info.setPhotoId(a.getId());
            info.setAttachment(a);
        }
    	}
        	info.setCorpId(corp.getId());
            info.setHeadline(headline);
            info.setBody(body);
            info.setLink(link);
            info.setSharelocation(showLocation);
            info.setShareid(uId);
            lm.save(info);
            if (titlePrefix == "Add")
            {
            	String type="My Articles";
                String activity_page="corp/myinfo/edit.jsp";
            	String corp_id=String.valueOf(c.getId());
            	String corp_name=c.getName();
            	String corp_loc=c.getSuburb();
            	String user_id=String.valueOf(u.getId());
            	String user_name=u.getFullName();
            	String invitor_id=String.valueOf(info.getId());
            	String invitor_name="---";
            	String flag="add";
                String from="---";
            	String to="---";
            	String desc="Add Articles Info By User";
            	 try{
                 	//Util.allActivitiesLogs("My Articles", u.getId(), corp.getName(), "Add Articles Info");
                		Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
                	}catch(Exception ee){
                		
                	}
            }
            else
            {
            	String type="My Articles";
                String activity_page="corp/myinfo/edit.jsp";
            	String corp_id=String.valueOf(c.getId());
            	String corp_name=c.getName();
            	String corp_loc=c.getSuburb();
            	String user_id=String.valueOf(u.getId());
            	String user_name=u.getFullName();
            	String invitor_id=String.valueOf(info.getId());
            	String invitor_name="---";
            	String flag="update";
            	String to="Headline:"+headline+";Body:"+body+";link:"+link+";";
            	String desc="Edit Articles Info By User";
            	 try{
                 	//Util.allActivitiesLogs("My Articles", u.getId(), corp.getName(), "Edit Articles Info");
                		Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,old,to,desc);
                		response.sendRedirect("/corp/myinfo/");
                        return;
                	}catch(Exception ee){
                		
                	}
            	
            }
            
            
            
            if (mp.getParameter("submit").contains("Close")) {
                response.sendRedirect("/corp/myinfo/");
                return;
            }
            id = info.getId();
        }

    } else {
        if (info != null) {
            headline = info.getHeadline();
            body = info.getBody();
            link = info.getLink();
            showLocation = info.isSharelocation();
        }
    }

    if (info != null) {
      if (info.getPhotoId() > 0) {
                a = info.getAttachment();
            }
        }

    if (isDisabled)
        error = "This is a library article and can not be edited";
%>
<html>
<head>
    <title><%=titlePrefix%> Info Article</title>
<script type="text/javascript">
$( document ).ready(function showorhide(){
	var checked = $("input[name='showLocation']").attr("checked") ? "True" : "False";
	var foo = []; 
	$('#multitypedub :selected').each(function(i, selected){ 
	  foo[i] = $(selected).text(); 
	});
	$.each(foo, function( index, value ) {
	var row = $('#multitype');
	row.find("option[value="+value+']').attr('selected','selected');
		});
	var foo3 = []; 
	$('#multitype :selected').each(function(i, selected){ 
	  foo3[i] = $(selected).val(); 
	});
	$("#multi").val(foo3);
	if(checked == "True"){
	$("#multitype").show();
	if($("#multitype").val() != null){
	$("#errorhideorshow").hide();
	}else if($("#body").val() != ''){
		$("#errorhideorshow").show();
	}else if($("#body").val() == ''){
		$("#errorhideorshow").hide();
	}
	}else{
	$("#multitype").hide();
	$("#errorhideorshow").hide();
	}
	
});
function showorhide(){
	var foo2 = []; 
	$('#multitypedub :selected').each(function(i, selected){ 
	  foo2[i] = $(selected).text(); 
	});
	$.each(foo2, function( index, value ) {
	var row = $('#multitype');
	row.find("option[value="+value+']').attr('selected','selected');
		});
	var foo = []; 
	$('#multitype :selected').each(function(i, selected){ 
	  foo[i] = $(selected).val(); 
	});
	$("#multi").val(foo);
	var checked = $("input[name='showLocation']").attr("checked") ? "True" : "False";
	if(checked == "True"){
	$("#multitype").show();
	}else{
	$("#multitype").hide();
	$("#errorhideorshow").hide();
	}
}

function showorhidetabs() {
	var foo2 = []; 
	$('#multitypedub :selected').each(function(i, selected){ 
	  foo2[i] = $(selected).text(); 
	});
	$.each(foo2, function( index, value ) {
	var row = $('#multitype');
	row.find("option[value="+value+']').attr('selected','selected');
		});
	var foo = []; 
	$('#multitype :selected').each(function(i, selected){ 
	  foo[i] = $(selected).val(); 
	});
	$("#multi").val(foo);
	
}
function isCheckEmpty() {
	var headline = $("#headline").val();
	var body = $("#body").val();
	var photo = $("#photo").val();
	var phototype = $("#photo").val().split('.').pop();
	if(headline == ""){
		$("#showError").show();
		$("#errorValue").show();
		$("#errorValue1").hide();
		$("#errorValue2").hide();
		return false;
	}else if(body == ""){
		$("#errorValue").hide();
		$("#showError").show();
		$("#errorValue1").show();
		$("#errorValue2").hide();
		return false;
	}else if(photo != "" && phototype != "jpg"){
		$("#errorValue").hide();
		$("#errorValue1").hide();
		$("#showError").show();
		$("#errorValue2").show();
		return false;
	}else{
		$("#errorValue").hide();
		$("#errorValue1").hide();
		$("#showError").hide();
		$("#errorValue2").hide();
		return true;
	} 
}
function goBack() {
	window.location="/corp/myinfo/";
}
</script>
</head>
<body>
<div class="page-header">
    <h1>My Info <small><%=titlePrefix%> Article</small></h1>
</div>
<%@include file="/WEB-INF/includes/breadcrumbs.jsp"%>
<%@include file="/WEB-INF/includes/formErrorSimple.jsp"%>
<div class="row" id="showError" style="display: none;">
<div class="span4">
<div class="alert alert-error">
<p id="errorValue">Please enter a headline</p>
<p id="errorValue1">Please enter a body</p>
<p id="errorValue2">Only .jpg files allowed</p>
</div>
</div>
</div>
<div class="row">
    <div class="span9">
        <form action="edit.jsp" method="post" class="form-horizontal" enctype="multipart/form-data" onsubmit="return isCheckEmpty();">
            <input type="hidden" name="id" value="<%=id%>">
            <input type="hidden" id="multi" name="multi">
            <%=HTML.textInput("headline", "Headline", "50", "100", headline)%>
            <%=HTML.textArea("body", "Body", body, "input-xxlarge", 10)%>
            <%=HTML.textInput("externalLink", "External link", "50", "100", link, "Optional link.", null)%>
            <%
                if (!isDisabled) {
            %>
            <%=HTML.fileInput("photo", ((a != null) ? "Update Photo" : "Add Photo"), null)%>
            <%
                }
            %>
             <%
                if (a != null) {
            %>
            <div class="control-group">
            <label class="control-label" for="textarea">&nbsp;</label>
            <div class="controls">
              <img width="200" height="150" src="<%=a.getImageUrl(Attachment.ImageSize.f320)%>">
            </div>
          </div>
            <%
                }
            %>
            <% if (id != 0) {%>
            <div class="control-group" style="margin-top: 15px;">
                <label for="Delete" class="control-label">&nbsp;</label>
                <div class="controls">

                <%
                if (!isDisabled) {
            %>
            	<% if (Config.getString("https").equals("true")){ %>
            		<a href="https://<%=corp.getSlug()%>.<%=Config.getString("baseDomain")%>/info.jsp?id=<%=id%>" class="btn fancybox">Preview</a>
                <%}else{%>
                	<a href="http://<%=corp.getSlug()%>.<%=Config.getString("baseDomain")%>/info.jsp?id=<%=id%>" class="btn fancybox">Preview</a>
                <% }%>
                
                <a href="inline/delete.jsp?id=<%=id%>&name=<%=headline%>" class="btn btn-danger fancybox">Delete Article</a>
                    <%
                        }
                    %>

            </div>
            </div>
                <% } %>
            <%CorporationManager cm = new CorporationManager();
            if(cm.getCorporations(u.getId()).size() > 1){
            %>
            <div class="control-group">
            <label class="control-label" for="optionsCheckbox">Share across locations</label>
            <div class="controls">
              <label class="checkbox">
                <input type="checkbox" onclick="showorhide();" id="optionsCheckbox" name="showLocation" value="true"<%=(showLocation) ? " checked": ""%>>
                </label>
                <select id="multitype" name="multitype" onclick="showorhidetabs();" onchange="showorhidetabs();" multiple style="display: none;margin: -5px 0 0 19px;">
             <% List<Corporation> corps = null;
			    if (u != null) {
// 			        CorporationManager cm = new CorporationManager();
			        corps = cm.getCorporations(u.getId());
			    }%>
            <%
            if (corps != null && corps.size() > 0) {
                    for (Corporation corpu : corps) {
                        if (c != null && c.equals(corpu)) {
                            continue;
                        }
                  %>
                   <option value="<%=corpu.getId()%>"><%=corpu.getName()%></option>
                  <%
                    }
                %>

                  <%
                      }%>
              </select><div class="alert alert-error" id="errorhideorshow" style="width: 250px; margin: -60px 0px 15px 250px; "><p><% if(request.getAttribute("MutiError") != null){%><%= request.getAttribute("MutiError") %><%}%></p></div>
              
              <select id="multitypename" name="multitypename" multiple style="display: none; margin: -5px 0 0 19px;">
             <% 
             	List<Corporation> corpsid = null;
			    if (u != null) {
// 			        CorporationManager cm = new CorporationManager();
			        if(!headline.equals("")){
			        corpsid = lm.getCorpidforArticles(headline,corp.getCompanyId(),uId,id);
			        }
			    }%>
            <% 
            if (corpsid != null && corpsid.size() > 0) {
                   if (corpsid != null && corpsid.size() > 0) {
                          for (Corporation corpu1 : corpsid) {
                  %>
                  <option value="<%=corpu1.getId()%>" <%=(corpu1.getId() == corpu1.getId()) ? "selected" : ""%>><%=corpu1.getName()%>
                		  <% 
                  %></option>
                  <%
                    }
                %>

                  <%
                   }
            }%>
              </select>
            </div>
          </div>
          <%} %>
          <select id="multitypedub" name="multitypedub" multiple style="display: none; margin: -5px 0 0 19px;">
             <% 
             	List<Corporation> corpsid = null;
            	List<Long> Eidlist = new ArrayList<Long>();
			    if (u != null) {
// 			        CorporationManager cm = new CorporationManager();
			        if(!headline.equals("")){
			        corpsid = lm.getCorpidforArticles(headline,corp.getCompanyId(),uId,id);
			        }
			    }%>
            <% 
            if (corpsid != null && corpsid.size() > 0) {
                   if (corpsid != null && corpsid.size() > 0) {
                          for (Corporation corpu1 : corpsid) {
                  %>
                  <option value="<%=corpu1.getId()%>" <%=(corpu1.getCompanyId() == corpu1.getCompanyId()) ? "selected" : ""%>><%=corpu1.getCompanyId()%>
                		  <% 
                		  Eidlist.add(corpu1.getId());
                  %></option>
                  <%
                    }
                %>

                  <%
                   }
                   session.setAttribute("Eidlist", Eidlist);
            }%>
              </select>
            <%
                if (!isDisabled) {
            %>
            <div class="form-actions">
				<button class="btn btn-primary" value="Save" type="submit" name="submit" >Save</button>
				<button class="btn" value="Cancel" type="button" name="submit" onclick="goBack();">Cancel</button>
			</div>
            <%
                }
            %>

        </form>
    </div>
</div>
</body>
</html>