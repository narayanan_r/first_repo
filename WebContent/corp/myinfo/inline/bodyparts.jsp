<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.Exercise" %>
<%@ page import="au.com.ourbodycorp.model.MyInfo" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.PreparedStatement" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    long id = Long.parseLong(request.getParameter("id"));
    LibraryManager lm = new LibraryManager();
    MyInfo e = lm.getInfo(id);
    String error = null;
    if (request.getMethod().equalsIgnoreCase("post")) {
        if (request.getParameter("submit").equals("Cancel")) {
            response.sendRedirect("/close.jsp");
            return;
        }

        if (request.getParameterValues("bp") == null || request.getParameterValues("type") == null) {
            error = "select at least 1 body port and 1 type";
        } else {

            Connection conn = Util.getConnection();
            try {
                conn.setAutoCommit(false);
                PreparedStatement pst = conn.prepareStatement("delete from info_bodyparts where info = ?");
                pst.setLong(1, e.getId());
                pst.executeUpdate();
                pst.close();

                pst = conn.prepareStatement("delete from info_exercise_types where info = ?");
                pst.setLong(1, e.getId());
                pst.executeUpdate();
                pst.close();

                pst = conn.prepareStatement("insert into info_bodyparts (info, bodypart) values (?,?)");
                for (String bp : request.getParameterValues("bp")) {
                    try {
                        Exercise.BodyPart b = Exercise.BodyPart.valueOf(bp);
                    } catch (IllegalArgumentException ex) {
                        continue;
                    }
                    pst.setLong(1, e.getId());
                    pst.setString(2, bp);
                    pst.execute();
                }
                pst.close();

                pst = conn.prepareStatement("insert into info_exercise_types (info, type) values (?,?)");
                for (String type : request.getParameterValues("type")) {
                    try {
                        Exercise.Type t = Exercise.Type.valueOf(type);
                    } catch (IllegalArgumentException ex) {
                        continue;
                    }
                    pst.setLong(1, e.getId());
                    pst.setString(2, type);
                    pst.execute();
                }
                pst.close();

                conn.commit();

                response.sendRedirect("/close.jsp?reload=true");
            } finally {
                conn.setAutoCommit(true);
                conn.close();
            }
        }
    }

%>
<html>
<head><title>Select body parts and exercise types</title></head>
<body>
<div class="page-header"><h1>Body parts and exercise types</h1></div>
<%@include file="/WEB-INF/includes/formErrorSimple.jsp"%>
<div class="row">
    <form action="bodyparts.jsp" method="post">

    <input type="hidden" name="id" value="<%=id%>">

        <div class="span3">
            <h2>Body Parts</h2>
            <%
                for (Exercise.BodyPart bp : Exercise.BodyPart.values()) {
            %>
                <label class="checkbox">
                <input type="checkbox" name="bp" value="<%=bp.name()%>" <%=e.getBodyParts().contains(bp) ? "checked" : ""%>> <%=bp.getValue()%><br>
                </label>
            <%
                }
            %>
        </div>
        <div class="span3">
            <h2>Exercise Types</h2>
            <%
                for (Exercise.Type type : Exercise.Type.values()) {
            %>
                <label class="checkbox">
                <input type="checkbox" name="type" value="<%=type.name()%>" <%=e.getTypes().contains(type) ? "checked" : ""%>> <%=type.getValue()%><br>
                </label>
            <%
                }
            %>
        </div>
</div>
    <div class="row">
        <div class="span6">
            <%=HTML.saveCancel()%>
        </div>
           </div>
</form>
</body>
</html>