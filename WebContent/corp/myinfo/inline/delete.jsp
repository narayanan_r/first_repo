<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.MyInfo" %>
<%@ page import="au.com.ourbodycorp.model.managers.AttachmentManager" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="java.net.URLEncoder" %>
<%@ page import="au.com.ourbodycorp.model.managers.CorporationManager" %>
<%@ page import="au.com.ourbodycorp.model.CorporationUser" %>
<%@ page import="au.com.ourbodycorp.model.Corporation" %>
<%@ page import="au.com.ourbodycorp.model.User" %>
<%@	page import="java.util.ArrayList"%>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    long id = Long.parseLong(request.getParameter("id"));
	CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");
	Corporation corp = cu.getCorporation();
    boolean confirm = (request.getParameter("confirm") != null && Boolean.parseBoolean(request.getParameter("confirm")));
    String submit = Util.notNull(request.getParameter("submit"));
    LibraryManager lm = new LibraryManager();
    User u = cu.getUser();
  /*   try{
    	Util.allActivitiesLogs("My Articles", u.getId(), corp.getName(), "Delete Articles Info");
    }catch(Exception ee){
    	
    } */
    List<Long> Eideditlist;
    String name = request.getParameter("name");
    String headline = "";
    if(name != null){
    	session.setAttribute("name", name);
    	headline = session.getAttribute("name").toString();
    }
    MyInfo e = lm.getInfo(id);

    if (e == null) {
        response.sendError(404);
        return;
    }

    if (submit.equalsIgnoreCase("cancel")) {
        response.sendRedirect(corp.getHost()+"/close.jsp");
        return;
    } else if (confirm) {
    	String type="My Articles";
	    String activity_page="corp/myinfo/inline/delete.jsp";
		String corp_id=String.valueOf(corp.getId());
		String corp_name=corp.getName();
		String corp_loc=corp.getSuburb();
		String user_id=String.valueOf(u.getId());
		String user_name=u.getFullName();
		String invitor_id=String.valueOf(e.getId());
		String invitor_name="---";
		String flag="delete";
		String from="---";
	    String to="---";
	    try{
	    	//Util.allActivitiesLogs("My Articles", u.getId(), corp.getName(), "Delete Articles Info");
        	Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,"Delete Articles  Info By User");
        }catch(Exception ee){
        	
        }
    	List<Corporation> corpsid = null;
    	List<Long> Eidlist = new ArrayList<Long>();
        if (u != null) {
            CorporationManager cm = new CorporationManager();
            if(!headline.equals("")){
            corpsid = lm.getCorpidforArticles(headline,corp.getCompanyId(),e.getShareid(),id);
            }
        }
    	
    	if (corpsid != null && corpsid.size() > 0) {
    	       if (corpsid != null && corpsid.size() > 0) {
    	              for (Corporation corpu1 : corpsid) {
    	    		  Eidlist.add(corpu1.getId());
    	        }
    	       }
    	       session.setAttribute("Eidlist", Eidlist);
    	}
        Eideditlist = (ArrayList<Long>)session.getAttribute("Eidlist");
    	if(Eideditlist != null){
    	for(Long Eid : Eideditlist){
        lm.deleteInfo(Eid);
    	}
    	}
        if (e.getAttachment() != null) {
            AttachmentManager am = new AttachmentManager();
            am.delete(e.getAttachment().getId());
        }
        response.sendRedirect(corp.getHost()+"/close.jsp?redir=" + URLEncoder.encode("/corp/myinfo/", "UTF-8"));
        return;
    }

%>
<html>
<head><title>Delete Article</title></head>
<body>
<div class="page-header">
    <h1>Delete Article <small><%=Util.textAreaValue(e.getHeadline())%></small></h1>
</div>
<div class="row">
    <div class="span6">
        <p>Are you sure you want to delete this article? It will be removed from any program it is used in.</p>
        <form action="delete.jsp" method="POST">
            <input type="hidden" name="id" value="<%=id%>">
            <input type="hidden" name="confirm" value="true">
            <%=HTML.confirmCancel()%>
        </form>
    </div>
</div>
</body>
</html>