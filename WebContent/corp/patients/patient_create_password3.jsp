<%@ page import="au.com.ourbodycorp.model.managers.PatientManager" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    PatientManager um = new PatientManager();
    um.logout(session, response);
%>
<html>
  <head><title>Create Password</title></head>
  <body>
  <div class="page-header">
    <h1>Your password has been created</h1>
</div>
<div class="row">
    <div class="span8">
        You can now login by your (iOS and Android) Fiizio App using your new password.
    </div>
    <div class="span8">
        &nbsp;
        <p>Fiizio App is available for iOS and Android:</p>
         <a href="#">
            <img src="/images/app_store.png" alt="Available on the App Store" width="150">
        </a>
        <a href="#" style="margin-left: 10px;">
            <img src="/images/play_store.png" alt="Available on the Google Play Store" width="150">
        </a>
    </div>
</div>
  </body>
</html>