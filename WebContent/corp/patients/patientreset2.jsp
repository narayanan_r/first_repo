<%@ page import="au.com.ourbodycorp.model.managers.PatientManager" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    PatientManager pm = new PatientManager();
%>
<html>
  <head><title>Password Reset</title></head>
  <body>
  <div class="page-header">
    <h1>Your password has been changed</h1>
</div>
<div class="row">
    <div class="span8">
        <a>Now,you can download your program through the received mail using the new password.</a>
    </div>
    <div class="span8">
        &nbsp;
    </div>
</div>
  </body>
</html>