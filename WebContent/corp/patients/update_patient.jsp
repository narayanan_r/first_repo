<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="au.com.ourbodycorp.model.managers.CorporationManager" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.util.*" %>
<%@ page import="java.util.Date" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
	long id = Long.parseLong(Util.notNull(request.getParameter("id"), "0"));
         Connection conn = Util.getConnection();
		PreparedStatement pst;
        pst = conn.prepareStatement("update patient set new_patient=null where id = ?");
        pst.setLong(1, id);
        pst.executeUpdate();
        pst.close();
		conn.close(); 

%>
