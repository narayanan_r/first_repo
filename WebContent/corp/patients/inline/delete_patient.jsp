<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.Corporation" %>
<%@ page import="au.com.ourbodycorp.model.CorporationUser" %>
<%@ page import="au.com.ourbodycorp.model.Patient" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="au.com.ourbodycorp.Config" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%

    CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");

    Corporation corp = cu.getCorporation();
    User u = cu.getUser();
    long id = Long.parseLong(Util.notNull(request.getParameter("id"), "0"));

   /*  try{
    	Util.allActivitiesLogs("Patients", u.getId(), corp.getName(), "Delete Patient");
    }catch(Exception e){
    	
    } */

    LibraryManager lm = new LibraryManager();
    Patient p;
    if (id != 0) {
        p = lm.getPatient(id);
        if (p == null || p.getCorpId() != corp.getId()) {
            response.sendError(404, "Patient not found");
            return;
        }
    } else {
        response.sendError(400, "No program ID provided");
        return;
    }

    if (request.getMethod().equalsIgnoreCase("post")) {
        if (request.getParameter("submit").startsWith("Cancel")) {
        	/* if(Config.getString("https").equals("true")){
        		response.sendRedirect(Config.getString("url.secure")+"/close.jsp?reload=true");
        	}else{
        		response.sendRedirect(Config.getString("url")+"/close.jsp?reload=true");
        	} */
        	response.sendRedirect(corp.getHost() + "/close.jsp?reload=true");
        } else {
        	String type="Patients";
    	    String activity_page="corp/patients/inline/delete_patient.jsp";
    		String corp_id=String.valueOf(corp.getId());
    		String corp_name=corp.getName();
    		String corp_loc=corp.getSuburb();
    		String user_id=String.valueOf(u.getId());
    		String user_name=u.getFullName();
    		String invitor_id=String.valueOf(p.getId());
    		String invitor_name=p.getFullName();
    		String flag="new";
    		String from="Delete Patient By User";
    	    String to="FirstName:"+p.getFirstName()+";LastName:"+p.getLastName()+";Email:"+p.getEmail()+";Phone:"+p.getPhone()+";Reference:"+p.getReference();
    	    try{
    	    	//Util.allActivitiesLogs("Patients", u.getId(), corp.getName(), "Delete Patient");
            	Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,"Delete Patient "+invitor_name+" By User");
            }catch(Exception ee){
            	
            }
            lm.deletePatient(p.getId());
            /* if(Config.getString("https").equals("true")){
        		response.sendRedirect(Config.getString("url.secure")+"/close.jsp?redir=/corp/patients/");
        	}else{
        		response.sendRedirect(Config.getString("url")+"/close.jsp?redir=/corp/patients/");
        	} */
            response.sendRedirect(corp.getHost() + "/close.jsp?redir=/corp/patients/");
        }
        return;
    }

 %>

<html>
<head><title>Delete Program</title></head>
<body>
<div class="page-header">
    <h1>Delete Patient</h1>
</div>
<div class="row">
    <div class="span6">
        <p>
            Delete patient <b><%=p.getFullName()%></b>.
        </p>
        <!-- <p>
            <b>This action can not be undone.</b>
        </p> -->
        <p>
            Are you sure?
        </p>
        <form action="delete_patient.jsp" method="post">
            <input type="hidden" name="id" value="<%=p.getId()%>">
            <%=HTML.confirmCancel()%>
        </form>
    </div>
</div>
</body>
</html>