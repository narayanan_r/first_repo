<%@page import="org.apache.el.lang.ELSupport"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="java.util.List"%>
<%@page import="au.com.ourbodycorp.model.managers.SubscriptionManager"%>
<%@ page import="au.com.ourbodycorp.HTML"%>
<%@ page import="au.com.ourbodycorp.Util"%>
<%@ page import="au.com.ourbodycorp.model.*"%>
<%@ page import="au.com.ourbodycorp.model.managers.CorporationManager"%>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager"%>
<%@ page import="java.util.Calendar"%>
<%@ page import="java.util.Date"%>
<%@ page import="java.util.HashMap"%>
<%@ page import="java.io.BufferedReader"%>
<%@ page import="java.io.IOException"%>
<%@ page import="java.io.InputStreamReader"%>
<%@ page import="java.net.URL"%>
<%@ page import="java.net.URLConnection"%>
<%@ page import="org.json.JSONObject"%>
<%@ page import="sun.net.www.protocol.http.HttpURLConnection"%>
<%@ page import="java.net.URLEncoder"%>
<%@ page import="java.io.OutputStreamWriter"%>
<%@ page import="au.com.ourbodycorp.MailMessage"%>
<%@ page import="java.util.Properties"%>
<%@ page import="au.com.ourbodycorp.Config"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.UUID"%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%
	CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");

    if (cu == null) {
        response.sendRedirect(Util.loginUrl(Constants.LoginReason.login, request));
        return;
    }
    User u = cu.getUser();
    Corporation corp = cu.getCorporation();
    
    LibraryManager lm = new LibraryManager();
    SubscriptionManager sm=new SubscriptionManager();
    Subscription s=null;
    long id = Long.parseLong(Util.notNull(request.getParameter("id"), "0"));
    String title = "Add";
    String old=null;
    Patient p = null;
    long paymentSubId=0;
    Calendar cal = Calendar.getInstance();
	Date transactionDate =null;
	Date expiredDate=null;
    PaymentSubscription paymentSubscription = null;
    boolean tooMany = false;

    if (id > 0) {
    	/* try{
        	Util.allActivitiesLogs("Patients", u.getId(), corp.getName(), "Edit Patient");
        }catch(Exception e){
        	
        } */
        title = "Edit";
        p = lm.getPatient(id);
        s=sm.getSubscriptions(p.getSubscriptionId());
    	old="FirstName:"+p.getFirstName()+";LastName:"+p.getLastName()+";Phone:"+p.getPhone()+";Reference:"+p.getReference()+";SubscriptionName:"+s.getSubscription()+";";

        if (p == null || p.getCorpId() != corp.getId()) {
            response.sendError(404, "Patient not found or not part of this practice");
            return;
        }
        
    } 
    else {
    	/* try{
        	Util.allActivitiesLogs("Patients", u.getId(), corp.getName(), "Add Patient");
        }catch(Exception e){
        	
        } */
        int max = corp.getMaxPatients();
        int createdThisMonth = lm.countOfPatientsCreatedThisMonth(corp.getId());
        tooMany = (max > 0 && max <= createdThisMonth);
    }

    String fname = Util.notNull(request.getParameter("fname"));
    String lname = Util.notNull(request.getParameter("lname"));
    String email = Util.notNull(request.getParameter("email"));
    String phone = Util.notNull(request.getParameter("phone"));
    String ref = Util.notNull(request.getParameter("ref"));
    String subId = Util.notNull(request.getParameter("subscription"));
    String subDate=Util.notNull(request.getParameter("subscriptionDate"));
	email=email.toLowerCase();	
    HashMap<String, String> errorFields = new HashMap<String, String>();

    if (request.getMethod().equalsIgnoreCase("post")) {
    	if (fname.trim().length() == 0) {
            errorFields.put("fname", "please enter a name");
        }

        if (lname.trim().length() == 0) {
            errorFields.put("lname", "please enter a name");
        }
        if (email.trim().length() == 0) {
            errorFields.put("email", "Please enter a email address");
        }
        if(email.trim().length() > 0){
	        if (!email.matches("[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}")) {
	            errorFields.put("email", "Please enter a valid email address");
	        }
        }
        s=sm.getSubscriptions(Long.parseLong(subId));
    	if(errorFields.size() == 0 && p != null){
        	
      		   Connection conn = Util.getConnection();
      		   PreparedStatement pst = null;
      		 PreparedStatement pst1 = null;
      		   try {
      			 if(Long.parseLong(subId)==p.getSubscriptionId())
              	{      				
       				pst = conn.prepareStatement("update patient set fname=?, lname=?, phone=?, reference=? where id=?");
       				pst.setLong(5,p.getId());
                      
              	}else{     
              		
       	            pst = conn.prepareStatement("update patient set fname=?, lname=?, phone=?, reference=?,subscription_id=?,subscriptiondate=? where id=?");
       	          	pst.setLong(5, Long.parseLong(subId));
           			pst.setTimestamp(6,new Timestamp(new Date().getTime()));
           			pst.setLong(7,p.getId());
              	}	
              
		      			 	pst.setString(1, fname);
		    	            pst.setString(2, lname);
		  	            	pst.setString(3, phone);
		  	            	pst.setString(4, ref);
    	            pst.execute();
    	            System.out.println("p.getId()"+p.getId());
    	            PreparedStatement pst2 = conn.prepareStatement("select *  from payment_subscription  where user_id= ? order by transaction_date desc limit 1");
    	            pst2.setLong(1, p.getId());
    	            ResultSet rs = pst2.executeQuery();
    	            if (rs.next()) {
    	         	   paymentSubId=rs.getLong("id");
    	            }
    	            transactionDate= new Date();
    	         if(subId.equals("1")){
       	        	 expiredDate = cal.getTime();    
       	         }else if(subId.equals("2")){
               		 	cal.add(Calendar.DATE, +3);
        	            expiredDate = cal.getTime();    
               	 }else if(subId.equals("3")){
               		    cal.add(Calendar.MONTH, +1);
        	            expiredDate = cal.getTime();      	            
               	 }else if(subId.equals("4")){
               		    cal.add(Calendar.MONTH, +3);
        	            expiredDate = cal.getTime();      	            
               	 }else if(subId.equals("5")){
               		    cal.add(Calendar.MONTH, +6);
        	            expiredDate = cal.getTime();      	            
               	 }
    	             
    	            System.out.println("paymentSubId"+paymentSubId);
      			     pst1 = conn.prepareStatement("update payment_subscription set sub_id=? , transaction_date=? ,expired=? where id=?");
      			 	 pst1.setString(1, subId);
      			 	 pst1.setTimestamp(2, new Timestamp(transactionDate.getTime()));
      			 	if(expiredDate != null){
      			 	 	pst1.setTimestamp(3, new Timestamp(expiredDate.getTime()));
      			 	 }else{
      			 		pst1.setTimestamp(3,null);
      			 	 }
	      			 pst1.setLong(4, paymentSubId);
      				 
      				 pst1.execute();
      	        }
      	        finally {
      	            if (conn != null) {
      	                conn.close();
      	            }
      	        }
        	String type="Patients";
            String activity_page="corp/patients/edit.jsp";
        	String corp_id=String.valueOf(corp.getId());
        	String corp_name=corp.getName();
        	String corp_loc=corp.getSuburb();
        	String user_id=String.valueOf(u.getId());
        	String user_name=u.getFullName();
        	String invitor_id=String.valueOf(p.getId());
        	String invitor_name=fname+" "+lname;
        	String flag="update";
        	String to="FirstName:"+fname+";LastName:"+lname+";Phone:"+phone+";Reference:"+ref+";SubscriptionName:"+s.getSubscription()+";";
        	String desc="Edit Patient "+invitor_name+" By User";
        	 try{
             	//Util.allActivitiesLogs("Patients", u.getId(), corp.getName(), "Edit Patient");
         		Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,old,to,desc);
         		response.sendRedirect("/corp/patients/detail.jsp?id="+p.getId());
                return;
         	}catch(Exception ee){
         		
         	}
        }
        boolean checkTrial = false;
        
        if(title == "Add" || !p.getEmail().equals(email)){
	        Connection con = Util.getConnection();
	        PreparedStatement pst = con.prepareStatement("select id from patient where email = ? ");
	        pst.setString(1, email);
	        ResultSet rs = pst.executeQuery();
	        if(rs.next()){
	        	errorFields.put("email", "This email address already exist");
	        }
	        pst.close();
	        con.close();
        }
        
        if (errorFields.size() == 0) {
            if (p == null) {
                p = new Patient();
                p.setCreated(new Date());
                p.setCreator(u.getId());
                p.setCorpId(corp.getId());
                checkTrial = true;
            }
            p.setFirstName(fname);
            p.setLastName(lname);
            p.setEmail(email);
            p.setReference(ref);
            p.setPhone(phone);
            if(StringUtils.isNotBlank(subId)){
            	 p.setSubscriptionId(Long.parseLong(subId));
            	 if(Long.parseLong(subId)==p.getSubscriptionId() && subDate !="")
            	 {
            		 p.setSubscriptionDate(Timestamp.valueOf(subDate));
            	 }
            }
            p.setCheck_patient_exist("Checked");           
			
            if (paymentSubscription == null) {
            	paymentSubscription = new PaymentSubscription();
            	paymentSubscription.setTotalAmount(0);
            	if(subId.equals("0")){
            		paymentSubscription.setSubscriptionId("au.com.fiizioapp.100");
            	}
            	if(subId.equals("1")){
            		paymentSubscription.setSubscriptionId("au.com.fiizioapp.1000");
            	}
            	if(subId.equals("2")){
            		paymentSubscription.setSubscriptionId("au.com.fiizioapp.0");
            	}
            	if(subId.equals("3")){
            		paymentSubscription.setSubscriptionId("au.com.fiizioapp.1");
            	}
            	if(subId.equals("4")){
            		paymentSubscription.setSubscriptionId("au.com.fiizioapp.2");
            	}
            	if(subId.equals("5")){
            		paymentSubscription.setSubscriptionId("au.com.fiizioapp.3");
            	}
            	paymentSubscription.setTransactionId(null);
            	paymentSubscription.setTransStatus("Success");
            	paymentSubscription.setTransactionDate(new Date());
 				paymentSubscription.setSubId(subId);
				}
            
            try{
  	    		    String corporation=null;
	   	    		Connection con1 = Util.getConnection();
		    	    	lm.save(p);
		    	    	lm.savePaymentSubscription(paymentSubscription,email);
 	    		    	String type="Patients";
 	    		        String activity_page="corp/patients/edit.jsp";
 	    		    	String corp_id=String.valueOf(corp.getId());
 	    		    	String corp_name=corp.getName();
 	    		    	String corp_loc=corp.getSuburb();
 	    		    	String user_id=String.valueOf(u.getId());
 	    		    	String user_name=u.getFullName();
 	    		    	String invitor_id=String.valueOf(p.getId());
 	    		    	String invitor_name=p.getFullName();
 	    		    	String flag="new";
 	    		    	String from="Create New Patinet By User";
 	    		    	String to="FirstName:"+fname+";LastName:"+lname+";Email:"+email+";Phone:"+phone+";Reference:"+ref+";SubscriptionName:"+s.getSubscription()+";";
 	    		    	String desc="Add Patient "+invitor_name+" By User";
 	    		    	 try{
 	    		        	//Util.allActivitiesLogs("Patients", u.getId(), corp.getName(), "Add Patient");
 	    		     		Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
 	    		     	}catch(Exception ee){
 	    		     		
 	    		     	}
  					String key = UUID.randomUUID().toString();
					PreparedStatement pst2 = con1.prepareStatement("update patient set password = ? where id = ?");
  	                	pst2.setString(1, key);
  	                	pst2.setLong(2, p.getId());
  	                	pst2.executeUpdate();
  	                	pst2.close();
  	    		    	Properties props = new Properties();
  	    	        	props.put("firstName", fname);
  	    	        	props.put("lastName", lname);
  	    	        	props.put("invitor", corp.getName());
  	    	        	props.put("email", email);
  	    	        String url = null;
  	    	        if(Config.getString("https").equals("true")){
  	    	        	url = Config.getString("url.secure");
  	    	        	}else{
  	    	        	url = Config.getString("url");
  	    	        	}
  	    	        props.put("url", url + "/corp/patients/patient_create_password.jsp?key=" + key);
  	    	        MailMessage msg = new MailMessage(props, "create_new_password.vm", email, "Welcome to Fiizio – Create new password",false);
  	    	        try {
  	    	            msg.send();
  	    	        	}
  	    	        catch (Exception e) {
  	    	        	}
  	    		    	
      	            	con1.close();
  	    	   			
    	    	   
    	    	   /*  } else{
    	    	   errorFields.put("email", "This email address is not active. Please check the spelling or enter an alternative address.");
    	       } */
//     	         System.out.println(urlString);
    	       }catch(IOException e)
    	      {
    	         e.printStackTrace();
    	      } 
            if (errorFields.size() == 0) {
            if (checkTrial) {
                if (corp.isTrial() && corp.getExpiry() == null) {
                    cal = Calendar.getInstance();
                    cal.add(Calendar.DATE, 10); // add 10 days  
                    corp.setExpiry(cal.getTime());
                    CorporationManager cm = new CorporationManager();
                    cm.save(corp, 0);
                }
            }

            response.sendRedirect("/corp/patients/detail.jsp?id="+p.getId());
            return;
            }
        }
    } else if (p != null) {
        fname = p.getFirstName();
        lname = p.getLastName();
        email = p.getEmail();
        ref = p.getReference();
        phone = p.getPhone();
        if(p.getSubscriptionId() >= 0){
        	subId=String.valueOf(p.getSubscriptionId());
        	if(p.getSubscriptionDate()!=null)
        	{
        	subDate=p.getSubscriptionDate().toString();
        	}
        }
    }

    String[][] breadcrumbs = {{"/corp/", corp.getName()}, {"/corp/patients/", "Patients"}, {null, title + " Patient"}};
%>
<html>
<head>
<title><%=title%> Patient</title>
<script type="text/javascript">
	function isAlphabetswithDot(txt, e) {
		keyEntry = (e.keyCode) ? e.keyCode : e.which;
		if (((keyEntry >= '65') && (keyEntry <= '90'))
				|| ((keyEntry >= '97') && (keyEntry <= '122'))|| ((keyEntry >= '48') && (keyEntry <= '57'))
				|| (keyEntry == '46') || keyEntry == '8' || keyEntry == '9'
				|| keyEntry == '32' || keyEntry == '37' || keyEntry == '39')
			return true;
		else
			return false;
	};
	function isAlphabetswithSpecialChars(txt, e) {
		keyEntry = (e.keyCode) ? e.keyCode : e.which;
		if (((keyEntry >= '65') && (keyEntry <= '90'))
				|| ((keyEntry >= '97') && (keyEntry <= '122'))
				|| (keyEntry == '46') || keyEntry == '8' || keyEntry == '9'
				|| keyEntry == '32' || keyEntry == '37' || keyEntry == '39' || keyEntry == '45')
			return true;
		else
			return false;
	};
	function isNumeric(txt, e) {
		charCode = (e.keyCode) ? e.keyCode : e.which;
		var len = txt.value.length;
		if (len == 3)
			txt.value = txt.value + '-'
		if (len == 7)
			txt.value = txt.value + '-'
		if ((charCode >= 48 && charCode <= 57) || charCode == 40
				|| charCode == 41 || charCode == 43 || charCode == 32
				|| charCode == 8 || charCode == 46 || charCode == 37
				|| charCode == 39) {
			return true;
		} else {
			return false;
		}
	};
</script>
</head>
<body>
	<div class="page-header">
		<h1><%=title%>
			Patient
		</h1>
	</div>
	<%@include file="/WEB-INF/includes/breadcrumbs.jsp"%>
	<%
		if (tooMany) {
	%>
	<div class="row">
		<div class="span9">
			<div class="alert alert-error">
				<h3>Patient limit reached</h3>
				<p>
					Your practice has a limit of
					<%=corp.getMaxPatients()%>
					patients per month, which you have reached.
				</p>
				<p>Please contact support to increase your subscription or
					delete some patients.</p>
			</div>
		</div>
	</div>
	<%
		} else {
	%>

	<div class="row">
		<div class="span9">
			<%@ include file="/WEB-INF/includes/formError.jsp"%>
			<form action="edit.jsp" method="post" class="form-horizontal">
				<input type="hidden" name="id" value="<%=id%>">
				<input type="hidden" name="subscriptionDate" value="<%=subDate%>">
				<%=HTML.textNameAlphabetsInput("fname", "First Name", "50", "50", fname, null, errorFields, "return isAlphabetswithSpecialChars(this,event);")%>
				<%=HTML.textNameAlphabetsInput("lname", "Last Name", "50", "50", lname, null, errorFields, "return isAlphabetswithSpecialChars(this,event);")%>
				<%if(p != null){ %>
      		<div class="control-group">
      		<label class="control-label" for="email">Email</label>
			 <div class="controls">
				<input id="email" readonly="readonly" class="input-xlarge" type="text" value="<%=p.getEmail()%>" name="email" maxlength="100" size="40">
			 </div>
			</div>
      			<%}else{ %>
				<%=HTML.textInput("email", "Email", "100", "100", email, null, errorFields)%>
				<%}%>
				<%=HTML.textNumericInput("phone", "Phone", "15", "15", phone, null, errorFields, "return isNumeric(this,event);")%>
				<%=HTML.textNameAlphabetsInput("ref", "Reference", "50", "50", ref, null, errorFields, "return isAlphabetswithDot(this,event);")%>
				<div class="control-group" style="margin-top: 15px;"><label class="control-label"> Subscription</label>
		            <div class="controls">
		            <%
		            PaymentSubscription ps= new PaymentSubscription();
		            if(p != null){
		            	ps = lm.findPaymentSubscriptionById(p.getId());
		            }
		            if(ps != null && ps.getExpired()!=null && ps.getExpired().compareTo(new Date()) < 0 ){
		            	subId = "1"; 
		             }%>
		            	<select name="subscription" id="subscription" class="input-xlarge">
		            	<option value="2" <%="4".equals(subId) ? "selected" : "" %>>Free Trial</option>
		            	<option value="3" <%="3".equals(subId) ? "selected" : "" %>>1 Month</option>
		            	<option value="4" <%="4".equals(subId) ? "selected" : "" %>>3 Months</option>
		            	<option value="5" <%="5".equals(subId) ? "selected" : "" %>>6 Months</option>
		            	<option value="0" <%="0".equals(subId) ? "selected" : "" %>>Print Only/No App Access</option>
		            	<option value="1" <%="1".equals(subId) ? "selected" : "" %>>None</option>
		            	</select>   
			        </div>
		        </div>
				<%=HTML.submitButton("Save")%>
			</form>
		</div>
	</div>
	<%
		}
	%>
</body>
</html>
