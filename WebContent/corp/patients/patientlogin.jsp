<%@ page import="au.com.ourbodycorp.Util" %>
<%@page import="au.com.ourbodycorp.model.Patient"%>
<%@page import="au.com.ourbodycorp.model.managers.PatientManager"%>
<%@ page import="au.com.ourbodycorp.Config" %>
<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.UUID" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%
String email = (request.getParameter("email") != null) ? request.getParameter("email").trim().toLowerCase() : "";
String password = (request.getParameter("password") != null) ? request.getParameter("password").trim() : "";
long pId =0;
if (!request.getMethod().equalsIgnoreCase("post")){
pId=Long.parseLong(Util.notNull(request.getParameter("id"), "0"));
request.setAttribute("id", pId);
session.setAttribute("pid", pId);
}

String error = null;
PatientManager pm = new PatientManager();
//System.out.println("Email: "+email);
//long id = pm.getProgramId(email);
LibraryManager lm = new LibraryManager();

//System.out.println("Id "+id);
//Patient patient = lm.getPatient(program.getPatientId());
//System.out.println("program: "+program.getId());


if (request.getMethod().equalsIgnoreCase("post"))
{
   if(email.length() == 0)
   {
	  error = "Please enter a email";
   } else if (!email.matches("[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}")) 
   {
	error = "Please enter a valid email address";
   } else if(password.length() == 0)
   {
    error = "Please enter a password";
    }
 }

if (request.getMethod().equalsIgnoreCase("post") && email.length() > 0 && password.length() > 0) {
	long id= (Long)session.getAttribute("pid");
	Program program=null;
	if(id>0){
	program = lm.getProgram(id);
	}else{
		long id2 = pm.getProgramId(email);
		program = lm.getProgram(id2);
		
	}
//     UserManager mm = new UserManager();
//     User m = mm.getUserByEmailOrUserName(email);
     if(pm.validate(email, password))
     {
    	 
    	 session.setAttribute("patient_session", email);
    	 String pdfUrl = "/pdf/pdf_printout.jsp?id=" + program.getId();
    	 //System.out.println("url="+pdfUrl);
    	 response.sendRedirect(pdfUrl);
     }
     else
     {
         error="Invalid Email or Password"; 
    	 /* response.sendError(403, "The user does not exist."); */
     }
 }

 
%>
<html>
  <head>
  <title>PatientLogin</title>
  
  </head>
  <body>
  <div class="page-header" style="padding-top: 50px;">
    <h1>PatientLogin</h1>
  </div>


  <div class="row">
    <div class="span4">
      <h2>Welcome</h2>
      <p>Use your registered email address and password to log in to patient login<%-- <%=Config.getString("site.name")%> --%>.</p>
      <p>If you have forgotten your password, <a href="forgot.jsp">click here</a> to reset it.</p>
          </div>
  <div class="span5">
  <%@include file="/WEB-INF/includes/formErrorSimple.jsp"%>
 
  <form action="patientlogin.jsp" method="post" class="form-horizontal">
      <fieldset>
          <legend>Enter email and password</legend>

<%--       <input type="hidden" name="return" value="<%=returnUrl%>"/>
 --%>      <%if(request.getParameter("loginEmail") != null){ %>
      	<div class="control-group">
				<label class="control-label" for="email">Email</label>
			<div class="controls">
				<input id="email" readonly="readonly" class="input-xlarge" type="text" value="<%=request.getParameter("loginEmail")%>" name="email" maxlength="100" size="40">
			</div>
		</div>
      <%}else{ %>
          <%=HTML.textInput("email", "Email", "40", "100", null)%>
      <%} %>
          <%=HTML.passwordInput("password", "Password", "40", "40", null, null)%>

          <div class="control-group">
            <label class="control-label" for="optionsCheckbox">&nbsp;</label>
            <div class="controls">
            
              <label class="checkbox">
<%--                 <input type="checkbox" name="remember" <%=(remember) ? "checked" : ""%> value="true" /> Keep me logged in. --%>  
            </label>
            </div>
          </div>
        <%--   <%
              if (true) {
          %>
          <div class="control-group">
              <label for="country" class="control-label">Disclaimer</label>
              <div class="controls">
                  <div class="span3 well" style="margin-left: 0;">
                    <p>Before loging in, you must <a href="inline/disclaimer.jsp" class="fancybox">read and accept the disclaimer</a>.</p>
                  </div>
              </div>
          </div>
          <%
              }
          %> --%>

      <%=HTML.submitButton("Submit")%>
          </fieldset>
  </form>
        </div>
      </div>
  </body>
</html>