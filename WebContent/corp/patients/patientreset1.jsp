<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.Patient" %>
<%@ page import="au.com.ourbodycorp.model.managers.PatientManager" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.regex.*" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
   Patient p = (Patient) session.getAttribute("resetForPatient");
   if (p == null) {
    response.sendRedirect("/");
    return;
   }
   HashMap<String,String> errorFields = new HashMap<String, String>();
    String pw1 = request.getParameter("pw1");
    String pw2 = request.getParameter("pw2");
    if (pw1 != null && pw2 != null) {
    	boolean b1=pw1.matches("[a-zA-Z]+");
        boolean b2= pw1.matches("[0-9]+"); 
        boolean b3= pw1.matches("[^A-Za-z0-9 ]+");
        boolean b4=pw1.matches("[^a-zA-Z]+");
        boolean b5=pw1.matches("[^0-9]+");
        if(b1==true || b2==true || b3==true || b4==true || b5==true){
        	errorFields.put("pw1", "Your new password must be AlphaNumeric");
        }else if (pw1.length() < 5) {
            errorFields.put("pw1", "Your new password must be at least 5 characters");
            errorFields.put("pw2", "");
        } else if (!pw1.equals(pw2)) {
            errorFields.put("pw1", "New passwords do not match");
            errorFields.put("pw2", "");
        } else {
        	PatientManager pm = new PatientManager();
        	String email=p.getEmail();
            pm.savePatient(Util.encryptPassword(pw1),email);
            pm.deletePasswordReset(p.getId());
            response.sendRedirect("patientreset2.jsp");
            return;
        }
       } 
    %>
<html>
<head><title>Reset Password</title></head>
<body>

<div class="page-header">
    <h1>Reset Password</h1>
</div>

<div class="row">
    <div class="span9">
        <%@ include file="/WEB-INF/includes/formError.jsp" %>
        <form action="patientreset1.jsp" method="post" class="form form-horizontal">
            <fieldset>
                <legend>New Password for <%=p.getEmail()%></legend>
          <%=HTML.passwordInput("pw1", "Password", "25", "25", null, null, errorFields)%>
          <%=HTML.passwordInput("pw2", "Confirm Password", "25", "25", null, null, errorFields)%>
                </fieldset>
          <%=HTML.submitButton("Submit")%>
</form>
    </div>
    <div class="span4 columns">
        &nbsp;
    </div>
</div>
</body>
</html>