<%@page import="java.util.Date"%>
<%@page import="org.joda.time.DateTime"%>
<%@page import="au.com.ourbodycorp.model.managers.SubscriptionManager"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="java.util.LinkedList" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="java.util.Date" %>
<%
	Calendar cal = Calendar.getInstance();
	Date renevalDate =null;
	Date expiredDate=null;
	Date toMonth=null;
	Date todayDate = new Date();
    CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");
	
    if (cu == null) {
        response.sendRedirect(Util.loginUrl(Constants.LoginReason.login, request));
        return;
    }
    User u = cu.getUser();
    Corporation corp = cu.getCorporation();
    
    LibraryManager lm = new LibraryManager();

    int currentPage = 1;
    try {
        currentPage = Integer.parseInt(request.getParameter("page"));
    } catch (NumberFormatException nfe) {
        // ignore
    }
    SimpleDateFormat sdf = new SimpleDateFormat("d MMM yyyy");
    SubscriptionManager sm=new SubscriptionManager();
    String query = (request.getParameter("query") != null && request.getParameter("query").trim().length() > 0) ? request.getParameter("query") : null;

    int perPage = 15;
    int count = 0;
    String activity_page="corp/patients/index.jsp";
	String corp_id=String.valueOf(corp.getId());
	String corp_name=corp.getName();
	String corp_loc=corp.getSuburb();
	String user_id=String.valueOf(u.getId());
	String user_name=u.getFullName();
	String invitor_id="";
	String invitor_name="---";
	String flag="view";
    String from="---";
	String to="---";
    if (query != null) {
    	 try{
    		//Util.allActivitiesLogs("Patients", u.getId(), corp.getName(), "Search Patients");
    		Util.allActivitiesLogs2("Patients",activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,"Search Patients By User");
        }catch(Exception ee){
        	
        } 
        count = lm.findPatientsCount(corp.getId(), query);
    } else {
    	 try{
    		//Util.allActivitiesLogs("Patients", u.getId(), corp.getName(), "View Patients");
    		Util.allActivitiesLogs2("Patients",activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,"View Patients By User");
        }catch(Exception ee){
        	
        } 
    	
        count = lm.countPatients(corp.getId());
    }


    int pages = count / perPage;
    if (count % perPage > 0) pages++;

    if (currentPage > pages + 1) {
        currentPage = pages;
    }

    List<Patient> patients=null;
    if (query != null) {
        patients = lm.findPatients(corp.getId(), query, (currentPage - 1) * perPage, perPage);
    } else {
        patients = lm.getPatients(corp.getId(), (currentPage - 1) * perPage, perPage);
    }

    String[][] breadcrumbs = {{"/corp/", corp.getName()}, {null, "Patients"}};

    if (query != null) {
        breadcrumbs = new String[][]{{"/corp/", corp.getName()}, {"/corp/patients/", "Patients"}, {null, "Search Results"}};
    }

    LinkedList<String> pageList = new LinkedList<String>();
    for (int i = 1; i <= pages; i++) {
        if (query != null) {
            pageList.add("?query=" + query + "&page=" + i);
        } else {
            pageList.add("?page=" + i);
        }

    }


%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head><title>Patients</title>
<script type="text/javascript">
$(document).ready(function () {
    $('.device_hover').each(function (key, val) {
        $(val).mouseenter(function () {
        	var search = $(this).html().search("img");
        	if(search != -1){
        		var imgid = $(this).html().split("\"");
        		var val = $(this).html().split("=");
                var val1 = val[2].split("\"");
                var id = val1[0];
                $.ajax({
            		url: 'update_patient.jsp?id='+id+'',
                    type: 'POST',
                    success: function(data) {
                        $("#"+imgid[3]+"").hide();
                    }
                 });
        	}
        });
    });
    
    $('.device_hover').click(function(event) {
    	var search = $(this).html().search("img");
    	if(search != -1){
    		var imgid = $(this).html().split("\"");
    		var val = $(this).html().split("=");
            var val1 = val[2].split("\"");
            var id = val1[0];
            $.ajax({
        		url: 'update_patient.jsp?id='+id+'',
                type: 'POST',
                success: function(data) {
                    $("#"+imgid[3]+"").hide();
                }
             });
    	}
    });
});
</script>
<script src="/js/sortable.js">
</script>
<style>
.span9{
     width: 940px;
}
</style>
</head>
<body>
<div class="page-header">
    <h1>Patients</h1>
</div>
<%@include file="/WEB-INF/includes/breadcrumbs.jsp"%>

<div class="row">
    <div class="span2">
        <a href="edit.jsp" class="btn btn-primary"><i class="icon-plus icon-white"></i> Add Patient</a>
    </div>
    <div class="span4">
        <%
            if (query == null && corp.getMaxPatients() > 0) {
                int max = corp.getMaxPatients();
                int createdThisMonth = lm.countOfPatientsCreatedThisMonth(corp.getId());
        %>
        You are allowed a maximum of <%= max%> patients per month. You've added <%=createdThisMonth%> this month. leaving you <%= max - createdThisMonth%> more.
        <%
            } else {
        %>
        &nbsp;
        <%
            }
        %>
    </div>

    <div class="span3">
        <form class="form-search" method="get" action="/corp/patients/">
        <input type="text" class="input-medium search-query" name="query" value="<%=Util.formValue(query)%>">
        <button type="submit" class="btn">Search</button>
        </form>
    </div>
</div>
<div class="row">
    <div class="span9 columns horizontalstyle">
        <table class="table table-zebra-stripes sortable" border="0" style="overflow-x: scroll;">
            <thead>
            <tr>
                <th style="width:20pc;">Name</th>
                <th style="width:20pc;">Email</th>
                <th style="width:20pc;">Phone</th>
                <th style="width:20pc;">Reference</th>
                <th style="width:20pc;">Registration Date</th>
                <th style="width:20pc;">Subscription Date</th>
                <th style="width:20pc;">Subscription Plan</th>
                <th style="width:20pc;">Expires On</th>
                <th style="width:20pc;">Renewal Date</th>
                <th style="width:20pc;">Status</th>
                <th style="width:20pc;">Delete</th>
            </tr>
            </thead>
            <tbody>
            <%
            int countt=0;
            int rowcount = 1;
            int i=patients.size()-1;
            
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy MM dd");
            Date date = new Date();
            System.out.println(dateFormat.format(date));
    //        System.out.println(dateFormat.format(paymentSubscriptionlist.get(i).getExpired()));
            
           
           
           
   
           
           
            
                for (Patient p : patients) {
                	
                	PaymentSubscription ps= lm.findPaymentSubscriptionById(p.getId());
                	long subId=p.getSubscriptionId();
                	
                	String subName="";
                	String subPeriod="";
                	
                	
                	
                	
                	i--;
                	/* if(ps.getExpired()!=null && ps.getExpired().compareTo(new Date()) < 0 ){
                		sm.updatePatientSubExpiryDate(p.getId(),p.getEmail());
                	} */
                	/* if("false".equals(loginStatus)){
                		p.setSubscriptionId(1);
                	} */
            %>
            <tr>
                <td class="device_hover"><a href="detail.jsp?id=<%=p.getId()%>"><%=Util.textAreaValue(p.getFirstName())%> <%=Util.textAreaValue(p.getLastName())%></a>
                <%if(p.getNew_patient() != null){if(p.getNew_patient().equals("New_Patient") || p.getNew_patient().equals("Updated_Patient")){ %> <img width="30" border="0" id="img<%=rowcount%>" height="14" src="/images/new.gif" style="display: none;"><%}} %></td>
                <td><%=Util.textAreaValue(p.getEmail())%></td>
                <td><%=Util.textAreaValue(p.getPhone())%></td>
                <td><%=Util.textAreaValue(p.getReference())%></td>
                <td><%=sdf.format(p.getCreated())%></td>
                
                
                 <%if(ps != null && ps.getTransactionDate()!=null) {%>
                
                <td><%=sdf.format(ps.getTransactionDate())%></td>
                <%} else{%>
                <td></td>
                <%} %>
                
                
                
 <%Subscription subPatientList=sm.getSubscriptions(p.getSubscriptionId()); 
                 if(p.getSubscriptionId()>=0){
                 
                	
                 %>
                <td>
                
                <%
                    if(ps != null && ps.getExpired()!=null && ps.getExpired().compareTo(new Date()) < 0 ){%>
                    None
                    <%}else{
                    if(p.getSubscriptionId()==0){%>
                    Print Only / No App Access 
                     <%}
           
                    if(p.getSubscriptionId()==1){%>
                    None
                     <%}
                     if(p.getSubscriptionId()==2){%>
                     Free Trial
                     <%}
                     if(p.getSubscriptionId()==3){%>
                     1 Month
                     <%}
                     if(p.getSubscriptionId()==4){%>
                     3 Months
                     <%}
                     if(p.getSubscriptionId()==5){%>
                     6 Months
                     <%}
                      }}%>	
                     
                     
                     
                    <%if(ps != null && ps.getExpired()!=null) {%>
    
                 <td><%=sdf.format(ps.getExpired())%></td>
                 <%}else{ %>
                 <td></td>
                 <%} %> 
                     
                     
                  <%if(ps != null && ps.getExpired()!=null) {%>
                 <%renevalDate = ps.getExpired();
 	             cal.setTime(renevalDate); 
 	             cal.add(Calendar.DATE, 1);
 	             renevalDate = cal.getTime(); %>
            
                <td><%=sdf.format(renevalDate)%></td>
                 <%}else{%>
                 <td></td>
        
                 <%} %>
                                 
             <%if(p.getSubscriptionId()==0 || (p.getSubscriptionId() == 1 || (ps != null && ps.getExpired()!=null && ps.getExpired().compareTo(new Date()) < 0 ))){ %>
             	<td>Inactive</td>
             <%}else{ %>
             	<td>Active</td>
             <%} %>
               
              
           
               <%--   <% if(ps.getExpired()<=todayDate) { %>     
                   
                     <td>Expired</td>
                	 <% }else if(p.getSubscriptionId()>0)
                	 {%>
                	 <td>Active</td>
                <% }%> --%>
                     
                     
                  <%--    <%List<String> status=lm.getPatientSubscriptoin(p.getEmail());
                     if(status!=null && status.size()>0)
                     {
                    	 DateTime expired=new DateTime(status.get(1));
                    	 DateTime renewal=new DateTime(expired.plusDays(1));
                     %>
                     <td><%=sdf.format(expired.toDate())%></td>
                     <td><%=sdf.format(renewal.toDate())%></td>
                     <%}
                     if(p.getSubscriptionId()==2){                     
                     cal.add(Calendar.DATE, +3);
     	             expiredDate = cal.getTime();      	            
     	             renevalDate = expiredDate;
     	             cal.setTime(renevalDate); 
     	             cal.add(Calendar.DATE, 1);
     	             renevalDate = cal.getTime();
     	            %>
                    <td><%=sdf.format(expiredDate)%></td>
                    <td><%=sdf.format(renevalDate)%></td>
                    <%}
                     if(p.getSubscriptionId()==3){                     
                         cal.add(Calendar.MONTH, +1);
         	             expiredDate = cal.getTime();      	            
         	             renevalDate = expiredDate;
         	             cal.setTime(renevalDate); 
         	             cal.add(Calendar.DATE, 1);
         	             renevalDate = cal.getTime();
         	            %>
                        <td><%=sdf.format(expiredDate)%></td>
                        <td><%=sdf.format(renevalDate)%></td>
                        <%}

                     if(p.getSubscriptionId()==4){                     
                         cal.add(Calendar.MONTH, +3);
         	             expiredDate = cal.getTime();      	            
         	             renevalDate = expiredDate;
         	             cal.setTime(renevalDate); 
         	             cal.add(Calendar.DATE, 1);
         	             renevalDate = cal.getTime();
         	            %>
                        <td><%=sdf.format(expiredDate)%></td>
                        <td><%=sdf.format(renevalDate)%></td>
                        <%}

                     if(p.getSubscriptionId()==5){                     
                         cal.add(Calendar.MONTH, +6);
         	             expiredDate = cal.getTime();      	            
         	             renevalDate = expiredDate;
         	             cal.setTime(renevalDate); 
         	             cal.add(Calendar.DATE, 1);
         	             renevalDate = cal.getTime();
         	            %>
                        <td><%=sdf.format(expiredDate)%></td>
                        <td><%=sdf.format(renevalDate)%></td>
                        <%}                    
                     if(p.getSubscriptionId()==1) 
                     {%>
                     <td>Expired</td>
                	 <% }else if(p.getSubscriptionId()>0)
                	 {%>
                	 <td>Active</td>
                <% }
                	
                 }                   
                 else{%>
                    <td>&nbsp;&nbsp;&nbsp;</td>
                    <td>No Subscription</td>
                    <td>&nbsp;&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;&nbsp;</td>
                
                <%
                   }	 
                 %> --%>
                 <td><a class="btn btn-danger btn-mini fancybox" href="inline/delete_patient.jsp?id=<%=p.getId()%>">
						<i class="icon icon-trash icon-white"></i>
						Delete
					</a></td>
            </tr>
            <%
            rowcount = rowcount+1;
            countt=0; 
            }
            %>
            </tbody>
        </table>
        <%=HTML.pages(pageList, currentPage)%>
    </div>
</div>
</body>
</html>