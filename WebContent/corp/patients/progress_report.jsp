<%@page import="org.apache.velocity.runtime.directive.Parse"%>
<%@page import="java.util.Date"%>
<%@ page import="au.com.ourbodycorp.Util"%>
<%@ page import="au.com.ourbodycorp.model.*"%>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager"%>
<%@ page import="au.com.ourbodycorp.model.managers.UserManager"%>
<%@ page import="java.text.SimpleDateFormat"%>

<%@ page import="java.util.Calendar"%>
<%@ page import="org.apache.commons.lang.time.DateUtils"%>

<%@ page import="java.util.HashMap"%>
<%@ page import="java.util.LinkedList"%>
<%@ page import="java.util.List"%>
<%@ page import="au.com.ourbodycorp.model.managers.CorporationManager"%>
<%@ page import="java.util.List"%>
<%@	page import="java.util.ArrayList"%>
<%@ page import="java.sql.Connection"%>
<%@ page import="java.sql.PreparedStatement"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.Collections"%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%!// --- String Join Function converts from Java array to javascript string.  
	public String join(ArrayList<?> arr, String del) {

		StringBuilder output = new StringBuilder();

		for (int i = 0; i < arr.size(); i++) {

			if (i > 0)
				output.append(del);

			// --- Quote strings, only, for JS syntax  
			if (arr.get(i) instanceof String)
				output.append("\"");
			output.append(arr.get(i));
			if (arr.get(i) instanceof String)
				output.append("\"");
		}

		return output.toString();
	}%>

<%
	CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");
	Corporation c = (Corporation) request.getAttribute("corp");
	  Corporation corp = cu.getCorporation();
	   User u = cu.getUser();
	boolean showLocation = request.getParameter("showLocation") != null
			&& Boolean.parseBoolean(request.getParameter("showLocation"));
	String[] multitype = null;
	multitype = request.getParameterValues("multitype");
	long pgmid = Long.parseLong(request.getParameter("id"));
	long patientId = Long.parseLong(Util.notNull(request.getParameter("patient")));
	long programId = Long.parseLong(Util.notNull(request.getParameter("id"), "0"));
	LibraryManager lm = new LibraryManager();
	Patient p2 = lm.getPatient(patientId);
	if(p2!=null)
    {
    	String exerciseId = request.getParameter("excerciseid");
    String types="Patients";
    String activity_page="corp/patients/progress_report.jsp";
	String corp_id=String.valueOf(corp.getId());
	String corp_name=corp.getName();
	String corp_loc=corp.getSuburb();
	String user_id=String.valueOf(u.getId());
	String user_name=u.getFullName();
	String invitor_id=String.valueOf(p2.getId());
	String invitor_name=p2.getFullName();
	String flag="add";
	String desc="Add Exercise By User";
	String from="Exercise add";
	String to="List of Exercise is add"+exerciseId;
	 try{
	    	//Util.allActivitiesLogs("Patients", u.getId(), corp.getName(), "Edit Exercise");
		 Util.allActivitiesLogs2("Patients",activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
		 System.out.println("Detailed Progress Report");
 	}catch(Exception ee){
 		ee.printStackTrace();
 		//System.err.append("Exception on editecercise",ee.printStackTrace());
 		System.out.println("Exception on Detailed Progress Report"+ee.getMessage());
 	} 
    }
	
	
	String type = request.getParameter("type");
	String retvalprevious = null;
	String retvalnext = null;
	if(type!=null){
		retvalprevious = request.getParameter("date");
		retvalnext=request.getParameter("date");
		System.out.println( "retvalprevious::::::::::::::::::::::::::::::::::::"+retvalprevious);
		System.out.println( "retvalnext::::::::::::::::::::::::::::::::::::"+retvalnext);	
	}
	System.out.println("Type::::" + type);
	Calendar cal = Calendar.getInstance();
	Date today = cal.getTime();
	SimpleDateFormat s = new SimpleDateFormat("MM/dd/yy");
	String currentdate = s.format(today);

	ArrayList<String> barchartdate = null;
	ArrayList<String> YAxis = new ArrayList<String>();
	List<Integer> comparingList = new ArrayList<Integer>();
	String startdate=null;
	if (type == null) {
		barchartdate = lm.getDateofBarchart(null, currentdate);
		for (int i = 0; i < barchartdate.size(); i++) {
			System.out.println(barchartdate.get(6) + "start dateeeee");
			YAxis.add(barchartdate.get(i));
			
		}
		
	} else {
		if (type.equalsIgnoreCase("previous")) {
			System.out.println(retvalprevious + "retvalprevious date");
				barchartdate = lm.getDateofBarchart(type, retvalprevious);
				System.out.println(retvalprevious + "retvalprevious date");
				for (int i = 0; i < barchartdate.size(); i++) {
					System.out.println(barchartdate.get(i) + "previous date");
					YAxis.add(barchartdate.get(i));
				}
				
			}else{
				barchartdate = lm.getDateofBarchart(type, retvalnext);
				for (int i = 0; i < barchartdate.size(); i++) {
					System.out.println(barchartdate.get(i) + "next date");
					
					YAxis.add(barchartdate.get(i));
				}

			} 
	}
	int sum=0;
	if(type==null ||type.equalsIgnoreCase("previous")){
	retvalprevious = barchartdate.get(6);
	retvalnext = barchartdate.get(0);
	}else{
		retvalnext = barchartdate.get(6);
	}
	
	int tests=0;
	ArrayList<BarchartData> pe = lm.saveTotalDuration(programId,retvalprevious,retvalnext );
	ArrayList<BarchartData> scoresAvg= lm.scoreAvg(programId,retvalprevious,retvalnext );
    ArrayList<BarchartData> painLevelAvg = lm.painLevelAvg(programId,retvalprevious,retvalnext);
	System.out.println("retvalprevious::::" + retvalprevious);
	System.out.println("retvalnext::::" + retvalnext);
	Program frq = lm.getActualFreq(programId);
	System.out.println("freq val::::1" + frq.getInterval());
	System.out.println("freq type::::2" + frq.getIntervalType());
	ArrayList<Integer> adherencescore = new ArrayList<Integer>();
	ArrayList<Integer> frequencyscore = new ArrayList<Integer>();
	ArrayList<Integer> frequencyscoreinpercentage = new ArrayList<Integer>();
	ArrayList<String> dateDone = new ArrayList<String>();
	ArrayList<String> freqDateDone = new ArrayList<String>();
	ArrayList<String> tempListforaherence = new ArrayList<String>();
	ArrayList<String> tempListforpain = new ArrayList<String>();
	ArrayList<Integer> XAxis = new ArrayList<Integer>();
	ArrayList<Integer> Xaxisforpain = new ArrayList<Integer>();
	ArrayList<Integer> XaxisforFreq = new ArrayList<Integer>();
	ArrayList<Integer> XaxisforFreqPercentage = new ArrayList<Integer>();
	ArrayList<String> YLable = new ArrayList<String>();
	ArrayList<Float> painLevel = new ArrayList<Float>();
	ArrayList<String> notes = new ArrayList<String>();
	for(String dt:YAxis){
		for (BarchartData adherence : scoresAvg) {
		String createdDate = s.format(adherence.getDateDone());
		if(createdDate.equals(dt)){
				System.out.println("createdDate"+createdDate);
				System.out.println("dt"+dt);
				if(100<=adherence.getAdhenceAvg()){
					adherencescore.add(100);
				}else{
					adherencescore.add(adherence.getAdhenceAvg());
				}
				frequencyscoreinpercentage.add(adherence.getFreqAvg());
				dateDone.add(createdDate);
				frequencyscore.add(adherence.getProgramFreqValue());
				tempListforaherence.add(dt);
				System.out.println("adhere avg::::" +adherence.getAdhenceAvg());
				System.out.println("frq avg::::" +adherence.getProgramFreqValue());
				System.out.println("dateDone avg::::" +createdDate);
				break;
			}
		}
		if(!tempListforaherence.contains(dt)){
			tempListforaherence.add(dt);
			adherencescore.add(0);
			frequencyscoreinpercentage.add(0);
			frequencyscore.add(0);
		}
	}
	
	for(String dt:YAxis){
		for (BarchartData painlevel : painLevelAvg) {
		String createdDate = s.format(painlevel.getDateDone());
		if(createdDate.equals(dt)){
				System.out.println("createdDate"+createdDate);
				System.out.println("dt"+dt);
				dateDone.add(createdDate);
				painLevel.add(painlevel.getPainlevlAvg());
				tempListforpain.add(dt);
				System.out.println("painLevel avg::::" +painlevel.getPainlevlAvg());
				System.out.println("dateDone avg::::" +createdDate);
				break;
			}
		}
		if(!tempListforpain.contains(dt)){
			painLevel.add((float)0);
		}
	}
	
for(String test:YAxis){
	SimpleDateFormat sr = new SimpleDateFormat("MM/dd");
	Date date=sr.parse(test);
	test=sr.format(date);
	YLable.add(test);
}

int XLabel = 0;  
while(XLabel <=frq.getInterval())  
{  
	if(XLabel==0){
		XAxis.add(XLabel);  
	}
	XAxis.add(XLabel=XLabel+1);  
}   

int pain = 0;  
while(pain <=5)  
{  
	if(pain==0){
		Xaxisforpain.add(pain);  
	}
	Xaxisforpain.add(pain=pain+1);  
}  
int maxvalueofFre=0;
maxvalueofFre= Collections.max(frequencyscore);
 if(maxvalueofFre!=0){
	 int fre=0;
	
	 while(fre <=(maxvalueofFre-1))  
	 {  
	 	if(fre==0){
	 		XaxisforFreq.add(fre);  
	 	}
	 	XaxisforFreq.add(fre=fre+1);  
	 }  
 
 }else {
		 int fre=0;
		 while(fre <= 2)  
		 {  
		 	if(fre==0){
		 		XaxisforFreq.add(fre);  
		 	}
		 	XaxisforFreq.add(fre=fre+1);  
		 }  

 }
 int maxValueOfFreqPercetage=Collections.max(frequencyscoreinpercentage);
 if(200<=maxValueOfFreqPercetage){
	 int frePerc=0;
	
	 while(frePerc <=maxValueOfFreqPercetage)  
	 {  
	 	if(frePerc==0){
	 		XaxisforFreqPercentage.add(frePerc);  
	 	}
	 	XaxisforFreqPercentage.add(frePerc=frePerc+50);  
	 }  
 
 } else {
		 int frePerc=0;
		 while(frePerc<=150)  
		 {  
		 	if(frePerc==0){
		 		XaxisforFreqPercentage.add(frePerc);  
		 	}
		 	XaxisforFreqPercentage.add(frePerc=frePerc+50);  
		 }  

 } 
Program	programsss = lm.getProgram(programId);

String[][] breadcrumbs = {{"/corp/", corp.getName()}, {"/corp/patients/", "Patients"},{"/corp/patients/detail.jsp?id="+patientId,p2.getFullName()},{"/corp/patients/detail.jsp?id="+patientId,programsss.getName()}, {null , "Progress Report"}};

if ((type ==null) ||type.equalsIgnoreCase("previous")) {
Collections.reverse(YLable);
Collections.reverse(adherencescore);
Collections.reverse(frequencyscore);
Collections.reverse(painLevel);
Collections.reverse(frequencyscoreinpercentage);
}

%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Progress Report</title>
<script type="text/javascript" src="/js/zingchart.min.js"></script>
<script>
    zingchart.MODULESDIR = "https://cdn.zingchart.com/modules/";
    ZC.LICENSE = ["569d52cefae586f634c54f86dc99e6a9", "ee6b7db5b51705a13dc2339db3edaf6d"];
  </script>
<style>		
	#newLineChart{overflow:visible !important;}		
	#myChart2{overflow:visible !important;}		
	#myChart1{overflow:visible !important;}		
	#myChart{overflow:visible !important;}		
	.tooltip {
    position: relative;
    display: inline-block;
    opacity:1;
    padding:0;
}

.tooltip .tooltiptext {
    visibility: hidden;
    width: 200px;
    background-color: black;
    color: #fff;
    text-align: center;
    border-radius: 6px;
    padding: 5px 0;
    position: absolute;
    z-index: 1;
    top: -2px;
    left: 100%;
    margin-left: 12px;
}

.tooltip .tooltiptext::after {
    content: "";
    position: absolute;
    top: 50%;
    right: 100%;
    margin-top: -5px;
    border-width: 5px;
    border-style: solid;
    border-color: transparent black transparent transparent;
}
.tooltip:hover .tooltiptext {
    visibility: visible;
}

.tooltip1 {
    position: relative;
    display: inline-block;
    opacity:1;
    padding:0;
}

.tooltip1 .tooltiptext1 {
    visibility: hidden;
    width: 200px;
    background-color: black;
    color: #fff;
    text-align: center;
    border-radius: 6px;
    padding: 5px 0;
    position: absolute;
    z-index: 1;
    top: -2px;
    right: 100%;
    margin-left: 12px;
}

.tooltip1 .tooltiptext1::after {
        content: "";
    position: absolute;
    top: 50%;
    left: 100%;
    margin-top: -5px;
    border-width: 5px;
    border-style: solid;
    border-color: transparent black transparent transparent;
    transform: rotate(180deg);
}
.tooltip1:hover .tooltiptext1 {
    visibility: visible;
}
	</style>
  
  <script>
  var XaxisforFreqPercentage = [<%=join(XaxisforFreqPercentage, ",")%>];
	var dateDone = [<%=join(YLable, ",")%>];
	var adherencescore = [<%=join(adherencescore, ",")%>];
	var frequencyscoreinpercentage = [<%=join(frequencyscoreinpercentage, ",")%>];
	var xaxisforfreq = [<%=join(XAxis, ",")%>];
	$(document).ready(function() {
		var type=<%=type%>;
		
		if(type==null){
			  $("#next").hide();
		}
		var myChart1= {
			"type" : "bar",
			"title" : {
				"margin-top": 20,
				"text" : "Exercise Frequency - Adherence Score"
			},
			 "plotarea": {
				 "margin-top": 100,
				  },
			"plot" : {
				"value-box" : {
					"text" : "%v%","color":"gray"
				},
				"tooltip" : {
					"text" : "%v%"
				}
			},
			
			"scale-x" : {
				"labels" : dateDone

			},
			"scale-y" : {
				"format":"%v%",
				"values" : XaxisforFreqPercentage
			},
			"series" : [{
				"values" : frequencyscoreinpercentage,

				"text" : "Exercise Frequency - Adherence Score",
				"background-color":"#db6e00"
			} ]
		};
		zingchart.render({
			id : "myChart1",
			data : myChart1,
			height : "80%",
			width : "45%"
		});

	});
</script>
<script>
	var dateDone = [<%=join(YLable, ",")%>];
	var adherencescore = [<%=join(adherencescore, ",")%>];
	var frequencyscore = [<%=join(frequencyscore, ",")%>];
	var xaxisforfreq = [<%=join(XAxis, ",")%>];
	$(document).ready(function() {
		var type=<%=type%>;
		
		if(type==null){
			  $("#next").hide();
			  $("#lablenext").hide();
		}
		var myChart = {
			"type" : "bar",
			"title" : {
				"margin-top": 20,			
				"text" : "Exercise Program Completion"
			},
			 "plotarea": {
				 "margin-top": 100,
				  },
			"plot" : {
				"value-box" : {
					"text" : "%v%","color":"gray"
				},
				"tooltip" : {
					"text" : "%v%"
				}
			},
			
			"scale-x" : {
				"labels" : dateDone

			},
			"scale-y" : {
				"format":"%v%",
				"values" : [ 0,25, 50,75, 100 ]
			},
			"series" : [{
				"values" : adherencescore,

				"text" : "Exercise Program Completion",
				"background-color":"#db6e00"
			} ]
		};
		zingchart.render({
			id : "myChart",
			data : myChart,
			height : "80%",
			width : "45%"
		});

	});
</script>
<script>
	var dateDone = [<%=join(YLable, ",")%>];
	var frequencyscore = [<%=join(frequencyscore, ",")%>];
	var xaxisforfreq = [<%=join(XaxisforFreq, ",")%>];
    var ActualFreq=<%=programsss.getInterval()%>
	$(document).ready(function() {
		var type=<%=type%>;
		
		if(type==null){
			  $("#next").hide();
		}
		var myChart2 = {
			"type" : "bar",
			"title" : {
				
				"margin-top": 20,
				"text" : "Exercise Frequency – Number Of Times"
			}, 
			"legend": {
				"margin-top": 70,
				"toggle-action": "hide",
			    "header": {
			      "text": "Exercise Frequency – Number Of Times"
			    },
			    "item": {
			      "cursor": "pointer"
			    },
			    "draggable": true,
			    "drag-handler": "icon"
			  },

			
			 "plotarea": {
				 "margin-top": 140,"line-width": 1
				  },
			"plot" : {
				"value-box" : {
					"text" : "%v","line-width": 1,"color":"gray"
				},
				"tooltip" : {
					"text" : "%v",
					"line-width": 0
				}
			},
			
			"scale-x" : {
				"labels" : dateDone

			},
			"scale-y" : {
				"values" : xaxisforfreq,
				"line-width": 1
			},
			"series" : [{
				"values" : frequencyscore,
				"text" : "Recommended Number Of Times : "+ActualFreq,
				"background-color":"#db6e00"
			} ]
		};
		zingchart.render({
			id : "myChart2",
			data : myChart2,
			height : "80%",
			width : "45%"
		});

	});
</script>

<script>
var dateDone = [<%=join(YLable, ",")%>];
var painlevel = [<%=join(painLevel, ",")%>];
$(document).ready(function() {
    var myConfig = {
      type: 'line',
        title: {
        adjustLayout: true,
        text: "Pain Symptom Average Rating",
        	"margin-top": "20"
      },
      plot: {
        valueBox: {
        	"text" : "%v","line-width": 1,"color":"gray"
        }
      },

      scaleX: {
    	 
    	  values:dateDone
      },
      scaleY: {
        guide: {
          lineStyle: 'solid'
          
        },
        label: {
          text: 'Pain Symptom Average Rating'
        },
        values:[0,1,2,3,4,5]
      },
      tooltip: {
        text: "%v",
        borderRadius: 2,verticalAlign: 'top'
      },
      crosshairX: {
        lineColor: '#000',
        scaleLabel: {
          borderRadius: 2
        }
       
      },
      series: [{
        values:painlevel,
        verticalAlign: 'top',
        text: "Pain Symptom Average Rating:",
        lineColor: '#f57c00',
        marker: {
          backgroundColor: '#f57c00'
        }
      }]
    };

    zingchart.render({
      id: 'newLineChart',
      data: myConfig,
      height: '80%',
      width: '45%'
    });

});
  </script>

</head>

<div class="page-header">
</div>

<body>
<div style="margin-top: 46px;"><%@include file="/WEB-INF/includes/breadcrumbs.jsp"%></div>
	<p>
	<table style="width:100%;"><tr><td>
	<h5 style="float: left;"><a id="previous" href="progress_report.jsp?id=<%=programId%>&patient=<%=patientId%>&type=previous&date=<%=retvalprevious%>">
		<div class="tooltip"><img 
			src="../patients/left.png"
			 name="previous"style="color: black"/><span class="tooltiptext">Click to View Previous Week</span></div>
			 
	</a></h5><h4 style="float: left;margin-left: 10px; color:#00f;">previous</h4></td><td style="padding-left: 311px; float:right;"> <h4 id="lablenext" style="padding-right: 8px;float:left;margin-left: 10px;margin-top:2px;color:#00f;">Next week</h4><h5 style="float:left;" id="next"><a id="next"
			href="progress_report.jsp?id=<%=programId%>&patient=<%=patientId%>&type=next&date=<%=retvalnext%>"
			 name="next" style="color: black">
			 <div class="tooltip1"><img 
			src="../patients/right.png"
			 name="previous"style="color: 0660A1"/><span class="tooltiptext1">Click to View Next Week</span></div></a></h5></td></tr>
		</table>	
	
	</p>
	<div id="newLineChart" style="margin-top: 20px;">
	 <div class="span6 well" style="margin-left: 50%;">
        <h3>Patient Notes</h3>
        <table class="table table-zebra-striped">	
            <tbody>
      <% for (BarchartData note : pe) {
          String body = note.getNote();
          Date date=note.getDateDone();
          if(body!=null){
          %>
         <tr>
                <td style="width: 75px; "><%=date%></td>
                <td class="span5" style="width: 350px;margin-left: 0;display: block;text-overflow: ellipsis;"><%=body %></td>
           </tr>
           <%} %>
            <%}%>
            </tbody>
        </table>
    </div>
	</div>
	 <div id="myChart2" style="margin-top:-46px;"></div>
	 <div id="myChart1" style="margin-top:-46px;"></div>
	 <div id="myChart" style="margin-top:-46px;">
	
	</div>
	
	 
</body>