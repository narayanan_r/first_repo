<%@page import="org.joda.time.DateTime"%>
<%@page import="au.com.ourbodycorp.model.managers.SubscriptionManager"%>
<%@page import="au.com.ourbodycorp.model.managers.UserManager"%>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="java.util.Date" %>
<%
    CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");
    Calendar cal = Calendar.getInstance();
	Date renevalDate =null;
	Date expiredDate=null;
    if (cu == null) {
        response.sendRedirect(Util.loginUrl(Constants.LoginReason.login, request));
        return;
    }
    User u = cu.getUser();
    Corporation corp = cu.getCorporation();
   /*  try{
    	Util.allActivitiesLogs("Patients", u.getId(), corp.getName(), "Patient Details");
    }catch(Exception e){
    	
    } */
    
    LibraryManager lm = new LibraryManager();

    long id = Long.parseLong(Util.notNull(request.getParameter("id"), "0"));

    boolean remove = Boolean.parseBoolean(Util.notNull(request.getParameter("remove"), "false"));
    if (remove){
        long groupId = Long.parseLong(Util.notNull(request.getParameter("groupId"), "0"));
        if (groupId > 0 && id > 0){
            lm.removePatientFromGroup(id, groupId);
        }
    }
    
    boolean followingTutorial = Boolean.parseBoolean(Util.notNull(request.getParameter("tutorial"), "false"));
    UserManager userManager = new UserManager();
    if (followingTutorial){
        userManager.setTutorialProgressForUser(u.getId(), UserManager.TUTORIAL_WENT_TO_PATIENT_PAGE, false);
    }
    int tutorialProgress = userManager.getTutorialProgressForUser(u.getId());
    
    Patient p = null;

    if (id > 0) {
        p = lm.getPatient(id);
        if (p == null || p.getCorpId() != corp.getId()) {
            response.sendError(404, "Patient not found or not part of this practice");
            return;
        }
        String type="Patient";
        String activity_page="corp/patients/detail.jsp";
    	String corp_id=String.valueOf(corp.getId());
    	String corp_name=corp.getName();
    	String corp_loc=corp.getSuburb();
    	String user_id=String.valueOf(u.getId());
    	String user_name=u.getFullName();
    	String invitor_id=String.valueOf(p.getId());
    	String invitor_name=p.getFullName();
    	String flag="view";
        String from="---";
        String to="---";
    	 String desc="View Patient "+invitor_name+" Details By User";
        try{
        	//Util.allActivitiesLogs("Patients", u.getId(), corp.getName(), "Patient Details");
        	Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
        }catch(Exception ee){
        	
        } 
    } else {
    	
        response.sendRedirect("/corp/patients/edit.jsp");
        return;
    }

    List<Program> programs = lm.getPrograms(p.getId());
    Program latestProgram = null;
    if (programs.size() > 0){
        latestProgram = programs.get(0);
    }

    List<PatientGroup> groupsPatientIsIn = lm.getGroupsForPatient(p.getId());

    String[][] breadcrumbs = {{"/corp/", corp.getName()}, {"/corp/patients/", "Patients"}, {null, "Details"}};

    SimpleDateFormat sdf = new SimpleDateFormat("d MMM yyyy");
 //   sdf.setTimeZone(corp.getTimeZone());

     System.out.println("p.getId()---"+p.getId());
    PaymentSubscription paymentSubscription=lm.findPaymentSubscriptionById(p.getId());
    if(paymentSubscription != null && paymentSubscription.getExpired()!=null){
    System.out.println("subscription exp date----"+paymentSubscription.getExpired());
    System.out.println("exp date from db---"+sdf.format(paymentSubscription.getExpired())); 
    }
%>
<html>
    <head><title>Patient</title></head>
    <body>
    <div class="page-header">
        <h1>Patient Details <small><%=Util.textAreaValue(p.getFullName())%></small></h1>
    </div>
    <%@include file="/WEB-INF/includes/breadcrumbs.jsp"%>
    <div class="row">
        <div class="span4">
            <h2>Details</h2>
            <p>
            <strong><%=Util.textAreaValue(p.getFullName())%></strong><br>
            <%=Util.textAreaValue(p.getEmail())%><br>
            <%=Util.textAreaValue(p.getPhone())%><br>
            <%
                if (p.getReference() != null && p.getReference().trim().length() >0) {
            %>
            <strong>Reference:</strong> <%=Util.textAreaValue(p.getReference())%><br>
            <%
                }
            %>
            </p>
            <p><a href="edit.jsp?id=<%=p.getId()%>" class="btn btn-primary" style="width:120px;"><i class="icon-white icon-edit"></i> Edit Details</a>
            <% if (corp != null && corp.isPushAllowed() && latestProgram != null){ %>
                <a href="/corp/programs/inline/push_message.jsp?programId=<%=latestProgram.getId()%>" class="btn btn-primary fancybox"><i class="icon-envelope icon-white"></i> Push Message</a>
            <% } %>
            </p>

        </div>
        <div class="span6">
            <h2>Programs for Patient</h2>
            <p>
                <a href="/corp/programs/inline/edit_details.jsp?patient=<%=p.getId()%>&Create=No_Share&Check=Name_Exist" class="btn btn-primary fancybox"><i class="icon-plus icon-white"></i> Add Program</a>
                <a href="/corp/programs/inline/select_template.jsp?patient=<%=p.getId()%>" class="btn btn-primary fancybox"><i class="icon-white icon-file"></i> Program from Template</a>
            </p>
            <table class="table table-zebra-striped">
                <tbody>
                <% 
                    int programsAdded = 0;
                    for (Program program : programs) {
                %>
                <tr>
                    <td><%=sdf.format(program.getCreated())%></td>
                    <td>
                        <a href="/corp/programs/edit.jsp?id=<%=program.getId()%><%=(tutorialProgress == UserManager.TUTORIAL_WENT_TO_PATIENT_PAGE)? "&tutorial=true" : "" %>" <%= (programsAdded == 0)? "id='first_program'" : ""%>><%=Util.notNull(program.getName()).length() > 0 ? Util.notNull(program.getName()) : "(not named)"%></a>
                    </td>
                     <td style="padding-top: 0px;padding-bottom: 0px;">
                        <a href="/corp/patients/progress_report.jsp?id=<%=program.getId()%>&patient=<%=p.getId()%>"><img src="bar.png"  alt="Cinque Terre" style= "max-width: 20px;margin-top: 7px;"></i></a>
                    </td>
                    <td>
                        <a href="/corp/programs/inline/delete_program.jsp?id=<%=program.getId()%>" class="fancybox"><i class="icon-trash"></i></a>
                    </td>
                   
                </tr>
                <%
                        programsAdded++;
                    } %>
                </tbody>
            </table>
        </div>
    </div>
  
    <div class="row">
        <div class="span4">
        <h2>Groups</h2>
         <table class="table table-zebra-striped">
            <tbody>
            <% for (PatientGroup group : groupsPatientIsIn) {
            %>
            <tr>
                <td><%=Util.textAreaValue(group.getGroupName())%></td>
                <td><a href="/corp/patients/detail.jsp?remove=true&id=<%=p.getId()%>&groupId=<%=group.getId()%>"><i class="icon-remove"></i> Remove</a></td>
            </tr>
            <%
            } %>
            </tbody>
        </table>
        <p><a href="/corp/groups/inline/add_patient.jsp?patient=<%=p.getId()%>" class="btn btn-primary fancybox"><i class="icon-plus icon-white"></i> Add To Group</a></p>
    </div>
     
        <div class="span6">
            <h2>Subscription Details</h2>
            
            <p>
            <%List<String> status=lm.getPatientSubscriptoin(p.getEmail());
            	long subId=p.getSubscriptionId();
            	String subName="";
            	String subPeriod="";
            	
            	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy MM dd");
                Date date = new Date();
            	
            	expiredDate=paymentSubscription != null ? paymentSubscription.getExpired() : null;
            	
             if(subId>1 && status!=null && status.size()>0){
            	 SubscriptionManager sm=new SubscriptionManager();
            	 Subscription s=sm.getSubscriptions(p.getSubscriptionId());
            	 DateTime expired=new DateTime(status.get(0));
                 DateTime renewal=new DateTime(expired.plusDays(1));%>               
	            <P><strong>Subscription Plan Name</strong> : <%=s.getSubscription()%>
	            <p><strong>Subscription Period</strong> -	<%=s.getSubscriptionPeriod()%>&nbsp;<%=s.getSubPeriodType()%><br></p>
	            <p><strong>Subscribed On</strong>	:	<%=(paymentSubscription != null && paymentSubscription.getTransactionDate() != null) ? sdf.format(paymentSubscription.getTransactionDate()) : ""%><br></p>
	            <p><strong>Expires On</strong>	:	<%=sdf.format(expiredDate)%><br></p>
	            <p><strong>Renewal Date</strong>	:	<%=sdf.format(renewal.toDate())%><br></p>
	            <p><strong>Subscription Status</strong>	:	<%=(p.getSubscriptionId() > 1) ? "Active" : "InActive"%><br></p> 
	                  	
            <%}
             else if(subId>1 && paymentSubscription != null && paymentSubscription.getExpired()!=null){
            	 
            	 
            	 System.out.println("date exp--"+paymentSubscription.getExpired());
            	 
            	 if(subId==2){
            		 subName="DAYS PLAN";
            		 subPeriod="Free Trial";
            	//	 cal.add(Calendar.DATE, +3);
     	          //   expiredDate = cal.getTime();      	            
     	             renevalDate = expiredDate;
     	             cal.setTime(renevalDate); 
     	             cal.add(Calendar.DATE, 1);
     	             renevalDate = cal.getTime();
            	 }else if(subId==3){
            		 subName="MONTH PLAN";
            		 subPeriod="1 MONTH";
            	//	 cal.add(Calendar.MONTH, +1);
     	        //     expiredDate = cal.getTime();      	            
     	             renevalDate = expiredDate;
     	             cal.setTime(renevalDate); 
     	             cal.add(Calendar.DATE, 1);
     	             renevalDate = cal.getTime();
            	 }else if(subId==4){
            		 subName="MONTH PLAN";
            		 subPeriod="3 MONTHS";
            	//	 cal.add(Calendar.MONTH, +3);
     	     //        expiredDate = cal.getTime();      	            
     	             renevalDate = expiredDate;
     	             cal.setTime(renevalDate); 
     	             cal.add(Calendar.DATE, 1);
     	             renevalDate = cal.getTime();
            	 }else if(subId==5){
            		 subName="MONTH PLAN";
            		 subPeriod="6 MONTHS";
            	//	 cal.add(Calendar.MONTH, +6);
     	       //      expiredDate = cal.getTime();      	            
     	             renevalDate = expiredDate;
     	             cal.setTime(renevalDate); 
     	             cal.add(Calendar.DATE, 1);
     	             renevalDate = cal.getTime();
            	 }
            	 
            	 if(paymentSubscription != null && paymentSubscription.getExpired()!=null)
                 {
                String expiredDateFormat=dateFormat.format(paymentSubscription.getExpired());
                Date  date2 = dateFormat.parse(expiredDateFormat);
                String toDayaDate=dateFormat.format(date);   
                Date  date1 = dateFormat.parse(toDayaDate);
                System.out.println("substract"+(date1.getTime()-date2.getTime()));
                long subDate=date2.getTime()-date1.getTime();
            	 
            	 
            	 
            	 
            	%>               
	            <P><strong>Subscription Plan Name</strong> : <%=subName%>
	            <p><strong>Subscription Period</strong> -	<%=subPeriod%><br></p>
	            <p><strong>Subscribed On</strong>	:	<%=(paymentSubscription != null && paymentSubscription.getTransactionDate() != null) ? sdf.format(paymentSubscription.getTransactionDate()) : ""%><br></p>
	            <p><strong>Expires On</strong>	:	<%=sdf.format(expiredDate)%><br></p>
	            <p><strong>Renewal Date</strong>	:	<%=sdf.format(renevalDate)%><br></p>
	            <p><strong>Subscription Status</strong>	:	<%=(subDate > 0 || subId == 0) ? "Active" : "Inactive"%><br></p> 
	                  	
            <%}}else if(p.getSubscriptionId()==1)
            {%><strong>Subscription Status : None</strong><%}
            else{%><strong>Print Only / No App Access</strong><%}
            %>
            </p>
        </div>
     
    
    </div>
    
   
    <% if (tutorialProgress == UserManager.TUTORIAL_WENT_TO_PATIENT_PAGE){ %>
    <script>
        function explainPage() {
                $('<div />').qtip({
                	//myself
                    // content: {
                       // text:  $('<p />', {html: "From here you can add clients to a Patient Group, view past programs or make a new programs using a template.<br/><br/>For now, click on the one we've already made for you"}),
                      /*  title: 'The Patient Details page' */
                 //   }, 
                    position: {
                        my: 'top right', at: 'bottom center',
                        target: $('#first_program')
                    },
                    show: {
                        ready: true,
                        modal: {
                            on: false,
                            blur: false
                        }
                    },
                    hide: false,
                    style: {
                        width: 500,
                        classes: 'qtip-bootstrap'
                    },
                    events: {
                        hide: function(event, api) {
                            api.destroy();
                        }
                    }
                });
            }
            
            explainPage();
     </script>
     <% } %>
</body>
</html>