<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head><title>Password reset email sent</title></head>
  <body>
  <div class="page-header">
    <h1>Password reset email sent</h1>
</div>
<div class="row">
    <div class="span8 columns">
        An email has been sent to you with instruction for resetting your password.
    </div>
    <div class="span8 columns">
        &nbsp;
    </div>
</div>

  </body>
</html>