<%@ page import="au.com.ourbodycorp.model.Patient" %>
<%@ page import="au.com.ourbodycorp.model.managers.PatientManager" %>
<%@ page import="java.sql.*" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    PatientManager um = new PatientManager();
	Connection conn = Util.getConnection();
	PreparedStatement pst = conn.prepareStatement("select id, email from patient where password = ?");
	pst.setString(1, request.getParameter("key"));
	ResultSet rs = pst.executeQuery();
    if (rs.next()) {
        session.setAttribute("createForPatient1", rs.getLong("id"));
        session.setAttribute("email", rs.getString("email"));
        response.sendRedirect("patient_create_password2.jsp");
        return;
    }
	pst.close();
	conn.close();
%>
<html>
<head><title>Create Password</title></head>
<body>
  <div class="page-header">
    <h1>Create Password not found</h1>
</div>
<div class="row">
    <div class="span8 columns">
        The create password key supplied has not been found. Please contact your fiizio.
    </div>
    <div class="span8 columns">
        &nbsp;
    </div>
</div>
</body>
</html>