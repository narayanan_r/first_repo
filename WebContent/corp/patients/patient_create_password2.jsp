<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.Patient" %>
<%@ page import="au.com.ourbodycorp.model.managers.PatientManager" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.regex.Pattern" %>
<%@ page import="java.util.regex.Matcher" %>
<%@ page import="java.sql.*" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    if (session.getAttribute("createForPatient1") == null) {
        response.sendRedirect("/");
        return;
    }
    HashMap<String,String> errorFields = new HashMap<String, String>();
    String pw1 = request.getParameter("pw1");
    String pw2 = request.getParameter("pw2");
   
    if (pw1 != null && pw2 != null) {
    	  boolean b1=pw1.matches("[a-zA-Z]+");
          boolean b2= pw1.matches("[0-9]+");
          boolean b3= pw1.matches("[^A-Za-z0-9 ]+");
          boolean b4= pw1.matches("[^a-zA-Z]+");
          boolean b5= pw1.matches("[^0-9]+");
          if(b1==true || b2==true || b3==true || b4==true || b5==true){
       	   errorFields.put("pw1", "Your new password must be AlphaNumeric");
          }else if (pw1.length() < 5) {
            errorFields.put("pw1", "Your new password must be at least 5 characters");
            errorFields.put("pw2", "");
        } else if (!pw1.equals(pw2)) {
            errorFields.put("pw1", "New passwords do not match");
            errorFields.put("pw2", "");
        } else {
        	PatientManager um = new PatientManager();
            Connection con1 = Util.getConnection();
		    PreparedStatement pst1 = con1.prepareStatement("update patient set password = ? where id = ?");
            pst1.setString(1, Util.encryptPassword(pw1));
            pst1.setLong(2, Long.parseLong(session.getAttribute("createForPatient1").toString()));
            pst1.executeUpdate();
            pst1.close();
            con1.close();
            um.logout(session, response);
            response.sendRedirect("patient_create_password3.jsp");
            return;
        }
    }
%>
<html>
<head><title>Create Password</title>
</head>
<body>

<div class="page-header">
    <h1>Create Password</h1>
</div>

<div class="row">
    <div class="span9">
        <%@ include file="/WEB-INF/includes/formError.jsp" %>
        <form action="patient_create_password2.jsp" method="post" class="form form-horizontal">
            <fieldset>
                <legend>New Password for <%=session.getAttribute("email").toString()%></legend>
          <%=HTML.passwordInput("pw1", "Password", "25", "25", null, null, errorFields)%>
          <%=HTML.passwordInput("pw2", "Confirm Password", "25", "25", null, null, errorFields)%>
                </fieldset>
          <%=HTML.submitButton("Submit")%>
</form>
    </div>
    <div class="span4 columns">
        &nbsp;
    </div>
</div>
</body>
</html>