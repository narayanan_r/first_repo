<%@page import="au.com.ourbodycorp.model.managers.UserManager"%>
<%@ page import="au.com.ourbodycorp.Config" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%@ page import="au.com.ourbodycorp.model.managers.AttachmentManager" %>
<%@ page import="au.com.ourbodycorp.model.managers.CorporationManager" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="au.com.ourbodycorp.model.managers.MiniNewsManager" %>
<%@page import="au.com.ourbodycorp.ImportCliniko"%>
<%@ page import="org.joda.time.DateTime" %>
<%@ page import="org.joda.time.Days" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.List" %>
<%@page import="com.eway.process.SOAPClientJava"%>
<%@page import="javax.xml.namespace.QName"%>
<%@page import="javax.xml.soap.*"%>
<%@page import="javax.xml.transform.*"%>
<%@page import="javax.xml.transform.stream.*"%>
<%@ page import="java.sql.*" %>
<%@ page import="java.util.*" %>
<%@ page import="java.util.Date" %>
<%@ page import="org.apache.http.client.ClientProtocolException" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
	long StartMilliSec = (long)System.currentTimeMillis();
    System.out.println("Staring from corp/index.jsp>>>  "+StartMilliSec);
    CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");

    if (cu == null) {
        response.sendRedirect(Util.loginUrl(Constants.LoginReason.login, request));
        return;
    }
    Corporation corp = cu.getCorporation();
    User u = cu.getUser();
    /*  try{
    	Util.allActivitiesLogs("Login", u.getId(), corp.getName(), "Dashboard");
    }catch(Exception e){
    	
    }  */
    String query = (request.getParameter("query") != null && request.getParameter("query").trim().length() > 0) ? request.getParameter("query") : null;

    CorporationManager cm = new CorporationManager();
    LibraryManager lm = new LibraryManager();
    
     String type="Login";
    String activity_page="corp/index.jsp";
	String corp_id=String.valueOf(corp.getId());
	String corp_name=corp.getName();
	String corp_loc=corp.getSuburb();
	String user_id=String.valueOf(u.getId());
	String user_name=u.getFullName();
	String invitor_id="---";
	String invitor_name="---";
	String flag="view";
	String from="---";
	String to="---";
	String desc="Dashboard By User "+u.getFullName(); 
     try
    {		
     Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
    }
    catch(Exception e)
    {
    } 

    
    //Recurring Transaction Details
    
    /* SOAPClientJava soap = new SOAPClientJava();
    
    try {
 		 Connection conn3 = Util.getConnection();
         PreparedStatement pst3 = conn3.prepareStatement("select id,rebill_customer_id,rebill_id,order_date,canceled_date from cancel_subscription where corp_id = ? and canceled_date is null");
         pst3.setLong(1, corp.getId());
         ResultSet rs3 = pst3.executeQuery();
         if(rs3.next()){
         	session.setAttribute("CustomerId", rs3.getLong("rebill_customer_id"));
         	session.setAttribute("RebillId", rs3.getLong("rebill_id"));
	   	 }else{
	 		session.setAttribute("CustomerId", null);
	 		session.setAttribute("RebillId", null);
	 	 } pst3.close();
	 	
	 	String subtype = null;
	 	if(corp.getSubType().equals("unlimited")){
	 		subtype = "large";
	 	}else{
	 		subtype = corp.getSubType();
	 	}
	 	
	 	PreparedStatement pst4 = conn3.prepareStatement("select invoice_number from plan_price where lower(plan_name) like '%"+subtype+"%'");
        ResultSet rs4 = pst4.executeQuery();
        if(rs4.next()){
        	session.setAttribute("subtype_invoice_number", rs4.getString("invoice_number"));
	   	}else{
	 		session.setAttribute("subtype_invoice_number", null);
	 	} pst4.close();
 	
 	
	            		    String cId = session.getAttribute("CustomerId").toString();
		                    String rId = session.getAttribute("RebillId").toString();
		                    String invNo = session.getAttribute("subtype_invoice_number").toString();
		                    
		                    // Create SOAP Connection
		                    SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
		                    SOAPConnection soapConnection = soapConnectionFactory.createConnection();

		                    // Send SOAP Message to SOAP Server
		                    String url = "https://www.eway.com.au/gateway/rebill/manageRebill.asmx";
//		                    String url = "https://www.eway.com.au/gateway/rebill/test/manageRebill_test.asmx";
		                    	
		                    	Date date = new Date();
		                		Calendar cal = Calendar.getInstance();
		                		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
		                		cal.set(Calendar.DAY_OF_MONTH, Calendar.getInstance().getActualMinimum(Calendar.DAY_OF_MONTH));
		                		String strDate = sdf1.format(cal.getTime());
		                		Calendar cal1 = Calendar.getInstance();
		                		String currentDate = sdf1.format(cal1.getTime());
		                		
		                    	SOAPMessage soapResponseRecurringTransactionDetails = soapConnection.call(soap.createSOAPRecurringTransactionDetailsRequest(cId, rId, strDate, currentDate, "Successful"), url);

		                        // Process the SOAP Response
		                        soap.printSOAPResponse(soapResponseRecurringTransactionDetails);
		                        
		                        String Status = soapResponseRecurringTransactionDetails.getSOAPBody().getElementsByTagName("Status").item(0).getFirstChild().getNodeValue();
		                        String TotalAmt = soapResponseRecurringTransactionDetails.getSOAPBody().getElementsByTagName("Amount").item(0).getFirstChild().getNodeValue();
		                        String TransnId = soapResponseRecurringTransactionDetails.getSOAPBody().getElementsByTagName("TransactionNumber").item(0).getFirstChild().getNodeValue();
		                        String TransactionDate = soapResponseRecurringTransactionDetails.getSOAPBody().getElementsByTagName("TransactionDate").item(0).getFirstChild().getNodeValue();
		                        
		                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		                        Date parsed = format.parse(TransactionDate);
		                        java.sql.Date TnsDate = new java.sql.Date(parsed.getTime());
		                        
			                    System.out.print("\n Status "+Status);
			                    
			                    
			                   double totamt = Double.parseDouble(TotalAmt);
			                   boolean transStatus = false;
			                   if(Status.equals("Successful")){
			                	   transStatus = true;
			                   }
			         		   double tamt = (totamt / 100);
			         		   java.math.BigDecimal totalamt = new java.math.BigDecimal(tamt);
			         		   
			         		  PaymentHistory ph;
			         		 PreparedStatement pst5 = conn3.prepareStatement("select trans_id from payment_history where trans_id = ?");
			         		 pst5.setLong(1, Long.parseLong(TransnId));
			                 ResultSet rs5 = pst5.executeQuery();
			                 if(rs5.next()){

			                 }else{
			         	   		ph = new PaymentHistory();
				  	        	ph.setUserid(u.getId());
				  	        	ph.setCorp_id(corp.getId());
				  	        	ph.setCompany(corp.getCompanyId());
				  	        	ph.setAccess_code(cId);
				  	        	ph.setResponse(rId);
				  	        	ph.setInvoice_number(invNo);
				  	        	ph.setTotal_amount(totalamt);
				  	        	ph.setTrans_id(Long.parseLong(TransnId));
				  	        	ph.setTrans_status(transStatus);
				  	        	ph.setCreated(TnsDate);
				  	        	lm.savePaymentHistoryDetails(ph);
			         	 	 } pst5.close(); conn3.close();
				  	        	
		                    soapConnection.close(); 
		                } catch (Exception e) {
// 		                    System.err.println("Error occurred while sending SOAP Request to Server");
// 		                    e.printStackTrace();
						try{
							String cId = session.getAttribute("CustomerId").toString();
		                    String rId = session.getAttribute("RebillId").toString();
		                    String invNo = session.getAttribute("subtype_invoice_number").toString();
		                    
		                    // Create SOAP Connection
		                    SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
		                    SOAPConnection soapConnection = soapConnectionFactory.createConnection();

		                    // Send SOAP Message to SOAP Server
		                    String url = "https://www.eway.com.au/gateway/rebill/manageRebill.asmx";
//		                    String url = "https://www.eway.com.au/gateway/rebill/test/manageRebill_test.asmx";
		                    	
		                    	Date date = new Date();
		                		Calendar cal = Calendar.getInstance();
		                		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
		                		cal.set(Calendar.DAY_OF_MONTH, Calendar.getInstance().getActualMinimum(Calendar.DAY_OF_MONTH));
		                		String strDate = sdf1.format(cal.getTime());
		                		Calendar cal1 = Calendar.getInstance();
		                		String currentDate = sdf1.format(cal1.getTime());
		                		
		                    SOAPMessage soapResponseRecurringTransactionDetails = soapConnection.call(soap.createSOAPRecurringTransactionDetailsRequest(cId, rId, strDate, currentDate, "Failed"), url);

	                        // Process the SOAP Response
	                        soap.printSOAPResponse(soapResponseRecurringTransactionDetails);
	                        
	                        String Status = soapResponseRecurringTransactionDetails.getSOAPBody().getElementsByTagName("Status").item(0).getFirstChild().getNodeValue();
	                        String TotalAmt = soapResponseRecurringTransactionDetails.getSOAPBody().getElementsByTagName("Amount").item(0).getFirstChild().getNodeValue();
	                        String TransnId = soapResponseRecurringTransactionDetails.getSOAPBody().getElementsByTagName("TransactionNumber").item(0).getFirstChild().getNodeValue();
	                        String TransactionDate = soapResponseRecurringTransactionDetails.getSOAPBody().getElementsByTagName("TransactionDate").item(0).getFirstChild().getNodeValue();
	                        
	                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
	                        Date parsed = format.parse(TransactionDate);
	                        java.sql.Date TnsDate = new java.sql.Date(parsed.getTime());
	                        
		                    System.out.print("\n Status "+Status);
		                    
		                    
		                   double totamt = Double.parseDouble(TotalAmt);
		                   boolean transStatus = false;
		                   if(Status.equals("Successful")){
		                	   transStatus = true;
		                   }
		         		   double tamt = (totamt / 100);
		         		   java.math.BigDecimal totalamt = new java.math.BigDecimal(tamt);
		         		   
		         		  PaymentHistory ph;
		         		 Connection conn4 = Util.getConnection();
		         		 PreparedStatement pst6 = conn4.prepareStatement("select trans_id from payment_history where trans_id = ?");
		         		 pst6.setLong(1, Long.parseLong(TransnId));
		                 ResultSet rs6 = pst6.executeQuery();
		                 if(rs6.next()){

		                 }else{
		         	   		ph = new PaymentHistory();
			  	        	ph.setUserid(u.getId());
			  	        	ph.setCorp_id(corp.getId());
			  	        	ph.setCompany(corp.getCompanyId());
			  	        	ph.setAccess_code(cId);
			  	        	ph.setResponse(rId);
			  	        	ph.setInvoice_number(invNo);
			  	        	ph.setTotal_amount(totalamt);
			  	        	ph.setTrans_id(Long.parseLong(TransnId));
			  	        	ph.setTrans_status(transStatus);
			  	        	ph.setCreated(TnsDate);
			  	        	lm.savePaymentHistoryDetails(ph);
		         	 	 } pst6.close(); conn4.close();
		         	 	soapConnection.close(); 
						}
						catch(Exception e1)
						{
							
						}
		                }*/
    
    //End 
    
    int trialDaysLeft = cm.trialDaysLeft(corp.getId());
    if(trialDaysLeft < 0){
    	 if (corp.getLocked() != null || corp.isExpiredTrial()) {
    		 response.sendRedirect("/corp/subscription/");
    		 return;
	     }
	     else if(cm.hasTrialExpired(corp.getId())){
	    	 response.sendRedirect("/corp/subscription/");
	    	 return;
	     }
    }
    
    Connection conn = Util.getConnection();
    
    /* if(corp.getCliniko_apikey() != null){
    try{
		new ImportCliniko().getAndParseJSON(new ImportCliniko().getDetailsFromCliniko(corp.getCliniko_apikey()),corp.getId(),u.getId());
	} catch (ClientProtocolException e){
// 			e.printStackTrace(); 
       	 PreparedStatement pst2 = conn.prepareStatement("update corporation set cliniko_apikey=null where id = ?");
       	 pst2.setLong(1, corp.getId());
       	 pst2.executeUpdate();
       	 pst2.close();
	} catch (Exception ee){
//			e.printStackTrace(); 
	}
    } */
    
    int programsThisMonth = lm.countOfProgramsCreatedThisMonth(corp.getId());
    int programsLastMonth = lm.countOfProgramsCreatedLastMonth(corp.getId());

    int patientsThisMonth = lm.countOfPatientsCreatedThisMonth(corp.getId());
    int patientsLastMonth = lm.countOfPatientsCreatedLastMonth(corp.getId());

    List<Patient> patients = lm.getPatients(corp.getId(), 0, 5, "created desc");

    MiniNewsManager mn = new MiniNewsManager();
    List<MiniNews> news = mn.listNews(corp.getId(), 5, 0, "published");

    SimpleDateFormat sdf = new SimpleDateFormat("d MMM HH:mm");
    sdf.setTimeZone(corp.getTimeZone());
    
    UserManager userManager = new UserManager();
    boolean followingTutorial = Boolean.parseBoolean(Util.notNull(request.getParameter("tutorial"), "false"));
    if (followingTutorial){
        userManager.setTutorialProgressForUser(u.getId(), UserManager.TUTORIAL_FINISHED, false);
    }
    
    int tutorialProgress = userManager.getTutorialProgressForUser(u.getId());
%>
<html>
    <head><title><%=corp.getName()%></title>
    
    <style> 
/* Chrome, Safari, Opera */

#div_animate { 
    position: relative;
    -webkit-animation: mymove 0.5s; /* Chrome, Safari, Opera */
    animation: mymove 0.5s;
    -webkit-animation-timing-function: ease;
    }


/* Standard syntax */

#div_animate {
    position: relative;
    -webkit-animation: mymove 0.5s; /* Chrome, Safari, Opera */
    animation: mymove 0.5s;
    animation-timing-function: ease;
    }


/* Chrome, Safari, Opera */
@-webkit-keyframes mymove {
    from {right: -500px;}
    to {right: 0px;}
}

/* Standard syntax */
@keyframes mymove {
    from {right: -500px;}
    to {right: 0px;}
}

#break{
	background: rgba(0, 0, 0, 0) url("/images/app/bg.jpg") repeat scroll 0 0; padding: 0px 0px 5px; margin: 20px -20px 15px;
}
</style>
</head>
    <body>
        <div class="page-header">
            <h1><%=corp.getName()%> <small>Fiizio App</small></h1>
        </div>
        <%
            if (corp.getLogoId() < 1 && cu.hasRole(CorporationRole.Role.Admin)) {
        %>
        <div class="hero-unit">
            <h1>Welcome to Fiizio App</h1>
            <p style="font-size: 15px; margin-top: 10px;">To further customize the app, add your logo here. You can skip this until later but Fiizio App looks best with your logo on it!</p>
            <p>
                <a href="/corp/admin" class="btn btn-primary btn-large">Click to Add Your Logo</a>
            </p>
        </div>
        <%
            }
        %>
        <div class="row">
            <div class="span3 well">
                <h3>Patients</h3>
                <table class="table table-zebra-striped">
                    <tbody>
                        <tr>
                            <td colspan="2" style="text-align: center;">
                                <a href="/corp/patients/edit.jsp" class="btn btn-primary">Add New Patient</a>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: center;">
                                <form class="form-search" style="margin-bottom: 0px" method="get" action="/corp/patients/">
                                    <input type="text" class="input-medium search-query" name="query" value="<%=Util.formValue(query)%>">
                                    <button type="submit" class="btn">Search</button>
                                </form>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: center;"><strong>Recently Added</strong></td>
                        </tr>
                        <%
                            if (patients != null && patients.size() > 0) {
                                int numAdded = 0;
                                for (Patient p : patients) {%>
                        <tr>
                            <td <%=(numAdded == 0)? "id=first_patient" : ""%> ><a href="/corp/patients/detail.jsp?id=<%=p.getId()%><%=(tutorialProgress == UserManager.TUTORIAL_NOT_DONE)? "&tutorial=true" : "" %>"><%=p.getFirstName()%> <%=p.getLastName()%></a></td>
                            <td><%=sdf.format(p.getCreated())%></td>
                        </tr>
                            <%      numAdded++;
                                }
                        }
                        else {
                        %>
                        <tr>
                            <td>None</td>
                            <td></td>
                        </tr>
                        <% }%>
                    </tbody>
                </table>
            </div>
            <div class="span3 well">
                <h3>Recent Statistics</h3>
                <table class="table table-zebra-striped">
                    <tbody>
                        <tr>
                            <td colspan="2" style="text-align: center;"><strong>This Month</strong></td>
                        </tr>
                        <tr>
                            <td>New Patients:</td>
                            <td><%=patientsThisMonth%></td>
                        </tr>
                        <tr>
                            <td>New Programs:</td>
                            <td><%=programsThisMonth%></td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: center;"><strong>Last Month</strong></td>
                        </tr>
                        <tr>
                            <td>New Patients:</td>
                            <td><%=patientsLastMonth%></td>
                        </tr>
                        <tr>
                            <td>New Programs:</td>
                            <td><%=programsLastMonth%></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <%-- <%if(corp.getCliniko_apikey() == null){ %>
            <div id="div_animate" class="span3 well">
                <h3>Notifications</h3>
                <p>Cliniko integration now available. <a href="/corp/integrations/">Click here for setup.</a> <!-- <img width="30" border="0" height="14" src="/images/new.gif"> --></p>
            </div>
            <%}else{ 
            	 int count = 0;
            	 int countupdated = 0;
            	 PreparedStatement pst = conn.prepareStatement("select * from patient where reference = 'Cliniko' and corp_id = ? and new_patient = 'New_Patient'");
            	 pst.setLong(1, corp.getId());
            	 ResultSet rs = pst.executeQuery();
            	 while(rs.next()){ 
            		 count = count + 1;
            	 }
            	 pst.close();
            	 
            	 PreparedStatement pst1 = conn.prepareStatement("select * from patient where reference = 'Cliniko' and corp_id = ? and new_patient = 'Updated_Patient'");
            	 pst1.setLong(1, corp.getId());
            	 ResultSet rs1 = pst1.executeQuery();
            	 while(rs1.next()){ 
            		 countupdated = countupdated + 1;
            	 }
            	 pst1.close();
            %>
            <%if(count != 0 || countupdated != 0){ %>
            <div id="div_animate" class="span3 well">
                <h3>Notifications</h3>
                <%if(count != 0){ %>
                <p><a href="/corp/patients/"><%=count%> <%if(count > 1){ %>New Patients<%}else{ %>New Patient<%} %></a> from cliniko. <!-- <img width="30" border="0" height="14" src="/images/new.gif"> --></p>
                <%} %>
                <%if(countupdated != 0){ %>
                <p><a href="/corp/patients/"><%=countupdated%> <%if(countupdated > 1){ %>Patients<%}else{ %>Patient<%} %></a> details updated from cliniko. <!-- <img width="30" border="0" height="14" src="/images/new.gif"> --></p>
           		<%} %>
            </div>
            <%}} %> --%>
            <div class="span3 well">
                <h3>Latest News</h3>
                <table class="table table-zebra-striped">
                    <tbody>
                        <%
                            for (MiniNews p : news) {
                        %>
                        <tr>
                        <td>
                        <% if (Config.getString("https").equals("true")){ %>
<%--                         	<a href="https://<%=corp.getSlug()%>.<%=Config.getString("baseDomain")%>/news.jsp?id=<%=p.getId()%>" class="fancybox"><%=p.getHeadline()%></a> --%>
                        	<a href="https://<%=corp.getSlug()%>.<%=Config.getString("baseDomain")%>/news.jsp?id=<%=p.getId()%>" class="fancybox"><%=p.getHeadline()%></a>
		                <%}else{%>
<%-- 		                	<a href="<%=corp.getSlug()%>.<%=Config.getString("baseDomain")%>/news.jsp?id=<%=p.getId()%>" class="fancybox"><%=p.getHeadline()%></a>
 --%>
		                	<a href="/news.jsp?id=<%=p.getId()%>" class="fancybox"><%=p.getHeadline()%></a>
		                <% }%>
                            </td>
                            <td><a href="/corp/news/edit.jsp?id=<%=p.getId()%>"><i class="icon-edit"></i></a></td>
                        </tr>
                        <%
                            }
                        %>
                    </tbody>
                </table>
                
               <%-- <p id="break">&nbsp;</p>
                <h3>SMS</h3>
                <p>You have <%=corp.getSmsCount()%> SMS remaining. <a href="/corp/subscription/pricing_sms.jsp">Need more credits? Click here</a>.</p>
				<% if (trialDaysLeft > 0){ %>
            		<p id="break">&nbsp;</p>
	                <h3>Trial</h3>
	                <p>You have <%=trialDaysLeft%> <%=(trialDaysLeft > 1)? "days" : "day"%> left of your trial. <a href="/corp/subscription/pricing.jsp">Subscribe here</a>.</p>
	            <% } else if (trialDaysLeft == 0){ %>
	            	<p id="break">&nbsp;</p>
	                <h3>Trial - Expiring Today!</h3>
	                <p>This is the last day of your trial period. We'd love to keep working with you, please <a href="/corp/subscription/pricing.jsp">Subscribe here</a>.</p>
	            <% } %> --%> 
            </div>
        </div>
<%conn.close(); %>
        <style>
            .qtip-content { text-align: left; }
            .qtip { max-width: 500px;}
            .qtip-titlebar { text-align: center;background-color: #902300 !important;
			    background-repeat: repeat-x;
			    border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25); 
			    color: #ffffff;font-weight: 700 !important;}
            #next_btn {
                margin-left: 200px;
                display:inline !important;
            }
            #yes_btn {
                margin-left: 45px;
                display:inline !important;
            }
            #no_btn{
                 display:inline !important;
            }
        </style>
        <script>
        <% if (cu.hasRole(CorporationRole.Role.Admin)) {%>
        	var idArr = ['#dashboard_side', '#members_side', '#patients_side', '#groups_side', '#templates_side', '#news_side', '#exercises_side', '#articles_side'
//                		, '#integrations_side'
               		, '#settings_side', '#reports_side', '#subscription_side'
                 ];
        	
        	var helpTextArr = ["From here you can add new clients, search existing clients and see what News items are currently published to your client base.",
        	                   "Invite the rest of your physiotherapy team to use Fiizio App from here.",
        	                   "This is a searchable database of all clients that you have connected with using Fiizio App.",
        	                   "Are you involved with sports teams or workplace groups? Collect their name and phone number (a signup sheet) and using Patient Groups you can provide them all with an injury prevention program specific to their activity.<br/><br/>It's immediately useful to them, they'll always have it with them and fully branded to your practice. You've just converted dozens of prospective clients to engage further with your brand, with a few clicks and at no cost to you. Well done!",
        	                   "Templates create an instant backbone to your exercise program which you can simply adjust for each individual. You can easily send a highly professional, fully customised program to a client's phone in less than 60 seconds using Fiizio App Templates.",
        	                   "All connected clients will receive your News articles in the MyInfo tab on their App. It's perfect for practice newsletters and promotions.",
        	                   "You can view and edit the supplied library or add your own specials. Just use .jpg files for photos and .m4v files for videos, the standard formats for smartphones. Only you and your team will have access to the exercises you add.",
        	                   "Similar to handing out an info booklet, these modules can be attached to programs to improve your client education for things like diagnosis, self management concepts and general health literacy. They arrive in the MyInfo Tab on the client's phone.<br/><br/>Be sure to add your own short modules as they are a powerful way to reinforce your in-clinic education.",
//         	                   "Integrate your Fiizio App account with most popular services.",
							   "If you need to change your phone number, opening hours or other details, update them here.",
							   "Use the practice statistics report to learn how well your team utilise the system and where improvements can be made.",
        	                   "To upgrade your Fiizio App here."];
        	                   
        	               var titleTextArr = ["Welcome to your Dashboard",
        	                   "Team Members",
        	                   "Patients",
        	                   "Groups are a great way to spread your brand to prospective clients",
        	                   "Templates make things quick...really quick",
        	                   "News articles go to everyone",
        	                   "My Exercises is your exercise library",
        	                   "My Articles are your information modules",
// 							   "My Integrations",
        	                   "Settings",
        	                   "Learn who prescribes best",
        	                   "Fiizio Subscription"];
        <% }else{ %>
            var idArr = ['#dashboard_side', '#patients_side', '#groups_side', '#templates_side', '#exercises_side', '#articles_side',
//                          '#integrations_side', 
                         '#subscription_side'
            ];
            
            var helpTextArr = ["From here you can add new clients, search existing clients and see what News items are currently published to your client base.",
                               "This is a searchable database of all clients that you have connected with using Fiizio App.",
                               "Are you involved with sports teams or workplace groups? Collect their name and phone number (a signup sheet) and using Patient Groups you can provide them all with an injury prevention program specific to their activity.<br/><br/>It's immediately useful to them, they'll always have it with them and fully branded to your practice. You've just converted dozens of prospective clients to engage further with your brand, with a few clicks and at no cost to you. Well done!",
                               "Templates create an instant backbone to your exercise program which you can simply adjust for each individual. You can easily send a highly professional, fully customised program to a client's phone in less than 60 seconds using Fiizio App Templates.",
                               "You can view and edit the supplied library or add your own specials. Just use .jpg files for photos and .m4v files for videos, the standard formats for smartphones. Only you and your team will have access to the exercises you add.",
                               "Similar to handing out an info booklet, these modules can be attached to programs to improve your client education for things like diagnosis, self management concepts and general health literacy. They arrive in the MyInfo Tab on the client's phone.<br/><br/>Be sure to add your own short modules as they are a powerful way to reinforce your in-clinic education.",
//                                "Integrate your Fiizio App account with most popular services.",
							   "Use the practice statistics report to learn how well your team utilise the system and where improvements can be made.",
							   "To upgrade your Fiizio App here."];
                               
                           var titleTextArr = ["Welcome to your Dashboard",
                               "Patients",
                               "Groups are a great way to spread your brand to prospective clients",
                               "Templates make things quick...really quick",
                               "My Exercises is your exercise library",
                               "My Articles are your information modules",
// 							   "My Integrations",
							   "Learn who prescribes best",
                               "Fiizio Subscription"];
         <% } %>
    
            var upToIndex = 0;
            function dialogue(content, title) {
                $('<div />').qtip({
                    content: {
                        text: content,
                        title: title
                    },
                    position: {
                        my: 'center', at: 'center',
                        target: $(window)
                    },
                    show: {
                        ready: true,
                        modal: {
                            on: true,
                            blur: false
                        }
                    },
                    hide: false,
                    style: 'qtip-bootstrap',
                    events: {
                        render: function(event, api) {
                            $('button', api.elements.content).click(function(e) {
                                api.hide(e);
                                if (e.currentTarget.id == 'yes_btn') {
                                    showNextTooltip();
                                }
                                else {
                                    showSampleProgram();
                                }
                            });
                        },
                        hide: function(event, api) {
                            api.destroy();
                        }
                    }
                });
            }

            function showNextTooltip() {
                if (upToIndex >= idArr.length){
                    var message = $('<p />', {html: "STOP. Before beginning the next section, please download the free Fiizio App to your personal smartphone from <a href='#' target='_blank'>iTunes App Store</a> (iPhone) or <a href='#' target='_blank'>Google Play</a> (Android). Once you have downloaded the free App, click okay below."});
                    var ok = $('<button id="next_btn" class="btn btn-primary" style="display: inline !important;" >Okay</button>');
                    dialogue(message.add(ok), 'Grab The Fiizio App');
                    upToIndex++;
                    return;
                }
                else if (upToIndex > idArr.length) {
                    showSampleProgram();
                    return;
                }
                var elementId = idArr[upToIndex];
                var helpText = helpTextArr[upToIndex];
                var titleText = titleTextArr[upToIndex];
                upToIndex++;

                var btnText =  (upToIndex >= idArr.length)? "Done" : "Okay";
                $(elementId).addClass('active');
                $('<div />').qtip({
                    content: {
                        text:  $('<p />', {html: helpText}).add($('<button id="next_btn" class="btn btn-primary">'+btnText+'</button>')),
                        title: titleText
                    },
                    position: {
                        my: 'center left', at: 'center right',
                        target: $(elementId)
                    },
                    show: {
                        ready: true,
                        modal: {
                            on: false,
                            blur: false
                        }
                    },
                    hide: false,
                    style: {
                        width: 500,
                        classes: 'qtip-bootstrap'
                    },
                    events: {
                        render: function(e, api) {
                            $('button', api.elements.content).click(function(e) {
                                api.hide(e);
                                if (e.currentTarget.id == 'next_btn') {
                                    showNextTooltip();
                                     $(elementId).removeClass('active');
                                }
                            });
                        },
                        hide: function(event, api) {
                            api.destroy();
                        }
                    }
                });
            }
            
            function showSampleProgram() {
                $('<div />').qtip({
                    content: {
                        text:  'To see how Fiizio App works, click on your name under Recently Added',
                        title: 'We have prepared a demo program for you'
                    },
                    position: {
                        my: 'center left', at: 'center right',
                        target: $('#first_patient')
                    },
                    show: {
                        ready: true,
                        modal: {
                            on: false,
                            blur: false
                        }
                    },
                    hide: false,
                    style: {
                        width: 500,
                        classes: 'qtip-bootstrap'
                    },
                    events: {
                        hide: function(event, api) {
                            api.destroy();
                        }
                    }
                });
            }

            <% 
            if (tutorialProgress == UserManager.TUTORIAL_NOT_DONE) {
            %>
                var message = $('<p />', {html: 'Please download the Fiizio App to your smartphone now so you can watch the magic unfold.<br/><br/>You have 2 guided options to explore the system. Your goal is to send a custom prescription direct to your phone.'});
                var ok = $('<button id="yes_btn" class="btn btn-primary" style="display: inline !important;" >Take the full tour (6 min)</button>');
                var cancel = $('<button id="no_btn" class="btn" style="margin-left:10px; display: inline !important;" >Get straight to the fun bit (3 min)</button>');
                dialogue(message.add(ok).add(cancel), 'Welcome to your Dashboard');
            <% } %>
        </script>
        <%
        long EndMilliSec = (long)System.currentTimeMillis();
        long TotalMillySec = EndMilliSec - StartMilliSec;
        System.out.println("Ending  from corp/index.jsp>>>  "+TotalMillySec+"ms"); %>
    </body>
</html>