<%@page import="java.util.List"%>
<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%@ page import="au.com.ourbodycorp.model.managers.CorporationManager" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.HashMap" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");

    if (cu == null) {
        response.sendRedirect(Util.loginUrl(Constants.LoginReason.login, request));
        return;
    }
    User u = cu.getUser();
    Corporation corp = cu.getCorporation();
    /* try{
    	Util.allActivitiesLogs("Patient Groups", u.getId(), corp.getName(), "Add Patient Groups");
    }catch(Exception e){
    	
    } */
    LibraryManager lm = new LibraryManager();

    long id = Long.parseLong(Util.notNull(request.getParameter("id"), "0"));
    String title = "Add";

    PatientGroup pg = null;
    
    if (id > 0) {
        title = "Edit";
        pg = lm.getPatientGroup(id);
        if (pg == null || pg.getCorporationId()!= corp.getId()) {
            response.sendError(404, "Patient Group not found or not part of this practice");
            return;
        }
    } 

    HashMap<String, String> errorFields = new HashMap<String, String>();

    String groupName = Util.notNull(request.getParameter("groupName"));
    if (request.getMethod().equalsIgnoreCase("post")) {
        if (groupName.trim().length() == 0) {
            errorFields.put("groupName", "please enter a group name");
        }

        if (errorFields.size() == 0) {
            if (pg == null) {
                pg = new PatientGroup();
                pg.setCorporationId(corp.getId());
            }
            pg.setGroupName(groupName);

            lm.save(pg);
            String type="Patient Groups";
            String activity_page="corp/groups/edit.jsp";
        	String corp_id=String.valueOf(corp.getId());
        	String corp_name=corp.getName();
        	String corp_loc=corp.getSuburb();
        	String user_id=String.valueOf(u.getId());
        	String user_name=u.getFullName();
        	String invitor_id=String.valueOf(pg.getId());
        	String invitor_name="---";
        	String flag="add";
        	String from="";
        	String to="";
        	String desc="Create Patient Group "+groupName+" By User";
        	 try{
        	    	//Util.allActivitiesLogs("Patient Groups", u.getId(), corp.getName(), "Add Patient Groups");
         		Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
         	}catch(Exception ee){
         		
         	}
            System.out.println(desc);
            response.sendRedirect("/corp/groups/detail.jsp?id="+pg.getId());
            return;
        }
    }

    String[][] breadcrumbs = {{"/corp/", corp.getName()}, {"/corp/groups/", "Patient Groups"}, {null, title + " Patient Groups"}};


%>
<html>
<head><title><%=title%> Patient</title></head>
<body>
<div class="page-header">
    <h1><%=title%> Patient</h1>
</div>
<%@include file="/WEB-INF/includes/breadcrumbs.jsp"%>

<div class="row">
    <div class="span9">
         <%@ include file="/WEB-INF/includes/formError.jsp" %>
        <form action="edit.jsp" method="post" class="form-horizontal">
            <input type="hidden" name="id" value="<%=id%>">
            <%=HTML.textInput("groupName", "Group Name", "30", "30", (pg != null)? pg.getGroupName() : "", null, errorFields)%>
            <%=HTML.submitButton("Save")%>
        </form>
    </div>
</div>

</body>
</html>