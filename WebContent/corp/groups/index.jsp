<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="java.util.LinkedList" %>
<%@ page import="java.util.List" %>
<%
    CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");

    if (cu == null) {
        response.sendRedirect(Util.loginUrl(Constants.LoginReason.login, request));
        return;
    }
    User u = cu.getUser();
    Corporation corp = cu.getCorporation();
   /*  try{
    	Util.allActivitiesLogs("Patient Groups", u.getId(), corp.getName(), "View Patient Groups");
    }catch(Exception e){
    	
    } */
    String type="Patient Groups";
    String activity_page="corp/groups/index.jsp";
	String corp_id=String.valueOf(corp.getId());
	String corp_name=corp.getName();
	String corp_loc=corp.getSuburb();
	String user_id=String.valueOf(u.getId());
	String user_name=u.getFullName();
	String invitor_id="";
	String invitor_name="---";
	String flag="view";
    String from="---";
    String to="---";
	String desc="View Patient Groups By User";
     try{
    	//Util.allActivitiesLogs("Patient Groups", u.getId(), corp.getName(), "View Patient Groups");
    	Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
    }catch(Exception ee){
    	
    } 
    LibraryManager lm = new LibraryManager();

    int currentPage = 1;
    try {
        currentPage = Integer.parseInt(request.getParameter("page"));
    } catch (NumberFormatException nfe) {
        // ignore
    }


    List<PatientGroup> patientGroups = lm.getPatientGroups(corp.getId());

    String[][] breadcrumbs = {{"/corp/", corp.getName()}, {null, "Patient Groups"}};
%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head><title>Patient Groups</title></head>
<body>
<div class="page-header">
    <h1>Patient Groups</h1>
</div>
<%@include file="/WEB-INF/includes/breadcrumbs.jsp"%>

<div class="row">
    <div class="span2">
        <a href="edit.jsp" class="btn btn-primary"><i class="icon-plus icon-white"></i> Add Group</a>
    </div>
</div>
<div class="row">
    <div class="span9">
        <table class="table table-zebra-striped">
            <thead>
            <tr>
                <th>Group Name</th>
                <th>Member Count</th>
            </tr>
            </thead>
            <tbody>
            <%
                for (PatientGroup pg : patientGroups) {
            %>
            <tr>
                <td><a href="detail.jsp?id=<%=pg.getId()%>"><%=Util.textAreaValue(pg.getGroupName())%></a></td>
                <td><%=lm.countOfPatientsInGroup(pg.getId())%></td>
            </tr>
            <%
                }
            %>
            </tbody>
        </table>
    </div>
</div>
</body>
</html>