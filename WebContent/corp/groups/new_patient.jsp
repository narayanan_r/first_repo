<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%@ page import="au.com.ourbodycorp.model.managers.CorporationManager" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.io.BufferedReader" %>
<%@ page import="java.io.IOException" %>
<%@ page import="java.io.InputStreamReader" %>
<%@ page import="java.sql.*"%>
<%@ page import="java.net.URL" %>
<%@ page import="java.net.URLConnection" %>
<%@ page import="org.json.JSONObject" %>
<%@ page import="sun.net.www.protocol.http.HttpURLConnection" %>
<%@ page import="java.net.URLEncoder" %>
<%@ page import="au.com.ourbodycorp.MailMessage"%>
<%@ page import="java.util.Properties"%>
<%@ page import="au.com.ourbodycorp.Config"%>
<%@ page import="java.util.UUID"%>
<%@ page import="java.io.OutputStreamWriter" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");

    if (cu == null) {
        response.sendRedirect(Util.loginUrl(Constants.LoginReason.login, request));
        return;
    }
    User u = cu.getUser();
    Corporation corp = cu.getCorporation();
   /*  try{
    	Util.allActivitiesLogs("Patient Groups", u.getId(), corp.getName(), "Add Patient");
    }catch(Exception e){
    	
    } */
    LibraryManager lm = new LibraryManager();

    int max = corp.getMaxPatients();
    int createdThisMonth = lm.countOfPatientsCreatedThisMonth(corp.getId());
    boolean tooMany = (max > 0 && max <= createdThisMonth);

    String fname = Util.notNull(request.getParameter("fname"));
    String lname = Util.notNull(request.getParameter("lname"));
    String email = Util.notNull(request.getParameter("email"));
    String phone = Util.notNull(request.getParameter("phone"));
    String ref = Util.notNull(request.getParameter("ref"));
    Long groupId = Long.parseLong(Util.notNull(request.getParameter("groupId")));

    HashMap<String, String> errorFields = new HashMap<String, String>();

    if (request.getMethod().equalsIgnoreCase("post") && groupId > 0) {
        if (fname.trim().length() == 0) {
            errorFields.put("fname", "please enter a name");
        }

        if (lname.trim().length() == 0) {
            errorFields.put("lname", "please enter a name");
        }
        
        if (email.trim().length() == 0) {
            errorFields.put("email", "Please enter a email address");
        }
        if(email.trim().length() > 0){
	        if (!email.matches("[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}")) {
	            errorFields.put("email", "Please enter a valid email address");
	        }
        }
        if(email!=null){
	        Connection con = Util.getConnection();
	        PreparedStatement pst = con.prepareStatement("select id from patient where email = ? ");
	        pst.setString(1, email);
	        ResultSet rs = pst.executeQuery();
	        if(rs.next()){
	        	errorFields.put("email", "This email address already exist");
	        }
	        pst.close();
	        con.close();
        }
        

        if (errorFields.size() == 0) {
            Patient p = new Patient();
            p.setCreated(new Date());
            p.setCreator(u.getId());
            p.setCorpId(corp.getId());
            p.setFirstName(fname);
            p.setLastName(lname);
            p.setEmail(email);
            p.setReference(ref);
            p.setPhone(phone);
            p.setCheck_patient_exist("Checked");
            try
  	      {
          	/* int intIndex = -1;
        	  if(email.split("@")[1].toLowerCase().startsWith("yahoo")){
        		  URL url = new URL("http://my-addr.com/email/?mail="+email+"");
     	         URLConnection urlConnection = url.openConnection();
     	         HttpURLConnection connection = null;
     	         if(urlConnection instanceof HttpURLConnection)
     	         {
     	            connection = (HttpURLConnection) urlConnection;
     	         }
     	         else
     	         {
     	            System.out.println("Please enter an HTTP URL.");
     	            return;
     	         }

        	    BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
     	         String urlString = "";
     	         String current;
     	         while((current = in.readLine()) != null)
     	         {
     	            urlString += current;
     	         }
//      	         System.out.println(urlString);
     	         intIndex = urlString.indexOf("<b>e-mail exist</b>");
        	  }else{
        	 
        		  String data = URLEncoder.encode("email", "UTF-8") + "=" + URLEncoder.encode(""+email+"", "UTF-8");

            	    URL url = new URL("http://www.emailvalidator.co/EmailValidation/ProcessSingle");
            	    URLConnection conn = url.openConnection();
            	    conn.setDoOutput(true);
            	    OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            	    wr.write(data);
            	    wr.flush();

            	    BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            	    String line = rd.readLine();
            	    
      	  	   intIndex = line.indexOf("is a valid Email address");
        	  }
    	       if(intIndex != - 1){ */
    	    	   p = lm.save(p);
    	    	   lm.addPatientToGroup(p.getId(), groupId);
    	    	   PatientGroup pg=lm.getPatientGroup(groupId);
    	    	    String type="Patient Groups";
    	            String activity_page="corp/groups/new_patient.jsp";
    	       		String corp_id=String.valueOf(corp.getId());
    	       		String corp_name=corp.getName();
    	       		String corp_loc=corp.getSuburb();
    	       		String user_id=String.valueOf(u.getId());
    	       		String user_name=u.getFullName();
    	       		String invitor_id=String.valueOf(p.getId());
    	       		String invitor_name=p.getFullName();
    	       		String flag="new";
    	            String from="Create New Patinet into Group: "+pg.getGroupName();
    	            String to="FirstName:"+p.getFirstName()+";LastName:"+p.getLastName()+";Email:"+p.getEmail()+";Phone:"+p.getPhone()+";Reference:"+p.getReference();
    	        	String desc="Add patinet "+invitor_name+" into Group "+pg.getGroupName()+"  By User";
    	           try{
    	           	//Util.allActivitiesLogs("Patient Groups", u.getId(), corp.getName(), "Add Patient");
    	           	Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
    	           }catch(Exception ee){
    	           	
    	           }
    	           String key = UUID.randomUUID().toString();
    	           Connection con1 = Util.getConnection();
					PreparedStatement pst2 = con1.prepareStatement("update patient set password = ? where id = ?");
 	                	pst2.setString(1, key);
 	                	pst2.setLong(2, p.getId());
 	                	pst2.executeUpdate();
 	                	pst2.close();
 	    		    	Properties props = new Properties();
 	    	        	props.put("firstName", fname);
 	    	        	props.put("lastName", lname);
 	    	        	props.put("invitor", corp.getName());
 	    	        	props.put("email", email);
 	    	        String url = null;
 	    	        if(Config.getString("https").equals("true")){
 	    	        	url = Config.getString("url.secure");
 	    	        	}else{
 	    	        	url = Config.getString("url");
 	    	        	}
 	    	        props.put("url", url + "/corp/patients/patient_create_password.jsp?key=" + key);
 	    	        MailMessage msg = new MailMessage(props, "create_new_password.vm", email, "Welcome to Fiizio – Create new password",false);
 	    	        try {
 	    	            msg.send();
 	    	        	}
 	    	        catch (Exception e) {
 	    	        	}
 	    		    	
     	            	con1.close();
    	           System.out.println(desc);
    	    	   response.sendRedirect("/corp/groups/detail.jsp?id="+groupId);
    	           return;
  	       /* }else{
  	    	   errorFields.put("email", "This email address is not active. Please check the spelling or enter an alternative address.");
  	       } */
//   	         System.out.println(urlString);
  	      }catch(IOException e)
  	      {
  	         e.printStackTrace();
  	      } 
            
        }
    }

    String[][] breadcrumbs = {{"/corp/", corp.getName()}, {"/corp/groups/", "Patient Groups"}, {null, "Add New Patient"}};


%>
<html>
<head><title>Add New Patient</title></head>
<body>
<div class="page-header">
    <h1>Add New Patient</h1>
</div>
<%@include file="/WEB-INF/includes/breadcrumbs.jsp"%>
<%
    if (tooMany) {
%>
<div class="row">
    <div class="span9">
        <div class="alert alert-error">
        <h3>Patient limit reached</h3>
        <p>
            Your practice has a limit of <%=corp.getMaxPatients()%> patients per month, which you have reached.
        </p>
        <p>
            Please contact support to increase your subscription or delete some patients.
        </p>
        </div>
    </div>
</div>
<%
    } else {
%>

<div class="row">
    <div class="span9">
        <%@ include file="/WEB-INF/includes/formError.jsp" %>
        <form action="new_patient.jsp" method="post" class="form-horizontal">
            <input type="hidden" name="groupId" value="<%=request.getParameter("groupId")%>">
            <%=HTML.textInput("fname", "First Name", "30", "30", fname, null, errorFields)%>
            <%=HTML.textInput("lname", "Last Name", "30", "30", lname, null, errorFields)%>
            <%=HTML.textInput("email", "Email", "30", "30", email, null, errorFields)%>
            <%=HTML.textInput("phone", "Phone", "30", "30", phone, null, errorFields)%>
            <%=HTML.textInput("ref", "Reference", "30", "30", ref, null, errorFields)%>
            <%=HTML.submitButton("Save")%>
        </form>
    </div>
</div>
<%
    }
%>
</body>
</html>