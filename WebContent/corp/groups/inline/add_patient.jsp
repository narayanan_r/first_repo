<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.List" %>
<%@ page import="java.net.URLEncoder" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");

    if (cu == null) {
        response.sendRedirect(Util.loginUrl(Constants.LoginReason.login, request));
        return;
    }
    User u = cu.getUser();
    Corporation corp = cu.getCorporation();
    /* try{
    	Util.allActivitiesLogs("Patients", u.getId(), corp.getName(), "Add to Group");
    }catch(Exception e){
    	
    } */
    LibraryManager lm = new LibraryManager();

    
    long patientId = Long.parseLong(Util.notNull(request.getParameter("patientId"), "0"));
    long groupId = Long.parseLong(Util.notNull(request.getParameter("groupId"), "0"));
    if (patientId > 0 && groupId > 0){
    	Patient p=lm.getPatient(patientId);
    	PatientGroup pg=lm.getPatientGroup(groupId); 	 	
        lm.addPatientToGroup(patientId, groupId);
        String type="Patients";
        String activity_page="corp/groups/inline/add_patient.jsp";
    	String corp_id=String.valueOf(corp.getId());
    	String corp_name=corp.getName();
    	String corp_loc=corp.getSuburb();
    	String user_id=String.valueOf(u.getId());
    	String user_name=u.getFullName();
    	String invitor_id=String.valueOf(p.getId());
    	String invitor_name=p.getFullName();
    	String flag="new";
        String from="Add Patinet into Group: "+pg.getGroupName();
        String to="FirstName:"+p.getFirstName()+";LastName"+p.getLastName()+";Email:"+p.getEmail()+";Phone:"+p.getPhone()+";Reference:"+p.getReference();
    	String desc="Add patinet "+invitor_name+" into Group "+pg.getGroupName()+"  By User";
        try{
        	//Util.allActivitiesLogs("Patients", u.getId(), corp.getName(), "Add to Group");
        	Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
        }catch(Exception ee){
        	
        }
        response.sendRedirect(corp.getHost() + "/close.jsp?redir=" + URLEncoder.encode("/corp/patients/detail.jsp?id=" + patientId, "UTF-8"));
        return;
    }
    PatientGroup pg=lm.getPatientGroup(groupId);
    long id = Long.parseLong(Util.notNull(request.getParameter("patient"), "0"));

    Patient patient = null;

    if (id > 0) {
        patient = lm.getPatient(id);
        if (patient == null || patient.getCorpId() != corp.getId()) {
            response.sendError(404, "Patient not found or not part of this practice");
            return;
        }
    } 
    else {
    	
        response.sendRedirect("/corp/patients/");
        return;
    }

    List<PatientGroup> groups = lm.getPatientGroups(corp.getId());
    List<PatientGroup> groupsPatientIsIn = lm.getGroupsForPatient(patient.getId());
%>
<html>
<head><title>Patient</title></head>
<body>
<div class="page-header">
    <h1>Groups</h1>
</div>

<div class="row">
    <table class="table table-zebra-striped">
            <thead>
            <tr>
                <th>Group Name</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
        <% if (groups != null) {
               for (PatientGroup group : groups){ %>
                 <tr>
                     <td><%=Util.textAreaValue(group.getGroupName())%></td>
                     <% if (!groupsPatientIsIn.contains(group)){ %>
                        <td><a href="/corp/groups/inline/add_patient.jsp?patientId=<%=patient.getId()%>&groupId=<%=group.getId()%>" class="fancybox"><i class="icon-plus"></i> Add</a></td>
                     <% } else { %>
                        <td>Member</td>
                     <% } %>
                 </tr>
            <% }
           } %>
           </tbody>
        </table>
</div>
</body>
</html>