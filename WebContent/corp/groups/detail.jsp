<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");

    if (cu == null) {
        response.sendRedirect(Util.loginUrl(Constants.LoginReason.login, request));
        return;
    }
    User u = cu.getUser();
    Corporation corp = cu.getCorporation();
   /*  try{
    	Util.allActivitiesLogs("Patient Groups", u.getId(), corp.getName(), "View Group Details");
    }catch(Exception e){
    	
    } */
    LibraryManager lm = new LibraryManager();

    long id = Long.parseLong(Util.notNull(request.getParameter("id"), "0"));
    boolean remove = Boolean.parseBoolean(Util.notNull(request.getParameter("remove"), "false"));
    if (remove){
        long patientId = Long.parseLong(Util.notNull(request.getParameter("patientId"), "0"));
        if (id > 0 && patientId > 0){
            lm.removePatientFromGroup(patientId, id);
        }
    }
        
    PatientGroup pg = null;
    List<Patient> patientsInGroup = null;
    if (id > 0) {
        pg = lm.getPatientGroup(id);
        if (pg == null || pg.getCorporationId() != corp.getId()) {
            response.sendError(404, "Patient Group not found or not part of this practice");
            return;
        }
        
        patientsInGroup = lm.getPatientsInGroup(id);
        String type="Patient Groups";
        String activity_page="corp/groups/detail.jsp";
    	String corp_id=String.valueOf(corp.getId());
    	String corp_name=corp.getName();
    	String corp_loc=corp.getSuburb();
    	String user_id=String.valueOf(u.getId());
    	String user_name=u.getFullName();
    	String invitor_id=String.valueOf(pg.getId());
    	String invitor_name="---";
    	String flag="view";
        String from="---";
        String to="---";
    	String desc="View "+pg.getGroupName()+" Group Details By User";
         try{
        	//Util.allActivitiesLogs("Patient Groups", u.getId(), corp.getName(), "View Group Details");
        	Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
        }catch(Exception ee){
        	
        } 
        
    } 
    else {
    	
        response.sendRedirect("/corp/groups/");
        return;
    }
    
    List<Program> programs = lm.getProgramsForGroup(pg.getId());
    Program latestProgram = null;
    if (programs.size() > 0){
        latestProgram = programs.get(0);
    }

    String[][] breadcrumbs = {{"/corp/", corp.getName()}, {"/corp/groups/", "Patient Groups"}, {null, "Group Details"}};
    
    SimpleDateFormat sdf = new SimpleDateFormat("d MMM yyyy");
    sdf.setTimeZone(corp.getTimeZone());
%>
<html>
<head><title>Patient Group</title></head>
<body>
<div class="page-header">
    <h1><%=Util.textAreaValue(pg.getGroupName())%></h1>
</div>
<%@include file="/WEB-INF/includes/breadcrumbs.jsp"%>
<div class="row">
    <div class="span7">
            <h2>Programs for Group</h2>
            <p>
                <a href="/corp/programs/inline/edit_details.jsp?group=<%=pg.getId()%>&Create=No_Share&Check=Name_Exist" class="btn btn-primary fancybox"><i class="icon-plus icon-white"></i> Add Programs</a>
                <a href="/corp/programs/inline/select_template.jsp?group=<%=pg.getId()%>" class="btn btn-primary fancybox"><i class="icon-white icon-file"></i> Program from Template</a>
            </p>
            <table class="table table-zebra-striped">
                <tbody>
                <% for (Program program : programs) {
                %>
                <tr>
                    <td><%=sdf.format(program.getCreated())%></td>
                    <td>
                        <a href="/corp/programs/edit.jsp?id=<%=program.getId()%>"><%=Util.notNull(program.getName()).length() > 0 ? Util.notNull(program.getName()) : "(not named)"%></a>
                    </td>
                    <td>
                        <a href="/corp/programs/inline/delete_program.jsp?id=<%=program.getId()%>" class="fancybox"><i class="icon-trash"></i></a>
                    </td>
                </tr>
                <%
                } %>
                </tbody>
            </table>
        </div>
</div>
<div class="row">
    <div class="span7">
        <h2>Members</h2>
        
        <p><a href="new_patient.jsp?groupId=<%=pg.getId()%>" class="btn btn-primary"><i class="icon-plus icon-white"></i> Create New Patient</a>
            <% if (corp != null && corp.isPushAllowed() && latestProgram != null){ %>
                <a href="/corp/programs/inline/push_message.jsp?programId=<%=latestProgram.getId()%>" class="btn btn-primary fancybox"><i class="icon-envelope icon-white"></i> Send Push Message</a>
            <% } %>
        </p>

        
         <% if (patientsInGroup != null && patientsInGroup.size() > 0) { %>
            <table class="table table-zebra-striped">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
            <% for (Patient patient : patientsInGroup){ %>
                    <tr>
                        <td><a href="../patients/detail.jsp?id=<%=patient.getId()%>"><%=Util.textAreaValue(patient.getFirstName())%> <%=Util.textAreaValue(patient.getLastName())%></a></td>
                        <td><%=Util.textAreaValue(patient.getEmail())%></td>
                        <td><%=Util.textAreaValue(patient.getPhone())%></td>
                        <td><a href="/corp/groups/detail.jsp?remove=true&patientId=<%=patient.getId()%>&id=<%=pg.getId()%>"><i class="icon-remove"></i> Remove</a></td>
                    </tr>
               <%} 
            }
            else {%>
                <p>This group currently has no members, you can add some in the <a href="/corp/patients">patients area.</a></p>
            <%}%>
            
           </tbody>
        </table>
        
    </div>
</div>
</body>
</html>