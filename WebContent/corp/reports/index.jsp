<%@page import="com.opensymphony.sitemesh.Content"%>
<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="java.util.List" %>
<%
    CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");

    if (cu == null) {
        response.sendRedirect(Util.loginUrl(Constants.LoginReason.login, request));
        return;
    }
    User u = cu.getUser();
    Corporation corp = cu.getCorporation();
 /*    try{
    	Util.allActivitiesLogs("Reports", u.getId(), corp.getName(), "View Report Details");
    }catch(Exception e){
    	
    } */
    LibraryManager lm = new LibraryManager();
    String type="Reports";
    String activity_page="corp/reports/index.jsp";
	String corp_id=String.valueOf(corp.getId());
	String corp_name=corp.getName();
	String corp_loc=corp.getSuburb();
	String user_id=String.valueOf(u.getId());
	String user_name=u.getFullName();
	String invitor_id="---";
	String invitor_name="---";
	String flag="reports";
	String from="---";
	String to="---";
	String desc="View Report Details By User";
     try
    {		
     Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
    }
    catch(Exception e)
    {
    } 

    String[][] breadcrumbs = {{"/corp/", corp.getName()}, {null, "Reports"}};
%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head><title>Reports</title></head>
<body>
<div class="page-header">
    <h1>Reports</h1>
</div>
<%@include file="/WEB-INF/includes/breadcrumbs.jsp"%>
<div class="span2 well">
        <h3>Practice Statistics</h3>
        <a href="/corp/reports/exportpracticestatsbymanagerservlet.xls">Download Practice Stats</a>
 </div>
</body>
</html>