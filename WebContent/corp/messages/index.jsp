<%@ page import="au.com.ourbodycorp.Config" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%@ page import="au.com.ourbodycorp.model.managers.ScheduledMessageManager" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");

    if (cu == null) {
        response.sendRedirect(Util.loginUrl(Constants.LoginReason.login, request));
        return;
    }
    User u = cu.getUser();
    Corporation corp = cu.getCorporation();
   /*  try{
    	Util.allActivitiesLogs("Messages", u.getId(), corp.getName(), "Scheduled Messages");
    }catch(Exception e){
    	
    } */
    String activity_page="corp/messages/index.jsp";
	String corp_id=String.valueOf(corp.getId());
	String corp_name=corp.getName();
	String corp_loc=corp.getSuburb();
	String user_id=String.valueOf(u.getId());
	String user_name=u.getFullName();
	String invitor_id="";
	String invitor_name="---";
	String flag="view";
    String from="---";
    String to="---";
	String desc="Scheduled Messages By User";
     try{
    	//Util.allActivitiesLogs("Messages", u.getId(), corp.getName(), "Scheduled Messages");
    	Util.allActivitiesLogs2("Messages",activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
    }catch(Exception ee){
    	
    } 
    String[][] breadcrumbs = {{"/corp/", corp.getName()}, {"/corp/messages/", "Messages"}};

    ScheduledMessageManager messageManager = new ScheduledMessageManager();
    List<ScheduledMessage> messages = messageManager.listMessages(corp.getId(), 0, 0);
    
    SimpleDateFormat sdf = new SimpleDateFormat("d MMM yyyy HH:mm");
    sdf.setTimeZone(corp.getTimeZone());
%>
<html>
<head><title>News</title></head>
<body>
<div class="page-header">
    <h1>Scheduled Messages</h1>
</div>
<%@include file="/WEB-INF/includes/breadcrumbs.jsp"%>
<div class="row">
    <div class="span9">
        <a href="edit.jsp" class="btn btn-primary"><i class="icon-plus icon-white"></i> New Message</a>
    </div>
</div>
<div class="row">
    <div class="span9">
        <table class="table">
            <thead>
            <tr>
                <th>Title</th>
                <th>Show After</th>
                <th>Preview</th>
            </tr>
            </thead>
            <tbody>
            <%
                for (ScheduledMessage message : messages) {
            %>
            <tr>
                <td><a href="edit.jsp?id=<%=message.getId()%>"><%=message.getHeadline()%></a></td>
                <td><%=message.getShowAfterDays()%> day<%=message.getShowAfterDays() > 0 ? "s" : ""%></td>
                <td>
                <% if (Config.getString("https").equals("true")){ %>
                	<a href="https://<%=corp.getSlug()%>.<%=Config.getString("baseDomain")%>/message.jsp?smid=<%=message.getId()%>" class="fancybox btn btn-mini">preview</a>
                <%}else{%>
                    <a href="http://<%=corp.getSlug()%>.<%=Config.getString("baseDomain")%>/message.jsp?smid=<%=message.getId()%>" class="fancybox btn btn-mini">preview</a>
                <% }%>
                    <a href="inline/delete_message.jsp?id=<%=message.getId()%>" class="fancybox"><i class="icon-trash"></i></a>
                </td>
            </tr>
            <%
                }
            %>
            </tbody>
        </table>
    </div>
</div>
</body>
</html>