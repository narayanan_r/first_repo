<%@page import="au.com.ourbodycorp.model.ScheduledMessage"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.Corporation" %>
<%@ page import="au.com.ourbodycorp.model.CorporationUser" %>
<%@ page import="au.com.ourbodycorp.model.MiniNews" %>
<%@ page import="au.com.ourbodycorp.model.managers.AttachmentManager" %>
<%@ page import="au.com.ourbodycorp.model.managers.ScheduledMessageManager" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%
    CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");

    Corporation corp = cu.getCorporation();
    User u = cu.getUser();
    /* try{
    	Util.allActivitiesLogs("Messages", u.getId(), corp.getName(), "Delete Scheduled Messages");
    }catch(Exception e){
    	
    } */
    long id = Long.parseLong(Util.notNull(request.getParameter("id"), "0"));

    ScheduledMessage message = null;

    ScheduledMessageManager messageManager = new ScheduledMessageManager();

    if (id != 0) {
        message = messageManager.getMessage(id);
        if (message == null || message.getCorpId() != corp.getId()) {
            response.sendError(404, "News item not found");
            return;
        }
    } 
    else {
        response.sendError(400, "No note ID provided");
        return;
    }

    if (request.getMethod().equalsIgnoreCase("post")) {
        if (request.getParameter("submit").startsWith("Cancel")) {
            response.sendRedirect(corp.getHost() + "/close.jsp");
        } 
        
        	String type="Messages";
    	    String activity_page="corp/messages/inline/delete_message.jsp";
    		String corp_id=String.valueOf(corp.getId());
    		String corp_name=corp.getName();
    		String corp_loc=corp.getSuburb();
    		String user_id=String.valueOf(u.getId());
    		String user_name=u.getFullName();
    		String invitor_id=String.valueOf(message.getId());
    		String invitor_name="---";
    		String flag="new";
    		String from="Delete Message from Library";
    	    String to="Name:"+message.getHeadline()+";Body:"+message.getBody();
    	    try{
    	    	//Util.allActivitiesLogs("Messages", u.getId(), corp.getName(), "Delete Scheduled Messages");
            	Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,"Delete Scheduled Messages By User");
            }catch(Exception ee){
            	
            }
            messageManager.deleteMessage(message.getId());

            if (message.getPhotoId() > 0) {
                AttachmentManager am = new AttachmentManager();
                am.delete(message.getPhotoId());
            }

            response.sendRedirect(corp.getHost() + "/close.jsp?reload=true");
        
        return;
    }

 %>

<html>
<head><title>Delete Message</title></head>
<body>
<div class="page-header">
    <h1>Delete Message</h1>
</div>
<div class="row">
    <div class="span6">
        <p>
            Delete <i><%=message.getHeadline()%></i>.
        </p>
        <p>
            Are you sure?
        </p>
        <form action="delete_message.jsp" method="post">
            <input type="hidden" name="id" value="<%=message.getId()%>">
            <%=HTML.confirmCancel()%>
        </form>
    </div>
</div>
</body>
</html>