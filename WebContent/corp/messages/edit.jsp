<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.MultiPartHelper" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%@ page import="au.com.ourbodycorp.model.managers.AttachmentManager" %>
<%@ page import="au.com.ourbodycorp.model.managers.ScheduledMessageManager" %>
<%@ page import="org.apache.commons.fileupload.FileItem" %>
<%@ page import="org.apache.commons.fileupload.servlet.ServletFileUpload" %>
<%@ page import="java.util.Date" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");

    if (cu == null) {
        response.sendRedirect(Util.loginUrl(Constants.LoginReason.login, request));
        return;
    }
    User u = cu.getUser();
    Corporation corp = cu.getCorporation();
    
    String titlePrefix = "Add";

    ScheduledMessageManager messageManager = new ScheduledMessageManager();

    long id = 0;
    try {
        id = Long.parseLong(request.getParameter("id"));
    } catch (Exception e) {
        // is ok
    }

    String headline = Util.notNull(request.getParameter("headline"));
    String body = Util.notNull(request.getParameter("body"));
    String link = Util.notNull(request.getParameter("link"));
    String status = Util.notNull(request.getParameter("status"));
    String daysAfter = Util.notNull(request.getParameter("daysAfter"));
	String old=null;

    MultiPartHelper mp = null;
    if (ServletFileUpload.isMultipartContent(request)) {
        mp = new MultiPartHelper(request);
        id = Long.parseLong(mp.getParameter("id"));

        headline = Util.notNull(mp.getParameter("headline"));
        body = Util.notNull(mp.getParameter("body"));
        link = Util.notNull(mp.getParameter("link"));
        status = Util.notNull(mp.getParameter("status"));
        daysAfter = Util.notNull(mp.getParameter("daysAfter"));
    }


    ScheduledMessage message = null;
    if (id > 0) {
    	/* try{
        	Util.allActivitiesLogs("Messages", u.getId(), corp.getName(), "Edit Scheduled Messages");
        }catch(Exception e){
        	
        } */
        message = messageManager.getMessage(id);
        if (message == null || message.getCorpId() != corp.getId()) {
            response.sendError(404, "no such message");
            return;
        }
        titlePrefix = "Edit";
        old="Headline:"+message.getHeadline()+";Body:"+message.getBody()+";DaysAfter:"+message.getShowAfterDays()+";link:"+message.getLink()+";";
    } /* else{
    	try{
        	Util.allActivitiesLogs("Messages", u.getId(), corp.getName(), "Add Scheduled Messages");
        }catch(Exception e){
        	
        }
    } */
    
    String[][] breadcrumbs = {{"/corp/", corp.getName()}, {"/corp/messages/", "Messages"}, {null, titlePrefix + " Message"}};

    AttachmentManager am = new AttachmentManager();
    Attachment a = null;
    String error = null;

    if (request.getMethod().equalsIgnoreCase("post")) {
        if (mp.getParameter("submit").equals("Cancel")) {
            response.sendRedirect("/corp/messages/");
            return;
        }
        if(message!=null){
        	String type="Messages";
            String activity_page="corp/messages/edit.jsp";
        	String corp_id=String.valueOf(corp.getId());
        	String corp_name=corp.getName();
        	String corp_loc=corp.getSuburb();
        	String user_id=String.valueOf(u.getId());
        	String user_name=u.getFullName();
        	String invitor_id=String.valueOf(message.getId());
        	String invitor_name="---";
        	String flag="update";
        	String to="Headline:"+headline+";Body:"+body+";DaysAfter:"+daysAfter+";link:"+link+";";
        	String desc="Edit Scheduled Messages By User";
        	 try{
             	//Util.allActivitiesLogs("Messages", u.getId(), corp.getName(), "Edit Scheduled Messages");
         		Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,old,to,desc);
         		response.sendRedirect("/corp/messages/");
                return;
         	}catch(Exception ee){
         		
         	}
        }

        if (headline.trim().length() == 0) {
            error = "Please enter a headline";
        } 
        else if (body.trim().length() == 0) {
            error = "Please enter some body text";
        }
        else if (daysAfter.trim().length() == 0) {
            error = "Please enter the amount of days";
        }
        
        try{
            Integer.parseInt(daysAfter);
        }
        catch (NumberFormatException e){
            error = "Please enter the amount of days as a number";
        }

        try {
            headline = Util.safeInput(headline, true);
            body = Util.safeInput(body, true);
            link = Util.safeInput(link, true);
        } catch (Exception e) {
            error = "HTML input is not allowed. Remove and &gt; and &lt; characters";
        }

        FileItem photo = mp.getFileItem("photo");
        if (photo != null && photo.getSize() > 0) {
            if (!photo.getName().toLowerCase().endsWith(".jpg")) {
                error = "Photos must be uploaded as .jpg file";
            }
        }

        if (error == null) {
            if (message == null) {
                message = new ScheduledMessage();
                message.setCorpId(corp.getId());
                message.setCreatedDate(new Date());
            }

            if (photo != null && photo.getSize() > 0) {
                if (message.getPhotoId() > 0) {
                    a = am.getAttachment(message.getPhotoId(), false);
                } 
                else {
                    a = new Attachment();
                    a.setUploaded(new Date());
                }
                a.setVersion(a.getVersion() + 1);
                a.setType(Attachment.AttachmentType.image);
                a.setFilename(photo.getName());
                a.setUpdated(new Date());
                a.setMimeType(photo.getContentType());
                a.setActive(true);
                a.setSize(photo.getSize());
                a.setData(photo.get());
                am.saveAttachment(a);
                message.setPhotoId(a.getId());
            }

            message.setHeadline(headline);
            message.setBody(body);
            message.setLink(link);
            message.setShowAfterDays(Integer.parseInt(daysAfter));
            messageManager.save(message);
            String type="Messages";
            String activity_page="corp/messages/edit.jsp";
        	String corp_id=String.valueOf(corp.getId());
        	String corp_name=corp.getName();
        	String corp_loc=corp.getSuburb();
        	String user_id=String.valueOf(u.getId());
        	String user_name=u.getFullName();
        	String invitor_id=String.valueOf(message.getId());
        	String invitor_name="---";
        	String flag="new";
        	String from="Add Message";
        	String to="MessageName:"+message.getHeadline()+";Body:"+message.getBody()+";DaysAfter:"+daysAfter+";link:"+link+";";
        	String desc="Add Scheduled Messages By User";
        	 try{
             	//Util.allActivitiesLogs("Messages", u.getId(), corp.getName(), "Add Scheduled Messages");
         		Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
         	}catch(Exception ee){
         		
         	}
            response.sendRedirect("/corp/messages/");
            return;
        }

    } 
    else {
        if (message != null) {
            headline = message.getHeadline();
            body = message.getBody();
            link = message.getLink();
            daysAfter = ""+message.getShowAfterDays();

            if (message.getPhotoId() > 0) {
                a = am.getAttachment(message.getPhotoId(), false);
            }
        }
    }

%>
<html>
<head><title><%=titlePrefix%> Message</title></head>
<body>
<div class="page-header">
    <h1>Message <small><%=titlePrefix%> message</small></h1>
</div>
<%@include file="/WEB-INF/includes/breadcrumbs.jsp"%>
<%@include file="/WEB-INF/includes/formErrorSimple.jsp"%>
<div class="row">
    <div class="span9">
        <form action="edit.jsp" method="post" class="form-horizontal" enctype="multipart/form-data">
            <input type="hidden" name="id" value="<%=id%>">
            <%=HTML.textInput("headline", "Headline", "50", "100", headline)%>
            <%=HTML.textArea("body", "Body", body, "input-xxlarge", 10)%>
            <%=HTML.textInput("daysAfter", "Show After Days:", "2", "2", daysAfter)%>
            <%=HTML.textInput("link", "External link", "50", "100", link, "Blogged about it? Enter the link here.", null)%>
            <%=HTML.fileInput("photo", "Add or Update Photo", null)%>
            <%
                if (a != null) {
            %>
            <div class="control-group">
            <label class="control-label" for="textarea">&nbsp;</label>
            <div class="controls">
              <img width="200" height="150" src="<%=a.getImageUrl(Attachment.ImageSize.f320)%>">
            </div>
          </div>
            <%
                }
            %>
            <%=HTML.saveCancel()%>
        </form>
    </div>
</div>
</body>
</html>