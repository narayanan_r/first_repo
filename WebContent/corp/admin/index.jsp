<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%@ page import="au.com.ourbodycorp.model.managers.AttachmentManager" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");

    if (cu == null || !cu.hasRole(CorporationRole.Role.Admin)) {
        response.sendRedirect(Util.loginUrl(Constants.LoginReason.admin, request));
        return;
    }
    Corporation corp = cu.getCorporation();
    User u = cu.getUser();
    String type="Settings";
    String activity_page="corp/admin/index.jsp";
	String corp_id=String.valueOf(corp.getId());
	String corp_name=corp.getName();
	String corp_loc=corp.getSuburb();
	String user_id=String.valueOf(u.getId());
	String user_name=u.getFullName();
	String invitor_id="";
	String invitor_name="---";
	String flag="view";
    String from="---";
    String to="---";
	String desc="View Settings Details in "+corp_name+" By User";
    /* try{
    	//Util.allActivitiesLogs("Settings", u.getId(), corp.getName(), "Settings Details");
    	Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,"View My Info By Fiizio System Admin");
    }catch(Exception ee){
    	
    } */
    /* try{
    	Util.allActivitiesLogs("Settings", u.getId(), corp.getName(), "Settings Details");
    }catch(Exception e){
    	
    } */
    
    String[][] breadcrumbs = {{"/corp/", corp.getName()}, {null, "Settings"}};
    String payment = corp.getMetaString("payment");
%>
<html>
<head><title>Settings</title></head>
<body>
<div class="page-header">
    <h1>Settings</h1>
</div>
<%@include file="/WEB-INF/includes/breadcrumbs.jsp"%>
<div class="row">
    <div class="span3">
        <h2>Basic Details</h2>
        <p><a href="edit.jsp" class="btn btn-primary">Edit Details</a></p>
    </div>
    <div class="span3">
        <h3>Name</h3>
        <p><%=corp.getName()%></p>

        <h3>Practice Address</h3>
            <address>
        <%=corp.getAddress1()%><br/>
        <%=(corp.getAddress2() != null && corp.getAddress2().length() > 0) ? corp.getAddress2() + "<br>" : ""%>
        <%=corp.getSuburb()%> <%=corp.getState()%> <%=corp.getPostcode()%><br>
                <%=corp.getCountryName()%>

            </address>

        <h3>Timezone</h3>
        <p><%=Util.notNull(corp.getTimeZone().getDisplayName())%></p>

    </div>
    <div class="span3">
        <h2>Session Timeout</h2>
        <p>Your session will be expired in <%if(corp.getSession_timeout() == 0){ %>20<%}else{%><%=corp.getSession_timeout()%><%} %> minutes </p><p><a href="/corp/admin/inline/session_timeout.jsp?timeout=<%if(corp.getSession_timeout() == 0){ %><%=20%><%}else{%><%=corp.getSession_timeout()%><%}%>" class="btn btn-primary fancybox">Set Timeout</a></p>
    </div>
</div>
<div class="row">
    <div class="span3">
        <h2>Your Logo</h2>
        <p>Upload the logo you wish to display in the app. Must be a .jpg or .png file. We recommend 640x332 pixels for the best result.</p>
        <p><a href="inline/logo.jsp" class="btn btn-primary fancybox">Edit Logo</a></p>
    </div>
    <div class="span6">
        <p>
        <%
            Attachment a = null;
            if (corp.getLogoId() > 0) {
                AttachmentManager am = new AttachmentManager();
                a = am.getAttachment(corp.getLogoId(), false);
            }

            if (a != null) {
        %>
        <img src="<%=a.getImageUrl(Attachment.ImageSize.f320)%>" width="320">
        <%
            } else {
        %>
        <img src="/images/logo_320x150.gif" width="320">
        <%
            }
        %>
        </p>
    </div>
</div>
<div class="row">
    <div class="span3">
        <h2>Practice Information</h2>
        <p>Your practice's opening hours, contact details and programmed messages to appear on the info pages of the App.</p>
        <p><a href="practice.jsp" class="btn btn-primary">Edit Practice</a></p>
    </div>
    <div class="span6">
        <h3>Opening Hours</h3>
        <p>
            <%=Util.notNull(corp.getMetaString("hours"), "Not set").replaceAll("\n", "<br>")%>
        </p>
        <h3>Phone</h3>
        <p><%=Util.notNull(corp.getMetaString("phone"), "Not set")%></p>
        <h3>After Hours Phone</h3>
        <p><%=Util.notNull(corp.getMetaString("phone2"), "Not set")%></p>
        <h3>Email</h3>
        <p><%=Util.notNull(corp.getMetaString("email"), "Not set")%></p>
        <h3>Website</h3>
        <p><%=Util.notNull(corp.getMetaString("www"), "Not set")%></p> 
    </div>
</div>
<% if (corp != null && corp.isPushAllowed()){ %>
<div class="row">
    <div class="span3">
        <h2>Scheduled Messages</h2>
        <p>Configure the messages your clients receive after loading up their App</p>
        <p><a href="/corp/messages/index.jsp" class="btn btn-primary">Edit Messages</a></p>
    </div>
</div>
<%}%>
<%-- <div class="row">
    <div class="span3">
        <h2>Import from Cliniko</h2>
        <p>To import the patient details from Cliniko: Practice Management Software</p>
        <p>Please Update API Key first before you import patient details from Cliniko.</p>
        <table><tr>
        <td><p><a href="inline/updateAPI.jsp?id=<%=u.getId()%>" class="btn btn-primary fancybox">Update API Key</a></p></td>
        <td><p><a href="/corp/admin/Cliniko" class="btn btn-primary">Import</a></p></td>
        </tr></table>
    </div>
</div> --%>
</body>
</html>