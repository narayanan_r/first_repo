<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%@ page import="au.com.ourbodycorp.model.managers.CorporationManager" %>
<%@ page import="java.util.*" %>
<%@ page import="au.com.ourbodycorp.model.managers.CountryManager" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");

    if (cu == null || !cu.hasRole(CorporationRole.Role.Admin)) {
        response.sendRedirect(Util.loginUrl(Constants.LoginReason.admin, request));
        return;
    }
    Corporation corp = cu.getCorporation();
    User u = cu.getUser();
 	String from="Name:"+corp.getName()+";AddressLine1:"+corp.getAddress1()+";AddressLine2:"+corp.getAddress2()+";City/Suburb:"+corp.getSuburb()+";State/Territory:"+corp.getState()+";Postcode:"+corp.getPostcode();

    /* try{
    	Util.allActivitiesLogs("Settings", u.getId(), corp.getName(), "Edit Basic Details");
    }catch(Exception e){
    	
    } */
    String name = request.getParameter("name");
    String address1 = request.getParameter("address1");
    String address2 = request.getParameter("address2");
    String suburb = request.getParameter("suburb");
    String state = request.getParameter("state");
    String manualState = (request.getParameter("manualState") != null) ? request.getParameter("manualState").trim() : "";
    String postcode = request.getParameter("postcode");
    String timezone = request.getParameter("timezone");
    String payment = request.getParameter("payment");
    String country = (request.getParameter("country") != null) ? request.getParameter("country").trim() : "";

//     if (!country.equals("AU")) {
//         state = manualState;
//     }

    HashMap<String, String> errorFields = new HashMap<String, String>();

    CorporationManager cm = new CorporationManager();

    if (request.getMethod().equalsIgnoreCase("get")) {
        name = corp.getName();
        address1 = corp.getAddress1();
        address2 = corp.getAddress2();
        suburb = corp.getSuburb();
        if(corp.getCountry().equals("AU")){
        state = corp.getState();
        manualState = "";
        }else{
        state = "";
        manualState = corp.getState();
        }
        country = corp.getCountry();
        postcode = corp.getPostcode();
        timezone = corp.getTimeZone().getID();


        payment = corp.getMetaString("payment");


    } else {

        Corporation test = cm.getCorporationByName(name);
        boolean exists = (test != null && test.getId() != corp.getId());

        if (name == null || name.trim().length() < 3) {
            errorFields.put("name", "A name of 3 characters or more is required");
        } else if (exists) {
            errorFields.put("name", "This name is already in use");
        }

        if (address1 == null || address1.trim().length() < 3) {
            errorFields.put("address1", "An address is required");
            errorFields.put("address2", "");
        }

        if (suburb == null || suburb.trim().length() < 2) {
            errorFields.put("suburb", "A suburb is required");
        }

        if (country.equals("AU") && (postcode == null || !postcode.matches("[0-9]{4}"))) {
            errorFields.put("postcode", "A valid postcode is required");
        }
        
        if (!country.equals("AU") && (postcode == "")) {
            errorFields.put("postcode", "Please enter a postcode");
        }

        if (country.equals("AU") && (state.length() == 0 || state.equals("XX"))) {
            errorFields.put("state", "Please select an Australian state");
        }
        
        if (!country.equals("AU") && (manualState.length() == 0 || manualState.equals("XX"))) {
            errorFields.put("manualState", "Please enter a state");
        }

        if (timezone == null || timezone.length() == 0) {
            errorFields.put("timezone", "Please select a timezone");
        }

        if (errorFields.size() == 0) {
            Corporation c = cm.getCorporation(corp.getId());
            c.setAddress1(address1);
            c.setAddress2(address2);
            c.setName(name);
            c.setCountry(country);
            c.setPostcode(postcode);
            if(manualState != ""){
            c.setState(manualState);
            }else{
            c.setState(state);
            }
            c.setSuburb(suburb);
            c.setTimeZone(TimeZone.getTimeZone(timezone));
            c = cm.save(c, u.getId());
            String type="Settings";
            String activity_page="corp/admin/edit.jsp";
        	String corp_id=String.valueOf(c.getId());
        	String corp_name=c.getName();
        	String corp_loc=c.getSuburb();
        	String user_id=String.valueOf(u.getId());
        	String user_name=u.getFullName();
        	String invitor_id="";
        	String invitor_name="---";
        	String flag="update";
        	String to="Name:"+name+";AddressLine1:"+address1+";AddressLine2:"+address2+";City/Suburb:"+suburb+";State/Territory:"+state+";Postcode:"+postcode;
        	String desc="Edit Basic Details into "+corp_name+" By User";
        	 try{
        	  //  	Util.allActivitiesLogs("Settings", u.getId(), corp.getName(), "Edit Basic Details");
         		Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
         	}catch(Exception ee){
         		
         	}

            response.sendRedirect("/corp/admin/");
            return;
        }
    }

    String zones[] = TimeZone.getAvailableIDs();
    Arrays.sort(zones);
    LinkedList<TimeZone> tz = new LinkedList<TimeZone>();
    for (String zone : zones) {
         tz.add(TimeZone.getTimeZone(zone));

    }

    String[] tzValues = new String[tz.size()];
    String[] tzNames = new String[tz.size()];

    int ti = 0;
    for (TimeZone timeZone : tz) {
        tzValues[ti] = timeZone.getID();
        tzNames[ti] = timeZone.getID();
        ti++;
    }

    String[][] breadcrumbs = {{"/corp/", corp.getName()}, {"/corp/admin/", "Settings"}, {null, "Edit Details"}};

    CountryManager ctm = new CountryManager();
    List<Country> countries = ctm.listCountries();

%>
<html>
<head>
    <title>Edit Basic Details</title>
    <script type="text/javascript">
    $( document ).ready(function() {
    var s = document.getElementById("country");
    var au = document.getElementById("state_au");
    var other = document.getElementById("state_other");
    if (s[s.selectedIndex].value == "AU") {
        au.style.display = "block";
        other.style.display = "none";
    } else {
        au.style.display = "none";
        other.style.display = "block";
    }
    });
    
          function toggleState() {
              var s = document.getElementById("country");
              var au = document.getElementById("state_au");
              var other = document.getElementById("state_other");
              if (s[s.selectedIndex].value == "AU") {
                  au.style.display = "block";
                  other.style.display = "none";
                  $("#manualState").val("");
              } else {
                  au.style.display = "none";
                  other.style.display = "block";
              }
          }
    </script>
</head>
<body>


  <div class="page-header">
    <h1>Edit Basic Details</h1>
  </div>
  <%@include file="/WEB-INF/includes/breadcrumbs.jsp"%>
  <div class="row">
    <div class="span3 columns">
      <h2>Update details</h2>
      <p>If you change the name of your clinic, the clinic name as appears currently on the subdomain web address will not change. If you want to change that too, please contact support.</p>
    </div>
    <div class="span6 columns">
        <%@ include file="/WEB-INF/includes/formError.jsp" %>
      <form action="edit.jsp" method="post" class="form-horizontal">
          <fieldset>
              <legend>Name</legend>
              <%
              Corporation.Type typeArray[] = Corporation.Type.values();
              String typeValues[] = new String[typeArray.length];
              String typeLabels[] = new String[typeArray.length];
              for (int i = 0; i < typeArray.length; i++) {
                  typeValues[i] = typeArray[i].name();
                  typeLabels[i] = typeArray[i].getDescription();
              }
          %>


              <%=HTML.textInput("name","Name","30","50",name, null, errorFields)%>
          </fieldset>

        <fieldset>
          <legend>Practice Address</legend>
          <%=HTML.textInput("address1", "Address line 1", "30", "50", address1, null, errorFields)%>
          <%=HTML.textInput("address2", "Address line 2", "30", "50", address2, null, errorFields)%>

          <%=HTML.textInput("suburb", "City / Suburb", "30", "50", suburb, null, errorFields)%>

          <%
              Constants.State stateArray[] = Constants.State.values();
              String stateValues[] = new String[stateArray.length];
              String stateLabels[] = new String[stateArray.length];
              for (int i = 0; i < stateArray.length; i++) {
                  stateValues[i] = stateArray[i].name();
                  stateLabels[i] = stateArray[i].getFullName();
              }
          %>
          <div id="state_au" <%=(!country.equals("AU")) ? "style=\"display:none\"" : ""%>>
            <%=HTML.select("state", "State or Territory", stateValues, stateLabels, state, null, errorFields, true)%>
          </div>

          <div id="state_other" <%=(country.equals("AU")) ? "style=\"display:none\"" : ""%>>
            <%=HTML.textInput("manualState", "State/Province", "30", "50", manualState, null, errorFields)%>
          </div>

          <%=HTML.textInput("postcode", "Postcode", "30", "50", postcode, null, errorFields)%>

            <div class="control-group">
              <label for="country" class="control-label">Country</label>
              <div class="controls">
                  <select id="country" name="country" onchange="toggleState()">
                      <%
                          for (Country c : countries) {
                      %>
                      <option value="<%=c.getIso()%>" <%=(c.getIso().equals(country)) ? "selected" : ""%>><%=c.getDisplayName()%></option>
                      <%
                          }
                      %>
                    </select>
              </div>
          </div>

          <%=HTML.select("timezone", "Timezone", tzValues, tzNames, timezone, null,errorFields, false)%>
          </fieldset>

        <%=HTML.submitButton("Save Changes")%>
      </form>
    </div>
  </div>



</body>
</html>