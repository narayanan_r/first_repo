<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%@ page import="au.com.ourbodycorp.model.managers.AttachmentManager" %>
<%@ page import="au.com.ourbodycorp.model.managers.CorporationManager" %>
<%@ page import="org.apache.commons.fileupload.FileItem" %>
<%@ page import="org.apache.commons.fileupload.FileItemFactory" %>
<%@ page import="org.apache.commons.fileupload.disk.DiskFileItemFactory" %>
<%@ page import="org.apache.commons.fileupload.servlet.ServletFileUpload" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.List" %>
<%@ page import="java.net.URLEncoder" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");

    if (cu == null || !cu.hasRole(CorporationRole.Role.Admin)) {
        response.sendRedirect(Util.loginUrl(Constants.LoginReason.admin, request));
        return;
    }
    Corporation corp = cu.getCorporation();

    CorporationManager cm = new CorporationManager();
    User u = cu.getUser();
   /*  try{
    	Util.allActivitiesLogs("Settings", u.getId(), corp.getName(), "Upload Corporation Logo");
    }catch(Exception e){
    	
    } */
    String error = null;

    if (ServletFileUpload.isMultipartContent(request)) {
        FileItemFactory factory = new DiskFileItemFactory();
        ServletFileUpload upload = new ServletFileUpload(factory);
        List /* FileItem */ items = upload.parseRequest(request);
        for (Object o : items) {
            FileItem item = (FileItem) o;
            if (!item.isFormField()) {
                if (item.getName().toLowerCase().endsWith(".png") || item.getName().toLowerCase().endsWith(".jpg")) {
                    Attachment a = new Attachment();
                    a.setType(Attachment.AttachmentType.image);
                    a.setData(item.get());
                    a.setFilename(item.getName());
                    a.setMimeType(item.getContentType());
                    a.setUploaded(new Date());
                    AttachmentManager am = new AttachmentManager();
                    am.saveAttachment(a);

                    Corporation c = cm.getCorporation(corp.getId());
                    long old = c.getLogoId();
                    c.setLogoId(a.getId());
                    cm.save(c, 0);

                    if (old > 0) {
                        am.delete(old);
                    }
                    String type="Settings";
                    String activity_page="corp/admin/inline/logo.jsp";
                	String corp_id=String.valueOf(c.getId());
                	String corp_name=c.getName();
                	String corp_loc=c.getSuburb();
                	String user_id=String.valueOf(u.getId());
                	String user_name=u.getFullName();
                	String invitor_id="";
                	String invitor_name="---";
                	String flag="upload";
                	String from="---";
                	String to="---";
                	String desc="Upload Corporation Logo into "+corp_name+" By User";
                	 try{
                	    //	Util.allActivitiesLogs("Settings", u.getId(), corp.getName(), "Upload Corporation Logo");
                 		Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
                 	}catch(Exception ee){
                 		
                 	}
                	response.sendRedirect(corp.getHost() + "/close.jsp?redir=" + URLEncoder.encode("/corp/admin/", "UTF-8"));
                    return;
                } else {
                    error = "File must be .png or .jpg";
                    break;
                }

            }
        }
    }

%>
<html>
<head><title>Upload Logo</title></head>
<body>
<div class="page-header">
    <h1>Upload Logo</h1>
</div>
<%@include file="/WEB-INF/includes/formErrorSimple.jsp"%>
<div class="row">
    <div class="span6">
        <form method="post" enctype="multipart/form-data" action="logo.jsp" class="form-horizontal">
            <div class="control-group">
            <label class="control-label" for="fileInput">Image File</label>
            <div class="controls">
              <input class="input-file" id="fileInput" type="file" name="file">
              <p class="help-block">Must be a .jpg or .png file. We recommend 640x332 pixels for the best result.</p>
            </div>
          </div>
            <%=HTML.submitButton("Upload Image")%>
        </form>
    </div>
</div>
</body>
</html>