<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%@ page import="au.com.ourbodycorp.model.managers.AttachmentManager" %>
<%@ page import="au.com.ourbodycorp.model.managers.CorporationManager" %>
<%@ page import="org.apache.commons.fileupload.FileItem" %>
<%@ page import="org.apache.commons.fileupload.FileItemFactory" %>
<%@ page import="org.apache.commons.fileupload.disk.DiskFileItemFactory" %>
<%@ page import="org.apache.commons.fileupload.servlet.ServletFileUpload" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.List" %>
<%@ page import="java.net.URLEncoder" %>
<%@ page import="java.sql.*" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
	CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");

    if (cu == null || !cu.hasRole(CorporationRole.Role.Admin)) {
        response.sendRedirect(Util.loginUrl(Constants.LoginReason.admin, request));
        return;
    }

    Corporation corp = cu.getCorporation();

    CorporationManager cm = new CorporationManager();
    User u = cu.getUser();
	String from="SetTimeout:"+corp.getSession_timeout()+";";

   /*  try{
    	Util.allActivitiesLogs("Session Timeout", u.getId(), corp.getName(), "Edit Session Timeout");
    }catch(Exception e){
    	
    } */
    String error = null;

    String timeoutChoices[] = {"5", "10", "20", "30", "45", "60", "90"};
    String timeout = Util.notNull(request.getParameter("timeout"), timeoutChoices[2]);
    
    if (request.getMethod().equalsIgnoreCase("post")) {
        if (request.getParameter("submit").startsWith("Cancel")) {
        	response.sendRedirect(corp.getHost() + "/close.jsp");
        	return;
        } 
        else {
        	String type="Session Timeout";
            String activity_page="corp/admin/inline/session_timeout.jsp";
        	String corp_id=String.valueOf(corp.getId());
        	String corp_name=corp.getName();
        	String corp_loc=corp.getSuburb();
        	String user_id=String.valueOf(u.getId());
        	String user_name=u.getFullName();
        	String invitor_id="";
        	String invitor_name="---";
        	String flag="update";
        	String to="SetTimeout:"+request.getParameter("timeout")+";";
        	String desc="Edit Session Timeout into "+corp_name+" By User";
        	 try{
        	    	//Util.allActivitiesLogs("Session Timeout", u.getId(), corp.getName(), "Edit Session Timeout");
         		Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
         	}catch(Exception ee){
         		
         	}
        	Connection conn = Util.getConnection();
            PreparedStatement pst = conn.prepareStatement("update corporation set session_timeout = ? where id = ?");
            pst.setLong(1, Long.parseLong(timeout));
            pst.setLong(2, corp.getId());
            pst.executeUpdate();
            pst.close();
            conn.close();
        }
            
            response.sendRedirect(corp.getHost() + "/close.jsp?redir=" + URLEncoder.encode("/corp/admin/","UTF-8"));
        	return;
    }
    
%>
<html>
<head><title>Session Timeout</title></head>
<body>
<div class="page-header">
    <h1>Session Timeout</h1>
</div>
<%@include file="/WEB-INF/includes/formErrorSimple.jsp"%>
<div class="row">
    <div class="span6">
        <form method="post" action="session_timeout.jsp" class="form-horizontal">
           <%=HTML.selectMini("timeout", "Set Timeout", timeoutChoices, timeoutChoices, timeout, "Minutes", null, false)%>
           <%=HTML.saveCancel()%>
        </form>
    </div>
</div>
</body>
