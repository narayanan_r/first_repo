<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.Constants" %>
<%@ page import="au.com.ourbodycorp.model.Corporation" %>
<%@ page import="au.com.ourbodycorp.model.CorporationRole" %>
<%@ page import="au.com.ourbodycorp.model.CorporationUser" %>
<%@ page import="au.com.ourbodycorp.model.managers.CorporationManager" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");

    if (cu == null || !cu.hasRole(CorporationRole.Role.Admin)) {
        response.sendRedirect(Util.loginUrl(Constants.LoginReason.admin, request));
        return;
    }
    Corporation corp = cu.getCorporation();
    User u = cu.getUser();
   /*  try{
    	Util.allActivitiesLogs("Settings", u.getId(), corp.getName(), "Edit Practice");
    }catch(Exception e){
    	
    } */
    CorporationManager cm = new CorporationManager();

    String hours = Util.notNull(request.getParameter("hours"));
    String phone = Util.notNull(request.getParameter("phone"));
    String phone2 = Util.notNull(request.getParameter("phone2"));
    String email = Util.notNull(request.getParameter("email"));
    String www = Util.notNull(request.getParameter("www"));
	

    if (request.getMethod().equalsIgnoreCase("post")) {
        if (request.getParameter("submit").equals("Cancel")) {
            response.sendRedirect("/corp/admin/");
            return;
        }
        String type="Settings";
        String activity_page="corp/admin/practice.jsp";
    	String corp_id=String.valueOf(corp.getId());
    	String corp_name=corp.getName();
    	String corp_loc=corp.getSuburb();
    	String user_id=String.valueOf(u.getId());
    	String user_name=u.getFullName();
    	String invitor_id="";
    	String invitor_name="---";
    	String flag="new";
    	String from="Create Practice into Corporation: '"+corp_name+"' By User:"+user_name;
    	String to="OpeningHours:"+hours+";Phone:"+phone+";AfterHours:"+phone2+";Email:"+email+";WebUrl:"+www;
    	String desc="Edit Practice in "+corp_name+" By User";
    	 try{
    	    //	Util.allActivitiesLogs("Settings", u.getId(), corp.getName(), "Edit Practice");
     		Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
     	}catch(Exception ee){
     		
     	}
        cm.setCorpMeta(corp.getId(),"hours", Util.safeInput(hours, false));
        cm.setCorpMeta(corp.getId(),"phone", Util.safeInput(phone, false));
        cm.setCorpMeta(corp.getId(),"phone2", Util.safeInput(phone2, false));
        cm.setCorpMeta(corp.getId(),"email", Util.safeInput(email, false));
        cm.setCorpMeta(corp.getId(),"www", Util.safeInput(www, false));
        response.sendRedirect("/corp/admin");
        return;
    } 
    else {
        phone = corp.getMetaString("phone");
        phone2 = corp.getMetaString("phone2");
        hours = corp.getMetaString("hours");
        email = corp.getMetaString("email");
        www = corp.getMetaString("www");
    }

    String[][] breadcrumbs = {{"/corp/", corp.getName()}, {"/corp/admin/", "Settings"}, {null, "Edit Practice"}};
%>
<html>
<head>
    <title>Edit Practice</title>

    <script type="text/javascript" src="/tiny_mce/tiny_mce.js"></script>
    <script type="text/javascript">
    tinyMCE.init({
        // General options
        mode : "exact",
        elements: "body",
        theme : "advanced",
        plugins : "autolink,lists,spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",

        // Theme options
        theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,formatselect,fontselect,fontsizeselect",
        theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,bullist,numlist,|,outdent,indent,blockquote,|,insertdate,inserttime,preview,|,forecolor,backcolor,|,hr,removeformat,visualaid,|,sub,sup",
        theme_advanced_buttons3 : "",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_statusbar_location : "bottom",
        theme_advanced_resizing : false,

        // Skin options
        skin : "o2k7",
        skin_variant : "silver",

        // Example content CSS (should be your site CSS)
        content_css : "css/example.css",

        // Drop lists for link/image/media/template dialogs
        template_external_list_url : "js/template_list.js",
        external_link_list_url : "js/link_list.js",
        external_image_list_url : "js/image_list.js",
        media_external_list_url : "js/media_list.js"

    });
</script>
</head>
<body>
<div class="page-header">
    <h1>Edit Practice</h1>
  </div>
  <%@include file="/WEB-INF/includes/breadcrumbs.jsp"%>
  <div class="row">
      <div class="span2">
      <h2>Update details</h2>
      <p></p>
    </div>
    <div class="span7">
        <form action="practice.jsp" method="post" class="form-horizontal">
            <%=HTML.textArea("hours", "Opening Hours", hours, 5)%>
            <%=HTML.textInput("phone", "Phone", "20", "20", phone)%>
            <%=HTML.textInput("phone2", "After Hours", "20", "20", phone2)%>
            <%=HTML.textInput("email", "Email", "100", "100", email)%>
            <%=HTML.textInput("www", "Website URL", "100", "100", www)%>

            <%=HTML.saveCancel()%>
        </form>
    </div>
  </div>
</body>
</html>