<%@page import="java.math.BigDecimal"%>
<%-- <%@page import="com.ibm.icu.math.BigDecimal"%> --%>
<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="au.com.ourbodycorp.model.managers.CorporationManager" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.util.*" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="org.json.JSONObject" %>
<%@ page import="au.com.ourbodycorp.Config" %>
<%@ page import="au.com.ourbodycorp.MailMessage" %>
<%@page import="com.eway.xml.bean.GetAccessCodeResultResponse"%>
<%@page import="com.eway.process.RapidAPI"%>
<%@page import="com.eway.process.SOAPClientJava"%>
<%@page import="javax.xml.namespace.QName"%>
<%@page import="javax.xml.soap.*"%>
<%@page import="javax.xml.transform.*"%>
<%@page import="javax.xml.transform.stream.*"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<META Http-Equiv="Cache-Control" Content="no-cache"/>
<META Http-Equiv="Pragma" Content="no-cache"/>
<META Http-Equiv="Expires" Content="0"/>
<%
    CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");

    if (cu == null) {
        response.sendRedirect(Util.loginUrl(Constants.LoginReason.login, request));
        return;
    }
    
    User u = cu.getUser();
    Corporation corp = cu.getCorporation();
    LibraryManager lm = new LibraryManager();
    SOAPClientJava soap = new SOAPClientJava();
    
	if(session.getAttribute("eWayAccessCode") !=null){
		   String access = new RapidAPI().CallHttpGetURL(session.getAttribute("eWayAccessCode").toString(),"https://api.ewaypayments.com/AccessCode/");
		   JSONObject jsonObject = new JSONObject(access);
		   String accessCode = jsonObject.getString("AccessCode");
		   String resp = access;
		   double totamt = jsonObject.getDouble("TotalAmount");
		   String invoiceNumber = jsonObject.getString("InvoiceNumber");
		   String responseCode = jsonObject.getString("ResponseCode");
		   int transId = 0;
		   if(responseCode.equals("00")){
		   	   transId = jsonObject.getInt("TransactionID");
		   }
		   boolean transStatus = jsonObject.getBoolean("TransactionStatus");
		   double tamt = (totamt / 100);
		   java.math.BigDecimal totalamt = new java.math.BigDecimal(tamt);
		   
		    PaymentHistory ph;
	        	
	        	ph = new PaymentHistory();
	        	ph.setUserid(u.getId());
	        	ph.setCorp_id(corp.getId());
	        	ph.setCompany(corp.getCompanyId());
	        	ph.setAccess_code(accessCode);
	        	ph.setResponse(resp);
	        	ph.setInvoice_number(invoiceNumber);
	        	ph.setTotal_amount(totalamt);
	        	ph.setTrans_id(transId);
	        	ph.setTrans_status(transStatus);
	        	ph.setCreated(new Date());
	        	lm.savePaymentHistoryDetails(ph);
	        	session.setAttribute("eWayAccessCode", null);
	        	
	        	String[] cusId = access.split(",");
	        	String[] cusId1 = cusId[9].split(":");
	        	String ctId = cusId1[1];
	        	
	        	if(!ctId.equals("null")){
	        		
	        		String Customer = new RapidAPI().CallHttpGetURL(ctId,"https://api.ewaypayments.com/Customer/");

		            String[] num = Customer.split(",");
		        	String[] num1 = num[0].split(":");
		        	
		        	String[] nam = Customer.split(",");
		        	String[] nam1 = nam[1].split(":");
		        	
		        	String[] mo = Customer.split(",");
		        	String[] mo1 = mo[2].split(":");
		        	
		        	String[] ye = Customer.split(",");
		        	String[] ye1 = ye[3].split(":");
		        	
		        	String[] cid2 = Customer.split(",");
		        	String[] cid3 = cid2[7].split(":");
		        	
		        	String Number = num1[3].replace("\"","");
		        	String Name = nam1[1].replace("\"","");
		        	String ExpiryMonth = mo1[1].replace("\"","");
		        	String ExpiryYear = ye1[1].replace("\"","");
		        	String TokenCusID = cid3[1].replace("\"","");
		        	
		        	CardDetails card = new CardDetails();
		    		card.setTokencustomerid(Long.parseLong(TokenCusID));
		        	card.setAccountno(Number);
		        	card.setMonth(Long.parseLong(ExpiryMonth));
		        	card.setYear(Long.parseLong(ExpiryYear));
		        	if (Number.substring(0,2).startsWith("4")) {
		        		card.setCard_type("Visa");
		            } else{
		            	card.setCard_type("MC");
		            }
		        	card.setName_on_card(Name);
		        	card.setCorp_id(corp.getId());
		        	card.setCompany(corp.getCompanyId());
		        	card.setUserid(u.getId());
		        	card.setCreated(new Date());
		        	lm.saveCardDetails(card);
	        	}
	        	
	        	Connection conn3 = Util.getConnection();
	            PreparedStatement pst3 = conn3.prepareStatement("select id,rebill_customer_id,rebill_id,order_date,canceled_date from cancel_subscription where corp_id = ? and canceled_date is null");
	            pst3.setLong(1, corp.getId());
	            ResultSet rs3 = pst3.executeQuery();
	            if(rs3.next()){
	            	session.setAttribute("CustomerId", rs3.getLong("rebill_customer_id"));
	            	session.setAttribute("RebillId", rs3.getLong("rebill_id"));
		      	}else{
		    			session.setAttribute("CustomerId", null);
		    			session.setAttribute("RebillId", null);
		    	} pst3.close(); conn3.close();

				SimpleDateFormat sdf = new SimpleDateFormat("d MMM, yyyy hh:mm a");

				String no = String.valueOf(ph.getId());
	            String orderdate = String.valueOf(sdf.format(ph.getCreated()));
			    String transactionno = String.valueOf(ph.getTrans_id());

				String planename = session.getAttribute("plan_name").toString();
	            String price = session.getAttribute("price").toString();
			    String tax = session.getAttribute("tax").toString();
			    String total = session.getAttribute("totalamt").toString();

	            Properties props = new Properties();
	            props.put("no", no);
	            props.put("orderdate", orderdate);
	            props.put("billedto", u.getFirstName()+" "+u.getLastName());
	            props.put("transactionno", transactionno);
	            props.put("company", corp.getName());
	            props.put("address1", corp.getAddress1());
	            props.put("address2", corp.getAddress2());
	            props.put("city", corp.getSuburb());
	            props.put("state", corp.getState());
	            props.put("pincode", corp.getPostcode());
	            props.put("country", corp.getCountryName());
	            props.put("itemname", planename);
	            props.put("price", price);
	            props.put("tax", tax);
	            props.put("total", total);
	            MailMessage msg = new MailMessage(props, "new_order.vm", "shagul001@gmail.com","New Order "+ph.getId()+"",false);

	            try {
	                msg.send();
	            }
	            catch (Exception e) {
// 	                e.printStackTrace();
// 	                throw e;
	            }
	            
	            if(session.getAttribute("savedpromocode") != null){
	            	
	            String promocode = session.getAttribute("savedpromocode").toString();
	            
	            Connection conn = Util.getConnection();
	            PreparedStatement pst = conn.prepareStatement("update promotion set company = ?, accepted = current_timestamp where promo_code = ?");
	            pst.setLong(1, corp.getCompanyId());
	            pst.setString(2, promocode);
	            pst.executeUpdate();
	            pst.close();
	            conn.close();
	            
	            }
	            
	            if(session.getAttribute("EWAY_CARDNUMBER") !=null){
	            	
	            	if(session.getAttribute("CustomerId") !=null){
	            		
	            		try {
	            			String cId = session.getAttribute("CustomerId").toString();
		                    // Create SOAP Connection
		                    SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
		                    SOAPConnection soapConnection = soapConnectionFactory.createConnection();

		                    // Send SOAP Message to SOAP Server
		                    String url = "https://www.eway.com.au/gateway/rebill/manageRebill.asmx";
//		                    String url = "https://www.eway.com.au/gateway/rebill/test/manageRebill_test.asmx";
		                    SOAPMessage soapResponseUpdateCustomer = soapConnection.call(soap.createSOAPUpdateCustomerRequest(cId, u.getFirstName(), u.getLastName(), corp.getAddress1(), corp.getAddress2(), corp.getSuburb(), corp.getState(), corp.getName(), corp.getPostcode(), corp.getCountry(), u.getEmail(), u.getMobile()), url);

		                    // Process the SOAP Response
		                    soap.printSOAPResponse(soapResponseUpdateCustomer);
		                    
		                    String RebillCustomerID = soapResponseUpdateCustomer.getSOAPBody().getElementsByTagName("RebillCustomerID").item(0).getFirstChild().getNodeValue(); 
		                    System.out.print("\nRebillCustomerID "+RebillCustomerID);
		                    
		                    if(RebillCustomerID != null){
		                    	String rId = session.getAttribute("RebillId").toString();
		                    	
		                    	Date date = new Date();
		                		Calendar cal = Calendar.getInstance();
		                		SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyyy");
		                		String strDate = sdf1.format(cal.getTime());
		                		
		                		 String[] curDate = strDate.split("/");
		                		 int day = Integer.parseInt(curDate[0]) + 1;
		                		 int month = Integer.parseInt(curDate[1]) + 1;
		                		 int addfiveYear = Integer.parseInt(curDate[2]);
		                		 
		               		 	 cal.set(Calendar.MONTH, month - 1);
		               		 	 String nextMonth = sdf1.format(cal.getTime());
		               		 	 
		               		 	 cal.set(Calendar.MONTH, month);
		               		 	 String nextstartMonth = sdf1.format(cal.getTime());
		               		 	 
		                   		 cal.set(Calendar.DATE, day);
		                   		 cal.set(Calendar.MONTH, month - 3);
		                   		 cal.set(Calendar.YEAR, addfiveYear + 5);
		             		 	 String fiveYear = sdf1.format(cal.getTime());
		                		
		                    	String InvRef = session.getAttribute("invoice_number").toString();
		                    	String InvDes = "1 x "+session.getAttribute("plan_name").toString();
		                    	String CCName = session.getAttribute("EWAY_CARDNAME").toString();
		                    	String CCNumber = session.getAttribute("EWAY_CARDNUMBER").toString();
		                    	String CCMM = session.getAttribute("EWAY_CARDEXPIRYMONTH").toString();
		                    	String CCYY = session.getAttribute("EWAY_CARDEXPIRYYEAR").toString();
		                    	String TotAmt = session.getAttribute("TotalAmountforRebiiling").toString();
		                    	
		                    	SOAPMessage soapResponseUpdateRebillEvent = soapConnection.call(soap.createSOAPUpdateRebillEventRequest(RebillCustomerID, rId, InvRef, InvDes, CCName, CCNumber, CCMM, CCYY, TotAmt, nextMonth, nextstartMonth, fiveYear), url);

		                        // Process the SOAP Response
		                        soap.printSOAPResponse(soapResponseUpdateRebillEvent);
		                        
		                        String RebillID = soapResponseUpdateRebillEvent.getSOAPBody().getElementsByTagName("RebillID").item(0).getFirstChild().getNodeValue(); 
			                    System.out.print("\nRebillID "+RebillID);
			                    
			                    Connection conn = Util.getConnection();
			    	            PreparedStatement pst = conn.prepareStatement("update cancel_subscription set order_date = current_timestamp where rebill_customer_id = ?");
			    	            pst.setLong(1, Long.parseLong(RebillCustomerID));
			    	            pst.executeUpdate();
			    	            pst.close();
			    	            conn.close();
		                    }

		                    soapConnection.close();
		                } catch (Exception e) {
		                    System.err.println("Error occurred while sending SOAP Request to Server");
		                    e.printStackTrace();
		                }
	            		
	            	}else{
	            		
	            	try {
	                    // Create SOAP Connection
	                    SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
	                    SOAPConnection soapConnection = soapConnectionFactory.createConnection();

	                    // Send SOAP Message to SOAP Server
	                    String url = "https://www.eway.com.au/gateway/rebill/manageRebill.asmx";
//	                    String url = "https://www.eway.com.au/gateway/rebill/test/manageRebill_test.asmx";
	                    SOAPMessage soapResponseCreateCustomer = soapConnection.call(soap.createSOAPCreateCustomerRequest(u.getFirstName(), u.getLastName(), corp.getAddress1(), corp.getAddress2(), corp.getSuburb(), corp.getState(), corp.getName(), corp.getPostcode(), corp.getCountry(), u.getEmail(), u.getMobile()), url);

	                    // Process the SOAP Response
	                    soap.printSOAPResponse(soapResponseCreateCustomer);
	                    
	                    String RebillCustomerID = soapResponseCreateCustomer.getSOAPBody().getElementsByTagName("RebillCustomerID").item(0).getFirstChild().getNodeValue(); 
	                    System.out.print("\nRebillCustomerID "+RebillCustomerID);
	                    
	                    if(RebillCustomerID != null){
	                    	Date date = new Date();
	                		Calendar cal = Calendar.getInstance();
	                		SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyyy");
	                		String strDate = sdf1.format(cal.getTime());
	                		
	                		 String[] curDate = strDate.split("/");
	                		 int day = Integer.parseInt(curDate[0]) + 1;
	                		 int month = Integer.parseInt(curDate[1]) + 1;
	                		 int addfiveYear = Integer.parseInt(curDate[2]);
	                		 
	               		 	 cal.set(Calendar.MONTH, month - 1);
	               		 	 String nextMonth = sdf1.format(cal.getTime());
	               		 	 
	               		 	 cal.set(Calendar.MONTH, month);
	               		 	 String nextstartMonth = sdf1.format(cal.getTime());
	               		 	 
	                   		 cal.set(Calendar.DATE, day);
	                   		 cal.set(Calendar.MONTH, month - 3);
	                   		 cal.set(Calendar.YEAR, addfiveYear + 5);
	             		 	 String fiveYear = sdf1.format(cal.getTime());
	                		
	                    	String InvRef = session.getAttribute("invoice_number").toString();
	                    	String InvDes = "1 x "+session.getAttribute("plan_name").toString();
	                    	String CCName = session.getAttribute("EWAY_CARDNAME").toString();
	                    	String CCNumber = session.getAttribute("EWAY_CARDNUMBER").toString();
	                    	String CCMM = session.getAttribute("EWAY_CARDEXPIRYMONTH").toString();
	                    	String CCYY = session.getAttribute("EWAY_CARDEXPIRYYEAR").toString();
	                    	String TotAmt = session.getAttribute("TotalAmountforRebiiling").toString();
	                    	
	                    	SOAPMessage soapResponseCreateRebillEvent = soapConnection.call(soap.createSOAPCreateRebillEventRequest(RebillCustomerID, InvRef, InvDes, CCName, CCNumber, CCMM, CCYY, TotAmt, nextMonth, nextstartMonth, fiveYear), url);

	                        // Process the SOAP Response
	                        soap.printSOAPResponse(soapResponseCreateRebillEvent);
	                        
	                        String RebillID = soapResponseCreateRebillEvent.getSOAPBody().getElementsByTagName("RebillID").item(0).getFirstChild().getNodeValue(); 
		                    System.out.print("\nRebillID "+RebillID);
		                    
		                    CancelSubscription cs = new CancelSubscription();
		                    cs.setRebill_customer_id(Long.parseLong(RebillCustomerID));
		                    cs.setRebill_id(Long.parseLong(RebillID));
		                    cs.setOrder_date(new Date());
		                    cs.setCanceled_date(null);
		                    cs.setReason(null);
		                    cs.setCorp_id(corp.getId());
		                    cs.setCompany(corp.getCompanyId());
		                    cs.setUserid(u.getId());
		                    cs.setCreated(new Date());
		                	lm.saveCancelSubscriptionDetails(cs);
	                    }

	                    soapConnection.close();
	                } catch (Exception e) {
	                    System.err.println("Error occurred while sending SOAP Request to Server");
	                    e.printStackTrace();
	                }
	            	}
	            }
	       
 	}
	
	if(session.getAttribute("eWayTokenCode") !=null){
		String Token = session.getAttribute("ResponceTokenText").toString();
// 		System.out.println("token"+Token);
		   
			String[] tra = Token.split(",");
			String[] tra1 = tra[3].split(":");
			
			String[] cid = Token.split(",");
			String[] cid1 = cid[19].split(":");
			
			String[] inv = Token.split(",");
			String[] inv1 = inv[39].split(":");
			
			String[] tamt = Token.split(",");
			String[] tamt1 = tamt[38].split(":");
			
			String[] trast = Token.split(",");
			String[] trast1 = trast[4].split(":");
			
			String Trans_id = tra1[1].replace("\"","");
			String Token_CusID = cid1[1].replace("\"","");
			String Inv_no = inv1[1].replace("\"","");
			String Tot_tmt = tamt1[2].replace("\"","");
			String traStatus = trast1[1].replace("\"","");
			
		   
		   double totamt = Double.parseDouble(Tot_tmt);
		   double t_amt = (totamt / 100);
		   java.math.BigDecimal totalamt = new java.math.BigDecimal(t_amt);
		   boolean transStatus = Boolean.parseBoolean(traStatus);
		   
		    PaymentHistory ph;
	        	
	        	ph = new PaymentHistory();
	        	ph.setUserid(u.getId());
	        	ph.setCorp_id(corp.getId());
	        	ph.setCompany(corp.getCompanyId());
	        	ph.setAccess_code(Token_CusID);
	        	ph.setResponse(Token_CusID);
	        	ph.setInvoice_number(Inv_no);
	        	ph.setTotal_amount(totalamt);
	        	ph.setTrans_id(Long.parseLong(Trans_id));
	        	ph.setTrans_status(transStatus);
	        	ph.setCreated(new Date());
	        	lm.savePaymentHistoryDetails(ph);
	        	session.setAttribute("eWayTokenCode", null);
	        	session.setAttribute("ResponceTokenText", null);

				SimpleDateFormat sdf = new SimpleDateFormat("d MMM, yyyy hh:mm a");

				String no = String.valueOf(ph.getId());
	            String orderdate = String.valueOf(sdf.format(ph.getCreated()));
			    String transactionno = String.valueOf(ph.getTrans_id());

				String planename = session.getAttribute("plan_name").toString();
	            String price = session.getAttribute("price").toString();
			    String tax = session.getAttribute("tax").toString();
			    String total = session.getAttribute("totalamt").toString();

	            Properties props = new Properties();
	            props.put("no", no);
	            props.put("orderdate", orderdate);
	            props.put("billedto", u.getFirstName()+" "+u.getLastName());
	            props.put("transactionno", transactionno);
	            props.put("company", corp.getName());
	            props.put("address1", corp.getAddress1());
	            props.put("address2", corp.getAddress2());
	            props.put("city", corp.getSuburb());
	            props.put("state", corp.getState());
	            props.put("pincode", corp.getPostcode());
	            props.put("country", corp.getCountryName());
	            props.put("itemname", planename);
	            props.put("price", price);
	            props.put("tax", tax);
	            props.put("total", total);
	            MailMessage msg = new MailMessage(props, "new_order.vm", "shagul001@gmail.com","New Order "+ph.getId()+"",false);

	            try {
	                msg.send();
	            }
	            catch (Exception e) {
//	                e.printStackTrace();
//	                throw e;
	            }
	            
	            if(session.getAttribute("savedpromocode") != null){
	            	
	            String promocode = session.getAttribute("savedpromocode").toString();
	            
	            Connection conn = Util.getConnection();
	            PreparedStatement pst = conn.prepareStatement("update promotion set company = ?, accepted = current_timestamp where promo_code = ?");
	            pst.setLong(1, corp.getCompanyId());
	            pst.setString(2, promocode);
	            pst.executeUpdate();
	            pst.close();
	            conn.close();
	            
	            }
	       
	}


    String[][] breadcrumbs = {{"/corp/", corp.getName()}, {"/corp/subscription", "Subscription"}, {null, "Payment History"}};

%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head><title>Payment History</title>
</head>
<body>
<div class="page-header">
    <h1>Payment History</h1>
</div>
<%@include file="/WEB-INF/includes/breadcrumbs.jsp"%>
<div class="span8 well">
    <form class="form-horizontal" method="post" action="card_details.jsp">
    <%
    Connection conn = Util.getConnection();
    PreparedStatement pst;
    pst = conn.prepareStatement("select p.plan_name,ph.total_amount,ph.trans_id,ph.invoice_number,ph.trans_status,to_char(ph.created, 'DD-Mon-YYYY at HH24:MI') as created from payment_history ph join plan_price p on p.invoice_number = ph.invoice_number where ph.corp_id = ?");
    pst.setLong(1,corp.getId());
    ResultSet rs1 = pst.executeQuery();
    
    pst = conn.prepareStatement("select p.plan_name,ph.total_amount,ph.trans_id,ph.invoice_number,ph.trans_status,to_char(ph.created, 'DD-Mon-YYYY at HH24:MI') as created from payment_history ph join plan_price_history p on p.invoice_number = ph.invoice_number where ph.corp_id = ?");
    pst.setLong(1,corp.getId());
    ResultSet rs3 = pst.executeQuery();
    
    if(rs1.next() == false && rs3.next() == false){ %>
    	<p style="color: darkred; text-align: center; font-size: large;">There is no transaction (or) payment history here.</p>
    <%}else{%>
    <table class="table">
    <tr>
    <th>Plan Name</th>
    <th>Total Amount</th>
    <th>Transaction ID</th>
    <th>Invoice No.</th>
    <th>Order Date</th>
    <th>Status</th>
    </tr>
<%
	            pst = conn.prepareStatement("select p.plan_name,ph.total_amount,ph.trans_id,ph.invoice_number,ph.trans_status,to_char(ph.created, 'DD-Mon-YYYY at HH24:MI') as created from payment_history ph join plan_price p on p.invoice_number = ph.invoice_number where ph.corp_id = ? order by ph.id DESC");
	            pst.setLong(1,corp.getId());
	            ResultSet rs = pst.executeQuery();
	            while(rs.next()){ 
		 		   boolean transStatus = rs.getBoolean("trans_status");
		 		 
	            %>
              	
    <tr>
    <td style="color: darkred;"><%=rs.getString("plan_name")%></td>
    <td>$<%=rs.getString("total_amount")%></td>
    <td><%=rs.getString("trans_id")%></td>
    <td><%=rs.getString("invoice_number")%></td>
    <td><%=rs.getString("created")%></td>
    <%if(transStatus == true){%><td style="color: green;">Success<%}else{ %><td style="color: red;">Failed<%} %></td>
    </tr>
    <%}%>
<%
	            pst = conn.prepareStatement("select p.plan_name,ph.total_amount,ph.trans_id,ph.invoice_number,ph.trans_status,to_char(ph.created, 'DD-Mon-YYYY at HH24:MI') as created from payment_history ph join plan_price_history p on p.invoice_number = ph.invoice_number where ph.corp_id = ? order by ph.id DESC");
	            pst.setLong(1,corp.getId());
	            ResultSet rs2 = pst.executeQuery();
	            while(rs2.next()){ 
		 		   boolean transStatus = rs2.getBoolean("trans_status");
		 		 
	            %>
              	
    <tr>
    <td style="color: darkred;"><%=rs2.getString("plan_name")%></td>
    <td>$<%=rs2.getString("total_amount")%></td>
    <td><%=rs2.getString("trans_id")%></td>
    <td><%=rs2.getString("invoice_number")%></td>
    <td><%=rs2.getString("created")%></td>
    <%if(transStatus == true){%><td style="color: green;">Success<%}else{ %><td style="color: red;">Failed<%} %></td>
    </tr>
    <%}%>
    </table>
    <%} pst.close();conn.close(); %>
	</form>
	</div>
</body>
</html>
</html>