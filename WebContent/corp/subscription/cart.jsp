<%@page import="sun.net.www.http.Hurryable"%>
<%@page import="java.math.BigDecimal"%>
<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%@ page import="au.com.ourbodycorp.model.managers.CorporationManager" %>
<%@ page import="java.util.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.util.Date" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="au.com.ourbodycorp.model.managers.CountryManager" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");

	if (cu == null) {
	    response.sendRedirect(Util.loginUrl(Constants.LoginReason.login, request));
	    return;
	}
    Corporation corp = cu.getCorporation();
    User u = cu.getUser();
    /* try{
    	Util.allActivitiesLogs("Subscription", u.getId(), corp.getName(), "View Cart Details");
    }catch(Exception e){
    	
    } */
    
    if(session.getAttribute("pmcode") == null){
    	session.setAttribute("discountError", null);
    	session.setAttribute("Cart_Error", "Error");
    	session.setAttribute("discount", null);
    	session.setAttribute("promoError", null);
    }else{
    	session.setAttribute("discountError", "Error");
    	session.setAttribute("promoError", "Error");
    }
    	if (session.getAttribute("pmcode") != null) {
          	Connection conn2 = Util.getConnection();
      		PreparedStatement pst2;
            pst2 = conn2.prepareStatement("select expires, discount, company from promotion where promo_code = ?");
//             pst2.setString(1, u.getEmail());
            pst2.setString(1, session.getAttribute("pmcode").toString());
            ResultSet rs2 = pst2.executeQuery();
            if(rs2.next()){ 
            	if(rs2.getLong("company") > 0){
            		session.setAttribute("Used", "Used");
            		session.setAttribute("Expired", null);
            		session.setAttribute("discount", null);
            	} else if(rs2.getDate("expires").before(new Date())){
            		session.setAttribute("Expired", "Expired");
            		session.setAttribute("discount", null);
            		session.setAttribute("Used", null);
            	} else{
            		session.setAttribute("discount", rs2.getLong("discount"));
            		session.setAttribute("Expired", null);
            		session.setAttribute("Used", null);
            	}
            	session.setAttribute("discountError", null);
            }else{
            	session.setAttribute("discount", null);
            	session.setAttribute("pmcode", session.getAttribute("pmcode"));
            }
            
            pst2.close();
            conn2.close();
            session.setAttribute("Cart_Error", "No_Error");

        }
    
    LibraryManager lm = new LibraryManager();
    	
   		Connection conn = Util.getConnection();
		PreparedStatement pst;
        pst = conn.prepareStatement("select plan,type from cart_details where corp_id = ?");
        pst.setLong(1, corp.getId());
        ResultSet rs = pst.executeQuery();
        if(rs.next()){
        	session.setAttribute("Cart_Plan", rs.getLong("plan"));
        	session.setAttribute("Cart_Type", rs.getString("type"));
        	session.setAttribute("plan", rs.getLong("plan"));
       	    session.setAttribute("type", rs.getString("type"));
        		if (request.getMethod().equalsIgnoreCase("post")) {
        			session.setAttribute("Cart_Error", "No_Error");
        		}if(session.getAttribute("cart") != null){
        			session.setAttribute("Cart_Error", "No_Error");
        		}
        	
        }else{
	        	session.setAttribute("plan", null);
	       	    session.setAttribute("type", null);
	        	session.setAttribute("Cart_Error", "No_Error");
        		if(request.getParameter("type") != null){
        			session.setAttribute("Cart_Plan", request.getParameter("plan"));
            		session.setAttribute("Cart_Type", request.getParameter("type"));
            		session.setAttribute("promoError", null);
        		}else{
        			session.setAttribute("Cart_Plan", "");
        			session.setAttribute("Cart_Type", "");
        		}
        	if(session.getAttribute("plan") == null){
            	if(request.getParameter("plan") != null){
            		session.setAttribute("promoError", null);
		        	CartDetails cd = new CartDetails();
		        	cd.setPlan(Long.parseLong(request.getParameter("plan")));
		        	cd.setType(request.getParameter("type"));
		        	cd.setCorp_id(corp.getId());
		        	cd.setCompany(corp.getCompanyId());
		        	cd.setUserid(u.getId());
		        	cd.setCreated(new Date());
		        	lm.saveCartDetails(cd);
		        	session.setAttribute("plan", request.getParameter("plan"));
		       	 	session.setAttribute("type", request.getParameter("type"));
            	}else{
            		session.setAttribute("Cart_Error", "Error");
            		session.setAttribute("Cart_Plan", "");
            	}
        	}
        }
        pst.close();
		conn.close();
	
		if(request.getParameter("cart") != null){
			session.setAttribute("Cart_Error", "No_Error");
   	 	}
		
		String[][] breadcrumbs;
    if(session.getAttribute("Cart_Type").toString().equals("sms")){
    	breadcrumbs = new String[][] {{"/corp/", corp.getName()}, {"/corp/subscription/", "Subscription"}, {"/corp/subscription/pricing_sms.jsp", "Pricing"}, {null, "Cart"}};
    }else{
    	breadcrumbs = new String[][] {{"/corp/", corp.getName()}, {"/corp/subscription/", "Subscription"}, {"/corp/subscription/pricing.jsp", "Pricing"}, {null, "Cart"}};
    }
    
    session.setAttribute("CreateAccessCode", "CreateAccessCode");
%>
<html>
<head><title>Cart</title>
<style type="text/css">

#shopp #cart {
    margin: 0 0 15px;
}
#shopp #cart {
    width: 100%;
}
#shopp div.description, #shopp .products, #shopp .products ul, #shopp .products li.row, #shopp .category, #shopp #cart, #shopp #cart table {
    overflow: hidden;
}
#shopp form {
    text-align: left;
}
form {
    margin: 0;
}
*::-moz-selection {
    background: #2d8bd8 none repeat scroll 0 0;
    color: #fff;
    text-shadow: none;
}
#shopp {
    font-size: 14px;
}
.entry {
    font-size: 16px;
    line-height: 19px;
}

#hidden-update {
    left: -999em;
    position: absolute;
}
.btn-click, #content .btn-click, input[type="submit"], input[type="reset"], input[type="button"], button, .txt-area .entry a.btn-click {
    background: #db6e00 none repeat scroll 0 0;
    border: 1px solid #000;
    border-radius: 5px;
    color: #fff;
    display: inline-block;
    font-size: 18px;
    height: 40px;
    line-height: 40px;
    padding: 0 24px;
    position: relative;
    text-decoration: none;
    vertical-align: top;
}
input:valid, textarea:valid {
}
button, input[type="button"], input[type="reset"], input[type="submit"] {
    cursor: pointer;
}
button, input {
    line-height: normal;
}
button, input, select, textarea {
    font-size: 100%;
    margin: 0;
    outline: medium none;
    vertical-align: baseline;
}

#shopp #cart table {
    padding: 10px 0;
    width: 100%;
}
#shopp div.description, #shopp .products, #shopp .products ul, #shopp .products li.row, #shopp .category, #shopp #cart, #shopp #cart table {
    overflow: hidden;
}
#shopp form table, #shopp form table td {
    border: medium none;
}
table {
    border-collapse: collapse;
    border-spacing: 0;
}
*::-moz-selection {
    background: #2d8bd8 none repeat scroll 0 0;
    color: #fff;
    text-shadow: none;
}
#shopp form {
    text-align: left;
}
#shopp #cart .item {
    width: 50%;
}
#shopp #cart th, #shopp #cart td {
    padding: 3px;
}
#shopp #cart th {
    font-weight: bold;
    text-align: left;
}

#shopp #cart .money, #shopp #cart .totals th, #shopp #cart .buttons td {
    text-align: right;
    white-space: nowrap;
}

#shopp #cart .remove {
    font-size: 13px;
}
#shopp .remove {
    display: block;
    font-size: 14px;
    line-height: 24px;
    margin: 5px 0 0;
    padding: 5px 10px;
}

#shopp .update-button {
    display: block;
    float: right;
    font-size: 14px;
    line-height: 24px;
    margin: 0 0 10px;
    padding: 5px 10px;
}
#shopp #cart table {
    padding: 10px 0;
    width: 100%;
}
#shopp {
    position: relative;
}
.btn-click:hover, .btn-login:hover, input[type="submit"]:hover, input[type="reset"]:hover, input[type="button"]:hover, button:hover, #content .btn-click:hover, .txt-area .entry a.btn-click:hover {
    background: #ed9944 none repeat scroll 0 0;
    text-decoration: none;
    color: #fff;
}
#shopp #cart .money, #shopp #cart .totals th, #shopp #cart .buttons td {
    text-align: right;
    white-space: nowrap;
}

#shopp #cart .totals.total th, #shopp #cart .totals.total td {
    font-size: 130%;
}
#shopp #cart .money, #shopp #cart .totals th, #shopp #cart .buttons td {
    text-align: right;
    white-space: nowrap;
}
#shopp #cart th, #shopp #cart td {
    padding: 3px;
}
#shopp #cart th {
    font-weight: bold;
    text-align: left;
}
</style>
<script type="text/javascript">
function promotioncheckvalid() {
	var procode = $("#promocode").val();
	window.location.replace("promotion_check_valid.jsp?promocode="+procode+"");
}
function removecart() {
	var type = $("#type").val();
	window.location.replace("delete_cart.jsp?type="+type+"");
}
function removesmscart() {
	var type = $("#type").val();
	window.location.replace("delete_cart.jsp?type="+type+"");
}
function checkoutcart(id) {
	var type = $("#type").val();
	window.location.replace("/corp/subscription/checkout.jsp?plan="+id+"&type="+type+"");
}
</script>
</head>
<body>


  <div class="page-header">
    <h1>Cart</h1>
  </div>
  <%@include file="/WEB-INF/includes/breadcrumbs.jsp"%>
  <div class="span8 well">
  <form action="cart.jsp" method="post" class="form-horizontal">
  <input type="hidden" id="type" name="type" value="<%=session.getAttribute("type")%>"/>
  <%if(session.getAttribute("Cart_Plan").toString().equals("")){ %>
  <div id="noItem" class="grid cart">
	<p class="warning" style="font-size: 20px; color: #db6e00;">There are currently no items in your shopping cart.</p>
	  <%if(session.getAttribute("promoError") != null){ %>
	  <p class="warning" style="font-size: 20px; margin: 25px 0 0; color: db6e00;">Please choose your plan first and apply your promo code.</p>
	  <%}%>
  </div>
  <%}else if(session.getAttribute("Cart_Type").toString().equals("sms")){ %>
  <%if(session.getAttribute("Cart_Error").toString().equals("Error")){ %>
  <h2 style="color: #db6e00;">Error</h2>
	<p style="color: darkred; margin: 20px 0 10px 4px;">A subscription must be purchased separately. Complete your current transaction and try again.</p>
  <%} %>
  <div id="shopp" class="grid cart">
  <div id="cart">
<%
              	Connection conn1 = Util.getConnection();
	      		PreparedStatement pst1;
	            pst1 = conn1.prepareStatement("select id,plan_name,cents_per_sms,currency_type,tax,price,invoice_number from plan_price where id = ?");
	            pst1.setLong(1, Long.parseLong(session.getAttribute("Cart_Plan").toString()));
	            ResultSet rs1 = pst1.executeQuery();
	            while(rs1.next()){ %>
<table class="cart">
<tbody>
<tr>
<th class="item" scope="col">Cart Items</th>
<th class="money" scope="col">Item Price</th>
<th class="money" scope="col">Item Total</th>
</tr>
<tr>
<td>
<h2 style="color: #db6e00;"><%=rs1.getString("plan_name")%></h2>
<%session.setAttribute("plan_name", rs1.getString("plan_name"));
  session.setAttribute("invoice_number", rs1.getString("invoice_number"));%>
<p style="margin: 20px 0 10px;"><%=rs1.getString("cents_per_sms")%> cents per sms</p>
<input id="remove" type="button" onclick="removesmscart();" value="Remove" name="remove" class="remove">
</td>
<td class="money">
<%	
	java.math.BigDecimal price = rs1.getBigDecimal("price");
	
	if(session.getAttribute("discount") != null){
		java.math.BigDecimal discount = new BigDecimal(session.getAttribute("discount").toString());
		java.math.BigDecimal pricePercentage = price.multiply(discount);
		java.math.BigDecimal priceDivHundred = pricePercentage.divide(new BigDecimal("100"));
		price = price.subtract(priceDivHundred);
	}else{
		price = rs1.getBigDecimal("price");
	}
	
%>
					<%if(rs1.getString("currency_type").equals("AUD")){%>
              			$<%=price%>
              		<%}else if(rs1.getString("currency_type").equals("USD")){%>
              			$<%=price%>
              		<%}else if(rs1.getString("currency_type").equals("Euro")){%>
              			$<%=price%>
              		<%}else if(rs1.getString("currency_type").equals("Pounds")){%>
              			$<%=price%>
              		<%}%></td>
<td class="money">
<%	
	java.math.BigDecimal price1 = rs1.getBigDecimal("price");
	
	if(session.getAttribute("discount") != null){
		java.math.BigDecimal discount = new BigDecimal(session.getAttribute("discount").toString());
		java.math.BigDecimal pricePercentage = price1.multiply(discount);
		java.math.BigDecimal priceDivHundred = pricePercentage.divide(new BigDecimal("100"));
		price1 = price1.subtract(priceDivHundred);
	}else{
		price1 = rs1.getBigDecimal("price");
	}
	
%>
					<%if(rs1.getString("currency_type").equals("AUD")){%>
              			$<%=price%>
              		<%}else if(rs1.getString("currency_type").equals("USD")){%>
              			$<%=price%>
              		<%}else if(rs1.getString("currency_type").equals("Euro")){%>
              			$<%=price1%>
              		<%}else if(rs1.getString("currency_type").equals("Pounds")){%>
              			$<%=price1%>
              		<%}%></td>
</tr>
<tr class="totals">
<td>
<%if(session.getAttribute("discountError") != null && session.getAttribute("pmcode") != ""){ %>
<p style="color: darkred;"><%=session.getAttribute("pmcode")%> is not a valid code.</p>
<%} %>
<%if(session.getAttribute("Expired") != null){ %>
<p style="color: darkred; font-weight: bold;">This promo code has expired.</p>
<%} %>
<%if(session.getAttribute("Used") != null){ %>
<p style="color: darkred; font-weight: bold;">This promo code already used.</p>
<%} %>
<%if(session.getAttribute("discount") != null){ %>
<p style="color: darkgreen; font-weight: bold;">Congratulations! <%=session.getAttribute("discount")%>% Discount Promo Code Applied.</p>
<%} %>
<!-- <span  style="float: left;">
<input id="promocode" style="width: 100px; margin: 5px 10px 0 0;" type="text" value="<%if(session.getAttribute("pmcode") != null){%><%=session.getAttribute("pmcode")%><%}%>" size="20" maxlength="20" name="promocode">
</span>
<span>
<input id="apply-code" type="button" onclick="promotioncheckvalid();" value="Apply Promo Code" name="update">
</span> -->
<%if(session.getAttribute("pmcode") == ""){ %>
<p style="color: darkred; margin: 10px 0 0;">Please enter a promo code.</p>
<%} %>
</td>
<th scope="row">Subtotal</th>
<td class="money">
<span class="shopp-cart cart-subtotal">
<%	
	java.math.BigDecimal price2 = rs1.getBigDecimal("price");

	if(session.getAttribute("discount") != null){
		java.math.BigDecimal discount = new BigDecimal(session.getAttribute("discount").toString());
		java.math.BigDecimal pricePercentage = price2.multiply(discount);
		java.math.BigDecimal priceDivHundred = pricePercentage.divide(new BigDecimal("100"));
		price2 = price2.subtract(priceDivHundred);
	}else{
		price2 = rs1.getBigDecimal("price");
	}
	
%>
					<%if(rs1.getString("currency_type").equals("AUD")){%>
              			$<%=price2%>
              		<%}else if(rs1.getString("currency_type").equals("USD")){%>
              			$<%=price2%>
              		<%}else if(rs1.getString("currency_type").equals("Euro")){%>
              			$<%=price2%>
              		<%}else if(rs1.getString("currency_type").equals("Pounds")){%>
              			$<%=price2%>
              		<%}%></span>
</td>
</tr>
<tr class="totals">
<td></td>
<th scope="row">Tax</th>
<td class="money">
<span class="shopp-cart cart-tax">
					<%if(rs1.getString("currency_type").equals("AUD")){%>
              			$<%=rs1.getBigDecimal("tax")%>
              		<%}else if(rs1.getString("currency_type").equals("USD")){%>
              			$<%=rs1.getBigDecimal("tax")%>
              		<%}else if(rs1.getString("currency_type").equals("Euro")){%>
              			$<%=rs1.getBigDecimal("tax")%>
              		<%}else if(rs1.getString("currency_type").equals("Pounds")){%>
              			$<%=rs1.getBigDecimal("tax")%>
              		<%}%></span>
</td>
</tr>
<tr class="totals total">
<td></td>
<th scope="row" style="font-size: 18px;">Total</th>
<td class="money">
<span class="shopp-cart cart-total">
<%	
	java.math.BigDecimal tax = rs1.getBigDecimal("tax");
	java.math.BigDecimal price3 = rs1.getBigDecimal("price");
	java.math.BigDecimal total = new BigDecimal("0.00");

	if(session.getAttribute("discount") != null){
		java.math.BigDecimal discount = new BigDecimal(session.getAttribute("discount").toString());
		java.math.BigDecimal pricePercentage = price3.multiply(discount);
		java.math.BigDecimal priceDivHundred = pricePercentage.divide(new BigDecimal("100"));
		price3 = price3.subtract(priceDivHundred);
	}else{
		price3 = rs1.getBigDecimal("price");
	}
	
	total = tax.add(price3);
	session.setAttribute("totalamt", total);
	session.setAttribute("tax", tax);
	session.setAttribute("price", price3);
%>					
					<%if(rs1.getString("currency_type").equals("AUD")){%>
              			$<%=total%>
              		<%}else if(rs1.getString("currency_type").equals("USD")){%>
              			$<%=total%>
              		<%}else if(rs1.getString("currency_type").equals("Euro")){%>
              			$<%=total%>
              		<%}else if(rs1.getString("currency_type").equals("Pounds")){%>
              			$<%=total%>
              		<%}%></span>
</td>
</tr>
</tbody>
</table>
<a style="float: right; margin: 10px 0 0; cursor: pointer;" class="right btn-click" onclick="checkoutcart(<%=rs1.getString("id")%>);" >Proceed to Checkout »</a>
<%} pst1.close(); conn1.close(); %>
</div>
</div>
<%} else if(session.getAttribute("Cart_Type").toString().equals("upgrade")){ %>
  <%if(session.getAttribute("Cart_Error").toString().equals("Error")){ %>
  <h2 style="color: #db6e00;">Error</h2>
	<p style="color: darkred; margin: 20px 0 10px 4px;">A subscription must be purchased separately. Complete your current transaction and try again.</p>
  <%} %>
  <div id="shopp" class="grid cart">
  <div id="cart">
<%
              	Connection conn1 = Util.getConnection();
	      		PreparedStatement pst1;
	            pst1 = conn1.prepareStatement("select id,plan_name,currency_type,tax,price,invoice_number from plan_price where id = ?");
	            pst1.setLong(1, Long.parseLong(session.getAttribute("Cart_Plan").toString()));
	            ResultSet rs1 = pst1.executeQuery();
	            while(rs1.next()){ %>
<table class="cart">
<tbody>
<tr>
<th class="item" scope="col">Cart Items</th>
<th class="money" scope="col">Item Price</th>
<th class="money" scope="col">Item Total</th>
</tr>
<tr>
<td>
<h2 style="color: #db6e00;"><%=rs1.getString("plan_name")%></h2>
<%session.setAttribute("plan_name", rs1.getString("plan_name"));
  session.setAttribute("invoice_number", rs1.getString("invoice_number"));%>
<p style="margin: 10px 0px;">Subscription fee will be debited automatically every month</p>
<input id="remove" type="button" onclick="removecart();" value="Remove" name="remove" class="remove">
</td>
<td class="money">
<%	
	java.math.BigDecimal price4 = rs1.getBigDecimal("price");

	if(session.getAttribute("discount") != null){
		java.math.BigDecimal discount = new BigDecimal(session.getAttribute("discount").toString());
		java.math.BigDecimal pricePercentage = price4.multiply(discount);
		java.math.BigDecimal priceDivHundred = pricePercentage.divide(new BigDecimal("100"));
		price4 = price4.subtract(priceDivHundred);
	}else{
		price4 = rs1.getBigDecimal("price");
	}
	
%>
					<%if(rs1.getString("currency_type").equals("AUD")){%>
              			$<%=price4%>
              		<%}else if(rs1.getString("currency_type").equals("USD")){%>
              			$<%=price4%>
              		<%}else if(rs1.getString("currency_type").equals("Euro")){%>
              			$<%=price4%>
              		<%}else if(rs1.getString("currency_type").equals("Pounds")){%>
              			$<%=price4%>
              		<%}%></td>
<td class="money">
<%	
	java.math.BigDecimal price5 = rs1.getBigDecimal("price");
	
	if(session.getAttribute("discount") != null){
		java.math.BigDecimal discount = new BigDecimal(session.getAttribute("discount").toString());
		java.math.BigDecimal pricePercentage = price5.multiply(discount);
		java.math.BigDecimal priceDivHundred = pricePercentage.divide(new BigDecimal("100"));
		price5 = price5.subtract(priceDivHundred);
	}else{
		price5 = rs1.getBigDecimal("price");
	}
	
%>
					<%if(rs1.getString("currency_type").equals("AUD")){%>
              			$<%=price5%>
              		<%}else if(rs1.getString("currency_type").equals("USD")){%>
              			$<%=price5%>
              		<%}else if(rs1.getString("currency_type").equals("Euro")){%>
              			$<%=price5%>
              		<%}else if(rs1.getString("currency_type").equals("Pounds")){%>
              			$<%=price5%>
              		<%}%></td>
</tr>
<tr>
<td height="15" colspan="3"></td>
</tr>
<tr>
<td class="money" colspan="3">
</td>
</tr>
<tr class="totals">
<td>
<%if(session.getAttribute("discountError") != null && session.getAttribute("pmcode") != ""){ %>
<p style="color: darkred;"><%=session.getAttribute("pmcode")%> is not a valid code.</p>
<%} %>
<%if(session.getAttribute("Expired") != null){ %>
<p style="color: darkred; font-weight: bold;">This promo code has expired.</p>
<%} %>
<%if(session.getAttribute("Used") != null){ %>
<p style="color: darkred; font-weight: bold;">This promo code already used.</p>
<%} %>
<%if(session.getAttribute("discount") != null){ %>
<p style="color: darkgreen; font-weight: bold;">Congratulations! <%=session.getAttribute("discount")%>% Discount Promo Code Applied.</p>
<%} %>
<span  style="float: left;">
<input id="promocode" style="width: 100px; margin: 5px 10px 0 0;" value="<%if(session.getAttribute("pmcode") != null){%><%=session.getAttribute("pmcode")%><%}%>" type="text" size="20" maxlength="20" name="promocode">
</span>
<span>
<input id="apply-code" type="button" onclick="promotioncheckvalid();" value="Apply Promo Code" name="update">
</span>
<%if(session.getAttribute("pmcode") == ""){ %>
<p style="color: darkred; margin: 10px 0 0;">Please enter a promo code.</p>
<%} %>
</td>
<th scope="row">Subtotal</th>
<td class="money">
<span class="shopp-cart cart-subtotal">
<%
	java.math.BigDecimal price6 = rs1.getBigDecimal("price");

	if(session.getAttribute("discount") != null){
		java.math.BigDecimal discount = new BigDecimal(session.getAttribute("discount").toString());
		java.math.BigDecimal pricePercentage = price6.multiply(discount);
		java.math.BigDecimal priceDivHundred = pricePercentage.divide(new BigDecimal("100"));
		price6 = price6.subtract(priceDivHundred);
	}else{
		price6 = rs1.getBigDecimal("price");
	}
	
%>
					<%if(rs1.getString("currency_type").equals("AUD")){%>
              			$<%=price6%>
              		<%}else if(rs1.getString("currency_type").equals("USD")){%>
              			$<%=price6%>
              		<%}else if(rs1.getString("currency_type").equals("Euro")){%>
              			$<%=price6%>
              		<%}else if(rs1.getString("currency_type").equals("Pounds")){%>
              			$<%=price6%>
              		<%}%></span>
</td>
</tr>
<tr class="totals">
<td></td>
<th scope="row">Tax</th>
<td class="money">
<span class="shopp-cart cart-tax">
					<%if(rs1.getString("currency_type").equals("AUD")){%>
              			$<%=rs1.getBigDecimal("tax")%>
              		<%}else if(rs1.getString("currency_type").equals("USD")){%>
              			$<%=rs1.getBigDecimal("tax")%>
              		<%}else if(rs1.getString("currency_type").equals("Euro")){%>
              			$<%=rs1.getBigDecimal("tax")%>
              		<%}else if(rs1.getString("currency_type").equals("Pounds")){%>
              			$<%=rs1.getBigDecimal("tax")%>
              		<%}%></span>
</td>
</tr>
<tr class="totals total">
<td></td>
<th scope="row" style="font-size: 18px;">Total</th>
<td class="money">
<span class="shopp-cart cart-total">
<%	
	java.math.BigDecimal tax1 = rs1.getBigDecimal("tax");
	java.math.BigDecimal price7 = rs1.getBigDecimal("price");
	java.math.BigDecimal total1 = new BigDecimal("0.00");

	if(session.getAttribute("discount") != null){
		java.math.BigDecimal discount = new BigDecimal(session.getAttribute("discount").toString());
		java.math.BigDecimal pricePercentage = price7.multiply(discount);
		java.math.BigDecimal priceDivHundred = pricePercentage.divide(new BigDecimal("100"));
		price7 = price7.subtract(priceDivHundred);
	}else{
		price7 = rs1.getBigDecimal("price");
	}
	
	total1 = tax1.add(price7);
	session.setAttribute("totalamt", total1);
	session.setAttribute("tax", tax1);
	session.setAttribute("price", price7);
%>					
					<%if(rs1.getString("currency_type").equals("AUD")){%>
              			$<%=total1%>
              		<%}else if(rs1.getString("currency_type").equals("USD")){%>
              			$<%=total1%>
              		<%}else if(rs1.getString("currency_type").equals("Euro")){%>
              			$<%=total1%>
              		<%}else if(rs1.getString("currency_type").equals("Pounds")){%>
              			$<%=total1%>
              		<%}%></span>
</td>
</tr>
</tbody>
</table>
<a style="float: right; margin: 10px 0 0; cursor: pointer;" class="right btn-click" onclick="checkoutcart(<%=rs1.getString("id")%>);" >Proceed to Checkout »</a>
<%} pst1.close(); conn1.close();%>
</div>
</div>
<%}else{ %>
	<div id="noItem" class="grid cart">
		<p class="warning" style="font-size: 20px; color: darkred;">There are currently no items in your shopping cart.</p>
  	</div>
<%} %>
<%if(session.getAttribute("discount") != null){ session.setAttribute("savedpromocode", session.getAttribute("pmcode"));}else{session.setAttribute("savedpromocode", null);} 
  session.setAttribute("pmcode", null); %>
</form>
    </div>
</body>
</html>