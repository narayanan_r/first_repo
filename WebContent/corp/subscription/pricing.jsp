<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%@ page import="au.com.ourbodycorp.model.managers.CorporationManager" %>
<%@ page import="java.util.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="au.com.ourbodycorp.model.managers.CountryManager" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");

	if (cu == null) {
	    response.sendRedirect(Util.loginUrl(Constants.LoginReason.login, request));
	    return;
	}
    Corporation corp = cu.getCorporation();
    User u = cu.getUser();
   /*  try{
    	Util.allActivitiesLogs("Pricing", u.getId(), corp.getName(), "Pricing Details");
    }catch(Exception e){
    	
    } */
    
    String[][] breadcrumbs = {{"/corp/", corp.getName()}, {"/corp/subscription/", "Subscription"}, {null, "Pricing"}};

%>
<html>
<head><title>Pricing</title>
<style type="text/css">
.info-data {
    padding: 0 0 25px;
}
/* .info-data table { */
/*     width: 772px; */
/* } */
.info-data table form {
    padding: 15px 0 0;
}
.info-data td {
    padding: 0 5px;
    text-align: center;
    vertical-align: middle;
}
.info-data td.col01 {
    width: 152px;
}
.info-data td.col02 {
    width: 172px;
}
.info-data td.col03 {
    width: 146px;
}
.info-data td.col04 {
    width: 147px;
}
.info-data .row01 td {
    color: #fff;
    font-size: 20px;
    font-weight: bold;
    height: 90px;
    line-height: 25px;
}
.info-data .row01 .col01 {
    background: #db9700 none repeat scroll 0 0;
    color: #000;
    font-size: 33px;
    font-weight: bold;
    line-height: 36px;
}
.info-data .row01 .col02 {
    background: #db9100 none repeat scroll 0 0;
    border-bottom-color: #000;
    border-right-color: #000;
    border-top-color: #000;
}
.info-data .row01 .col03 {
    background: #db8700 none repeat scroll 0 0;
    border-bottom-color: #000;
    border-right-color: #000;
    border-top-color: #000;
}
.info-data .row01 .col04 {
    background: #db7a00 none repeat scroll 0 0;
    border-bottom-color: #000;
    border-right-color: #000;
    border-top-color: #000;
}
.info-data .row01 .col05 {
    background: #db7200 none repeat scroll 0 0;
    border-bottom-color: #000;
    border-right-color: #000;
    border-top-color: #000;
}
.info-data .row01 .col06 {
    background: #db6500 none repeat scroll 0 0;
    border-bottom-color: #000;
    border-right-color: #000;
    border-top-color: #000;
}
.info-data .row02 td {
/*     background: #3A3A3A none repeat scroll 0 0; */
    font-size: 16px;
    height: 88px;
    line-height: 19px;
}
.info-data .row02 td.nobg {
    background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
}
.info-data .row02 td.col01 {
    font-weight: bold;
}
.info-data .row03 td {
    color: #000;
    font-size: 24px;
    font-weight: bold;
    height: 45px;
    line-height: 29px;
    padding: 5px 0;
}
.info-data .row03 .col01 {
    background: #db9700 none repeat scroll 0 0;
    font-size: 16px;
    line-height: 16px;
}
.tick {
    font-size: 30px;
}
.info-data .row03 .col02 {
    background: #db9100 none repeat scroll 0 0;
    color: #000;
}
.info-data .row03 .col03 {
    background: #db8700 none repeat scroll 0 0;
}
.info-data .row03 .col04 {
    background: #db7a00 none repeat scroll 0 0;
}
.info-data .row03 .col05 {
    background: #db7200 none repeat scroll 0 0;
}
.info-data .row03 .col06 {
    background: #db6500 none repeat scroll 0 0;
}
.btn-click, #content .btn-click, input[type="submit"], input[type="reset"], input[type="button"], button, .txt-area .entry a.btn-click {
    background: #db6e00 none repeat scroll 0 0;
    border: 1px solid #000;
    border-radius: 5px;
    color: #fff;
    display: inline-block;
    font-size: 17px;
    height: 40px;
    line-height: 40px;
    padding: 0 24px;
    position: relative;
    text-decoration: none;
    vertical-align: top;
    text-shadow: 0 0 0 rgba(255, 255, 255, 0.75) !important;
    margin: 15px 0;
}
.btn-click:hover, .btn-login:hover, input[type="submit"]:hover, input[type="reset"]:hover, input[type="button"]:hover, button:hover, #content .btn-click:hover, .txt-area .entry a.btn-click:hover {
    background: #ed9944 none repeat scroll 0 0;
    text-decoration: none;
    color: #fff;
    text-shadow: 0 0 0 rgba(255, 255, 255, 0.75) !important;
}
</style>
<script type="text/javascript">
function cartItem(plan) {
		<%session.setAttribute("discountError", null);
		session.setAttribute("plan", null);
		session.setAttribute("type", null);
		session.setAttribute("pmcode", null);
		session.setAttribute("Expired", null);
		session.setAttribute("Used", null);
		
      	Connection conn1 = Util.getConnection();
  		PreparedStatement pst1;
        pst1 = conn1.prepareStatement("select plan,type from cart_details where corp_id = ?");
        pst1.setLong(1, corp.getId());
        ResultSet rs7 = pst1.executeQuery();
        if(rs7.next()){ 
        	session.setAttribute("Cart_Error", "Error");
      	}else{
      		session.setAttribute("Cart_Error", "No_Error");
      	} 
        pst1.close();
		conn1.close();
		
		%>
		window.location.href = "cart.jsp?plan="+plan+"&type=upgrade";
}
</script>
</head>
<body>


  <div class="page-header">
    <h1>Pricing</h1>
  </div>
  <%@include file="/WEB-INF/includes/breadcrumbs.jsp"%>
  <div class="span8 well">
  <form class="form-horizontal" method="post">
  <fieldset>
  		<h2 style="text-align: center; color: #db6e00;">
			30 day money back guarantee. No setup fees. No contracts.
		</h2>
		<h3 style="text-align: center; margin: 0px 0px 30px;">
			Get your business on the leading edge of communication and innovation today!
		</h3>
		
		<div class="info-data">
<table border="1" align="center">
<tbody>
<tr class="row01">
<td class="col01">Plans</td>
 <%
	            int i = 2;
              	Connection conn = Util.getConnection();
	      		PreparedStatement pst;
	            pst = conn.prepareStatement("select plan_name from plan_price where type = ? order by price");
	            pst.setString(1, "Upgrade");
	            ResultSet rs = pst.executeQuery();
	            while(rs.next()){ %>
              		<td class="col0<%=i%>"><%=rs.getString("plan_name").replace(" ","<br>")%></td>
              	<%i = i + 1;} %>
</tr>
<tr class="row02">
<td class="col01">Maximum number of users</td>
 <%	
 				i = 2;
	            pst = conn.prepareStatement("select max_users from plan_price where type = ? order by price");
	            pst.setString(1, "Upgrade");
	            ResultSet rs1 = pst.executeQuery();
	            while(rs1.next()){ %>
              		<td class="col0<%=i%>">
              		<%if(rs1.getLong("max_users") == 1){%>
              			One user only
              		<%}%>
              		<%if(rs1.getLong("max_users") == 2){%>
              			Maximum two <br> users
              		<%}%>
              		<%if(rs1.getLong("max_users") == 3){%>
              			Maximum three <br> users
              		<%}%>
              		<%if(rs1.getLong("max_users") == 4){%>
              			Maximum four <br> users
              		<%}%>
              		<%if(rs1.getLong("max_users") == 5){%>
              			Maximum five <br> users
              		<%}%>
              		<%if(rs1.getLong("max_users") == 6){%>
              			Maximum six <br> users
              		<%}%>
              		<%if(rs1.getLong("max_users") == 7){%>
              			Maximum seven <br> users
              		<%}%>
              		<%if(rs1.getLong("max_users") == 8){%>
              			Maximum eight <br> users
              		<%}%>
              		<%if(rs1.getLong("max_users") == 9){%>
              			Maximum nine <br> users
              		<%}%>
              		<%if(rs1.getLong("max_users") == 10){%>
              			Maximum ten <br> users
              		<%}%>
              		<%if(rs1.getLong("max_users") > 10){%>
              			Maximum <%=rs1.getLong("max_users")%> users
              		<%}%>
              		</td>
              	<%i = i + 1;} %>
</tr>
<tr class="row02">
<td class="col01">Additional practice locations</td>
<%	
 				i = 2;
	            pst = conn.prepareStatement("select max_locations from plan_price where type = ? order by price");
	            pst.setString(1, "Upgrade");
	            ResultSet rs2 = pst.executeQuery();
	            while(rs2.next()){ %>
              		<td class="col0<%=i%>">
              		<%if(rs2.getLong("max_locations") == 0){%>
              			Not available
              		<%}else{ %>
              			+$10/mth/location. Max <%=rs2.getLong("max_locations")%> locations.
              		<%}%>
              		</td>
              	<%i = i + 1;} %>
</tr>
<tr class="row02">
<td class="col01">Practice logo and contact details</td>
<%	
 				i = 2;
	            pst = conn.prepareStatement("select practice_logo from plan_price where type = ? order by price");
	            pst.setString(1, "Upgrade");
	            ResultSet rs3 = pst.executeQuery();
	            while(rs3.next()){ %>
              		<td class="col0<%=i%>">
              		<%if(rs3.getBoolean("practice_logo") == true){%>
              			<span class="tick">✔</span>
              		<%}else{ %>
              			Not available
              		<%}%>
              		</td>
              	<%i = i + 1;} %>
</tr>
<tr class="row02">
<td class="col01">In-App Marketing and Push-Notifications</td>
<%	
 				i = 2;
	            pst = conn.prepareStatement("select push_notification from plan_price where type = ? order by price");
	            pst.setString(1, "Upgrade");
	            ResultSet rs4 = pst.executeQuery();
	            while(rs4.next()){ %>
              		<td class="col0<%=i%>">
              		<%if(rs4.getBoolean("push_notification") == true){%>
              			<span class="tick">✔</span>
              		<%}else{ %>
              			Not available
              		<%}%>
              		</td>
              	<%i = i + 1;} %>
</tr>
<tr class="row03">
<td class="col01">
Monthly
<br>
Subscription Fee
</td>
<%	
 				i = 2;
	            pst = conn.prepareStatement("select currency_type,price from plan_price where type = ? order by price");
	            pst.setString(1, "Upgrade");
	            ResultSet rs5 = pst.executeQuery();
	            while(rs5.next()){ %>
              		<td class="col0<%=i%>">
              		<%if(rs5.getString("currency_type").equals("AUD")){%>
              			$<%=rs5.getBigDecimal("price")%>
              		<%}else if(rs5.getString("currency_type").equals("USD")){%>
              			$<%=rs5.getBigDecimal("price")%>
              		<%}else if(rs5.getString("currency_type").equals("Euro")){%>
              			$<%=rs5.getBigDecimal("price")%>
              		<%}else if(rs5.getString("currency_type").equals("Pounds")){%>
              			$<%=rs5.getBigDecimal("price")%>
              		<%}%>
              		</td>
              	<%i = i + 1;} %>
</tr>
<tr>
<td></td>
<%	
	            pst = conn.prepareStatement("select id from plan_price where type = ? order by price");
	            pst.setString(1, "Upgrade");
	            ResultSet rs6 = pst.executeQuery();
	            while(rs6.next()){ %>
              		<td>
              		<input class="btn" type="button" value="Select" name="addtocart" onclick="cartItem(<%=rs6.getString("id")%>);"/>
              		</td>
              	<%}
	            pst.close();
              	conn.close();
              	%>
</tr>
</tbody>
</table>
</div>
<p>
Please note prices are in AUD$ and exclude GST (GST applicable to Australian customers only). 
<a target="_blank" href="http://www.fiizio.com/wordpress/wp-content/uploads/2012/12/Fiizio-App-Licence-Agreement.pdf" title="License Agreement">Terms and conditions of use</a>
. Large enterprise / multi-location business that needs customised solutions? 
<a target="_blank" href="http://www.fiizio.com/home/contact/">Get a quote!</a>
</p>
  </fieldset>
      </form>
    </div>
</body>
</html>