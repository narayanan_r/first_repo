<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%@ page import="au.com.ourbodycorp.model.managers.CorporationManager" %>
<%@ page import="java.util.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.util.Date" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="au.com.ourbodycorp.model.managers.CountryManager" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");

    if (cu == null) {
        response.sendRedirect(Util.loginUrl(Constants.LoginReason.admin, request));
        return;
    }
    
    String type = Util.notNull(request.getParameter("type"));
    
    Corporation corp = cu.getCorporation();
    User u = cu.getUser();
 /*    try{
    	Util.allActivitiesLogs("Subscription", u.getId(), corp.getName(), "Remove Cart Details");
    }catch(Exception e){
    	
    } */
    LibraryManager lm = new LibraryManager();
    	
		Connection conn = Util.getConnection();
		PreparedStatement pst;
        pst = conn.prepareStatement("delete from cart_details where corp_id = ?");
        pst.setLong(1, corp.getId());
        pst.executeUpdate();
        pst.close();
		conn.close();
		
		session.setAttribute("plan", null);
		session.setAttribute("Cart_Plan", "");
		session.setAttribute("Cart_Type", "");
		session.setAttribute("promoError", null);
	
%>
<html><head>
<script type="text/javascript">
	window.location.replace("cart.jsp?type=<%=type%>");
</script>
</head></html>
