<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%@ page import="au.com.ourbodycorp.model.managers.CorporationManager" %>
<%@ page import="java.util.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.util.Date" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="au.com.ourbodycorp.model.managers.CountryManager" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");

     if (cu == null || !cu.hasRole(CorporationRole.Role.Admin)) {
         response.sendRedirect(Util.loginUrl(Constants.LoginReason.admin, request));
         return;
     }
    
    Corporation corp = cu.getCorporation();
    User u = cu.getUser();
   /*  try{
    	Util.allActivitiesLogs("Subscription", u.getId(), corp.getName(), "Apply Promotion Code");
    }catch(Exception e){
    	
    } */
	String promocode = Util.notNull(request.getParameter("promocode"));

	if (promocode != null) {
          	Connection conn2 = Util.getConnection();
      		PreparedStatement pst2;
            pst2 = conn2.prepareStatement("select expires, discount, company from promotion where promo_code = ?");
//             pst2.setString(1, u.getEmail());
            pst2.setString(1, promocode);
            ResultSet rs2 = pst2.executeQuery();
            if(rs2.next()){ 
            	if(rs2.getLong("company") > 0){
            		session.setAttribute("Used", "Used");
            		session.setAttribute("Expired", null);
            		session.setAttribute("discount", null);
            	} else if(rs2.getDate("expires").before(new Date())){
            		session.setAttribute("Expired", "Expired");
            		session.setAttribute("discount", null);
            		session.setAttribute("Used", null);
            	} else{
            		session.setAttribute("discount", rs2.getLong("discount"));
            		session.setAttribute("Expired", null);
            		session.setAttribute("Used", null);
            	}
            	session.setAttribute("discountError", null);
            	session.setAttribute("pmcode", promocode);
            }else{
            	session.setAttribute("discount", null);
            	session.setAttribute("pmcode", promocode);
            }
            
            pst2.close();
            conn2.close();
            session.setAttribute("Cart_Error", "No_Error");

        }
	
%>
<html><head>
<script type="text/javascript">
	window.location.replace("cart.jsp");
</script>
</head></html>
