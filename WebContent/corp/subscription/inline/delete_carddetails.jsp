<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="au.com.ourbodycorp.model.managers.CorporationManager" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.util.*" %>
<%@ page import="java.util.Date" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");
	long id = Long.parseLong(Util.notNull(request.getParameter("id"), "0"));
    if (cu == null) {
        response.sendRedirect(Util.loginUrl(Constants.LoginReason.login, request));
        return;
    }
    User u = cu.getUser();
    Corporation corp = cu.getCorporation();
    /* try{
    	Util.allActivitiesLogs("Subscription", u.getId(), corp.getName(), "Delete Card");
    }catch(Exception e){
    	
    } */
    if (request.getMethod().equalsIgnoreCase("post")) {
        if (request.getParameter("submit").startsWith("Cancel")) {
            response.sendRedirect(corp.getHost() + "/close.jsp");
            return;
        } 
        Connection conn = Util.getConnection();
		PreparedStatement pst;
        pst = conn.prepareStatement("delete from card_details where id = ?");
        pst.setLong(1, id);
        pst.executeUpdate();
        pst.close();
		conn.close();
		response.sendRedirect(corp.getHost() + "/close.jsp?reload=true");
		return;
    }

%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head><title>Card Details</title>
</head>
<body>
<div class="page-header">
    <h1>Delete Card</h1>
</div>
<div class="row">
    <div class="span6">
        <p>
            Delete Card. Are you sure?
        </p>
        <form action="delete_carddetails.jsp" method="post">
        	<input type="hidden" name="id" value="<%=id%>">
            <%=HTML.confirmCancel()%>
        </form>
    </div>
</div>
</body>
</html>