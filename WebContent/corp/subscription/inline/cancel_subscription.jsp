<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%@ page import="au.com.ourbodycorp.Config" %>
<%@ page import="au.com.ourbodycorp.model.managers.CorporationManager" %>
<%@ page import="java.util.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@page import="com.eway.process.SOAPClientJava"%>
<%@page import="javax.xml.namespace.QName"%>
<%@page import="javax.xml.soap.*"%>
<%@page import="javax.xml.transform.*"%>
<%@page import="javax.xml.transform.stream.*"%>
<%@ page import="au.com.ourbodycorp.MailMessage" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");

    if (cu == null || !cu.hasRole(CorporationRole.Role.Admin)) {
        response.sendRedirect(Util.loginUrl(Constants.LoginReason.admin, request));
        return;
    }
    
    String cusId = request.getParameter("customerid");
    String rebillId = request.getParameter("rebillid");
    String reason = request.getParameter("reason");
    
    Corporation corp = cu.getCorporation();
    User u = cu.getUser();
    /* try{
    	Util.allActivitiesLogs("Subscription", u.getId(), corp.getName(), "Cancel Subscription");
    }catch(Exception e){
    	
    } */
    SOAPClientJava soap = new SOAPClientJava();
    
    String error = null;
    
    if (request.getMethod().equalsIgnoreCase("post")) {
        if (request.getParameter("submit").equals("Cancel")) {
        	if(Config.getString("https").equals("true")){
        		response.sendRedirect("https://"+request.getHeader("host")+"/close.jsp");
        	}else{
				response.sendRedirect("http://"+request.getHeader("host")+"/close.jsp");
			}
            return;
        }
        if (reason.trim().length() == 0) {
        	error = "Please enter a reason";
        }
        if (error == null) {
        try {
            // Create SOAP Connection
            SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
            SOAPConnection soapConnection = soapConnectionFactory.createConnection();

            // Send SOAP Message to SOAP Server
            String url = "https://www.eway.com.au/gateway/rebill/manageRebill.asmx";
//            String url = "https://www.eway.com.au/gateway/rebill/test/manageRebill_test.asmx";
            SOAPMessage createSOAPDeleteRebillEventRequest = soapConnection.call(soap.createSOAPDeleteRebillEventRequest(cusId, rebillId), url);

            // Process the SOAP Response
            soap.printSOAPResponse(createSOAPDeleteRebillEventRequest);
            
            String Result = createSOAPDeleteRebillEventRequest.getSOAPBody().getElementsByTagName("Result").item(0).getFirstChild().getNodeValue(); 
            System.out.print("\nResult "+Result);
            
            	
            	SOAPMessage soapResponseDeleteCustomer = soapConnection.call(soap.soapResponseDeleteCustomer(cusId), url);

                // Process the SOAP Response
                soap.printSOAPResponse(soapResponseDeleteCustomer);
                
                String RebillCustomerID = soapResponseDeleteCustomer.getSOAPBody().getElementsByTagName("RebillCustomerID").item(0).getFirstChild().getNodeValue(); 
                System.out.print("\nRebillCustomerID "+RebillCustomerID);
                
                Date date = new Date();
            	Calendar cal = Calendar.getInstance();
            	SimpleDateFormat sdf = new SimpleDateFormat("dd MMM, yyyy H:mm a");
            	String cancelledDate = sdf.format(cal.getTime());
            	
                Properties props = new Properties();
                props.put("planname", session.getAttribute("Plan_Name").toString());
                props.put("cancelleddate", cancelledDate);
                props.put("company", corp.getName());
                props.put("fullname", u.getFirstName()+" "+u.getLastName());
                props.put("reason", reason);
                props.put("email", u.getEmail());
                props.put("phone", u.getMobile());
                MailMessage msg = new MailMessage(props, "cancel_subscription.vm", "shagul001@gmail.com","Fiizio Subscription Cancelled",false);

                try {
                    msg.send();
                }
                catch (Exception e) {
//                     e.printStackTrace();
//                     throw e;
                }
                
                Connection conn = Util.getConnection();
	            PreparedStatement pst = conn.prepareStatement("update cancel_subscription set canceled_date = current_timestamp, reason = ? where rebill_customer_id = ?");
	            pst.setString(1, reason);
	            pst.setLong(2, Long.parseLong(RebillCustomerID));
	            pst.executeUpdate();
	            pst.close();
	            conn.close();

            soapConnection.close();
        } catch (Exception e) {
            System.err.println("Error occurred while sending SOAP Request to Server");
            e.printStackTrace();
        } 
        
        	if(Config.getString("https").equals("true")){
        		response.sendRedirect("https://"+request.getHeader("host")+"/close.jsp?reload=true");
        	}else{
				response.sendRedirect("http://"+request.getHeader("host")+"/close.jsp?reload=true");
			}
            return;
            
        }

    }

%>
<html>
<head><title>Cancel Subscription</title>
</head>
<body>
<%@include file="/WEB-INF/includes/formErrorSimple.jsp"%>
 <div class="page-header">
    <h1>Cancel Subscription</h1>
</div>
<div class="row">
    <div class="span6">
        <p>
            To confirm cancellation of this subscription. Please enter a reason of cancellation and click the Cancel Subscription button below.
        </p>
        <form action="cancel_subscription.jsp" method="post">
        <input type="hidden" name="customerid" value="<%=cusId%>" />
		<input type="hidden" name="rebillid" value="<%=rebillId%>" />
		<div class="control-group">
		<label class="control-label" for="reason" style="float: left; width: 85px;">Reason</label>
			<div class="controls">
			<textarea id="reason" class="input-xlarge" rows="5" name="reason"></textarea>
			</div>
		</div>
		<p>
            Are you sure?
        </p>
        <div class="form-actions">
			<button class="btn btn-primary" value="Confirm" type="submit" name="submit">Cancel Subscription</button>
			<button class="btn" value="Cancel" type="submit" name="submit">Do Not Cancel</button>
			</div>
        </form>
    </div>
</div>
</body>
</html>