<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@page import="java.math.BigDecimal"%>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%@ page import="au.com.ourbodycorp.model.managers.CorporationManager" %>
<%@ page import="java.util.*" %>
<%@ page import="au.com.ourbodycorp.model.managers.CountryManager" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.util.*" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.eway.xml.bean.CreateAccessCodeRequest" %>
<%@ page import="com.eway.xml.bean.CreateAccessCodeResponse" %>
<%@ page import="com.eway.process.RapidAPI" %>
<%@ page import="com.eway.xml.bean.Options" %>
<%@ page import="com.eway.xml.bean.Option" %>
<%@ page import="com.eway.xml.bean.LineItem" %>
<%@ page import="com.eway.xml.bean.Items" %>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.ResourceBundle"%>
<%@ page import="org.json.JSONObject" %>
<%

	if( session.getAttribute("Cart_Plan") == null){
		response.sendRedirect("/corp/subscription/");
    	return;
	}

	if(request.getParameter("AccessCode")!=null){
		   final ResourceBundle configResource = ResourceBundle.getBundle("config");
		   String access = new RapidAPI().CallHttpGetURL(request.getParameter("AccessCode"),"https://api.ewaypayments.com/AccessCode/");
		   JSONObject jsonObject = new JSONObject(access);
		   String responseCode = jsonObject.getString("ResponseCode");
		   String responseMessage = jsonObject.getString("ResponseMessage");
		   String accessCode = jsonObject.getString("AccessCode");
		   if(responseCode.equals("00")){
			   session.setAttribute("eWayErrorMessage", null);
			   session.setAttribute("eWayAccessCode", accessCode);
			   response.sendRedirect("/corp/subscription/payment_history.jsp");
		   }else{
			   System.out.println("Please Check Your Credit Card Details....");
			   session.setAttribute("eWayAccessCode", null);
			   String resErrorMessage = configResource.getString(responseMessage);
			   session.setAttribute("eWayErrorMessage", resErrorMessage);
		   }
	}else{
		session.setAttribute("eWayErrorMessage", null);
	}

	String totalAmount = (String) session.getAttribute("TotalAmount");
	String invoiceReference = (String) session.getAttribute("InvoiceReference");
	CreateAccessCodeResponse CreateAccessCodeResponse = (CreateAccessCodeResponse) session.getAttribute("ResponseText");
	
	System.out.println("totalAmount: " + totalAmount);
	System.out.println("invoiceReference: " + invoiceReference);
	System.out.println("accessCode: " + CreateAccessCodeResponse.AccessCode);
	System.out.println("FormActionURL: " + CreateAccessCodeResponse.FormActionURL);
	
    CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");

    if (cu == null || !cu.hasRole(CorporationRole.Role.Admin)) {
        response.sendRedirect(Util.loginUrl(Constants.LoginReason.admin, request));
        return;
    }

    if(session.getAttribute("plan") == null && session.getAttribute("type") == null){
	   	 session.setAttribute("plan", request.getParameter("plan"));
	   	 session.setAttribute("type", request.getParameter("type"));
    }
    
    Corporation corp = cu.getCorporation();
    User u = cu.getUser();
    
    String cardholder = request.getParameter("EWAY_CARDNAME");
    String cardno = request.getParameter("CARDNUMBER");
    String mm = request.getParameter("EWAY_CARDEXPIRYMONTH");
    String yy = request.getParameter("EWAY_CARDEXPIRYYEAR");
    String cardtype = request.getParameter("billing-cardtype");
    String cvv = request.getParameter("EWAY_CARDCVN");
    Long cardselectid = Long.parseLong(Util.notNull(request.getParameter("cardselectid"), "1"));
    boolean selectedcard = request.getParameter("selectcard"+cardselectid+"") != null && Boolean.parseBoolean(request.getParameter("selectcard"+cardselectid+""));
    String selectedcardcvv = request.getParameter("billing-selectedcard-cvv"+cardselectid+"");

    HashMap<String, String> errorFields = new HashMap<String, String>();

    CorporationManager cm = new CorporationManager();

    if (request.getMethod().equalsIgnoreCase("get")) {

    } else {

        if(selectedcard == false){
	        if (cardholder.trim().length() == 0) {
	        	errorFields.put("EWAY_CARDNAME", "Please enter a card holder name");
	        } else if (cardno.trim().length() == 0) {
	        	errorFields.put("CARDNUMBER", "Please enter a card number");
	        } else if (!cardno.substring(0,2).startsWith("40") && !cardno.substring(0,2).startsWith("41") && !cardno.substring(0,2).startsWith("42") && !cardno.substring(0,2).startsWith("43")
	        		&& !cardno.substring(0,2).startsWith("44") && !cardno.substring(0,2).startsWith("45") && !cardno.substring(0,2).startsWith("46") && !cardno.substring(0,2).startsWith("47")
	        		&& !cardno.substring(0,2).startsWith("48") && !cardno.substring(0,2).startsWith("49") && !cardno.substring(0,2).startsWith("51") && !cardno.substring(0,2).startsWith("52")
	        		&& !cardno.substring(0,2).startsWith("53") && !cardno.substring(0,2).startsWith("54") && !cardno.substring(0,2).startsWith("55")) {
	        	errorFields.put("CARDNUMBER", "Please enter a valid Visa/Master card number");
	        } else if(mm.trim().length() == 0){
	        	errorFields.put("EWAY_CARDEXPIRYMONTH", "");
	        } else if(yy.trim().length() == 0){
	        	errorFields.put("EWAY_CARDEXPIRYYEAR", "");
	        } else if(cardtype.trim().length() == 0){
	        	errorFields.put("billing-cardtype", "Please select a card type");
	        } else if(cvv.trim().length() == 0){
	        	errorFields.put("EWAY_CARDCVN", "");
	        }
        }else{
        	if(selectedcardcvv.trim().length() == 0){
            	errorFields.put("billing-selectedcard-cvv"+cardselectid+"", "");
            }
        }

        if (errorFields.size() == 0) {
//         		response.sendRedirect("/corp/subscription/GetResponse?AccessCode="+CreateAccessCodeResponse.AccessCode+"");
        }
    }

    CountryManager ctm = new CountryManager();
    List<Country> countries = ctm.listCountries();
    
    String cardChoices[] = {"", "Visa", "Mater Card"};
    String cardChoicesVal[] = {"", "Visa", "MC"};
    
    String tittleChoices[] = {"Mr.", "Miss", "Mrs."};
    String tittleChoicesVal[] = {"Mr.", "Miss", "Mrs."};

    String[][] breadcrumbs;
    if(session.getAttribute("type").equals("sms")){
    	breadcrumbs = new String[][] {{"/corp/", corp.getName()}, {"/corp/subscription/", "Subscription"}, {"/corp/subscription/pricing_sms.jsp", "Pricing"}, {"/corp/subscription/cart.jsp?plan="+session.getAttribute("plan")+"&type=sms", "Cart"}, {"/corp/subscription/checkout.jsp?plan="+session.getAttribute("plan")+"&type=sms", "Checkout"}, {null, "Confirm Checkout"}};
    }else{
    	breadcrumbs = new String[][] {{"/corp/", corp.getName()}, {"/corp/subscription/", "Subscription"}, {"/corp/subscription/pricing.jsp", "Pricing"}, {"/corp/subscription/cart.jsp?plan="+session.getAttribute("plan")+"&type=upgrade", "Cart"}, {"/corp/subscription/checkout.jsp?plan="+session.getAttribute("plan")+"&type=sms", "Checkout"}, {null, "Confirm Checkout"}};
    }

%>
<html>
<head><title>Confirm Checkout</title>
<style type="text/css">
#shopp .submit {
    text-align: right;
}
.entry p {
    margin: 0 0 20px;
}

.btn-click, #content .btn-click, input[type="submit"], input[type="reset"], input[type="button"], button, .txt-area .entry a.btn-click {
    background: #49a4a8 none repeat scroll 0 0;
    border: 1px solid #000;
    border-radius: 5px;
    color: #fff;
    display: inline-block;
    font-size: 18px;
    height: 40px;
    line-height: 40px;
    padding: 0 24px;
    position: relative;
    text-decoration: none;
    vertical-align: top;
}
.btn-click:hover, .btn-login:hover, input[type="submit"]:hover, input[type="reset"]:hover, input[type="button"]:hover, button:hover, #content .btn-click:hover, .txt-area .entry a.btn-click:hover {
    background: #6cc0c4 none repeat scroll 0 0;
    text-decoration: none;
    color: #fff;
    text-shadow: 0 0 0 rgba(255, 255, 255, 0.75) !important;
}

#shopp {
    font-size: 14px;
}
#shopp {
    position: relative;
}
*::-moz-selection {
    background: #2d8bd8 none repeat scroll 0 0;
    color: #fff;
    text-shadow: none;
}
.entry {
    font-size: 16px;
    line-height: 19px;
}

#shopp #checkout, #shopp #checkout > ul, #shopp #checkout > ul ul, #shopp #checkout > ul li {
    margin: 0;
    padding: 0;
    text-align: left;
}
#shopp form {
    text-align: left;
}

#shopp #cart {
    margin: 0 0 15px;
}
#shopp #cart {
    width: 100%;
}
#shopp div.description, #shopp .products, #shopp .products ul, #shopp .products li.row, #shopp .category, #shopp #cart, #shopp #cart table {
    overflow: hidden;
}

#shopp #cart table {
    padding: 10px 0;
    width: 100%;
}
#shopp div.description, #shopp .products, #shopp .products ul, #shopp .products li.row, #shopp .category, #shopp #cart, #shopp #cart table {
    overflow: hidden;
}
#shopp form table, #shopp form table td {
    border: medium none;
}
table {
    border-collapse: collapse;
    border-spacing: 0;
}

#shopp #cart .money, #shopp #cart .totals th, #shopp #cart .buttons td {
    text-align: right;
    white-space: nowrap;
}

#shopp #cart .totals.total th, #shopp #cart .totals.total td {
    font-size: 130%;
}
#shopp #cart .money, #shopp #cart .totals th, #shopp #cart .buttons td {
    text-align: right;
    white-space: nowrap;
}
#shopp #cart th, #shopp #cart td {
    padding: 3px;
}
#shopp #cart th {
    font-weight: bold;
    text-align: left;
}
.form-horizontal .control-label {
    float: left;
    margin: 0 0 5px;
    padding-top: 5px;
    text-align: left;
    width: 140px;
}
.form-horizontal .controls {
	margin: 1px;
}
fieldset {
    border: 0 none;
    margin: 0;
    padding: 0;
    width: 50% !important;
    float: left;
}
</style>
<script type="text/javascript">
			$( document ).ready(function() {
				if(<%=selectedcard%> == true){
					$("#carddetails").hide();
					var cardid = <%=cardselectid%>;
					$('#selectcard'+cardid+'').attr("checked",true);
				}
				if(<%=selectedcard%> == false){
					$("#carddetails").show();
				}
			});
			function cardchoose(j) {
				var selectcard = document.getElementById("selectcard"+j+"").checked;
				var rowCount = document.getElementById("CardDetailsTable").rows.length;
					if(selectcard == true){
						$("#carddetails").hide();
					}
					else{
						$("#carddetails").show();
					}
					for(k=1;k<rowCount;k++){
						if(k == j && selectcard == true){
							$("#cardselectid").val(j);
							$('#selectcard'+k+'').attr("checked",true);
						}else{
							$("#cardselectid").val(1);
							$('#selectcard'+k+'').attr("checked",false);  
						}
					}
			}
    </script>
    <script type="text/javascript">
			var element = "ewayBlock";
			function cardtype() {
				var cardnoval = $('#CARDNUMBER').val();
				var cardno = cardnoval.substring(0,2);
				var row = $('#billing-cardtype');
				if(cardno == 41 || cardno == 42 || cardno == 43 || cardno == 44 || cardno == 45 || cardno == 46 || cardno == 47 || cardno == 48 || cardno == 49 || cardno == 40){
					row.find("option[value=Visa]").attr('selected','selected');
				}else if(cardno == 51 || cardno == 52 || cardno == 53 || cardno == 54 || cardno == 55){
					row.find("option[value=MC]").attr('selected','selected');
				}else if(cardno != 41 || cardno != 42 || cardno != 43 || cardno != 44 || cardno != 45 || cardno != 46 || cardno != 47 || 
						cardno != 48 || cardno != 49 || cardno != 40 || cardno != 51 || cardno != 52 || cardno != 53 || cardno != 54 || cardno != 55 ){
					row.find("option[value=]").attr('selected','selected');
				}
			}
			function isNumeric(evt) {
				 cardnum = $("#CARDNUMBER").val();
				 $("#EWAY_CARDNUMBER").val(cardnum.replace(/\s/g, ''));
				 document.getElementById('CARDNUMBER').addEventListener('input', function (e) {
					  e.target.value = e.target.value.replace(/[^\dA-Z]/g, '').replace(/(.{4})/g, '$1 ').trim();
					}); 
				evt = (evt) ? evt : window.event;
			    var charCode = (evt.which) ? evt.which : evt.keyCode;
			    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
			        return false;
			    }
			    return true;
			};
</script>
<script type="text/javascript">
function validateForm() {
    var name = document.forms["confirm_checkout"]["EWAY_CARDNAME"].value;
    var cn = document.forms["confirm_checkout"]["CARDNUMBER"].value;
//     var mm = document.forms["confirm_checkout"]["EWAY_CARDEXPIRYMONTH"].value;
//     var yy = document.forms["confirm_checkout"]["EWAY_CARDEXPIRYYEAR"].value;
    var ct = document.forms["confirm_checkout"]["billing-cardtype"].value;
    var cvv = document.forms["confirm_checkout"]["EWAY_CARDCVN"].value;
    
    var nameoncard = document.getElementById("nameoncard");
    var creditcardnumber = document.getElementById("creditcardnumber");
//     var ewaycardexpirymonth = document.getElementById("ewaycardexpirymonth");
//     var ewaycardexpiryyear = document.getElementById("ewaycardexpiryyear");
    var billingcardtype = document.getElementById("billingcardtype");
    var ewaycardcvn = document.getElementById("ewaycardcvn");
    var cardnoval = $('#CARDNUMBER').val();
    var cardno = cardnoval.substring(0,2);
    
    $("#inputeWayError").hide();
    $("#inputeWayError1").hide();
    
    if (name == null || name == "") {
    	$("#inputError").show();
    	nameoncard.className="control-group error";
    	$("#nameoncardError").show();
        return false;
    }else if(cn == null || cn == ""){
    	$("#inputError").show();
    	$("#nameoncardError").hide();
    	nameoncard.className="control-group";
    	creditcardnumber.className="control-group error";
    	var row = $('#billing-cardtype');
    	if(cn == null || cn == ""){
    		$("#creditcardnumberError").show();
    		$("#creditcardnumbertypeError").hide();
    	}else{
    		$("#creditcardnumberError").hide();
    		if(cardno == 41 || cardno == 42 || cardno == 43 || cardno == 44 || cardno == 45 || cardno == 46 || cardno == 47 || cardno == 48 || cardno == 49 || cardno == 40){
    			row.find("option[value=Visa]").attr('selected','selected');
    		}else if(cardno == 51 || cardno == 52 || cardno == 53 || cardno == 54 || cardno == 55){
    			row.find("option[value=MC]").attr('selected','selected');
    		}else if(cardno != 41 || cardno != 42 || cardno != 43 || cardno != 44 || cardno != 45 || cardno != 46 || cardno != 47 || 
    				cardno != 48 || cardno != 49 || cardno != 40 || cardno != 51 || cardno != 52 || cardno != 53 || cardno != 54 || cardno != 55 ){
    			row.find("option[value=]").attr('selected','selected');
    			$("#creditcardnumbertypeError").show();
    		};
    	}
    	return false;
    }else if(cardnoval.length < 19){
    	creditcardnumber.className="control-group error";
    	$("#creditcardnumberError").hide();
    	$("#creditcardnumbertypeError").hide();
    	$("#creditcard16digitError").show();
    	return false;
    }else if(cardno != 41 && cardno != 42 && cardno != 43 && cardno != 44 && cardno != 45 && cardno != 46 && cardno != 47 && cardno != 48 && cardno != 49 && cardno != 40 && cardno != 51 && cardno != 52 && cardno != 53 && cardno != 54 && cardno != 55){
    	creditcardnumber.className="control-group error";
    	$("#creditcardnumberError").hide();
    	$("#creditcard16digitError").hide();
    	$("#creditcardnumbertypeError").show();
    	return false;
    }
    /* else if(mm == null || mm == ""){
    	$("#inputError").show();
    	$("#nameoncardError").hide();
    	$("#creditcardnumberError").hide();
    	$("#creditcardnumbertypeError").hide();
    	$("#creditcard16digitError").hide();
    	nameoncard.className="control-group";
    	creditcardnumber.className="control-group";
    	ewaycardexpirymonth.className="control-group error";
    	$("#ewaycardexpirymonthError").show();
        return false;
    }else if(yy == null || yy == ""){
    	$("#inputError").show();
    	$("#nameoncardError").hide();
    	$("#creditcardnumberError").hide();
    	$("#creditcardnumbertypeError").hide();
    	$("#ewaycardexpirymonthError").hide();
    	$("#creditcard16digitError").hide();
    	nameoncard.className="control-group";
    	creditcardnumber.className="control-group";
    	ewaycardexpirymonth.className="control-group";
    	ewaycardexpiryyear.className="control-group error";
    	$("#ewaycardexpiryyearError").show();
        return false;
    } */else if(ct == null || ct == ""){
    	$("#inputError").show();
    	$("#nameoncardError").hide();
    	$("#creditcardnumberError").hide();
    	$("#creditcardnumbertypeError").hide();
    	$("#ewaycardexpirymonthError").hide();
    	$("#ewaycardexpiryyearError").hide();
    	$("#creditcard16digitError").hide();
    	nameoncard.className="control-group";
    	creditcardnumber.className="control-group";
//     	ewaycardexpirymonth.className="control-group";
//     	ewaycardexpiryyear.className="control-group";
    	billingcardtype.className="control-group error";
    	$("#billingcardtypeError").show();
        return false;
    }else if(cvv == null || cvv == ""){
    	$("#inputError").show();
    	$("#nameoncardError").hide();
    	$("#creditcardnumberError").hide();
    	$("#creditcardnumbertypeError").hide();
    	$("#ewaycardexpirymonthError").hide();
    	$("#ewaycardexpiryyearError").hide();
    	$("#billingcardtypeError").hide();
    	$("#creditcard16digitError").hide();
    	nameoncard.className="control-group";
    	creditcardnumber.className="control-group";
//     	ewaycardexpirymonth.className="control-group";
//     	ewaycardexpiryyear.className="control-group";
    	billingcardtype.className="control-group";
    	ewaycardcvn.className="control-group error";
    	$("#ewaycardcvnError").show();
        return false;
    }else{
    	$("#inputError").hide();
    	$("#nameoncardError").hide();
    	$("#creditcardnumberError").hide();
    	$("#creditcardnumbertypeError").hide();
    	$("#ewaycardexpirymonthError").hide();
    	$("#ewaycardexpiryyearError").hide();
    	$("#billingcardtypeError").hide();
    	$("#ewaycardcvnError").hide();
    	$("#creditcard16digitError").hide();
    	nameoncard.className="control-group";
    	creditcardnumber.className="control-group";
//     	ewaycardexpirymonth.className="control-group";
//     	ewaycardexpiryyear.className="control-group";
    	billingcardtype.className="control-group";
    	ewaycardcvn.className="control-group";
    	return true;
    };
}

function isAlphabetswithDot(txt, e) {
	var arr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz . \b";
    var code;
    if (window.event)
        code = e.keyCode;
    else
        code = e.which;
    var char = keychar = String.fromCharCode(code);
    if (arr.indexOf(char) == -1)
        return false;
};
</script>
</head>
<body>
  <div class="page-header">
    <h1>Confirm Checkout</h1>
  </div>
  <%@include file="/WEB-INF/includes/breadcrumbs.jsp"%>
  <%@ include file="/WEB-INF/includes/formError.jsp" %>
  <div class="alert alert-error" id="inputError" style="display: none;">
	<p>There were some problems with your input, please check the form for details.</p>
  </div>
  <%if(session.getAttribute("eWayErrorMessage") != null){%>
  <div id="inputeWayError" class="alert alert-error">
	<p>There were some problems with your input, please check the form for details.</p>
  </div>
  <%} %>
  <div class="span8 well">
  <form class="form-horizontal" name="confirm_checkout" method="post" style="float: left;" action="<%=CreateAccessCodeResponse.FormActionURL %>" onsubmit="return validateForm();">
  <div id="shopp" class="grid checkout">
<div id="checkout" class="shopp validate">
<div id="cart" class="shopp">
<%if(session.getAttribute("type").equals("sms")){ %>
<%
              	Connection conn1 = Util.getConnection();
	      		PreparedStatement pst1;
	            pst1 = conn1.prepareStatement("select id,plan_name,currency_type,tax,price from plan_price where id = ?");
	            pst1.setLong(1, Long.parseLong(session.getAttribute("Cart_Plan").toString()));
	            ResultSet rs1 = pst1.executeQuery();
	            while(rs1.next()){ %>
<table>
<tbody>
<tr>
<th class="item" scope="col">Cart Items</th>
<th scope="col">Quantity</th>
<th class="money" scope="col">Item Price</th>
<th class="money" scope="col">Item Total</th>
</tr>
<tr>
<td>
<a href="/corp/subscription/pricing_sms.jsp"><%=rs1.getString("plan_name")%></a>
<%session.setAttribute("plan_name", rs1.getString("plan_name")); %>
</td>
<td>1</td>
<td class="money">
<%	
	java.math.BigDecimal price = rs1.getBigDecimal("price");
	
	if(session.getAttribute("discount") != null){
		java.math.BigDecimal discount = new BigDecimal(session.getAttribute("discount").toString());
		java.math.BigDecimal pricePercentage = price.multiply(discount);
		java.math.BigDecimal priceDivHundred = pricePercentage.divide(new BigDecimal("100"));
		price = price.subtract(priceDivHundred);
	}else{
		price = rs1.getBigDecimal("price");
	}
	
%>
					<%if(rs1.getString("currency_type").equals("USD")){%>
              			$<%=price%>
              		<%}else if(rs1.getString("currency_type").equals("Euro")){%>
              			$<%=price%>
              		<%}else if(rs1.getString("currency_type").equals("Pounds")){%>
              			$<%=price%>
              		<%}%></td>
<td class="money">
<%	
	java.math.BigDecimal price1 = rs1.getBigDecimal("price");
	
	if(session.getAttribute("discount") != null){
		java.math.BigDecimal discount = new BigDecimal(session.getAttribute("discount").toString());
		java.math.BigDecimal pricePercentage = price1.multiply(discount);
		java.math.BigDecimal priceDivHundred = pricePercentage.divide(new BigDecimal("100"));
		price1 = price1.subtract(priceDivHundred);
	}else{
		price1 = rs1.getBigDecimal("price");
	}
	
%>
					<%if(rs1.getString("currency_type").equals("USD")){%>
              			$<%=price1%>
              		<%}else if(rs1.getString("currency_type").equals("Euro")){%>
              			$<%=price1%>
              		<%}else if(rs1.getString("currency_type").equals("Pounds")){%>
              			$<%=price1%>
              		<%}%></td>
</tr>
<tr class="totals">
<td rowspan="5" colspan="2"> </td>
<th scope="row">Subtotal</th>
<td class="money">
<span class="shopp-cart cart-subtotal">
<%	
	java.math.BigDecimal price2 = rs1.getBigDecimal("price");

	if(session.getAttribute("discount") != null){
		java.math.BigDecimal discount = new BigDecimal(session.getAttribute("discount").toString());
		java.math.BigDecimal pricePercentage = price2.multiply(discount);
		java.math.BigDecimal priceDivHundred = pricePercentage.divide(new BigDecimal("100"));
		price2 = price2.subtract(priceDivHundred);
	}else{
		price2 = rs1.getBigDecimal("price");
	}
	
%>
					<%if(rs1.getString("currency_type").equals("USD")){%>
              			$<%=price2%>
              		<%}else if(rs1.getString("currency_type").equals("Euro")){%>
              			$<%=price2%>
              		<%}else if(rs1.getString("currency_type").equals("Pounds")){%>
              			$<%=price2%>
              		<%}%></span>
</td>
</tr>
<tr class="totals">
<th scope="row">Taxes</th>
<td class="money">
<span class="shopp-cart cart-tax"><%if(rs1.getString("currency_type").equals("USD")){%>
              			$<%=rs1.getBigDecimal("tax")%>
              		<%}else if(rs1.getString("tax").equals("Euro")){%>
              			$<%=rs1.getBigDecimal("price")%>
              		<%}else if(rs1.getString("currency_type").equals("Pounds")){%>
              			$<%=rs1.getBigDecimal("tax")%>
              		<%}%></span>
</td>
</tr>
<tr class="totals total">
<th scope="row">Total</th>
<td class="money">
<span class="shopp-cart cart-total">
<%	
	java.math.BigDecimal tax = rs1.getBigDecimal("tax");
	java.math.BigDecimal price3 = rs1.getBigDecimal("price");
	java.math.BigDecimal total = new BigDecimal("0.00");

	if(session.getAttribute("discount") != null){
		java.math.BigDecimal discount = new BigDecimal(session.getAttribute("discount").toString());
		java.math.BigDecimal pricePercentage = price3.multiply(discount);
		java.math.BigDecimal priceDivHundred = pricePercentage.divide(new BigDecimal("100"));
		price3 = price.subtract(priceDivHundred);
	}else{
		price3 = rs1.getBigDecimal("price");
	}
	
	total = tax.add(price3);
	session.setAttribute("totalamt", total);
	session.setAttribute("tax", tax);
	session.setAttribute("price", price3);
%>					
					<%if(rs1.getString("currency_type").equals("USD")){%>
              			$<%=total%>
              		<%}else if(rs1.getString("currency_type").equals("Euro")){%>
              			$<%=total%>
              		<%}else if(rs1.getString("currency_type").equals("Pounds")){%>
              			$<%=total%>
              		<%}%></span>
</td>
</tr>
</tbody>
</table>
<%} pst1.close(); conn1.close(); %>
<%}else if(session.getAttribute("type").equals("upgrade")){ %>
<%
              	Connection conn1 = Util.getConnection();
	      		PreparedStatement pst1;
	            pst1 = conn1.prepareStatement("select id,plan_name,currency_type,tax,price from plan_price where id = ?");
	            pst1.setLong(1, Long.parseLong(session.getAttribute("Cart_Plan").toString()));
	            ResultSet rs1 = pst1.executeQuery();
	            while(rs1.next()){ %>
<table>
<tbody>
<tr>
<th class="item" scope="col">Cart Items</th>
<th scope="col">Quantity</th>
<th class="money" scope="col">Item Price</th>
<th class="money" scope="col">Item Total</th>
</tr>
<tr>
<td>
<a href="/corp/subscription/pricing.jsp"><%=rs1.getString("plan_name")%></a>
<%session.setAttribute("plan_name", rs1.getString("plan_name")); %>
<p style="margin: 5px 0; font-size: 14px;">
<strong>Subscription:</strong>
</p>
<p style="color: darkred;">
<%if(rs1.getString("currency_type").equals("USD")){%>
              			$<%=rs1.getBigDecimal("price")%>
              		<%}else if(rs1.getString("currency_type").equals("Euro")){%>
              			$<%=rs1.getBigDecimal("price")%>
              		<%}else if(rs1.getString("currency_type").equals("Pounds")){%>
              			$<%=rs1.getBigDecimal("price")%>
              		<%}%> per month.
<br>
Subscription rebilled unlimited times.
</p>
</td>
<td>1</td>
<td class="money">
<%	
	java.math.BigDecimal price4 = rs1.getBigDecimal("price");

	if(session.getAttribute("discount") != null){
		java.math.BigDecimal discount = new BigDecimal(session.getAttribute("discount").toString());
		java.math.BigDecimal pricePercentage = price4.multiply(discount);
		java.math.BigDecimal priceDivHundred = pricePercentage.divide(new BigDecimal("100"));
		price4 = price4.subtract(priceDivHundred);
	}else{
		price4 = rs1.getBigDecimal("price");
	}
	
%>
					<%if(rs1.getString("currency_type").equals("USD")){%>
              			$<%=price4%>
              		<%}else if(rs1.getString("currency_type").equals("Euro")){%>
              			$<%=price4%>
              		<%}else if(rs1.getString("currency_type").equals("Pounds")){%>
              			$<%=price4%>
              		<%}%></td>
<td class="money">
<%	
	java.math.BigDecimal price5 = rs1.getBigDecimal("price");
	
	if(session.getAttribute("discount") != null){
		java.math.BigDecimal discount = new BigDecimal(session.getAttribute("discount").toString());
		java.math.BigDecimal pricePercentage = price5.multiply(discount);
		java.math.BigDecimal priceDivHundred = pricePercentage.divide(new BigDecimal("100"));
		price5 = price5.subtract(priceDivHundred);
	}else{
		price5 = rs1.getBigDecimal("price");
	}
	
%>
					<%if(rs1.getString("currency_type").equals("USD")){%>
              			$<%=price5%>
              		<%}else if(rs1.getString("currency_type").equals("Euro")){%>
              			$<%=price5%>
              		<%}else if(rs1.getString("currency_type").equals("Pounds")){%>
              			$<%=price5%>
              		<%}%></td>
</tr>
<tr class="totals">
<td rowspan="5" colspan="2"> </td>
<th scope="row">Subtotal</th>
<td class="money">
<span class="shopp-cart cart-subtotal">
<%
	java.math.BigDecimal price6 = rs1.getBigDecimal("price");

	if(session.getAttribute("discount") != null){
		java.math.BigDecimal discount = new BigDecimal(session.getAttribute("discount").toString());
		java.math.BigDecimal pricePercentage = price6.multiply(discount);
		java.math.BigDecimal priceDivHundred = pricePercentage.divide(new BigDecimal("100"));
		price6 = price6.subtract(priceDivHundred);
	}else{
		price6 = rs1.getBigDecimal("price");
	}
	
%>
					<%if(rs1.getString("currency_type").equals("USD")){%>
              			$<%=price6%>
              		<%}else if(rs1.getString("currency_type").equals("Euro")){%>
              			$<%=price6%>
              		<%}else if(rs1.getString("currency_type").equals("Pounds")){%>
              			$<%=price6%>
              		<%}%></span>
</td>
</tr>
<tr class="totals">
<th scope="row">Taxes</th>
<td class="money">
<span class="shopp-cart cart-tax"><%if(rs1.getString("currency_type").equals("USD")){%>
              			$<%=rs1.getBigDecimal("tax")%>
              		<%}else if(rs1.getString("currency_type").equals("Euro")){%>
              			$<%=rs1.getBigDecimal("tax")%>
              		<%}else if(rs1.getString("currency_type").equals("Pounds")){%>
              			$<%=rs1.getBigDecimal("tax")%>
              		<%}%></span>
</td>
</tr>
<tr class="totals total">
<th scope="row">Total</th>
<td class="money">
<span class="shopp-cart cart-total">
<%	
	java.math.BigDecimal tax1 = rs1.getBigDecimal("tax");
	java.math.BigDecimal price7 = rs1.getBigDecimal("price");
	java.math.BigDecimal total1 = new BigDecimal("0.00");

	if(session.getAttribute("discount") != null){
		java.math.BigDecimal discount = new BigDecimal(session.getAttribute("discount").toString());
		java.math.BigDecimal pricePercentage = price7.multiply(discount);
		java.math.BigDecimal priceDivHundred = pricePercentage.divide(new BigDecimal("100"));
		price7 = price7.subtract(priceDivHundred);
	}else{
		price7 = rs1.getBigDecimal("price");
	}
	
	total1 = tax1.add(price7);
	session.setAttribute("totalamt", total1);
	session.setAttribute("tax", tax1);
	session.setAttribute("price", price7);
%>					
					<%if(rs1.getString("currency_type").equals("USD")){%>
              			$<%=total1%>
              		<%}else if(rs1.getString("currency_type").equals("Euro")){%>
              			$<%=total1%>
              		<%}else if(rs1.getString("currency_type").equals("Pounds")){%>
              			$<%=total1%>
              		<%}%></span>
</td>
</tr>
</tbody>
</table>
<%} pst1.close(); conn1.close();%>
<%}else { %>
	<div id="noItem" class="grid cart">
		<p class="warning" style="font-size: 20px; color: darkred;">There are currently no items in your shopping cart.</p>
  	</div>
<%} %>
</div>
</div>
</div>
<%if(session.getAttribute("type").equals("sms") || session.getAttribute("type").equals("upgrade")) {%>
  <input type="hidden" id="cardselectid" name="cardselectid" value="<%=cardselectid%>">
  <fieldset>
          <legend>Billing Address:</legend>
          <div class="control-group">
				<label class="control-label" for="ddlTitle">Tittle</label>
			<div class="controls" style="margin: 5px 0 0 150px !important;">
				<%=CreateAccessCodeResponse.Customer.getTitle()%>
			</div>
		  </div>
		  <div class="control-group">
				<label class="control-label" for="name">Name</label>
			<div class="controls" style="margin: 5px 0 0 150px !important;">
				<%=CreateAccessCodeResponse.Customer.getFirstName()+" "+CreateAccessCodeResponse.Customer.getLastName()%>
			</div>
		  </div>
		  <div class="control-group">
				<label class="control-label" for="address1">Address line 1</label>
			<div class="controls" style="margin: 5px 0 0 150px !important;">
				<%=CreateAccessCodeResponse.Customer.getStreet1()%>
			</div>
		  </div>
		  <div class="control-group">
				<label class="control-label" for="address2">Address line 2</label>
			<div class="controls" style="margin: 5px 0 0 150px !important;">
				<%=CreateAccessCodeResponse.Customer.getStreet2()%>
			</div>
		  </div>
		  <div class="control-group">
				<label class="control-label" for="suburb">City / Suburb</label>
			<div class="controls" style="margin: 5px 0 0 150px !important;">
				<%=CreateAccessCodeResponse.Customer.getCity()%>
			</div>
		  </div>
		  <div class="control-group">
				<label class="control-label" for="state">State or Territory</label>
			<div class="controls" style="margin: 5px 0 0 150px !important;">
				<%
              	Connection conn2 = Util.getConnection();
	      		PreparedStatement pst2;
	            pst2 = conn2.prepareStatement("select printable_name from country where lower(iso) = ?");
	            pst2.setString(1, CreateAccessCodeResponse.Customer.getState());
	            ResultSet rs2 = pst2.executeQuery();
	            if(rs2.next()){ %>
	            	<%=rs2.getString("printable_name")%>
	            <% }else{%>
	           	 	<%=CreateAccessCodeResponse.Customer.getState()%>
	            <%} pst2.close(); conn2.close();%>
			</div>
		  </div>
          <div class="control-group">
				<label class="control-label" for="postcode">Postcode</label>
			<div class="controls" style="margin: 5px 0 0 150px !important;">
				<%=CreateAccessCodeResponse.Customer.getPostalCode()%>
			</div>
		  </div>
		  <div class="control-group">
				<label class="control-label" for="country">Country</label>
			<div class="controls" style="margin: 5px 0 0 150px !important;">
			<%
              	Connection conn3 = Util.getConnection();
	      		PreparedStatement pst3;
	            pst3 = conn3.prepareStatement("select printable_name from country where lower(iso) = ?");
	            pst3.setString(1, CreateAccessCodeResponse.Customer.getCountry());
	            ResultSet rs3 = pst3.executeQuery();
	            if(rs3.next()){ %>
	            	<%=rs3.getString("printable_name")%>
	            <% }else{%>
					<%=CreateAccessCodeResponse.Customer.getCountry()%>
	            <%} pst3.close(); conn3.close();%>
			</div>
		  </div>
          </fieldset>
  <fieldset>
  		<legend>Contact Information:</legend>
  		<div class="control-group">
				<label class="control-label" for="firstName">First Name</label>
			<div class="controls" style="margin: 5px 0 0 150px !important;">
				<%=CreateAccessCodeResponse.Customer.getFirstName()%>
			</div>
		</div>
		<div class="control-group">
				<label class="control-label" for="lastName">Last Name</label>
			<div class="controls" style="margin: 5px 0 0 150px !important;">
				<%=CreateAccessCodeResponse.Customer.getLastName()%>
			</div>
		</div>
		<div class="control-group">
				<label class="control-label" for="company">Company Name</label>
			<div class="controls" style="margin: 5px 0 0 150px !important;">
				<%=CreateAccessCodeResponse.Customer.getCompanyName()%>
			</div>
		</div>
		<div class="control-group">
				<label class="control-label" for="mobile">Mobile Phone Number</label>
			<div class="controls" style="margin: 5px 0 0 150px !important;">
				<%=CreateAccessCodeResponse.Customer.getMobile()%>
			</div>
		</div>
		<div class="control-group">
				<label class="control-label" for="email">Email</label>
			<div class="controls" style="margin: 5px 0 0 150px !important;">
				<%=CreateAccessCodeResponse.Customer.getEmail()%>
			</div>
		</div>
  </fieldset>

		<fieldset style="width: 100% !important;">
          <legend>Payment Information:</legend>
          <%if(session.getAttribute("eWayErrorMessage") != null){%>
          <div id="inputeWayError1" class="alert alert-error">
			<p>Please enter a valid credit card details.</p>
			<p><%=session.getAttribute("eWayErrorMessage")%>.</p>
		  </div>
		  <%} %>
          <div id="carddetails">
          <!-- The following fields are the ones that eWAY looks for in the POSTed data when the form is submitted. -->

                        <!-- This field should contain the access code received from eWAY -->
                        <input type='hidden' name='EWAY_ACCESSCODE' value="<%=CreateAccessCodeResponse.AccessCode%>" />
                        <input type="hidden" name="EWAY_PAYMENTTYPE" value="Credit Card" />
                        <input type="hidden" name="EWAY_CARDNUMBER" id="EWAY_CARDNUMBER" value="<%=cardno%>" />
        <div class="control-group" id="nameoncard">
			<label class="control-label" for="EWAY_CARDNAME">Name on Card</label>
		<div class="controls">
		 <%if(cardno != null){ %>
			<input id="EWAY_CARDNAME" class="input-xlarge" type="text" value="<%=cardholder%>" name="EWAY_CARDNAME" maxlength="50" size="30" onkeypress="return isAlphabetswithDot(this,event);" placeholder="Name on Card">
			<%}else{ %>
			<input id="EWAY_CARDNAME" class="input-xlarge" type="text" value="" name="EWAY_CARDNAME" maxlength="50" size="30" onkeypress="return isAlphabetswithDot(this,event);" placeholder="Name on Card">
			<%}%>
			<span class="help-inline" id="nameoncardError" style="display: none;">Please enter a card holder name</span>
		</div>
		</div>
		<div class="control-group" id="creditcardnumber" style="width: 460px; margin: 0; float: left;">
			<label class="control-label" for="CARDNUMBER">Credit Card Number</label>
		<div class="controls">
		<%if(cardno != null){%>
			<input id="CARDNUMBER" class="input-xlarge" type="text" autocomplete="off" onkeypress="return isNumeric(event);" onchange="cardtype();" placeholder="Card Number" value="<%=cardno%>" name="CARDNUMBER" maxlength="19" size="19">
			<%}else{ %>
			<input id="CARDNUMBER" class="input-xlarge" type="text" autocomplete="off" onkeypress="return isNumeric(event);" onchange="cardtype();" placeholder="Card Number" value="" name="CARDNUMBER" maxlength="19" size="19">
			<%}%>
			<span class="help-inline" id="creditcardnumberError" style="display: none; margin: 0 0 0 135px;">Please enter a credit card number</span>
			<span class="help-inline" id="creditcardnumbertypeError" style="display: none; margin: 0 0 0 135px;">Please enter a valid Visa/Master card number</span>
			<span class="help-inline" id="creditcard16digitError" style="display: none; margin: 0 0 0 135px;">Please enter a 16 digit card number</span>
		</div>
		</div>
		<!-- <div class="control-group" id="ewaycardexpirymonth" style="float: left; margin: 0; width: 130px; padding: 0px 0px 20px;">
			<label class="control-label" style="text-align: left; width: 50px;" for="EWAY_CARDEXPIRYMONTH">Expiry</label>
		<div class="controls" style="margin-left: 29px;">
			<input id="EWAY_CARDEXPIRYMONTH" class="input-mini" type="text" autocomplete="off" onkeypress="return isNumeric(event);" placeholder="MM" value="" name="EWAY_CARDEXPIRYMONTH" maxlength="2" size="2">
			<span class="help-inline" id="ewaycardexpirymonthError" style="display: none;"></span>
		</div>
		</div>
		<div class="control-group" id="ewaycardexpiryyear" style="margin-bottom: 0px;">
			<label class="control-label" style="display: none;" for="EWAY_CARDEXPIRYYEAR"></label>
		<div class="controls" style="margin-left: 0px; padding: 0 0 20px;">
			<input id="EWAY_CARDEXPIRYYEAR" class="input-mini" type="text" autocomplete="off" onkeypress="return isNumeric(event);" placeholder="YY" value="" name="EWAY_CARDEXPIRYYEAR" maxlength="2" size="2">
			<span class="help-inline" id="ewaycardexpiryyearError" style="display: none;"></span>
		</div>
		</div> -->
		<div class="control-group" style="float: left; margin: 0; width: 130px; padding: 0px 0px 20px;">
		<label class="control-label" style="text-align: left; width: 50px;">Expiry</label>
		<div class="controls" style="margin-left: 29px;">
		<select id="EWAY_CARDEXPIRYMONTH" class="input-mini" name="EWAY_CARDEXPIRYMONTH" >
		<%
                                	int expiryMonth = 1;
                                
                                		Date date = new Date();
                                		Calendar cal = Calendar.getInstance();
                                		expiryMonth = cal.get(Calendar.MONTH) + 1;
                                	
                                	for(int i = 1; i <= 12; i++)
                                	{
                                		out.print("<option value='" + i + "'");
                                		
                                		if(mm != null){
                                		if(Integer.parseInt(mm) == i)
                                		{	
                                			out.print(" selected='selected'");
                                		}}else{
                                		
                                		if(expiryMonth == i)
                                		{	
                                			out.print(" selected='selected'");
                                		}
                                		}
                                		out.print(">" + i + "</option>\n");
                                	}
                                %>
		</select>&nbsp;&nbsp;/
		</div>
		</div>
		<div class="control-group" style="margin-bottom: 0px;">
		<div class="controls" style="margin-left: 0px; padding: 0 0 20px;">
		<select id="EWAY_CARDEXPIRYYEAR" class="input-mini" name="EWAY_CARDEXPIRYYEAR" >
		 <%
                                	int currentYear = cal.get(Calendar.YEAR);
                                	int cardExpiryYear = 0;
                                	int expiryYear = currentYear + 11;
                                	
                                	for(int i = currentYear; i <= expiryYear; i++)
                                	{
                                		out.print("<option value='" + i + "'");
                                		
                                		if(yy != null){
                                    		if(Integer.parseInt(yy) == i)
                                    		{	
                                    			out.print(" selected='selected'");
                                    		}}else{
                                    		
                                    			if(cardExpiryYear == i)
                                        		{	
                                        			out.print(" selected='selected'");
                                        		}
                                    		}
                                		
                                		out.print(">" + i + "</option>\n");
                                	}
                                %>
		</select>
		</div>
		</div> 
		<div class="control-group" id="billingcardtype" style="width: 270px; float: left;">
			<label class="control-label">Card Type</label>
		<div class="controls" style="width: 260px;">
			<select id="billing-cardtype" class="input-mini" name="billing-cardtype" style="width:100px;">
				<option value=""></option>
				<option value="Visa">Visa</option>
				<option value="MC">Mater Card</option>
			</select>
			<span class="help-inline" id="billingcardtypeError" style="display: none; margin: 0 0 0 135px;">Please select a card type</span>
		</div>
		</div>
		<div class="control-group" id="ewaycardcvn" style="float: left; margin: 0; width: 495px; padding: 0px 0px 20px;">
			<label class="control-label" style="width:80px;" for="EWAY_CARDCVN">CVV</label>
		<div class="controls" style="margin-left: 0px;">
			<input id="EWAY_CARDCVN" class="input-mini" type="text" autocomplete="off" onkeypress="return isNumeric(event);" placeholder="CVV" value="" name="EWAY_CARDCVN" maxlength="4" size="4">
			<span class="help-inline" id="ewaycardcvnError" style="display: none;"></span>
		</div>
		</div>
<%--             <%=HTML.textInput("EWAY_CARDNAME","Name on Card","30","50", cardholder, null, errorFields)%> --%>
<%-- 			<%=HTML.textBillingCardInput("CARDNUMBER","Credit Card Number","19","19", cardno, null, errorFields, "Card Number", "cardtype();", "return isNumeric(event);")%> --%>
<%-- 			<%=HTML.textBillingCardMMInput("EWAY_CARDEXPIRYMONTH","Expiry","2","2", mm, null, errorFields, "MM", "return isNumeric(event);")%> --%>
<%-- 			<%=HTML.textBillingCardYYInput("EWAY_CARDEXPIRYYEAR","","2","2",yy, null, errorFields, "YY", "return isNumeric(event);")%> --%>
<%-- 			<%=HTML.selectCardTypeMini("billing-cardtype", "Card Type", cardChoicesVal, cardChoices, cardtype, null, errorFields, false)%> --%>
<%-- 			<%=HTML.textBillingCardCVVInput("EWAY_CARDCVN","CVV","4","4",cvv, null, errorFields, "CVV", "return isNumeric(event);")%> --%>
		  </div>
		<%-- <%
		Connection conn2 = Util.getConnection();
        PreparedStatement pst2 = conn2.prepareStatement("select id,accountno,month,year,card_type,name_on_card from card_details where corp_id = ?");
        pst2.setLong(1, corp.getId());
        ResultSet rs2 = pst2.executeQuery();
        if(rs2.next() == false){	conn2.close();
        %>
        <%}else{ %>
        <fieldset style="width: 100% !important;">
		<legend style="margin:0;">Saved Cards:</legend>
	<table class="table" id="CardDetailsTable">
    <tr>
        <th>Select Card</th>
    	<th>Account Number</th>
        <th>Card Type</th>
        <th>CVV</th>
    </tr>
        <%
		Connection conn1 = Util.getConnection();
        conn1.setAutoCommit(false);
        PreparedStatement pst1 = conn1.prepareStatement("select id,accountno,month,year,card_type,name_on_card from card_details where corp_id = ?");
        pst1.setLong(1, corp.getId());
        ResultSet rs1 = pst1.executeQuery();
        int i = 1;
        while(rs1.next()){
        	String accno = rs1.getString("accountno");
        	Long id = rs1.getLong("id");
        %>
    <tr>
		<td>
		<input id="selectcard<%=i%>" type="checkbox" name="selectcard<%=i%>" onclick="cardchoose(<%=i%>);" value="true">
		</td>
        <td><%=accno.substring(0,4)%>-XXXX-XXXX-<%=accno.substring(Math.max(accno.length() - 4, 0))%></td>
        <td><%if(rs1.getString("card_type").equals("MC")){%>Master Card<%}else{ %><%=rs1.getString("card_type")%><%} %></td>
        <td><%=HTML.textBillingSelectedCardCVVInput("billing-selectedcard-cvv"+i+"","","4","4","", null, errorFields, "CVV", "return isNumeric(event);")%></td>
  	</tr>

<%i = i +1;}conn1.close();%>
</table>
<fieldset style="width: 100% !important;"><legend></legend></fieldset>
<%} %> 
</fieldset>--%>
</fieldset>
<fieldset>
			 <div class="control-group">
				<label for="marketing">
					<input type="hidden" value="no" name="marketing">
					<input id="marketing" type="checkbox" value="yes" checked="checked" name="marketing" style="margin: -5px 0 0 0 !important;">
					Yes, I would like to receive e-mail updates and special offers!
					</label>
			 </div>
        </fieldset>
        
        <p class="submit" style="margin: 0 0 40px;">
			<span class="payoption-button payoption-0" style="display: inline; float: right;">
			<input id="checkout-button" class="checkout-button" type="submit" value="Submit Order" name="process">
			</span>
		</p>
      </form>
    <div class="security_logo">
<div id="ewayBlock">
<div style="text-align: center">
<div>
<a title="eWAY - Online payments made easy" target="_eway" href="http://www.eway.com.au/secure-site-seal">
<img border="0" title="eWAY - Online payments made easy" alt="eWAY - Online payments made easy" src="https://www.eway.com.au/developer/payment-code/verified-seal.ashx?img=11&size=3">
</a>
</div>
<div style="text-align: center; color: #000;">
<a style="text-decoration: none; color: #000" title="Credit Card Processing" target="_eway" href="https://www.eway.com.au/secure-site-seal">Credit Card Processing</a>
|
<a style="text-decoration: none; color: #000" title="eCommerce Website" target="_eway" href="https://www.eway.com.au/secure-site-seal">eCommerce Website</a>
|
<a style="text-decoration: none; color: #000" title="Online Payments" target="_eway" href="https://www.eway.com.au/secure-site-seal">Online Payments</a>
</div>
</div>
</div>
</div>
<%} %>
    </div>
</body>
</html>