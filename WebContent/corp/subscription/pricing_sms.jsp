<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%@ page import="au.com.ourbodycorp.model.managers.CorporationManager" %>
<%@ page import="java.util.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="au.com.ourbodycorp.model.managers.CountryManager" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");

	if (cu == null) {
	    response.sendRedirect(Util.loginUrl(Constants.LoginReason.login, request));
	    return;
	}
    Corporation corp = cu.getCorporation();
    User u = cu.getUser();
    /* try{
    	Util.allActivitiesLogs("Pricing", u.getId(), corp.getName(), "Pricing Shop SMS");
    }catch(Exception e){
    	
    } */
    
    String[][] breadcrumbs = {{"/corp/", corp.getName()}, {"/corp/subscription/", "Subscription"}, {null, "Pricing"}};

%>
<html>
<head><title>Pricing</title>
<style type="text/css">
.info-data {
    padding: 0 0 25px;
}
/* .info-data table { */
/*     width: 500px; */
/*     margin: auto; */
/* } */
.info-data table form {
    padding: 15px 0 0;
}
.info-data td {
    padding: 0 5px;
    text-align: center;
    vertical-align: middle;
}
.info-data td.col01 {
    width: 152px;
}
.info-data td.col02 {
    width: 172px;
}
.info-data td.col03 {
    width: 146px;
}
.info-data td.col04 {
    width: 147px;
}
.info-data .row01 td {
    color: #fff;
    font-size: 20px;
    font-weight: bold;
    height: 90px;
    line-height: 25px;
}
.info-data .row01 .col01 {
    background: #db9700 none repeat scroll 0 0;
    color: #000;
    font-size: 33px;
    font-weight: bold;
    line-height: 36px;
}
.info-data .row01 .col02 {
    background: #db9100 none repeat scroll 0 0;
    border-bottom-color: #000;
    border-right-color: #000;
    border-top-color: #000;
}
.info-data .row01 .col03 {
    background: #db8700 none repeat scroll 0 0;
    border-bottom-color: #000;
    border-right-color: #000;
    border-top-color: #000;
}
.info-data .row01 .col04 {
    background: #db7a00 none repeat scroll 0 0;
    border-bottom-color: #000;
    border-right-color: #000;
    border-top-color: #000;
}
.info-data .row01 .col05 {
    background: #db7200 none repeat scroll 0 0;
   border-bottom-color: #000;
    border-right-color: #000;
    border-top-color: #000;
}
.info-data .row01 .col06 {
    background: #db6500 none repeat scroll 0 0;
    border-bottom-color: #000;
    border-right-color: #000;
    border-top-color: #000;
}
.info-data .row02 td {
/*     background: #3A3A3A none repeat scroll 0 0; */
    font-size: 16px;
    height: 70px;
    line-height: 19px;
}
.info-data .row02 td.nobg {
    background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
}
.info-data .row02 td.col01 {
    font-weight: bold;
}
.info-data .row03 td {
    color: #000;
    font-size: 24px;
    font-weight: bold;
    height: 45px;
    line-height: 29px;
    padding: 5px 0;
}
.info-data .row03 .col01 {
    background: #db9700 none repeat scroll 0 0;
    font-size: 16px;
    line-height: 16px;
}
.tick {
    font-size: 30px;
}
.info-data .row03 .col02 {
    background: #db9100 none repeat scroll 0 0;
    color: #000;
}
.info-data .row03 .col03 {
    background: #db8700 none repeat scroll 0 0;
}
.info-data .row03 .col04 {
    background: #db7a00 none repeat scroll 0 0;
}
.info-data .row03 .col05 {
    background: #db7200 none repeat scroll 0 0;
}
.info-data .row03 .col06 {
    background: #db6500 none repeat scroll 0 0;
}
.btn-click, #content .btn-click, input[type="submit"], input[type="reset"], input[type="button"], button, .txt-area .entry a.btn-click {
    background: #db6e00 none repeat scroll 0 0;
    border: 1px solid #000;
    border-radius: 5px;
    color: #fff;
    display: inline-block;
    font-size: 17px;
    height: 40px;
    line-height: 40px;
    padding: 0 24px;
    position: relative;
    text-decoration: none;
    vertical-align: top;
    text-shadow: 0 0 0 rgba(255, 255, 255, 0.75) !important;
    margin: 15px 0;
}
.btn-click:hover, .btn-login:hover, input[type="submit"]:hover, input[type="reset"]:hover, input[type="button"]:hover, button:hover, #content .btn-click:hover, .txt-area .entry a.btn-click:hover {
    background: #ed9944 none repeat scroll 0 0;
    text-decoration: none;
    color: #fff;
    text-shadow: 0 0 0 rgba(255, 255, 255, 0.75) !important;
}
</style>
<script type="text/javascript">
function cartItem(plan) {
		<%session.setAttribute("discountError", null);
		session.setAttribute("plan", null);
		session.setAttribute("type", null);
		session.setAttribute("Expired", null);
		session.setAttribute("Used", null);
		
		Connection conn1 = Util.getConnection();
  		PreparedStatement pst1;
        pst1 = conn1.prepareStatement("select plan,type from cart_details where corp_id = ?");
        pst1.setLong(1, corp.getId());
        ResultSet rs7 = pst1.executeQuery();
        if(rs7.next()){ 
        	session.setAttribute("Cart_Error", "Error");
      	}else{
      		session.setAttribute("Cart_Error", "No_Error");
      	} 
        pst1.close();
		conn1.close();%>
		window.location.href = "cart.jsp?plan="+plan+"&type=sms";
}
</script>
</head>
<body>


  <div class="page-header">
    <h1>Pricing Shop SMS</h1>
  </div>
  <%@include file="/WEB-INF/includes/breadcrumbs.jsp"%>
  <div class="span8 well">
  <form class="form-horizontal" method="post">
  <fieldset>
  		<h2 style="text-align: center; color: #db6e00; margin: 0px 0px 30px">
			Purchase sms credits for use within Fiizio App
		</h2>
		<div class="info-data">
<table border="1" align="center">
<tbody>
<tr class="row01">
<td class="col01">Plans</td>
 <%
	            int i = 2;
              	Connection conn = Util.getConnection();
	      		PreparedStatement pst;
	            pst = conn.prepareStatement("select plan_name from plan_price where type = ? order by price");
	            pst.setString(1, "SMS");
	            ResultSet rs = pst.executeQuery();
	            while(rs.next()){ %>
              		<td class="col0<%=i%>"><%=rs.getString("plan_name")%></td>
              	<%i = i + 1;} %>
</tr>
<tr class="row02">
<td class="col01">Cents per sms</td>
<%	
 				i = 2;
	            pst = conn.prepareStatement("select cents_per_sms from plan_price where type = ? order by price");
	            pst.setString(1, "SMS");
	            ResultSet rs1 = pst.executeQuery();
	            while(rs1.next()){ %>
              		<td class="col0<%=i%>">
              			<%=rs1.getLong("cents_per_sms")%> cents per sms
              		</td>
              	<%i = i + 1;} %>
</tr>
<tr class="row02">
<td class="col01">Price</td>
<%	
 				i = 2;
	            pst = conn.prepareStatement("select currency_type,price from plan_price where type = ? order by price");
	            pst.setString(1, "SMS");
	            ResultSet rs2 = pst.executeQuery();
	            while(rs2.next()){ %>
              		<td class="col0<%=i%>">
              		<%if(rs2.getString("currency_type").equals("AUD")){%>
              			$<%=rs2.getBigDecimal("price")%>
              		<%}else if(rs2.getString("currency_type").equals("USD")){%>
              			$<%=rs2.getBigDecimal("price")%>
              		<%}else if(rs2.getString("currency_type").equals("Euro")){%>
              			$<%=rs2.getBigDecimal("price")%>
              		<%}else if(rs2.getString("currency_type").equals("Pounds")){%>
              			$<%=rs2.getBigDecimal("price")%>
              		<%}%>
              		</td>
              	<%i = i + 1;} %>
</tr>
<tr>
<td></td>
<%	
	            pst = conn.prepareStatement("select id from plan_price where type = ? order by price");
	            pst.setString(1, "SMS");
	            ResultSet rs3 = pst.executeQuery();
	            while(rs3.next()){ %>
              		<td>
              		<input class="btn" type="button" value="Select" name="addtocart" onclick="cartItem(<%=rs3.getString("id")%>);">
              		</td>
              	<%}
	            pst.close();
              	conn.close();
              	%>
</tr>
</tbody>
</table>
</div>
  </fieldset>
      </form>
    </div>
</body>
</html>