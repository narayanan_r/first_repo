<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@page import="java.math.BigDecimal"%>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%@ page import="au.com.ourbodycorp.model.managers.CorporationManager" %>
<%@ page import="java.util.*" %>
<%@ page import="au.com.ourbodycorp.model.managers.CountryManager" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="au.com.ourbodycorp.Config" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.util.*" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.eway.xml.bean.CreateAccessCodeRequest" %>
<%@ page import="com.eway.xml.bean.CreateAccessCodeResponse" %>
<%@ page import="com.eway.process.RapidAPI" %>
<%@ page import="com.eway.xml.bean.Options" %>
<%@ page import="com.eway.xml.bean.Option" %>
<%@ page import="com.eway.xml.bean.LineItem" %>
<%@ page import="com.eway.xml.bean.Items" %>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@ page import="org.json.JSONObject" %>
<%@ page import="com.eway.json.bean.CreateTokenRequest" %>
<%@ page import="com.eway.json.bean.DirectPaymentResponse" %>
<%@page import="java.util.ResourceBundle"%>
<%

	final ResourceBundle configResource = ResourceBundle.getBundle("config");
	
	String redirectURL = configResource.getString("redirectURL");
	String method = configResource.getString("requestMethod");
	String format = configResource.getString("requestFormat");
	String tokencustomerid = request.getParameter("tokencustomerid");
	
	String formActionURL = "";
	
    CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");

    if (cu == null) {
        response.sendRedirect(Util.loginUrl(Constants.LoginReason.login, request));
        return;
    }

    if(session.getAttribute("plan") == null && session.getAttribute("type") == null){
	   	 session.setAttribute("plan", request.getParameter("plan"));
	   	 session.setAttribute("type", request.getParameter("type"));
    }
    
    if(session.getAttribute("Cart_Plan") == "" || session.getAttribute("type") == null || session.getAttribute("Cart_Plan") == null){
    	response.sendRedirect("/corp/subscription/");
        return;
    }
    
    Corporation corp = cu.getCorporation();
    User u = cu.getUser();
    /* try{
    	Util.allActivitiesLogs("Subscription", u.getId(), corp.getName(), "Checkout");
    }catch(Exception e){
    	
    } */
    HashMap<String, String> errorFields = new HashMap<String, String>();

    CorporationManager cm = new CorporationManager();
    if (request.getMethod().equalsIgnoreCase("post") && request.getParameter("checkout-button-JSON") != null) {
    if (request.getParameter("checkout-button-JSON").startsWith("Submit Order")) {
    	
    	String txtInvoiceNumber = session.getAttribute("invoice_number").toString();
    	String txtInvoiceDescription = "1 x "+session.getAttribute("plan_name").toString();
		String txtInvoiceReference = session.getAttribute("invoice_number").toString();
		String txtCurrencyCode = "AUD";
		
    	BigDecimal pr = new BigDecimal(session.getAttribute("price").toString());
	    BigDecimal ta = new BigDecimal(session.getAttribute("tax").toString());
	    BigDecimal tt = new BigDecimal(session.getAttribute("totalamt").toString());
	    BigDecimal prmul = pr.multiply(new BigDecimal("100"));
	    BigDecimal tamul = ta.multiply(new BigDecimal("100"));
	    BigDecimal ttmul = tt.multiply(new BigDecimal("100"));
	    int prhd = Integer.valueOf(prmul.intValue());
	    int tahd = Integer.valueOf(tamul.intValue());
	    int tthd = Integer.valueOf(ttmul.intValue());
		    
		String txtAmount = String.valueOf(tthd);
    	
    	String ddlMethod = "ProcessPayment";
    	
    	//Create AccessCode Request Object
    	CreateTokenRequest CreateTokenRequest = new CreateTokenRequest();
    	
    	//Populate values for Customer Object
    		    //Note: TokenCustomerID is Required Field When Update an existing TokenCustomer
    	CreateTokenRequest.Customer.setTokenCustomerID(tokencustomerid);
    	session.setAttribute("tkCusId", tokencustomerid);
    	
    	//Populate values for Payment Object
    		    //Note: TotalAmount is a Required Field When Process a Payment, TotalAmount should set to "0" or leave EMPTY when Create/Update A TokenCustomer
    	CreateTokenRequest.Payment.setTotalAmount(txtAmount);
    	CreateTokenRequest.Payment.setInvoiceNumber(txtInvoiceNumber);
    	CreateTokenRequest.Payment.setInvoiceDescription(txtInvoiceDescription);
    	CreateTokenRequest.Payment.setInvoiceReference(txtInvoiceReference);
    	CreateTokenRequest.Payment.setCurrencyCode(txtCurrencyCode);
    	
    	//Url to the page for getting the result with an AccessCode
    		    //Note: RedirectUrl is a Required Field For all cases
//     			CreateAccessCodeRequest.RedirectUrl = txtRedirectURL;
    	
    	//Method for this request. e.g. ProcessPayment, Create TokenCustomer, Update TokenCustomer & TokenPayment
    	CreateTokenRequest.Method = ddlMethod;
    	CreateTokenRequest.TransactionType = "Recurring";
    	
    	//Create RapidAPI Service
    	RapidAPI rapidAPI = new RapidAPI();
    	
    	//Call RapidAPI
    	String DirectPaymentResponse = rapidAPI.CreateToken(CreateTokenRequest);
    	session.setAttribute("ResponceTokenText", DirectPaymentResponse);
    	
    	String Token = session.getAttribute("ResponceTokenText").toString();
        
        String[] ResMes = Token.split(",");
    	String[] ResMes1 = ResMes[1].split(":");
//     	String Error = DirectPaymentResponse.substring(DirectPaymentResponse.length() - 7);
//     	boolean Errors = Error.contains("null");

    	//Check if any error returns
    	if(ResMes1[1].equals("00"))
    	{
    		/* try{
    	    	Util.allActivitiesLogs("Subscription", u.getId(), corp.getName(), "Submit Order");
    	    }catch(Exception e){
    	    	
    	    } */
    		JSONObject jsonObject = new JSONObject(Token);
    		String responseCode = jsonObject.getString("ResponseCode");
 		    String responseMessage = jsonObject.getString("ResponseMessage");
    		if(responseCode.equals("00")){
 			    session.setAttribute("eWaySavedCardErrorMessage", null);
 			    session.setAttribute("eWayErrorMessage", null);
 				session.setAttribute("eWayErrorMessage1", null);
 				session.setAttribute("eWayErrorMessage2", null);
 				session.setAttribute("eWayErrorMessage3", null);
 				session.setAttribute("eWayErrorMessage4", null);
 				session.setAttribute("eWayErrorMessage5", null);
 			    session.setAttribute("eWayTokenCode", tokencustomerid);
 			    response.sendRedirect("/corp/subscription/");
 		   }
        	
    	}else{
    			JSONObject jsonObject1 = new JSONObject(DirectPaymentResponse);
				String ErrorCode = jsonObject1.getString("ResponseMessage");
				String resErrorMessage = configResource.getString(ErrorCode);
				session.setAttribute("eWaySavedCardErrorMessage", resErrorMessage);
// 				for(int i = 1; i<ErrorCode.length;i++){
// 					session.setAttribute("eWayErrorMessage"+i+"", configResource.getString(ErrorCode[i]));
// 				}
				
				String[] Tamt = Token.split(",");
				String[] Tamt1 = Tamt[42].split(":");
				
				String[] Tinv = Token.split(",");
				String[] Tinv1 = Tinv[43].split(":");
				
				String[] Tid = Token.split(",");
				String[] Tid1 = Tid[3].split(":");
				
				String[] Tst = Token.split(",");
				String[] Tst1 = Tst[4].split(":");
				
				String[] Tac = Token.split(",");
				String[] Tac1 = Tac[19].split(":");
				
				   double totamt = Double.valueOf(Tamt1[2]);
				   String invoiceNumber = Tinv1[1];
				   int transId = Integer.parseInt(Tid1[1]);
				   boolean transStatus = Boolean.parseBoolean(Tst1[1]);
				   double tamt = (totamt / 100);
				   java.math.BigDecimal totalamt = new java.math.BigDecimal(tamt);
				   
				    PaymentHistory ph;
				    LibraryManager lm = new LibraryManager();
				    if(transId > 0){
			        	ph = new PaymentHistory();
			        	ph.setUserid(u.getId());
			        	ph.setCorp_id(corp.getId());
			        	ph.setCompany(corp.getCompanyId());
			        	ph.setAccess_code(Tac1[1]);
			        	ph.setResponse(Tac1[1]);
			        	ph.setInvoice_number(invoiceNumber.replace("\"", ""));
			        	ph.setTotal_amount(totalamt);
			        	ph.setTrans_id(transId);
			        	ph.setTrans_status(transStatus);
			        	ph.setCreated(new Date());
			        	lm.savePaymentHistoryDetails(ph);
				    } 
    	}
    	
    } 
    }

    if (session.getAttribute("CreateAccessCode") != null) {
    	session.setAttribute("CreateAccessCode", null);
try
		{
			session.setAttribute("eWaySavedCardErrorMessage", null);
			session.setAttribute("eWayErrorMessage", null);
			session.setAttribute("eWayErrorMessage1", null);
			session.setAttribute("eWayErrorMessage2", null);
			session.setAttribute("eWayErrorMessage3", null);
			session.setAttribute("eWayErrorMessage4", null);
			session.setAttribute("eWayErrorMessage5", null);
			session.setAttribute("tkCusId", null);
			
			String txtCustomerRef = "";
			String ddlTitle = "Mr.";
			String txtFirstName = u.getFirstName();
			String txtLastName = u.getLastName();
			String txtCompanyName = corp.getName();
			String txtJobDescription = "";
			String txtStreet1 = corp.getAddress1();
			String txtStreet2 = corp.getAddress2();
			String txtCity = corp.getSuburb();
			String txtState = corp.getState();
			String txtPostalcode = corp.getPostcode();
			String txtCountry = corp.getCountry();
			String txtEmail = u.getEmail();
			String txtPhone = u.getMobile();
			String txtMobile = u.getMobile();
			String txtComments = "";
			String txtFax = "";
			String txtUrl = "";
			
			String txtShippingFirstName = u.getFullName();
// 			String txtShippingLastName = request.getParameter("txtShippingLastName");
			String txtShippingStreet1 = corp.getAddress1();
			String txtShippingStreet2 = corp.getAddress2();
			String txtShippingCity = corp.getSuburb();
			String txtShippingState = corp.getState();
			String txtShippingCountry = corp.getCountry();
			String txtShippingPostalCode = corp.getPostcode();
			String txtShippingEmail = u.getEmail();
			String txtShippingPhone = u.getMobile();
			
			BigDecimal pr = new BigDecimal(session.getAttribute("price").toString());
		    BigDecimal ta = new BigDecimal(session.getAttribute("tax").toString());
		    BigDecimal tt = new BigDecimal(session.getAttribute("totalamt").toString());
		    BigDecimal prmul = pr.multiply(new BigDecimal("100"));
		    BigDecimal tamul = ta.multiply(new BigDecimal("100"));
		    BigDecimal ttmul = tt.multiply(new BigDecimal("100"));
		    int prhd = Integer.valueOf(prmul.intValue());
		    int tahd = Integer.valueOf(tamul.intValue());
		    int tthd = Integer.valueOf(ttmul.intValue());
			    
			String txtAmount = String.valueOf(tthd);
			String txtInvoiceNumber = session.getAttribute("invoice_number").toString();
			String txtInvoiceDescription = "1 x "+session.getAttribute("plan_name").toString();
			String txtInvoiceReference = session.getAttribute("invoice_number").toString();
			String txtCurrencyCode = "AUD";
			
			String txtRedirectURL = "";
			if(Config.getString("https").equals("true")){
				txtRedirectURL = "https://"+corp.getSlug()+"."+Config.getString("baseDomain")+"/corp/subscription/checkout.jsp";
// 				txtRedirectURL = "https://"+Config.getString("baseDomain")+"/corp/subscription/checkout.jsp";
		    }else{
			    txtRedirectURL = "http://"+corp.getSlug()+"."+Config.getString("baseDomain")+"/corp/subscription/checkout.jsp";
// 		    	txtRedirectURL = "http://"+Config.getString("baseDomain")+"/corp/subscription/checkout.jsp";
		    }
			String ddlMethod = "TokenPayment";
			
			//Create AccessCode Request Object
			CreateAccessCodeRequest CreateAccessCodeRequest = new CreateAccessCodeRequest();
			
			//Populate values for Customer Object
		    //Note: TokenCustomerID is Required Field When Update an existing TokenCustomer
			CreateAccessCodeRequest.Customer.setReference(txtCustomerRef);
			CreateAccessCodeRequest.Customer.setFirstName(txtFirstName); 		//Note: FirstName is Required Field When Create/Update a TokenCustomer
			CreateAccessCodeRequest.Customer.setLastName(txtLastName);			//Note: LastName is Required Field When Create/Update a TokenCustomer
			CreateAccessCodeRequest.Customer.setReference(txtInvoiceReference);
			CreateAccessCodeRequest.Customer.setTitle(ddlTitle); 				//Note: Title is Required Field When Create/Update a TokenCustomer
			CreateAccessCodeRequest.Customer.setCompanyName(txtCompanyName);
			CreateAccessCodeRequest.Customer.setJobDescription(txtJobDescription);
			CreateAccessCodeRequest.Customer.setStreet1(txtStreet1);
			CreateAccessCodeRequest.Customer.setStreet2(txtStreet2);
			CreateAccessCodeRequest.Customer.setCity(txtCity);
			CreateAccessCodeRequest.Customer.setState(txtState);
			CreateAccessCodeRequest.Customer.setPostalCode(txtPostalcode);
			CreateAccessCodeRequest.Customer.setCountry(txtCountry);			//Note: Country is Required Field When Create/Update a TokenCustomer
			CreateAccessCodeRequest.Customer.setEmail(txtEmail);
			CreateAccessCodeRequest.Customer.setPhone(txtPhone);
			CreateAccessCodeRequest.Customer.setMobile(txtMobile);
			CreateAccessCodeRequest.Customer.setComments(txtComments);
			CreateAccessCodeRequest.Customer.setFax(txtFax);
			CreateAccessCodeRequest.Customer.setUrl(txtUrl);
			
			//Populate values for ShippingAddress Object.
		    //This values can be taken from a Form POST as well. Now is just some dummy data.
			CreateAccessCodeRequest.ShippingAddress.setFirstName(txtShippingFirstName);
// 			CreateAccessCodeRequest.ShippingAddress.setLastName(txtShippingLastName);
			CreateAccessCodeRequest.ShippingAddress.setStreet1(txtShippingStreet1);
			CreateAccessCodeRequest.ShippingAddress.setStreet2(txtShippingStreet2);
			CreateAccessCodeRequest.ShippingAddress.setCity(txtShippingCity);
			CreateAccessCodeRequest.ShippingAddress.setState(txtShippingState);
			CreateAccessCodeRequest.ShippingAddress.setCountry(txtShippingCountry);
			CreateAccessCodeRequest.ShippingAddress.setPostalCode(txtShippingPostalCode);
			CreateAccessCodeRequest.ShippingAddress.setEmail(txtShippingEmail);
			CreateAccessCodeRequest.ShippingAddress.setPhone(txtShippingPhone);
			CreateAccessCodeRequest.ShippingAddress.setShippingMethod("Other");		//ShippingMethod, e.g. "LowCost", "International", "Military". Check the spec for available values.
			
			//Populate values for Payment Object
		    //Note: TotalAmount is a Required Field When Process a Payment, TotalAmount should set to "0" or leave EMPTY when Create/Update A TokenCustomer
			CreateAccessCodeRequest.Payment.setTotalAmount(txtAmount);
			CreateAccessCodeRequest.Payment.setInvoiceNumber(txtInvoiceNumber);
			CreateAccessCodeRequest.Payment.setInvoiceDescription(txtInvoiceDescription);
			CreateAccessCodeRequest.Payment.setInvoiceReference(txtInvoiceReference);
			CreateAccessCodeRequest.Payment.setCurrencyCode(txtCurrencyCode);
			
			//Populate values for LineItems
			LineItem lineItem1 = new LineItem();
			lineItem1.setSKU(session.getAttribute("plan_name").toString());
			lineItem1.setDescription("1 x "+session.getAttribute("plan_name").toString());
			lineItem1.setQuantity(1);
			lineItem1.setUnitCost(prhd);
			lineItem1.setTax(tahd);
			lineItem1.setTotal(tthd);
			
			CreateAccessCodeRequest.Items = new Items();
			CreateAccessCodeRequest.Items.LineItem[0] = lineItem1;
			
			//Url to the page for getting the result with an AccessCode
		    //Note: RedirectUrl is a Required Field For all cases
			CreateAccessCodeRequest.RedirectUrl = txtRedirectURL;
			
			//Method for this request. e.g. ProcessPayment, Create TokenCustomer, Update TokenCustomer & TokenPayment
			CreateAccessCodeRequest.Method = ddlMethod;
			CreateAccessCodeRequest.TransactionType = "Purchase";
			
			//Create RapidAPI Service
			RapidAPI rapidAPI = new RapidAPI();
			
			//Call RapidAPI
			CreateAccessCodeResponse CreateAccessCodeResponse = rapidAPI.CreateAccessCode(CreateAccessCodeRequest);
			System.out.println("sucess"+CreateAccessCodeResponse);
			
			//Check if any error returns
			if(CreateAccessCodeResponse.Errors == null || CreateAccessCodeResponse.Errors.equals("") || CreateAccessCodeResponse.Errors == "")
			{
				//Save result into Session. payment.jsp and results.jsp will retrieve this result from Session
				session.setAttribute("TotalAmount", txtAmount);
				session.setAttribute("InvoiceReference", txtInvoiceReference);
				session.setAttribute("ResponseText", CreateAccessCodeResponse);
				
				//All good then redirect to the payment page
				response.sendRedirect("checkout.jsp");
			}
			else
			{
				session.setAttribute("errors", CreateAccessCodeResponse.Errors);
				response.sendRedirect("checkout.jsp");
			}
		}
		catch (Exception e)
		{
			System.out.println("\nException in RapidAPIXMLService\n");
			e.printStackTrace();
			response.sendRedirect("checkout.jsp");
		}
    }


    if(request.getParameter("AccessCode")!=null){
    	   session.setAttribute("tkCusId", null);
		   String access = new RapidAPI().CallHttpGetURL(request.getParameter("AccessCode"),"https://api.ewaypayments.com/AccessCode/");
		   JSONObject jsonObject = new JSONObject(access);
		   String responseCode = jsonObject.getString("ResponseCode");
		   String responseMessage = jsonObject.getString("ResponseMessage");
		   String accessCode = jsonObject.getString("AccessCode");
		   if(responseCode.equals("00")){
			   /* try{
	    	    	Util.allActivitiesLogs("Subscription", u.getId(), corp.getName(), "Submit Order");
	    	    }catch(Exception e){
	    	    	
	    	    } */
			    session.setAttribute("eWayErrorMessage", null);
				session.setAttribute("eWayErrorMessage1", null);
				session.setAttribute("eWayErrorMessage2", null);
				session.setAttribute("eWayErrorMessage3", null);
				session.setAttribute("eWayErrorMessage4", null);
				session.setAttribute("eWayErrorMessage5", null);
			    session.setAttribute("eWayAccessCode", accessCode);
			    response.sendRedirect("/corp/subscription/");
		   }else{
			   String resp = access;
			   double totamt = jsonObject.getDouble("TotalAmount");
			   String invoiceNumber = jsonObject.getString("InvoiceNumber");
			   int transId = 0;
			   if(!responseCode.equals("00") && !responseMessage.equals("V6101")){
			   	   transId = jsonObject.getInt("TransactionID");
			   }
			   boolean transStatus = jsonObject.getBoolean("TransactionStatus");
			   double tamt = (totamt / 100);
			   java.math.BigDecimal totalamt = new java.math.BigDecimal(tamt);
			   
			    PaymentHistory ph;
			    LibraryManager lm = new LibraryManager();
			    if(transId > 0){
		        	ph = new PaymentHistory();
		        	ph.setUserid(u.getId());
		        	ph.setCorp_id(corp.getId());
		        	ph.setCompany(corp.getCompanyId());
		        	ph.setAccess_code(accessCode);
		        	ph.setResponse(resp);
		        	ph.setInvoice_number(invoiceNumber);
		        	ph.setTotal_amount(totalamt);
		        	ph.setTrans_id(transId);
		        	ph.setTrans_status(transStatus);
		        	ph.setCreated(new Date());
		        	lm.savePaymentHistoryDetails(ph);
			    } 
			   System.out.println("Please Check Your Credit Card Details.");
			   session.setAttribute("eWayAccessCode", null);
			   String resErrorMessage = configResource.getString(responseMessage);
			   session.setAttribute("eWayErrorMessage", resErrorMessage);
			   
			    String txtCustomerRef = "";
				String ddlTitle = "Mr.";
				String txtFirstName = u.getFirstName();
				String txtLastName = u.getLastName();
				String txtCompanyName = corp.getName();
				String txtJobDescription = "";
				String txtStreet1 = corp.getAddress1();
				String txtStreet2 = corp.getAddress2();
				String txtCity = corp.getSuburb();
				String txtState = corp.getState();
				String txtPostalcode = corp.getPostcode();
				String txtCountry = corp.getCountry();
				String txtEmail = u.getEmail();
				String txtPhone = u.getMobile();
				String txtMobile = u.getMobile();
				String txtComments = "";
				String txtFax = "";
				String txtUrl = "";
				
				String txtShippingFirstName = u.getFullName();
//	 			String txtShippingLastName = request.getParameter("txtShippingLastName");
				String txtShippingStreet1 = corp.getAddress1();
				String txtShippingStreet2 = corp.getAddress2();
				String txtShippingCity = corp.getSuburb();
				String txtShippingState = corp.getState();
				String txtShippingCountry = corp.getCountry();
				String txtShippingPostalCode = corp.getPostcode();
				String txtShippingEmail = u.getEmail();
				String txtShippingPhone = u.getMobile();
				
				BigDecimal pr = new BigDecimal(session.getAttribute("price").toString());
			    BigDecimal ta = new BigDecimal(session.getAttribute("tax").toString());
			    BigDecimal tt = new BigDecimal(session.getAttribute("totalamt").toString());
			    BigDecimal prmul = pr.multiply(new BigDecimal("100"));
			    BigDecimal tamul = ta.multiply(new BigDecimal("100"));
			    BigDecimal ttmul = tt.multiply(new BigDecimal("100"));
			    int prhd = Integer.valueOf(prmul.intValue());
			    int tahd = Integer.valueOf(tamul.intValue());
			    int tthd = Integer.valueOf(ttmul.intValue());
				    
				String txtAmount = String.valueOf(tthd);
				String txtInvoiceNumber = session.getAttribute("invoice_number").toString();
				String txtInvoiceDescription = "1 x "+session.getAttribute("plan_name").toString();
				String txtInvoiceReference = session.getAttribute("invoice_number").toString();
				String txtCurrencyCode = "AUD";
				
				String txtRedirectURL = "";
				if(Config.getString("https").equals("true")){
// 					txtRedirectURL = "https://"+corp.getSlug()+"."+Config.getString("baseDomain")+"/corp/subscription/checkout.jsp";
					txtRedirectURL = "https://"+Config.getString("baseDomain")+"/corp/subscription/checkout.jsp";
			    }else{
// 				    txtRedirectURL = "http://"+corp.getSlug()+"."+Config.getString("baseDomain")+"/corp/subscription/checkout.jsp";
			    	txtRedirectURL = "http://"+Config.getString("baseDomain")+"/corp/subscription/checkout.jsp";
			    }
				String ddlMethod = "TokenPayment";
				
				//Create AccessCode Request Object
				CreateAccessCodeRequest CreateAccessCodeRequest = new CreateAccessCodeRequest();
				
				//Populate values for Customer Object
			    //Note: TokenCustomerID is Required Field When Update an existing TokenCustomer
				CreateAccessCodeRequest.Customer.setReference(txtCustomerRef);
				CreateAccessCodeRequest.Customer.setFirstName(txtFirstName); 		//Note: FirstName is Required Field When Create/Update a TokenCustomer
				CreateAccessCodeRequest.Customer.setLastName(txtLastName);			//Note: LastName is Required Field When Create/Update a TokenCustomer
				CreateAccessCodeRequest.Customer.setReference(txtInvoiceReference);
				CreateAccessCodeRequest.Customer.setTitle(ddlTitle); 				//Note: Title is Required Field When Create/Update a TokenCustomer
				CreateAccessCodeRequest.Customer.setCompanyName(txtCompanyName);
				CreateAccessCodeRequest.Customer.setJobDescription(txtJobDescription);
				CreateAccessCodeRequest.Customer.setStreet1(txtStreet1);
				CreateAccessCodeRequest.Customer.setStreet2(txtStreet2);
				CreateAccessCodeRequest.Customer.setCity(txtCity);
				CreateAccessCodeRequest.Customer.setState(txtState);
				CreateAccessCodeRequest.Customer.setPostalCode(txtPostalcode);
				CreateAccessCodeRequest.Customer.setCountry(txtCountry);			//Note: Country is Required Field When Create/Update a TokenCustomer
				CreateAccessCodeRequest.Customer.setEmail(txtEmail);
				CreateAccessCodeRequest.Customer.setPhone(txtPhone);
				CreateAccessCodeRequest.Customer.setMobile(txtMobile);
				CreateAccessCodeRequest.Customer.setComments(txtComments);
				CreateAccessCodeRequest.Customer.setFax(txtFax);
				CreateAccessCodeRequest.Customer.setUrl(txtUrl);
				
				//Populate values for ShippingAddress Object.
			    //This values can be taken from a Form POST as well. Now is just some dummy data.
				CreateAccessCodeRequest.ShippingAddress.setFirstName(txtShippingFirstName);
//	 			CreateAccessCodeRequest.ShippingAddress.setLastName(txtShippingLastName);
				CreateAccessCodeRequest.ShippingAddress.setStreet1(txtShippingStreet1);
				CreateAccessCodeRequest.ShippingAddress.setStreet2(txtShippingStreet2);
				CreateAccessCodeRequest.ShippingAddress.setCity(txtShippingCity);
				CreateAccessCodeRequest.ShippingAddress.setState(txtShippingState);
				CreateAccessCodeRequest.ShippingAddress.setCountry(txtShippingCountry);
				CreateAccessCodeRequest.ShippingAddress.setPostalCode(txtShippingPostalCode);
				CreateAccessCodeRequest.ShippingAddress.setEmail(txtShippingEmail);
				CreateAccessCodeRequest.ShippingAddress.setPhone(txtShippingPhone);
				CreateAccessCodeRequest.ShippingAddress.setShippingMethod("Other");		//ShippingMethod, e.g. "LowCost", "International", "Military". Check the spec for available values.
				
				//Populate values for Payment Object
			    //Note: TotalAmount is a Required Field When Process a Payment, TotalAmount should set to "0" or leave EMPTY when Create/Update A TokenCustomer
				CreateAccessCodeRequest.Payment.setTotalAmount(txtAmount);
				CreateAccessCodeRequest.Payment.setInvoiceNumber(txtInvoiceNumber);
				CreateAccessCodeRequest.Payment.setInvoiceDescription(txtInvoiceDescription);
				CreateAccessCodeRequest.Payment.setInvoiceReference(txtInvoiceReference);
				CreateAccessCodeRequest.Payment.setCurrencyCode(txtCurrencyCode);
				
				//Populate values for LineItems
				LineItem lineItem1 = new LineItem();
				lineItem1.setSKU(session.getAttribute("plan_name").toString());
				lineItem1.setDescription("1 x "+session.getAttribute("plan_name").toString());
				lineItem1.setQuantity(1);
				lineItem1.setUnitCost(prhd);
				lineItem1.setTax(tahd);
				lineItem1.setTotal(tthd);
				
				CreateAccessCodeRequest.Items = new Items();
				CreateAccessCodeRequest.Items.LineItem[0] = lineItem1;
				
				//Url to the page for getting the result with an AccessCode
			    //Note: RedirectUrl is a Required Field For all cases
				CreateAccessCodeRequest.RedirectUrl = txtRedirectURL;
				
				//Method for this request. e.g. ProcessPayment, Create TokenCustomer, Update TokenCustomer & TokenPayment
				CreateAccessCodeRequest.Method = ddlMethod;
				CreateAccessCodeRequest.TransactionType = "Purchase";
				
				//Create RapidAPI Service
				RapidAPI rapidAPI = new RapidAPI();
				
				//Call RapidAPI
				CreateAccessCodeResponse CreateAccessCodeResponse = rapidAPI.CreateAccessCode(CreateAccessCodeRequest);
				System.out.println("sucess"+CreateAccessCodeResponse);
				
				//Check if any error returns
				if(CreateAccessCodeResponse.Errors == null || CreateAccessCodeResponse.Errors.equals("") || CreateAccessCodeResponse.Errors == "")
				{
					//Save result into Session. payment.jsp and results.jsp will retrieve this result from Session
					session.setAttribute("TotalAmount", txtAmount);
					session.setAttribute("InvoiceReference", txtInvoiceReference);
					session.setAttribute("ResponseText", CreateAccessCodeResponse);
				}
		   }
	}else{
		session.setAttribute("eWayErrorMessage", null);
	}
    
    if(request.getParameter("CreateNewAccessCodeMethod")!=null){
			   session.setAttribute("eWayAccessCode", null);
			   
			    String txtCustomerRef = "";
				String ddlTitle = "Mr.";
				String txtFirstName = u.getFirstName();
				String txtLastName = u.getLastName();
				String txtCompanyName = corp.getName();
				String txtJobDescription = "";
				String txtStreet1 = corp.getAddress1();
				String txtStreet2 = corp.getAddress2();
				String txtCity = corp.getSuburb();
				String txtState = corp.getState();
				String txtPostalcode = corp.getPostcode();
				String txtCountry = corp.getCountry();
				String txtEmail = u.getEmail();
				String txtPhone = u.getMobile();
				String txtMobile = u.getMobile();
				String txtComments = "";
				String txtFax = "";
				String txtUrl = "";
				
				String txtShippingFirstName = u.getFullName();
//	 			String txtShippingLastName = request.getParameter("txtShippingLastName");
				String txtShippingStreet1 = corp.getAddress1();
				String txtShippingStreet2 = corp.getAddress2();
				String txtShippingCity = corp.getSuburb();
				String txtShippingState = corp.getState();
				String txtShippingCountry = corp.getCountry();
				String txtShippingPostalCode = corp.getPostcode();
				String txtShippingEmail = u.getEmail();
				String txtShippingPhone = u.getMobile();
				
				BigDecimal pr = new BigDecimal(session.getAttribute("price").toString());
			    BigDecimal ta = new BigDecimal(session.getAttribute("tax").toString());
			    BigDecimal tt = new BigDecimal(session.getAttribute("totalamt").toString());
			    BigDecimal prmul = pr.multiply(new BigDecimal("100"));
			    BigDecimal tamul = ta.multiply(new BigDecimal("100"));
			    BigDecimal ttmul = tt.multiply(new BigDecimal("100"));
			    int prhd = Integer.valueOf(prmul.intValue());
			    int tahd = Integer.valueOf(tamul.intValue());
			    int tthd = Integer.valueOf(ttmul.intValue());
				    
				String txtAmount = String.valueOf(tthd);
				String txtInvoiceNumber = session.getAttribute("invoice_number").toString();
				String txtInvoiceDescription = "1 x "+session.getAttribute("plan_name").toString();
				String txtInvoiceReference = session.getAttribute("invoice_number").toString();
				String txtCurrencyCode = "AUD";
				
				String txtRedirectURL = "";
				if(Config.getString("https").equals("true")){
//	 				txtRedirectURL = "https://"+corp.getSlug()+"."+Config.getString("baseDomain")+"/corp/subscription/checkout.jsp";
					txtRedirectURL = "https://"+Config.getString("baseDomain")+"/corp/subscription/checkout.jsp";
			    }else{
//	 			    txtRedirectURL = "http://"+corp.getSlug()+"."+Config.getString("baseDomain")+"/corp/subscription/checkout.jsp";
			    	txtRedirectURL = "http://"+Config.getString("baseDomain")+"/corp/subscription/checkout.jsp";
			    }
				String ddlMethod = request.getParameter("CreateNewAccessCodeMethod");
				
				//Create AccessCode Request Object
				CreateAccessCodeRequest CreateAccessCodeRequest = new CreateAccessCodeRequest();
				
				//Populate values for Customer Object
			    //Note: TokenCustomerID is Required Field When Update an existing TokenCustomer
				CreateAccessCodeRequest.Customer.setReference(txtCustomerRef);
				CreateAccessCodeRequest.Customer.setFirstName(txtFirstName); 		//Note: FirstName is Required Field When Create/Update a TokenCustomer
				CreateAccessCodeRequest.Customer.setLastName(txtLastName);			//Note: LastName is Required Field When Create/Update a TokenCustomer
				CreateAccessCodeRequest.Customer.setReference(txtInvoiceReference);
				CreateAccessCodeRequest.Customer.setTitle(ddlTitle); 				//Note: Title is Required Field When Create/Update a TokenCustomer
				CreateAccessCodeRequest.Customer.setCompanyName(txtCompanyName);
				CreateAccessCodeRequest.Customer.setJobDescription(txtJobDescription);
				CreateAccessCodeRequest.Customer.setStreet1(txtStreet1);
				CreateAccessCodeRequest.Customer.setStreet2(txtStreet2);
				CreateAccessCodeRequest.Customer.setCity(txtCity);
				CreateAccessCodeRequest.Customer.setState(txtState);
				CreateAccessCodeRequest.Customer.setPostalCode(txtPostalcode);
				CreateAccessCodeRequest.Customer.setCountry(txtCountry);			//Note: Country is Required Field When Create/Update a TokenCustomer
				CreateAccessCodeRequest.Customer.setEmail(txtEmail);
				CreateAccessCodeRequest.Customer.setPhone(txtPhone);
				CreateAccessCodeRequest.Customer.setMobile(txtMobile);
				CreateAccessCodeRequest.Customer.setComments(txtComments);
				CreateAccessCodeRequest.Customer.setFax(txtFax);
				CreateAccessCodeRequest.Customer.setUrl(txtUrl);
				
				//Populate values for ShippingAddress Object.
			    //This values can be taken from a Form POST as well. Now is just some dummy data.
				CreateAccessCodeRequest.ShippingAddress.setFirstName(txtShippingFirstName);
//	 			CreateAccessCodeRequest.ShippingAddress.setLastName(txtShippingLastName);
				CreateAccessCodeRequest.ShippingAddress.setStreet1(txtShippingStreet1);
				CreateAccessCodeRequest.ShippingAddress.setStreet2(txtShippingStreet2);
				CreateAccessCodeRequest.ShippingAddress.setCity(txtShippingCity);
				CreateAccessCodeRequest.ShippingAddress.setState(txtShippingState);
				CreateAccessCodeRequest.ShippingAddress.setCountry(txtShippingCountry);
				CreateAccessCodeRequest.ShippingAddress.setPostalCode(txtShippingPostalCode);
				CreateAccessCodeRequest.ShippingAddress.setEmail(txtShippingEmail);
				CreateAccessCodeRequest.ShippingAddress.setPhone(txtShippingPhone);
				CreateAccessCodeRequest.ShippingAddress.setShippingMethod("Other");		//ShippingMethod, e.g. "LowCost", "International", "Military". Check the spec for available values.
				
				//Populate values for Payment Object
			    //Note: TotalAmount is a Required Field When Process a Payment, TotalAmount should set to "0" or leave EMPTY when Create/Update A TokenCustomer
				CreateAccessCodeRequest.Payment.setTotalAmount(txtAmount);
				CreateAccessCodeRequest.Payment.setInvoiceNumber(txtInvoiceNumber);
				CreateAccessCodeRequest.Payment.setInvoiceDescription(txtInvoiceDescription);
				CreateAccessCodeRequest.Payment.setInvoiceReference(txtInvoiceReference);
				CreateAccessCodeRequest.Payment.setCurrencyCode(txtCurrencyCode);
				
				//Populate values for LineItems
				LineItem lineItem1 = new LineItem();
				lineItem1.setSKU(session.getAttribute("plan_name").toString());
				lineItem1.setDescription("1 x "+session.getAttribute("plan_name").toString());
				lineItem1.setQuantity(1);
				lineItem1.setUnitCost(prhd);
				lineItem1.setTax(tahd);
				lineItem1.setTotal(tthd);
				
				CreateAccessCodeRequest.Items = new Items();
				CreateAccessCodeRequest.Items.LineItem[0] = lineItem1;
				
				//Url to the page for getting the result with an AccessCode
			    //Note: RedirectUrl is a Required Field For all cases
				CreateAccessCodeRequest.RedirectUrl = txtRedirectURL;
				
				//Method for this request. e.g. ProcessPayment, Create TokenCustomer, Update TokenCustomer & TokenPayment
				CreateAccessCodeRequest.Method = ddlMethod;
				CreateAccessCodeRequest.TransactionType = "Purchase";
				
				//Create RapidAPI Service
				RapidAPI rapidAPI = new RapidAPI();
				
				//Call RapidAPI
				CreateAccessCodeResponse CreateAccessCodeResponse = rapidAPI.CreateAccessCode(CreateAccessCodeRequest);
				System.out.println("sucess"+CreateAccessCodeResponse);
				
				//Check if any error returns
				if(CreateAccessCodeResponse.Errors == null || CreateAccessCodeResponse.Errors.equals("") || CreateAccessCodeResponse.Errors == "")
				{
					//Save result into Session. payment.jsp and results.jsp will retrieve this result from Session
					session.setAttribute("TotalAmount", txtAmount);
					session.setAttribute("InvoiceReference", txtInvoiceReference);
					session.setAttribute("ResponseText", CreateAccessCodeResponse);
				}
		   }

	String totalAmount = (String) session.getAttribute("TotalAmount");
	String invoiceReference = (String) session.getAttribute("InvoiceReference");
	CreateAccessCodeResponse CreateAccessCodeResponse = (CreateAccessCodeResponse) session.getAttribute("ResponseText");
	
	System.out.println("totalAmount: " + totalAmount);
	System.out.println("invoiceReference: " + invoiceReference);
	System.out.println("accessCode: " + CreateAccessCodeResponse.AccessCode);
	System.out.println("FormActionURL: " + CreateAccessCodeResponse.FormActionURL);
	
 
 String cardholder = request.getParameter("EWAY_CARDNAME");
 String cardno = request.getParameter("CARDNUMBER");
 String mm = request.getParameter("EWAY_CARDEXPIRYMONTH");
 String yy = request.getParameter("EWAY_CARDEXPIRYYEAR");
 String cardtype = request.getParameter("billing-cardtype");
 String cvv = request.getParameter("EWAY_CARDCVN");
 Long cardselectid = Long.parseLong(Util.notNull(request.getParameter("cardselectid"), "1"));
 boolean selectedcard = request.getParameter("selectcard"+cardselectid+"") != null && Boolean.parseBoolean(request.getParameter("selectcard"+cardselectid+""));
//  String selectedcardcvv = request.getParameter("billing-selectedcard-cvv"+cardselectid+"");

 if (request.getMethod().equalsIgnoreCase("get")) {

 } else {

     if(selectedcard == false){
	        /* if (cardholder.trim().length() == 0) {
	        	errorFields.put("EWAY_CARDNAME", "Please enter a card holder name");
	        } else if (cardno.trim().length() == 0) {
	        	errorFields.put("CARDNUMBER", "Please enter a card number");
	        } else if (!cardno.substring(0,2).startsWith("40") && !cardno.substring(0,2).startsWith("41") && !cardno.substring(0,2).startsWith("42") && !cardno.substring(0,2).startsWith("43")
	        		&& !cardno.substring(0,2).startsWith("44") && !cardno.substring(0,2).startsWith("45") && !cardno.substring(0,2).startsWith("46") && !cardno.substring(0,2).startsWith("47")
	        		&& !cardno.substring(0,2).startsWith("48") && !cardno.substring(0,2).startsWith("49") && !cardno.substring(0,2).startsWith("51") && !cardno.substring(0,2).startsWith("52")
	        		&& !cardno.substring(0,2).startsWith("53") && !cardno.substring(0,2).startsWith("54") && !cardno.substring(0,2).startsWith("55")) {
	        	errorFields.put("CARDNUMBER", "Please enter a valid Visa/Master card number");
	        } else if(mm.trim().length() == 0){
	        	errorFields.put("EWAY_CARDEXPIRYMONTH", "");
	        } else if(yy.trim().length() == 0){
	        	errorFields.put("EWAY_CARDEXPIRYYEAR", "");
	        } else if(cardtype.trim().length() == 0){
	        	errorFields.put("billing-cardtype", "Please select a card type");
	        } else if(cvv.trim().length() == 0){
	        	errorFields.put("EWAY_CARDCVN", "");
	        } */
     }else{
//      	if(selectedcardcvv.trim().length() == 0){
//          	errorFields.put("billing-selectedcard-cvv"+cardselectid+"", "");
//          }
     }

     if (errorFields.size() == 0) {
//      		response.sendRedirect("/corp/subscription/GetResponse?AccessCode="+CreateAccessCodeResponse.AccessCode+"");
     }
 }

 String cardChoices[] = {"", "Visa", "Mater Card"};
 String cardChoicesVal[] = {"", "Visa", "MC"};
 
 
    String[][] breadcrumbs;
    if(session.getAttribute("type").equals("sms")){
    	breadcrumbs = new String[][] {{"/corp/", corp.getName()}, {"/corp/subscription/", "Subscription"}, {"/corp/subscription/pricing_sms.jsp", "Pricing"}, {"/corp/subscription/cart.jsp?plan="+session.getAttribute("plan")+"&type=sms", "Cart"}, {null, "Checkout"}};
    }else{
    	breadcrumbs = new String[][] {{"/corp/", corp.getName()}, {"/corp/subscription/", "Subscription"}, {"/corp/subscription/pricing.jsp", "Pricing"}, {"/corp/subscription/cart.jsp?plan="+session.getAttribute("plan")+"&type=upgrade", "Cart"}, {null, "Checkout"}};
    }

%>
<html>
<head><title>Checkout</title>
<style type="text/css">
#shopp .submit {
    text-align: right;
}
.entry p {
    margin: 0 0 20px;
}

.btn-click, #content .btn-click, input[type="submit"], input[type="reset"], input[type="button"], button, .txt-area .entry a.btn-click {
    background: #db6e00 none repeat scroll 0 0;
    border: 1px solid #000;
    border-radius: 5px;
    color: #fff;
    display: inline-block;
    font-size: 18px;
    height: 40px;
    line-height: 40px;
    padding: 0 24px;
    position: relative;
    text-decoration: none;
    vertical-align: top;
}
.btn-click:hover, .btn-login:hover, input[type="submit"]:hover, input[type="reset"]:hover, input[type="button"]:hover, button:hover, #content .btn-click:hover, .txt-area .entry a.btn-click:hover {
    background: #ed9944 none repeat scroll 0 0;
    text-decoration: none;
    color: #fff;
    text-shadow: 0 0 0 rgba(255, 255, 255, 0.75) !important;
}

#shopp {
    font-size: 14px;
}
#shopp {
    position: relative;
}
*::-moz-selection {
    background: #2d8bd8 none repeat scroll 0 0;
    color: #fff;
    text-shadow: none;
}
.entry {
    font-size: 16px;
    line-height: 19px;
}

#shopp #checkout, #shopp #checkout > ul, #shopp #checkout > ul ul, #shopp #checkout > ul li {
    margin: 0;
    padding: 0;
    text-align: left;
}
#shopp form {
    text-align: left;
}

#shopp #cart {
    margin: 0 0 15px;
}
#shopp #cart {
    width: 100%;
}
#shopp div.description, #shopp .products, #shopp .products ul, #shopp .products li.row, #shopp .category, #shopp #cart, #shopp #cart table {
    overflow: hidden;
}

#shopp #cart table {
    padding: 10px 0;
    width: 100%;
}
#shopp div.description, #shopp .products, #shopp .products ul, #shopp .products li.row, #shopp .category, #shopp #cart, #shopp #cart table {
    overflow: hidden;
}
#shopp form table, #shopp form table td {
    border: medium none;
}
table {
    border-collapse: collapse;
    border-spacing: 0;
}

#shopp #cart .money, #shopp #cart .totals th, #shopp #cart .buttons td {
    text-align: right;
    white-space: nowrap;
}

#shopp #cart .totals.total th, #shopp #cart .totals.total td {
    font-size: 130%;
}
#shopp #cart .money, #shopp #cart .totals th, #shopp #cart .buttons td {
    text-align: right;
    white-space: nowrap;
}
#shopp #cart th, #shopp #cart td {
    padding: 3px;
}
#shopp #cart th {
    font-weight: bold;
    text-align: left;
}
.form-horizontal .control-label {
    float: left;
    margin: 0 0 5px;
    padding-top: 5px;
    text-align: left;
    width: 140px;
}
.form-horizontal .controls {
	margin: 0 !important;
}
fieldset {
    border: 0 none;
    margin: 0;
    padding: 0;
    width: 50% !important;
    float: left;
}
</style>
<script type="text/javascript" src="/js/creditcard.js"></script>
<script type="text/javascript">
			$( document ).ready(function() {
				$("#selectval").val(0);
				if(<%=selectedcard%> == true){
					$("#carddetails").hide();
					$("#checkout-button").hide();
					$("#checkout-button-JSON").show();
					var cardid = <%=cardselectid%>;
					$("#cardselectid").val(cardid);
					$('#selectcard'+cardid+'').attr("checked",true);
				}
				if(<%=selectedcard%> == false){
					$("#carddetails").show();
					$("#cardselectid").val(0);
					$("#checkout-button").show();
					$("#checkout-button-JSON").hide();
				}
			});
			function cardchoose(j,tokencid) {
				$("#selectval").val(j);
// 				$("#EWAY_CARDNAME").val(name);
// 				$("#EWAY_CARDNUMBER").val(accno);
// 				$("#CARDNUMBER").val(accno);
// 				var date = new Date();
// 				var month = date.getMonth();
// 				var year = date.getFullYear();
// 				var expiryMonth = month + 1;
// 				var row = $('#billing-cardtype');
// 				var rowmm = $('#EWAY_CARDEXPIRYMONTH');
// 				var rowyy = $('#EWAY_CARDEXPIRYYEAR');
// 				var yyyy = year; 
// 				for(r=1;r<13;r++){
// 					rowmm.find("option[value="+r+"]").removeAttr('selected');
// 				}
			
// 				for(q=1;q<13;q++){
// 					rowyy.find("option[value="+yyyy+"]").removeAttr('selected');
// 					yyyy = yyyy + 1;
// 				}
// 				row.find("option[value="+cardtype+"]").attr('selected','selected');
// 				rowmm.find("option[value="+mm+"]").attr('selected','selected');
// 				rowyy.find("option[value="+yy+"]").attr('selected','selected');
				var selectcard = document.getElementById("selectcard"+j+"").checked;
				var rowCount = document.getElementById("CardDetailsTable").rows.length;
// 				var billingselectedcardcvv = document.getElementById("billingselectedcardcvv"+j+"");
// 	    		billingselectedcardcvv.className="control-group";
					if(selectcard == true){
						$("#carddetails").hide();
						$("#checkout-button").hide();
						$("#checkout-button-JSON").show();
						$("#tokencustomerid").val(tokencid);
						
					}
					else{
						$("#selectval").val(0);
						$("#carddetails").show();
						$("#checkout-button").show();
						$("#checkout-button-JSON").hide();
						$("#tokencustomerid").val("");
// 						$("#savecard").attr("checked", true);
						$("#EWAY_CARDNAME").val("");
						$("#EWAY_CARDNUMBER").val("");
						$("#CARDNUMBER").val("");
// 						var row = $('#billing-cardtype');
// 						var rowmm = $('#EWAY_CARDEXPIRYMONTH');
// 						var rowyy = $('#EWAY_CARDEXPIRYYEAR');
// 						row.find("option[value=]").attr('selected','selected');
// 						for(r=1;r<13;r++){
// 							rowmm.find("option[value="+r+"]").removeAttr('selected');
// 						}
					
// 						for(q=1;q<13;q++){
// 							rowyy.find("option[value="+yy+"]").removeAttr('selected');
// 							yy = yy + 1;
// 						}
// 						rowmm.find("option[value="+expiryMonth+"]").attr('selected','selected');
// 						rowyy.find("option[value="+year+"]").attr('selected','selected');
					}
					for(k=1;k<rowCount;k++){
						if(k == j && selectcard == true){
							$("#cardselectid").val(j);
							$('#selectcard'+k+'').attr("checked",true);
						}else{
							$("#cardselectid").val(1);
							$('#selectcard'+k+'').attr("checked",false);  
						}
					}
			}
    </script>
    <script type="text/javascript">
    function saveCard() {
    	
    }
    </script>
    <script type="text/javascript">
			function cardtype() {
				var cardnoval = $('#CARDNUMBER').val();
				var cardno = cardnoval.substring(0,2);
				var row = $('#billing-cardtype');
				if(cardno == 41 || cardno == 42 || cardno == 43 || cardno == 44 || cardno == 45 || cardno == 46 || cardno == 47 || cardno == 48 || cardno == 49 || cardno == 40){
					row.find("option[value=Visa]").attr('selected','selected');
				}else if(cardno == 51 || cardno == 52 || cardno == 53 || cardno == 54 || cardno == 55){
					row.find("option[value=MC]").attr('selected','selected');
				}else if(cardno != 41 || cardno != 42 || cardno != 43 || cardno != 44 || cardno != 45 || cardno != 46 || cardno != 47 || 
						cardno != 48 || cardno != 49 || cardno != 40 || cardno != 51 || cardno != 52 || cardno != 53 || cardno != 54 || cardno != 55 ){
					row.find("option[value=]").attr('selected','selected');
				}
			}
			function isNumeric(evt) {

		        document.getElementById('CARDNUMBER').addEventListener('input', function (e) {
				var ua = window.navigator.userAgent;
				var uasafari = navigator.userAgent.toLowerCase(); 
		        var msie = ua.indexOf("MSIE ");
		        var safari = uasafari.indexOf("safari");
		        	if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)){      // If Internet Explorer, return version number
// 	 		            alert('IE');
		        		$("#CARDNUMBER").attr('maxlength','16');
			        }else if(safari > 0){
			        	$("#CARDNUMBER").attr('maxlength','16');
			        }
			        else{                 // If another browser, return 0
// 	 		            alert('otherbrowser');
			        	$("#CARDNUMBER").attr('maxlength','19');
						  e.target.value = e.target.value.replace(/[^\dA-Z]/g, '').replace(/(.{4})/g, '$1 ').trim();
			        }
					}); 

				evt = (evt) ? evt : window.event;
			    var charCode = (evt.which) ? evt.which : evt.keyCode;
			    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
			        return false;
			    }
			    return true;
			};
</script>
<script type="text/javascript">
function validateForm() {
	var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");
	var j = $("#selectval").val();
	var selectcard = false;
	if(j > 0){
		selectcard = document.getElementById("selectcard"+j+"").checked;
// 		$("#savecard").attr("checked", false);
// 		savecard = document.getElementById("savecard").checked;
	}else{
// 		savecard = document.getElementById("savecard").checked;
	}
    var name = document.forms["checkout"]["EWAY_CARDNAME"].value;
    var cn = document.forms["checkout"]["CARDNUMBER"].value;
    $("#EWAY_CARDNUMBER").val(cn.replace(/\s/g, ''));
    var ewaycn = document.forms["checkout"]["EWAY_CARDNUMBER"].value;
    var mm = document.forms["checkout"]["EWAY_CARDEXPIRYMONTH"].value;
    var yy = document.forms["checkout"]["EWAY_CARDEXPIRYYEAR"].value;
    var ct = document.forms["checkout"]["billing-cardtype"].value;
    var cvv = document.forms["checkout"]["EWAY_CARDCVN"].value;
    
    var nameoncard = document.getElementById("nameoncard");
    var creditcardnumber = document.getElementById("creditcardnumber");
    var ewaycardexpirymonth = document.getElementById("ewaycardexpirymonth");
    var ewaycardexpiryyear = document.getElementById("ewaycardexpiryyear");
    var billingcardtype = document.getElementById("billingcardtype");
    var ewaycardcvn = document.getElementById("ewaycardcvn");
    var cardnoval = $('#CARDNUMBER').val();
    var cardno = cardnoval.substring(0,2);
    var myCardType = document.getElementById("billing-cardtype").value;
    if(myCardType == "Visa"){
    	myCardType = "Visa";
    	$("#billing-cardtype").val("Visa");
    }else if(myCardType == "MC"){
    	myCardType = "MasterCard";
    	$("#billing-cardtype").val("MC");
    }

    $("#inputeWayError").hide();
    $("#inputeWayError1").hide();
    $("#inputeWayError2").hide();
    $("#inputError1").hide();
    $("#inputError").hide();
    
    if(selectcard == true){
    	$("#inputError").hide();
    	$("#nameoncardError").hide();
    	$("#creditcardnumberError").hide();
    	$("#creditcardnumbertypeError").hide();
    	$("#ewaycardexpirymonthError").hide();
    	$("#ewaycardexpiryyearError").hide();
    	$("#billingcardtypeError").hide();
    	$("#ewaycardcvnError").hide();
    	$("#creditcard16digitError").hide();
    	$("#validcreditcardnumberError").hide();
    	nameoncard.className="control-group";
    	creditcardnumber.className="control-group";
    	ewaycardexpirymonth.className="control-group";
    	ewaycardexpiryyear.className="control-group";
    	billingcardtype.className="control-group";
    	ewaycardcvn.className="control-group";
    	if(j > 0){
    		var billingselectedcard = document.getElementById("billing-selectedcard-cvv"+j+"").value;
    		if(billingselectedcard == null || billingselectedcard == ""){
// 	    		var billingselectedcardcvv = document.getElementById("billingselectedcardcvv"+j+"");
// 	    		billingselectedcardcvv.className="control-group error";
	    		$("#EWAY_CARDCVN").val("");
	    		$("#ewayselectedcardcvnError"+j+"").show();
	    		$("#inputError").show();
	    		return false;
    		}else{
//     			var billingselectedcardcvv = document.getElementById("billingselectedcardcvv"+j+"");
//     			$("#EWAY_CARDCVN").val(billingselectedcard);
//         		billingselectedcardcvv.className="control-group";
        		$("#ewayselectedcardcvnError"+j+"").hide();
        		$("#inputError").hide();
        		return true;
    		};
    	}else{
//     		var billingselectedcardcvv = document.getElementById("billingselectedcardcvv"+j+"");
//     		billingselectedcardcvv.className="control-group";
    		$("#ewayselectedcardcvnError"+j+"").hide();
    		$("#inputError").hide();
    		return true;
    	};
    
    }else{
    if (name == null || name == "") {
    	$("#inputError").show();
    	nameoncard.className="control-group error";
    	$("#nameoncardError").show();
        return false;
    }else if(cn == null || cn == ""){
    	$("#inputError").show();
    	$("#nameoncardError").hide();
    	$("#validcreditcardnumberError").hide();
    	nameoncard.className="control-group";
    	creditcardnumber.className="control-group error";
    	var row = $('#billing-cardtype');
    	if(cn == null || cn == ""){
    		$("#creditcardnumberError").show();
    		$("#creditcardnumbertypeError").hide();
    	}else{
    		$("#creditcardnumberError").hide();
    		if(cardno == 41 || cardno == 42 || cardno == 43 || cardno == 44 || cardno == 45 || cardno == 46 || cardno == 47 || cardno == 48 || cardno == 49 || cardno == 40){
    			row.find("option[value=Visa]").attr('selected','selected');
    		}else if(cardno == 51 || cardno == 52 || cardno == 53 || cardno == 54 || cardno == 55){
    			row.find("option[value=MC]").attr('selected','selected');
    		}else if(cardno != 41 || cardno != 42 || cardno != 43 || cardno != 44 || cardno != 45 || cardno != 46 || cardno != 47 || 
    				cardno != 48 || cardno != 49 || cardno != 40 || cardno != 51 || cardno != 52 || cardno != 53 || cardno != 54 || cardno != 55 ){
    			row.find("option[value=]").attr('selected','selected');
    			$("#creditcardnumbertypeError").show();
    		};
    	}
    	return false;
    }
    else if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./) && cardnoval.length < 16){      // If Internet Explorer, return version number
//          alert('IE');
        	creditcardnumber.className="control-group error";
        	$("#creditcardnumberError").hide();
        	$("#creditcardnumbertypeError").hide();
        	$("#creditcard16digitError").show();
        	$("#validcreditcardnumberError").hide();
        	return false;
    }
    else if(msie == 0 && cardnoval.length < 19){                 // If another browser, return 0
//          alert('otherbrowser');
        	creditcardnumber.className="control-group error";
        	$("#creditcardnumberError").hide();
        	$("#creditcardnumbertypeError").hide();
        	$("#creditcard16digitError").show();
        	$("#validcreditcardnumberError").hide();
        	return false;
    }else if(cardno != 41 && cardno != 42 && cardno != 43 && cardno != 44 && cardno != 45 && cardno != 46 && cardno != 47 && cardno != 48 && cardno != 49 && cardno != 40 && cardno != 51 && cardno != 52 && cardno != 53 && cardno != 54 && cardno != 55){
    	creditcardnumber.className="control-group error";
    	$("#creditcardnumberError").hide();
    	$("#creditcard16digitError").hide();
    	$("#creditcardnumbertypeError").show();
    	$("#validcreditcardnumberError").hide();
    	return false;
    }
    
     else if(mm == 0 || mm == null){
    	$("#inputError").show();
    	$("#nameoncardError").hide();
    	$("#creditcardnumberError").hide();
    	$("#creditcardnumbertypeError").hide();
    	$("#creditcard16digitError").hide();
    	$("#validcreditcardnumberError").hide();
    	nameoncard.className="control-group";
    	creditcardnumber.className="control-group";
    	ewaycardexpirymonth.className="control-group error";
    	$("#ewaycardexpirymonthError").show();
        return false;
    }else if(yy == 0 || yy == null){
    	$("#inputError").show();
    	$("#nameoncardError").hide();
    	$("#creditcardnumberError").hide();
    	$("#creditcardnumbertypeError").hide();
    	$("#ewaycardexpirymonthError").hide();
    	$("#creditcard16digitError").hide();
    	$("#validcreditcardnumberError").hide();
    	nameoncard.className="control-group";
    	creditcardnumber.className="control-group";
    	ewaycardexpirymonth.className="control-group";
    	ewaycardexpiryyear.className="control-group error";
    	$("#ewaycardexpiryyearError").show();
        return false;
    } else if(ct == null || ct == ""){
    	$("#inputError").show();
    	$("#nameoncardError").hide();
    	$("#creditcardnumberError").hide();
    	$("#creditcardnumbertypeError").hide();
    	$("#ewaycardexpirymonthError").hide();
    	$("#ewaycardexpiryyearError").hide();
    	$("#creditcard16digitError").hide();
    	$("#validcreditcardnumberError").hide();
    	nameoncard.className="control-group";
    	creditcardnumber.className="control-group";
    	ewaycardexpirymonth.className="control-group";
    	ewaycardexpiryyear.className="control-group";
    	billingcardtype.className="control-group error";
    	$("#billingcardtypeError").show();
        return false;
    }else if (checkCreditCard (ewaycn,myCardType) == false) {
    	creditcardnumber.className="control-group error";
    	$("#creditcardnumberError").hide();
    	$("#creditcard16digitError").hide();
    	$("#creditcardnumbertypeError").hide();
    	$("#billingcardtypeError").hide();
    	$("#validcreditcardnumberError").show();
    	
    	return false;
    }else if(cvv == null || cvv == ""){
    	$("#inputError").show();
    	$("#nameoncardError").hide();
    	$("#creditcardnumberError").hide();
    	$("#creditcardnumbertypeError").hide();
    	$("#ewaycardexpirymonthError").hide();
    	$("#ewaycardexpiryyearError").hide();
    	$("#billingcardtypeError").hide();
    	$("#creditcard16digitError").hide();
    	$("#validcreditcardnumberError").hide();
    	nameoncard.className="control-group";
    	creditcardnumber.className="control-group";
    	ewaycardexpirymonth.className="control-group";
    	ewaycardexpiryyear.className="control-group";
    	billingcardtype.className="control-group";
    	ewaycardcvn.className="control-group error";
    	$("#ewaycardcvnError").show();
        return false;
    }else{
    	$("#inputError").hide();
    	$("#nameoncardError").hide();
    	$("#creditcardnumberError").hide();
    	$("#creditcardnumbertypeError").hide();
    	$("#ewaycardexpirymonthError").hide();
    	$("#ewaycardexpiryyearError").hide();
    	$("#billingcardtypeError").hide();
    	$("#ewaycardcvnError").hide();
    	$("#creditcard16digitError").hide();
    	$("#validcreditcardnumberError").hide();
    	nameoncard.className="control-group";
    	creditcardnumber.className="control-group";
//     	ewaycardexpirymonth.className="control-group";
//     	ewaycardexpiryyear.className="control-group";
    	billingcardtype.className="control-group";
    	ewaycardcvn.className="control-group";
    	
    	document.checkout.action = "<%=CreateAccessCodeResponse.FormActionURL%>";
        document.checkout.submit();
    };
    };
}

function isAlphabetswithDot(txt, e) {
	keyEntry = (e.keyCode) ? e.keyCode : e.which;
    if (((keyEntry >= '65') && (keyEntry <= '90')) || ((keyEntry >= '97') && (keyEntry <= '122')) || (keyEntry == '46') || keyEntry == '8' || keyEntry == '9' || keyEntry == '32' || keyEntry == '37' || keyEntry == '39')
        return true;
    else
        return false;
};
</script>
<script type="text/javascript">
$(document).ready(function() {
    $('#savecard').click(function(event) {
    	savecard = document.getElementById("savecard").checked;
    	if(savecard == true){
    		var action = "TokenPayment"; 
    	}else{
    		var action = "ProcessPayment"; 
    	}
//     	alert(action);
      $.ajax({
        url: 'checkout.jsp?CreateNewAccessCodeMethod='+action+'',
        type: 'POST',
        success: function(data) {
//      	   alert($(data).find("#EWAY_ACCESSCODE").val());
          $('#EWAY_ACCESSCODE').val($(data).find("#EWAY_ACCESSCODE").val());
        }
      });
    });
    
    $('#EWAY_CARDNAME').keyup(function(event) {
    	var EWAY_CARDNAME = document.getElementById("EWAY_CARDNAME").value;
    	var EWAY_CARDNUMBER = document.getElementById("CARDNUMBER").value;
//     	alert(EWAY_CARDNUMBER.replace(/ /g,''));
    	var EWAY_CARDEXPIRYMONTH = document.getElementById("EWAY_CARDEXPIRYMONTH").value;
    	var EWAY_CARDEXPIRYYEAR = document.getElementById("EWAY_CARDEXPIRYYEAR").value;
      $.ajax({
        url: 'save_carddetails.jsp?EWAY_SAVECARD=SAVE&EWAY_CARDNAME='+EWAY_CARDNAME+'&EWAY_CARDNUMBER='+EWAY_CARDNUMBER.replace(/ /g,'')+'&EWAY_CARDEXPIRYMONTH='+EWAY_CARDEXPIRYMONTH+'&EWAY_CARDEXPIRYYEAR='+EWAY_CARDEXPIRYYEAR+'',
        type: 'POST',
        success: function(data) {
//      	   alert($(data).find("#EWAY_ACCESSCODE").val());
//           $('#EWAY_ACCESSCODE').val($(data).find("#EWAY_ACCESSCODE").val());
        }
      });
    });
      $('#CARDNUMBER').keyup(function(event) {
      	var EWAY_CARDNAME = document.getElementById("EWAY_CARDNAME").value;
      	var EWAY_CARDNUMBER = document.getElementById("CARDNUMBER").value;
//       	alert(EWAY_CARDNUMBER.replace(/ /g,''));
      	var EWAY_CARDEXPIRYMONTH = document.getElementById("EWAY_CARDEXPIRYMONTH").value;
      	var EWAY_CARDEXPIRYYEAR = document.getElementById("EWAY_CARDEXPIRYYEAR").value;
        $.ajax({
          url: 'save_carddetails.jsp?EWAY_SAVECARD=SAVE&EWAY_CARDNAME='+EWAY_CARDNAME+'&EWAY_CARDNUMBER='+EWAY_CARDNUMBER.replace(/ /g,'')+'&EWAY_CARDEXPIRYMONTH='+EWAY_CARDEXPIRYMONTH+'&EWAY_CARDEXPIRYYEAR='+EWAY_CARDEXPIRYYEAR+'',
          type: 'POST',
          success: function(data) {
//        	   alert($(data).find("#EWAY_ACCESSCODE").val());
//             $('#EWAY_ACCESSCODE').val($(data).find("#EWAY_ACCESSCODE").val());
          }
        });
      });
        $('#EWAY_CARDEXPIRYMONTH').click(function(event) {
        	var EWAY_CARDNAME = document.getElementById("EWAY_CARDNAME").value;
        	var EWAY_CARDNUMBER = document.getElementById("CARDNUMBER").value;
//         	alert(EWAY_CARDNUMBER.replace(/ /g,''));
        	var EWAY_CARDEXPIRYMONTH = document.getElementById("EWAY_CARDEXPIRYMONTH").value;
        	var EWAY_CARDEXPIRYYEAR = document.getElementById("EWAY_CARDEXPIRYYEAR").value;
          $.ajax({
            url: 'save_carddetails.jsp?EWAY_SAVECARD=SAVE&EWAY_CARDNAME='+EWAY_CARDNAME+'&EWAY_CARDNUMBER='+EWAY_CARDNUMBER.replace(/ /g,'')+'&EWAY_CARDEXPIRYMONTH='+EWAY_CARDEXPIRYMONTH+'&EWAY_CARDEXPIRYYEAR='+EWAY_CARDEXPIRYYEAR+'',
            type: 'POST',
            success: function(data) {
//          	   alert($(data).find("#EWAY_ACCESSCODE").val());
//               $('#EWAY_ACCESSCODE').val($(data).find("#EWAY_ACCESSCODE").val());
            }
          });
        });
          $('#EWAY_CARDEXPIRYYEAR').click(function(event) {
          	var EWAY_CARDNAME = document.getElementById("EWAY_CARDNAME").value;
          	var EWAY_CARDNUMBER = document.getElementById("CARDNUMBER").value;
//           	alert(EWAY_CARDNUMBER.replace(/ /g,''));
          	var EWAY_CARDEXPIRYMONTH = document.getElementById("EWAY_CARDEXPIRYMONTH").value;
          	var EWAY_CARDEXPIRYYEAR = document.getElementById("EWAY_CARDEXPIRYYEAR").value;
            $.ajax({
              url: 'save_carddetails.jsp?EWAY_SAVECARD=SAVE&EWAY_CARDNAME='+EWAY_CARDNAME+'&EWAY_CARDNUMBER='+EWAY_CARDNUMBER.replace(/ /g,'')+'&EWAY_CARDEXPIRYMONTH='+EWAY_CARDEXPIRYMONTH+'&EWAY_CARDEXPIRYYEAR='+EWAY_CARDEXPIRYYEAR+'',
              type: 'POST',
              success: function(data) {
//            	   alert($(data).find("#EWAY_ACCESSCODE").val());
//                 $('#EWAY_ACCESSCODE').val($(data).find("#EWAY_ACCESSCODE").val());
              }
            });
    });
    
    $('#selectcard1').click(function(event) {
    	var EWAY_CARDNAME = document.getElementById("EWAY_CARDNAME").value;
    	var EWAY_CARDNUMBER = document.getElementById("EWAY_CARDNUMBER").value;
    	var EWAY_CARDEXPIRYMONTH = document.getElementById("EWAY_CARDEXPIRYMONTH").value;
    	var EWAY_CARDEXPIRYYEAR = document.getElementById("EWAY_CARDEXPIRYYEAR").value;
      $.ajax({
        url: 'save_carddetails.jsp?EWAY_SAVECARD=null',
        type: 'POST',
        success: function(data) {
//      	   alert($(data).find("#EWAY_ACCESSCODE").val());
//           $('#EWAY_ACCESSCODE').val($(data).find("#EWAY_ACCESSCODE").val());
        }
      });
    });
  });
</script>
</head>
<body>
  <div class="page-header">
    <h1>Checkout</h1>
  </div>
  <%@include file="/WEB-INF/includes/breadcrumbs.jsp"%>
  <%@ include file="/WEB-INF/includes/formError.jsp" %>
  <div class="alert alert-error" id="inputError1" style="display: none;">
	<p>There were some problems with your input, please check the form for details.</p>
  </div>
  <%if(session.getAttribute("eWayErrorMessage") != null){%>
  <div id="inputeWayError" class="alert alert-error">
	<p>There were some problems with your input, please check the form for details.</p>
  </div>
  <%} %>
  <div class="span8 well">
  <form class="form-horizontal" method="post" name="checkout" id="checkout" style="float: left; width: 100%;">
  <div id="shopp" class="grid checkout">
<div id="checkout" class="shopp validate">
<div id="cart" class="shopp">
<%
Connection conn = Util.getConnection();
if(session.getAttribute("type").equals("sms")){ %>
<%
	      		PreparedStatement pst1;
	            pst1 = conn.prepareStatement("select id,plan_name,currency_type,tax,price,invoice_number from plan_price where id = ?");
	            pst1.setLong(1, Long.parseLong(session.getAttribute("Cart_Plan").toString()));
	            ResultSet rs1 = pst1.executeQuery();
	            while(rs1.next()){ %>
<table>
<tbody>
<tr>
<th class="item" scope="col">Cart Items</th>
<th scope="col">Quantity</th>
<th class="money" scope="col">Item Price</th>
<th class="money" scope="col">Item Total</th>
</tr>
<tr>
<td>
<a href="/corp/subscription/pricing_sms.jsp"><%=rs1.getString("plan_name")%></a>
<%session.setAttribute("plan_name", rs1.getString("plan_name"));
  session.setAttribute("invoice_number", rs1.getString("invoice_number"));%>
</td>
<td>1</td>
<td class="money">
<%	
	java.math.BigDecimal price = rs1.getBigDecimal("price");
	
	if(session.getAttribute("discount") != null){
		java.math.BigDecimal discount = new BigDecimal(session.getAttribute("discount").toString());
		java.math.BigDecimal pricePercentage = price.multiply(discount);
		java.math.BigDecimal priceDivHundred = pricePercentage.divide(new BigDecimal("100"));
		price = price.subtract(priceDivHundred);
	}else{
		price = rs1.getBigDecimal("price");
	}
	
%>
					<%if(rs1.getString("currency_type").equals("AUD")){%>
              			$<%=price%>
              		<%}else if(rs1.getString("currency_type").equals("USD")){%>
              			$<%=price%>
              		<%}else if(rs1.getString("currency_type").equals("Euro")){%>
              			$<%=price%>
              		<%}else if(rs1.getString("currency_type").equals("Pounds")){%>
              			$<%=price%>
              		<%}%></td>
<td class="money">
<%	
	java.math.BigDecimal price1 = rs1.getBigDecimal("price");
	
	if(session.getAttribute("discount") != null){
		java.math.BigDecimal discount = new BigDecimal(session.getAttribute("discount").toString());
		java.math.BigDecimal pricePercentage = price1.multiply(discount);
		java.math.BigDecimal priceDivHundred = pricePercentage.divide(new BigDecimal("100"));
		price1 = price1.subtract(priceDivHundred);
	}else{
		price1 = rs1.getBigDecimal("price");
	}
	
%>
					<%if(rs1.getString("currency_type").equals("AUD")){%>
              			$<%=price1%>
              		<%}else if(rs1.getString("currency_type").equals("USD")){%>
              			$<%=price1%>
              		<%}else if(rs1.getString("currency_type").equals("Euro")){%>
              			$<%=price1%>
              		<%}else if(rs1.getString("currency_type").equals("Pounds")){%>
              			$<%=price1%>
              		<%}%></td>
</tr>
<tr class="totals">
<td rowspan="5" colspan="2"> </td>
<th scope="row">Subtotal</th>
<td class="money">
<span class="shopp-cart cart-subtotal">
<%	
	java.math.BigDecimal price2 = rs1.getBigDecimal("price");

	if(session.getAttribute("discount") != null){
		java.math.BigDecimal discount = new BigDecimal(session.getAttribute("discount").toString());
		java.math.BigDecimal pricePercentage = price2.multiply(discount);
		java.math.BigDecimal priceDivHundred = pricePercentage.divide(new BigDecimal("100"));
		price2 = price2.subtract(priceDivHundred);
	}else{
		price2 = rs1.getBigDecimal("price");
	}
	
%>
					<%if(rs1.getString("currency_type").equals("AUD")){%>
              			$<%=price2%>
              		<%}else if(rs1.getString("currency_type").equals("USD")){%>
              			$<%=price2%>
              		<%}else if(rs1.getString("currency_type").equals("Euro")){%>
              			$<%=price2%>
              		<%}else if(rs1.getString("currency_type").equals("Pounds")){%>
              			$<%=price2%>
              		<%}%></span>
</td>
</tr>
<tr class="totals">
<th scope="row">Taxes</th>
<td class="money">
<span class="shopp-cart cart-tax">
					<%if(rs1.getString("currency_type").equals("AUD")){%>
              			$<%=rs1.getBigDecimal("tax")%>
              		<%}else if(rs1.getString("tax").equals("USD")){%>
              			$<%=rs1.getBigDecimal("price")%>
              		<%}else if(rs1.getString("tax").equals("Euro")){%>
              			$<%=rs1.getBigDecimal("price")%>
              		<%}else if(rs1.getString("currency_type").equals("Pounds")){%>
              			$<%=rs1.getBigDecimal("tax")%>
              		<%}%></span>
</td>
</tr>
<tr class="totals total">
<th scope="row">Total</th>
<td class="money">
<span class="shopp-cart cart-total">
<%	
	java.math.BigDecimal tax = rs1.getBigDecimal("tax");
	java.math.BigDecimal price3 = rs1.getBigDecimal("price");
	java.math.BigDecimal total = new BigDecimal("0.00");
	java.math.BigDecimal totalforses = new BigDecimal("0.00");
	
	totalforses = tax.add(price3);
	
    BigDecimal prmul = price3.multiply(new BigDecimal("100"));
    BigDecimal tamul = tax.multiply(new BigDecimal("100"));
    BigDecimal ttmul = totalforses.multiply(new BigDecimal("100"));
    int prhd = Integer.valueOf(prmul.intValue());
    int tahd = Integer.valueOf(tamul.intValue());
    int tthd = Integer.valueOf(ttmul.intValue());
	    
	String txtAmount = String.valueOf(tthd);
	
	session.setAttribute("TotalAmountforRebiiling", txtAmount);

	if(session.getAttribute("discount") != null){
		java.math.BigDecimal discount = new BigDecimal(session.getAttribute("discount").toString());
		java.math.BigDecimal pricePercentage = price3.multiply(discount);
		java.math.BigDecimal priceDivHundred = pricePercentage.divide(new BigDecimal("100"));
		price3 = price3.subtract(priceDivHundred);
	}else{
		price3 = rs1.getBigDecimal("price");
	}
	
	total = tax.add(price3);
	session.setAttribute("totalamt", total);
	session.setAttribute("tax", tax);
	session.setAttribute("price", price3);
%>					
					<%if(rs1.getString("currency_type").equals("AUD")){%>
              			$<%=total%>
              		<%}else if(rs1.getString("currency_type").equals("USD")){%>
              			$<%=total%>
              		<%}else if(rs1.getString("currency_type").equals("Euro")){%>
              			$<%=total%>
              		<%}else if(rs1.getString("currency_type").equals("Pounds")){%>
              			$<%=total%>
              		<%}%></span>
</td>
</tr>
</tbody>
</table>
<%} pst1.close(); %>
<%}else if(session.getAttribute("type").equals("upgrade")){ %>
<%
	      		PreparedStatement pst1;
	            pst1 = conn.prepareStatement("select id,plan_name,currency_type,tax,price,invoice_number from plan_price where id = ?");
	            pst1.setLong(1, Long.parseLong(session.getAttribute("Cart_Plan").toString()));
	            ResultSet rs1 = pst1.executeQuery();
	            while(rs1.next()){ %>
<table>
<tbody>
<tr>
<th class="item" scope="col">Cart Items</th>
<th scope="col">Quantity</th>
<th class="money" scope="col">Item Price</th>
<th class="money" scope="col">Item Total</th>
</tr>
<tr>
<td>
<a href="/corp/subscription/pricing.jsp"><%=rs1.getString("plan_name")%></a>
<%session.setAttribute("plan_name", rs1.getString("plan_name"));
  session.setAttribute("invoice_number", rs1.getString("invoice_number"));%>
<p style="margin: 5px 0; font-size: 14px;">
<strong>Subscription:</strong>
</p>
<p>
					<%if(rs1.getString("currency_type").equals("AUD")){%>
              			$<%=rs1.getBigDecimal("price")%>
              		<%}else if(rs1.getString("currency_type").equals("USD")){%>
              			$<%=rs1.getBigDecimal("price")%>
              		<%}else if(rs1.getString("currency_type").equals("Euro")){%>
              			$<%=rs1.getBigDecimal("price")%>
              		<%}else if(rs1.getString("currency_type").equals("Pounds")){%>
              			$<%=rs1.getBigDecimal("price")%>
              		<%}%> per month.
<!-- <br> -->
<!-- Subscription rebilled unlimited times. -->
</p>
</td>
<td>1</td>
<td class="money">
<%	
	java.math.BigDecimal price4 = rs1.getBigDecimal("price");

	if(session.getAttribute("discount") != null){
		java.math.BigDecimal discount = new BigDecimal(session.getAttribute("discount").toString());
		java.math.BigDecimal pricePercentage = price4.multiply(discount);
		java.math.BigDecimal priceDivHundred = pricePercentage.divide(new BigDecimal("100"));
		price4 = price4.subtract(priceDivHundred);
	}else{
		price4 = rs1.getBigDecimal("price");
	}
	
%>
					<%if(rs1.getString("currency_type").equals("AUD")){%>
              			$<%=price4%>
              		<%}else if(rs1.getString("currency_type").equals("USD")){%>
              			$<%=price4%>
              		<%}else if(rs1.getString("currency_type").equals("Euro")){%>
              			$<%=price4%>
              		<%}else if(rs1.getString("currency_type").equals("Pounds")){%>
              			$<%=price4%>
              		<%}%></td>
<td class="money">
<%	
	java.math.BigDecimal price5 = rs1.getBigDecimal("price");
	
	if(session.getAttribute("discount") != null){
		java.math.BigDecimal discount = new BigDecimal(session.getAttribute("discount").toString());
		java.math.BigDecimal pricePercentage = price5.multiply(discount);
		java.math.BigDecimal priceDivHundred = pricePercentage.divide(new BigDecimal("100"));
		price5 = price5.subtract(priceDivHundred);
	}else{
		price5 = rs1.getBigDecimal("price");
	}
	
%>
					<%if(rs1.getString("currency_type").equals("AUD")){%>
              			$<%=price5%>
              		<%}else if(rs1.getString("currency_type").equals("USD")){%>
              			$<%=price5%>
              		<%}else if(rs1.getString("currency_type").equals("Euro")){%>
              			$<%=price5%>
              		<%}else if(rs1.getString("currency_type").equals("Pounds")){%>
              			$<%=price5%>
              		<%}%></td>
</tr>
<tr class="totals">
<td rowspan="5" colspan="2"> </td>
<th scope="row">Subtotal</th>
<td class="money">
<span class="shopp-cart cart-subtotal">
<%
	java.math.BigDecimal price6 = rs1.getBigDecimal("price");

	if(session.getAttribute("discount") != null){
		java.math.BigDecimal discount = new BigDecimal(session.getAttribute("discount").toString());
		java.math.BigDecimal pricePercentage = price6.multiply(discount);
		java.math.BigDecimal priceDivHundred = pricePercentage.divide(new BigDecimal("100"));
		price6 = price6.subtract(priceDivHundred);
	}else{
		price6 = rs1.getBigDecimal("price");
	}
	
%>
					<%if(rs1.getString("currency_type").equals("AUD")){%>
              			$<%=price6%>
              		<%}else if(rs1.getString("currency_type").equals("USD")){%>
              			$<%=price6%>
              		<%}else if(rs1.getString("currency_type").equals("Euro")){%>
              			$<%=price6%>
              		<%}else if(rs1.getString("currency_type").equals("Pounds")){%>
              			$<%=price6%>
              		<%}%></span>
</td>
</tr>
<tr class="totals">
<th scope="row">Taxes</th>
<td class="money">
<span class="shopp-cart cart-tax">
					<%if(rs1.getString("currency_type").equals("AUD")){%>
              			$<%=rs1.getBigDecimal("tax")%>
              		<%}else if(rs1.getString("currency_type").equals("USD")){%>
              			$<%=rs1.getBigDecimal("tax")%>
              		<%}else if(rs1.getString("currency_type").equals("Euro")){%>
              			$<%=rs1.getBigDecimal("tax")%>
              		<%}else if(rs1.getString("currency_type").equals("Pounds")){%>
              			$<%=rs1.getBigDecimal("tax")%>
              		<%}%></span>
</td>
</tr>
<tr class="totals total">
<th scope="row">Total</th>
<td class="money">
<span class="shopp-cart cart-total">
<%	
	java.math.BigDecimal tax1 = rs1.getBigDecimal("tax");
	java.math.BigDecimal price7 = rs1.getBigDecimal("price");
	java.math.BigDecimal total1 = new BigDecimal("0.00");
	java.math.BigDecimal totalforses = new BigDecimal("0.00");
	
	totalforses = tax1.add(price7);
	
    BigDecimal prmul = price7.multiply(new BigDecimal("100"));
    BigDecimal tamul = tax1.multiply(new BigDecimal("100"));
    BigDecimal ttmul = totalforses.multiply(new BigDecimal("100"));
    int prhd = Integer.valueOf(prmul.intValue());
    int tahd = Integer.valueOf(tamul.intValue());
    int tthd = Integer.valueOf(ttmul.intValue());
	    
	String txtAmount = String.valueOf(tthd);
	
	session.setAttribute("TotalAmountforRebiiling", txtAmount);

	if(session.getAttribute("discount") != null){
		java.math.BigDecimal discount = new BigDecimal(session.getAttribute("discount").toString());
		java.math.BigDecimal pricePercentage = price7.multiply(discount);
		java.math.BigDecimal priceDivHundred = pricePercentage.divide(new BigDecimal("100"));
		price7 = price7.subtract(priceDivHundred);
	}else{
		price7 = rs1.getBigDecimal("price");
	}
	
	total1 = tax1.add(price7);
	session.setAttribute("totalamt", total1);
	session.setAttribute("tax", tax1);
	session.setAttribute("price", price7);
%>					
					<%if(rs1.getString("currency_type").equals("AUD")){%>
              			$<%=total1%>
              		<%}else if(rs1.getString("currency_type").equals("USD")){%>
              			$<%=total1%>
              		<%}else if(rs1.getString("currency_type").equals("Euro")){%>
              			$<%=total1%>
              		<%}else if(rs1.getString("currency_type").equals("Pounds")){%>
              			$<%=total1%>
              		<%}%></span>
</td>
</tr>
</tbody>
</table>
<%} pst1.close();%>
<%}else { %>
	<div id="noItem" class="grid cart">
		<p class="warning" style="font-size: 20px; color: darkred;">There are currently no items in your shopping cart.</p>
  	</div>
<%} %>
</div>
</div>
</div>

<fieldset style="width: 100% !important;">
          <legend>Payment Information:</legend>
          <%if(session.getAttribute("eWayErrorMessage") != null){%>
          <div id="inputeWayError1" class="alert alert-error">
			<p>Please enter a valid credit card details.</p>
			<p><%=session.getAttribute("eWayErrorMessage")%>.</p>
			<%if(session.getAttribute("eWayErrorMessage1") != null){ %>
			<p><%=session.getAttribute("eWayErrorMessage1")%>.</p>
			<%} %>
			<%if(session.getAttribute("eWayErrorMessage2") != null){ %>
			<p><%=session.getAttribute("eWayErrorMessage2")%>.</p>
			<%} %>
			<%if(session.getAttribute("eWayErrorMessage3") != null){ %>
			<p><%=session.getAttribute("eWayErrorMessage3")%>.</p>
			<%} %>
			<%if(session.getAttribute("eWayErrorMessage4") != null){ %>
			<p><%=session.getAttribute("eWayErrorMessage4")%>.</p>
			<%} %>
			<%if(session.getAttribute("eWayErrorMessage5") != null){ %>
			<p><%=session.getAttribute("eWayErrorMessage5")%>.</p>
			<%} %>
		  </div>
		  <%} %>
          <div id="carddetails">
          <!-- The following fields are the ones that eWAY looks for in the POSTed data when the form is submitted. -->

                        <!-- This field should contain the access code received from eWAY -->
                        <input type='hidden' name='EWAY_ACCESSCODE' value="<%=CreateAccessCodeResponse.AccessCode%>" id="EWAY_ACCESSCODE"/>
                        <input type="hidden" name="EWAY_PAYMENTTYPE" value="Credit Card" />
                        <input type="hidden" name="EWAY_CARDNUMBER" id="EWAY_CARDNUMBER" />
        <div class="control-group" id="nameoncard">
			<label class="control-label" for="EWAY_CARDNAME">Name on Card</label>
		<div class="controls">
<%-- 		 <%if(cardno != null){ %> --%>
<%-- 			<input id="EWAY_CARDNAME" class="input-xlarge" type="text" value="<%=cardholder%>" name="EWAY_CARDNAME" maxlength="50" size="30" onkeypress="return isAlphabetswithDot(this,event);" placeholder="Name on Card"> --%>
<%-- 			<%}else{ %> --%>
			<input id="EWAY_CARDNAME" class="input-xlarge" type="text" name="EWAY_CARDNAME" maxlength="50" size="30" onkeypress="return isAlphabetswithDot(this,event);" placeholder="Name on Card">
<%-- 			<%}%> --%>
			<span class="help-inline" id="nameoncardError" style="display: none;">Please enter a card holder name</span>
		</div>
		</div>
		<div class="control-group" id="creditcardnumber" style="width: 460px; margin: 0; float: left;">
			<label class="control-label" for="CARDNUMBER">Credit Card Number</label>
		<div class="controls">
		<%if(cardno != null){%>
			<input id="CARDNUMBER" class="input-xlarge" type="text" autocomplete="off" onkeypress="return isNumeric(event);" onchange="cardtype();" placeholder="Card Number" value="<%=cardno%>" name="CARDNUMBER">
			<%}else{ %>
			<input id="CARDNUMBER" class="input-xlarge" type="text" autocomplete="off" onkeypress="return isNumeric(event);" onchange="cardtype();" placeholder="Card Number" value="" name="CARDNUMBER">
			<%}%>
			<span class="help-inline" id="creditcardnumberError" style="display: none; margin: 0 0 0 135px;">Please enter a credit card number</span>
			<span class="help-inline" id="creditcardnumbertypeError" style="display: none; margin: 0 0 0 135px;">Please enter a valid Visa/Master card number</span>
			<span class="help-inline" id="validcreditcardnumberError" style="display: none; margin: 0 0 0 135px;">Please enter a valid credit card number</span>
			<span class="help-inline" id="creditcard16digitError" style="display: none; margin: 0 0 0 135px;">Please enter a 16 digit card number</span>
		</div>
		</div>
		<!-- <div class="control-group" id="ewaycardexpirymonth" style="float: left; margin: 0; width: 130px; padding: 0px 0px 20px;">
			<label class="control-label" style="text-align: left; width: 50px;" for="EWAY_CARDEXPIRYMONTH">Expiry</label>
		<div class="controls" style="margin-left: 29px;">
			<input id="EWAY_CARDEXPIRYMONTH" class="input-mini" type="text" autocomplete="off" onkeypress="return isNumeric(event);" placeholder="MM" value="" name="EWAY_CARDEXPIRYMONTH" maxlength="2" size="2">
			<span class="help-inline" id="ewaycardexpirymonthError" style="display: none;"></span>
		</div>
		</div>
		<div class="control-group" id="ewaycardexpiryyear" style="margin-bottom: 0px;">
			<label class="control-label" style="display: none;" for="EWAY_CARDEXPIRYYEAR"></label>
		<div class="controls" style="margin-left: 0px; padding: 0 0 20px;">
			<input id="EWAY_CARDEXPIRYYEAR" class="input-mini" type="text" autocomplete="off" onkeypress="return isNumeric(event);" placeholder="YY" value="" name="EWAY_CARDEXPIRYYEAR" maxlength="2" size="2">
			<span class="help-inline" id="ewaycardexpiryyearError" style="display: none;"></span>
		</div>
		</div> -->
		<div class="control-group" id="ewaycardexpirymonth" style="float: left; margin: 0; width: 130px; padding: 0px 0px 20px;">
		<label class="control-label" style="text-align: left; width: 50px;">Expiry</label>
		<div class="controls" style="margin-left: 29px;">
		<select id="EWAY_CARDEXPIRYMONTH" class="input-mini" name="EWAY_CARDEXPIRYMONTH" >
		<%
                                	int expiryMonth = 1;
                                
                                		Date date = new Date();
                                		Calendar cal = Calendar.getInstance();
                                		expiryMonth = cal.get(Calendar.MONTH) + 1;
                                	
                                	for(int i = 0; i <= 12; i++)
                                	{
                                		if(i > 9){
                                			out.print("<option value='" + i + "'");
                                		}else if(i == 0){
                                			out.print("<option value='" + i + "'");
                                		}else{
                                			out.print("<option value='0" + i + "'");
                                		}
                                		if(mm != null){
                                    		if(Integer.parseInt(mm) == i)
                                    		{	
                                    			out.print(" selected='selected'");
                                    		}}
                                		
//                                 		else{
                                    		
//                                     		if(expiryMonth == i)
//                                     		{	
//                                     			out.print(" selected='selected'");
//                                     		}
//                                     		}
                                		
                                		if(i == 0){
                                			out.print(">MM</option>\n");
                                		}else{
                                			out.print(">" + i + "</option>\n");
                                		}
                                	}
                                %>
		</select>&nbsp;&nbsp;/
		</div>
		</div>
		<div class="control-group" id="ewaycardexpiryyear" style="margin-bottom: 0px;">
		<div class="controls" style="margin-left: 0px; padding: 0 0 20px;">
		<select id="EWAY_CARDEXPIRYYEAR" class="input-mini" name="EWAY_CARDEXPIRYYEAR" >
		 <%
                                	int currentYear = cal.get(Calendar.YEAR);
                                	int cardExpiryYear = 0;
                                	int expiryYear = currentYear + 11;
                                	int Year = currentYear - 1;
                                	currentYear = currentYear - 1;
                                	
                                	for(int i = currentYear; i <= expiryYear; i++)
                                	{
                                		if(currentYear == Year){
                                			out.print("<option value='0'");
                                		}else{
                                			out.print("<option value='" + i + "'");
                                		}
                                		
                                		if(yy != null){
                                    		if(Integer.parseInt(yy) == i)
                                    		{	
                                    			out.print(" selected='selected'");
                                    		}
                                    		}
                                		
//                                 		else{
                                    		
//                                     			if(cardExpiryYear == i)
//                                         		{	
//                                         			out.print(" selected='selected'");
//                                         		}
//                                     		}
                                		
                                		if(currentYear == Year){
                                			out.print(">YY</option>\n");
                                			Year = Year + 1;
                                		}else{
                                			out.print(">" + i + "</option>\n");
                                		}
                                	}
                                %>
		</select>
		</div>
		</div> 
		<div class="control-group" id="billingcardtype" style="width: 270px; float: left;">
			<label class="control-label">Card Type</label>
		<div class="controls" style="width: 260px;">
			<select id="billing-cardtype" class="input-mini" name="billing-cardtype" style="width:100px;">
				<option value=""></option>
				<option value="Visa">Visa</option>
				<option value="MC">Mater Card</option>
			</select>
			<span class="help-inline" id="billingcardtypeError" style="display: none; margin: 0 0 0 135px;">Please select a card type</span>
		</div>
		</div>
		<div class="control-group" id="ewaycardcvn" style="margin: 0; width: 495px; padding: 0px 0px 20px;">
			<label class="control-label" style="width:80px;" for="EWAY_CARDCVN">CVV</label>
		<div class="controls" style="margin-left: 0px;">
			<input id="EWAY_CARDCVN" class="input-mini" type="text" autocomplete="off" onkeypress="return isNumeric(event);" placeholder="CVV" name="EWAY_CARDCVN" maxlength="3" size="3">
			<span class="help-inline" id="ewaycardcvnError" style="display: none;"></span>
		</div>
		</div>
	    <div class="control-group">
				<label for="savecard">
					<input id="savecard" type="checkbox" name="savecard" onclick="saveCard();" value="yes" checked="checked" name="savecard" style="margin: -3px 0 0 0 !important;">
					Save this card for faster checkout
					</label>
	    </div>
<%--             <%=HTML.textInput("EWAY_CARDNAME","Name on Card","30","50", cardholder, null, errorFields)%> --%>
<%-- 			<%=HTML.textBillingCardInput("CARDNUMBER","Credit Card Number","19","19", cardno, null, errorFields, "Card Number", "cardtype();", "return isNumeric(event);")%> --%>
<%-- 			<%=HTML.textBillingCardMMInput("EWAY_CARDEXPIRYMONTH","Expiry","2","2", mm, null, errorFields, "MM", "return isNumeric(event);")%> --%>
<%-- 			<%=HTML.textBillingCardYYInput("EWAY_CARDEXPIRYYEAR","","2","2",yy, null, errorFields, "YY", "return isNumeric(event);")%> --%>
<%-- 			<%=HTML.selectCardTypeMini("billing-cardtype", "Card Type", cardChoicesVal, cardChoices, cardtype, null, errorFields, false)%> --%>
<%-- 			<%=HTML.textBillingCardCVVInput("EWAY_CARDCVN","CVV","4","4",cvv, null, errorFields, "CVV", "return isNumeric(event);")%> --%>
		  </div>
		<%
        PreparedStatement pst2 = conn.prepareStatement("select id,accountno,month,year,card_type,name_on_card from card_details where corp_id = ?");
        pst2.setLong(1, corp.getId());
        ResultSet rs2 = pst2.executeQuery();
        if(rs2.next() == false){
        %>
        <%}else{ %>
        <fieldset style="width: 100% !important;">
		<legend style="margin:0;">Saved Cards:</legend>
		<%if(session.getAttribute("eWaySavedCardErrorMessage") != null){%>
          <div id="inputeWayError2" class="alert alert-error">
			<p>Please enter a valid credit card details.</p>
			<p><%=session.getAttribute("eWaySavedCardErrorMessage")%>.</p>
			<%if(session.getAttribute("eWayErrorMessage1") != null){ %>
			<p><%=session.getAttribute("eWayErrorMessage1")%>.</p>
			<%} %>
			<%if(session.getAttribute("eWayErrorMessage2") != null){ %>
			<p><%=session.getAttribute("eWayErrorMessage2")%>.</p>
			<%} %>
			<%if(session.getAttribute("eWayErrorMessage3") != null){ %>
			<p><%=session.getAttribute("eWayErrorMessage3")%>.</p>
			<%} %>
			<%if(session.getAttribute("eWayErrorMessage4") != null){ %>
			<p><%=session.getAttribute("eWayErrorMessage4")%>.</p>
			<%} %>
			<%if(session.getAttribute("eWayErrorMessage5") != null){ %>
			<p><%=session.getAttribute("eWayErrorMessage5")%>.</p>
			<%} %>
			</div>
			<%} %>
	<input type="hidden" name="tokencustomerid" id="tokencustomerid" />
	<input type="hidden" name="cardselectid" id="cardselectid" />
	<table class="table" id="CardDetailsTable" style="width: 75%;">
    <tr>
        <th>Select Card</th>
    	<th>Account Number</th>
        <th>Card Type</th>
<!--         <th>CVV</th> -->
    </tr>
        <%
        conn.setAutoCommit(false);
        PreparedStatement pst1 = conn.prepareStatement("select id,tokencustomerid,accountno,month,year,card_type,name_on_card from card_details where corp_id = ?");
        pst1.setLong(1, corp.getId());
        ResultSet rs1 = pst1.executeQuery();
        int i = 1;
        while(rs1.next()){
        	String accno = rs1.getString("accountno");
        	Long id = rs1.getLong("id");
        	Long tokenCid = rs1.getLong("tokencustomerid");
        %>
    <tr>
		<td>
		<input id="selectcard<%=i%>" type="checkbox" name="selectcard<%=i%>" onclick="cardchoose(<%=i%>,<%=tokenCid%>);" value="true">
		</td>
        <td><%=accno.substring(0,4)%>-XXXX-XXXX-<%=accno.substring(Math.max(accno.length() - 4, 0))%></td>
        <td><%if(rs1.getString("card_type").equals("MC")){%>Master Card<%}else{ %><%=rs1.getString("card_type")%><%} %></td>
       <%--  <td>
        <input type="hidden" name="selectval" id="selectval" />
        <div class="control-group" id="billingselectedcardcvv<%=i%>">
		<label class="control-label" style="display: none;" for="billing-selectedcard-cvv<%=i%>"></label>
		<div class="controls">
			<input id="billing-selectedcard-cvv<%=i%>" class="input-mini" type="text" autocomplete="off" onkeypress="return isNumeric(event);" placeholder="CVV" value="" name="billing-selectedcard-cvv<%=i%>" maxlength="3" size="3">
			<span class="help-inline" id="ewayselectedcardcvnError<%=i%>" style="display: none;"></span>
		</div>
		</div></td> --%>
  	</tr>

<%i = i +1;}conn.close();%>
</table>
<fieldset style="width: 100% !important;"><legend></legend></fieldset>
<%} %> 
</fieldset>
</fieldset>
<!-- <fieldset>
			 <div class="control-group">
				<label for="marketing">
					<input id="marketing" type="checkbox" value="yes" checked="checked" name="marketing" style="margin: -5px 0 0 0 !important;">
					Yes, I would like to receive e-mail updates and special offers!
					</label>
			 </div>
        </fieldset> -->
        
        <p class="submit" style="margin: 0 0 40px;">
			<span class="payoption-button payoption-0" style="display: inline; float: right;">
			<input id="checkout-button" class="checkout-button" type="submit" value="Submit Order" onclick="return validateForm();" name="process">
			<input id="checkout-button-JSON" class="checkout-button" type="submit" value="Submit Order" name="checkout-button-JSON" style="display: none;">
			</span>
		</p>
      </form>
    <div class="security_logo">
<div id="ewayBlock">
<div style="text-align: center">
<div>
<a title="eWAY - Online payments made easy" target="_eway" href="http://www.eway.com.au/secure-site-seal">
<img border="0" title="eWAY - Online payments made easy" alt="eWAY - Online payments made easy" src="https://www.eway.com.au/developer/payment-code/verified-seal.ashx?img=11&size=1">
</a>
</div>
<div style="text-align: center; color: #000;">
<a style="text-decoration: none; color: #000" title="Credit Card Processing" target="_eway" href="https://www.eway.com.au/secure-site-seal">Credit Card Processing</a>
|
<a style="text-decoration: none; color: #000" title="eCommerce Website" target="_eway" href="https://www.eway.com.au/secure-site-seal">eCommerce Website</a>
|
<a style="text-decoration: none; color: #000" title="Online Payments" target="_eway" href="https://www.eway.com.au/secure-site-seal">Online Payments</a>
</div>
</div>
</div>
</div>
    </div>
</body>
</html>