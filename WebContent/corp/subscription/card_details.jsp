<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="au.com.ourbodycorp.model.managers.CorporationManager" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.util.*" %>
<%@ page import="java.util.Date" %>
<%@ page import="com.eway.json.bean.CreateTokenRequest" %>
<%@ page import="com.eway.json.bean.DirectPaymentResponse" %>
<%@ page import="com.eway.process.RapidAPI" %>
<%@ page import="com.eway.xml.bean.Options" %>
<%@ page import="com.eway.xml.bean.Option" %>
<%@ page import="com.eway.xml.bean.LineItem" %>
<%@ page import="com.eway.xml.bean.Items" %>
<%@page import="java.util.Calendar"%>
<%@ page import="org.json.JSONObject" %>
<%@ page import="au.com.ourbodycorp.Config" %>
<%@page import="java.util.ResourceBundle"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
	CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");

    if (cu == null) {
        response.sendRedirect(Util.loginUrl(Constants.LoginReason.login, request));
        return;
    }
    
	final ResourceBundle configResource = ResourceBundle.getBundle("config");
	
	String redirectURL = configResource.getString("redirectURL");
	String method = configResource.getString("requestMethod");
	String format = "JSON";
	
    User u = cu.getUser();
    Corporation corp = cu.getCorporation();

//     String title = request.getParameter("ddlTitle");
    String cardholder = request.getParameter("billing-cardholder");
    String cardno = request.getParameter("billing-card");
    String mm = request.getParameter("billing-cardexpires-mm");
    String yy = request.getParameter("billing-cardexpires-yy");
    String cardtype = request.getParameter("billing-cardtype");
    String cvv = request.getParameter("EWAY_CARDCVN");

    HashMap<String, String> errorFields = new HashMap<String, String>();
    LibraryManager lm = new LibraryManager();
    CardDetails card = null;
    String userAgent = request.getHeader("User-Agent");
    String  user = userAgent.toLowerCase();
    
    session.setAttribute("mmError", null);
	session.setAttribute("yyError", null);
	
    if (request.getMethod().equalsIgnoreCase("post")) {
    	if (request.getParameter("submit").startsWith("Cancel")) {
            response.sendRedirect(corp.getHost() + "/corp/subscription/");
            return;
        } 
    	
    	session.setAttribute("eWayErrorMessage", null);
    	session.setAttribute("eWayErrorMessage1", null);
		session.setAttribute("eWayErrorMessage2", null);
		session.setAttribute("eWayErrorMessage3", null);
		session.setAttribute("eWayErrorMessage4", null);
		session.setAttribute("eWayErrorMessage5", null);
    	
        if (cardholder.trim().length() == 0) {
        	errorFields.put("billing-cardholder", "Please enter a card holder name");
        } else if (cardno.trim().length() == 0) {
        	errorFields.put("billing-card", "Please enter a card number");
        } else if (!cardno.substring(0,2).startsWith("40") && !cardno.substring(0,2).startsWith("41") && !cardno.substring(0,2).startsWith("42") && !cardno.substring(0,2).startsWith("43")
        		&& !cardno.substring(0,2).startsWith("44") && !cardno.substring(0,2).startsWith("45") && !cardno.substring(0,2).startsWith("46") && !cardno.substring(0,2).startsWith("47")
        		&& !cardno.substring(0,2).startsWith("48") && !cardno.substring(0,2).startsWith("49") && !cardno.substring(0,2).startsWith("51") && !cardno.substring(0,2).startsWith("52")
        		&& !cardno.substring(0,2).startsWith("53") && !cardno.substring(0,2).startsWith("54") && !cardno.substring(0,2).startsWith("55")) {
        	cardtype = "";
        	errorFields.put("billing-card", "Please enter a valid Visa/Master card number");
        } else if(user.contains("trident") == true && cardno.length() < 16){
        	errorFields.put("billing-card", "Please enter a 16 digit card number");
        } else if(user.contains("safari") == true && cardno.length() < 16){
        	errorFields.put("billing-card", "Please enter a 16 digit card number");
        } else if(user.contains("trident") != false && cardno.length() < 19){
        	errorFields.put("billing-card", "Please enter a 16 digit card number");
        } else if(!cardno.substring(0,2).startsWith("40") && !cardno.substring(0,2).startsWith("41") && !cardno.substring(0,2).startsWith("42") && !cardno.substring(0,2).startsWith("43")
        		&& !cardno.substring(0,2).startsWith("44") && !cardno.substring(0,2).startsWith("45") && !cardno.substring(0,2).startsWith("46") && !cardno.substring(0,2).startsWith("47")
        		&& !cardno.substring(0,2).startsWith("48") && !cardno.substring(0,2).startsWith("49") && !cardno.substring(0,2).startsWith("51") && !cardno.substring(0,2).startsWith("52")
        		&& !cardno.substring(0,2).startsWith("53") && !cardno.substring(0,2).startsWith("54") && !cardno.substring(0,2).startsWith("55")){
        	errorFields.put("billing-card", "Please enter a valid Visa/Master card number");
        } else if(lm.validCCNumber(cardno.replaceAll("\\W", "")) == false) {
 	      	errorFields.put("billing-card", "Please enter a valid credit card number");
 	    } else if(mm.equals("0")){
        	session.setAttribute("mmError", "Error");
        	session.setAttribute("yyError", null);
        	errorFields.put("billing-cardexpires-mm", "Please enter a valid credit card number");
        } else if(yy.equals("0")){
        	session.setAttribute("mmError", null);
        	session.setAttribute("yyError", "Error");
        	errorFields.put("billing-cardexpires-yy", "Please enter a valid credit card number");
        } else if(cvv == ""){
        	session.setAttribute("mmError", null);
        	session.setAttribute("yyError", null);
        	errorFields.put("EWAY_CARDCVN", "");
        } else if (cardno.substring(0,2).startsWith("4")) {
        	cardtype = "Visa";
        	session.setAttribute("mmError", null);
        	session.setAttribute("yyError", null);
        } else if (cardno.substring(0,2).startsWith("51") || cardno.substring(0,2).startsWith("52")
        		|| cardno.substring(0,2).startsWith("53") || cardno.substring(0,2).startsWith("54") || cardno.substring(0,2).startsWith("55")) {
        	cardtype = "MC";
        	session.setAttribute("mmError", null);
        	session.setAttribute("yyError", null);
        }

        if (errorFields.isEmpty()) {
        	
	String txtAmount = String.valueOf("0");
	
	String ddlMethod = "CreateTokenCustomer";
	
	//Create AccessCode Request Object
	CreateTokenRequest CreateTokenRequest = new CreateTokenRequest();
	
	//Populate values for Customer Object
		    //Note: TokenCustomerID is Required Field When Update an existing TokenCustomer
	CreateTokenRequest.Customer.setTitle("Mr."); 				//Note: Title is Required Field When Create/Update a TokenCustomer
	CreateTokenRequest.Customer.setFirstName(u.getFirstName()); 		//Note: FirstName is Required Field When Create/Update a TokenCustomer
	CreateTokenRequest.Customer.setLastName(u.getLastName());			//Note: LastName is Required Field When Create/Update a TokenCustomer
	CreateTokenRequest.Customer.setCountry(corp.getCountry().toLowerCase());			//Note: Country is Required Field When Create/Update a TokenCustomer
	
	//Populate values for Customer Object
	CreateTokenRequest.Customer.CardDetails.setName(cardholder); 
	CreateTokenRequest.Customer.CardDetails.setNumber(cardno.replaceAll("\\W", ""));
	CreateTokenRequest.Customer.CardDetails.setExpiryMonth(mm);
	CreateTokenRequest.Customer.CardDetails.setExpiryYear(yy);
	CreateTokenRequest.Customer.CardDetails.setCVN(cvv);
	
	//Populate values for Payment Object
		    //Note: TotalAmount is a Required Field When Process a Payment, TotalAmount should set to "0" or leave EMPTY when Create/Update A TokenCustomer
	CreateTokenRequest.Payment.setTotalAmount(txtAmount);
	
	//Url to the page for getting the result with an AccessCode
		    //Note: RedirectUrl is a Required Field For all cases
// 			CreateAccessCodeRequest.RedirectUrl = txtRedirectURL;
	
	//Method for this request. e.g. ProcessPayment, Create TokenCustomer, Update TokenCustomer & TokenPayment
	CreateTokenRequest.Method = ddlMethod;
	CreateTokenRequest.TransactionType = "Recurring";
	
	//Create RapidAPI Service
	RapidAPI rapidAPI = new RapidAPI();
	
	//Call RapidAPI
	String DirectPaymentResponse = rapidAPI.CreateToken(CreateTokenRequest);
	session.setAttribute("ResponceText", DirectPaymentResponse);
	String Error = DirectPaymentResponse.substring(DirectPaymentResponse.length() - 7);
	boolean Errors = Error.contains("null");

	//Check if any error returns
	if(Errors == true)
	{
		String Token = session.getAttribute("ResponceText").toString();
		String[] num = Token.split(",");
		String[] num1 = num[12].split(":");
		
		String[] nam = Token.split(",");
		String[] nam1 = nam[13].split(":");
		
		String[] mo = Token.split(",");
		String[] mo1 = mo[14].split(":");
		
		String[] ye = Token.split(",");
		String[] ye1 = ye[15].split(":");
		
		String[] cid = Token.split(",");
		String[] cid1 = cid[19].split(":");
		
		String Number = num1[3].replace("\"","");
		String Name = nam1[1].replace("\"","");
		String ExpiryMonth = mo1[1].replace("\"","");
		String ExpiryYear = ye1[1].replace("\"","");
		String TokenCusID = cid1[1].replace("\"","");
		
		session.setAttribute("eWayErrorMessage", null);
		session.setAttribute("eWayErrorMessage1", null);
		session.setAttribute("eWayErrorMessage2", null);
		session.setAttribute("eWayErrorMessage3", null);
		session.setAttribute("eWayErrorMessage4", null);
		session.setAttribute("eWayErrorMessage5", null);
		
		card = new CardDetails();
		card.setTokencustomerid(Long.parseLong(TokenCusID));
    	card.setAccountno(Number);
    	card.setMonth(Long.parseLong(ExpiryMonth));
    	card.setYear(Long.parseLong(ExpiryYear));
    	if (Number.substring(0,2).startsWith("4")) {
    		card.setCard_type("Visa");
        } else{
        	card.setCard_type("MC");
        }
    	card.setName_on_card(Name);
    	card.setCorp_id(corp.getId());
    	card.setCompany(corp.getCompanyId());
    	card.setUserid(u.getId());
    	card.setCreated(new Date());
    	lm.saveCardDetails(card);
    	
    	session.setAttribute("Success", "Success");
    	
	}else{
		JSONObject jsonObject = new JSONObject(DirectPaymentResponse);
		String[] ErrorCode = jsonObject.getString("Errors").split(",");
		String resErrorMessage = configResource.getString(ErrorCode[0]);
		session.setAttribute("eWayErrorMessage", resErrorMessage);
		for(int i = 1; i<ErrorCode.length;i++){
			session.setAttribute("eWayErrorMessage"+i+"", configResource.getString(ErrorCode[i]));
		}
		session.setAttribute("Success", null);
	}
	
    }
    }
    String cardChoices[] = {"", "Visa", "Mater Card"};
    String cardChoicesVal[] = {"", "Visa", "MC"};
    
    String tittleChoices[] = {"Mr.", "Miss", "Mrs."};
    String tittleChoicesVal[] = {"Mr.", "Miss", "Mrs."};
    
    String[][] breadcrumbs = {{"/corp/", corp.getName()}, {"/corp/subscription", "Subscription"}, {null, "Card Details"}};
%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head><title>Card Details</title>
<script type="text/javascript" src="/js/creditcard.js"></script>
<style type="text/css">
.form-horizontal .control-label {
    float: left;
    padding-top: 5px;
    text-align: left;
}
</style>
<script type="text/javascript">
function cardtype() {
	var cardnoval = document.getElementById('billing-card').value;
	var cardno = cardnoval.substring(0,2);
	var row = $('#billing-cardtype');
	if(cardno == 41 || cardno == 42 || cardno == 43 || cardno == 44 || cardno == 45 || cardno == 46 || cardno == 47 || cardno == 48 || cardno == 49 || cardno == 40){
		row.find("option[value=Visa]").attr('selected','selected');
	}else if(cardno == 51 || cardno == 52 || cardno == 53 || cardno == 54 || cardno == 55){
		row.find("option[value=MC]").attr('selected','selected');
	}else if(cardno != 41 || cardno != 42 || cardno != 43 || cardno != 44 || cardno != 45 || cardno != 46 || cardno != 47 || 
			cardno != 48 || cardno != 49 || cardno != 40 || cardno != 51 || cardno != 52 || cardno != 53 || cardno != 54 || cardno != 55 ){
		row.find("option[value=]").attr('selected','selected');
	}
}
function isNumeric(evt) {
	document.getElementById('billing-card').addEventListener('input', function (e) {
	var ua = window.navigator.userAgent;
	var uasafari = navigator.userAgent.toLowerCase(); 
    var msie = ua.indexOf("MSIE ");
    var safari = uasafari.indexOf("safari");
    	if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)){      // If Internet Explorer, return version number
// 	            alert('IE');
    		$("#billing-card").attr('maxlength','16');
        }else if(safari > 0){
//         	alert('safari');
        	$("#billing-card").attr('maxlength','16');
        }
        else{                 // If another browser, return 0
// 	            alert('otherbrowser');
        	$("#billing-card").attr('maxlength','19');
			  e.target.value = e.target.value.replace(/[^\dA-Z]/g, '').replace(/(.{4})/g, '$1 ').trim();
        }
	});
	evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
};

function isAlphabetswithDot(txt, e) {
	keyEntry = (e.keyCode) ? e.keyCode : e.which;
    if (((keyEntry >= '65') && (keyEntry <= '90')) || ((keyEntry >= '97') && (keyEntry <= '122')) || (keyEntry == '46') || keyEntry == '8' || keyEntry == '9' || keyEntry == '32')
        return true;
    else
        return false;
};

</script>
</head>
<body>
<div class="page-header">
    <h1>Card Details</h1>
</div>
<%@include file="/WEB-INF/includes/breadcrumbs.jsp"%>
<%@ include file="/WEB-INF/includes/formError.jsp" %>
  <%if(session.getAttribute("eWayErrorMessage") != null){%>
  <div id="inputeWayError" class="alert alert-error">
	<p>There were some problems with your input, please check the form for details.</p>
  </div>
  <%} %>
<div class="span8 well">
	<fieldset><legend>Card Details:</legend></fieldset>
    	<form class="form-horizontal" method="post" action="card_details.jsp">
    	<%if(session.getAttribute("eWayErrorMessage") != null){%>
          <div id="inputeWayError1" class="alert alert-error">
			<p>Please enter a valid credit card details.</p>
			<p><%=session.getAttribute("eWayErrorMessage")%>.</p>
			<%if(session.getAttribute("eWayErrorMessage1") != null){ %>
			<p><%=session.getAttribute("eWayErrorMessage1")%>.</p>
			<%} %>
			<%if(session.getAttribute("eWayErrorMessage2") != null){ %>
			<p><%=session.getAttribute("eWayErrorMessage2")%>.</p>
			<%} %>
			<%if(session.getAttribute("eWayErrorMessage3") != null){ %>
			<p><%=session.getAttribute("eWayErrorMessage3")%>.</p>
			<%} %>
			<%if(session.getAttribute("eWayErrorMessage4") != null){ %>
			<p><%=session.getAttribute("eWayErrorMessage4")%>.</p>
			<%} %>
			<%if(session.getAttribute("eWayErrorMessage5") != null){ %>
			<p><%=session.getAttribute("eWayErrorMessage5")%>.</p>
			<%} %>
		  </div>
		  <%} %>
<%--     		<%=HTML.select("ddlTitle", "Tittle", tittleChoicesVal, tittleChoices, title, null, errorFields, false)%> --%>
			<%=HTML.textBillingCardName("billing-cardholder","Name on Card","30","50", cardholder, null, errorFields, "Name on Card", "return isAlphabetswithDot(this,event);")%>
			<%=HTML.textBillingCardInput("billing-card","Credit Card Number","19","19", cardno, null, errorFields, "Card Number", "cardtype();", "return isNumeric(event);")%>
<%-- 			<%=HTML.textBillingCardMMInput("billing-cardexpires-mm","Expiry","2","2", mm, null, errorFields, "MM", "return isNumeric(event);")%> --%>
<%-- 			<%=HTML.textBillingCardYYInput("billing-cardexpires-yy","","2","2",yy, null, errorFields, "YY", "return isNumeric(event);")%> --%>
		<%if(session.getAttribute("mmError") != null){%>
			<div class="control-group error" style="float: left; margin: 0; width: 130px; padding: 0px 0px 20px;">
		<%}else{ %>
			<div class="control-group" style="float: left; margin: 0; width: 130px; padding: 0px 0px 20px;">
		<%} %>
		<label class="control-label" style="text-align: left; width: 50px;">Expiry</label>
		<div class="controls" style="margin-left: 29px;">
		<select id="billing-cardexpires-mm" class="input-mini" name="billing-cardexpires-mm" >
		<%
                                	int expiryMonth = 1;
                                
                                		Date date = new Date();
                                		Calendar cal = Calendar.getInstance();
                                		expiryMonth = cal.get(Calendar.MONTH) + 1;
                                	for(int i = 0; i <= 12; i++)
                                	{
                                		if(i > 9){
                                			out.print("<option value='" + i + "'");
                                		}else if(i == 0){
                                			out.print("<option value='" + i + "'");
                                		}else{
                                			out.print("<option value='0" + i + "'");
                                		}
                                		if(mm != null){
                                    		if(Integer.parseInt(mm) == i)
                                    		{	
                                    			out.print(" selected='selected'");
                                    		}}
                                		
//                                 		else{
                                    		
//                                     		if(expiryMonth == i)
//                                     		{	
//                                     			out.print(" selected='selected'");
//                                     		}
//                                     		}
                                		
                                		if(i == 0){
                                			out.print(">MM</option>\n");
                                		}else{
                                			out.print(">" + i + "</option>\n");
                                		}
                                	}
                                %>
		</select>&nbsp;&nbsp;/
		</div>
		<%if(session.getAttribute("mmError") != null){%>
			</div>
		<%}else{ %>
			</div>
		<%} %>
		
		<%if(session.getAttribute("yyError") != null){%>
			<div class="control-group error" style="margin-bottom: 0px;">
		<%}else{ %>
			<div class="control-group" style="margin-bottom: 0px;">
		<%} %>
		<div class="controls" style="margin-left: 0px; padding: 0 0 20px;">
		<select id="billing-cardexpires-yy" class="input-mini" name="billing-cardexpires-yy" >
		 <%
                                	int currentYear = cal.get(Calendar.YEAR);
                                	int cardExpiryYear = 0;
                                	int expiryYear = currentYear + 11;
                                	int Year = currentYear - 1;
                                	currentYear = currentYear - 1;
                                	
                                	for(int i = currentYear; i <= expiryYear; i++)
                                	{
                                		
                                		if(currentYear == Year){
                                			out.print("<option value='0'");
                                		}else{
                                			out.print("<option value='" + i % 100 + "'");
                                		}
                                		
                                		if(yy != null){
                                    		if(Integer.parseInt(yy) == i % 100)
                                    		{	
                                    			out.print(" selected='selected'");
                                    		}
                                    		}
                                		
//                                 		else{
                                    		
//                                     			if(cardExpiryYear == i)
//                                         		{	
//                                         			out.print(" selected='selected'");
//                                         		}
//                                     		}
                                		
                                		if(currentYear == Year){
                                			out.print(">YY</option>\n");
                                			Year = Year + 1;
                                		}else{
                                			out.print(">" + i + "</option>\n");
                                		}
                                		
                                	}
                                %>
		</select>
		</div>
		<%if(session.getAttribute("yyError") != null){%>
			</div>
		<%}else{ %>
			</div>
		<%} %>
	    <%=HTML.selectCardTypeMini("billing-cardtype", "Card Type", cardChoicesVal, cardChoices, cardtype, null, errorFields, false)%>
		<%=HTML.textBillingCardCVVInput("EWAY_CARDCVN","CVV","3","3",cvv, null, errorFields, "CVV", "return isNumeric(event);")%>
			 <fieldset>
			 </fieldset>
		<%if(session.getAttribute("Success") != null){ %>
				<span style="z-index: 1; position: absolute; margin: 40px 0px 0px 320px; color: green;">Card Saved Successfully...</span>
		<%} %>
		<div class="form-actions">
			<button class="btn btn-primary" value="Save_Card" type="submit" name="submit">Save Card</button>
			<button class="btn" value="Cancel" type="submit" name="submit">Cancel</button>
		</div>
	</form>
	</div>
		<%if(session.getAttribute("Success") != null){ 
				session.setAttribute("Success", null);
				response.sendRedirect("/corp/subscription/");
				return;
		}%>
</body>
</html>
</html>