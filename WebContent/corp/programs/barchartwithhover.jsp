<%@ page import="java.io.*" %>  
<%@ page import="java.util.*" %>  
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="java.net.URLEncoder" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.List" %>
<%@	page import="java.util.ArrayList"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%!  

  
    // --- String Join Function converts from Java array to javascript string.  
    public String join(ArrayList<?> arr, String del)  
    {  

        StringBuilder output = new StringBuilder();  

        for (int i = 0; i < arr.size(); i++)  
        {  

            if (i > 0) output.append(del);  

              // --- Quote strings, only, for JS syntax  
              if (arr.get(i) instanceof String) output.append("\"");  
              output.append(arr.get(i));  
              if (arr.get(i) instanceof String) output.append("\"");  
        }  

        return output.toString();  
    }  
%>
<html>  
<head>  
    <title>Simple JSP Demo</title>  
    <script type="text/javascript" src="https://cdn.zingchart.com/zingchart.min.js"></script>
    <script> zingchart.MODULESDIR = "https://cdn.zingchart.com/modules/";
		ZC.LICENSE = ["569d52cefae586f634c54f86dc99e6a9","ee6b7db5b51705a13dc2339db3edaf6d"];</script>  
</head>  
<body>  
    <script>  
        <% 
        long programId = Long.parseLong(Util.notNull(request.getParameter("id"), "0"));
        
        long pgmid = Long.parseLong(Util.notNull(request.getParameter("pgmid"), "0"));
        
        LibraryManager lm = new LibraryManager();
        ProgramExercise pe = lm.getProgramExercise(programId);
        if (pe == null) {
            response.sendError(404, "Program not found");
            return;
        }
        
        String from=null;
        Program program = lm.getProgram(pe.getProgramId());
        Patient p2=lm.getPatient(program.getPatientId());
    	if(programId>0){
    	from="ProgramName:"+program.getName()+";ExerciseName:"+pe.getName()+";Sets:"+pe.getSets()+";Reps:"+pe.getReps()+";Holds:"+pe.getHolds()+";Side:"+pe.getSide()+";Rest:"+pe.getRest()+";Description:"+pe.getDescription()+";";	
    	}
    	System.out.println("from"+from);
        System.out.println("programId"+programId);
        String[][] breadcrumbs = {{"/corp/programs/", "Pie Chart"}, {"/corp/programs/", "Barchart"}, {null, "Chart" + " Barchart"}};
        
           /*  ArrayList<String> months = new ArrayList<String>();  
            ArrayList<Integer> users = new ArrayList<Integer>();  

            int counter = 1;  
            while(counter < 31)  
            {  
                months.add("Aug " + counter);  
                users.add(counter=counter+2);  
            }   */
            
           /*  ArrayList<String> months = (ArrayList<String>)session.getAttribute("months");
            ArrayList<String> users = (ArrayList<String>)session.getAttribute("users"); */
            
            ArrayList<String> months = new ArrayList<String>();  
            ArrayList<Integer> users = new ArrayList<Integer>();  
            
            months.add("ITB Release on Roller ");  
            months.add("Gastrocs Stretch");  
            months.add("Lunge"); 
            months.add("ITB Release on Roller ");  
            months.add("Gastrocs Stretch");  
            months.add("Lunge"); 
            months.add("ITB Release on Roller ");  
            months.add("Gastrocs Stretch");  
            months.add("Lunge"); 
            months.add("ITB Release on Roller ");  
            months.add("Gastrocs Stretch");  
            months.add("Lunge"); 
            
            months.add("ITB Release on Roller ");  
            months.add("Gastrocs Stretch");  
            months.add("Lunge"); 
            months.add("ITB Release on Roller ");  
            months.add("Gastrocs Stretch");  
            months.add("Lunge"); 
            months.add("ITB Release on Roller ");  
            months.add("Gastrocs Stretch");  
            months.add("Lunge"); 
            months.add("ITB Release on Roller ");  
            months.add("Gastrocs Stretch");  
            months.add("Lunge"); 
            
            months.add("ITB Release on Roller ");  
            months.add("Gastrocs Stretch");  
            months.add("Lunge"); 
            months.add("ITB Release on Roller ");  
            months.add("Gastrocs Stretch");  
            months.add("Lunge"); 
            months.add("ITB Release on Roller ");  
            months.add("Gastrocs Stretch");  
            months.add("Lunge"); 
            months.add("ITB Release on Roller ");  
            months.add("Gastrocs Stretch");  
            months.add("Lunge"); 
            
        %>  
	
       // --- add a comma after each value in the array and convert to javascript string representing an array  
        var monthData = [<%= join(months, ",") %>];  
        var userData = [<%= join(users, ",") %>];  
 
    </script>
<script>  
window.onload = function() {  
  zingchart.render({
    id: "myChart",
    width: "100%",
    height: 400,
    data: {
      "type": "bar",
      "plot":{
    	    "hover-mode":"node",
    	    "hover-state":{
    	      "background-color":"purple green"
    	    }
    	  },
      "scale-x": {
        "labels": monthData
      },
     
      "series": [{
    	    "values": [3, 4, 11, 5, 19, 7]
    	  }, {
    	    "values": [9, 19, 15, 25, 12, 14]
    	  }, {
    	    "values": [15, 29, 19, 21, 25, 26]
    	  }]
    }
  
  });
};
</script>
<%@include file="/WEB-INF/includes/breadcrumbs.jsp"%>
	
<div class="page-header">
 <h1><%=program.getName()%></h1>
  </div>
  <div class="row">
    <div class="span6">
     <div id="myChart"></div>  
    </div>
  </div>
</body>  
</html>  



