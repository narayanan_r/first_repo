<%@page import="org.apache.velocity.runtime.directive.Parse"%>
<%@page import="java.util.Date"%>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="au.com.ourbodycorp.model.managers.UserManager" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.LinkedList" %>
<%@ page import="java.util.List" %>
<%@ page import="au.com.ourbodycorp.model.managers.CorporationManager" %>
<%@ page import="java.util.List" %>
<%@	page import="java.util.ArrayList"%>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.PreparedStatement" %>
<%@ page import="java.sql.*" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.util.Calendar"%>
<%
    CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");
	Corporation c = (Corporation) request.getAttribute("corp");
	boolean showLocation = request.getParameter("showLocation") != null && Boolean.parseBoolean(request.getParameter("showLocation"));
    String[] multitype = null;
    multitype = request.getParameterValues("multitype");
    long pgmid = Long.parseLong(request.getParameter("id"));
    User u = cu.getUser();
    Corporation corp = cu.getCorporation();
    String name = null;
    long patientId = Long.parseLong(Util.notNull(request.getParameter("patient"), "0"));
    long programId = Long.parseLong(Util.notNull(request.getParameter("id"), "0"));
    long seq = Long.parseLong(Util.notNull(request.getParameter("seq"), "0"));
    long groupId = Long.parseLong(Util.notNull(request.getParameter("group"), "0"));
    long uniqueId = 0;
    boolean showInfo = Boolean.parseBoolean(Util.notNull(request.getParameter("info"), "false"));
 
    if (programId == 0) {
        response.sendRedirect("/corp/patients/");
        return;
    }

    LibraryManager lm = new LibraryManager();

    Patient patient = null;
    PatientGroup patientGroup = null;
    Program program = null;

    if (patientId > 0) {
        patient = lm.getPatient(patientId);
    } 
    else if (programId > 0) {
        program = lm.getProgram(programId);
        if (program == null) {
        	response.sendError(404, "Program not found");
            return;
        }
        uniqueId = program.getShareid();
        session.setAttribute("uId", uniqueId);
        showLocation = program.isSharelocation();
        session.setAttribute("pgmName", program.getName());
        name = session.getAttribute("pgmName").toString();
        if (program != null) {
        	if(program.getPatientId() > 0){
        		session.setAttribute("patient_side", "patient_side");
        	}else{
            	session.setAttribute("patient_side", null);
            }
            patient = lm.getPatient(program.getPatientId());
            patientGroup = lm.getPatientGroup(program.getGroupId());
            if (patientGroup != null){
                groupId = patientGroup.getId();
            }
        }
    }
 
  /* if(patient != null){
		try{
	    	Util.allActivitiesLogs("Patients", u.getId(), corp.getName(), "View Program Details");
	    }catch(Exception e){
	    	
	    }
	}else if(patientGroup != null){
		try{
	    	Util.allActivitiesLogs("Patient Groups", u.getId(), corp.getName(), "View Program Details");
	    }catch(Exception e){
	    	
	    }
	}else{
		try{
	    	Util.allActivitiesLogs("Templates", u.getId(), corp.getName(), "View Program Details");
	    }catch(Exception e){
	    	
	    }
	} */
    if (program.getCorpId() != corp.getId()) {
        response.sendError(403, "Patient or program not part of this practice.");
        return;
    }
	

    User creator = null;
    List<ProgramExercise> pe = new LinkedList<ProgramExercise>();
    List<ProgramExercise> pe1 = new LinkedList<ProgramExercise>();
    List<MyInfo> myInfos = new LinkedList<MyInfo>();
	List<ProgramNote> notes = new LinkedList<ProgramNote>();

    if (program != null) {
    	List<Corporation> corpsid = null;
    	List<Long> Eidlist = new ArrayList<Long>();
        if (u != null) {
            CorporationManager cm = new CorporationManager();
            if(!name.equals("")){
            corpsid = lm.getCorpidforPrograms(name,corp.getCompanyId(),uniqueId,programId);
            }
        }
    	
        pe = lm.getProgramExercises(pgmid);
        myInfos = lm.listInfoForProgram(pgmid);
        notes = lm.getNotes(pgmid);
    
        UserManager um = new UserManager();
        creator = um.getUser(program.getCreator());
        if(patient != null){
        	String type="Patients";
            String activity_page="corp/programs/edit.jsp";
        	String corp_id=String.valueOf(corp.getId());
        	String corp_name=corp.getName();
        	String corp_loc=corp.getSuburb();
        	String user_id=String.valueOf(u.getId());
        	String user_name=u.getFullName();
        	String invitor_id=String.valueOf(patient.getId());
        	String invitor_name=patient.getFullName();
        	String flag="view";
        	String from="---";
        	String to="---";
        	String desc="View Program Details for "+patient.getFullName()+" By User";
            try
            {		
             Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
            }
            catch(Exception e)
            {
            } 
    	}else if(patientGroup != null){
    		String type="Patients Groups";
            String activity_page="corp/programs/edit.jsp";
        	String corp_id=String.valueOf(corp.getId());
        	String corp_name=corp.getName();
        	String corp_loc=corp.getSuburb();
        	String user_id=String.valueOf(u.getId());
        	String user_name=u.getFullName();
        	String invitor_id=String.valueOf(patientGroup.getId());
        	String invitor_name=patientGroup.getGroupName();
        	String flag="view";
        	String from="---";
        	String to="---";
        	String desc="View Program Details for "+patientGroup.getGroupName()+" By User";	
            try
            {		
             Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
            }
            catch(Exception e)
            {
            } 
    	}else{
            	String type="Templates";
                String activity_page="corp/programs/edit.jsp";
            	String corp_id=String.valueOf(corp.getId());
            	String corp_name=corp.getName();
            	String corp_loc=corp.getSuburb();
            	String user_id=String.valueOf(u.getId());
            	String user_name=u.getFullName();
            	String invitor_id=String.valueOf(program.getId());
            	String invitor_name="";
            	String flag="view";
            	String from="---";
            	String to="---";
            	String desc="View Program Details for By User";
                 try
                {		
                 Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
                }
                catch(Exception e)
                {
                } 
    	}

    }
    
    boolean followingTutorial = Boolean.parseBoolean(Util.notNull(request.getParameter("tutorial"), "false"));
    UserManager userManager = new UserManager();
    if (followingTutorial){
        userManager.setTutorialProgressForUser(u.getId(), UserManager.TUTORIAL_WENT_TO_PROGRAM_PAGE, false);
    }
    int tutorialProgress = userManager.getTutorialProgressForUser(u.getId());

    SimpleDateFormat sdf = new SimpleDateFormat("d MMM yyyy 'at' HH:mm");
    sdf.setTimeZone(corp.getTimeZone());

    SimpleDateFormat sdfNote = new SimpleDateFormat("d MMM yy");
    sdfNote.setTimeZone(corp.getTimeZone());

    String interval = "Not set";
    if (program.getInterval() > 0) {
        if (program.getIntervalType() == Program.IntervalType.hours) {
            if (program.getInterval() == 1){
                interval = String.format("Every hour", program.getInterval());
            }
            else {
                interval = String.format("Every %d hours", program.getInterval());
            }
        } 
        else if (program.getIntervalType() == Program.IntervalType.perDay){
            if (program.getInterval() == 1){
                interval = "Once per day";
            }
            else {
                interval = String.format("%d times per day", program.getInterval());
            }
        }
        else {
            if (program.getInterval() == 1){
                interval = "Once per week";
            }
            else {
                interval = String.format("%d times per week", program.getInterval());
            }
        }
    }
    
    HashMap<Long, Long> previous = new HashMap<Long, Long>();
    HashMap<Long, Long> next = new HashMap<Long, Long>();
    long prev = 0;
    for (ProgramExercise e : pe) {
        next.put(prev, e.getId());
        if (prev != 0) {
            previous.put(e.getId(), prev);
        }
        prev = e.getId();
    }

    String bcPatient[][] = {{"/corp/", corp.getName()}, {"/corp/patients/", "Patients"}, {"/corp/patients/detail.jsp?id=" + ((patient != null) ? patient.getId() : "0"), ((patient != null) ? patient.getFullName() : "")}, {null, program.getName()}};
    String bcTemplate[][] = {{"/corp/", corp.getName()}, {"/corp/programs/templates.jsp", "Program Templates"}, {null, program.getName()}};
    String bcGroup[][] = {{"/corp/", corp.getName()}, {"/corp/groups/", "Patient Groups"}, {"/corp/groups/detail.jsp?id=" + ((patientGroup != null) ? patientGroup.getId() : "0"), ((patientGroup != null) ? patientGroup.getGroupName() : "")}, {null, program.getName()}};
    String[][] breadcrumbs = bcTemplate;
    if (patient != null){
        breadcrumbs = bcPatient;
    }
    else if (patientGroup != null){
        breadcrumbs = bcGroup;
    }else{
        breadcrumbs = bcTemplate;
    }
    if (request.getMethod().equalsIgnoreCase("post")){
    	String exerciseId = request.getParameter("excerciseid");
    	if(exerciseId!=null){
    	    String sets = request.getParameter("sets");
    	    String reps =  request.getParameter("reps");
    	    String holds =  request.getParameter("holds");
    	    String side=request.getParameter("side");
    	    ProgramExercise programExercise = lm.getProgramExercise(Long.valueOf(exerciseId));
    	  //  programExercise.setSide(side);

    	    programExercise.setSets(Integer.parseInt(sets));
    	    if(reps != null && Long.valueOf(reps) > 0){
    	    	programExercise.setReps(Integer.parseInt(reps));
    	    }else{
    	    	programExercise.setHolds(Integer.parseInt(holds));
    	    }
    	    programExercise.setSide(side);
            //update the programs last updated date
            program.setLastUpdated(new Date());
            program = lm.save(program);
            lm.save(programExercise);
            pe = lm.getProgramExercises(programId);
	    	response.sendRedirect("edit.jsp?id="+programId);
	    	return;
    	}
    }
	int sumofadforthisweek=0;
 	int sumofadforpastweek=0;
 	int sumoffreqforthisweek=0;
 	int sumoffreqforpastweek=0;
 	float avgofpainlevelforthisweek=0;
 	float avgofpainlevelforpastweek=0;
 	float recenttpainlevelforthisweek=0;
 	int recentadherenceforthisweek=0;
 	int recentfreqforthisweek=0;
 	String avgofpainlevelforthisweekfloat=null;
	String avgofpainlevelforpastweekfloat=null;
    Boolean  checkpgid=lm.findpgidinprogressreport(programId);
    
   if(checkpgid==true){
    
	Calendar cal = Calendar.getInstance();
    Date today = cal.getTime();
	SimpleDateFormat s = new SimpleDateFormat("dd/MM/yy");
	String currentdate = s.format(today);
	 cal.setTime(s.parse(currentdate));
	 cal.add(Calendar.DATE, -8);
	 Date end = cal.getTime();     
	 String endDate = s.format(end);
   
 	int painofthisweek=0;
 	int painofthispastweek=0;
 	int totaladofthisweek=0;
 	int totaladofpstweek=0;
 	int totalfreqofthisweek=0;
 	int totalfreqofpastweek=0;
 	float totalpainlevelforthisweek=0;
 	float totalpainlevelforpastweek=0;
 	String novalue="N/A";
    ArrayList<BarchartData> adherenceforthiswk=lm.findadherencescoreforcurrentweek(programId,currentdate);
    ArrayList<BarchartData> adherenceforpastwk=lm.findadherencescoreforpastweek(programId,endDate);
    BarchartData recentdata=lm.getMostRecentdata(programId);
   	ArrayList<BarchartData> scoresAvgforThisWeek= lm.scoreAvgForThisWeek(programId,currentdate );
 	ArrayList<BarchartData> scoresAvgforLastWeek= lm.scoreAvgForLastWeek(programId,endDate);
 	 ArrayList<BarchartData> painLevelAvgForThisWeek = lm.painLevelAvgForThisWeek(programId,currentdate);
 	 ArrayList<BarchartData> painLevelAvgforLastWeek = lm.painLevelAvgforLastWeek(programId,endDate);
 	ArrayList<Integer> adherencescoreforcurrentweek = new ArrayList<Integer>();  
 	ArrayList<Integer> frequencyscoreforcurrentweek  = new ArrayList<Integer>(); 
	ArrayList<Integer> adherencescoreforpastweek = new ArrayList<Integer>();  
 	ArrayList<Integer> frequencyscoreforpasttweek  = new ArrayList<Integer>(); 
 	ArrayList<Float> painlevlforpasttweek  = new ArrayList<Float>();
 	ArrayList<Float> painlevlforcurrenttweek  = new ArrayList<Float>();
 
 	for(BarchartData score:scoresAvgforThisWeek){
 		if(100<=score.getAdhenceAvg()){
 			adherencescoreforcurrentweek.add(100);
		}else{
			adherencescoreforcurrentweek.add(score.getAdhenceAvg());
		}
 		
		frequencyscoreforcurrentweek.add(score.getFreqAvg());
 		/* System.out.println("Date done this ::"+score.getDateDone());
 		System.out.println("painlevlforcurrenttweek::"+score.getPainlevlAvg()); */
 	}
 	
 	for(BarchartData score:scoresAvgforLastWeek){
 		if(100<=score.getAdhenceAvg()){
 			adherencescoreforpastweek.add(100);
		}else{
			adherencescoreforpastweek.add(score.getAdhenceAvg());
		}
		frequencyscoreforpasttweek.add(score.getFreqAvg());
 		/*System.out.println("Date done past::"+score.getDateDone());
 		System.out.println("painlevlforpasttweek::"+score.getPainlevlAvg()); */
 	}
 	
 	for(BarchartData pain:painLevelAvgForThisWeek){
 		painlevlforcurrenttweek.add(pain.getPainlevlAvg());
 		 	}
 	
 	for(BarchartData pain:painLevelAvgforLastWeek){
 		painlevlforpasttweek.add(pain.getPainlevlAvg());
 	 	}
 	
 	
 	
 	
 	int adherencescoreforcurrentweeksize = adherencescoreforcurrentweek.size();
	int freqscoreforcurrentweeksize = frequencyscoreforcurrentweek.size();
	int freqscoreforpastweeksize = frequencyscoreforpasttweek.size();
	int adherencescoreforpastweeksize = adherencescoreforpastweek.size();
	float avgofpainlevelforthisweeksize=painlevlforcurrenttweek.size();
	float avgofpainlevelforpastweeksize=painlevlforpasttweek.size();
	
	 // get  adherence for this week
 	if(adherencescoreforcurrentweeksize!=0){
 		for (int i: adherencescoreforcurrentweek) {
 			totaladofthisweek  += i;
 	    }
 		sumofadforthisweek=totaladofthisweek/adherencescoreforcurrentweeksize;
 		System.out.println("sumofadforthisweek"+sumofadforthisweek);
 		
 	}else{
 		sumofadforthisweek=0;
 	}
 	 // get  adherence for past week
 	if(adherencescoreforpastweeksize!=0){
 		for (int i: adherencescoreforpastweek) {
 			totaladofpstweek  += i;
 	    }
 		sumofadforpastweek=totaladofpstweek/adherencescoreforpastweeksize;
 		System.out.println("sumofadforpastweek"+sumofadforpastweek);
 	}else{
 		sumofadforpastweek=0;
 	}
 // get  freq for this week
 	if(freqscoreforcurrentweeksize!=0){
 		for (int i: frequencyscoreforcurrentweek) {
 			totalfreqofthisweek  += i;
 	    }
 		sumoffreqforthisweek=totalfreqofthisweek/freqscoreforcurrentweeksize;
 		System.out.println("sumoffreqforthisweek"+sumoffreqforthisweek);
 	}else{
 		sumoffreqforthisweek=0;
 	}
 	// get  freq for past week
 	if(freqscoreforpastweeksize!=0){
 		for (int i: frequencyscoreforpasttweek) {
 			totalfreqofpastweek  += i;
 	    }
 		sumoffreqforpastweek=totalfreqofpastweek/freqscoreforpastweeksize;
 		System.out.println("sumoffreqforpastweek"+sumoffreqforpastweek);
 	}else{
 		sumoffreqforpastweek=0;
 	}
 	
	 // get  pain level for last week
	if(avgofpainlevelforpastweeksize!=0){
		for (float i: painlevlforpasttweek) {
			totalpainlevelforpastweek  += i;
	    }
		avgofpainlevelforpastweek=totalpainlevelforpastweek/avgofpainlevelforpastweeksize;
		avgofpainlevelforpastweekfloat = String.format ("%.1f", avgofpainlevelforpastweek);
		System.out.println("sumofpainforpastweek"+avgofpainlevelforpastweek);
		System.out.println("avgofpainlevelforpastweekfloat"+avgofpainlevelforpastweekfloat);
	}else{
		avgofpainlevelforpastweek=((float)0);
	}
	 
	  // get  pain level for this week
	if(avgofpainlevelforthisweeksize!=((float)0)){
		for (float i: painlevlforcurrenttweek) {
			totalpainlevelforthisweek  += i;
			System.out.println("totalpainlevelforthisweek"+totalpainlevelforthisweek);
	    }
		System.out.println("avgofpainlevelforthisweeksize"+avgofpainlevelforthisweeksize);
		avgofpainlevelforthisweek=totalpainlevelforthisweek/avgofpainlevelforthisweeksize;
		avgofpainlevelforthisweekfloat = String.format ("%.1f", avgofpainlevelforthisweek);
		System.out.println("sumofpainforThisweek"+avgofpainlevelforthisweek);
		System.out.println("avgofpainlevelforthisweekfloat"+avgofpainlevelforthisweekfloat);
	}else{
		avgofpainlevelforthisweek=((float)0);
	}
 	
	 
/*  // get  pain level for this week
 
 if(avgofpainlevelforthisweeksize!=0){
 		for (float i: painlevlforcurrenttweek) {
 			totalpainlevelforthisweek  += i;
 	    }
 		avgofpainlevelforthisweek=totalpainlevelforthisweek/avgofpainlevelforthisweeksize;
 		  avgofpainlevelforthisweekfloat = String.format ("%.1f", avgofpainlevelforthisweek);
 		System.out.println("avgofpainlevelforthisweek"+avgofpainlevelforthisweek);
 	}else{
 		avgofpainlevelforthisweek=0;
 	}
 
//get  pain level for past week
if(avgofpainlevelforpastweeksize!=0){
 		for (float i: painlevlforpasttweek) {
 			totalpainlevelforpastweek  += i;
 	    }
 		avgofpainlevelforpastweek=totalpainlevelforpastweek/avgofpainlevelforpastweeksize;
 		avgofpainlevelforpastweekfloat = String.format ("%.1f", avgofpainlevelforpastweek);
 	}else{
 		avgofpainlevelforpastweek=0;
 	}
}  */
	if(scoresAvgforThisWeek.size()>0){
		  recenttpainlevelforthisweek=painlevlforcurrenttweek.get(0);
		    recentadherenceforthisweek=adherencescoreforcurrentweek.get(0);
		    recentfreqforthisweek=frequencyscoreforcurrentweek.get(0);
		   System.out.println("recenttpainlevelforthisweek ::"+recenttpainlevelforthisweek);
	 		System.out.println("recentadherenceforthisweek::"+recentadherenceforthisweek);
	 		System.out.println("recentfreqforthisweek::"+recentfreqforthisweek);
	}else{
		 	if(scoresAvgforLastWeek.size()>0){
		 	recenttpainlevelforthisweek=painlevlforpasttweek.get(0);	
		 	
		    recentadherenceforthisweek=adherencescoreforpastweek.get(0);
		    recentfreqforthisweek=frequencyscoreforpasttweek.get(0);
		    System.out.println("recenttpainlevelforthisweek ::"+recenttpainlevelforthisweek);
	 		System.out.println("recentadherenceforthisweek::"+recentadherenceforthisweek);
	 		System.out.println("recentfreqforthisweek::"+recentfreqforthisweek);
   }
}
}

%>
<html>
<head><title>Edit Program</title>
<link rel="stylesheet" href="/css/program_rSlider.min.css" type='text/css'>
<script type="text/javascript" src="/js/rSlider.min.js"></script>
<script type="text/javascript">
$( document ).ready(function showorhide(){
	var checked = $("input[name='showLocation']").attr("checked") ? "True" : "False";
	var foo = []; 
	$('#multitypedub :selected').each(function(i, selected){ 
	  foo[i] = $(selected).text(); 
	});
	$.each(foo, function( index, value ) {
	var row = $('#multitype');
	row.find("option[value="+value+']').attr('selected','selected');
		});
	if(checked == "True"){
	$("#multitype").show();
	}else{
	$("#multitype").hide();
	}
	
});
function showorhide(){
	var foo2 = []; 
	$('#multitypedub :selected').each(function(i, selected){ 
	  foo2[i] = $(selected).text(); 
	});
	$.each(foo2, function( index, value ) {
	var row = $('#multitype');
	row.find("option[value="+value+']').attr('selected','selected');
		});
	var checked = $("input[name='showLocation']").attr("checked") ? "True" : "False";
	if(checked == "True"){
	$("#multitype").show();
	}else{
	$("#multitype").hide();
	}
}

function showorhidetabs() {
	var foo2 = []; 
	$('#multitypedub :selected').each(function(i, selected){ 
	  foo2[i] = $(selected).text(); 
	});
	$.each(foo2, function( index, value ) {
	var row = $('#multitype');
	row.find("option[value="+value+']').attr('selected','selected');
		});
	
}

function setsSlider (val,id) {
	var setId = "#sets_"+id;
 var sets = new rSlider({
        target: setId,
        values: [1, 2, 3, 4, 5, 6],
        range: false,
        set: [val],
        tooltip: false,
        onChange: function (vals) {
            console.log(vals);
        }
	});
}

function holdSlider (val,id) {
	var holdId = "#hold_"+id;
 var hold = new rSlider({
        target: holdId,
        values: [5, 10, 15, 20, 30, 40,  60, 90,120, 180],
        range: false,
        set: [val],
        tooltip: false,
        onChange: function (vals) {
            console.log(vals);
        }
	});
}
function repsSlider (val,id) {
	var repsId = "#reps_"+id;
 var reps = new rSlider({
        target: repsId,
        values: [1,2,3,4,5,6,7,8,9,10,11,12,15,20,25,30, 40],
        range: false,
        set: [val],
        tooltip: false,
        onChange: function (vals) {
            console.log(vals);
        }
	});
}

 


window.onload = function() {
	
	<%for (ProgramExercise exercise : pe) {%>
	
	<%if(exercise.isShowSides()==true){ %>
		setsSlider(<%=exercise.getSets()%>,<%=exercise.getId()%>);
		holdSlider(<%=exercise.getHolds()%>,<%=exercise.getId()%>);
		repsSlider(<%=exercise.getReps()%>,<%=exercise.getId()%>);
		setSides('<%=exercise.getSide()%>',<%=exercise.getId()%>);

	<%} else{%>
	setsSlider(<%=exercise.getSets()%>,<%=exercise.getId()%>);
	holdSlider(<%=exercise.getHolds()%>,<%=exercise.getId()%>);
	repsSlider(<%=exercise.getReps()%>,<%=exercise.getId()%>);
	<%}%>
	<%}%>
};

function setSides(side,exerciseId){
	/* alert("side"+side);
	alert(document.getElementById("side1"+exerciseId).value); */
	 if(document.getElementById("side1"+exerciseId).value==side){
		 document.getElementById("side1"+exerciseId).checked = true;
	
	}if(document.getElementById("side2"+exerciseId).value==side){
		document.getElementById("side2"+exerciseId).checked = true;
		
	}if(document.getElementById("side3"+exerciseId).value==side){
		document.getElementById("side3"+exerciseId).checked = true;
		
	}if(document.getElementById("side4"+exerciseId).value==side){
		document.getElementById("side4"+exerciseId).checked = true;
	}   
	
	
}

</script>
<style type="text/css">
#table-2 tr {
    float: left;
/*     position: relative; */
    width: 100%;
    cursor: move;
}
body {
 font-family: Arial, Helvetica, sans-serif;
 margin: 0;
 padding: 0 0 50px;
 color: #333;
 font-size: 14px;
}
p {
    margin: 0;
}
.container {
    width: 80%;
    margin: 70px auto;
}
.slider-container {
    width: 100%;
    max-width: 800px;
    margin: 0 auto 50px;
}
.progress-table{
 overflow:hidden;
 width:100%;
 padding-bottom:0px;
 padding-top:8px;
}
.progress-table .table tr td{text-align:center; line-height: 16px; padding:6px; vertical-align: middle;}
.progress-table .table tr td.lft{text-align:left;}
.mlft{margin-left:12px;}
.lft0{margin-left:0;}
.progress-table .table tr th{line-height: 16px;}

.span-300{width:300px;}
.span-370{width:370px;}
.progress-summary-div table{margin-bottom:0px;}
.progress-summary-div h5{
text-align: center; padding:4px;
}
.progress-summary-div table tr th{font-size:12px; text-align: center;}
.lft b{font-size: 13px;}
.progress-summary-div h3{
 
}
.question-img{position:absolute; margin-left:4px;}
.add-lineh{line-height: 30px;width:238px}

.tooltip {
    display: inline-block;
     opacity: 1 !important;
     margin-top:-5px;
}

.tooltip .tooltiptext {
     visibility: hidden;
    width: 300px;
    background-color: black;
    color: #fff;
    text-align: justify;
    border-radius: 6px;
    padding: 5px 5px;
    position: absolute;
    z-index: 1;
    top: -5px;
    left: 110%;
    font-size: 12px;
}
.tooltip .tooltiptext::after {
    content: "";
    position: absolute;
    top: 25%;
    right: 100%;
    margin-top: -7px;
    border-width: 7px;
    border-style: solid;
    border-color: transparent black transparent transparent;
}

.tooltip:hover .tooltiptext {
    visibility: visible;
}
.text-blue{color:#0000ff;}
.minht{max-height:210px;}
.table th, .table td{padding:10px;}
</style>
<script type="text/javascript">
$(document).ready(function() {
    $('#table-2 > *').sortable({
        helper: fixHelper,
        axis: 'y',
        update: function ()
        {
        	var pid = <%=program.getId()%>;
        	var table = document.getElementsByTagName("tr");
        	var length = table.length;
        	var i;
        	var EidList = []; 
        	for (i = 0; i < length;i++) {
        		var row = document.getElementsByTagName("tr")[i].id;
        		EidList.push(row);
            }
        	var EidList1 = $("#EidFirst").val(); 
        	if(EidList != EidList1){
        		var jspcall = "order.jsp?id="+0+"&eidList="+EidList+"&i="+i+"&pid="+pid+"";
            	window.location.href = jspcall;
        	}
        }
    }).disableSelection();
    var fixHelper = function(e, ui) {
        ui.children().each(function() {
            $(this).width($(this).width());
        });
        return ui;
    };
    var table = document.getElementsByTagName("tr");
	var length = table.length;
	var i;
	var EidList = []; 
	for (i = 0; i < length;i++) {
		var row = document.getElementsByTagName("tr")[i].id;//tab.rows[i];
		EidList.push(row);
    }
	$("#EidFirst").val(EidList);
});

function saveInStantEditDtls(exerciseId){
	var sets = document.getElementById("sets_"+exerciseId).value;
	var hold = 0;
	if(document.getElementById("hold_"+exerciseId) != null){
		hold = document.getElementById("hold_"+exerciseId).value;
	}
	var reps = 0;
	if(document.getElementById("reps_"+exerciseId) != null){
		reps = document.getElementById("reps_"+exerciseId).value;
	}
	var side=null;
	var id = <%=programId%>;
	<%for (ProgramExercise exercise : pe) {%>
	
	<% System.out.println("Show side ::::"+exercise.isShowSides()); %>
	<%if( exercise.isShowSides()==false){ %>
	<% System.out.println("Show false ::::"+exercise.isShowSides()); %>
		
	var url = 'edit.jsp?sets='+sets+'&holds='+hold+'&reps='+reps+'&side='+side+'&id='+id+'&excerciseid='+exerciseId;
	
	<%} else{%>
	<% System.out.println("Show true ::::"+exercise.isShowSides()); %>
	if(document.getElementById("side1"+exerciseId)!=null && document.getElementById("side1"+exerciseId).checked){
		side=document.getElementById("side1"+exerciseId).value;
	}if(document.getElementById("side2"+exerciseId)!=null && document.getElementById("side2"+exerciseId).checked){
		side=document.getElementById("side2"+exerciseId).value;
	}if(document.getElementById("side3"+exerciseId)!=null && document.getElementById("side3"+exerciseId).checked){
		side=document.getElementById("side3"+exerciseId).value;
	}if(document.getElementById("side4"+exerciseId)!=null && document.getElementById("side4"+exerciseId).checked){
		side=document.getElementById("side4"+exerciseId).value;
	}
	
	var url = 'edit.jsp?sets='+sets+'&holds='+hold+'&reps='+reps+'&side='+side+'&id='+id+'&excerciseid='+exerciseId;

	<%}%>
	<%}%>
	$.ajax({
    url: url,
    type: 'POST',
    success: function(data) {
    	$("#exerciseMediaDiv").load(url+" #exerciseMediaDiv>*","",function(){
    		<%for (ProgramExercise exercise : pe) {%>
		setsSlider(<%=exercise.getSets()%>,<%=exercise.getId()%>);
		holdSlider(<%=exercise.getHolds()%>,<%=exercise.getId()%>);
		repsSlider(<%=exercise.getReps()%>,<%=exercise.getId()%>);
		setSides('<%=exercise.getSide()%>',<%=exercise.getId()%>);
	<%}%>
    		
    	});
    	location.reload();
    }
  });
}



</script>
</head>
<body>
<input type="hidden" id="EidFirst"/>
<div class="page-header">
    <h1>
        <% String programFor = "Template";
        if (patient != null) programFor = patient.getFullName();
        if (patientGroup != null) programFor = patientGroup.getGroupName();
        %>
        Edit Program <small><%=programFor%></small>
    </h1>
</div>
<%@include file="/WEB-INF/includes/exercise_breadcrumbs.jsp" %>
<%
    if (patient != null || patientGroup != null) {
%>
<div class="row">
    <div class="span5 well span-370 minht">
        <div class="row">
            <div class="span4 span-300">
                <h3><%=Util.textAreaValue(program.getName())%></h3>
                &nbsp;
            </div>
            <div class="span1 lft0">
                <a href="inline/edit_details.jsp?id=<%=programId%>&Create=No_Template&Check=No" class="btn btn-primary fancybox"><i class="icon icon-edit icon-white"></i>Edit</a>

            </div>
        </div>
        <div class="row">
            <div class="span2 add-lineh">
                <% if (patient != null){ %>
                    <b>Patient:</b> <a href="/corp/patients/detail.jsp?id=<%=patient.getId()%>"><%=patient.getFullName()%></a><br>
                <% } else { %>
                    <b>Group:</b> <a href="/corp/groups/detail.jsp?id=<%=patientGroup.getId()%>"><%=patientGroup.getGroupName()%></a><br>
                <% } %>
                <b>Frequency:</b> <%=interval%><br>
            </div>
            <div class="span3 add-lineh">
                <b>Created By:</b> <%=(creator != null) ? creator.getFullName() : ""%><br>
                <b>Created:</b> <%=sdf.format(program.getCreated())%><br>
                <b>Downloaded:</b> <%= (program.getRetrieved() != null) ? sdf.format(program.getRetrieved()) : "Never"%><br>
            </div>
        </div>
    </div>
    <div class="span2 mlft" style="text-align: center;">
        <div class="well">
            <%
            if (pe.size() == 0) {
        %>
        <% if (corp.isSmsEnabledCountry()) { %><button disabled="" class="btn btn-success disabled">Send via SMS</button><% } %>
        <button disabled="" class="btn btn-primary disabled" style="margin-top: 20px;">Send via Email</button>

        <%
            } else {
        %>
        <% if (corp.isSmsEnabledCountry()) { %><a href="inline/sms.jsp?id=<%=program.getId()%>" class="btn btn-success fancybox" id="send_sms_btn">Send via SMS</a> <% } %>
        <a href="inline/email.jsp?id=<%=program.getId()%>" class="btn btn-primary fancybox" style="margin-top: 20px;" id="send_email_btn">Send via Email</a>
        <a href="inline/generate_printout.jsp?id=<%=program.getId()%>" target="_blank" class="btn btn-primary" style="margin-top: 20px;">Generate Printout</a>
		<%-- <a href="inline/manual_entry.jsp?id=<%=program.getId()%>" class="btn btn-primary fancybox" style="margin-top: 20px;">Manual Entry</a> --%>
        <%
            }
        %>
        </div>
        <div>

        </div>
    </div>
     <div class="span3 mlft minht" style="width: 425px;">
        <div class="well progress-table">
           <div class="progress-summary-div">
                    <h3> Progress Summary</h3> 
         <table class="table table-bordered">
	<tbody>
    	<tr>
    	<th class="lft-color"></th>
        <th class="lft-color">Most Recent</th>
        <th class="lft-color">Current Week average</th>
        <th class="lft-color">Previous Week Average</th>
		</tr>
		<tr>
		<td class="lft"><b>Pain Level</b></td>
		<%if(recenttpainlevelforthisweek==0){ %>
		<td class="td-content text-blue">N/A</td><%}else{ %>
		<td class="td-content text-blue"><%=recenttpainlevelforthisweek%></td>
		<%} %>
		<%if(avgofpainlevelforthisweekfloat==null){ %>
		<td class="td-content text-blue">N/A</td><%}else{ %>
		<td class="td-content text-blue"><%=avgofpainlevelforthisweekfloat%></td>
		<%} %>
		<%if(avgofpainlevelforpastweekfloat==null){ %>
		<td class="td-content text-blue">N/A</td><%}else{ %>
		<td class="td-content text-blue"><%=avgofpainlevelforpastweekfloat%></td>
		<%} %>
		</tr>
		<tr>
		<td style="width:130px;"  class="lft lityelo-color">
		<div class="popupt"><b>Frequency Adherence Score</b><div class="tooltip"><img id= class="question-img" src="../patients/help.png" /><span class="tooltiptext">This score is calculated based on the number of times the patient is actually performing the exercise program in comparison to the recommended number of performance, per day.</span></div>
         </div>
		
		</td>
		<%if(recentfreqforthisweek==0){ %>
		<td class="td-content text-blue">N/A</td><%}else{ %>
		<td class="td-content text-blue"><%=recentfreqforthisweek%>%</td>
		<%} %>
		<%if(sumoffreqforthisweek==0){ %>
		<td class="td-content text-blue">N/A</td><%}else{ %>
		<td class="td-content text-blue"><%=sumoffreqforthisweek%>%</td>
		<%} %>
		
		<%if(sumoffreqforpastweek==0){ %>
		<td class="td-content text-blue">N/A</td><%}else{ %>
		<td class="td-content text-blue"><%=sumoffreqforpastweek%>%</td>
		<%} %>
		</tr>
		<tr>
		<td style="width:130px;" class="lft">
		<b>Completion Adherence Score</b><div class="tooltip"><img id= class="question-img" src="../patients/help.png" /><span class="tooltiptext">This score represents the total time elapsed during the performance of all exercises in a given program, as a % of the cumulative, total time of all the exercises in a given program, excluding rest times.</span></div>
		</td>
		
			<%if(recentadherenceforthisweek==0){ %>
		<td class="td-content text-blue">N/A</td><%}else{ %>
		<td class="td-content text-blue"><%=recentadherenceforthisweek%>%</td>
		<%} %>
			<%if(sumofadforthisweek==0){ %>
		<td class="td-content text-blue">N/A</td><%}else{ %>
		<td class="td-content text-blue"><%=sumofadforthisweek%>%</td>
		<%} %>
			<%if(sumofadforpastweek==0){ %>
		<td class="td-content text-blue">N/A</td><%}else{ %>
		<td class="td-content text-blue"><%=sumofadforpastweek%>%</td>
		<%} %>
		</tr>

	</tbody>
</table>

<h5><a href="/corp/patients/progress_report.jsp?id=<%=program.getId()%>&patient=<%=patient.getId()%>" class="text-blue" style="text-decoration:underline;">Click to view the Detailed Progress Report </a></h5>
            </div>    
        </div>
       
    </div>
    </div>
  
    <div class="control-group" style="display: none;">
                            <div class="controls">
                              <label class="checkbox">
                                <input type="checkbox" onclick="showorhide();" id="optionsCheckbox" name="showLocation" value="true"<%=(showLocation) ? " checked": ""%>>
                                </label>
                                <select id="multitype" name="multitype" onclick="showorhidetabs();" onchange="showorhidetabs();" multiple style="display: none;margin: -5px 0 0 19px;">
                             <% List<Corporation> corps = null;
                			    if (u != null) {
                			        CorporationManager cm = new CorporationManager();
                			        corps = cm.getCorporations(u.getId());
                			    }%>
                            <%
                            if (corps != null && corps.size() > 0) {
                                    for (Corporation corpu : corps) {
                                        if (c != null && c.equals(corpu)) {
                                            continue;
                                        }
                                  %>
                                   <option value="<%=corpu.getId()%>"><%=corpu.getName()%></option>
                                  <%
                                    }
                                %>

                                  <%
                                      }%>
                              </select>
                              <select id="multitypedub" name="multitypedub" multiple style="display: none; margin: -5px 0 0 19px;">
                             <% 
                             	List<Corporation> corpsid = null;
                            	List<Long> Eidlist = new ArrayList<Long>();
                			    if (u != null) {
                			        CorporationManager cm = new CorporationManager();
                			        if(!name.equals("")){
                			        	if(showLocation == false){
                			        		corpsid = lm.getCorpidforPatientPrograms(name,corp.getId(),"Noshare",0,programId);
                			        	}else{
                			        		corpsid = lm.getCorpidforPatientPrograms(name,corp.getCompanyId(),null,uniqueId,programId);
                			        	}
                			        }
                			    }%>
                            <% 
                            if (corpsid != null && corpsid.size() > 0) {
                                   if (corpsid != null && corpsid.size() > 0) {
                                          for (Corporation corpu1 : corpsid) {
                                  %>
                                  <option value="<%=corpu1.getId()%>" <%=(corpu1.getCompanyId() == corpu1.getCompanyId()) ? "selected" : ""%>><%=corpu1.getCompanyId()%>
                                		  <% 
                                		  Eidlist.add(corpu1.getId());
                                  %></option>
                                  <%
                                    }
                                %>

                                  <%
                                   }
                                   session.setAttribute("Eidlist", Eidlist);
                            }else{
                         	   		session.setAttribute("Eidlist", null);
                        	} %>
                              </select>
                              <select id="multitypename" name="multitypename" multiple style="display: none; margin: -5px 0 0 19px;">
                             <% 
                			    if (u != null) {
                			        CorporationManager cm = new CorporationManager();
                			        if(!name.equals("")){
                			        		corpsid = lm.getCorpidforPatientPrograms(name,corp.getCompanyId(),null,uniqueId,programId);
                			        }
                			    }%>
                            <% 
                            if (corpsid != null && corpsid.size() > 0) {
                                   if (corpsid != null && corpsid.size() > 0) {
                                          for (Corporation corpu1 : corpsid) {
                                  %>
                                  <option value="<%=corpu1.getId()%>" <%=(corpu1.getId() == corpu1.getId()) ? "selected" : ""%>><%=corpu1.getName()%>
                                		  <% 
                                  %></option>
                                  <%
                                    }
                                %>

                                  <%
                                   }
                            }%>
                              </select>
                            </div>
                          </div>
<%
    } else {
%>
<div class="row">
    <div class="span5 well">
        <div class="row">
            <div class="span4">
                <h3><%=Util.textAreaValue(program.getName())%></h3>
                &nbsp;
            </div>
            <div class="span2">
                <a href="inline/edit_details.jsp?id=<%=programId%>&Create=New_Template&Check=No" class="btn btn-primary fancybox"><i class="icon icon-edit icon-white"></i> Edit</a>
            </div>
            <div class="control-group">
            <div class="controls">
                <select id="multitype" name="multitype" onclick="showorhidetabs();" onchange="showorhidetabs();" multiple style="display: none;margin: -5px 0 0 19px;">
             <% List<Corporation> corps = null;
			    if (u != null) {
			        CorporationManager cm = new CorporationManager();
			        corps = cm.getCorporations(u.getId());
			    }%>
            <%
            if (corps != null && corps.size() > 0) {
                    for (Corporation corpu : corps) {
                        if (c != null && c.equals(corpu)) {
                            continue;
                        }
                  %>
                   <option value="<%=corpu.getId()%>"><%=corpu.getName()%></option>
                  <%
                    }
                %>

                  <%
                      }%>
              </select>
              <select id="multitypedub" name="multitypedub" multiple style="display: none; margin: -5px 0 0 19px;">
             <% 
             	List<Corporation> corpsid = null;
            	List<Long> Eidlist = new ArrayList<Long>();
			    if (u != null) {
			        CorporationManager cm = new CorporationManager();
			        if(!name.equals("")){
			        	if(showLocation == false){
			        		corpsid = lm.getCorpidforPatientPrograms(name,corp.getId(),"Noshare",0,programId);
			        	}else{
			        		corpsid = lm.getCorpidforPatientPrograms(name,corp.getCompanyId(),null,uniqueId,programId);
			        	}
			        }
			    }
            
            if (corpsid != null && corpsid.size() > 0) {
                   if (corpsid != null && corpsid.size() > 0) {
                          for (Corporation corpu1 : corpsid) {
                		  Eidlist.add(corpu1.getId());
                    }
                   }
                   session.setAttribute("Eidlist", Eidlist); 
            }else{
            	   session.setAttribute("Eidlist", null);
            }%>
              </select>
              <select id="multitypename" name="multitypename" multiple style="display: none; margin: -5px 0 0 19px;">
             <% 
			    if (u != null) {
			        CorporationManager cm = new CorporationManager();
			        if(!name.equals("")){
			        		corpsid = lm.getCorpidforPatientPrograms(name,corp.getCompanyId(),null,uniqueId,programId);
			        }
			    }%>
            <% 
            if (corpsid != null && corpsid.size() > 0) {
                   if (corpsid != null && corpsid.size() > 0) {
                          for (Corporation corpu1 : corpsid) {
                  %>
                  <option value="<%=corpu1.getId()%>" <%=(corpu1.getId() == corpu1.getId()) ? "selected" : ""%>><%=corpu1.getName()%>
                		  <% 
                  %></option>
                  <%
                    }
                %>

                  <%
                   }
            }%>
              </select>
            </div>
          </div>
        </div>
    </div>
</div>
<%
    }
%>

<div class="row">
    <div class="span10">
        <div class="btn-toolbar" style="margin-bottom: 20px;">
        <div class="btn-group">
        <a href="inline/add_exercise.jsp?id=<%=programId%>&patient=<%=patientId%>&group=<%=groupId%>" class="btn btn-primary"><i class="icon-white icon-plus"></i> Exercise</a> 
        <a href="inline/add_rest.jsp?id=<%=programId%>" class="btn btn-primary fancybox"><i class="icon-white icon-plus"></i> Rest</a>
        <a href="inline/add_info.jsp?id=<%=programId%>" class="btn btn-primary fancybox"><i class="icon icon-white icon-plus"></i> Reading Info</a>
        <a href="inline/add_note.jsp?program=<%=programId%>" class="btn btn-primary fancybox"><i class="icon icon-white icon-plus"></i> Quick Note</a>
  </div>
            <div class="btn-group">
        <a href="inline/delete_program.jsp?id=<%=programId%>" class="btn btn-danger fancybox"><i class="icon icon-white icon-trash"></i> Delete</a>
                </div>
        </div>
    </div>
</div>
<div class="row" id="exerciseMediaDiv">
    <div class="span11">
    <form method="post" action="edit.jsp" class="well form-inline">
        <table class="table" id="table-2"	>
            <tbody class="table ui-sortable">

            <%
                for (ProgramExercise exercise : pe) {
                    long n = next.containsKey(exercise.getId()) ? next.get(exercise.getId()) : 0;
                    long b = previous.containsKey(exercise.getId()) ? previous.get(exercise.getId()) : 0;
					
                    if (exercise.getType().equals("exercise")) {
                        String url = "/images/photo_150_100.gif";
                        if (exercise.getKeyPhotoId() > 0) {
                            url = "/media?id=" + exercise.getKeyPhotoId() + "&width=150&v=" + exercise.getKeyPhotoVersion();
                        }

            %>
            <div id="refresh_<%=exercise.getId()%>">
            <tr id="<%=exercise.getId()%>">
                <td>
                    <img src="<%=url%>" width="150" height="105"><br>
                    <%=Util.textAreaValue(exercise.getName())%>
                </td>
                <td style="width: 650px;">
                
                        <% if (exercise.getBase().equals(Exercise.TIMED_EXERCISE)) {%>
                        <table>
                        	<tr>
                        		<td> <b>Hold:</b> <%=exercise.getHolds()%> Seconds</td><td style="width: 440px;padding-left: 17px;"><input type="range" onchange="rangevalue.value=value" id="hold_<%=exercise.getId()%>" name="hold" value=<%=exercise.getHolds()%> min="5" max="200"/></td>
        					</tr>
        				</table>
        				 <table>
                        	<tr>
                        		<td> <b>Sets:</b> <%=exercise.getSets()%> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td style="width: 440px; padding-left: 20px;"><input type="range" onchange="rangevalue.value=value" id="sets_<%=exercise.getId()%>" name="sets" value=<%=exercise.getSets()%> /></td>
        					</tr>
        				</table>
                    <div id="sides"> 
                    <% if (exercise.isShowSides()) {%>
        				 <table>
                        	<tr>
                        		<td> <b>Side:</b> <%=exercise.getSide()%> </td><td style="width: 5px;padding-left: 0px;"> <td><label for="radio1">Left&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><input
									class="side" type="radio" id="side1<%=exercise.getId()%>" name="side<%=exercise.getId()%>"  value="Left"  style="    margin-bottom: -36px;
    margin-left: -39px;" /></td>
								<td><label for="radio2">Right&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><input
									class="side" type="radio" id="side2<%=exercise.getId()%>" name="side<%=exercise.getId()%>" value="Right" style="margin-bottom: -36px;
    margin-left: -44px;
									" /></td>
								<td><label for="radio3">Both&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><input
									class="side" type="radio" id="side3<%=exercise.getId()%>"name="side<%=exercise.getId()%>" value="Both" style="margin-bottom: -36px; margin-left: -44px;"/></td>
								<td><label for="radio4">Alternating&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><input
									class="side" type="radio" id="side4<%=exercise.getId()%>" name="side<%=exercise.getId()%>"
									value="Alternating"  style="    margin-bottom: -36px;
    margin-left: -62px;"/></td><br>
        					</tr>
        				</table>
        				  <%
                        }
                    %>
        				
        				</div>
                        <% } else {%>
                        <table>
                        	<tr>
                        		<td> <b>Reps:</b> <%=exercise.getReps()%> </td><td style="width: 440px;padding-left: 64px;"><input type="range" onchange="rangevalue.value=value" id="reps_<%=exercise.getId()%>" name="reps" value=<%=exercise.getReps()%>/></td>
        					</tr>
        				</table>
        				 <table>
                        	<tr>
                        		<td> <b>Sets:</b> <%=exercise.getSets()%> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td style="width: 440px; padding-left: 20px;"><input type="range" onchange="rangevalue.value=value" id="sets_<%=exercise.getId()%>" name="sets" value=<%=exercise.getSets()%> /></td>
        					</tr>
        				</table>
                    <div id="sides"> 
                    <% if (exercise.isShowSides()) {%>
        				 <table>
                        	<tr>
                        		<td> <b>Side:</b> <%=exercise.getSide()%> </td><td style="width: 5px;padding-left: 0px;"> <td><label for="radio1">Left&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><input
									class="side" type="radio" id="side1<%=exercise.getId()%>" name="side<%=exercise.getId()%>"  value="Left"  style="    margin-bottom: -36px;
    margin-left: -39px;" /></td>
								<td><label for="radio2">Right&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><input
									class="side" type="radio" id="side2<%=exercise.getId()%>" name="side<%=exercise.getId()%>" value="Right" style="margin-bottom: -36px;
    margin-left: -44px;
									" /></td>
								<td><label for="radio3">Both&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><input
									class="side" type="radio" id="side3<%=exercise.getId()%>"name="side<%=exercise.getId()%>" value="Both" style="margin-bottom: -36px; margin-left: -44px;"/></td>
								<td><label for="radio4">Alternating&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><input
									class="side" type="radio" id="side4<%=exercise.getId()%>" name="side<%=exercise.getId()%>"
									value="Alternating"  style="    margin-bottom: -36px;
    margin-left: -62px;"/></td><br>
        					</tr>
        				</table>
        				  <%
                        }
                    %>
        				
        				</div>
                        <% } %>
                       
        				<br>
                 
                
                    
                </td>
                <td style="width: 100px;">
                    <p class="exercise-options" style="text-align: right;">
                          <input name="submit" class="btn btn-success btn-mini" type="button" id="saveInstantExercise_<%=exercise.getId()%>" value="Save Instant Edit" onclick="saveInStantEditDtls(<%=exercise.getId()%>);"/>
                        <a href="inline/edit_exercise.jsp?id=<%=exercise.getId()%>" class="btn btn-primary btn-mini " style="margin: 6px 0px;"><i class="icon icon-edit icon-white"></i>Full Edit</a>
                        <a href="inline/delete_exercise.jsp?id=<%=exercise.getId()%>" class="btn btn-danger btn-mini fancybox"><i class="icon icon-trash icon-white"></i> Delete</a>
                    </p>
                </td>
                <td class="span1">
                    <%
                        if (b > 0) {
                    %>
                    <a href="order.jsp?id=<%=program.getId()%>&e1=<%=b%>&e2=<%=exercise.getId()%>"><i class="icon-arrow-up"></i></a>
                    <%
                        } else {
                    %>
                    <i class="icon-white icon-foo"></i>
                    <%
                        }
                    %>
                    <%
                        if (n > 0) {
                    %>
                    <a href="order.jsp?id=<%=program.getId()%>&e1=<%=n%>&e2=<%=exercise.getId()%>"><i class="icon-arrow-down"></i></a>
                    <%
                        } else {
                    %>
                    <i class="icon-white icon-foo"></i>
                    <%
                        }
                    %>

                </td>
            </tr>
            </div>
            <%
                    } else {
            %>
            <div id="refresh_<%=exercise.getId()%>">
            <tr id="<%=exercise.getId()%>">
                <td><b>Rest:</b> <%=exercise.getRest()%> Seconds</td>
                <td style="width: 750px;">&nbsp;</td>
                <td style="width: 100px;">
               
                    <p class="exercise-options" style="text-align: right;">
                        <a href="inline/edit_rest.jsp?id=<%=exercise.getId()%>" class="btn btn-primary btn-mini fancybox " style="margin: 6px 0px;"><i class="icon icon-edit icon-white"></i>Full Edit</a>
                        <a href="inline/delete_exercise.jsp?id=<%=exercise.getId()%>" class="btn btn-danger btn-mini fancybox"><i class="icon icon-trash icon-white"></i> Delete</a>
                    </p>     
                </td>
                <td class="span1">
                    <%
                        if (b > 0) {
                    %>
                    <a href="order.jsp?id=<%=program.getId()%>&e1=<%=b%>&e2=<%=exercise.getId()%>"><i class="icon-arrow-up"></i></a>
                    <%
                        } else {
                    %>
                    <i class="icon-white icon-foo"></i>
                    <%
                        }
                    %>
                    <%
                        if (n > 0) {
                    %>
                    <a href="order.jsp?id=<%=program.getId()%>&e1=<%=n%>&e2=<%=exercise.getId()%>"><i class="icon-arrow-down"></i></a>
                    <%
                        } else {
                    %>
                    <i class="icon-white icon-foo"></i>
                    <%
                        }
                    %>

                </td>
            </tr>
            </div>
            <%
                    }
                }
            %>
            </tbody>
        </table>
        </form>
    </div>
</div>

<div class="row">
    <% if (myInfos != null && myInfos.size() > 0) {%>
    <div class="span4 well">
        <h3>Selected Information Articles</h3>
        <ul class="thumbnails">
            <%
                for (MyInfo info : myInfos) {
                    String url = "/images/photo_150_100.gif";
                    if (info.getAttachment() != null) {
                        url = info.getAttachment().getImageUrl(Attachment.ImageSize.f150);
                    }
                    String link = "/info.jsp?id=" + info.getId();
            %>
            <li>
                <div class="thumbnail">
                <img src="<%=url%>" width="150" height="105"><br>
                <h5 style="width: 146px; height: 35px; overflow: hidden;"><%=info.getHeadline()%></h5>
                <p>
                    <a href="/info.jsp?id=<%=info.getId()%>" class="btn fancybox">Preview</a>
                    <a href="inline/remove_info.jsp?info=<%=info.getId()%>&id=<%=programId%>" class="btn btn-danger fancybox">Delete</a>
                </p>
                </div>
            </li>
            <%
                }
            %>
        </ul>
    </div>
    <% } %>
    <% if (notes != null && notes.size() > 0) {%>
    <div class="span5 well <% if (myInfos == null || myInfos.size() == 0) {%>offset4<%}%>">
        <h3>Notes</h3>
        <table class="table table-zebra-striped">
            <tbody>
            <%
                for (ProgramNote note : notes) {
                    String body = note.getNote().replaceAll("\n", " ");
                    if (body.length() > 103) {
                        body = body.substring(0, 100).trim();
                        int l = body.lastIndexOf(" ");
                        if (l > 0) {
                            body = body.substring(0, l).trim();
                        }
                        body += "...";
                    }
            %>
            <tr>
                <td style="width: 75px; "><%=sdfNote.format(note.getDate())%></td>
                <td class="span4" style="display: block;width: 314px;margin-left: 0;text-overflow: ellipsis;overflow: hidden;"><%=body%></td>
                <td>
                    <a href="inline/add_note.jsp?note=<%=note.getId()%>" class="fancybox"><i class="icon-edit"></i></a>
                    <a href="inline/delete_note.jsp?note=<%=note.getId()%>" class="fancybox"><i class="icon-trash"></i></a>
                </td>
            </tr>
            <%
                }
            %>
            </tbody>
        </table>
    </div>
    <% } %>
</div>

<%if (pe.size() > 3) {%>
<div class="row">
    <div class="span10">
        <div class="btn-toolbar" style="margin-bottom: 20px;">
            <div class="btn-group">
                <a href="inline/add_exercise.jsp?id=<%=programId%>&patient=<%=patientId%>&group=<%=groupId%>" class="btn btn-primary"><i class="icon-white icon-plus"></i> Exercise</a>
                <a href="inline/add_rest.jsp?id=<%=programId%>" class="btn btn-primary fancybox"><i class="icon-white icon-plus"></i> Rest</a>
                <a href="inline/add_info.jsp?id=<%=programId%>" class="btn btn-primary fancybox"><i class="icon icon-white icon-plus"></i> Reading Info</a>
                <a href="inline/add_note.jsp?program=<%=programId%>" class="btn btn-primary fancybox"><i class="icon icon-white icon-plus"></i> Quick Note</a>
            </div>
        </div>
    </div>
</div>
<%
    }
%>

<% if (tutorialProgress == UserManager.TUTORIAL_WENT_TO_PROGRAM_PAGE){ %>
     <style>
        .qtip-content { text-align: left; }
        .qtip { max-width: 500px;}
        .qtip-titlebar { text-align: center; }
        #next_btn, #staff_btn, #finish_btn, #video_btn, #no_btn {
            display:inline !important;
        }
        #next_btn, #staff_btn, #finish_btn{
            margin-left: 200px;
        }
        #video_btn {
            margin-left: 170px;
            margin-right: 10px;
        }
     </style>
        
    <script>
        var upToIndex = 0;
        
        function explainPage(titleText, messageText, buttons, targetElement, my, at) {
            var target = (targetElement == null) ? $(window) : $(targetElement);
                $('<div />').qtip({
                    content: {
                        text:  $('<p />', {html: messageText}).add($(buttons)),
                        title: titleText
                    },
                    position: {
                        my: my, at: at,
                        target: target
                    },
                    show: {
                        ready: true,
                        modal: {
                            on: false,
                            blur: false
                        }
                    },
                    hide: false,
                    style: {
                        width: 500,
                        classes: 'qtip-bootstrap'
                    },
                    events: {
                        render: function(e, api) {
                            $('button', api.elements.content).click(function(e) {
                                if (e.currentTarget.id == 'next_btn') {
                                    explainPage(null, "Would you like to watch a quick video demonstration of the App in action on an iPhone?"<% if (cu.hasRole(CorporationRole.Role.Admin)) {%>'<button id="video_btn" class="btn btn-primary">Yes</button><button id="no_btn" class="btn btn-primary">No Thanks</button>',<% }else{ %>'<button id="video_btnmember" class="btn btn-primary" style="display: inline !important;margin-left: 170px;margin-right: 10px;float: left;">Yes</button><button id="staff_btn" class="btn btn-primary" style="margin: 0 !important;">No Thanks</button>',<% } %> null, "center", "center");
                                }
                                else if (e.currentTarget.id == 'video_btn'){
                                    window.open('http://www.fiizio.com/home/help/videos/getting-around-the-app/');
                                    explainPage("Invite your staff to start using Fiizio App too", "The only thing left to do is ask your physiotherapy team join up to your practice on Fiizio App. Invite them as users from within the Team Members section. Occasionally over-zealous email filters pick it up so if it doesn't arrive immediately, ask them to check their junk mail folder.", '<button id="staff_btn" class="btn btn-primary">Got It</button>', "#members_side", "center left", "center right");
                                }
                                else if (e.currentTarget.id == 'video_btnmember'){
                                    window.open('http://www.fiizio.com/home/help/videos/getting-around-the-app/');
                                    explainPage("Congratulations, your ready!", "You are now fully qualified to start using Fiizio App with your clients. There are more features to explore in your own time but you have the foundations. We recommend that you have a practice run on a colleague now to show them how it works. Create and send a new program for them by selecting Add New Patient from your practice Dashboard.<br/><br/>If you need further instruction or have a question you'd like to ask before your trial expires, go the Help button at the top of every screen (it's where we keep the answers!).", "<button id='finish_btn' class='btn btn-primary'>I'm ready</button>", null, "center", "center");
                                }
                                else if (e.currentTarget.id == 'no_btn'){
                                    explainPage("Invite your staff to start using Fiizio App too", "The only thing left to do is ask your physiotherapy team join up to your practice on Fiizio App. Invite them as users from within the Team Members section. Occasionally over-zealous email filters pick it up so if it doesn't arrive immediately, ask them to check their junk mail folder.", '<button id="staff_btn" class="btn btn-primary">Got It</button>', "#members_side", "center left", "center right");
                                }
                                else if (e.currentTarget.id == 'staff_btn'){
                                    explainPage("Congratulations, your ready!", "You are now fully qualified to start using Fiizio App with your clients. There are more features to explore in your own time but you have the foundations. We recommend that you have a practice run on a colleague now to show them how it works. Create and send a new program for them by selecting Add New Patient from your practice Dashboard.<br/><br/>If you need further instruction or have a question you'd like to ask before your trial expires, go the Help button at the top of every screen (it's where we keep the answers!).", "<button id='finish_btn' class='btn btn-primary'>I'm ready</button>", null, "center", "center");
                                }
                                else if (e.currentTarget.id == 'finish_btn'){
                                    window.location = "/corp?tutorial=true";
                                }

                                api.hide(e);
                            });
                        },
                        hide: function(event, api) {
                            api.destroy();
                        }
                    }
                });
            }
            
            <% if (corp.isSmsEnabledCountry()) { %>
            explainPage("Your demo program is not perfect... yet", "Why don't you try to make it better by adding a new exercise, including some reading info, or a quick note? When you are ready, click the send button on the right to send it to your phone.<br/><br/>Open the message on your smartphone and follow the instructions.", '<button id="next_btn" class="btn btn-primary">Okay</button>', "#send_sms_btn", "center right", "center left");
            <% } else {%>
                explainPage("Your demo program is not perfect... yet", "Why don't you try to make it better by adding a new exercise, including some reading info, or a quick note? When you are ready, click the send button on the right to send it to your phone.<br/><br/>Open the message on your smartphone and follow the instructions.", '<button id="next_btn" class="btn btn-primary">Okay</button>', "#send_email_btn", "center right", "center left");
            <% } %>
    </script>
     <% } %>

<style>
	.exercise-options{float: right;width: 110px;}
</style>
</body>
</html>
