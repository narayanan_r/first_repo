<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.Corporation" %>
<%@ page import="au.com.ourbodycorp.model.CorporationUser" %>
<%@ page import="au.com.ourbodycorp.model.Program" %>
<%@ page import="au.com.ourbodycorp.model.User" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");

    User u = cu.getUser();
    Corporation corp = cu.getCorporation();
   /*  try{
    	Util.allActivitiesLogs("Templates", u.getId(), corp.getName(), "View Program Templates");
    }catch(Exception e){
    	
    } */
    LibraryManager lm = new LibraryManager();
    List<Program> templates = lm.getProgramTemplates(corp.getId());
    session.setAttribute("uId", 0);
    
    String type="Templates";
    String activity_page="corp/programs/templates.jsp";
	String corp_id=String.valueOf(corp.getId());
	String corp_name=corp.getName();
	String corp_loc=corp.getSuburb();
	String user_id=String.valueOf(u.getId());
	String user_name=u.getFullName();
	String invitor_id="---";
	String invitor_name="---";
	String flag="view";
	String from="---";
	String to="---";
	String desc="View Program Templates By User";
     try
    {		
     Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
    }
    catch(Exception e)
    {
    } 

    String[][] breadcrumbs = {{"/corp", corp.getName()}, {null, "Program Templates"}};
%>
<html>
<head><title>Templates</title></head>
<body>
<div class="page-header">
    <h1>Program Templates</h1>
</div>
<%@include file="/WEB-INF/includes/breadcrumbs.jsp"%>
<div class="row">
    <div class="span9">
        <a href="/corp/programs/inline/edit_details.jsp?Create=New_Template&Check=Name_Exist" class="btn fancybox"><i class="icon-plus"></i> Add Template</a>
        <%
            if (templates.size() == 0) {
        %>
        <div class="alert alert-info">
            No templates created yet
        </div>
        <%
            } else {
        %>
        <table class="table">
            <thead>
            <tr>
                <th>Name</th>
            </tr>
            </thead>
            <tbody>
            <%
                for (Program p : templates) {
            %>
            <tr>
                <td><a href="edit.jsp?id=<%=p.getId()%>"><%=Util.textAreaValue(p.getName()).length() > 0 ? Util.textAreaValue(p.getName()) : "(not named)" %></a></td>
            </tr>
            <%
                }
            %>
            </tbody>
        </table>
        <%
            }
        %>
    </div>
</div>
</body>
</html>