<%@ page import="au.com.ourbodycorp.Util"%>
<%@ page import="au.com.ourbodycorp.model.*"%>
<%@ page import="au.com.ourbodycorp.HTML"%>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager"%>
<%@ page import="java.net.URLEncoder"%>
<%@ page import="java.util.Date"%>
<%@ page import="java.util.List"%>
<%@	page import="java.util.ArrayList"%>
<%@ page import="java.sql.Connection"%>
<%@ page import="java.sql.PreparedStatement"%>
<%@ page import="java.sql.*"%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ page
	import="au.com.ourbodycorp.model.managers.ProgramMessageManager"%>
<%
	CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");

	if (cu == null) {
		response.sendRedirect("/close.jsp?redir="
				+ URLEncoder.encode(Util.loginUrl(Constants.LoginReason.login, request), "UTF-8"));
		return;
	}

	User u = cu.getUser();
	Corporation corp = cu.getCorporation();

	long patientId = Long.parseLong(Util.notNull(request.getParameter("patient"), "0"));
	long groupId = Long.parseLong(Util.notNull(request.getParameter("group"), "0"));
	long programId = Long.parseLong(Util.notNull(request.getParameter("id"), "0"));
	long add = Long.parseLong(Util.notNull(request.getParameter("add"), "0"));
	String bodyPart = Util.notNull(request.getParameter("bp"), (String) session.getAttribute("bpType.bp"));
	String type = Util.notNull(request.getParameter("type"), (String) session.getAttribute("bpType.type"));

	session.setAttribute("bpType.bp", bodyPart);
	session.setAttribute("bpType.type", type);

	String query = (request.getParameter("query") != null && request.getParameter("query").trim().length() > 0)
			? request.getParameter("query")
			: null;

	LibraryManager lm = new LibraryManager();
	Patient patient = null;
	Program program = null;
	PatientGroup patientGroup = null;

	program = lm.getProgram(programId);

	if (program == null) {
		response.sendError(404, "Program not found");
		return;
	}

	if (program != null) {
		patient = lm.getPatient(program.getPatientId());
		patientGroup = lm.getPatientGroup(program.getGroupId());

	}

	String bcPatient[][] = {{"/corp/", corp.getName()}, {"/corp/patients/", "Patients"},
			{"/corp/patients/detail.jsp?id=" + ((patient != null) ? patient.getId() : "0"),
					((patient != null) ? patient.getFullName() : "")},
			{null, program.getName()}};
	String bcTemplate[][] = {{"/corp/", corp.getName()}, {"/corp/programs/templates.jsp", "Program Templates"},
			{null, program.getName()}};
	String bcGroup[][] = {{"/corp/", corp.getName()}, {"/corp/groups/", "Patient Groups"},
			{"/corp/groups/detail.jsp?id=" + ((patientGroup != null) ? patientGroup.getId() : "0"),
					((patientGroup != null) ? patientGroup.getGroupName() : "")},
			{null, program.getName()}, {null, "Add program"}};
	String[][] breadcrumbs = bcTemplate;
	if (patient != null) {
		breadcrumbs = bcPatient;
	} else if (patientGroup != null) {
		breadcrumbs = bcGroup;
	} else {
		breadcrumbs = bcTemplate;
	}
	Exercise.BodyPart pBodyPart = null;
	Exercise.Type pType = null;
	try {
		pBodyPart = Exercise.BodyPart.valueOf(bodyPart);
	} catch (Exception e) {
	}

	try {
		pType = Exercise.Type.valueOf(type);
	} catch (Exception e) {
	}

	List<Exercise> exercises = null;

	if (query != null) {
		exercises = lm.getExercisesBySearchforAddExercise(pBodyPart, pType, corp.getId(), query);
	} else {
		if (pBodyPart != null || pType != null) {
			exercises = lm.getExercises(pBodyPart, pType, corp.getId());
		}
	}
%>




<html>
<head>
<title>Add exercise</title>
<style>
#dropHere {
	height: 1000px;
	border: solid 2px black;
}

.col-md-6.demo-1 {
	margin: 5px;
	display: inline-block;
	width: 40%;
	background: #FFF;
	float: left;
	width: 40%;
	height: 500px;
	/* overflow-x: scroll; */
	overflow-y: scroll;
}

.col-md-6.demo {
	display: inline-block;
	background: #FFF;
	border: solid 2px black;
	float: left;
	margin: 5px;
	display: block;
	width: 40%;
	width: 40%;
	height: 500px;
	/* overflow-x: scroll; */
	overflow-y: scroll;
}

#sortorder {
	position: relative;
	margin-top: 60%;
	left: 89%;
	background: green;
	width: 100px;
	height: 50px;
}
</style>
<script type="text/javascript"
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
<script type="text/javascript"
	src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.js"></script>
<script>
	var counts = [ 0 ];	
	var ids = new Array();
	function dragDropFunction() {
	    $(".thumbnail").draggable({
	        cancel: "a.ui-icon",
	        revert: true,
	        helper: "clone",
	        cursor: "move",
	        revertDuration: 0,
	      //Create counter
			start : function() {
				counts[0]++;
			}
	    });

	    $('.images').droppable({
	        accept: ".thumbnail",
	        drop: function(event, ui) {
	        	var $item = $(ui.draggable)
	            if (!$item.hasClass('clone')) {
	            	var test = $(ui.helper).attr('id');
	            	var exceriseIds = document.getElementById("excerciseid").value;
					var count = 0;
					for (var i = 0; i < ids.length; i++) {
						if (ids[i] == test) {
							count = 1;
						}
					}
					if (count == 0) {
						ids.push(test);
					}
	                $item = $item.clone().addClass('clone-' + counts[0]).css({"width":"143px"}).css({"display":"inline-block"})
	                .css({"margin":"15px"})
	                .css({"opacity":"1"});
	                $item.draggable({
	                    cancel: "a.ui-icon",
	                    revert: true,
	                    cursor: "move",
	                    revertDuration: 0
	                });
	            }
	        	$(this).addClass('has-drop').append($item);
	        	$(".clone-" + counts[0]).dblclick(
				function() {
					$(this).remove();
					removeA(ids, test);
					document.getElementById("excerciseid").value = ids.toString();
				});
	        	document.getElementById("excerciseid").value = ids.toString();
	        }
	    });

	};
	
	function removeA(arr) {
		//alert(arr);
		var what, a = arguments, L = a.length, ax;
		while (L > 1 && arr.length) {
			what = a[--L];
			while ((ax = arr.indexOf(what)) !== -1) {
				arr.splice(ax, 1);
			}
		}
		return arr;
	}
	
	$(document).ready(function() {
	    $('#type').change(function(event) {
	    	var type = document.getElementById("type").value;
	        $.ajax({
	        url: 'add_exercise.jsp?type='+type+'&id='+<%=programId%>,
	        type: 'POST',
	        success: function(data) {
	        if(document.getElementById("excerciseid") == null){
	        	$("#span10").load(location.href+" #span10>*","",function() {
	        		dragDropFunction();
		          });
	        }
             $("#exerciseMediaDiv").load(location.href+" #exerciseMediaDiv>*","",function() {
            	 dragDropFunction();
	          });
	        }
	      });
	    });
	    
	    
	    $('#bp').change(function(event) {
	    	var bp = document.getElementById("bp").value;
	        $.ajax({
	        url: 'add_exercise.jsp?bp='+bp+'&id='+<%=programId%>,
	        type: 'POST',
	        success: function(data) {
		        if(document.getElementById("excerciseid") == null){
		        	$("#span10").load(location.href+" #span10>*","",function() {
		        		dragDropFunction();
			          });
	        }
	          $("#exerciseMediaDiv").load(location.href+" #exerciseMediaDiv>*","",function() {
	        	  dragDropFunction();
	          });
	        }
	      });
	    });
	});
	
	function validateAddExercise(){
        if((document.getElementById("excerciseid") == null) || (document.getElementById("excerciseid").value == "")){
        	document.getElementById("errorhideorshow").innerHTML="Please Drag/Drop the exercises";
        	document.getElementById("errorhideorshow").style.display='block';
        	return false;
        }else {
        	document.getElementById("errorhideorshow").style.display='none';
        	return true;
        }
	}
	
	function redirectOnCancel() {
		window.location="/corp/programs/edit.jsp?id="+<%=programId%>;
	}
</script>
</head>

<body onload="dragDropFunction();">
	<div class="page-header">
		<%
			String exerciseFor = "";
			if (patient != null)
				exerciseFor = patient.getFullName();
			if (patientGroup != null)
				exerciseFor = patientGroup.getGroupName();
		%>
		<h1>
			Add Exercise <small><%=exerciseFor%></small>
		</h1>

	</div>
	<%@include file="/WEB-INF/includes/breadcrumbs.jsp"%>
	<div class="row">
		<div class="span10">
		
			<form method="post" action="add_exercise.jsp"
				class="well form-inline" name="bpType">
				<input type="hidden" name="id" value="<%=programId%>"> <input
					type="hidden" name="patient" value="<%=patientId%>"> <input
					type="hidden" name="group" value="<%=groupId%>">

				<div style="margin: 0 0 20px 1px; width: 250px;display: none;" >
					<input type="text" class="input-medium search-query" name="query"
						value="<%=Util.formValue(query)%>">
					<button type="submit" class="btn">Search</button>
				</div>
				<select name="type" id="type">
					<option value="">Select Exercise Type</option>
					<%
						for (Exercise.Type option : Exercise.Type.values()) {
					%>
					<option value="<%=option.name()%>"
						<%=(option.name().equals(type)) ? "selected" : ""%>><%=option.getValue()%></option>
					<%
						}
					%>
				</select> <select name="bp" id="bp">
					<option value="">Select Body Part</option>
					<%
						for (Exercise.BodyPart option : Exercise.BodyPart.values()) {
					%>
					<option value="<%=option.name()%>"
						<%=(option.name().equals(bodyPart)) ? "selected" : ""%>><%=option.getValue()%></option>
					<%
						}
					%>
				</select>
			</form>
		</div>
	</div>

	<div class="row">
	<div class="alert alert-error" id="errorhideorshow" 
		style="margin-left: 33px;width: 250px; display: none;" >
	</div>
		<div class="span10" id="span10">
			<%
				if (exercises == null) {
			%>
			<p>
				<b>Select categories to display exercises</b>
			</p>
			<%
				} else if (exercises.size() == 0) {
			%>
			<p>
				<b>There are no exercises for the selected categories</b>

			</p>
			<%
				} else {
			%><form method="POST" action="/AddExercise">
				<input type="hidden" name="excerciseid" id="excerciseid"> <input
					type="hidden" name="pgid" value="<%=programId%>">
				<div class="col-md-6 demo" id="exerciseMediaDiv">
					<ul class="thumbnails" style="margin-left: -8px; margin-top: 18px;">
						<%
							for (Exercise exercise : exercises) {
									String url = "/images/photo_146_97.gif";
									if (exercise.getKeyPhotoId() > 0) {
										url = "/media?id=" + exercise.getKeyPhotoId() + "&width=146&v=" + exercise.getKeyPhotoVersion()
												+ "&add=" + exercise.getId();
									}
									String link = "add_exercise.jsp?id=" + programId + "&patient=" + patientId + "&group=" + groupId
											+ "&add=" + exercise.getId();
						%>
						<li>
							<div class="thumbnail" id="<%=exercise.getId()%>"
								name="<%=exercise.getName()%>">

								<img class="dragImg" src="<%=url%>" id="<%=exercise.getId()%>"
									name="<%=exercise.getName()%>" width="146" height="105"><br>
								<h5 style="width: 146px; height: 35px; overflow: hidden;"><%=exercise.getName()%></h5>
							</div>
						</li>
						<%
							}
						%>
					</ul>

				</div>
				<div class="col-md-6 demo-1">
					<div class="dropthumbnail">

						<div id="dropHere" class="images">
							<h5 style="width: 146px; height: 35px; overflow: hidden;"></h5>
						</div>
					</div>
				</div>
				<div class="span10">
					<div class="form-actions">
						<input name="submit" class="btn btn-primary" onclick="return validateAddExercise();" type="submit" value="Save"/>
						<input name="submit" class="btn" onclick="redirectOnCancel();" value="Cancel" type="button"/>
					</div>

				</div>
			</form>
			<%
				}
			%>



		</div>

	</div>


</body>
</html>