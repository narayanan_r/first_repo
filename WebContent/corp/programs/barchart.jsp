<%@ page import="java.io.*" %>  
<%@ page import="java.util.*" %>  
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="java.net.URLEncoder" %>
<%@ page import="java.util.Date" %>
<%@page import="java.util.*,java.text.*"%>
<%@ page import="java.util.List" %>
<%@	page import="java.util.ArrayList"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
   <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%!  

  
    // --- String Join Function converts from Java array to javascript string.  
    public String join(ArrayList<?> arr, String del)  
    {  

        StringBuilder output = new StringBuilder();  

        for (int i = 0; i < arr.size(); i++)  
        {  

            if (i > 0) output.append(del);  

              // --- Quote strings, only, for JS syntax  
              if (arr.get(i) instanceof String) output.append("\"");  
              output.append(arr.get(i));  
              if (arr.get(i) instanceof String) output.append("\"");  
        }  

        return output.toString();  
    }  
%>
<html>  
<head>  
    <title>Simple JSP Demo</title>  
    <script type="text/javascript" src="../js/zingchart.min.js"></script>  
</head>  
<body>  

    <script>  
        <% 
        long programId = Long.parseLong(Util.notNull(request.getParameter("id"), "0"));
        LibraryManager lm = new LibraryManager();
        ProgramExercise pe = lm.getProgramExercise(programId);
        if (pe == null) {
            response.sendError(404, "Program not found");
            return;
        }
        
        String from=null;
        Program program = lm.getProgram(pe.getProgramId());
        Patient p2=lm.getPatient(program.getPatientId());
        long  exerciseId=pe.getExerciseId();
        int sequence=pe.getSequence();
      
          System.out.println("exerciseId : "+exerciseId);
          System.out.println("programId : "+programId);
          System.out.println("programId : "+programId);
          
           List<ProgramExercise> datelist=lm.saveProgramexcercisedate(programId,exerciseId,sequence);
           ArrayList<String> months = new ArrayList<String>();  
           ArrayList<Integer> sets = new ArrayList<Integer>();  

           for(ProgramExercise programExercise:datelist)
           {
        	   System.out.println("date"+programExercise.getCreatedDate());
        	   SimpleDateFormat s = new SimpleDateFormat("dd/MM/yy");
               String createdDate = s.format(programExercise.getCreatedDate());
               sets.add(programExercise.getMaxSets());
               System.err.println(createdDate);
        	   months.add(createdDate);
           }
           
    	if(programId>0){
    	from="ProgramName:"+program.getName()+";ExerciseName:"+pe.getName()+";Sets:"+pe.getSets()+";Reps:"+pe.getReps()+";Holds:"+pe.getHolds()+";Side:"+pe.getSide()+";Rest:"+pe.getRest()+";Description:"+pe.getDescription()+";";	
    	}
    	System.out.println("from"+from);
        System.out.println("programId"+programId);
        System.out.println("datelistinjsp"+datelist);
        String[][] breadcrumbs = {{"/corp/programs/", "Pie Chart"}, {"/corp/programs/", "Barchart"}, {null, "Chart" + " Barchart"}};
        
            ArrayList<Integer> users = new ArrayList<Integer>(); 
            ArrayList<Integer> yAxis = new ArrayList<Integer>();
                   
         /*    SimpleDateFormat s = new SimpleDateFormat("dd/MM");
            String d = s.format(new Date());
            System.err.println(d);
            
            Calendar cal = Calendar.getInstance();           
            cal.add(Calendar.DATE, -1);          
            Date todate1 = cal.getTime();    
            String fromdate = s.format(todate1);            
            System.err.println(fromdate);

            Calendar cal2 = Calendar.getInstance();           
            cal2.add(Calendar.DATE, -2);
            Date todate2 = cal2.getTime();    
            String fromdate2 = s.format(todate2);            
            System.err.println(fromdate2);
            
            Calendar cal3 = Calendar.getInstance();           
            cal3.add(Calendar.DATE, -3);
            Date todate3 = cal3.getTime();    
            String fromdate3 = s.format(todate3);            
            System.err.println(fromdate3);
            
            Calendar cal4 = Calendar.getInstance();           
            cal4.add(Calendar.DATE, 1);
            Date todate4 = cal4.getTime();    
            String fromdate4 = s.format(todate4);            
            System.err.println(fromdate4);
            
            Calendar cal5 = Calendar.getInstance();           
            cal5.add(Calendar.DATE, 2);
            Date todate5 = cal5.getTime();    
            String fromdate5 = s.format(todate5);            
            System.err.println(fromdate5);
            
            Calendar cal6 = Calendar.getInstance();           
            cal6.add(Calendar.DATE, 3);
            Date todate6 = cal6.getTime();    
            String fromdate6 = s.format(todate6);            
            System.err.println(fromdate6);
          
            ArrayList<String> dateList = new ArrayList<String>();  
            dateList.add(fromdate3);
            dateList.add(fromdate2);         
            dateList.add(fromdate);           
            dateList.add(d);
            dateList.add(fromdate4);
            dateList.add(fromdate5);
            dateList.add(fromdate6);
            months.addAll(dateList);
             */
           /*  ArrayList<Integer> sets = new ArrayList<Integer>();  
            sets.add(3);
            sets.add(0);
            sets.add(1);
            sets.add(4);
            sets.add(7);
            sets.add(4);
            sets.add(2);
 */            users.addAll(sets);				
                        
            System.err.println("Sets:"+pe.getSets());
            System.err.println("Reps:"+pe.getReps()); 
         
          /*   int counter = 11;  
            while(counter < 18)  
            {  
              months.add("Sept " + dateList);  
                users.add(counter=counter+1);   
            }   */
            
            int yLabel = 0;  
            while(yLabel < 11)  
            {  
            	if(yLabel==0){
            		yAxis.add(yLabel);  
            	}
            	yAxis.add(yLabel=yLabel+1);  
            }         
        %>  
	
       // --- add a comma after each value in the array and convert to javascript string representing an array  
        var monthData = [<%= join(months, ",") %>];  
        var userData = [<%= join(users, ",") %>];  
        var yScale = [<%= join(yAxis, ",") %>];  
 
    </script>
<script>  
window.onload = function() {  
  zingchart.render({
    id: "myChart",
    width: "100%",
    height: 400,
    data: {
      "type": "bar",
      "title": {
        "text": "Progress Bar"
      },
      "scale-x": {
        "labels": monthData
      },
      "scale-y": {
          "labels": yScale
        },
      "plot": {
        "line-width": 1,
        "background-color":"#db6e00"
      },
      "series": [{
        "values": userData
      }]
    }
  });
};
</script>
<%@include file="/WEB-INF/includes/breadcrumbs.jsp"%>
	
<div class="page-header">
 <h1><%=pe.getName()%></h1>
  </div>
  <div class="row">
    <div class="span6">
     <div id="myChart"></div>  
    </div>
  </div>
</body>  
</html>  



