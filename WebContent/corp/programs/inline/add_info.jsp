<%@page import="java.sql.ResultSet"%>
<%@page import="java.util.Date"%>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="java.net.URLEncoder" %>
<%@ page import="java.util.LinkedList" %>
<%@ page import="java.util.List" %>
<%@	page import="java.util.ArrayList"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");

    if (cu == null) {
        response.sendRedirect("/close.jsp?redir=" + URLEncoder.encode(Util.loginUrl(Constants.LoginReason.login, request), "UTF-8"));
        return;
    }

    User u = cu.getUser();
    Corporation corp = cu.getCorporation();
    /* try{
    	Util.allActivitiesLogs("Patients", u.getId(), corp.getName(), "Add Article");
    }catch(Exception e){
    	
    } */
    long programId = Long.parseLong(Util.notNull(request.getParameter("id"), "0"));
    long add = Long.parseLong(Util.notNull(request.getParameter("add"), "0"));

   /* String bodyPart = Util.notNull(request.getParameter("bp"), (String) session.getAttribute("bpType.bp"));
    String type = Util.notNull(request.getParameter("type"), (String) session.getAttribute("bpType.type"));

    session.setAttribute("bpType.bp", bodyPart);
    session.setAttribute("bpType.type", type);*/

    if (programId == 0) {
        response.sendRedirect(corp.getHost() + "/close.jsp");
        return;
    }

    LibraryManager lm = new LibraryManager();


    Program program = null;

    program = lm.getProgram(programId);
    if (program == null) {
        response.sendError(404, "Program no found");
        return;
    }

    Patient patient = lm.getPatient(program.getPatientId());

    List<MyInfo> myInfos = lm.listInfoForProgram(program.getId());
    LinkedList<Long> infoIds = new LinkedList<Long>();
    for (MyInfo myInfo : myInfos) {
        infoIds.add(myInfo.getId());
    }

    if (add > 0) {
        MyInfo info = lm.getInfo(add);
        List<Long> Eideditlist = (ArrayList<Long>)session.getAttribute("Eidlist");
        for(Long Eid : Eideditlist){
        if (info != null) {
            if (!infoIds.contains(info.getId())) {
                	lm.addInfo(Eid, info.getId());
                //update the programs last updated date
                program.setLastUpdated(new Date());
                program = lm.save(program);
            }}}
        if(patient!=null){
        String type="Patients";
        String activity_page="corp/programs/inline/add_info.jsp";
    	String corp_id=String.valueOf(corp.getId());
    	String corp_name=corp.getName();
    	String corp_loc=corp.getSuburb();
    	String user_id=String.valueOf(u.getId());
    	String user_name=u.getFullName();
    	String invitor_id=String.valueOf(patient.getId());
    	String invitor_name=patient.getFullName();
    	String flag="new";
    	String from="Add info into Program:"+program.getName();
    	String to="InfoHeadline:"+info.getHeadline();
    	String desc="Adding info to "+invitor_name+" By User";
    	try{
    		Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
        }catch(Exception e){
        	
        }
        }

            response.sendRedirect(corp.getHost() + "/close.jsp?redir=" + URLEncoder.encode("/corp/programs/edit.jsp?info=true&id=" + programId, "UTF-8"));
            return;
    }
    
    Exercise.BodyPart pBodyPart = null;
    Exercise.Type pType = null;
    /*try {
        pBodyPart = Exercise.BodyPart.valueOf(bodyPart);
    } catch (Exception e) {}

    try {
        pType = Exercise.Type.valueOf(type);
    } catch (Exception e) {}*/

    List<MyInfo> infos = lm.listInfo(corp.getId(), pBodyPart, pType);
%>
<html>
<head><title>Add Article</title></head>
<body>
<div class="page-header">
    <h1>Add Article <small><%=(patient != null) ? patient.getFullName() : ""%></small></h1>
</div>
<%--
<div class="row">
    <div class="span6">
        <form method="get" action="add_info.jsp" class="well form-inline" name="bpType">
            <input type="hidden" name="id" value="<%=programId%>">
            <select name="type" onchange="document.forms.bpType.submit();">
                <option value="">Select Exercise Type</option>
                <%
                    for (Exercise.Type option : Exercise.Type.values()) {
                %>
                <option value="<%=option.name()%>" <%=(option.name().equals(type)) ? "selected" : ""%>><%=option.getValue()%></option>
                <%
                    }
                %>
            </select>
            <select name="bp" onchange="document.forms.bpType.submit();">
                <option value="">Select Body Part</option>
                <%
                    for (Exercise.BodyPart option : Exercise.BodyPart.values()) {
                %>
                <option value="<%=option.name()%>" <%=(option.name().equals(bodyPart)) ? "selected" : ""%>><%=option.getValue()%></option>
                <%
                    }
                %>
            </select>
        </form>
    </div>
</div>
--%>
<div class="row">
    <div class="span6">
        <ul class="thumbnails">
            <%
                for (MyInfo info : infos) {
                    String url = "/images/photo_146_97.gif";
                    if (info.getAttachment() != null) {
                        url = info.getAttachment().getImageUrl(Attachment.ImageSize.f146);
                    }
                    String link = "add_info.jsp?id=" + programId + "&add=" + info.getId();
            %>
            <li>
                <div class="thumbnail">
                <a href="<%=link%>"><img src="<%=url%>" width="146" height="105"><br>
                <h5 style="width: 146px; height: 35px; overflow: hidden;"><%=info.getHeadline()%></h5></a>
                </div>
            </li>
            <%
                }
            %>
        </ul>
    </div>
</div>
</body>
</html>