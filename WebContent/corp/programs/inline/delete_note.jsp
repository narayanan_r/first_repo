<%@page import="java.util.Date"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.Corporation" %>
<%@ page import="au.com.ourbodycorp.model.CorporationUser" %>
<%@ page import="au.com.ourbodycorp.model.Program" %>
<%@ page import="au.com.ourbodycorp.model.ProgramNote" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.PreparedStatement" %>
<%@ page import="java.sql.*" %>
<%@	page import="java.util.ArrayList"%>
<%@ page import="java.util.List" %>
<%@ page import="java.net.URLEncoder" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%
    CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");

    Corporation corp = cu.getCorporation();
    User u = cu.getUser();
    long programId = Long.parseLong(Util.notNull(request.getParameter("program"), "0"));
    long noteId = Long.parseLong(Util.notNull(request.getParameter("note"), "0"));
   /*  try{
    	Util.allActivitiesLogs("Patients", u.getId(), corp.getName(), "Delete Note");
    }catch(Exception e){
    	
    } */
    Program program = null;
    ProgramNote note = null;
    String n=null;
    LibraryManager lm = new LibraryManager();
    if (noteId != 0) {
        note = lm.getNote(noteId);
        if (note == null) {
            response.sendError(404, "Note not found");
            return;
        }
        n=note.getNote();
        program = lm.getProgram(note.getProgramId());
        
//         if (program.getCorpId() != corp.getId()) {
//             response.sendError(403);
//             return;
//         }
    } else {
        response.sendError(400, "No note ID provided");
        return;
    }

    if (request.getMethod().equalsIgnoreCase("post")) {
        if (request.getParameter("submit").startsWith("Cancel")) {
            response.sendRedirect(corp.getHost() + "/close.jsp");
        } 
        else {
        	List<Long> Eideditlist = (ArrayList<Long>)session.getAttribute("Eidlist");
        	for(Long Eid : Eideditlist){
        		Connection conn = Util.getConnection();
	            PreparedStatement pst = conn.prepareStatement("select id from program_notes where program = ? and note = ? ");
	            pst.setLong(1, Eid);
	            pst.setString(2, note.getNote());
	       		ResultSet rs = pst.executeQuery();
	       		if(rs.next()){
	       			lm.deleteNote(rs.getLong("id"));
	       		}
	       		conn.close();
        	}
        	Patient p=lm.getPatient(program.getPatientId());
            //update the programs last updated date
            program.setLastUpdated(new Date());
            program = lm.save(program);
            program.setLastUpdated(new Date());
            program = lm.save(program);
            
            if(p!=null)
            {
            String type="Patients";
            String activity_page="corp/programs/inline/delete_note.jsp";
        	String corp_id=String.valueOf(corp.getId());
        	String corp_name=corp.getName();
        	String corp_loc=corp.getSuburb();
        	String user_id=String.valueOf(u.getId());
        	String user_name=u.getFullName();
        	String invitor_id=String.valueOf(p.getId());
        	String invitor_name=p.getFullName();
        	String flag="delete";
        	String from="---";
        	String to="---";
        	String desc="Deleted note "+n+" from Program "+program.getName()+" By User";
            try
            {		
             Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
            }
            catch(Exception e)
            {
            }
            }
            response.sendRedirect(corp.getHost() + "/close.jsp?redir=" + URLEncoder.encode("/corp/programs/edit.jsp?id="+program.getId()+"", "UTF-8"));
        }
        return;
    }

 %>

<html>
<head><title>Delete Note</title></head>
<body>
<div class="page-header">
    <h1>Delete Note</h1>
</div>
<div class="row">
    <div class="span6">
        <p>
            Delete note from this program.
        </p>
        <p>
            Are you sure?
        </p>
        <form action="delete_note.jsp" method="post">
            <input type="hidden" name="note" value="<%=note.getId()%>">
            <%=HTML.confirmCancel()%>
        </form>
    </div>
</div>
</body>
</html>