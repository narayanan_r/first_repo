<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@ page import="au.com.ourbodycorp.Config" %>
<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.MailMessage" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="java.util.Properties" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");

    if (cu == null) {
        response.sendRedirect("/close.jsp");
        return;
    }

    User u = cu.getUser();
    Corporation corp = cu.getCorporation();

    long id = Long.parseLong(request.getParameter("id"));

    LibraryManager lm = new LibraryManager();
    Program program = lm.getProgram(id);
    Patient patient = lm.getPatient(program.getPatientId());
    PatientGroup patientGroup = lm.getPatientGroup(program.getGroupId());
    if(patient != null){
    	String type="Patients";
        String activity_page="corp/programs/inline/manual_entry.jsp";
    	String corp_id=String.valueOf(corp.getId());
    	String corp_name=corp.getName();
    	String corp_loc=corp.getSuburb();
    	String user_id=String.valueOf(u.getId());
    	String user_name=u.getFullName();
    	String invitor_id=String.valueOf(patient.getId());
    	String invitor_name=patient.getFullName();
    	String flag="maualEntry";
    	String from="---";
    	String to="---";
    	String desc="Manual entry for Patients "+patient.getFullName()+" By User";
        try
        {		
         Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
        }
        catch(Exception e)
        {
        }
	}else{
		String type="Patients";
        String activity_page="corp/programs/inline/manual_entry.jsp";
    	String corp_id=String.valueOf(corp.getId());
    	String corp_name=corp.getName();
    	String corp_loc=corp.getSuburb();
    	String user_id=String.valueOf(u.getId());
    	String user_name=u.getFullName();
    	String invitor_id=String.valueOf(patientGroup.getId());
    	String invitor_name=patientGroup.getGroupName();
    	String flag="manualEntry";
    	String from="---";
    	String to="---";
    	String desc="Manual enrty for Group Patients "+patientGroup.getGroupName()+" By User";
        try
        {		
         Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
        }
        catch(Exception e)
        {
        }
	}

%>
<html>
<head><title>Send Email</title>
</head>
<body>
<div class="page-header">
    <h1>Manual Entry</h1>
</div>
<div class="row">
    <div class="span9">
        <form action="manual_entry.jsp" method="post" class="form-horizontal">
        	<p>If you have Fiizio App installed:</p>
        	<p>Please enter this username and password to download this program:</p>
        	 <div class="control-group" style="margin-top: 15px;">
                    <label class="control-label">Username:</label>
                <div class="controls">
                <%if(patient != null){ %>
                	<input type="text" value="<%=patient.getLastName().toLowerCase()%>" readonly="readonly" style="cursor: text;"/>
                <%}else{ %>
                    <input type="text" value="<%=patientGroup.getUserName().toLowerCase()%>" readonly="readonly" style="cursor: text;"/>
                <%} %>
            	</div>
             </div>
             <div class="control-group" style="margin-top: 15px;">
                    <label class="control-label">Password:</label>
                <div class="controls">
                    <input type="text" value="<%=program.getCode().toLowerCase()%>" readonly="readonly" style="cursor: text;"/>
            	</div>
             </div>
        </form>
    </div>
</div>
</body>
</html>