<%@page import="javax.sound.midi.SysexMessage"%>
<%@ page import="java.sql.ResultSet"%>
<%@ page import="java.sql.PreparedStatement"%>
<%@ page import="java.sql.Connection" %>
<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="java.net.URLEncoder" %>
<%@ page import="au.com.ourbodycorp.model.managers.CorporationManager" %>
<%@ page import="java.util.List" %>
<%@	page import="java.util.ArrayList"%>
<%@ page import="java.util.Date" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%

System.out.println("inside edit_details jsp=======>");

    CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");
	Corporation c = (Corporation) request.getAttribute("corp");
	boolean showLocation = request.getParameter("showLocation") != null && Boolean.parseBoolean(request.getParameter("showLocation"));
    String[] multitype = null;
    if(showLocation == true){
    multitype = request.getParameterValues("multitype");
    }
    if (cu == null) {
        response.sendRedirect("/close.jsp?redir=" + URLEncoder.encode(Util.loginUrl(Constants.LoginReason.login, request), "UTF-8"));
        return;
    }

    User u = cu.getUser();
    
    Corporation corp = cu.getCorporation();
    String activity_page="corp/programs/inline/edit_details.jsp";
	String corp_id=String.valueOf(corp.getId());
	String corp_name=corp.getName();
	String corp_loc=corp.getSuburb();
	String user_id=String.valueOf(u.getId());
	String user_name=u.getFullName();
	String old=null;
    long programId = Long.parseLong(Util.notNull(request.getParameter("id"), "0"));
    System.out.println("programId=========>"+programId);
    String Check = request.getParameter("Check");
		if(Check != null){
			session.setAttribute("Check", Check);
		}
    long uId = 0;
    if(session.getAttribute("Check").equals("Name_Exist")){
    	session.setAttribute("uId", 0);
    }else{
    	uId = Long.parseLong(session.getAttribute("uId").toString());
    }
    long patientId = Long.parseLong(Util.notNull(request.getParameter("patient"), "0"));
    long groupId = Long.parseLong(Util.notNull(request.getParameter("group"), "0"));
    
    LibraryManager lm = new LibraryManager();
    Program program = null;
    Patient patient = null;
    PatientGroup group = null;

    if (programId > 0) {
    	/*  try{
    	    	Util.allActivitiesLogs("Patients", u.getId(), corp.getName(), "Edit Program");
    	    }catch(Exception e){
    	    	
    	    } */
        program = lm.getProgram(programId);
        if (program == null) {
            response.sendError(404, "Program not found");
            return;
        }
   	 	old="Program Name: "+program.getName()+";Frequency: Interval="+program.getInterval()+";Type="+program.getIntervalType();
        Long preId = programId;
    	if(preId == programId){
    	session.setAttribute("preId", preId);
    	}
    } 
    else if (patientId > 0) {
    	/* try{
	    	Util.allActivitiesLogs("Patients", u.getId(), corp.getName(), "Create Program");
	    }catch(Exception e){
	    	
	    } */
        patient = lm.getPatient(patientId);
    }
    else if (groupId > 0) {
    	/* try{
	    	Util.allActivitiesLogs("Patient Groups", u.getId(), corp.getName(), "Create Program");
	    }catch(Exception e){
	    	
	    } */
        group = lm.getPatientGroup(groupId);
    }
    else{
    	/* try{
	    	Util.allActivitiesLogs("Templates", u.getId(), corp.getName(), "Create Program");
	    }catch(Exception e){
	    	
	    } */
    }

    if ((program != null && program.getCorpId() != corp.getId()) || 
            (patient != null && patient.getCorpId() != corp.getId()) || 
            (group != null && group.getCorporationId() != corp.getId()) ) {
        response.sendRedirect(corp.getHost() + "/close.jsp");
        return;
    }
    Boolean shareLoc = false;
    if (program != null) {
        patient = lm.getPatient(program.getPatientId());
        group = lm.getPatientGroup(program.getGroupId());
        shareLoc = program.isSharelocation();
    }

    String intervalChoices[] = {"1","2","3","4","5","6","7","8","9","10","11","12"};

    String name = Util.notNull(request.getParameter("name"));
    String interval = Util.notNull(request.getParameter("interval"), intervalChoices[0]);
    String intervalType = Util.notNull(request.getParameter("intervalType"));
    String error = null;
    if (request.getMethod().equalsIgnoreCase("post")) {
    	
    	
    	System.out.println("program scheduled mail cheking post method------>");
    	Invitation i=new Invitation();
    	//// MailMessage msg = new MailMessage(props, "invite.vm", i.getEmail(), "Invitation to join " + i.getCorporation().getName(), true);
    	 
    	 
    	
    	
        if (request.getParameter("submit").startsWith("Cancel")) {
            response.sendRedirect(corp.getHost() + "/close.jsp");
            return;
        }
       
        if (name.equals("")) {
            error = "Please enter a name";
        } else if(showLocation == true){
        	if(multitype == null){
				error = "Please select a location";
        	}
        }
        
        if (error == null) {
        	if(uId!=0)
	        {  CorporationManager cm = new CorporationManager();
	           int intr = Integer.parseInt(interval);
	           cm.chkForTemplateNameUpdate(uId,name,intervalType,intr);
	        } if (programId > 0) {
	        	String type=null;
	        	String invitor_id=null;
	        	String invitor_name=null;	
	        	if(patient!=null){
	        		type="Patient";
	        		invitor_id=String.valueOf(patient.getId());
		        	invitor_name=patient.getFullName();
	        	}else{
	        		type="Template";
	        		invitor_id="";
		        	invitor_name=request.getParameter("name");	
	        	}
	        	
	        	String flag="update";
	        	String to="Program Name :"+request.getParameter("name")+";Frequency: Interval="+request.getParameter("interval")+";Type="+request.getParameter("intervalType");
	        	String desc="Edit Program "+invitor_name+" By User";
	        	 try{
	     	    	//Util.allActivitiesLogs("Patients", u.getId(), corp.getName(), "Edit Program");
	     	    	Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,old,to,desc);
	           		response.sendRedirect(corp.getHost() + "/close.jsp?redir=" + URLEncoder.encode("/corp/programs/edit.jsp?id=" + program.getId(), "UTF-8"));
	                return;
	         	}catch(Exception ee){
	         		
	         	}
	        }else if(multitype != null){
        		for(String companyid : multitype){
        				uId = Long.parseLong(session.getAttribute("uId").toString());
            			List<Corporation> Exerceid = null;
        			    if (u != null && name != null) {
        			        CorporationManager cm = new CorporationManager();
        			   Exerceid = cm.getExerciseidforPrograms(Long.parseLong(companyid),name,uId,programId);
        			    }
        			    if(Long.parseLong(companyid) != c.getId()){
            				if(!Exerceid.isEmpty()){
            					for (Corporation corpuexid : Exerceid) {
            						program.setId(corpuexid.getId());
            						program.setCode(corpuexid.getName());
            						program.setLastUpdated(new Date());
            						program.setPatientId(corpuexid.getCompanyId());
            						program.setGroupId(corpuexid.getCompanyId());
            					}
            				}else{
            					program = new Program();
                	            program.setCreated(new Date());
                	            program.setCreator(u.getId());
                	            program.setName(Util.safeInput(name, false));
                    	        program.setInterval(Integer.parseInt(interval));
                    	        try {
                    	            program.setIntervalType(Program.IntervalType.valueOf(intervalType));
                    	        } catch (Exception e) {
                    	            // Don't care
                    	        }
                    	        program.setCorpId(Long.parseLong(companyid));
                    	        program.setSharelocation(showLocation);
                    	        if(showLocation == true){
                    	        	program.setShareprogram(true);
                    	        	program.setSharegroup(true);
                    	        }
                    	        	program.setShareid(uId);
                    	        lm.save(program);
                    	        Connection conn = Util.getConnection();
                            	if(uId == 0){
                	        		PreparedStatement pst;
                		            pst = conn.prepareStatement("select shareid from programs where id = ?");
                		            pst.setLong(1, program.getId());
                		            ResultSet rs = pst.executeQuery();
                		            if(rs.next()){
                		            	session.setAttribute("uId", rs.getLong("shareid"));
                		            }
                		            pst.close();
                        		}
                            	conn.close();
                    	        if(programId > 0){
                    	        	lm.copyProgramforAllLocations(programId, u.getId(), program.getId());
                            	}
							}
                	        program.setName(Util.safeInput(name, false));
                	        program.setInterval(Integer.parseInt(interval));
                	        try {
                	            program.setIntervalType(Program.IntervalType.valueOf(intervalType));
                	        } catch (Exception e) {
                	            // Don't care
                	        }
                	        program.setCorpId(Long.parseLong(companyid));
                	        program.setSharelocation(showLocation);
                	        if(showLocation == true){
                	        	program.setShareprogram(true);
                	        	program.setSharegroup(true);
                	        }
                	        program.setShareid(uId);
                	        lm.save(program);
                	        
        			    }
        			}
        	}
	        if(patient != null && showLocation == true){
    	   	List<Corporation> Exerceid = null;
    	   	uId = Long.parseLong(session.getAttribute("uId").toString());
      		CorporationManager cm = new CorporationManager();
      	  
   	        Exerceid = cm.getExerciseidforPatientPrograms(corp.getId(),name,uId,programId);
   	     	if(!Exerceid.isEmpty()){
   	        for (Corporation corpuexid : Exerceid) {
	      		program.setId(corpuexid.getId());
   	        	program.setCode(corpuexid.getName());
   	        }
      			program.setLastUpdated(new Date());
      			}else{
           			program = new Program();
           			program.setCreator(u.getId());
      			}
        program.setName(Util.safeInput(name, false));
        program.setInterval(Integer.parseInt(interval));
        try {
            program.setIntervalType(Program.IntervalType.valueOf(intervalType));
        } catch (Exception e) {
            // Don't care
        }
        program.setCorpId(corp.getId());
        program.setCreated(new Date());
        program.setSharelocation(showLocation);
        if(showLocation == true){
        	program.setShareprogram(true);
        	program.setSharegroup(true);
        }
        program.setShareid(uId);
        lm.save(program);
        if(programId > 0 ){
        	if(shareLoc == false){
        		lm.copyProgramforAllLocations(programId, u.getId(), program.getId());
        	}
    	}
        }
	        if(group != null && showLocation == true){
    	   	List<Corporation> Exerceid = null;
    	   	uId = Long.parseLong(session.getAttribute("uId").toString());
      		CorporationManager cm = new CorporationManager();
      		
   	        Exerceid = cm.getExerciseidforGroupPrograms(corp.getId(),name,uId,programId);
   	     	if(!Exerceid.isEmpty()){
   	        for (Corporation corpuexid : Exerceid) {
	      		program.setId(corpuexid.getId());
   	        	program.setCode(corpuexid.getName());
   	        }
      			program.setLastUpdated(new Date());
      			}else{
           			program = new Program();
           			program.setCreator(u.getId());
      			}
        program.setName(Util.safeInput(name, false));
        program.setInterval(Integer.parseInt(interval));
        try {
            program.setIntervalType(Program.IntervalType.valueOf(intervalType));
        } catch (Exception e) {
            // Don't care
        }
        program.setCorpId(corp.getId());
        program.setCreated(new Date());
        program.setSharelocation(showLocation);
        if(showLocation == true){
        	program.setShareprogram(true);
        	program.setSharegroup(true);
        }
        program.setShareid(uId);
        lm.save(program);
        if(programId > 0){
       	if(shareLoc == false){
       		lm.copyProgramforAllLocations(programId, u.getId(), program.getId());
       	}
   	}}
        if(showLocation == true){
        	List<Corporation> Exerceid = null;
        	CorporationManager cm = new CorporationManager();
    	   	uId = Long.parseLong(session.getAttribute("uId").toString());
   	        Exerceid = cm.getExerciseidforPatientorGroupPrograms(corp.getCompanyId(),name,uId,programId);
   	     	if(!Exerceid.isEmpty()){
   	        for (Corporation corpuexid : Exerceid) {
   	        	if(corpuexid.getId() != programId){
	      		program.setId(corpuexid.getId());
   	        	program.setCode(corpuexid.getName());
   	        	program.setCorpId(corpuexid.getCompanyId());
   	        	program.setPatientId(corpuexid.getMaxPatients());
   	        	program.setGroupId(corpuexid.getMaxPractitioners());
      			program.setLastUpdated(new Date());
      			program.setName(Util.safeInput(name, false));
      	        program.setInterval(Integer.parseInt(interval));
      	        try {
      	            program.setIntervalType(Program.IntervalType.valueOf(intervalType));
      	        } catch (Exception e) {
      	            // Don't care
      	        }
      	        program.setCreated(new Date());
      	        program.setSharelocation(showLocation);
      	        if(showLocation == true){
      	        	program.setShareprogram(true);
      	        	program.setSharegroup(true);
      	        }
      	        program.setShareid(uId);
      	        lm.save(program);
      	        if(programId > 0 ){
      	        		lm.copyProgramforAllLocationPatients(programId, u.getId(), program.getId());
      	    	}
   	        	}
      		}
   	     	}
        }
        
       if(programId > 0){
   		List<Corporation> Exerceid = null;
   		uId = Long.parseLong(session.getAttribute("uId").toString());
   		CorporationManager cm = new CorporationManager();
   			long preId = Long.parseLong(session.getAttribute("preId").toString());
	        Exerceid = cm.getExerciseidforProgramUnique(preId);
	        for (Corporation corpuexid : Exerceid) {
	        	program.setCode(corpuexid.getName());
	        }
		   		program.setId(Long.parseLong(session.getAttribute("preId").toString()));
		   		program.setLastUpdated(new Date());
   			}else{
        			program = new Program();
        			program.setCreator(u.getId());
   			}
            program.setName(Util.safeInput(name, false));
            program.setInterval(Integer.parseInt(interval));
            try {
                program.setIntervalType(Program.IntervalType.valueOf(intervalType));
            } catch (Exception e) {
                // Don't care
            }
            program.setCorpId(corp.getId());
            program.setCreated(new Date());
            program.setSharelocation(showLocation);
            String invitor_id=String.valueOf(program.getId());
        	String invitor_name=program.getName();
        	String flag="add";
        	String from="";
        	String to="";
        	String desc="Create Program: '"+invitor_name+"' to Template By User";
    		if (patient != null) {
            	try{
         	    	//Util.allActivitiesLogs("Patients", u.getId(), corp.getName(), "Create Program");
         	    	Util.allActivitiesLogs2("Patients",activity_page,corp_id,corp_name,corp_loc,user_id,user_name,String.valueOf(patient.getId()),patient.getFullName(),flag,from,to,"Create Program: '"+invitor_name+"' to Patient: "+patient.getFullName()+" By User");
             	}catch(Exception ee){
             	}
                program.setPatientId(patient.getId());
            }else if (group != null) {
            	if(patient == null){
                	program.setPatientId(0);
                }
            	try{
         	    	//Util.allActivitiesLogs("Patient Groups", u.getId(), corp.getName(), "Create Program");
         	    	Util.allActivitiesLogs2("Patient Groups",activity_page,corp_id,corp_name,corp_loc,user_id,user_name,String.valueOf(group.getId()),group.getGroupName(),flag,from,to,"Create Program: '"+invitor_name+"' to Patient Group: "+group.getGroupName()+" By User");
             	}catch(Exception ee){
             	}
                program.setGroupId(group.getId());
            }else{
            	if(patient == null){
                	program.setPatientId(0);
                }
            	try{
              		 //Util.allActivitiesLogs("Templates", u.getId(), corp.getName(), "Create Program");
              		 Util.allActivitiesLogs2("Templates",activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
               }catch(Exception ee){
               }
            	program.setGroupId(0);
            }
	        	program.setShareprogram(true);
	        	program.setSharegroup(true);
	        	program.setShareid(uId);
            lm.save(program);
            
            if (!session.getAttribute("Create").equals("New_Template")){
            	
            if(showLocation == false){
             	List<Corporation> corpsid = null;
            	List<Long> Eidlist = new ArrayList<Long>();
			    if (u != null) {
			        CorporationManager cm = new CorporationManager();
			        if(!name.equals("")){
			        corpsid = lm.getCorpidforPatientPrograms(name,corp.getCompanyId(),null,uId,programId);
			        }
			    }
            if (corpsid != null && corpsid.size() > 0) {
                   if (corpsid != null && corpsid.size() > 0) {
                          for (Corporation corpu1 : corpsid) {
                		  Eidlist.add(corpu1.getId());
                    }
                   }}
            		Connection conn = Util.getConnection();
            	for(Long Eid : Eidlist){
	        		PreparedStatement pst;
		            pst = conn.prepareStatement("UPDATE programs set sharelocation = FALSE, shareprogram = FALSE, sharegroup = FALSE where id = ?");
		            pst.setLong(1, Eid);
		            pst.executeUpdate();
		            pst.close();
        		}
            	conn.close();
            }else{
            	List<Corporation> corpsid = null;
            	List<Long> Eidlist = new ArrayList<Long>();
			    if (u != null) {
			        CorporationManager cm = new CorporationManager();
			        if(!name.equals("")){
			        corpsid = lm.getCorpidforPatientPrograms(name,corp.getCompanyId(),null,uId,programId);
			        }
			    }
            if (corpsid != null && corpsid.size() > 0) {
                   if (corpsid != null && corpsid.size() > 0) {
                          for (Corporation corpu1 : corpsid) {
                		  Eidlist.add(corpu1.getId());
                    }
                   }}
            		Connection conn = Util.getConnection();
            	for(Long Eid : Eidlist){
	        		PreparedStatement pst;
		            pst = conn.prepareStatement("UPDATE programs set sharelocation = TRUE, shareprogram = TRUE, sharegroup = TRUE where id = ?");
		            pst.setLong(1, Eid);
		            pst.executeUpdate();
		            pst.close();
        		}
            	conn.close();
            }
            }
       		response.sendRedirect(corp.getHost() + "/close.jsp?redir=" + URLEncoder.encode("/corp/programs/edit.jsp?id=" + program.getId(), "UTF-8"));
        return;
    } }
    else if (program != null) {
        name = program.getName();
        interval = Integer.toString(program.getInterval());
        intervalType = (program.getIntervalType() != null) ? program.getIntervalType().name() : "perDay";
        showLocation = program.isSharelocation();
    }

%>
<html>
<head><title>Edit Program</title>
<script type="text/javascript">
$( document ).ready(function showorhide(){
	//System.out.print("fdfd===>");
	//console.log("fdfd===>")
	var checked = $("input[name='showLocation']").attr("checked") ? "True" : "False";
	var foo = []; 
	$('#multitypedub :selected').each(function(i, selected){ 
	  foo[i] = $(selected).text(); 
	});
	$.each(foo, function( index, value ) {
	var row = $('#multitype');
	row.find("option[value="+value+']').attr('selected','selected');
		});
	if(checked == "True"){
	$("#multitype").show();
	}else{
	$("#multitype").hide();
	}
	
});
function showorhide(){
	var foo2 = []; 
	$('#multitypedub :selected').each(function(i, selected){ 
	  foo2[i] = $(selected).text(); 
	});
	$.each(foo2, function( index, value ) {
	var row = $('#multitype');
	row.find("option[value="+value+']').attr('selected','selected');
		});
	var checked = $("input[name='showLocation']").attr("checked") ? "True" : "False";
	if(checked == "True"){
	$("#multitype").show();
	}else{
	$("#multitype").hide();
	}
}

function showorhidetabs() {
	var foo2 = []; 
	$('#multitypedub :selected').each(function(i, selected){ 
	  foo2[i] = $(selected).text(); 
	});
	$.each(foo2, function( index, value ) {
	var row = $('#multitype');
	row.find("option[value="+value+']').attr('selected','selected');
		});
	
}
</script>
</head>
<body>
<%@include file="/WEB-INF/includes/formErrorSimple.jsp"%>
<div class="page-header">
    <% String programFor = "Template";
        if (patient != null) programFor = patient.getFullName();
        if (group != null) programFor = group.getGroupName();
     %>
    <h1><%=(programId > 0) ? "Edit" : "Create"%> Program <small><%=programFor%></small></h1>
</div>
<div class="row">
    <div class="span6">
        <form action="edit_details.jsp" method="post" class="form-horizontal">
            <input type="hidden" name="id" value="<%=programId%>">
            <input type="hidden" name="patient" value="<%=patientId%>">
            <input type="hidden" name="group" value="<%=groupId%>">
            <%=HTML.textInput("name", "Name", "100", "100", name)%>
            <div class="control-group">
            <label class="control-label">Frequency</label>
            <div class="controls">
                <select name="interval" class="input-mini">
                    <%
                        for (String ic : intervalChoices) {
                    %>
                    <option value="<%=ic%>" <%=(ic.equals(interval)) ? "selected" : ""%>><%=ic%></option>
                    <%
                        }
                    %>
                </select>
                <select name="intervalType" class="input-large">
                    <option value="perDay" <%=(intervalType.equals("perDay")) ? "selected" : ""%>>(number) Times per Day</option>
                    <option value="perWeek" <%=(intervalType.equals("perWeek")) ? "selected" : ""%>>(number) Times per Week</option>
                    <option value="hours" <%=(intervalType.equals("hours")) ? "selected" : ""%>>Every (number) Hours</option>
                </select>
            </div>
          </div>
            <%
          				String Create = request.getParameter("Create");
          				if(Create != null){
         					session.setAttribute("Create", Create);
          				}
          				if(session.getAttribute("Create") != null) {
                        if(session.getAttribute("Create").equals("New_Template")) {
                    %>
            <%CorporationManager cm = new CorporationManager();
            if(cm.getCorporations(u.getId()).size() > 1){
            %>
            <div class="control-group">
            <label class="control-label" for="optionsCheckbox">Share across locations</label>
            <div class="controls">
              <label class="checkbox">
                <input type="checkbox" onclick="showorhide();" id="optionsCheckbox" name="showLocation" value="true"<%=(showLocation) ? " checked": ""%>>
                </label>
                <select id="multitype" name="multitype" onclick="showorhidetabs();" onchange="showorhidetabs();" multiple style="display: none;margin: -5px 0 0 19px;">
             <% List<Corporation> corps = null;
			    if (u != null) {
// 			        CorporationManager cm = new CorporationManager();
			        corps = cm.getCorporations(u.getId());
			    }%>
            <%
            if (corps != null && corps.size() > 0) {
                    for (Corporation corpu : corps) {
                        if (c != null && c.equals(corpu)) {
                            continue;
                        }
                  %>
                   <option value="<%=corpu.getId()%>"><%=corpu.getName()%></option>
                  <%
                    }
                %>

                  <%
                      }%>
              </select>
              
              <select id="multitypename" name="multitypename" multiple style="display: none; margin: -5px 0 0 19px;">
             <% 
                List<Corporation> corpsid = null;
			    if (u != null) {
// 			        CorporationManager cm = new CorporationManager();
			        if(!name.equals("")){
			        		corpsid = lm.getCorpidforPatientPrograms(name,corp.getCompanyId(),null,uId,programId);
			        }
			    }%>
            <% 
            if (corpsid != null && corpsid.size() > 0) {
                   if (corpsid != null && corpsid.size() > 0) {
                          for (Corporation corpu1 : corpsid) {
                  %>
                  <option value="<%=corpu1.getId()%>" <%=(corpu1.getId() == corpu1.getId()) ? "selected" : ""%>><%=corpu1.getName()%>
                		  <% 
                  %></option>
                  <%
                    }
                %>

                  <%
                   }
            }%>
              </select>
            </div>
          </div>
          <%} %>
          <select id="multitypedub" name="multitypedub" multiple style="display: none; margin: -5px 0 0 19px;">
             <% 
             	List<Corporation> corpsid = null;
            	List<Long> Eidlist = new ArrayList<Long>();
			    if (u != null) {
// 			        CorporationManager cm = new CorporationManager();
			        if(!name.equals("")){
			        	if(showLocation == false){
			        		corpsid = lm.getCorpidforPatientPrograms(name,corp.getId(),"NotShare",0,programId);
			        	}else{
			        		corpsid = lm.getCorpidforPatientPrograms(name,corp.getCompanyId(),null,uId,programId);
			        	}
			        }
			    }%>
            <% 
            if (corpsid != null && corpsid.size() > 0) {
                   if (corpsid != null && corpsid.size() > 0) {
                          for (Corporation corpu1 : corpsid) {
                  %>
                  <option value="<%=corpu1.getId()%>" <%=(corpu1.getCompanyId() == corpu1.getCompanyId()) ? "selected" : ""%>><%=corpu1.getCompanyId()%>
                		  <% 
                		  Eidlist.add(corpu1.getId());
                  %></option>
                  <%
                    }
                %>

                  <%
                   }
                   session.setAttribute("Eidlist", Eidlist);
            }else{
         	   		session.setAttribute("Eidlist", null);
        	}%>
              </select>
           <%
                        }if (session.getAttribute("Create").equals("No_Template")){%>
                        <%CorporationManager cm = new CorporationManager();
			            if(cm.getCorporations(u.getId()).size() > 1){
			            %>
                        	<div class="control-group">
                            <label class="control-label" for="optionsCheckbox">Share across locations</label>
                            <div class="controls">
                              <label class="checkbox">
                                <input type="checkbox" onclick="showorhide();" id="optionsCheckbox" name="showLocation" value="true"<%=(showLocation) ? " checked": ""%>>
                                </label>
                                <select id="multitype" name="multitype" onclick="showorhidetabs();" onchange="showorhidetabs();" multiple style="display: none;margin: -5px 0 0 19px;">
                             <% List<Corporation> corps = null;
                			    if (u != null) {
//                 			        CorporationManager cm = new CorporationManager();
                			        corps = cm.getCorporations(u.getId());
                			    }%>
                            <%
                            if (corps != null && corps.size() > 0) {
                                    for (Corporation corpu : corps) {
                                        if (c != null && c.equals(corpu)) {
                                            continue;
                                        }
                                  %>
                                   <option value="<%=corpu.getId()%>"><%=corpu.getName()%></option>
                                  <%
                                    }
                                %>

                                  <%
                                      }%>
                              </select>
                              <select id="multitypedub" name="multitypedub" multiple style="display: none; margin: -5px 0 0 19px;">
                             <% 
                             	List<Corporation> corpsid = null;
                            	List<Long> Eidlist = new ArrayList<Long>();
                			    if (u != null) {
//                 			        CorporationManager cm = new CorporationManager();
                			        if(!name.equals("")){
                			        	if(showLocation == false){
                			        		corpsid = lm.getCorpidforPatientPrograms(name,corp.getId(),"NotShare",0,programId);
                			        	}else{
                			        		corpsid = lm.getCorpidforPatientPrograms(name,corp.getCompanyId(),null,uId,programId);
                			        	}
                			        }
                			    }%>
                            <% 
                            if (corpsid != null && corpsid.size() > 0) {
                                   if (corpsid != null && corpsid.size() > 0) {
                                          for (Corporation corpu1 : corpsid) {
                                  %>
                                  <option value="<%=corpu1.getId()%>" <%=(corpu1.getCompanyId() == corpu1.getCompanyId()) ? "selected" : ""%>><%=corpu1.getCompanyId()%>
                                		  <% 
                                		  Eidlist.add(corpu1.getId());
                                  %></option>
                                  <%
                                    }
                                    }
                                   session.setAttribute("Eidlist", Eidlist);
                            }else{
                         	   		session.setAttribute("Eidlist", null);
                        	} %>
                              </select>
                              <select id="multitypename" name="multitypename" multiple style="display: none; margin: -5px 0 0 19px;">
                             <% 
                			    if (u != null) {
//                 			        CorporationManager cm = new CorporationManager();
                			        if(!name.equals("")){
                			        		corpsid = lm.getCorpidforPatientPrograms(name,corp.getCompanyId(),null,uId,programId);
                			        }
                			    }%>
                            <% 
                            if (corpsid != null && corpsid.size() > 0) {
                                   if (corpsid != null && corpsid.size() > 0) {
                                          for (Corporation corpu1 : corpsid) {
                                  %>
                                  <option value="<%=corpu1.getId()%>" <%=(corpu1.getId() == corpu1.getId()) ? "selected" : ""%>><%=corpu1.getName()%>
                                		  <% 
                                  %></option>
                                  <%
                                    }
                                %>

                                  <%
                                   }
                            }%>
                              </select>
                            </div>
                          </div>
                          <%} %>
                       <%
                        }

          				} %>
            <%=HTML.saveCancel()%>
        </form>
    </div>
</div>
</body>
</html>