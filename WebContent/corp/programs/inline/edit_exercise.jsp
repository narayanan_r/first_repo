<%@page import="java.util.List"%>
<%@page import="java.util.Date"%>
<%@ page import="au.com.ourbodycorp.HTML"%>
<%@ page import="au.com.ourbodycorp.Util"%>
<%@ page import="au.com.ourbodycorp.model.*"%>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager"%>
<%@ page import="java.net.URLEncoder"%>
<%@ page import="au.com.ourbodycorp.model.managers.CorporationManager"%>
<%@ page import="java.util.List"%>
<%@	page import="java.util.ArrayList"%>
<%@ page import="java.sql.Connection"%>
<%@ page import="java.sql.PreparedStatement"%>
<%@ page import="java.sql.*"%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%
    CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");
	Corporation c = (Corporation) request.getAttribute("corp");
	String repsperseconds = request.getParameter("repsperseconds") != null ? request.getParameter("repsperseconds") : "0";
	String sets = request.getParameter("sets") != null ? request.getParameter("sets") : "0";
	String rest = request.getParameter("rest") != null ? request.getParameter("rest") : "0";
	String repsvalue = request.getParameter("reps") != null ? request.getParameter("reps") : "0";	
	String holdtime = request.getParameter("holdtime") != null ? request.getParameter("holdtime") : "0";
	String side = Util.notNull(request.getParameter("side"));
	String weight = request.getParameter("weight") != null ? request.getParameter("weight") : "0";
	

	String name = null;
	if(session.getAttribute("pgmName") != null){
	name = session.getAttribute("pgmName").toString();
	}
	boolean showLocation = request.getParameter("showLocation") != null && Boolean.parseBoolean(request.getParameter("showLocation"));
	String[] multitype = null;
	multitype = request.getParameterValues("multitype");
    if (cu == null) {
        response.sendRedirect("/close.jsp?redir=" + URLEncoder.encode(Util.loginUrl(Constants.LoginReason.login, request), "UTF-8"));
        return;
    }

    User u = cu.getUser();
    Corporation corp = cu.getCorporation();
   /*  try{
    	Util.allActivitiesLogs("Patients", u.getId(), corp.getName(), "Edit Exercise");
    }catch(Exception e){
    	
    } */
    long id = Long.parseLong(Util.notNull(request.getParameter("id"), "0"));
    long add = Long.parseLong(Util.notNull(request.getParameter("add"), "0"));
    if (id == 0) {
        response.sendRedirect(corp.getHost() + "/close.jsp");
        return;
    }
    LibraryManager lm = new LibraryManager();
    ProgramExercise pe = lm.getProgramExercise(id);
    
    if (pe == null) {
        response.sendError(404, "Program not found");
        return;
    }
    if (pe == null) {
        response.sendRedirect(corp.getHost() + "/close.jsp");
        return;
    }
    String from=null;
    Program program = lm.getProgram(pe.getProgramId());
    Patient p2=lm.getPatient(program.getPatientId());
	if(id>0){
	from="ProgramName:"+program.getName()+";ExerciseName:"+pe.getName()+";Sets:"+pe.getSets()+";Reps:"+pe.getReps()+";Holds:"+pe.getHolds()+";Side:"+pe.getSide()+";Rest:"+pe.getRest()+";Description:"+pe.getDescription()+";";	
	
	System.out.println("from"+from);
	}
//     if (program.getCorpId() != corp.getId()) {
//         response.sendRedirect(corp.getHost() + "/close.jsp");
//         return;
//     }
    String redirectUrl = (corp.getHost() + "/close.jsp?redir=" + URLEncoder.encode("/corp/programs/edit.jsp?id=" + pe.getProgramId(), "UTF-8"));

String weightChoices[] = {"0","5", "10", "15", "20", "25", "30","35","40","45","50","55","60","65","70","75","80","85","90","95","100","105","110","115","120","125","130","135","140","145","150","155","160","165","175","180","185","190","195","200","205","210","215","220","225","230","235","240","245","250"};
  
    String description = Util.notNull(request.getParameter("description"));
    boolean isNew = Boolean.parseBoolean(Util.notNull(request.getParameter("new"), "false"));

    String instructions = "";
    List<ExerciseMedia> media = lm.getExerciseMedia(pe.getExerciseId());
    int i = 1;
    for (ExerciseMedia em : media){
        if (em.getCaption() != null && em.getCaption().trim().length() > 0){
            instructions += i + ". " + em.getCaption() + "<br/>";
        }
        i++;
    }
    
    if (request.getMethod().equalsIgnoreCase("post")) {
        if (request.getParameter("submit").startsWith("Cancel")) {
            response.sendRedirect(corp.getHost() + "/close.jsp");
            return;
        }
        		List<Long> Eideditlist = (ArrayList<Long>)session.getAttribute("Eidlist");
                for(Long Eid : Eideditlist){
                	ProgramExercise pe1 = lm.getProgramExerciseforProgram(Eid, pe.getName());
                		pe1.setSets(Integer.parseInt(sets));
                        pe1.setReps(Integer.parseInt(repsvalue));
                        if (pe.getBase().equals(Exercise.REP_EXERCISE)) {
                        pe1.setHolds(Integer.parseInt(repsperseconds));
                        }else{
                        pe1.setHolds(Integer.parseInt(holdtime));
                        }
                        pe1.setRest(Integer.parseInt(rest));
                        pe1.setSide(Util.safeInput(side, false));
                        pe1.setDescription(Util.safeInput(description, false));
                        pe1.setWeight(Integer.parseInt(weight));
                        
                        //update the programs last updated date
                        program.setLastUpdated(new Date());
                        program = lm.save(program);

                        lm.save(pe1);
                        System.out.println("Before adding exercise");
                        
        	} 	
                if(p2!=null)
                {
                String type="Patients";
                String activity_page="corp/programs/inline/edit_exercise.jsp";
            	String corp_id=String.valueOf(corp.getId());
            	String corp_name=corp.getName();
            	String corp_loc=corp.getSuburb();
            	String user_id=String.valueOf(u.getId());
            	String user_name=u.getFullName();
            	String invitor_id=String.valueOf(p2.getId());
            	String invitor_name=p2.getFullName();
            	String flag="update";
            	String to="ProgramName:"+program.getName()+";ExerciseName:"+pe.getName()+";Sets:"+sets+";Reps:"+repsvalue+";Holds:"+holdtime+";Side:"+side+";Rest:"+rest+";Description:"+description+";";
            	String desc="Edit Exercise By User";
            	
            	 try{
            	    	//Util.allActivitiesLogs("Patients", u.getId(), corp.getName(), "Edit Exercise");
            		 Util.allActivitiesLogs2("Patients",activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
            		 System.out.println(" adding exercise to list");
             	}catch(Exception ee){
             		ee.printStackTrace();
             		//System.err.append("Exception on editecercise",ee.printStackTrace());
             		System.out.println("Exception on edit_exercise"+ee.getMessage());
             	} 
                }
                System.out.println("After adding exercise");
               
            	System.out.println("Page redirect to edit.js");
        response.sendRedirect(corp.getHost() + "/close.jsp?redir=" + URLEncoder.encode("/corp/programs/edit.jsp?id=" + pe.getProgramId() + "&seq=" + pe.getSequence(), "UTF-8"));
        return;
                
    } else {
    	if(add > 0){
    		if (pe.getBase().equals(Exercise.REP_EXERCISE)) {
    			sets = "3";
    			repsvalue = "15";
    	        holdtime = "3";
    	        rest = "10";
    	        side = "Both";
    	        description = pe.getDescription();
    	    } else {
    	    	sets = "3";
    	    	repsvalue = "15";
    	        holdtime = "30";
    	        rest = "10";
    	        side = "Both";
    	        description = pe.getDescription();
    	    }
    	}else{
	        sets = Integer.toString(pe.getSets());
	        repsvalue = Integer.toString(pe.getReps());
	        holdtime = Integer.toString(pe.getHolds());
	        rest = Integer.toString(pe.getRest());
	        side = pe.getSide();
	        weight=Integer.toString(pe.getWeight());
	        description = pe.getDescription();
    	}
    }
%>
<html>
<head>
<title>Edit Exercise</title>
<link href='https://fonts.googleapis.com/css?family=Quantico'
	rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="/css/editexercise_rSlider.min.css" type='text/css'>
<style>
body {
	font-family: Arial, Helvetica, sans-serif;
	margin: 0;
	padding: 0 0 50px;
	color: #333;
	font-size: 14px;
}

p {
	margin: 0;
}

.container {
	width: 80%;
	margin: 70px auto;
}

.slider-container {
	width: 100%;
	max-width: 800px;
	margin: 0 auto 50px;
}
</style>
<style type="text/css">
 .add-exe-pop{
    width: 50%;
    height: 540px;
        position: fixed;
        top: 60px;
        left: 0;
        right: 0;
        opacity: 1;
        overflow: visible;
        margin: 0 auto;
        z-index: 8020;
    }
    .add-exe-pop-skin {
        position: relative;
        background: #f9f9f9;
        color: #444;
        text-shadow: none;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
        padding: 15px;
        width: auto;
        height: auto;
    }
    .add-exe-pop-outer{
        position:relative;
        padding: 0;
        margin: 0;
        border: 0;
        outline: none;
        vertical-align: top;
    }
    .add-exe-pop-inner{
        height: 630px;
        width: 100%;
        overflow: auto;
    }
   .add-exe-pop-close {
	    position: absolute;
	   top: -26px;
    right: -31px;
	    width: 36px;
	    height: 36px;
	    cursor: pointer;
	    z-index: 8040;
	    background-image: url(fancybox_sprite.png);
	}
    
    .page-headers{padding-bottom:17px;margin:18px 0;}
</style>
<script type="text/javascript">
		$(function(){
				window.parent.$("html, body").animate({scrollTop:0}, 'fast');				
		});
</script>
</head>
<body>
<div class="add-exe-pop" style="">
    <div class="add-exe-pop-skin" style="">
        <div class="add-exe-pop-outer">
            <div class="add-exe-pop-inner" style="">

	<div class="page-header">
		<h1>
			Edit Exercise <div><small><%=pe.getName()%></small></div>
		</h1>
	</div>
	<div class="row" style="margin-left: 0px;">
		<div class="span6">
			<form action="edit_exercise.jsp" method="post"
				class="form-horizontal">
				<input type="hidden" name="id" value="<%=id%>"> <input
					type="hidden" name="new" value="<%=isNew%>">
				<% if (pe.getBase().equals(Exercise.REP_EXERCISE)) {%>
				<div class="control-group" id="repsDiv" style="margin-bottom: 14px;">
					<label class="control-label" for="type" style="width: 80px;">Reps</label>

					<div class="controls" style="">
						<input type="range" onchange="rangevalue.value=value" id="reps"
							name="reps" value="<%=repsvalue%>" />
					</div>
				</div>
				<div class="control-group" id="repsPerSecDiv" style="margin-bottom: 14px;">
					<label class="control-label" for="type" style="width: 150px;">Reps per
						seconds</label>

					<div class="controls" style="">
						<input type="range" onchange="rangevalue.value=value"
							id="repsperseconds" name="repsperseconds" 
							value="<%=holdtime%>" />
					</div>
				</div>
				<% } else { %>
                     <div class="control-group" id="holdTimeDiv" style="margin-bottom: 14px;">
						<label class="control-label" for="type" style="width: 80px;">Hold Time</label>

						<div class="controls" style="">
								<input
									type="range" 
									onchange="rangevalue.value=value" id="holdtime"
									name="holdtime"   min="5" max="200" value="<%=holdtime%>"/>
						</div>
					</div>
				<% } %>
				<% if (pe.isShowSides()) {
            %>
				<div class="control-group"style="margin-bottom: 14px;">
					<label class="control-label" for="type" style="width: 80px;">Side</label>
					<div class="controls" style="margin-left: 158px;">
						<table>
							<tr>
								<td><label for="radio1">Left&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><input
									class="side" type="radio" id="radio1" name="side" value="Left" /></td>
								<td><label for="radio2">Right&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><input
									class="side" type="radio" id="radio2" name="side" value="Right" /></td>
								<td><label for="radio3">Both&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><input
									class="side" type="radio" id="radio3" name="side" value="Both" /></td>
								<td><label for="radio3">Alternating&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><input
									class="side" type="radio" id="radio4" name="side"
									value="Alternating" /></td>
							</tr>
						</table>
					</div>
				</div>
				<%
            }
            %>

				<div class="control-group"style="margin-bottom: 14px;" >
					<label class="control-label" for="type" style="width: 80px;">Sets</label>
					<div class="controls" style="">
						<input type="range" onchange="rangevalue.value=value" id="sets"
							name="sets" value="<%=sets%>" />
					</div>
				</div>
				<div class="control-group"style="margin-bottom: 14px;">
					<label class="control-label" for="type" style="width: 80px;">Rest</label>

					<div class="controls" style="">
						<input type="range" onchange="rangevalue.value=value" id="rest"
							name="rest" value="<%=rest%>" onload="" />
					</div>
				</div>	
				
				<%=HTML.selectMinis("weight", "Weight", weightChoices, weightChoices, weight, null, null,false)%>
				<%--=HTML.textArea("description", "Description", description,5)--%>
				<strong>Instructions:</strong>
				<p>
					<%=instructions%>
				</p>
				<%=HTML.saveCancel()%>
				</div>
    </div>
    <div  id="close" title="Close" class="fancybox-item add-exe-pop-close " onclick="redirectfunction()" >
    
        </div>
    </div>
</div>
</div></div>
<div id="fancybox-overlay" class="overlay-fixed" style="cursor: pointer; opacity: 0.8; display: block;">
</div>
			</form>
			<script type="text/javascript" src="/js/editexercise_rSlider.min.js"></script>
			<script type="text/javascript">
			function redirectfunction(){
				
				window.location.href='<%=redirectUrl%>'
			}
		$(document)
				.ready(
						function showorhide() {
							if ($("#HideLoc").val() == "true") {
								$("#HideLocation").hide();
							}
							var checked = $("input[name='showLocation']").attr(
									"checked") ? "True" : "False";
							var foo = [];
							$('#multitypedub :selected').each(
									function(i, selected) {
										foo[i] = $(selected).text();
									});
							$.each(foo, function(index, value) {
								var row = $('#multitype');
								row.find("option[value=" + value + ']').attr(
										'selected', 'selected');
							});
							if (checked == "True") {
								$("#multitype").show();
								$("#tabsedit").hide();
								$("#mediaedit").hide();
								if ($("#multitype").val() != null) {
									$("#errorhideorshow").hide();
								} else if ($("#name").val() != '') {
									$("#errorhideorshow").show();
									$("#errorLocation").val(
											"Please select a location");
	<%request.setAttribute("MutiError", "Please select a location");%>
		} else if ($("#name").val() == '') {
									$("#errorhideorshow").hide();
								}
							} else {
								$("#multitype").hide();
								$("#errorhideorshow").hide();
								$("#tabsedit").hide();
								$("#mediaedit").hide();
							}

						});
		function showorhide() {
			var foo2 = [];
			$('#multitypedub :selected').each(function(i, selected) {
				foo2[i] = $(selected).text();
			});
			$.each(foo2, function(index, value) {
				var row = $('#multitype');
				row.find("option[value=" + value + ']').attr('selected',
						'selected');
			});
			var checked = $("input[name='showLocation']").attr("checked") ? "True"
					: "False";
			if (checked == "True") {
				// 	$("#edittabs").hide();
				// 	$("#editmedia").hide();
				// 	$("#tabsedit").show();
				// 	$("#mediaedit").show();
				$("#multitype").show();
			} else {
				// 	$("#edittabs").hide();
				// 	$("#editmedia").hide();
				// 	$("#tabsedit").show();
				// 	$("#mediaedit").show();
				$("#multitype").hide();
				$("#errorhideorshow").hide();
			}
		}

		function showorhidetabs() {
			var foo2 = [];
			$('#multitypedub :selected').each(function(i, selected) {
				foo2[i] = $(selected).text();
			});
			$.each(foo2, function(index, value) {
				var row = $('#multitype');
				row.find("option[value=" + value + ']').attr('selected',
						'selected');
			});
			var foo = [];
			var foo1 = [];
			$('#multitypename :selected').each(function(i, selected) {
				foo[i] = $(selected).text();
			});
			$('#multitype :selected').each(function(i, selected) {
				foo1[i] = $(selected).text();
			});
			var a = foo.length;
			var b = foo1.length;
			if (a == b) {
				$("#edittabs").hide();
				$("#editmedia").hide();
				$("#tabsedit").show();
				$("#mediaedit").show();
			} else if (a <= b) {
				$("#edittabs").hide();
				$("#editmedia").hide();
				$("#tabsedit").show();
				$("#mediaedit").show();
			} else if (a >= b) {
				$("#edittabs").show();
				$("#editmedia").show();
				$("#tabsedit").hide();
				$("#mediaedit").hide();
			}

		}
		
		
		(function () {
            'use strict';

            var init = function () {    
            	
                var sets = new rSlider({
                    target: '#sets',
                    values: [1, 2, 3, 4, 5, 6],
                    range: false,
                    set: [<%=sets%>],
                    tooltip: false,
                    onChange: function (vals) {
                        console.log(vals);
                    }
                
                });
                
                
                var rest = new rSlider({
                    target: '#rest',
                    values: [0, 10, 20, 30, 60],
                    range: false,
                    set: [<%=rest%>],
                    tooltip: false,
                    onChange: function (vals) {
                        console.log(vals);
                    }
                
                });
                
                var reps = new rSlider({
                    target: '#reps',
                    values: [1,2,3,4,5,6,7,8,9,10,11,12,15,20,25,30, 40],
                    range: false,
                    set: [<%=repsvalue%>],
                    tooltip: false,
                    onChange: function (vals) {
                        console.log(vals);
                    }
                
                });
                
                var repsperseconds = new rSlider({
                    target: '#repsperseconds',
                    values: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
                    range: false,
                    set: [<%=holdtime%>],
                    tooltip: false,
                    onChange: function (vals) {
                        console.log(vals);
                    }
                
                });
                
                var holdtime = new rSlider({
                    target: '#holdtime',
                    values: [5, 10, 15, 20, 30, 40, 60, 90, 120, 180],
                    range: false,
                    set: [<%=holdtime%>],
                    tooltip: false,
                    onChange: function (vals) {
                        console.log(vals);
                    }
                
                });
            };
            window.onload = init;
        })();
		
		function setSide(){
			var side = '<%=side%>';
			//alert("side"+side);
			if (<%=pe.isShowSides()%>==true) {
            
			if(side == null || side == ""||side=="null"){
				//alert("side");
				document.getElementById("radio1").checked = true;
			
			}else{
				setCheckedValueOfRadioButtonGroup(side);
			}
			}
		}
		
		function setCheckedValueOfRadioButtonGroup(vValue) {
		    var radios = document.getElementsByName("side");
		    for (var j = 0; j < radios.length; j++) {
		        if (radios[j].value == vValue) {
		            radios[j].checked = true;
		            break;
		        }
		    }
		}
		
		$(document).ready(function () {
			setSide();
			
		});

		
		
		
	</script>
		</div>
	</div>
</body>
</html>