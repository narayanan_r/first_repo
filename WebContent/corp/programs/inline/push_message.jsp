<%@page import="au.com.ourbodycorp.model.managers.LibraryManager"%>
<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.MultiPartHelper" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%@ page import="au.com.ourbodycorp.model.managers.AttachmentManager" %>
<%@ page import="au.com.ourbodycorp.model.managers.ProgramMessageManager" %>
<%@ page import="org.apache.commons.fileupload.FileItem" %>
<%@ page import="org.apache.commons.fileupload.servlet.ServletFileUpload" %>
<%@ page import="java.util.Date" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");

    if (cu == null) {
        response.sendRedirect(Util.loginUrl(Constants.LoginReason.login, request));
        return;
    }
    User u = cu.getUser();
    Corporation corp = cu.getCorporation();
   /*  try{
    	Util.allActivitiesLogs("Patients", u.getId(), corp.getName(), "Send Push Message");
    }catch(Exception e){
    	
    } */
    ProgramMessageManager messageManager = new ProgramMessageManager();
    LibraryManager libraryManager = new LibraryManager();
    
    Program program = null;
    
    try {
        Long programId = Long.parseLong(request.getParameter("programId"));
        program = libraryManager.getProgram(programId);
        
    } 
    catch (Exception e) {}

    String headline = Util.notNull(request.getParameter("headline"));
    String body = Util.notNull(request.getParameter("body"));
    String link = Util.notNull(request.getParameter("link"));

    MultiPartHelper mp = null;
    if (ServletFileUpload.isMultipartContent(request)) {
        mp = new MultiPartHelper(request);
        Long programId = Long.parseLong(mp.getParameter("programId"));
        program = libraryManager.getProgram(programId);

        headline = Util.notNull(mp.getParameter("headline"));
        body = Util.notNull(mp.getParameter("body"));
        link = Util.notNull(mp.getParameter("link"));
    }

    AttachmentManager am = new AttachmentManager();
    Attachment a = null;
    String error = null;

    if (request.getMethod().equalsIgnoreCase("post")) {
        if (headline.trim().length() == 0) {
            error = "Please enter a headline";
        } 
        else if (body.trim().length() == 0) {
            error = "Please enter some body text";
        }

        try {
            headline = Util.safeInput(headline, true);
            body = Util.safeInput(body, true);
            link = Util.safeInput(link, true);
        } 
        catch (Exception e) {
            error = "HTML input is not allowed. Remove and &gt; and &lt; characters";
        }

        FileItem photo = mp.getFileItem("photo");
        if (photo != null && photo.getSize() > 0) {
            if (!photo.getName().toLowerCase().endsWith(".jpg")) {
                error = "Photos must be uploaded as .jpg file";
            }
        }

        if (error == null) {
            ProgramMessage message = new ProgramMessage();
            message.setProgramId(program.getId());
            message.setCreatedDate(new Date());

            if (photo != null && photo.getSize() > 0) {
                if (message.getPhotoId() > 0) {
                    a = am.getAttachment(message.getPhotoId(), false);
                } 
                else {
                    a = new Attachment();
                    a.setUploaded(new Date());
                }
                a.setVersion(a.getVersion() + 1);
                a.setType(Attachment.AttachmentType.image);
                a.setFilename(photo.getName());
                a.setUpdated(new Date());
                a.setMimeType(photo.getContentType());
                a.setActive(true);
                a.setSize(photo.getSize());
                a.setData(photo.get());
                am.saveAttachment(a);
                message.setPhotoId(a.getId());
            }

            message.setHeadline(headline);
            message.setBody(body);
            message.setLink(link);

            message = messageManager.save(message);
            
            try{
            	Patient patient = libraryManager.getPatient(program.getPatientId());
                messageManager.send(message);
                String type="Patients";
                String activity_page="corp/programs/inline/push_message.jsp";
            	String corp_id=String.valueOf(corp.getId());
            	String corp_name=corp.getName();
            	String corp_loc=corp.getSuburb();
            	String user_id=String.valueOf(u.getId());
            	String user_name=u.getFullName();
            	String invitor_id=String.valueOf(patient.getId());
            	String invitor_name=patient.getFullName();
            	String flag="new";
            	String from="Send push message to Patient:"+patient.getFullName();
            	String to="Email:"+patient.getEmail()+";Phone:"+patient.getPhone()+";MessageName:"+message.getHeadline()+";MessageBody:"+message.getBody();
            	String desc="Send push message Tittle:"+message.getHeadline()+" And Body:"+message.getBody()+" to Patient:"+patient.getFullName()+" By User";
                try
                {		
                 Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
                }
                catch(Exception e)
                {
                }
                
                response.sendRedirect(corp.getHost() + "/close.jsp");
                
                return;
            }
            catch (Exception e){
                error = "Failed sending push message, please try again later";
            }
        }

    } 
    else {
        //TODO?
    }

%>
<html>
<head><title>Push Message</title></head>
<body>
<div class="page-header">
    <h1>Push Message</h1>
</div>
<%@include file="/WEB-INF/includes/formErrorSimple.jsp"%>
<div class="row">
    <div class="span9">
        <form action="push_message.jsp" method="post" class="form-horizontal" enctype="multipart/form-data">
            <input type="hidden" name="programId" value="<%=program.getId()%>">
            <%=HTML.textInput("headline", "Title", "50", "100", headline)%>
            <%=HTML.textArea("body", "Message", body, "input-xlarge", 5)%>
            <%=HTML.textInput("link", "External link", "50", "100", link, "", null)%>
            <%=HTML.fileInput("photo", "Optional Photo", null)%>
            <%
                if (a != null) {
            %>
            <div class="control-group">
            <label class="control-label" for="textarea">&nbsp;</label>
            <div class="controls">
              <img src="<%=a.getImageUrl(Attachment.ImageSize.f320)%>">
            </div>
          </div>
            <%
                }
            %>
            <div class="form-actions"> 
                <button name="submit" type="submit" value="Send" class="btn btn-primary">Send Push Message</button>
            </div>
        </form>
    </div>
</div>
</body>
</html>