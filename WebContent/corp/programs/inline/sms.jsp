<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@ page import="au.com.ourbodycorp.Config" %>
<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="au.com.ourbodycorp.model.managers.CountryManager" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.TextLocalClient" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");

    if (cu == null) {
        response.sendRedirect("/close.jsp");
        return;
    }

    User u = cu.getUser();
    Corporation corp = cu.getCorporation();
    
    long id = Long.parseLong(request.getParameter("id"));
    String phone = request.getParameter("phone");

    LibraryManager lm = new LibraryManager();
    Program program = lm.getProgram(id);
    Patient patient = lm.getPatient(program.getPatientId());
    PatientGroup patientGroup = lm.getPatientGroup(program.getGroupId());
/* 	if(patient != null){
		try{
	    	Util.allActivitiesLogs("Patients", u.getId(), corp.getName(), "Send SMS");
	    }catch(Exception e){
	    	
	    }
	}else{
		try{
	    	Util.allActivitiesLogs("Patient Groups", u.getId(), corp.getName(), "Send Group SMS");
	    }catch(Exception e){
	    	
	    }
	} */
    String error = null;

    CountryManager cm = new CountryManager();
    Country c = cm.getCountry(corp.getCountry());
	String applicationName = u.getApplicationId()==1?"M3ClinicApp": "FiizioApp";
    if (request.getMethod().equalsIgnoreCase("post")) {
        if (patient != null){
            String validPhoneNumber = Util.convertToValidPhoneNumber(phone, c.getDial());
            if (validPhoneNumber == null) {
                error = "Please enter a phone number";
            }
            else if (corp.getSmsCount() <= 0){
                error = "You have no SMS message credits left, you can purchase more by <a href='http://www.fiizio.com/index.php/pricing-shop-sms' target='_blank'>clicking here</a>";
            }
            else {
                TextLocalClient mc = new TextLocalClient();
                String msg = String.format("Install your new "+applicationName+" program by clicking here: %s/p/%s", Config.getString("url"), program.getCode());
                System.out.println(applicationName);
                String res = mc.sendSimple(validPhoneNumber, applicationName, msg);
                System.out.println(res);
                if (res.trim().startsWith("ERROR")) {
                    error = "There was a problem sending your SMS, please check the number and try again. If the problem persists, contact support.";
                    System.err.println("Error sending SMS: '" + res.trim() + "'");
                } 
                else {
                    lm.logSMS(corp.getId(), validPhoneNumber, msg);
                    String type="Patients";
                    String activity_page="corp/programs/inline/sms.jsp";
                	String corp_id=String.valueOf(corp.getId());
                	String corp_name=corp.getName();
                	String corp_loc=corp.getSuburb();
                	String user_id=String.valueOf(u.getId());
                	String user_name=u.getFullName();
                	String invitor_id=String.valueOf(patient.getId());
                	String invitor_name=patient.getFullName();
                	String flag="new";
                	String from="Send Program to Patient through SMS";
                	String to="ProgramName:"+program.getName()+";PatientName:"+patient.getFullName()+";PatientEmail:"+patient.getEmail()+";PatientPhone:"+patient.getPhone();
                	String desc="Send sms for "+patient.getFullName()+" By User";
                    try
                    {		
                     Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
                    }
                    catch(Exception e)
                    {
                    }
                    response.sendRedirect(corp.getHost() + "/close.jsp");

                    return;
                }
            }
        }
        else if (patientGroup != null){
            StringBuffer phoneNumbers = new StringBuffer();
            List<String> numbersToLog = new ArrayList<String>();
            int phoneNumberCount = Integer.parseInt(Util.notNull(request.getParameter("phoneNumberCount"), "0"));
            for (int i = 1; i <= phoneNumberCount; i++){
                String validPhoneNumber = Util.convertToValidPhoneNumber(request.getParameter("phone" + i), c.getDial());
                String shouldSendSMS = request.getParameter("sendSMS" + i);
                if (shouldSendSMS != null && shouldSendSMS.equalsIgnoreCase("on") && validPhoneNumber != null){
                    phoneNumbers.append(validPhoneNumber);
                    numbersToLog.add(validPhoneNumber);
                    if (i != phoneNumberCount) phoneNumbers.append(",");
                }
            }
            if (phoneNumbers.length() == 0){
                error = "Please enter at least one phone number, and make sure you check the box next to it";
            }
            else if (corp.getSmsCount() - numbersToLog.size() < 0){
                error = "You have " + corp.getSmsCount() + " SMS message credits left, which is not enough to send SMS to " + numbersToLog.size() + " people. <a href='http://www.fiizio.com/index.php/pricing-shop-sms' target='_blank'>Buy more here</a>.";
            }
            
            if (error == null){
                TextLocalClient mc = new TextLocalClient();
                String msg = String.format("Install your new "+applicationName+" program by clicking here: %s/p/%s", Config.getString("url"), program.getCode());
                System.out.println(applicationName);
                String res = mc.sendSimple(phoneNumbers.toString(), applicationName, msg);
                System.out.println(res);
                if (res.trim().startsWith("ERROR")) {
                    error = "There was a problem sending your SMS, please check the number and try again. If the problem persists, contact support.";
                    System.err.println("Error sending SMS: '" + res.trim() + "'");
                } 
                else {
                    for (String number : numbersToLog){
                        lm.logSMS(corp.getId(), number, msg);
                    }
                    String type="Patient Groups";
                    String activity_page="corp/programs/inline/sms.jsp";
                	String corp_id=String.valueOf(corp.getId());
                	String corp_name=corp.getName();
                	String corp_loc=corp.getSuburb();
                	String user_id=String.valueOf(u.getId());
                	String user_name=u.getFullName();
                	String invitor_id=String.valueOf(patientGroup.getId());
                	String invitor_name=patientGroup.getGroupName();
                	String flag="sms";
                	String from="---";
                	String to="---";
                	String desc="Send group sms for "+patientGroup.getGroupName()+" By User";
                    try
                    {		
                     Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
                    }
                    catch(Exception e)
                    {
                    }

                    response.sendRedirect(corp.getHost() + "/close.jsp");

                    return;
                }
                

                return;
            }
        }
    } 
    else if (patient != null){
        phone = patient.getPhone();
    }



%>
<html>
<head><title>Send SMS</title>
<script>
    $(document).ready(function(){
    $('.check:button').toggle(function(){
        $('input:checkbox').attr('checked','checked');
        $(this).val('Un-check All');
    },function(){
        $('input:checkbox').removeAttr('checked');
        $(this).val('Check All');        
    });
});
</script>
</head>
<body>
<div class="page-header">
    <h1>Send SMS</h1>
</div>
<%@include file="/WEB-INF/includes/formErrorSimple.jsp"%>
<div class="row">
    <div class="span9">
        <form action="sms.jsp" method="post" class="form-horizontal">
            <input type="hidden" name="id" value="<%=id%>">
            <% if (patient != null) { %>
                <p>
                    Enter phone number in <%=corp.getCountryName()%> format (No country code) or international (e.g.: +<%=Util.notNull(c.getDial(), "61")%> 401 234 567)
                </p>
                <p>
                    Note: You have <strong><%=corp.getSmsCount()%> remaining</strong> SMS credits. <a href='/corp/subscription/pricing_sms.jsp' target='_blank'>Buy more</a>.
                </p>
                <br/>
                <%=HTML.textInput("phone", "Phone Number", "20", "20", phone)%>
                <%=HTML.submitButton("Send SMS")%>
            <% } 
               else if (patientGroup != null){ 
                   List<Patient> patientsInGroup = lm.getPatientsInGroup(patientGroup.getId());
                   int patientCount = 0;
                   %>
                   <p>
                    Enter phone numbers in <%=corp.getCountryName()%> format (No country code) or international (e.g.: +<%=Util.notNull(c.getDial(), "61")%> 401 234 567). Invalid phone numbers will be ignored.
                   </p>
                   <p>
                        Note: You have <strong><%=corp.getSmsCount()%> remaining</strong> SMS credits.
                   </p>
                <br/>
                   <table width="">
                   <%
                   for (Patient p : patientsInGroup) { 
                       patientCount++;                   %>
                       <tr>
                           <td><input type="checkbox" id="sendSMS<%=patientCount%>" name="sendSMS<%=patientCount%>" class="cb-sms" />&nbsp;</td>
                           <td><%=p.getFullName()%>&nbsp;</td>
                           <td><input id="phone<%=patientCount%>" name="phone<%=patientCount%>"class="input-xlarge" type="text" size="20" maxlen="20" name="%s" value="<%=p.getPhone()%>"/></td>
                       </tr>
            <%     } %>
                   </table>
                   <input type="button" class="check btn" value="Check All" />
                   <%=HTML.submitButton("Send Group SMS")%>
                   <input type="hidden" name="phoneNumberCount" value="<%=patientCount%>">
             <% } %>
        </form>
    </div>
</div>
</body>
</html>