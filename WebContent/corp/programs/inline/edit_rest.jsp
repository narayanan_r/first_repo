<%@ page import="java.util.Date"%>
<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="java.net.URLEncoder" %>
<%@ page import="java.util.List" %>
<%@	page import="java.util.ArrayList"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");

    if (cu == null) {
        response.sendRedirect("/close.jsp?redir=" + URLEncoder.encode(Util.loginUrl(Constants.LoginReason.login, request), "UTF-8"));
        return;
    }

    User u = cu.getUser();
    Corporation corp = cu.getCorporation();
    /* try{
    	Util.allActivitiesLogs("Patients", u.getId(), corp.getName(), "Edit Rest");
    }catch(Exception e){
    	
    } */    long id = Long.parseLong(Util.notNull(request.getParameter("id"), "0"));

String rest = request.getParameter("rest") != null ? request.getParameter("rest") : "0";
    if (id == 0) {
        response.sendRedirect(corp.getHost() + "/close.jsp");
        return;
    }
    LibraryManager lm = new LibraryManager();
    ProgramExercise pe = lm.getProgramExercise(id);
	
    if (pe == null) {
        response.sendError(404, "Program not found");
        return;
    }
    if (pe == null) {
        response.sendRedirect(corp.getHost() + "/close.jsp");
        return;
    }
    Program program = lm.getProgram(pe.getProgramId());
    Patient p=lm.getPatient(program.getPatientId());
    String from="ProgramName:"+program.getName()+";Rest:"+pe.getRest()+";";
//     if (program.getCorpId() != corp.getId()) {
//         response.sendRedirect(corp.getHost() + "/close.jsp");
//         return;
//     }

   // String restChoices[] = {"10", "20", "30", "45", "60", "90", "120", "180"};
   // String rest = Util.notNull(request.getParameter("rest"),restChoices[0]);
    boolean isNew = Boolean.parseBoolean(Util.notNull(request.getParameter("new"), "false"));

    if (request.getMethod().equalsIgnoreCase("post")) {
        if (request.getParameter("submit").startsWith("Cancel")) {
            response.sendRedirect(corp.getHost() + "/close.jsp");
            return;
        }
        List<Long> Eideditlist = (ArrayList<Long>)session.getAttribute("Eidlist");
        for(Long Eid : Eideditlist){
        ProgramExercise pe1 = lm.getProgramExerciseforProgram(Eid, pe.getName());
        pe1.setName("Rest " + rest + " Seconds");
        pe1.setRest(Integer.parseInt(rest));

        lm.save(pe1);
        
        //update the programs last updated date
        program.setLastUpdated(new Date());
        program = lm.save(program);
        
        if(p!=null)
        {
        String type="Patients";
        String activity_page="corp/programs/inline/edit_rest.jsp";
    	String corp_id=String.valueOf(corp.getId());
    	String corp_name=corp.getName();
    	String corp_loc=corp.getSuburb();
    	String user_id=String.valueOf(u.getId());
    	String user_name=u.getFullName();
    	String invitor_id=String.valueOf(p.getId());
    	String invitor_name=p.getFullName();
    	String flag="update";
    	String to="ProgramName:"+program.getName()+";Rest:"+pe1.getRest()+";";
    	String desc="Edit rest to "+p.getFullName()+" By user";
        try
        {		
         Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
        }
        catch(Exception e)
        {
        }
        }
        
        }
        response.sendRedirect(corp.getHost() + "/close.jsp?redir=" + URLEncoder.encode("/corp/programs/edit.jsp?id=" + pe.getProgramId() + "&seq=" + pe.getSequence(), "UTF-8"));
        return;
    } else {
        rest = Integer.toString(pe.getRest());
    }

%>
<html>
<head><title>Edit Rest</title>
<link rel="stylesheet" href="/css/editexercise_rSlider.min.css" type='text/css'>
<style>
body {
	font-family: Arial, Helvetica, sans-serif;
	margin: 0;
	padding: 0 0 50px;
	color: #333;
	font-size: 14px;
}

p {
	margin: 0;
}

.container {
	width: 80%;
	margin: 70px auto;
}

.slider-container {
	width: 100%;
	max-width: 800px;
	margin: 0 auto 50px;
}
</style>
<script type="text/javascript" src="/js/editexercise_rSlider.min.js"></script>

</head>
<body>
<div class="page-header">
    <h1>Edit Rest <small><%=pe.getName()%></small></h1>
</div>
<div class="row">
    <div class="span6">
        <form action="edit_rest.jsp" method="post" class="form-horizontal">
		<input type="hidden" name="id" value="<%=id%>">
         <div class="control-group">
					<label class="control-label" for="type" style="width: 10px;">Rest</label>

					<div class="controls" style="margin-left: 61px;">
						<input type="range" onchange="rangevalue.value=value" id="rest"
							name="rest" value="<%=rest%>" onload="" max="200"  />
					</div>
				</div>
				<%=HTML.saveCancel()%>
        </form>
        <script type="text/javascript">

var rest = new rSlider({
    target: '#rest',
    values: [10, 20, 30, 45, 60, 90, 120, 180],
    range: false,
    set: [<%=rest%>],
    tooltip: false,
    onChange: function (vals) {
        console.log(vals);
    }

});</script>
    </div>
</div>
</body>
</html>