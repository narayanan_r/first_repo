<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@ page import="au.com.ourbodycorp.Config" %>
<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.MailMessage" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="java.util.Properties" %>
<%@ page import="java.io.BufferedReader" %>
<%@ page import="java.io.IOException" %>
<%@ page import="java.io.InputStreamReader" %>
<%@ page import="java.net.URL" %>
<%@ page import="java.net.URLConnection" %>
<%@ page import="org.json.JSONObject" %>
<%@ page import="java.net.URLEncoder" %>
<%@ page import="java.io.OutputStreamWriter" %>
<%@ page import="sun.net.www.protocol.http.HttpURLConnection" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");

    if (cu == null) {
        response.sendRedirect("/close.jsp");
        return;
    }

    User u = cu.getUser();
    Corporation corp = cu.getCorporation();

    long id = Long.parseLong(request.getParameter("id"));
    String email = request.getParameter("email");

    LibraryManager lm = new LibraryManager();
    Program program = lm.getProgram(id);
    Patient patient = lm.getPatient(program.getPatientId());
    PatientGroup patientGroup = lm.getPatientGroup(program.getGroupId());
    /* if(patient != null){
		try{
	    	Util.allActivitiesLogs("Patients", u.getId(), corp.getName(), "Send Email");
	    }catch(Exception e){
	    	
	    }
	}else{
		try{
	    	Util.allActivitiesLogs("Patient Groups", u.getId(), corp.getName(), "Send Group Email");
	    }catch(Exception e){
	    	
	    }
	} */
	String applicationName = u.getApplicationId()==1?"M3Clinic": "Fiizio";
    String emailTemplate = u.getApplicationId()==1?"program_m3.vm": "program.vm";
    String error = null;

    if (request.getMethod().equalsIgnoreCase("post")) {
        //email to a single patient
        if (patient != null){
            if (!Util.isValidEmail(email)) {
                error = "Please enter a valid email address";
            } else if(!email.matches("[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}")){
    	        	error = "Please enter a valid email address";
            }        
            else {
            	
                Properties props = new Properties();
				props.put("firstName", u.getFirstName());
                props.put("lastName", u.getLastName());
                String add="/corp/patients/program.jsp?id="+program.getId();
                props.put("url", Config.getString("url")+add);
              //props.put("url", String.format("%s/p/%s", Config.getString("url"), program.getCode()));
                MailMessage mm = null;
                if(applicationName.equalsIgnoreCase("M3Clinic")){
                	mm = new MailMessage(props, emailTemplate, email, "Your "+applicationName+" Program","M3ClinicApp <info@m3clinicapp.com.au>",true);
                }else{
                	mm = new MailMessage(props, emailTemplate, email, "Your "+applicationName+" App Program",true);
                }
//                 mm.send();
//                 response.sendRedirect(corp.getHost() + "/close.jsp");
//                 return;
              try
      	      {/* 
            	  int intIndex = -1;
            	  if(email.split("@")[1].toLowerCase().startsWith("yahoo")){
            		  URL url = new URL("http://my-addr.com/email/?mail="+email+"");
         	         URLConnection urlConnection = url.openConnection();
         	         HttpURLConnection connection = null;
         	         if(urlConnection instanceof HttpURLConnection)
         	         {
         	            connection = (HttpURLConnection) urlConnection;
         	         }
         	         else
         	         {
         	            System.out.println("Please enter an HTTP URL.");
         	            return;
         	         }

            	    BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
         	         String urlString = "";
         	         String current;
         	         while((current = in.readLine()) != null)
         	         {
         	            urlString += current;
         	         }
//          	         System.out.println(urlString);
         	         intIndex = urlString.indexOf("<b>e-mail exist</b>");
            	  }else{
            	 
            		  String data = URLEncoder.encode("email", "UTF-8") + "=" + URLEncoder.encode(""+email+"", "UTF-8");

                	    URL url = new URL("http://www.emailvalidator.co/EmailValidation/ProcessSingle");
                	    URLConnection conn = url.openConnection();
                	    conn.setDoOutput(true);
                	    OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                	    wr.write(data);
                	    wr.flush();

                	    BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                	    String line = rd.readLine();
                	    
          	  	   intIndex = line.indexOf("is a valid Email address");
            	  }
//      	         System.out.println(urlString);
      	       if(intIndex != - 1){ */
      	          mm.send();
      	     String type="Patients";
             String activity_page="corp/programs/inline/email.jsp";
         	String corp_id=String.valueOf(corp.getId());
         	String corp_name=corp.getName();
         	String corp_loc=corp.getSuburb();
         	String user_id=String.valueOf(u.getId());
         	String user_name=u.getFullName();
         	String invitor_id=String.valueOf(patient.getId());
         	String invitor_name=patient.getFullName();
         	String flag="new";
         	String from="Send Program to Patient :"+patient.getFullName()+" throug Email";
         	String to="ProgramName:"+program.getName()+";PatientName:"+patient.getFullName()+";PatientEmail:"+patient.getEmail()+";PatientPhone:"+patient.getPhone();
         	String desc="Send Email to "+patient.getFullName()+" By User";
             try
             {		
              Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
             }
             catch(Exception e)
             {
             }
                  response.sendRedirect(corp.getHost() + "/close.jsp");
                  return;
      	       /* }else{ 
      	    	 Properties props1 = new Properties();
 	            props1.put("user", u.getFirstName()+" "+u.getLastName());
 	           if(applicationName.equalsIgnoreCase("M3Clinic")){
 	            	props1.put("program", "M3Clinic Program");
 	           }else{
 	        	     props1.put("program", "Fiizio App Program");
 	           }
 	          props1.put("email", email);
 	          props1.put("patientname", patient.getFirstName()+" "+patient.getLastName());
			  props1.put("phone", patient.getPhone());
			  props1.put("company", corp.getName());
 	          if(Config.getString("https").equals("true")){
  	             props1.put("url", "https://"+corp.getSlug()+"."+Config.getString("baseDomain")+"/corp/patients/edit.jsp?id="+patient.getId()+"");
  	           }else{
  	            props1.put("url", "http://"+corp.getSlug()+"."+Config.getString("baseDomain")+"/corp/patients/edit.jsp?id="+patient.getId()+"");
  	           
  	           
  	           }
 	           if(applicationName.equalsIgnoreCase("M3Clinic")){
 	        	   MailMessage msg = new MailMessage(props1, "email_send_failed.vm", u.getEmail(), "M3Clinic Program Send Failed");
 	        	  try {
 	 	                msg.send();
 	 	            }
 	 	            catch (Exception e) {
// 	  	                e.printStackTrace();
// 	  	                throw e;
 	 	            }
	           }else{
	        	   MailMessage msg = new MailMessage(props1, "email_send_failed.vm", u.getEmail(), "Fiizio App Program Send Failed");
	        	   try {
	 	                msg.send();
	 	            }
	 	            catch (Exception e) {
//	  	                e.printStackTrace();
//	  	                throw e;
	 	            }
	           }

      	    	 error = "<p>Delivery to the following recipient failed permanently: "+email+".</p> Please update the email address for this patient and click send email.";
      	       } */
//       	     wr.close();
//        	     rd.close();
//       	         System.out.println(urlString);
      	      }catch(IOException e)
      	      {
      	         e.printStackTrace();
      	      } 
            }
        }
        //group email
        else if (patientGroup != null){
            StringBuffer emailAddresses = new StringBuffer();
            int groupEmailCount = Integer.parseInt(Util.notNull(request.getParameter("groupEmailCount"), "0"));
            for (int i = 1; i <= groupEmailCount; i++){
                String emailAddress = request.getParameter("email" + i);
                String shouldSendEmail = request.getParameter("sendEmail" + i);
                if (shouldSendEmail != null && shouldSendEmail.equalsIgnoreCase("on") && Util.isValidEmail(emailAddress)){
                    //we don't validate group emails, just add the ones that are valid, and send them
                    emailAddresses.append(emailAddress);
                    if (i != groupEmailCount) emailAddresses.append(",");
                    
                }
            }
            if (emailAddresses.length() < 5){
                error = "Please enter at least one email, and make sure you check the box next to it";
            }
            if (error == null){
                Properties props = new Properties();
				props.put("firstName", u.getFirstName());
                props.put("lastName", u.getLastName());
                //props.put("url", String.format("%s/p/%s", Config.getString("url"), program.getCode()));
                String add="/corp/patients/program.jsp?id="+program.getId();
                props.put("url", Config.getString("url")+add);
                MailMessage mm = null;
                String tempemail = "";
                String successemail = "";
                
                try
        	      {
                for(String emailid : emailAddresses.toString().split(",")){
                	
                	/* int intIndex = -1;
              	  if(emailid.split("@")[1].toLowerCase().equals("yahoo.com")){
              		  URL url = new URL("http://my-addr.com/email/?mail="+emailid+"");
           	         URLConnection urlConnection = url.openConnection();
           	         Properties systemProperties = System.getProperties();
           	         systemProperties.setProperty("http.proxyHost","localhost");
           	            systemProperties.setProperty("http.proxyPort", "8080"); 
           	         HttpURLConnection connection = null;
           	         if(urlConnection instanceof HttpURLConnection)
           	         {
           	            connection = (HttpURLConnection) urlConnection;
           	         }
           	         else
           	         {
           	            System.out.println("Please enter an HTTP URL.");
           	            return;
           	         }
           	         BufferedReader in = new BufferedReader(
           	         new InputStreamReader(connection.getInputStream()));
           	         String urlString = "";
           	         String current;
           	         while((current = in.readLine()) != null)
           	         {
           	            urlString += current;
           	         }
//            	         System.out.println(urlString);
           	         intIndex = urlString.indexOf("<b>e-mail exist</b>");
              	  }else{
              		String data = URLEncoder.encode("email", "UTF-8") + "=" + URLEncoder.encode(""+emailid+"", "UTF-8");

              	    URL url = new URL("http://www.emailvalidator.co/EmailValidation/ProcessSingle");
              	    URLConnection conn = url.openConnection();
              	    conn.setDoOutput(true);
              	    OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
              	    wr.write(data);
              	    wr.flush();

              	    BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
              	    String line = rd.readLine();
              	    
        	  	   intIndex = line.indexOf("is a valid Email address");
              	  } 
          	       if(intIndex != - 1){*/
          	    	 if(applicationName.equalsIgnoreCase("M3Clinic")){
                     	mm = new MailMessage(props, emailTemplate, emailid, "Your "+applicationName+" Program","M3ClinicApp <info@m3clinicapp.com.au>",true);
                     }else{
                     	mm = new MailMessage(props, emailTemplate, emailid, "Your "+applicationName+" App Program",true);
                     }
        	          mm.send();
        	          successemail = successemail+", "+emailid;
        	          session.setAttribute("successEmail", "Email sent to"+successemail.substring(1)+"");
        	          /*} else{
        	    	   Properties props1 = new Properties();
        	            props1.put("user", u.getFirstName()+" "+u.getLastName());
        	           if(applicationName.equalsIgnoreCase("M3Clinic")){
        	            	props1.put("program", "M3Clinic Program");
        	           }else{
        	        	     props1.put("program", "Fiizio App Program");
        	           }
        	           props1.put("email", emailid);
        	           props1.put("company", corp.getName());
        	           List<Patient> patientsInGroup = lm.getPatientsInGroup(patientGroup.getId());
        	           if(Config.getString("https").equals("true")){
        	        	   for (Patient p : patientsInGroup) { 
        	        		   if(p.getEmail().equals(emailid)){
        	        			   props1.put("patientname", p.getFirstName()+" "+p.getLastName());
        	        			   props1.put("phone", p.getPhone());
        	            		   props1.put("url", "https://"+corp.getSlug()+"."+Config.getString("baseDomain")+"/corp/patients/edit.jsp?id="+p.getId()+"");
        	        		   }
        	        	   }
        	           }else{
        	        	   for (Patient p : patientsInGroup) { 
        	        		   if(p.getEmail().equals(emailid)){
        	        			   props1.put("patientname", p.getFirstName()+" "+p.getLastName());
        	        			   props1.put("phone", p.getPhone());
        	        			   props1.put("url", "http://"+corp.getSlug()+"."+Config.getString("baseDomain")+"/corp/patients/edit.jsp?id="+p.getId()+"");
        	        		   }
        	        	   }
        	           }
        	           if(applicationName.equalsIgnoreCase("M3Clinic")){
        	        	   MailMessage msg = new MailMessage(props1, "email_send_failed.vm", u.getEmail(), "M3Clinic Program Send Failed");
        	        	  try {
        	 	                msg.send();
        	 	            }
        	 	            catch (Exception e) {
//        	  	                e.printStackTrace();
//        	  	                throw e;
        	 	            }
       	           }else{
       	        	   MailMessage msg = new MailMessage(props1, "email_send_failed.vm", u.getEmail(), "Fiizio App Program Send Failed");
       	        	   try {
       	 	                msg.send();
       	 	            }
       	 	            catch (Exception e) {
//       	  	                e.printStackTrace();
//       	  	                throw e;
       	 	            }
       	           }
        	    	 tempemail = tempemail+", "+emailid;
        	    	 error = "<p>Delivery to the following recipient failed permanently: "+tempemail.substring(1)+".</p> Please update the email address for this patient and click send email.";
        	       } */
//         	         System.out.println(urlString);
        	      }
                String type="Patients";
                String activity_page="corp/programs/inline/email.jsp";
            	String corp_id=String.valueOf(corp.getId());
            	String corp_name=corp.getName();
            	String corp_loc=corp.getSuburb();
            	String user_id=String.valueOf(u.getId());
            	String user_name=u.getFullName();
            	String invitor_id=String.valueOf(patientGroup.getId());
            	String invitor_name=patientGroup.getGroupName();
            	String flag="sent";
            	String from="---";
            	String to="---";
            	String desc="Send group Email By User";
                try
                {		
                 Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
                }
                catch(Exception e)
                {
                }
                if(error == null){
        	         response.sendRedirect(corp.getHost() + "/close.jsp");
                     return;
                }
        	      }catch(IOException e)
        	      {
        	         e.printStackTrace();
        	      } 
        	      
//                 mm.send();
             
      	     
                   
//                 response.sendRedirect(corp.getHost() + "/close.jsp");
//                 return;
            }
        }
    } 
    else {
    	session.setAttribute("successEmail", null);
        if (patient != null){
            email = patient.getEmail();
        }
    }



%>
<html>
<head><title>Send Email</title>
<script>
    $(document).ready(function(){
    $('.check:button').toggle(function(){
        $('input:checkbox').attr('checked','checked');
        $(this).val('Un-check All');
    },function(){
        $('input:checkbox').removeAttr('checked');
        $(this).val('Check All');        
    });
});
</script>
</head>
<body>
<div class="page-header">
    <h1>Send Email</h1>
</div>
<%if(session.getAttribute("successEmail") != null){ %>
<div class="alert alert-success"><%=session.getAttribute("successEmail")%>.</div>
<%}session.setAttribute("successEmail", null); %>
<%@include file="/WEB-INF/includes/formErrorSimple.jsp"%>
<div class="row">
    <div class="span9">
        <form action="email.jsp" method="post" class="form-horizontal" name="email" id="email">
            <input type="hidden" name="id" value="<%=id%>">
            <input type="hidden" name="isValidEmail" id="isValidEmail">
            <% if (patient != null) { %>
                   <%=HTML.textInput("email", "Email Address", "100", "100", email)%>
                   <%=HTML.submitButton("Send Email")%>
            <% } 
               else if (patientGroup != null){ 
                   List<Patient> patientsInGroup = lm.getPatientsInGroup(patientGroup.getId());
                   int patientCount = 0;
                   %>
                   <table width="">
                   <%
                   for (Patient p : patientsInGroup) { 
                       patientCount++;                   %>
                       <tr>
                           <td><input type="checkbox" id="sendEmail<%=patientCount%>" name="sendEmail<%=patientCount%>" class="cb-email" />&nbsp;</td>
                           <td><%=p.getFullName()%>&nbsp;</td>
                           <td><input id="email<%=patientCount%>" name="email<%=patientCount%>"class="input-xlarge" type="text" size="20" maxlen="20" name="%s" value="<%=p.getEmail()%>"/></td>
                       </tr>
            <%     } %>
                   </table>
                   <input type="button" class="check btn" value="Check All" />
                   <%=HTML.submitButton("Send Emails")%>
                   <input type="hidden" name="groupEmailCount" value="<%=patientCount%>">
             <% } %>
        </form>
    </div>
</div>
</body>
</html>