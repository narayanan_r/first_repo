<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="java.net.URLEncoder" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.List" %>
<%@	page import="java.util.ArrayList"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");

System.out.println("inside add excercise");

    if (cu == null) {
    	System.out.println("cu == null");
        response.sendRedirect("/close.jsp?redir=" + URLEncoder.encode(Util.loginUrl(Constants.LoginReason.login, request), "UTF-8"));
        return;
    }

    User u = cu.getUser();
    Corporation corp = cu.getCorporation();
    
    System.out.println("inside add excercise=====>1");

    long patientId = Long.parseLong(Util.notNull(request.getParameter("patient"), "0"));
    long groupId = Long.parseLong(Util.notNull(request.getParameter("group"), "0"));
    long programId = Long.parseLong(Util.notNull(request.getParameter("id"), "0"));
    long add = Long.parseLong(Util.notNull(request.getParameter("add"), "0"));
    String bodyPart = Util.notNull(request.getParameter("bp"), (String) session.getAttribute("bpType.bp"));
    String type = Util.notNull(request.getParameter("type"), (String) session.getAttribute("bpType.type"));

    session.setAttribute("bpType.bp", bodyPart);
    session.setAttribute("bpType.type", type);
    
    String query = (request.getParameter("query") != null && request.getParameter("query").trim().length() > 0) ? request.getParameter("query") : null;
    
    LibraryManager lm = new LibraryManager();
    
    if ((groupId == 0 || patientId == 0) && programId == 0) {
        response.sendRedirect(corp.getHost() + "/close.jsp");
        return;
    }

    System.out.println("inside add excercise=======>2");
    Patient patient = null;
    Program program = null;
    PatientGroup patientGroup = null;

    program = lm.getProgram(programId);
    Patient p2=lm.getPatient(program.getPatientId());
    
    if (program == null) {
        response.sendError(404, "Program not found");
        return;
    }
    
    if (program != null) {
        patient = lm.getPatient(program.getPatientId());
        patientGroup = lm.getPatientGroup(program.getGroupId());
        
    }

//     if (program.getCorpId() != corp.getId()) {
//         response.sendError(403, "Patient or program not part of this practice.");
//         return;
//     }
	int pgmExSequene = 0;
    
    Exercise.BodyPart pBodyPart = null;
    Exercise.Type pType = null;
    try {
        pBodyPart = Exercise.BodyPart.valueOf(bodyPart);
    } catch (Exception e) {}

    try {
        pType = Exercise.Type.valueOf(type);
    } catch (Exception e) {}

    List<Exercise> exercises = null;
    
    System.out.println("inside add excercise============>3");

    if (query != null) {
    		exercises = lm.getExercisesBySearchforAddExercise(pBodyPart, pType, corp.getId(), query);
    } else {
    	if (pBodyPart != null || pType != null) {
            exercises = lm.getExercises(pBodyPart, pType, corp.getId());
        }
    }
    if(p2!=null)
    {
    	 System.out.println("pc!=null----->");
    	String exerciseId = request.getParameter("excerciseid");
    String types="Patients";
    String activity_page="corp/programs/inline/add_exercise.jsp";
	String corp_id=String.valueOf(corp.getId());
	String corp_name=corp.getName();
	String corp_loc=corp.getSuburb();
	String user_id=String.valueOf(u.getId());
	String user_name=u.getFullName();
	String invitor_id=String.valueOf(p2.getId());
	String invitor_name=p2.getFullName();
	String flag="add";
	String desc="Add Exercise By User";
	String from="Exercise add";
	String to="List of Exercise is add"+exerciseId;
	 try{
		 System.out.println("allActivitiesLogs2----->");
	    	//Util.allActivitiesLogs("Patients", u.getId(), corp.getName(), "Edit Exercise");
		 Util.allActivitiesLogs2("Patients",activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
		 System.out.println(" adding exercise to list");
 	}catch(Exception ee){
 		ee.printStackTrace();
 		//System.err.append("Exception on editecercise",ee.printStackTrace());
 		System.out.println("Exception on edit_exercise"+ee.getMessage());
 	} 
    }
    System.out.println("After adding exercise");
    String redirectUrl = corp.getHost() + "/close.jsp?redir=" + URLEncoder.encode("/corp/programs/edit.jsp?id=" + programId + "&seq=" + pgmExSequene, "UTF-8");
    if (request.getMethod().equalsIgnoreCase("post")){
    	System.out.println(" INSIDE POST METHOD---->");
    	
    	String exerciseId = request.getParameter("excerciseid");
    	if(exerciseId!=null){
	    	lm.addProgramExercises(Long.valueOf(programId), exerciseId);
	    	response.sendRedirect(redirectUrl);
	    	return;
    	}
    	
    }
    		
    
%>
<html>
<head><title>Add exercise</title>
<style type="text/css">
 .add-exe-pop{
    width: 95%;
    height: 540px;
        position: fixed;
        top: 60px;
        left: 0;
        right: 0;
        opacity: 1;
        overflow: visible;
        margin: 0 auto;
        z-index: 8020;
    }
    .add-exe-pop-skin {
        position: relative;
        background: #f9f9f9;
        color: #444;
        text-shadow: none;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
        padding: 15px;
        width: auto;
        height: auto;
    }
    .add-exe-pop-outer{
        position:relative;
        padding: 0;
        margin: 0;
        border: 0;
        outline: none;
        vertical-align: top;
    }
    .add-exe-pop-inner{
        height: 630px;
        width: 100%;
        overflow: auto;
    }
   .add-exe-pop-close {
	    position: absolute;
	    top: -18px;
	    right: -18px;
	    width: 36px;
	    height: 36px;
	    cursor: pointer;
	    z-index: 8040;
	    background-image: url(fancybox_sprite.png);
	}
    
    .page-headers{padding-bottom:17px;margin:18px 0;}
</style>
<script type="text/javascript">
var ids = new Array();
function validateAddExercise(){
    if((document.getElementById("excerciseid") == null) || (document.getElementById("excerciseid").value == "")){
    	document.getElementById("errorhideorshow").innerHTML="Please select the exercises";
    	document.getElementById("errorhideorshow").style.display='block';
    	return false;
    }else {
    	document.getElementById("errorhideorshow").style.display='none';
    	var exerciseId =  document.getElementById("excerciseid").value;
		if(exerciseId!= null){
			var arr = exerciseId.split(",");
			for(var i=0;i<arr.length;i++){
				if(document.getElementById(arr[i]) != null){
    				document.getElementById(arr[i]).checked = true;
				}
			}
		}
    	return true;
    }
}

function redirectOnCancel() {
	window.location='<%=redirectUrl%>';
}

$(document).ready(function() {
    $('#type').change(function(event) {
    	var type = document.getElementById("type").value;
    	var bp = document.getElementById("bp").value;
    	var query = document.getElementById("query").value;
    	var url = 'add_exercise.jsp?bp='+bp+'&type='+type+'&query='+query+'&id='+<%=programId%>;
        $.ajax({
        url: 'add_exercise.jsp?bp='+bp+'&type='+type+'&query='+query+'&id='+<%=programId%>,
        type: 'POST',
        success: function(data) {
        	$("#exerciseMediaDiv").load(url+" #exerciseMediaDiv>*","",function() {
        		var exerciseId =  document.getElementById("excerciseid").value;
        		if(exerciseId!= null){
        			var arr = exerciseId.split(",");
        			for(var i=0;i<arr.length;i++){
        				if(document.getElementById(arr[i]) != null){
            				document.getElementById(arr[i]).checked = true;
        				}
        			}
        		}
	          });
        }
      });
    });
    
    
    $('#bp').change(function(event) {
    	var type = document.getElementById("type").value;
    	var bp = document.getElementById("bp").value;
    	var query = document.getElementById("query").value;
    	var url = 'add_exercise.jsp?bp='+bp+'&type='+type+'&query='+query+'&id='+<%=programId%>;
        $.ajax({
        url: 'add_exercise.jsp?bp='+bp+'&type='+type+'&query='+query+'&id='+<%=programId%>,
        type: 'POST',
        success: function(data) {
        	$("#exerciseMediaDiv").load(url+" #exerciseMediaDiv>*","",function() {
        		var exerciseId =  document.getElementById("excerciseid").value;
        		if(exerciseId!= null){
        			var arr = exerciseId.split(",");
        			for(var i=0;i<arr.length;i++){
        				if(document.getElementById(arr[i]) != null){
            				document.getElementById(arr[i]).checked = true;
        				}
        			}
        		}
	          });
        }
      });
    });
    
    $('#search').click(function(event) {
    	var type = document.getElementById("type").value;
    	var bp = document.getElementById("bp").value;
    	var query = document.getElementById("query").value;
    	var url = 'add_exercise.jsp?bp='+bp+'&type='+type+'&query='+query+'&id='+<%=programId%>;
        $.ajax({
        url: 'add_exercise.jsp?bp='+bp+'&type='+type+'&query='+query+'&id='+<%=programId%>,
        type: 'POST',
        success: function(data) {
        	$("#exerciseMediaDiv").load(url+" #exerciseMediaDiv>*","",function() {
        		var exerciseId =  document.getElementById("excerciseid").value;
        		if(exerciseId!= null){
        			var arr = exerciseId.split(",");
        			for(var i=0;i<arr.length;i++){
        				if(document.getElementById(arr[i]) != null){
            				document.getElementById(arr[i]).checked = true;
        				}
        			}
        		}
	          });
        }
      });
    });
    
});
function selectexercise(obj){
	
	var exerciseId = obj.value;
	if(obj.checked){
		ids.push(exerciseId);
	}else{
		remove(ids, exerciseId);
	}
    document.getElementById("excerciseid").value = ids;
}

function remove(arr) {
	var what, a = arguments, L = a.length, ax;
	while (L > 1 && arr.length) {
		what = a[--L];
		while ((ax = arr.indexOf(what)) !== -1) {
			arr.splice(ax, 1);
		}
	}
	return arr;
}

function redirectfunction(){
	
	window.location.href='<%=redirectUrl%>'
}
</script>
</head>
<body>

<div class="add-exe-pop" style="">
    <div class="add-exe-pop-skin" style="">
        <div class="add-exe-pop-outer">
            <div class="add-exe-pop-inner" style="">

<div class="pageheaders" style="text-align: center;">
    <% String exerciseFor = "";
       if (patient != null) exerciseFor = patient.getFullName();
       if (patientGroup != null) exerciseFor = patientGroup.getGroupName();
    %>
    <h1>Add Exercise <small><%=exerciseFor%></small></h1>
</div>
<div class="row">
    <div class="span12" style="width: 97%;">
    <div class="alert alert-error" id="errorhideorshow" 
		style="margin-left: 33px;width: 250px; display: none;" >
	</div>
        <form method="post" action="add_exercise.jsp" class="well form-inline" name="bpType">
            <input type="hidden" name="id" value="<%=programId%>">
            <input type="hidden" name="patient" value="<%=patientId%>">
            <input type="hidden" name="group" value="<%=groupId%>">
            <input type="hidden" name="excerciseid" id="excerciseid">
            <div style="margin: 0px;width: 100%;float:left; padding-bottom: 10px;">
            	<table style="margin:0 auto;"><tr>
            	<td width="100px;"><input type="text" class="input-medium search-query" name="query" id="query" value="<%=Util.formValue(query)%>" style="width: 206px;"></td>
            	<td width="65px;"><input name="submit" class="btn btn-primary" type="button" value="Search" id="search" style="margin: 0 5px;"/></td>
            	<td width="65px;"><input name="submit" class="btn btn-primary" onclick="return validateAddExercise();" type="submit" value="Select"/></td>
            	<td width="65px;"><input name="submit" class="btn" onclick="redirectOnCancel();" value="Cancel" type="button"/></td>
            	</tr></table>
		    </div>
		    <div style="display:block; text-align:center;">
            <select name="type" id="type" style="">
                <option value="">Select Exercise Type</option>
                <%
                    for (Exercise.Type option : Exercise.Type.values()) {
                %>
                <option value="<%=option.name()%>" <%=(option.name().equals(type)) ? "selected" : ""%>><%=option.getValue()%></option>
                <%
                    }
                %>
            </select>
            <select name="bp" id="bp">
                <option value="">Select Body Part</option>
                <%
                    for (Exercise.BodyPart option : Exercise.BodyPart.values()) {
                %>
                <option value="<%=option.name()%>" <%=(option.name().equals(bodyPart)) ? "selected" : ""%>><%=option.getValue()%></option>
                <%
                    }
                %>
            </select>
            </div>
        </form>
    </div>
</div>
<div class="row">
    <div class="span12" id="exerciseMediaDiv" style="width: 97%;max-height: 400px;overflow: auto;">
        <%
            if (exercises == null) {
            %>
            <p>
                <b>Select categories to display exercises</b>
            </p>
            <%
                } else if (exercises.size() == 0) {
            %>
            <p>
                <b>There are no exercises for the selected categories</b>

            </p>
            <%
                } else {
        %>
        <ul class="thumbnails">
            <%

                for (Exercise exercise : exercises) {
                    String url = "/images/photo_146_97.gif";
                    if (exercise.getKeyPhotoId() > 0) {
                        url = "/media?id=" + exercise.getKeyPhotoId() + "&width=146&v=" + exercise.getKeyPhotoVersion();
                    }
                    String link = "add_exercise.jsp?id=" + programId + "&patient=" + patientId + "&group=" + groupId + "&add=" + exercise.getId();
            %>
            <li>
                <div class="thumbnail">
                <img src="<%=url%>" width="146" height="105" ><br><input type="checkbox" id="<%=exercise.getId()%>" onclick="selectexercise(this);" value="<%=exercise.getId()%>">
                <h5 style="width: 146px; height: 35px; overflow: hidden;"><%=exercise.getName()%></h5>
                </div>
            </li>
            <%
                }
              
                }
            %>


        </ul>
        <%
    
        %>
    </div>
</div>

</div>
    </div>
    <div  id="close" title="Close" class="fancybox-item add-exe-pop-close " onclick="redirectfunction()" >
    
        </div>
    </div>
</div>
<div id="fancybox-overlay" class="overlay-fixed" style="cursor: pointer; opacity: 0.8; display: block;">
</div>



</body>
</html>