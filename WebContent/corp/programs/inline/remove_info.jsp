<%@page import="java.util.Date"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="java.net.URLEncoder" %>
<%@	page import="java.util.ArrayList"%>
<%@	page import="java.util.List"%>
<%
    CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");
	List<Long> Eideditlist;
    if (cu == null) {
        response.sendRedirect("/close.jsp?redir=" + URLEncoder.encode(Util.loginUrl(Constants.LoginReason.login, request), "UTF-8"));
        return;
    }

    User u = cu.getUser();
    Corporation corp = cu.getCorporation();
    /* try{
    	Util.allActivitiesLogs("Patients", u.getId(), corp.getName(), "Delete Articles Info");
    }catch(Exception e){
    	
    } */
    long id = Long.parseLong(Util.notNull(request.getParameter("id"), "0"));
    long info = Long.parseLong(Util.notNull(request.getParameter("info"), "0"));
    if (id == 0) {
        response.sendRedirect(corp.getHost() + "/close.jsp");
        return;
    }
    LibraryManager lm = new LibraryManager();
    MyInfo myInfo = lm.getInfo(info);
    if (myInfo == null) {
        response.sendRedirect(corp.getHost() + "/close.jsp");
        return;
    }
    Program program = lm.getProgram(id);
	String infoName=myInfo.getHeadline();
    if (program == null) {
        response.sendError(404, "Program not found");
        return;
    }
    String to=null;
    if(id>0){
    	to="InfoName:"+myInfo.getHeadline();
    }
//     if (program.getCorpId() != corp.getId()) {
//         response.sendRedirect(corp.getHost() + "/close.jsp");
//         return;
//     }

    if (request.getMethod().equalsIgnoreCase("post")) {
        if (request.getParameter("submit").startsWith("Cancel")) {
            response.sendRedirect(corp.getHost() + "/close.jsp");
        } 
        else {
        		Eideditlist = (ArrayList<Long>)session.getAttribute("Eidlist");
            	if(Eideditlist != null){
            	for(Long Eid : Eideditlist){
    	       			lm.removeInfo(Eid, info);
            	}
        	}
            
            //update the programs last updated date
            program.setLastUpdated(new Date());
            program = lm.save(program);
            
           
            
            Patient patient = lm.getPatient(program.getPatientId());
            if(patient!=null)
            {
            String type="Patients";
            String activity_page="corp/programs/inline/remove_info.jsp";
        	String corp_id=String.valueOf(corp.getId());
        	String corp_name=corp.getName();
        	String corp_loc=corp.getSuburb();
        	String user_id=String.valueOf(u.getId());
        	String user_name=u.getFullName();
        	String invitor_id=String.valueOf(patient.getId());
        	String invitor_name=patient.getFullName();
        	String flag="new";
        	String from="Delete info from Program:"+program.getName();
        	
        	String desc="Delete articles info "+infoName+" from Patient "+patient.getFullName()+" By User";
            try
            {		
             Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
            }
            catch(Exception e)
            {
            }
            }
            response.sendRedirect(corp.getHost() + "/close.jsp?redir=" + URLEncoder.encode("/corp/programs/edit.jsp?info=true&id=" + id, "UTF-8"));
        }
        return;
    }

 %>

<html>
<head><title>Delete Article</title></head>
<body>
<div class="page-header">
    <h1>Delete Article</h1>
</div>
<div class="row">
    <div class="span6">
        <p>
            Delete <b><%=myInfo.getHeadline()%></b> from this program.
        </p>
        <p>
            Are you sure?
        </p>
        <form action="remove_info.jsp" method="post">
            <input type="hidden" name="id" value="<%=id%>">
            <input type="hidden" name="info" value="<%=info%>">
            <%=HTML.confirmCancel()%>
        </form>
    </div>
</div>
</body>
</html>