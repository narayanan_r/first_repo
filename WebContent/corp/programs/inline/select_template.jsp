<%@page import="au.com.ourbodycorp.model.PatientGroup"%>
<%@page import="au.com.ourbodycorp.model.Patient"%>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.Corporation" %>
<%@ page import="au.com.ourbodycorp.model.CorporationUser" %>
<%@ page import="au.com.ourbodycorp.model.Program" %>
<%@ page import="au.com.ourbodycorp.model.User" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="java.net.URLEncoder" %>
<%@ page import="java.util.List" %>
<%@page import="java.sql.PreparedStatement"%>
<%@ page import="java.sql.Connection" %>
<%@page import="java.sql.ResultSet"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");
    User u = cu.getUser();
    Corporation corp = cu.getCorporation();
    
    long patientId = Long.parseLong(Util.notNull(request.getParameter("patient"), "0"));
    long groupId = Long.parseLong(Util.notNull(request.getParameter("group"), "0"));
    long id = Long.parseLong(Util.notNull(request.getParameter("id"), "0"));
    String error = null;
    
    /* if(patientId > 0){
    	try{
        	Util.allActivitiesLogs("Patients", u.getId(), corp.getName(), "Program from Template");
        }catch(Exception e){
        	
        }
    }else{
    	try{
        	Util.allActivitiesLogs("Patient Groups", u.getId(), corp.getName(), "Program from Template");
        }catch(Exception e){
        	
        }
    }*/
    

    if (id != 0) {
        LibraryManager lm = new LibraryManager();
        Program p = lm.getProgram(id);
        Patient patient = lm.getPatient(patientId);
        PatientGroup patientGroup=lm.getPatientGroup(groupId);
        if(error == null){
        if (p.getCorpId() == corp.getId()) {
            long newId = lm.copyProgram(p.getId(), patientId, groupId, u.getId(), "copyProgram");
           
            if(patientId > 0){
            	
                String type="Patients";
                String activity_page="corp/programs/inline/select_template.jsp";
            	String corp_id=String.valueOf(corp.getId());
            	String corp_name=corp.getName();
            	String corp_loc=corp.getSuburb();
            	String user_id=String.valueOf(u.getId());
            	String user_name=u.getFullName();
            	String invitor_id=String.valueOf(patient.getId());
            	String invitor_name=patient.getFullName();
            	String flag="select";
            	String from="---";
            	String to="---";
            	String desc="Select Program "+p.getName()+" from Template for Patietnt "+patient.getFullName()+" By User";
                try
                {		
                 Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
                }
                catch(Exception e)
                {
                }
                response.sendRedirect(corp.getHost() + "/close.jsp?redir=" + URLEncoder.encode("/corp/programs/edit.jsp?id=" + newId, "UTF-8"));
                return;
            }else{
            	String type="Patient Groups";
                String activity_page="corp/programs/inline/select_template.jsp";
            	String corp_id=String.valueOf(corp.getId());
            	String corp_name=corp.getName();
            	String corp_loc=corp.getSuburb();
            	String user_id=String.valueOf(u.getId());
            	String user_name=u.getFullName();
            	String invitor_id="";
            	String invitor_name="---";
            	String flag="select";
            	String from="---";
            	String to="---";
            	String desc="Select Program "+p.getName()+" from Template for PatietntGroup "+patientGroup.getGroupName()+"  By User";
                try
                {		
                 Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
                }
                catch(Exception e)
                {
                }
                response.sendRedirect(corp.getHost() + "/close.jsp?redir=" + URLEncoder.encode("/corp/groups/detail.jsp?id=" + groupId, "UTF-8"));
                return;
            }
           /*  response.sendRedirect(corp.getHost() + "/close.jsp?redir=" + URLEncoder.encode("/corp/programs/edit.jsp?id=" + newId, "UTF-8"));
            return; */
        }}
    }


    LibraryManager lm = new LibraryManager();
    List<Program> templates = lm.getProgramTemplates(corp.getId());

%>
<html>
<head><title>Templates</title></head>
<body>
<%@include file="/WEB-INF/includes/formErrorSimple.jsp"%>
<div class="page-header">
    <h1>Select Program Template</h1>
</div>
<div class="row">
    <div class="span6">
        <%
            if (templates.size() == 0) {
        %>
        <div class="alert alert-info">
            No templates created yet
        </div>
        <%
            } else {
        %>
        <table class="table">
            <thead>
            <tr>
                <th>Name</th>
            </tr>
            </thead>
            <tbody>
            <%
                for (Program p : templates) {
            %>
            <tr>
                <td><a href="select_template.jsp?id=<%=p.getId()%>&patient=<%=patientId%>&group=<%=groupId%>"><%=Util.textAreaValue(p.getName()).length() > 0 ? Util.textAreaValue(p.getName()) : "(not named)" %></a></td>
            </tr>
            <%
                }
            %>
            </tbody>
        </table>
        <%
            }
        %>
    </div>
</div>
</body>
</html>