<%@page import="au.com.ourbodycorp.model.PatientGroup"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.Corporation" %>
<%@ page import="au.com.ourbodycorp.model.CorporationUser" %>
<%@ page import="au.com.ourbodycorp.model.Patient" %>
<%@ page import="au.com.ourbodycorp.model.Program" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="java.net.URLEncoder" %>
<%@	page import="java.util.ArrayList"%>
<%@ page import="java.util.List" %>
<%@page import="java.sql.PreparedStatement"%>
<%@ page import="java.sql.Connection" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%
    CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");
    Corporation corp = cu.getCorporation();
    User u = cu.getUser();
    /* try{
    	Util.allActivitiesLogs("Patients", u.getId(), corp.getName(), "Delete Program");
    }catch(Exception e){
    	
    } */
    long id = Long.parseLong(Util.notNull(request.getParameter("id"), "0"));
    Program program = null;
    String to=null;
    LibraryManager lm = new LibraryManager();
    if (id != 0) {
        program = lm.getProgram(id);
        if (program == null) {
            response.sendError(404, "Program not found");
            return;
        }
        
        to="ProgramName: "+program.getName();
//         if (program == null || program.getCorpId() != corp.getId()) {
//             response.sendError(404, "News item not found");
//             return;
//         } 
    } 
    else {
        response.sendError(400, "No program ID provided");
        return;
    }
    

    if (request.getMethod().equalsIgnoreCase("post")) {
        if (request.getParameter("submit").startsWith("Cancel")) {
            response.sendRedirect(corp.getHost() + "/close.jsp");
        } 
        else {
        	if(program.getPatientId() > 0 || program.getGroupId() > 0){
        		lm.deleteProgram(id);
        	}else{
        		List<Corporation> Eideditlist;
        		if(program.isSharelocation() == false){
        			Eideditlist = lm.getEidforDelTemplatePrograms(program.getName(),0,id);
	        	}else{
	        		Eideditlist = lm.getEidforDelTemplatePrograms(program.getName(),program.getShareid(),id);
	        	}
            	if(Eideditlist != null){
            	for(Corporation Eid : Eideditlist){
                	lm.deleteProgram(Eid.getId());
            		}
                		Connection conn = Util.getConnection();
                		List<Corporation> EidDellist = lm.getEidforDelPatientPrograms(program.getName(),program.getShareid(),id);
                	for(Corporation Eid : EidDellist){
    	        		PreparedStatement pst;
    		            pst = conn.prepareStatement("UPDATE programs set sharelocation = FALSE, shareprogram = FALSE, sharegroup = FALSE where id = ?");
    		            pst.setLong(1, Eid.getId());
    		            pst.executeUpdate();
    		            pst.close();
            		}
                	conn.close();
            	  }
        	else if(Eideditlist == null){
        		lm.deleteProgram(id);
        	}}
        	Patient p=lm.getPatient(program.getPatientId());
        	PatientGroup pg=lm.getPatientGroup(program.getGroupId());
        	String type="Patients";
            String activity_page="corp/programs/inline/delete_program.jsp";
        	String corp_id=String.valueOf(corp.getId());
        	String corp_name=corp.getName();
        	String corp_loc=corp.getSuburb();
        	String user_id=String.valueOf(u.getId());
        	String user_name=u.getFullName();
        	if(p!=null){
        		String invitor_id=String.valueOf(p.getId());
            	String invitor_name=p.getFullName();
            	String flag="new";
            	String from="Delete Program from Patient: "+invitor_name;
            	
            	String desc="Delete program from patient  By User";
                try
                {		
                 Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
                }
                catch(Exception e)
                {
                }	
        	}else if(pg !=null){
        		String invitor_id=String.valueOf(pg.getId());
            	String invitor_name=pg.getGroupName();
            	String flag="new";
            	String from="Delete Program from PatientGroup: "+invitor_name;
            	String desc="Delete program from patient By User";
                try
                {		
                 Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
                }
                catch(Exception e)
                {
                }
        	}else if(p == null && pg ==null){
        		String invitor_id="---";
            	String invitor_name="---";
            	String flag="new";
            	String from="Delete Program from Template By User";
            	
            	String desc="Delete program from patient By User";
                try
                {		
                 Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
                }
                catch(Exception e)
                {
                }
        	}
        	
	       

            
            String target = "/corp/programs/templates.jsp";
            if (program.getPatientId() > 0){
                target = "/corp/patients/detail.jsp?id=" + program.getPatientId();
            }
            else if (program.getGroupId() > 0){
                target = "/corp/groups/detail.jsp?id=" + program.getGroupId();
            }

            response.sendRedirect(corp.getHost() + "/close.jsp?redir=" + URLEncoder.encode(target, "UTF-8"));
        }
        return;
    }

 %>

<html>
<head><title>Delete Program</title></head>
<body>
<div class="page-header">
    <h1>Delete Program</h1>
</div>
<div class="row">
    <div class="span6">
        <p>
            Delete program. Are you sure?
        </p>
        <form action="delete_program.jsp" method="post">
            <input type="hidden" name="id" value="<%=program.getId()%>">
            <input type="hidden" name="patient" value="<%=program.getPatientId()%>">
            <%=HTML.confirmCancel()%>
        </form>
    </div>
</div>
</body>
</html>