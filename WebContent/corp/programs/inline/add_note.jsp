<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.List" %>
<%@	page import="java.util.ArrayList"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.PreparedStatement" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.net.URLEncoder" %>
<%

    CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");
    User u = cu.getUser();
    Corporation corp = cu.getCorporation();
    
    long programId = Long.parseLong(Util.notNull(request.getParameter("program"), "0"));
    long noteId = Long.parseLong(Util.notNull(request.getParameter("note"), "0"));
    String body = Util.notNull(request.getParameter("body"));

    Program program = null;
    ProgramNote note = null;
    LibraryManager lm = new LibraryManager();
    String type="Patients";
    String activity_page="corp/programs/inline/add_note.jsp";
	String corp_id=String.valueOf(corp.getId());
	String corp_name=corp.getName();
	String corp_loc=corp.getSuburb();
	String user_id=String.valueOf(u.getId());
	String user_name=u.getFullName();
	program = lm.getProgram(programId);
    Patient p =null;
    if (programId != 0) {
    	/* try{
        	Util.allActivitiesLogs("Patients", u.getId(), corp.getName(), "Add Note");
        }catch(Exception e){
        	
        } */
        
        if (program == null) {
            response.sendError(404, "Program not found");
            return;
        }
        p = lm.getPatient(program.getPatientId());
    } else if (noteId != 0) {
    	/* try{
        	Util.allActivitiesLogs("Patients", u.getId(), corp.getName(), "Edit Note");
        }catch(Exception e){
        	
        } */
        note = lm.getNote(noteId);
        if (note == null) {
            response.sendError(404, "Note not found");
            return;
        }
        program = lm.getProgram(note.getProgramId());
//         if (program.getCorpId() != corp.getId()) {
//             response.sendError(403);
//             return;
//         }
    } else {
        response.sendError(400, "No note or program ID provided");
        return;
    }

    String old=null;
    String title = "Add";
    if (note != null) {
        title = "Edit";
     	old="ProgramName:"+program.getName()+";Notes:"+note.getNote()+";";
    }

    if (request.getMethod().equalsIgnoreCase("post")) {
        if (request.getParameter("submit").equals("Cancel") || body.trim().length() == 0) {
            response.sendRedirect(corp.getHost() + "/close.jsp");
            return;
        }
    if(title == "Add"){
        List<Long> Eideditlist = (ArrayList<Long>)session.getAttribute("Eidlist");
        for(Long Eid : Eideditlist){
            note = new ProgramNote();
            note.setDate(new Date());
            note.setProgramId(Eid);
        	note.setNote(Util.safeInput(body, false));
        lm.save(note);
        
        //update the programs last updated date
        program.setLastUpdated(new Date());
        program = lm.save(program);
        }
        p = lm.getPatient(program.getPatientId());
        if(p!=null)
        {
        String invitor_id=String.valueOf(p.getId());
    	String invitor_name=p.getFullName();
    	String flag="new";
    	String from="Add note into Program"+program.getName();
    	String to="Notes:"+note.getNote();
    	String desc="Adding Notes to Program By User";
        try
    {		
    Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
    }
    catch(Exception e)
    {
    }
        }
    }else{
    	session.setAttribute("notename", note.getNote());
    	List<Long> Eideditlist = (ArrayList<Long>)session.getAttribute("Eidlist");
        for(Long Eid : Eideditlist){
        	Connection conn = Util.getConnection();
            PreparedStatement pst = conn.prepareStatement("select id from program_notes where program = ? and note = ? ");
            pst.setLong(1, Eid);
            pst.setString(2, session.getAttribute("notename").toString());
       		ResultSet rs = pst.executeQuery();
       		if(rs.next()){
       			note.setId(rs.getLong("id"));
       		}
       	note.setProgramId(Eid);
       	note.setDate(new Date());
        note.setNote(Util.safeInput(body, false));
        lm.save(note);
        
        //update the programs last updated date
        program.setLastUpdated(new Date());
        program = lm.save(program);
        p = lm.getPatient(program.getPatientId());
        
        if(p!=null)
        {
        String invitor_id=String.valueOf(p.getId());
    	String invitor_name=p.getFullName();
    	String flag="update";
    	String from="---";
    	String to="ProgramName:"+program.getName()+";Notes:"+note.getNote()+";";
    	String desc="Updating notes to Program By User";
        
    	try{
    		 Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,old,to,desc);
        }catch(Exception e){
        	
        }
        }
        conn.close();
        }
    }
        response.sendRedirect(corp.getHost() + "/close.jsp?redir=" + URLEncoder.encode("/corp/programs/edit.jsp?id="+program.getId()+"", "UTF-8"));

    } else if (note != null) {
        body = note.getNote();
    }


%>
<html>
<head><title><%=title%> Note</title></head>
<body>
<div class="page-header">
    <h1><%=title%> Note</h1>
</div>
<div class="row">
    <div class="span6">
        <form action="add_note.jsp" method="POST" class="form-horizontal">
            <input type="hidden" name="program" value="<%=programId%>">
            <input type="hidden" name="note" value="<%=noteId%>">
            <%=HTML.textArea("body", "Note", body, "input-xlarge", 10)%>
            <%=HTML.saveCancel()%>
        </form>
    </div>
</div>
</body>
</html>