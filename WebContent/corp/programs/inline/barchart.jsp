<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="java.net.URLEncoder" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.List" %>
<%@	page import="java.util.ArrayList"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%%>


<!DOCTYPE html>
<html>
<head>
    <base href="http://demos.telerik.com/kendo-ui/bar-charts/date-axis">
    <style>html { font-size: 14px; font-family: Arial, Helvetica, sans-serif; }</style>
    <title></title>
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2017.3.1026/styles/kendo.common.min.css" />
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2017.3.1026/styles/kendo.default.min.css" />
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2017.3.1026/styles/kendo.default.mobile.min.css" />

    <script src="https://kendo.cdn.telerik.com/2017.3.1026/js/jquery.min.js"></script>
    <script src="https://kendo.cdn.telerik.com/2017.3.1026/js/kendo.all.min.js"></script>
    

</head>
<body>

        <div id="example">
            <div class="demo-section k-content wide">
                <div id="chart"></div>
            </div>
            <div class="box wide">
                <div class="box-col">
                
<table><tr><td>
                    
                        <input id="baseUnitAuto" name="baseUnit"
                                type="radio" value="" autocomplete="off" />
                        <label for="baseUnitAuto">Automatic</label>
                   </td><td>
                        <input id="baseUnitYears" name="baseUnit"
                                type="radio" value="years" autocomplete="off" />
                        <label for="baseUnitYears">Years</label>
                    </td><td>
                    
                        <input id="baseUnitMonths" name="baseUnit"
                                type="radio" value="months" autocomplete="off" />
                        <label for="baseUnitMonths">Months</label>
                    </td><td>
                        <input id="baseUnitWeeks" name="baseUnit"
                                type="radio" value="weeks" checked="checked" autocomplete="off" />
                        <label for="baseUnitWeeks">Weeks</label>
  </td><td>                  
                        <input id="baseUnitDays" name="baseUnit"
                                type="radio" value="days" autocomplete="off" />
                        <label for="baseUnitDays">Days</label>
</td></tr></table>
              
            <script>
                var stats = [
                    { value: 30, date: new Date("2011/12/20") },
                    { value: 50, date: new Date("2011/12/21") },
                    { value: 45, date: new Date("2011/12/22") },
                    { value: 40, date: new Date("2011/12/23") },
                    { value: 35, date: new Date("2011/12/24") },
                    { value: 40, date: new Date("2011/12/25") },
                    { value: 42, date: new Date("2011/12/26") },
                    { value: 40, date: new Date("2011/12/27") },
                    { value: 35, date: new Date("2011/12/28") },
                    { value: 43, date: new Date("2011/12/29") },
                    { value: 38, date: new Date("2011/12/30") },
                    { value: 30, date: new Date("2011/12/31") },
                    { value: 48, date: new Date("2012/01/01") },
                    { value: 50, date: new Date("2012/01/02") },
                    { value: 55, date: new Date("2012/01/03") },
                    { value: 35, date: new Date("2012/01/04") },
                    { value: 30, date: new Date("2012/01/05") }
                ];

                function createChart() {
                    $("#chart").kendoChart({
                        dataSource: {
                            data: stats
                        },
                        series: [{
                            type: "column",
                            aggregate: "avg",
                            field: "value",
                            categoryField: "date"
                        }],
                        categoryAxis: {
                            baseUnit: "weeks",
                            majorGridLines: {
                                visible: false
                            }
                        },
                        valueAxis: {
                            line: {
                                visible: false
                            }
                        }
                    });
                }

                $(document).ready(createChart);
                $("#example").bind("kendo:skinChange", createChart);
                $(".box").bind("change", refresh);

                function refresh() {
                    var chart = $("#chart").data("kendoChart"),
                        series = chart.options.series,
                        categoryAxis = chart.options.categoryAxis,
                        baseUnitInputs = $("input:radio[name=baseUnit]"),
                        aggregateInputs = $("input:radio[name=aggregate]");

                    for (var i = 0, length = series.length; i < length; i++) {
                        series[i].aggregate = aggregateInputs.filter(":checked").val();
                    };

                    categoryAxis.baseUnit = baseUnitInputs.filter(":checked").val();

                    chart.refresh();
                }
            </script>          
        </div>


</body>
</html>
