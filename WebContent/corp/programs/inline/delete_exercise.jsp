<%@page import="java.util.Date"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="java.net.URLEncoder" %>
<%@ page import="java.util.List" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.PreparedStatement" %>
<%@ page import="java.sql.*" %>
<%@	page import="java.util.ArrayList"%>
<%
    CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");

    if (cu == null) {
        response.sendRedirect("/close.jsp?redir=" + URLEncoder.encode(Util.loginUrl(Constants.LoginReason.login, request), "UTF-8"));
        return;
    }

    User u = cu.getUser();
    Corporation corp = cu.getCorporation();
   /*  try{
    	Util.allActivitiesLogs("Patients", u.getId(), corp.getName(), "Delete Exercise");
    }catch(Exception e){
    	
    } */
    long id = Long.parseLong(Util.notNull(request.getParameter("id"), "0"));
    if (id == 0) {
        response.sendRedirect(corp.getHost() + "/close.jsp");
        return;
    }
    LibraryManager lm = new LibraryManager();
    ProgramExercise pe = lm.getProgramExercise(id);
    if (pe == null) {
        response.sendError(404, "Program not found");
        return;
    }
    if (pe == null) {
        response.sendRedirect(corp.getHost() + "/close.jsp");
        return;
    }
    String peName="ExerciseName:"+pe.getName()+";Descriptoin:"+pe.getDescription()+";Sequence:"+pe.getSequence()+";Base:"+pe.getBase()+";Reps:"+pe.getReps()+";Sets:"+pe.getSets()+";Rest:"+pe.getRest();
    Program program = lm.getProgram(pe.getProgramId());
    Patient p=lm.getPatient(program.getPatientId());

//     if (program.getCorpId() != corp.getId()) {
//         response.sendRedirect(corp.getHost() + "/close.jsp");
//         return;
//     }

    if (request.getMethod().equalsIgnoreCase("post")) {
        if (request.getParameter("submit").startsWith("Cancel")) {
            response.sendRedirect(corp.getHost() + "/close.jsp");
        }
        else {
        	List<Long> Eideditlist = (ArrayList<Long>)session.getAttribute("Eidlist");
        	for(Long Eid : Eideditlist){
        		Connection conn = Util.getConnection();
	            PreparedStatement pst = conn.prepareStatement("select id from program_exercises where program = ? and name = ? ");
	            pst.setLong(1, Eid);
	            pst.setString(2, pe.getName());
	       		ResultSet rs = pst.executeQuery();
	       		if(rs.next()){
            		lm.deleteProgramExercise(rs.getLong("id"));
	       		}
	       		conn.close();

        	}
            //update the programs last updated date
            program.setLastUpdated(new Date());
            program = lm.save(program);
            
            if(p!=null)
            {
            String type="Patients";
            String activity_page="corp/programs/inline/delete_exercise.jsp";
        	String corp_id=String.valueOf(corp.getId());
        	String corp_name=corp.getName();
        	String corp_loc=corp.getSuburb();
        	String user_id=String.valueOf(u.getId());
        	String user_name=u.getFullName();
        	String invitor_id=String.valueOf(p.getId());
        	String invitor_name=p.getFullName();
        	String flag="new";
        	String from="Delete Exercise for Program:"+program.getName();
        	String to=peName;
        	String desc="Deleted ProgramExercise "+peName+" from Program "+program.getName()+" By User";
            try
            {		
             Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
            }
            catch(Exception e)
            {
            }
            }

            
            response.sendRedirect(corp.getHost() + "/close.jsp?redir=" + URLEncoder.encode("/corp/programs/edit.jsp?id=" + pe.getProgramId(), "UTF-8"));
        }
        return;
    }

    String title = (pe.getType().equalsIgnoreCase("exercise")) ? "Exercise" : "Rest";
 %>

<html>
<head><title>Delete <%=title%></title></head>
<body>
<div class="page-header">
    <h1>Delete <%=title%></h1>
</div>
<div class="row">
    <div class="span6">
        <p>
            Delete <b><%=pe.getName()%></b> from this program.
        </p>
        <p>
            Are you sure?
        </p>
        <form action="delete_exercise.jsp" method="post">
            <input type="hidden" name="id" value="<%=id%>">
            <%=HTML.confirmCancel()%>
        </form>
    </div>
</div>
</body>
</html>