<%@ page import="java.util.List" %>
<%@ page import="au.com.ourbodycorp.Config" %>
<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="au.com.ourbodycorp.model.printout.ERR_T" %>
<%@ page import="au.com.ourbodycorp.model.printout.ExercisePrintoutGenerator" %>
<%@ page import="au.com.ourbodycorp.model.printout.PDFExercisePrintout" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.TextLocalClient" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    //the printout directory path specifies where to save the printout in the
    //web server. If root directory of the web server changes then printout 
    //dir path will need to change accordingly
    
    
    System.out.println("generate_printout jsp page starts========>");
    //myself
    final String PRINTOUT_DIR_PATH = Constants.TESTING_MODE ? "/Users/rustyshelf/netbeans_workspace/physio_pdfs" : "DDDD:/My project/fiizio/fizzio/WebContent/images/";
    
   //  final String PRINTOUT_DIR_PATH = Constants.TESTING_MODE ? "/Users/rustyshelf/netbeans_workspace/physio_pdfs" : "E:/Images/fiizio/images/";
    
    final int NULL_PROGRAM_INSTANCE = -1;

    CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");
    User u = cu.getUser();
    Corporation corp = cu.getCorporation();
    ExercisePrintoutGenerator gen = new ExercisePrintoutGenerator();
    String wrkspce[] = {null};   //create buffer to hold filename
    int buildStatus[] = {NULL_PROGRAM_INSTANCE};
    ERR_T status = ERR_T.ERR_INVALID;
    String path = null;
    String error = null;
    
    System.out.println("generate_printout jsp page starts========>1");
    if (cu != null) {
        Corporation practice = cu.getCorporation();
        long id = Long.parseLong(request.getParameter("id")); //the program id
        LibraryManager lm = new LibraryManager();
        Program program = lm.getProgram(id);
        if (program != null) {
            Patient patient = lm.getPatient(program.getPatientId());
            PatientGroup group = lm.getPatientGroup(program.getGroupId());
            if(patient != null){
            	  System.out.println("generate_printout jsp page starts========>if");
            	
            	String type="Patients";
                String activity_page="corp/programs/inline/generate_printout.jsp";
            	String corp_id=String.valueOf(corp.getId());
            	String corp_name=corp.getName();
            	String corp_loc=corp.getSuburb();
            	String user_id=String.valueOf(u.getId());
            	String user_name=u.getFullName();
            	String invitor_id=String.valueOf(patient.getId());
            	String invitor_name=patient.getFullName();
            	String flag="printout";
            	String from="---";
            	String to="---";
            	String desc="Generate Program Printout for "+patient.getFullName()+" By User";
                try
                {		
                 Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
                }
                catch(Exception e)
                {
                }
        		
        	}else{
        		  System.out.println("generate_printout jsp page starts========>else");
        		String type="Patient Groups";
                String activity_page="corp/programs/inline/generate_printout.jsp";
            	String corp_id=String.valueOf(corp.getId());
            	String corp_name=corp.getName();
            	String corp_loc=corp.getSuburb();
            	String user_id=String.valueOf(u.getId());
            	String user_name=u.getFullName();
            	String invitor_id=String.valueOf(group.getId());
            	String invitor_name=group.getGroupName();
            	String flag="printout";
            	String from="---";
            	String to="---";
            	String desc="Generate group Program Printout "+group.getGroupName()+" By user";
                try
                {		
                 Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
                }
                catch(Exception e)
                {
                }
        	}
            List<ProgramExercise> exercises = lm.getProgramExercises(id);
            status = gen.generateExercisePrintout(practice, patient, group, program, exercises, false, PRINTOUT_DIR_PATH, wrkspce, buildStatus);
        }

        if (status == ERR_T.ERR_NONE) {
        	
        	  System.out.println("Error page in generate_printout jsp page starts========>");
            //created printout
            StringBuffer buffer = new StringBuffer("/printouts/");
            buffer.append(wrkspce[0]);
            path = buffer.toString();   //store the path to the printout
            System.out.println("hello"+path);
            response.sendRedirect(path);
            return;
        }
        else {
        	System.out.println("Success page in generate_printout jsp page starts========>");
            //failed to create printout
            error =
                    "Error creating exercise program printout (Err type = " + status
                    + ", status code = " + buildStatus[0] + ")";
        }
    }
    else {
    	
    	System.out.println("before close generate_printout jsp page starts========>");
        response.sendRedirect("/close.jsp");
        return;
    }
%>
<html>
    <head>
        <style>
            a:hover {background-color:red;}
        </style>
        <title>Exercise Program Printout
        </title>
    </head>
    <body>
        <%@include file="/WEB-INF/includes/formErrorSimple.jsp"%>
        <%
            if (status == ERR_T.ERR_NONE) {
        %>
        <div class="row">
            <div class="span9">
                <p>
                    Generating program printout. The printout will automatically appear once it 
                    is generated. If the printout does not appear then <a href=<%=path%> >click here</a> 
                    to view it.
                </p>
            </div>
        </div>
        <%
            }
        %>
    </body>
</html>
