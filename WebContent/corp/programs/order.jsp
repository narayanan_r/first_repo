<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="java.util.ArrayList"%>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.PreparedStatement" %>
<%@ page import="java.sql.*" %>
<%@	page import="java.util.ArrayList"%>
<%@	page import="java.util.Arrays"%>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.util.Date" %>
<%
// System.out.println("Staring from fiizio/corp/programs/order.jsp>>>  "+new Date());
    CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");
    if (cu == null) {
        response.sendRedirect(Util.loginUrl(Constants.LoginReason.login, request));
        return;
    }
	
    User u = cu.getUser();
    Corporation corp = cu.getCorporation();
   /*  try{
    	Util.allActivitiesLogs("Patients", u.getId(), corp.getName(), "Order Exercise");
    }catch(Exception e){
    	
    } */
    long id = Long.parseLong(Util.notNull(request.getParameter("id"), "0"));
    long pid = Long.parseLong(Util.notNull(request.getParameter("pid"), "0"));
    System.out.println("id" + id);
    System.out.println("pid" + pid);
    if (id == 0) {
    	if(pid > 0){
    		String[] Eids = request.getParameter("eidList").split(",");
    		int i = 1;
    	Connection conn = Util.getConnection();
        conn.setAutoCommit(false);
        
    	for(String Eid1 : Eids){
    		if(StringUtils.isNotEmpty(Eid1)){
		PreparedStatement pst = conn.prepareStatement("update program_exercises set seq = ? where id = ?");
		
        pst.setLong(1, i);
        pst.setLong(2, Long.parseLong(Eid1.toString()));
        pst.executeUpdate();
        System.out.println("pst swap" + pst);
        i = i+1;
    	}
    	}
    	conn.commit();
    	conn.close();
        response.sendRedirect("edit.jsp?id=" + pid);
        return;
    	}
    	        response.sendRedirect(corp.getHost() + "/close.jsp");
        return;
    }
    
    LibraryManager lm = new LibraryManager();
    
    Program p = lm.getProgram(id);
    Patient patient = lm.getPatient(p.getPatientId());

//     if (p == null || p.getCorpId() != corp.getId()) {
//         response.sendError(304);
//         return;
//     }

    long e1 = 0;
    long e2 = 0;
    try {
        e1 = Integer.parseInt(request.getParameter("e1"));
        e2 = Integer.parseInt(request.getParameter("e2"));
        System.out.println("Order e1"+e1);
        System.out.println("Order e2"+e2);
    } catch (Exception e) {
        // don't care
        e.printStackTrace();
    }

    if (e1 > 0 && e2 > 0) {
        lm.swapExercises(e1, e2);
    }
     String type="Patients";
    String activity_page="corp/programs/order.jsp";
	String corp_id=String.valueOf(corp.getId());
	String corp_name=corp.getName();
	String corp_loc=corp.getSuburb();
	String user_id=String.valueOf(u.getId());
	String user_name=u.getFullName();
	String invitor_id=String.valueOf(patient.getId());
	String invitor_name=patient.getFullName();
	String flag="swap";
	String from="Position:"+e1;
	String to="Position:"+e2;
	String desc="Order Exercise for "+patient.getFullName()+" By User";
     try
    {		
     Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
    }
    catch(Exception e)
    {
    }  

//    System.out.println("Ending  from fiizio/corp/programs/order.jsp>>>  "+new Date());
    response.sendRedirect("edit.jsp?id=" + id);
    return;
%>