<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.Corporation" %>
<%@ page import="au.com.ourbodycorp.model.CorporationUser" %>
<%@ page import="au.com.ourbodycorp.model.Program" %>
<%@ page import="au.com.ourbodycorp.model.User" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");

    User u = cu.getUser();
    Corporation corp = cu.getCorporation();

    long id = Long.parseLong(Util.notNull(request.getParameter("id"), "0"));
    long newPatient = Long.parseLong(Util.notNull(request.getParameter("patient"), "0"));
    long newGroup = Long.parseLong(Util.notNull(request.getParameter("group"), "0"));

    LibraryManager lm = new LibraryManager();
    Program p = lm.getProgram(id);

    if (p.getCorpId() != corp.getId()) {
        response.sendError(403);
        return;
    }

    long newId = lm.copyProgram(p.getId(), newPatient, newGroup, u.getId(), null);

    response.sendRedirect("edit.jsp?id=" + newId);
%>