<%@ page import="au.com.ourbodycorp.Config" %>
<%@ page import="au.com.ourbodycorp.model.Corporation" %>
<%@ page import="au.com.ourbodycorp.model.CorporationRole" %>
<%@ page import="au.com.ourbodycorp.model.Invitation" %>
<%@ page import="au.com.ourbodycorp.model.User" %>
<%@ page import="au.com.ourbodycorp.model.managers.CorporationManager" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.List" %>
<%@ page import="java.sql.*" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
  request.setAttribute("section", "people");
    Corporation corp = (Corporation) request.getAttribute("corp");
    User u = (User) session.getAttribute("user");
    
    String key = request.getParameter("key");

    if (key != null) {
    	Connection con = Util.getConnection();
        PreparedStatement pst = con.prepareStatement("delete from invitations where id= ?");
        pst.setString(1, key);
        pst.executeUpdate();
        pst.close();
        con.close();
        response.sendRedirect("/corp/people/");
        return;
        }
    if (u == null || corp == null) {
        response.sendRedirect(Config.getString("url"));
        return;
    }
    CorporationManager cm = new CorporationManager();
   
    String active = "pending";
    String[][] breadcrumbs = {{"/corp/", corp.getName()},{"/corp/people/Invitations", "Invitations"},{null, "Pending Invites"}};
    %>
<html>
<head><title>People</title></head>
<body>
<div class="page-header"><h1>Pending Invites</h1></div>
<%@include file="/WEB-INF/includes/breadcrumbs.jsp"%>
    <div class="row">
        <div class="span9 columns">
 <%@include file="tabs.jsp"%>
    
        <%
        List<String> lst= cm.pendingStatus(u.getEmail());
        if(!lst.isEmpty())
        {%>
        <table class="table table-zebra-striped">
        <thead>
        <th>InviterName</th>
        <th>CorporationName</th>
        <th>Email</th>
        <th>Action</th>
        </thead>
        <tbody>
        <%
        for(int i=0;i<lst.size();i++)
    	{	
        	if(i%5==0){
        %>
        <tr>
            <td><%=lst.get(i)+" "+lst.get(i+1)%></td>
            <td><%=lst.get(i+2)%></td>
            <td><%=lst.get(i+3)%></td>
            <td><a class="btn btn-info " href="/users/invite.jsp?key=<%=lst.get(i+4)%>">Accept</a>
           	<a class="btn btn-danger " href="/corp/people/pending.jsp?key=<%=lst.get(i+4)%>">Reject</a></td>
        </tr>
        <%
        	}
         }%>
         </tbody>
    </table> 	
        
        <%}else{
        %>
        <p><font size="5">There is No Pending Invitations.</font></p>
        <%}%>


   </div>
    </div>
    <div class="row">
        <div class="span9 columns"></div>
    </div>
</body>
</html>