<%@page import="au.com.ourbodycorp.model.managers.UserManager"%>
<%@page import="au.com.ourbodycorp.model.managers.LibraryManager"%>
<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%@ page import="au.com.ourbodycorp.model.managers.CorporationManager" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.LinkedList" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.io.BufferedReader" %>
<%@ page import="java.io.IOException" %>
<%@ page import="java.io.InputStreamReader" %>
<%@ page import="java.net.URL" %>
<%@ page import="java.net.URLConnection" %>
<%@ page import="org.json.JSONObject" %>
<%@ page import="sun.net.www.protocol.http.HttpURLConnection" %>
<%@ page import="java.net.URLEncoder" %>
<%@ page import="java.io.OutputStreamWriter" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="java.sql.*" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%!
    public boolean unitExists(List<Unit> units, long unitId) {
        for (Unit unit : units) {
            if (unit.getId() == unitId) {
                return true;
            }
        }
        return false;
    }
%>
<%
    boolean sent = false;
    String sentEmail = null;
    request.setAttribute("section", "people");
    CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");

    Corporation corp = cu.getCorporation();
    User u = cu.getUser();
   /*  try{
    	//Util.allActivitiesLogs("Team Members", u.getId(), corp.getName(), "Send Invitation");
    }catch(Exception e){
    	
    } */
    
    LibraryManager lm = new LibraryManager();
    int maxPractitioners = corp.getMaxPractitioners();
    int countOfPractitioners = lm.countOfPractitioners(corp.getId());
    boolean allowedToAddMore = (maxPractitioners == 0) || (countOfPractitioners < maxPractitioners);

    String error = null;
    String name = request.getParameter("name");
    String email = request.getParameter("email");
    String email2 = request.getParameter("email2");

    CorporationManager cm = new CorporationManager();
    Map<String,String> roleMap = cm.getRoleMap();
    List<Unit> units = cm.getUnitsForCorp(corp.getId());

    String[] postRoles = request.getParameterValues("role");

    HashMap<String,String> selectedRoles = new HashMap<String, String>();
    if (postRoles != null && postRoles.length > 0) {
        for (String postRole : postRoles) {
            selectedRoles.put(postRole, postRole);
        }
    }

    if (request.getMethod().equalsIgnoreCase("post")) {
        email = email.toLowerCase().trim();
        email2 = email2.toLowerCase().trim();
        name = name.trim();
        if (name.length() < 2) {
            error = "Please enter a name";
        } else if (!email.matches(email2)) {
            error = "Email addresses do not match";
        } else if (!email.matches("[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}")) {
            error = "Please enter a valid email address";
        } else if (selectedRoles.containsKey(CorporationRole.Role.Owner.getCode()) && selectedRoles.containsKey(CorporationRole.Role.Tenant.getCode())) {
            error = "Owner and tenant roles can not be selected at the same time. Please send separate invitations for each.";
        } else if (selectedRoles.size() == 0) {
            error = "Please select at least one role";
        }
        Connection con = Util.getConnection();
        PreparedStatement pst = con.prepareStatement("select acceptor from invitations where email= ? and corpid = ?");
        pst.setString(1, email);
        pst.setLong(2, corp.getId());
        ResultSet rs = pst.executeQuery();
        if(rs.next()){
        	error = email+ " already accepted your invitation.";
        }
        if (error == null) {
            Invitation inv = new Invitation();
            inv.setEmail(email);

            inv.setRoles(postRoles);
            inv.setInviter(u);
            inv.setName(name);
            inv.setCorporation(corp);
            
//             cm.save(inv);
//             cm.sendInvitation(inv);
          try
  	      {
            	/* int intIndex = -1;
          	  if(email.split("@")[1].toLowerCase().startsWith("yahoo")){
          		  URL url = new URL("http://my-addr.com/email/?mail="+email+"");
       	         URLConnection urlConnection = url.openConnection();
       	         HttpURLConnection connection = null;
       	         if(urlConnection instanceof HttpURLConnection)
       	         {
       	            connection = (HttpURLConnection) urlConnection;
       	         }
       	         else
       	         {
       	            System.out.println("Please enter an HTTP URL.");
       	            return;
       	         }

          	    BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
       	         String urlString = "";
       	         String current;
       	         while((current = in.readLine()) != null)
       	         {
       	            urlString += current;
       	         }
//        	         System.out.println(urlString);
       	         intIndex = urlString.indexOf("<b>e-mail exist</b>");
          	  }else{
          	 
          		  String data = URLEncoder.encode("email", "UTF-8") + "=" + URLEncoder.encode(""+email+"", "UTF-8");

              	    URL url = new URL("http://www.emailvalidator.co/EmailValidation/ProcessSingle");
              	    URLConnection conn = url.openConnection();
              	    conn.setDoOutput(true);
              	    OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
              	    wr.write(data);
              	    wr.flush();

              	    BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
              	    String line = rd.readLine();
              	    
        	  	   intIndex = line.indexOf("is a valid Email address");
          	  } */
      	   /* if(intIndex != - 1){ */
  	    	 cm.save(inv);
  	    	cm.sendInvitation(inv);
      	   UserManager um=new UserManager();
      	   User i=um.getUserByEmail(email);
  	    String type="Team Members ";
  	    String activity_page="corp/people/invite.jsp";
  		String corp_id=String.valueOf(corp.getId());
  		String corp_name=corp.getName();
  		String corp_loc=corp.getSuburb();
  		String user_id=String.valueOf(u.getId());
  		String user_name=u.getFullName();
  		String invitor_id="---";
  		String invitor_name="---";
  		String flag="invite";
  		String from="---";
  		String to="---";
  		String desc="Invite a new member "+email+" to the practice by User";
             
  	       /* }else{
  	    	 error = "This email address is not active. Please check the spelling or enter an alternative address.";
  	       } */

         try
         {
            Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
          }
         catch(Exception e)
           {
            }
//   	         System.out.println(urlString);
  	      }catch(IOException e)
  	      {
  	         e.printStackTrace();
  	      } 
         if(error == null){
            sentEmail = email;
            email = null;
            email2 = null;
            name = null;
            selectedRoles = new HashMap<String, String>();
            postRoles = null;
            sent = true;
         }
        }

    }

    LinkedList<HTML.CheckboxListItem> roleItems = new LinkedList<HTML.CheckboxListItem>();
    for (String key : roleMap.keySet()) {
        HTML.CheckboxListItem item = new HTML.CheckboxListItem(roleMap.get(key), key, "role", selectedRoles.containsKey(key));
        roleItems.add(item);
    }

    String[][] breadcrumbs = {{"/corp/", corp.getName()}, {"/corp/people/", "People"}, {null, "Send Invitation"}};

%>
<html>
<head><title>Send Invitation</title></head>
<body>
<div class="page-header">
    <h1>Send Invitation <small>Invite a new member to the practice.</small></h1>
</div>
<%@include file="/WEB-INF/includes/breadcrumbs.jsp"%>
<% if (allowedToAddMore) { %>
    <div class="row">
        <div class="span9 columns">
            <form action="invite.jsp" method="post" class="form-horizontal">
                <fieldset>
                    <%
                    if (sent) {
                %>
                <div class="alert alert-success">Invitation sent to <%=sentEmail%>.</div>
                <%
                    }
                %>
                    <%@include file="/WEB-INF/includes/formErrorSimple.jsp"%>
                    <%=HTML.textInput("name", "Name", "30", "50", name, null, null)%>
                    <%=HTML.textInput("email", "Email", "30", "50", email, null, null)%>
                    <%=HTML.textInput("email2", "Confirm Email", "30", "50", email2, null, null)%>

                   <%=HTML.checkboxList("Roles", roleItems, "Select all the roles this user will perform.")%>

                   </fieldset>
                <%=HTML.submitButton("Send Invitation")%>
            </form>
        </div>
    </div>
<% } else { %>
    <div class="span9">
        <div class="alert alert-error">
        <h3>Practitioner limit reached</h3>
        <p>
            Your practice has a limit of <%=maxPractitioners%> practitioners, which you have reached.
        </p>
        <p>
            Please contact support to increase your subscription or remove ones that are no longer in use.
        </p>
        </div>
    </div>
<% } %>
</body>
</html>