<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%@ page import="au.com.ourbodycorp.model.managers.CorporationManager" %>
<%@ page import="au.com.ourbodycorp.model.managers.UserManager" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.List" %>
<%
    CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");

    if (cu == null) {
        return;
    }
    Corporation corp = cu.getCorporation();
    User u = cu.getUser();

    boolean canEdit = cu.hasRole(CorporationRole.Role.Admin, CorporationRole.Role.Secretary);

    if (!canEdit) {
        return;
    }

    UserManager um = new UserManager();
    User du = null;
    try {
        du = um.getUser(Long.parseLong(request.getParameter("id")));
    } catch (NumberFormatException e) {
        // ignore, du will be null
    }

    if (du == null) {
        return;
    }

    CorporationManager cm = new CorporationManager();

    String delete = request.getParameter("delete");
    if (delete != null) {
        cm.deleteUnitUser(du.getId(), Long.parseLong(delete));
        response.sendRedirect("detail.jsp?id=" + du.getId());
        return;
    }

    List<Unit> units = cm.getAvailableUnits(corp.getId(), du.getId());

    String[] values = new String[units.size()];
    String[] options = new String[units.size()];
    int i = 0;
    for (Unit unit : units) {
        values[i] = Long.toString(unit.getId());
        options[i] = unit.getName();
        i++;
    }


    if (request.getMethod().equalsIgnoreCase("post")) {
        String newUnit = request.getParameter("unit");
        if (newUnit != null && newUnit.length() > 0) {
            UnitUser uu = new UnitUser();
            Unit unit = new Unit();
            unit.setId(Long.parseLong(newUnit));
            uu.setUnit(unit);
            uu.setType(UnitUser.Type.owner);
            uu.setUser(du);
            uu.setSince(new Date());
            cm.save(uu);
        }
        response.sendRedirect("/close.jsp?reload=true");
        return;
    }

%>
<html>
<body>
<div class="page-header"><h1>Add Unit</h1></div>
<div class="row">
    <div class="span6">

<form action="unit.jsp" method="post" class="form-horizontal">
    <input type="hidden" name="id" value="<%=du.getId()%>"/>
    <fieldset>
        <%=HTML.select("unit", "Add Unit", values, options, null, null, null, true)%>
    </fieldset>
    <%=HTML.submitButton("Add Unit")%>
</form>
        </div>
</div>
</body>
</html>
