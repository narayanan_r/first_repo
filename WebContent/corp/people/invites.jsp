<%@ page import="au.com.ourbodycorp.Config" %>
<%@ page import="au.com.ourbodycorp.model.Corporation" %>
<%@ page import="au.com.ourbodycorp.model.CorporationRole" %>
<%@ page import="au.com.ourbodycorp.model.Invitation" %>
<%@ page import="au.com.ourbodycorp.model.User" %>
<%@ page import="au.com.ourbodycorp.model.managers.CorporationManager" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.List" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    request.setAttribute("section", "people");
    Corporation corp = (Corporation) request.getAttribute("corp");
    List<CorporationRole> roles = (List<CorporationRole>) request.getAttribute("roles");
    User u = (User) session.getAttribute("user");
   /*  try{
    	Util.allActivitiesLogs("Team Members", u.getId(), corp.getName(), "Invitations");
    }catch(Exception e){
    	
    } */
    
    if (u == null || corp == null) {
        response.sendRedirect(Config.getString("url"));
        return;
    }
    CorporationManager cm = new CorporationManager();
    List<Invitation> invites = cm.getInvitations(corp.getId());
    String active = "invites";
    String type="Team Members";
    String activity_page="corp/people/invites.jsp";
    String corp_id=String.valueOf(corp.getId());
    String corp_name=corp.getName();
    String corp_loc=corp.getSuburb();
    String user_id=String.valueOf(u.getId());
    String user_name=u.getFullName();
    String invitor_id="";
    String invitor_name="---";
    String flag="invites";
    String from="---";
    String to="---";
    String desc="View Sent Invitations by User";
    try
   {
      Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
    }
   catch(Exception e)
{ 
} 
    

    SimpleDateFormat sdf = new SimpleDateFormat("d MMM yy hh:mm");
    sdf.setTimeZone(corp.getTimeZone());
    String[][] breadcrumbs = {{"/corp/", corp.getName()}, {"/corp/people/", "People"}, {null, "Invitations"}};
%>
<html>
<head><title>People</title></head>
<body>
<div class="page-header"><h1>Member Management</h1></div>
<%@include file="/WEB-INF/includes/breadcrumbs.jsp"%>
    <div class="row">
        <div class="span9 columns">


    <%@include file="tabs.jsp"%>
    <table class="table table-zebra-striped">
        <thead>
        <th>Name</th>
        <th>Email</th>
        <th>Sent</th>
        <th>Status</th>
        </thead>
        <tbody>
        <%
            for (Invitation invite : invites) {
                String status = "Sent";
                if (invite.getAccepted() != null) {
                    status = "<a href=\"detail.jsp?id=" + invite.getAcceptor() + "\">Accepted</a> " + sdf.format(invite.getAccepted());
                } else if (invite.getExpires().before(new Date())) {
                    status = "Expired";
                }
        %>
        <tr>
            <td><%=invite.getName()%></td>
            <td><%=invite.getEmail()%></td>
            <td><%=sdf.format(invite.getSent())%></td>
            <td><%=status%></td>
        </tr>
        <%
            }
        %>
        </tbody>
    </table>


                </div>
    </div>
    <div class="row">
        <div class="span9 columns"><a class="btn btn-primary" href="invite.jsp"><i class="icon-plus icon-white"></i> Invite more people</a></div>
    </div>
</body>
</html>