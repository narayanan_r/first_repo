<%@ page import="java.util.LinkedHashMap" %>
<%
    LinkedHashMap<String, String> items = new LinkedHashMap<String, String>();
    items.put("index", "Members");
    items.put("invites", "Invitations");
    items.put("pending", "Pending Invites");
   
    
%>

<ul class="nav nav-tabs">
    <%
        for (String item : items.keySet())
        {
    %>
    <li<%=(item.equalsIgnoreCase("active")) ? " class=\"active\"" : ""%>><a href="<%=item%>.jsp"><%=items.get(item)%></a></li>
    <%
        }
    %>
</ul>