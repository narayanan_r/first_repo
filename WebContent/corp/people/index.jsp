<%@page import="au.com.ourbodycorp.model.managers.UserManager"%>
<%@ page import="au.com.ourbodycorp.model.Corporation" %>
<%@ page import="au.com.ourbodycorp.model.CorporationRole" %>
<%@ page import="au.com.ourbodycorp.model.CorporationUser" %>
<%@ page import="au.com.ourbodycorp.model.User" %>
<%@ page import="au.com.ourbodycorp.model.managers.CorporationManager" %>
<%@ page import="java.util.List" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="java.sql.*" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    request.setAttribute("section", "people");
    Corporation corp = (Corporation) request.getAttribute("corp");
    List<CorporationRole> roles = (List<CorporationRole>) request.getAttribute("roles");
    User u = (User) session.getAttribute("user");
    String type="Team Members";
    String activity_page="corp/people/index.jsp";
	String corp_id=String.valueOf(corp.getId());
	String corp_name=corp.getName();
	String corp_loc=corp.getSuburb();
	String user_id=String.valueOf(u.getId());
	String user_name=u.getFullName();
	
     try{
    	//Util.allActivitiesLogs("Team Members", u.getId(), corp.getName(), "Member Management");
    	Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,"---","---","---","---","---","View Team members in corporation by User ");
    }catch(Exception e){
    	
    } 
    
    CorporationManager cm = new CorporationManager();
    List<CorporationUser> users = cm.getUsersForCorporation(corp.getId());
    String active = "index";
    boolean removedUser = false;
    String id2=null;
    String action = (String)request.getParameter("action");
    if (action != null && action.equalsIgnoreCase("delete")){
        String userId = (String)request.getParameter("id");
        Connection conn = Util.getConnection();
        PreparedStatement pst = conn.prepareStatement("select id from users where email='kumar.sathish345@gmail.com' or email='shagul001@gmail.com'");
        ResultSet rs = pst.executeQuery();
        if(rs.next())    
        {
        id2=rs.getString("id");
        }
        if(id2==userId)
        {
        	 System.out.println("You can't remove admin user of kumar.sathish345@gmail.com");
        }else
        {	long u2Id=Long.parseLong(userId);
        	UserManager um=new UserManager();
        	User u2 =um.getUser(u2Id);
        	String invitor_id=String.valueOf(u2.getId());
        	String invitor_name=u2.getFullName();
        	String flag="new";
        	String from="Delete Team Member: "+invitor_name+" from Corporation: "+corp_name;
        	String to="Name:"+invitor_name+";Email:"+u2.getEmail()+";Phone:"+u2.getPhone()+";Mobile:"+u2.getMobile();
        	String desc="Name:"+invitor_name+";Email:"+u2.getEmail()+";Phone:"+u2.getPhone()+";Mobile:"+u2.getMobile();
            try
            {
               Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
             }
            catch(Exception e)
              {
               }
        if (userId != null && userId.trim().length() > 0){
            int userIdInt = Integer.parseInt(userId);
            cm.removeUser(userIdInt, corp.getId());
            response.sendRedirect("index.jsp?action=deleted");
        }
        }
       
    }
    else if (action != null && action.equalsIgnoreCase("deleted")){
        removedUser = true;
    }

    String[][] breadcrumbs = {{"/corp/", corp.getName()}, {null, "People"}};
%>
<html>
<head><title>People</title></head>
<body>
<div class="page-header"><h1>Member Management</h1></div>
<%@include file="/WEB-INF/includes/breadcrumbs.jsp"%>

<% if (removedUser) { %>
    <div class="span9">
        <div class="alert alert-error">
            <p>Team member removed</p>
        </div>
    </div>
<% } %>

    <div class="row">
        <div class="span9 columns">


    <%@include file="tabs.jsp"%>
    <table class="table table-zebra-striped">
        <thead>
        <th>Name</th>
        <th>Roles</th>
        </thead>
        <tbody>
        <%
            for (CorporationUser cu : users) {
        %>
        <tr>
            <td><a href="detail.jsp?id=<%=cu.getUser().getId()%>"><%=cu.getUser().getFirstName()%> <%=cu.getUser().getLastName()%></a></td>
            <td>
            <%
                String roleString = "";
                for (CorporationRole role : cu.getRoles()) {
                    roleString += role.getName() + ", ";
                }
                int idx = roleString.lastIndexOf(",");
                if (idx > 0) {
                    roleString = roleString.substring(0, idx);
                }
                %>
                <%=roleString%>

            </td>
        </tr>
        <%
            }
        %>
        </tbody>
    </table>


                </div>
    </div>
    <div class="row">
        <div class="span9 columns"><a class="btn btn-primary" href="invite.jsp"><i class="icon-plus icon-white"></i> Invite more people</a></div>
    </div>
</body>
</html>