<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%@ page import="au.com.ourbodycorp.model.managers.CorporationManager" %>
<%@ page import="au.com.ourbodycorp.model.managers.UserManager" %>
<%@ page import="java.util.List" %>
<%@ page import="java.sql.*" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    request.setAttribute("section", "people");
    CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");

    Corporation corp = cu.getCorporation();
    User u = cu.getUser();
   
    /* try{
    	//Util.allActivitiesLogs("Team Members", u.getId(), corp.getName(), "Member Details");
    	
    }catch(Exception e){
    	
    } */
    
    boolean canEdit = cu.hasRole(CorporationRole.Role.Admin, CorporationRole.Role.Secretary);

    UserManager um = new UserManager();
    User du = null;
    try {
        du = um.getUser(Long.parseLong(request.getParameter("id")));
    } catch (NumberFormatException e) {
        // ignore, du will be null
    }

    if (du == null) {
        response.sendRedirect("/corp/people/");
        return;
    }

    boolean hasAdminRole = false;
    CorporationManager cm = new CorporationManager();
    String type="Team Members";
    String activity_page="corp/people/detail.jsp";
	String corp_id=String.valueOf(corp.getId());
	String corp_name=corp.getName();
	String corp_loc=corp.getSuburb();
	String user_id=String.valueOf(u.getId());
	String user_name=u.getFullName();
	String invitor_id=String.valueOf(du.getId());
	String invitor_name=du.getFullName();
	String flag="display";
	String from="---";
	String to="---";
	String desc="Displaying Member Detailsin User";
     try{
    	
    	Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
    }catch(Exception e){
    	
    } 

    List<CorporationRole> roles = cm.getRolesForUserInCorporation(du.getId(), corp.getId());
    if (roles == null || roles.size() == 0) {
        response.sendRedirect("/corp/people/");
        return;
    }

    List<UnitUser> units = cm.getUnitsForUser(du.getId(), corp.getId());

    boolean unitEnabled = false;

    if (canEdit) {
        List<Unit> available = cm.getAvailableUnits(corp.getId(), du.getId());
        unitEnabled = (available.size() > 0);
    }
    String[][] breadcrumbs = {{"/corp/", corp.getName()}, {"/corp/people/", "People"}, {null, du.getFullName()}};
    
    Connection conn = Util.getConnection();
    PreparedStatement pst = conn.prepareStatement("select email from company where id = ?");
    pst.setLong(1, corp.getCompanyId());
    ResultSet rs = pst.executeQuery();
    if(rs.next()){
    if(!u.getEmail().equals(rs.getString("email"))){
    	session.setAttribute("Visitor", "Visitor");
    }else{
    	session.setAttribute("Visitor", null);
    }
    }else{
    	session.setAttribute("Visitor", null);
    }
%>
<html>
<head>
    <title><%=du.getFirstName()%> <%=du.getLastName()%></title>
    <script>
		$(document).ready(function(){
			$(".addRole").colorbox();
		});

        function rmRole(id, role, code) {
          <%PreparedStatement pst1 = conn.prepareStatement("select id from users where email='kumar.sathish345@gmail.com' or email='shagul001@gmail.com'");
            ResultSet rs1 = pst1.executeQuery();
            if(rs1.next()){%>
            	 alert("You can't remove admin users");
            <%}else{%>
	            if (confirm("Are you sure you want to remove the role " + role + "?")) {
	                location.href='role.jsp?delete=' + code + '&id=' + id;
	            }
            <%}
            pst1.close();
            rs1.close();%>
        }
        
        function removeMember(memberId, isAdmin) {
            if (isAdmin){
                alert("You can't remove admin users. If you want to remove this team member, remove them from the Admin group first");
            }
            else if (confirm("Are you sure you want to remove this member?")) {
                location.href='index.jsp?action=delete&id=' + memberId;
            }
        }

	</script>
</head>
<body>
<div class="page-header"><h1>Member Details</h1></div>
<%@include file="/WEB-INF/includes/breadcrumbs.jsp"%>
<div class="row">
    <div class="span3 columns">
      <h3>Member Details</h3>
      <p>These are the contact details of the member as well as roles in the practice.</p>
      <p>Administrators will be able to make changes.</p>
    </div>
    <div class="span6 columns">


    <h3>Name</h3>
    <p><%=Util.notNull(du.getTitle())%> <%=du.getFullName()%></p>
    <h3>Email</h3>
    <p><a href="mailto:<%=du.getEmail()%>"><%=du.getEmail()%></a></p>
    <h3>Address</h3>
    <address>
        <%=Util.notNull(du.getAddress1())%><br/>
        <%=(du.getAddress2() != null && du.getAddress2().length() > 0) ? du.getAddress2() + "<br/>" : ""%>
        <%=Util.notNull(du.getSuburb())%>, <%=Util.notNull(du.getState())%> <%=Util.notNull(du.getPostcode())%><br/>
        <%=Util.notNull(du.getCountryName())%><br/>
    </address>
    <p>
        <b>Home:</b>  <%=Util.notNull(du.getPhone())%><br/>
        <b>Mobile:</b>  <%=Util.notNull(du.getMobile())%><br/>
    </p>
</div>
    </div>
<br>
<%-- <%if(session.getAttribute("Visitor") == null){ %> --%>
    <div class="row">
        <div class="span3 columns">
            <h4>Roles</h4>
            <ul>
                <%-- if(u.getEmail().equals(du.getEmail())){
                int i = 1;
                for (CorporationRole role : roles) {
                	i = 1+i;
                }
                    for (CorporationRole role : roles) {
                        %>
                <li><%=role.getName()%> <% if (canEdit) {if (!role.getName().equals("Admin")) {%><a href="javascript:rmRole(<%=du.getId()%>,'<%=role.getName()%>','<%=role.getId()%>');"> <i class="icon-remove-sign"></i></a><% }} %></li>
                <%
                        if (role.getName().equalsIgnoreCase("Admin")){
                            hasAdminRole = true;
                        }
                    }
                %>
            </ul>
            <% if (canEdit) {%>
                	<a class="btn btn-primary fancybox" href="role.jsp?id=<%=du.getId()%>">Add Role</a>
            <%}%>
            <ul>
            <%}else{ --%>
            <%
                    for (CorporationRole role : roles) {
                        %>
                <li><%=role.getName()%> <% if (canEdit) {%><a href="javascript:rmRole(<%=du.getId()%>,'<%=role.getName()%>','<%=role.getId()%>');"> <i class="icon-remove-sign"></i></a><% } %></li>
                <%
                        if (role.getName().equalsIgnoreCase("Admin")){
                            hasAdminRole = true;
                        }
                    }
                %>
            </ul>
            <% if (canEdit) {%>
                	<a class="btn btn-primary fancybox" href="role.jsp?id=<%=du.getId()%>">Add Role</a>
                	<a class="btn btn-danger" href="javascript:removeMember(<%=du.getId()%>, <%=hasAdminRole%>);">Delete This Member</a>
            <%}%>
        </div>

    </div>
<%-- <%} %> --%>
</body>
</html>