<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.model.Corporation" %>
<%@ page import="au.com.ourbodycorp.model.CorporationRole" %>
<%@ page import="au.com.ourbodycorp.model.CorporationUser" %>
<%@ page import="au.com.ourbodycorp.model.User" %>
<%@ page import="au.com.ourbodycorp.Config" %>
<%@ page import="au.com.ourbodycorp.model.managers.CorporationManager" %>
<%@ page import="au.com.ourbodycorp.model.managers.UserManager" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%
    CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");

    if (cu == null) {
        return;
    }
    Corporation corp = cu.getCorporation();
    User u = cu.getUser();
   /*  try{
    	Util.allActivitiesLogs("Team Members", u.getId(), corp.getName(), "Add Role");
    }catch(Exception e){
    	
    }
     */
    boolean canEdit = cu.hasRole(CorporationRole.Role.Admin, CorporationRole.Role.Secretary);

    if (!canEdit) {
    	if(Config.getString("https").equals("true")){
    		response.sendRedirect("https://"+request.getHeader("host")+"/corp/");
    	}else{
    		response.sendRedirect("http://"+request.getHeader("host")+"/corp/");
    	}
        return;
    }

    UserManager um = new UserManager();
    User du = null;
    try {
        du = um.getUser(Long.parseLong(request.getParameter("id")));
    } catch (NumberFormatException e) {
        // ignore, du will be null
    }

    if (du == null) {
        return;
    }

    CorporationManager cm = new CorporationManager();
    String type="Team Members";
    String activity_page="corp/people/role.jsp";
	String corp_id=String.valueOf(corp.getId());
	String corp_name=corp.getName();
	String corp_loc=corp.getSuburb();
	String user_id=String.valueOf(u.getId());
	String user_name=u.getFullName();
	String invitor_id=String.valueOf(du.getId());
	String invitor_name=du.getFullName();
    String delete = request.getParameter("delete");
    if (delete != null) {
    	String flag="delete";
    	String from="---";
    	String to="---";
    	String desc="Delete Role: '"+delete+"' to TeamMember: "+invitor_name+" By User";
        System.out.println(desc);
        try
        {
           Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
         }
        catch(Exception e)
          {
           }
        cm.removeRole(du.getId(), corp.getId(), delete);
        if(Config.getString("https").equals("true")){
    		response.sendRedirect("https://"+request.getHeader("host")+"/corp/people/detail.jsp?id=" + du.getId());
    	}else{
    		response.sendRedirect("http://"+request.getHeader("host")+"/corp/people/detail.jsp?id=" + du.getId());
    	}
        return;
    }

    List<CorporationRole> roles = cm.getRolesForUserInCorporation(du.getId(), corp.getId());
    if (roles == null || roles.size() == 0) {
        return;
    }

    Map<String, String> roleMap = cm.getRoleMap();
    for (CorporationRole role : roles) {
        roleMap.remove(role.getId());
    }
    String[] values = new String[roleMap.size()];
    String[] options = new String[roleMap.size()];
    int i = 0;
    for (String key : roleMap.keySet()) {
        values[i] = key;
        options[i] = roleMap.get(key);
        i++;
    }

    if (request.getMethod().equalsIgnoreCase("post")) {
        String newRole = request.getParameter("role");
        if (newRole != null && newRole.length() > 0) {
            cm.addRole(du.getId(),corp.getId(),newRole, 0);
        }
        
    	String flag="add";
    	String from="---";
    	String to="---";
    	String desc="Add Role as "+newRole+" to TeamMember "+invitor_name+" By User";
        System.out.println(desc);
        try
        {
           Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
         }
        catch(Exception e)
          {
           }

        
        if(Config.getString("https").equals("true")){
    		response.sendRedirect("https://"+request.getHeader("host")+"/close.jsp?reload=true");
    	}else{
    		response.sendRedirect("http://"+request.getHeader("host")+"/close.jsp?reload=true");
    	}
        return;
    }

%>
<html>
<body>
<div class="page-header"><h1>Add Role</h1></div>
<div class="row">
    <div class="span6">

<form action="role.jsp" method="post" class="form-horizontal">
    <input type="hidden" name="id" value="<%=du.getId()%>"/>
    <fieldset>
        <%=HTML.select("role", "Select Role", values, options, null, null, null, true)%>
    </fieldset>
    <%=HTML.submitButton("Add Role")%>
</form>
    </div>
</div>

</body>
</html>
