<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.Exercise" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="java.net.URLEncoder" %>
<%@ page import="au.com.ourbodycorp.model.Corporation" %>
<%@ page import="au.com.ourbodycorp.model.CorporationUser" %>
<%@	page import="java.util.ArrayList"%>
<%@ page import="java.util.List" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    long id = Long.parseLong(request.getParameter("id"));
    boolean confirm = (request.getParameter("confirm") != null && Boolean.parseBoolean(request.getParameter("confirm")));
    String submit = Util.notNull(request.getParameter("submit"));
    LibraryManager lm = new LibraryManager();
    List<Long> Eideditlist;
    Exercise e = lm.getExercise(id);

    if (e == null) {
        response.sendError(404);
        return;
    }
    CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");
    Corporation corp = cu.getCorporation();
    User u = cu.getUser();
   /*  try{
    	Util.allActivitiesLogs("My Exercises", u.getId(), corp.getName(), "Delete Exercise");
    }catch(Exception ee){
    	
    } */
    if (e.getCorpId() != corp.getId()) {
        response.sendError(403);
        return;
    }

    if (submit.equalsIgnoreCase("cancel")) {
        response.sendRedirect(corp.getHost() + "/close.jsp");
        return;
    } else if (confirm) {
    	String type="My Exercises";
	    String activity_page="corp/library/inline/delete.jsp";
		String corp_id=String.valueOf(corp.getId());
		String corp_name=corp.getName();
		String corp_loc=corp.getSuburb();
		String user_id=String.valueOf(u.getId());
		String user_name=u.getFullName();
		String invitor_id=String.valueOf(e.getId());
		String invitor_name="---";
		String flag="new";
		String from="Delete Exercise from Library";
	    String to="Exercise Name: "+e.getName()+";Reps:"+e.getReps()+";Rest:"+e.getRestTime()+";type:"+e.getType();
	    try{
	    	//Util.allActivitiesLogs("My Exercises", u.getId(), corp.getName(), "Delete Exercise");
        	Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,"Delete Exercise "+e.getName()+" from Library  By User");
        }catch(Exception ee){
        	
        }
    	Eideditlist = (ArrayList<Long>)session.getAttribute("Eidlist");
    	if(Eideditlist != null){
    	for(Long Eid : Eideditlist){
        lm.deleteExercise(Eid);
    	}
    	}
        response.sendRedirect(corp.getHost() + "/close.jsp?redir=" + URLEncoder.encode("/corp/library/", "UTF-8"));
        return;
    }

%>
<html>
<head><title>Delete Exercise</title></head>
<body>
<div class="page-header">
    <h1>Delete Exercise <small><%=Util.textAreaValue(e.getName())%></small></h1>
</div>
<div class="row">
    <div class="span6">
        <p>Are you sure you want to delete this exercise? It will be removed from any program it is used in.</p>
        <form action="delete.jsp" method="POST">
            <input type="hidden" name="id" value="<%=id%>">
            <input type="hidden" name="confirm" value="true">
            <%=HTML.confirmCancel()%>
        </form>
    </div>
</div>
</body>
</html>