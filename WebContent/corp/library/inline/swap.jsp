<%@page import="au.com.ourbodycorp.Util"%>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="au.com.ourbodycorp.model.ExerciseMedia" %>
<%@ page import="au.com.ourbodycorp.model.Corporation" %>
<%@ page import="au.com.ourbodycorp.model.CorporationUser" %>
<%@ page import="au.com.ourbodycorp.model.Exercise" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%
    LibraryManager lm = new LibraryManager();
    ExerciseMedia em1 = lm.getExerciseMedia(Long.parseLong(request.getParameter("id1")), false);
    ExerciseMedia em2 = lm.getExerciseMedia(Long.parseLong(request.getParameter("id2")), false);

    Exercise e1 = lm.getExercise(em1.getExercise());
    Exercise e2 = lm.getExercise(em2.getExercise());

    CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");
    Corporation corp = cu.getCorporation();
    User u = cu.getUser();
    /* try{
    	Util.allActivitiesLogs("My Exercises", u.getId(), corp.getName(), "Order Exercise Media");
    }catch(Exception ee){
    	
    } */
    if (e1.getCorpId() != corp.getId() || e2.getCorpId() != corp.getId()) {
        response.sendError(403);
        return;
    }
    String type="My Exercises";
    String activity_page="corp/library/inline/swap.jsp";
	String corp_id=String.valueOf(corp.getId());
	String corp_name=corp.getName();
	String corp_loc=corp.getSuburb();
	String user_id=String.valueOf(u.getId());
	String user_name=u.getFullName();
	String invitor_id="";
	String invitor_name="---";
	String flag="swap";
	String from="---";
    String to="---";
     try{
    	//Util.allActivitiesLogs("My Exercises", u.getId(), corp.getName(), "Order Exercise Media");
    	Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,"Order Exercise Media By User");
    }catch(Exception ee){
    
    } 
    lm.swapMedia(em1.getId(), em2.getId());
    Util.redirectTo("media.jsp?id=" + request.getParameter("id"), request, response);
%>