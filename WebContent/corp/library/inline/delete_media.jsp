<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page import="java.util.ArrayList"%>
<%@ page import="java.util.List" %>
<%@ page import="java.sql.*" %>
<%

	String captionname = null;
    long id = Long.parseLong(request.getParameter("id"));
    long seq = Long.parseLong(request.getParameter("seq"));
    LibraryManager lm = new LibraryManager();
    ExerciseMedia oem1 = lm.getExerciseMedia(id, false);
    String capname = oem1.getCaption();
	if(capname != null){
		session.setAttribute("captionname", capname);
	}
		captionname = session.getAttribute("captionname").toString();
    boolean confirm = (request.getParameter("confirm") != null && Boolean.parseBoolean(request.getParameter("confirm")));
    String submit = Util.notNull(request.getParameter("submit"));
    ExerciseMedia em = lm.getExerciseMediaDelByLoc(id,captionname, false);
    Exercise e = lm.getExercise(em.getExercise());
    CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");
    Corporation corp = cu.getCorporation();
    User u = cu.getUser();
    /* try{
    	Util.allActivitiesLogs("My Exercises", u.getId(), corp.getName(), "Delete Exercise Media");
    }catch(Exception ee){
    	
    } */
    if (e.getCorpId() != corp.getId()) {
        response.sendError(403);
        return;
    }

    if (submit.equalsIgnoreCase("cancel")) {
        Util.redirectTo("media.jsp?id=" + e.getId(), request, response);
        return;
    } else if (confirm) {
    	    String type="My Exercises";
    	    String activity_page="corp/library/inline/delete_media.jsp";
    		String corp_id=String.valueOf(corp.getId());
    		String corp_name=corp.getName();
    		String corp_loc=corp.getSuburb();
    		String user_id=String.valueOf(u.getId());
    		String user_name=u.getFullName();
    		String invitor_id=String.valueOf(oem1.getId());
    		String invitor_name="---";
    		String flag="delete";
    		String from="---";
    	    String to="---";
    	    try{
    	    	//Util.allActivitiesLogs("My Exercises", u.getId(), corp.getName(), "Delete Exercise Media");
            	Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,"Delete ExerciseMedia from Exercise: "+e.getName()+" By User");
            }catch(Exception ee){
            	
            }
    	List<Long> Eidlist;
        Eidlist = (ArrayList<Long>)session.getAttribute("Eidlist");
        try{
            for (Long Eid : Eidlist) {
            	Connection conn = Util.getConnection();
            	PreparedStatement pst1 = conn.prepareStatement("select id from exercise_media where exercise = ? and caption = ? and seq = ?");
            	pst1.setLong(1,Eid);
            	pst1.setString(2, captionname);
            	pst1.setLong(3,seq);
                ResultSet rs = pst1.executeQuery();
                while (rs.next()) {
       			 lm.deleteExerciseMedia(rs.getLong("id"));
                }
            pst1.close();
            conn.close();    
            }
        }catch(Exception ee){
            ee.printStackTrace();
            }
        Util.redirectTo("media.jsp?id=" + e.getId(), request, response);
        return;
    }

    String url = "/images/movie_150_100.gif";
    if (em.getType().equalsIgnoreCase("photo")) {
        url = "/media?id=" + em.getId() + "&width=150";
    }
%>
<html>
<head><title>Delete Media</title></head>
<body>
<div class="page-header">
    <h1>Delete Media <small><%=Util.textAreaValue(e.getName())%></small></h1>
</div>
<div class="row">
    <div class="span6">
        <h2>Confirm delete of media</h2>
        <img src="<%=url%>" width="150">
        <form action="delete_media.jsp" method="POST">
            <input type="hidden" name="id" value="<%=id%>">
            <input type="hidden" name="seq" value="<%=seq%>">
            <input type="hidden" name="confirm" value="true">
            <%=HTML.confirmCancel()%>
        </form>
    </div>
</div>
</body>
</html>