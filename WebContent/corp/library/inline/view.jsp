<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%

    CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");
	User u = cu.getUser();
	Corporation corp = cu.getCorporation();
   /* 
	 try{
	    	Util.allActivitiesLogs("My Exercises", u.getId(), corp.getName(), "View Exercise Media Image");
	    }catch(Exception ee){
	    	
	    } */
    long id = Long.parseLong(request.getParameter("id"));
    long media_id = Long.parseLong(request.getParameter("media_id"));
    boolean edit = (request.getParameter("edit") != null) && Boolean.parseBoolean(request.getParameter("edit"));
    boolean isDisabled = (request.getParameter("isDisabled") != null) && Boolean.parseBoolean(request.getParameter("isDisabled"));
    LibraryManager lm = new LibraryManager();
    ExerciseMedia em = lm.getExerciseMedia(id,false);
    if (em == null) {
        response.sendError(404);
        return;
    }
    Exercise e = lm.getExercise(em.getExercise());
    if (Long.parseLong(session.getAttribute("precorpId").toString()) != 0 && Long.parseLong(session.getAttribute("precorpId").toString()) != corp.getId()) {
        response.sendError(403);
        return;
    }
    String type="My Exercises";
    String activity_page="corp/library/inline/view.jsp";
	String corp_id=String.valueOf(corp.getId());
	String corp_name=corp.getName();
	String corp_loc=corp.getSuburb();
	String user_id=String.valueOf(u.getId());
	String user_name=u.getFullName();
	String invitor_id=String.valueOf(e.getId());
	String invitor_name="---";
	String flag="view";
	String from="---";
    String to="---";
     try{
    	//Util.allActivitiesLogs("My Exercises", u.getId(), corp.getName(), "View Exercise Media Image");
    	Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,"View Exercise Media Image By User");
    }catch(Exception ee){
    
    } 

%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head><title>Preview</title></head>
<body>
<div class="page-header">
    <h1>Preview</h1>
</div>
<div class="row">
    <div class="span6">
        <% if (em.getType().equals("photo")) {
        %>
        <a href="/media?id=<%=id%>" target="_blank"><img src="/media?id=<%=id%>&width=480&v=<%=em.getVersion()%>"></a>
        <%
        } else {
        %>
        <video width="480" height="300" controls>
            <source src="/media?id=<%=id%>">
        </video>
        <%
            }
        %>
    </div>
</div>
<div class="row">
    <div class="span6">
        <p><%=Util.textAreaValue(em.getCaption())%></p>
    </div>
</div>
 <%if(!isDisabled){%>
<div class="row">
    <div class="span6">
        <div class="form-actions">
            <% if (edit) {
            %>
            <a href="add_media.jsp?id=<%=id%>&media=<%=em.getExercise()%>&close=true&name=<%=em.getCaption() %>" class="btn">Edit</a>
            <a href="<%=corp.getHost()%>/close.jsp" class="btn btn-primary">Close</a>
            <%
            } else if (Long.parseLong(session.getAttribute("precorpId").toString()) == corp.getId()) {
            %>
           
            <a href="media.jsp?id=<%=media_id%>" class="btn btn-primary">Back</a>
            <%
            }%>

          </div>
    </div>
</div>
<%} %>
</body>
</html>