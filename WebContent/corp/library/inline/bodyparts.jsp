<%@page import="java.util.ArrayList"%>
<%@ page import="java.util.List" %>
<%@ page import="java.sql.*" %>
<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.Exercise" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.PreparedStatement" %>
<%@ page import="au.com.ourbodycorp.model.Corporation" %>
<%@ page import="au.com.ourbodycorp.model.CorporationUser" %>
<%@ page import="au.com.ourbodycorp.model.*"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    long id = Long.parseLong(request.getParameter("id"));
    LibraryManager lm = new LibraryManager();
    Exercise e = lm.getExercise(id);
    if (e == null) {
    	response.sendError(404, "Page not found");
        return;
    }
    List<Long> Eidlist;
    Eidlist = (ArrayList<Long>)session.getAttribute("Eidlist");
    CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");
    Corporation corp = cu.getCorporation();
    User u = cu.getUser();
  /*   try{
    	Util.allActivitiesLogs("My Exercises", u.getId(), corp.getName(), "Add Exercise Types");
    }catch(Exception ee){
    	
    } */
    if (e.getCorpId() != corp.getId()) {
        response.sendError(403);
        return;
    }

    String error = null;
    if (request.getMethod().equalsIgnoreCase("post")) {
        if (request.getParameter("submit").equals("Cancel")) {
            response.sendRedirect(corp.getHost() + "/close.jsp");
            return;
        }

        if (request.getParameterValues("bp") == null || request.getParameterValues("type") == null) {
            error = "select at least 1 body port and 1 type";
        } else {
            String activity_page="corp/library/inline/bodyparts.jsp";
        	String corp_id=String.valueOf(corp.getId());
        	String corp_name=corp.getName();
        	String corp_loc=corp.getSuburb();
        	String user_id=String.valueOf(u.getId());
        	String user_name=u.getFullName();
        	String invitor_id=String.valueOf(e.getId());
        	String invitor_name="---";
        	String flag="add";
        	String from="---";
            String to="---";
            try{
            	//Util.allActivitiesLogs("My Exercises", u.getId(), corp.getName(), "Add Exercise Types");
            	Util.allActivitiesLogs2("My Exercises",activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,"Add BodyPart Exercise Types into Exercise: "+e.getName()+" By User");
            }catch(Exception ee){
            	
            }

            Connection conn = Util.getConnection();
            try {
                conn.setAutoCommit(false);
                PreparedStatement pst = conn.prepareStatement("delete from bodyparts where exercise = ?");
                try{
                for (Long Eid : Eidlist) {
                	PreparedStatement pst1 = conn.prepareStatement("select exercise from bodyparts where exercise = ?");
                	pst1.setLong(1, Eid);
                    ResultSet rs = pst1.executeQuery();
                    while (rs.next()) {
                pst.setLong(1, rs.getLong("exercise"));
                pst.executeUpdate();
                    }
                pst1.close();
                }}catch(Exception ee){
                ee.printStackTrace();
                }
                pst.close();

                pst = conn.prepareStatement("delete from exercise_types where exercise = ?");
                try{
                for (Long Eid : Eidlist) {
                	PreparedStatement pst1 = conn.prepareStatement("select exercise from exercise_types where exercise = ?");
                	pst1.setLong(1, Eid);
                    ResultSet rs = pst1.executeQuery();
                    while (rs.next()) {
                pst.setLong(1, rs.getLong("exercise"));
                pst.executeUpdate();
                    }
                pst1.close();
                }}catch(Exception ee){
                    ee.printStackTrace();
                    }
                pst.close();

                pst = conn.prepareStatement("insert into bodyparts (exercise, bodypart) values (?,?)");
                for (String bp : request.getParameterValues("bp")) {
                    try {
                        Exercise.BodyPart b = Exercise.BodyPart.valueOf(bp);
                    } catch (IllegalArgumentException ex) {
                        continue;
                    }
                    try{
                    for (Long Eid : Eidlist) {
                    pst.setLong(1, Eid);
                    pst.setString(2, bp);
                    pst.execute();
                    }
                    }catch(Exception ee){
                    	ee.printStackTrace();
                    }
                }
                pst.close();

                pst = conn.prepareStatement("insert into exercise_types (exercise, type) values (?,?)");
                for (String type : request.getParameterValues("type")) {
                    try {
                        Exercise.Type t = Exercise.Type.valueOf(type);
                    } catch (IllegalArgumentException ex) {
                        continue;
                    }
                    try{
                    for (Long Eid : Eidlist) {
                    pst.setLong(1, Eid);
                    pst.setString(2, type);
                    pst.execute();
                    }}catch(Exception ee){
                    	ee.printStackTrace();
                    }
                }
                pst.close();

                conn.commit();
// 				System.out.println("host"+corp.getHost()+"/corp/library/exercise.jsp?p?reload=true");
                response.sendRedirect(corp.getHost()+"/close.jsp?reload=true");
            } finally {
                conn.setAutoCommit(true);
                conn.close();
            }
        }
    }

%>
<html>
<head><title>Select body parts and exercise types</title></head>
<body>
<div class="page-header"><h1>Body parts and exercise types</h1></div>
<%@include file="/WEB-INF/includes/formErrorSimple.jsp"%>

<div class="row">
    <form action="bodyparts.jsp" method="post">

    <input type="hidden" name="id" value="<%=id%>">

        <div class="span3">
            <h2>Body Parts</h2>
            <%
                for (Exercise.BodyPart bp : Exercise.BodyPart.values()) {
            %>
                <label class="checkbox">
                <input type="checkbox" name="bp" value="<%=bp.name()%>" <%=e.getBodyParts().contains(bp) ? "checked" : ""%>> <%=bp.getValue()%><br>
                </label>
            <%
                }
            %>
        </div>
        <div class="span3">
            <h2>Exercise Types</h2>
            <%
                for (Exercise.Type type : Exercise.Type.values()) {
            %>
                <label class="checkbox">
                <input type="checkbox" name="type" value="<%=type.name()%>" <%=e.getTypes().contains(type) ? "checked" : ""%>> <%=type.getValue()%><br>
                </label>
            <%
                }
            %>
        </div>
</div>
    <div class="row">
        <div class="span6">
            <%=HTML.saveCancel()%>
        </div>
           </div>
</form>

</body>
</html>