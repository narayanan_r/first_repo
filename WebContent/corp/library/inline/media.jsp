<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.List" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");

    User u = cu.getUser();
    Corporation corp = cu.getCorporation();
    /* try{
    	Util.allActivitiesLogs("My Exercises", u.getId(), corp.getName(), "View Exercise Media");
    }catch(Exception ee){
    	
    } */
    long id = Long.parseLong(request.getParameter("id"));
    String captionname = request.getParameter("captionname");
    long CorpId = Long.parseLong(session.getAttribute("precorpId").toString());
    LibraryManager lm = new LibraryManager();
    Exercise e = lm.getExercise(id);
    if (e == null) {
    	response.sendError(404, "Page not found");
        return;
    }
    if (CorpId != corp.getId()) {
        response.sendError(403);
        return;
    } 
    
    if (e == null) {
        Util.redirectTo("media.jsp?id=" + Long.parseLong(session.getAttribute("preId").toString()), request, response);
        return;
    }
    String type="My Exercises";
    String activity_page="corp/library/inline/media.jsp";
	String corp_id=String.valueOf(corp.getId());
	String corp_name=corp.getName();
	String corp_loc=corp.getSuburb();
	String user_id=String.valueOf(u.getId());
	String user_name=u.getFullName();
	String invitor_id=String.valueOf(e.getId());
	String invitor_name="---";
	String flag="view";
	String from="---";
    String to="---";
     try{
    	//Util.allActivitiesLogs("My Exercises", u.getId(), corp.getName(), "View Exercise Media");
    	Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,"View Exercise Media By User");
    }catch(Exception ee){
    
    } 
    List<ExerciseMedia> em = lm.getExerciseMedia(id);
    HashMap<Long, Long> next = new HashMap<Long, Long>();
    long prev = 0;
    for (ExerciseMedia media : em) {
        next.put(prev, media.getId());
        prev = media.getId();
    }
%>
<html>
<head><title>Media</title></head>
<body>
<div class="page-header">
    <h1>Media <small><%=e.getName()%></small></h1>
</div>
<div class="row">
    <div class="span6">
        <a href="add_media.jsp?id=<%=id%>" class="btn btn-primary">Add Media</a>
    </div>
</div>
<div class="row" style="margin-top:15px;">
    <div class="span6">
        <table class="table table-zebra-striped">
            <%
                prev = 0;
                for (ExerciseMedia media : em) {
                    String name = media.getCaption();
                    String captionName = media.getCaption();
                    if (name == null || name.length() == 0) {
                        name = media.getName();
                        captionName = "";
                    }
                    if (name == null || name.length() == 0) {
                        name = media.getType();
                        captionName = "";
                    }
                    String preview;
                    if (media.getType().equals("video")) {
                        preview = "/images/video_75_56.gif";
                    } else {
                        preview = "/media?id=" + media.getId() + "&width=75&v=" + media.getVersion();
                    }
            %>
            <tr>
                <td><%=media.getSeq()%></td>
                <%--
                <td>
                    <% if (media.getType().equals("photo")) {%><a href="/media?id=<%=media.getId()%>" target="_blank"><img src="/media?id=<%=media.getId()%>" width="75"></a><%
                } else {
                    %>
                    <a href="/media?id=<%=media.getId()%>" target="_blank">Download</a>
                    <%
                }
                %>
                </td>

                <td><a href="/media?id=<%=media.getId()%>" target="_blank"><i class="<%=(media.getType().equals("video")) ? "icon-film" : "icon-picture"%>"></i></a> <a href="delete_media.jsp?id=<%=media.getId()%>" ><i class="icon-trash"></i></a> <a href="add_media.jsp?media=<%=media.getId()%>&id=<%=id%>"><%=Util.textAreaValue(name)%></a><% if (media.isKey()) {%> <i class="icon-ok"></i><% } %></td>
                --%>
                <td style="width:85px;"><a href="view.jsp?id=<%=media.getId()%>&media_id=<%=id%>"><img src="<%=preview%>" width="75"></a></td>
                <td><a href="delete_media.jsp?id=<%=media.getId()%>&name=<%=name%>&seq=<%=media.getSeq()%>" ><i class="icon-trash"></i></a></td>
                    <td style="width:270px;"><a href="add_media.jsp?media=<%=id%>&id=<%=media.getId()%>&name=<%=captionName%>"><%=Util.textAreaValue(name)%></a><% if (media.isKey()) {%> <i class="icon-ok"></i><% } %></td>
                <td>
                    <a href="swap.jsp?id=<%=id%>&id1=<%=media.getId()%>&id2=<%=prev%>"><i class="icon-arrow-up<%=(prev == 0) ? " icon-white" : ""%>"></i></a>
                    <% if (next.get(media.getId()) != null) {%>
                    <a href="swap.jsp?id=<%=id%>&id1=<%=media.getId()%>&id2=<%=next.get(media.getId())%>"><i class="icon-arrow-down"></i></a>
                    <% } %>

                </td>
                <td>

                </td>
            </tr>
            <%
                    prev = media.getId();
                }
            %>
        </table>
    </div>
</div>


</body>
</html>