<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.Exercise" %>
<%@ page import="au.com.ourbodycorp.model.ExerciseMedia" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="org.apache.commons.fileupload.FileItem" %>
<%@ page import="org.apache.commons.fileupload.FileItemFactory" %>
<%@ page import="org.apache.commons.fileupload.disk.DiskFileItemFactory" %>
<%@ page import="org.apache.commons.fileupload.servlet.ServletFileUpload" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.List" %>
<%@ page import="au.com.ourbodycorp.model.Corporation" %>
<%@ page import="au.com.ourbodycorp.model.CorporationUser" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page import="java.util.ArrayList"%>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%
	String captionname = null;
    long id = Long.parseLong(request.getParameter("id"));
    List<Long> Eidlist;
    Eidlist = (ArrayList<Long>)session.getAttribute("Eidlist");
    long media = Long.parseLong(Util.notNull(request.getParameter("media"),"0"));
    boolean key = false;
    boolean close = (request.getParameter("close") != null) && Boolean.parseBoolean(request.getParameter("close"));
    String capname = request.getParameter("name");
	if(capname != null){
		session.setAttribute("captionname", capname);
		captionname = session.getAttribute("captionname").toString();
	}
    LibraryManager lm = new LibraryManager();
    Exercise e = lm.getExercise(Long.parseLong(session.getAttribute("preId").toString()));
    if (e == null) {
    	response.sendError(404, "Page not found");
        return;
    }
    CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");
    Corporation corp = cu.getCorporation();
    User u = cu.getUser();
    if (e.getCorpId() != corp.getId()) {
        response.sendError(403);
        return;
    } 

    ExerciseMedia oem = null;
    ExerciseMedia oem1 = null;
    String title = "Add";
    if (media > 0) {
    	try{
        	//Util.allActivitiesLogs("My Exercises", u.getId(), corp.getName(), "Edit Exercise Media");
        }catch(Exception ee){
        	
        }
        title = "Edit";
        oem1 = lm.getExerciseMedia(id, false);
    }else{
    	try{
        	//Util.allActivitiesLogs("My Exercises", u.getId(), corp.getName(), "Add Exercise Media");
        }catch(Exception ee){
        	
        }
    }

    String name = (oem1 != null) ? oem1.getName() : "";
    String caption = (oem1 != null) ? oem1.getCaption() : "";
    HashMap<String, String> errorFields = new HashMap<String, String>();


    FileItem file = null;
    
    session.setAttribute("filenotselected", null);
    session.setAttribute("fileformaterror", null);
    
    if (request.getMethod().equalsIgnoreCase("post")) {
        HashMap<String, String> fields = new HashMap<String, String>();
        FileItemFactory factory = new DiskFileItemFactory();
        ServletFileUpload upload = new ServletFileUpload(factory);
        List<FileItem> items = upload.parseRequest(request);
        for (FileItem item : items) {
            if (item.isFormField()) {
                fields.put(item.getFieldName(), item.getString());
            } else {
                if (item.getName() != null && item.getName().trim().length() > 0)
                    file = item;
            }
        }

        key = fields.get("key") != null && Boolean.parseBoolean(fields.get("key"));

        if (fields.get("submit") != null && fields.get("submit").contains("Cancel")) {
            Util.redirectTo("media.jsp?id=" + Long.parseLong(session.getAttribute("preId").toString()), request, response);
            return;
        }

        name = fields.get("name");
        caption = fields.get("caption");

        if (oem1 == null && (file == null || file.getSize() == 0)) {
            errorFields.put("file", "No file selected");
            session.setAttribute("filenotselected", "No file selected");
        } else if (file != null && !file.getName().toLowerCase().endsWith(".m4v") && !file.getName().toLowerCase().endsWith(".jpg") && !file.getName().toLowerCase().endsWith(".jpeg")) {
            errorFields.put("file", "Only .m4v and .jpg files allowed");
            session.setAttribute("filenotselected", null);
            session.setAttribute("fileformaterror", "Only .m4v and .jpg files allowed");
        } else if (caption == null || caption.trim().length() == 0) {
            errorFields.put("caption", "Enter a caption");
            session.setAttribute("filenotselected", null);
            session.setAttribute("fileformaterror", null);
        }

        if (errorFields.size() == 0) {
        	session.setAttribute("filenotselected", null);
            session.setAttribute("fileformaterror", null);
            ExerciseMedia em;
		try{
            for (Long Eid : Eidlist) {
            if (oem1 == null) {
                em = new ExerciseMedia();
                em.setSeq(lm.nextExerciseMediaSequence(Eid));
            } else {
                em = lm.getExerciseMediaByLoc(Eid,captionname, false);
            }
            em.setExercise(Eid);

            if (file != null) {
                em.setType((file.getName().endsWith(".m4v")) ? "video" : "photo");
                em.setData(file.get());
                em.setVersion(em.getVersion() + 1);
            }
            em.setCaption(caption);
            em.setName(name);

            lm.save(em);
            
            if (key) {
                lm.setKeyPhoto(Eid, em.getId());
            }
            } 
            if(title == "Edit")
            {
            	String type="My Exercises";
    		    String activity_page="corp/library/inline/add_media.jsp";
    			String corp_id=String.valueOf(corp.getId());
    			String corp_name=corp.getName();
    			String corp_loc=corp.getSuburb();
    			String user_id=String.valueOf(u.getId());
    			String user_name=u.getFullName();
    			String invitor_name="---";
    			String flag="EDIT";
    			String from="---";
    		    String to="---";
    		    try{
                	Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,String.valueOf(oem1.getId()),invitor_name,flag,from,to,"Edit Exercise Media into Exercise: "+e.getName()+" By User");
                }catch(Exception ee){
                	
                }
            }
            }
       catch(Exception ee){
        	ee.printStackTrace();
        }
            if (close) {
                Util.redirectTo("view.jsp?edit=true&id=" + id+"&media_id="+id, request, response);
            } else {
                Util.redirectTo("media.jsp?id=" + session.getAttribute("preId"), request, response);
            }
            return;
        }
    } else {
        if (oem1 != null) {
            key = oem1.isKey();
        }
    }

%>
<html>
<head><title><%=title%> Media</title>
<script type="text/javascript">
function isCheckEmpty() {
	var capnameclass = document.getElementById("capname");
	var filenameclass = document.getElementById("filename");
	var capname = $("#caption").val();
	var filename = $("#fileInput").val().split('.').pop();
	var tittle = "<%=title%>";
	if(filename == "" && tittle == "Add"){
		filenameclass.className="control-group error";
		$("#nofile_error").show();
		$("#file_error").hide();
		return false;
	}/* else if(filename != "jpg"){
		filenameclass.className="control-group";
		$("#nofile_error").hide();
		filenameclass.className="control-group error";
		$("#file_error").show();
		return false;
	} */else if(capname == ""){
		filenameclass.className="control-group";
		$("#nofile_error").hide();
		filenameclass.className="control-group";
		$("#file_error").hide();
		capnameclass.className="control-group error";
		$("#cap_error").show();
		return false;
	}else{
		filenameclass.className="control-group";
		$("#nofile_error").hide();
		filenameclass.className="control-group";
		$("#file_error").hide();
		capnameclass.className="control-group";
		$("#cap_error").hide();
		return true;
	}
}
function goBack() {
	window.location="media.jsp?id=<%=Long.parseLong(session.getAttribute("preId").toString())%>";
}
</script>
</head>
<body>
<div class="page-header">
    <h1><%=title%> Media <small><%=e.getName()%></small></h1>
</div>
<div class="row">
    <div class="span6">
        <form action="add_media.jsp?id=<%=id%>&media=<%=media%>&close=<%=close%>&name=<%=capname%>" name="addmedia" id="addmedia" method="post" enctype="multipart/form-data" class="form-horizontal" onsubmit="return isCheckEmpty();">
		  <%if(session.getAttribute("filenotselected") != null){ %>
          <div class="control-group error" id="filename">
			<label class="control-label" for="fileInput">File</label>
			<div class="controls">
			<input id="fileInput" class="input-file" type="file" name="file" accept=".jpg,.m4v">
			<small>.jpg or .m4v only</small>
			<span class="help-inline" id="nofile_error" style="display: none;">Please select a .jpg or .m4v file</span>
			</div>
		  </div>
          <%}else if(session.getAttribute("fileformaterror") != null){ %>
          <div class="control-group error" id="filename">
			<label class="control-label" for="fileInput">File</label>
			<div class="controls">
			<input id="fileInput" class="input-file" type="file" name="file" accept=".jpg,.m4v">
			<small>.jpg or .m4v only</small>
			<span class="help-inline" id="nofile_error" style="display: none;">Please select a .jpg or .m4v file</span>
			<span class="help-inline" id="file_error">Only .m4v and .jpg files allowed</span>
			</div>
		 </div>
		 <%}else{ %>
		 <div class="control-group" id="filename">
            <label class="control-label" for="fileInput">File</label>
            <div class="controls">
              <input class="input-file" name="file" id="fileInput" type="file" accept=".jpg,.m4v">
                <small>.jpg or .m4v only</small>
                <span class="help-inline" id="nofile_error" style="display: none;">Please select a .jpg or .m4v file</span>
                <span class="help-inline" id="file_error" style="display: none;">Only .m4v and .jpg files allowed</span>
            </div>
          </div>
          <%} %>
            <%--=HTML.textInput("name", "Name", "30", "30", name, null, errorFields)--%>
<%--             <%=HTML.textInput("caption", "Caption", "2000", "2000", caption, null, errorFields)%> --%>
			<div class="control-group" id="capname">
				<label class="control-label" for="caption">Caption</label>
			<div class="controls">
				<input id="caption" class="input-xlarge" type="text" value="<%=caption%>" name="caption" maxlength="2000" size="2000">
				<span class="help-inline" id="cap_error" style="display: none;">Please enter a caption</span>
			</div>
			</div>
            <div class="control-group">
            <label class="control-label" for="optionsCheckbox">Key Photo</label>
            <div class="controls">
              <label class="checkbox">
                <input type="checkbox" id="optionsCheckbox" name="key" value="true" <%=(key) ? "checked" : ""%>>
                Set this to be the key photo
              </label>
            </div>
          </div>
<%--             <%=HTML.saveCancel()%> --%>
		<div class="form-actions">
			<button class="btn btn-primary" value="Save" type="submit" name="submit" >Save</button>
			<button class="btn" value="Cancel" type="button" name="submit" onclick="goBack();">Cancel</button>
		</div>
        </form>
    </div>
</div>
</body>
</html>