<%@page import="java.util.Arrays"%>
<%@	page import="java.util.ArrayList"%>
<%@	page import="java.util.Collection"%>
<%@ page import="au.com.ourbodycorp.HTML"%>
<%@ page import="au.com.ourbodycorp.Util"%>
<%@ page import="au.com.ourbodycorp.model.*"%>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager"%>
<%@ page import="java.util.HashMap"%>
<%@ page import="java.util.LinkedHashMap"%>
<%@ page import="java.util.List"%>
<%@ page import="java.sql.Connection"%>
<%@ page import="java.sql.PreparedStatement"%>
<%@ page import="java.sql.*"%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ page import="au.com.ourbodycorp.model.managers.CorporationManager"%>
<%
    CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");
	Corporation c = (Corporation) request.getAttribute("corp");
	session.setAttribute("precorpId", c.getId());
	long uId = 0;
	session.setAttribute("uId", 0);
    User u = cu.getUser();
    Corporation corp = cu.getCorporation();
    long id = Long.parseLong(Util.notNull(request.getParameter("id"), "0"));
    String name = Util.notNull(request.getParameter("name"));
    String description = Util.notNull(request.getParameter("description"));
    String type = Util.notNull(request.getParameter("type"));
    boolean showLocation = request.getParameter("showLocation") != null && Boolean.parseBoolean(request.getParameter("showLocation"));
    String[] multitype = null;
    if(showLocation == true){
    multitype = request.getParameterValues("multitype");
    }
    boolean showSides = request.getParameter("showSides") != null && Boolean.parseBoolean(request.getParameter("showSides"));
    boolean hideExercise = request.getParameter("hideExercise") != null && Boolean.parseBoolean(request.getParameter("hideExercise"));
    Exercise e = null;
    LibraryManager lm = new LibraryManager();
    String action = Util.notNull(request.getParameter("action"), "");
    if (action.equalsIgnoreCase("clone") && id > 0) {
        		e = lm.cloneExerciseToMyExercises(id, corp, u, corp.getCompanyId(),uId);
        		
        		String type0="My Exercises";
                String activity_page="corp/library/exercise.jsp";
            	String corp_id=String.valueOf(corp.getId());
            	String corp_name=corp.getName();
            	String corp_loc=corp.getSuburb();
            	String user_id=String.valueOf(u.getId());
            	String user_name=u.getFullName();
            	try{
                	Util.allActivitiesLogs2(type0,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,String.valueOf(e.getId()),"---","copy","","","Copy Exercise "+e.getName()+" InTo My Exercise  By User");
                }catch(Exception ee){
                	
                }
		        response.sendRedirect("exercise.jsp?id=" + e.getId());
    }
    String o=null;
   

    String title = "Create Exercise";
    if (id > 0) {
        title = "Edit Exercise";
    	if(title == "Edit Exercise"){
    		Connection conn2 = Util.getConnection();
    		PreparedStatement pst;
            pst = conn2.prepareStatement("select shareid from exercises where id = ?");
            pst.setLong(1, id);
            ResultSet rs = pst.executeQuery();
            if(rs.next()){
            	session.setAttribute("uId", rs.getLong("shareid"));
            	uId = Long.parseLong(session.getAttribute("uId").toString());
            }
            pst.close();
    		conn2.close();
        	Long preId = id;
        	if(preId == id){
        	session.setAttribute("preId", preId);
        	}
        }
        e = lm.getExercise(id);
        if (e == null) {
            response.sendError(404, "Exercise not found");
//             response.sendRedirect("/corp/library/index.jsp?bp=&type=&exclusive=true");
            return;
        }
        o="Name:"+e.getName()+";type:"+e.getType();
    }else{
    	session.removeAttribute("preId");
    	try{
        	//Util.allActivitiesLogs("My Exercises", u.getId(), corp.getName(), "Add Exercise");
        }catch(Exception ee){
        	
        }
    }

    HashMap<String, String> errorFields = new HashMap<String, String>();

    boolean isDisabled = (e != null && e.getCorpId() != corp.getId());
    
    if(isDisabled){
    	try{
        	//Util.allActivitiesLogs("My Exercises", u.getId(), corp.getName(), "Copy To My Exercise");
        }catch(Exception ee){
        	
        }
    }else if (id > 0){
    	try{
        	//Util.allActivitiesLogs("My Exercises", u.getId(), corp.getName(), "Edit Exercise");
        }catch(Exception ee){
        	
        }
    }

    if (request.getMethod().equalsIgnoreCase("get") && id > 0) {
        name = e.getName();
        description = e.getDescription();
        type = e.getType();
        showSides = e.isShowSides();
        hideExercise = e.isHideExercise();
        showLocation = e.isShareLocation();
        
    } 
    else if (request.getMethod().equalsIgnoreCase("post") && !isDisabled) {
        if (name.trim().length() == 0) {
            errorFields.put("name", "Please enter a name");
        }else if(showLocation == true){
        	if(multitype == null){
				request.setAttribute("MutiError", "Please select a location");
				errorFields.put("multitype", "Please select a location");
        	}
        }
            if (errorFields.size() == 0) {
            	CorporationManager cm = null;
            	if(u != null && name != null){
            		cm= new CorporationManager();
            		if(uId!=0){
			    		cm.chkForNameUpdate(uId, name);	
			    	}
            	}
            	if(multitype != null){
            		for(String companyid : multitype){
            			uId = Long.parseLong(session.getAttribute("uId").toString());
            			
            			List<Corporation> corpObjList = null;
        			    if (u != null && name != null) {
        			    	corpObjList = cm.getExerciseid(Long.parseLong(companyid),name,uId,id);
        			    }
        			    if(Long.parseLong(companyid) != c.getId()){
            				if(!corpObjList.isEmpty()){
            					for (Corporation corpuexid : corpObjList) {
            						e.setId(corpuexid.getId());
            					}
            				}else{
                            e = new Exercise();
                            e.setCreator(u.getId());
                            e.setCorpId(Long.parseLong(companyid));
                            e.setName(name);
                            e.setType(type);
                            e.setDescription(description);
                            e.setShowSides(showSides);
                            e.setHideExercise(hideExercise);
                            e.setShareLocation(showLocation);
                            e.setShareid(uId);
                            lm.save(e,corp.getCompanyId(),null);
                            
                           
                           
                            Connection conn = Util.getConnection();
                        	if(uId == 0){
            	        		PreparedStatement pst;
            		            pst = conn.prepareStatement("select shareid from exercises where id = ?");
            		            pst.setLong(1, e.getId());
            		            ResultSet rs = pst.executeQuery();
            		            if(rs.next()){
            		            	session.setAttribute("uId", rs.getLong("shareid"));
            		            }
            		            rs.close();
            		            pst.close();
                    		}
                            conn.setAutoCommit(false);
                       if(session.getAttribute("captionmedia") != null){
                    	   ExerciseMedia oem1 = null;
                           ExerciseMedia em = null;
                           /* PreparedStatement pst = conn.prepareStatement("select * from exercise_media where exercise = ? ");
                           pst.setLong(1, Long.parseLong(session.getAttribute("preId").toString()));
                      		ResultSet rs = pst.executeQuery();
                       while(rs.next()) {
                           pst = conn.prepareStatement("insert into exercise_media (id, exercise, seq, type, data, url, caption, name, key_photo, version) values (?,?,?,?,?,?,?,?,?,?)");
                           int i = 0;
                           PreparedStatement pst1 = conn.prepareStatement("select max(id+1) as id from exercise_media");
                           ResultSet rs1 = pst1.executeQuery();
                           if(rs1.next()) {
                           pst.setLong(++i, rs1.getLong("id"));
                           }
                           pst.setLong(++i, e.getId());
                           pst.setInt(++i, rs.getInt("seq"));
                           pst.setString(++i, rs.getString("type"));
                           pst.setBytes(++i, rs.getBytes("data"));
                           pst.setString(++i, rs.getString("url"));
                           pst.setString(++i, rs.getString("caption"));
                           pst.setString(++i, rs.getString("name"));
                           pst.setBoolean(++i, rs.getBoolean("key_photo"));
                           pst.setInt(++i, rs.getInt("version"));
                           pst.executeUpdate();
                           pst.close();
                       } */
                           List<ExerciseMedia> exWithData= new ArrayList<ExerciseMedia>();
                      		if(session.getAttribute("preId") != null){
                      			exWithData = lm.getExerciseMediaWithData(Long.parseLong(session.getAttribute("preId").toString()));
                      		}
						for(ExerciseMedia ex : exWithData){
							em = new ExerciseMedia();
							em.setExercise(e.getId());
                            em.setId(0);
                            em.setSeq(ex.getSeq());
                            em.setType(ex.getType());
                            em.setData(ex.getData());
                            em.setUrl(ex.getUrl());
                            em.setCaption(ex.getCaption());
                            em.setName(ex.getName());
                            em.setKey(ex.isKey());
                            em.setVersion(ex.getVersion());
                            lm.save(em);
                           
                        }

                            }
                        if(session.getAttribute("body") != null){
                        		PreparedStatement pst = conn.prepareStatement("insert into bodyparts (exercise, bodypart) values (?,?)");
                                List <String> bodynames;
                               	bodynames = (ArrayList<String>)session.getAttribute("body");
                                for (String b : bodynames) {
                                	String str1 = b.replaceFirst("and", "");
                            		String str = str1.replaceAll("\\.", "");
                            		str = str.replaceFirst(str.substring(0,1), str.substring(0,1).toLowerCase());
                                	String bp = str.replaceAll("\\s", "");
                                    try{
                                    pst.setLong(1, e.getId());
                                    pst.setString(2, bp);
                                    pst.execute();
                                    }catch(Exception ee){
                                    	ee.printStackTrace();
                                    }
                                }
                                pst.close();

                                pst = conn.prepareStatement("insert into exercise_types (exercise, type) values (?,?)");
                                List<String> typenames;
                                typenames = (ArrayList<String>)session.getAttribute("type");
                                for (String t : typenames) {
                                	String str1 = t.replaceFirst("and", "");
                            		String str = str1.replaceAll("\\.", "");
                            		if(str.equals("Release")){
                           			 	str = "selfRelease";
                           		 	}
                            		if(str.equals("Stretch")){
                           			 	str = "stretches";
                           		 	}
                            		str = str.replaceFirst(str.substring(0,1), str.substring(0,1).toLowerCase());
                                	String ty = str.replaceAll("\\s", "");
                                    try{
                                    pst.setLong(1, e.getId());
                                    pst.setString(2, ty);
                                    pst.execute();
                                    }catch(Exception ee){
                                    	ee.printStackTrace();
                                    }
                                }
                                pst.close();

                                conn.commit();
                             //   conn.close();
                            }
                        if (conn!=null)
                        	conn.close();
                            }
        			    }
            			}
            	}
            	if(title != "Create Exercise"){
            		e.setId(Long.parseLong(session.getAttribute("preId").toString()));
            	}

				if(title == "Create Exercise"){
					uId = Long.parseLong(session.getAttribute("uId").toString());
                    e = new Exercise();
                    e.setCreator(u.getId());
				}
                e.setCorpId(corp.getId());
                e.setName(name);
                e.setType(type);
                e.setDescription(description);
                e.setShowSides(showSides);
                e.setHideExercise(hideExercise);
                e.setShareLocation(showLocation);
                e.setShareid(uId);
                lm.save(e,corp.getCompanyId(),null);
               
               
                if(title=="Create Exercise"){
        		String type0="My Exercises";
                String activity_page="corp/library/exercise.jsp";
            	String corp_id=String.valueOf(corp.getId());
            	String corp_name=corp.getName();
            	String corp_loc=corp.getSuburb();
            	String user_id=String.valueOf(u.getId());
            	String user_name=u.getFullName();
            	try{
                	Util.allActivitiesLogs2(type0,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,String.valueOf(e.getId()),"---","new","Add Exercise into Library","Name:"+e.getName()+";type:"+e.getType(),"Add Exercise By User");
                }catch(Exception ee){
                	
                }
            	}
                else
                {
                	 String type0="My Exercises";
                     String activity_page="corp/library/exercise.jsp";
                 	String corp_id=String.valueOf(corp.getId());
                 	String corp_name=corp.getName();
                 	String corp_loc=corp.getSuburb();
                 	String user_id=String.valueOf(u.getId());
                 	String user_name=u.getFullName();
                 	try{
                 		Util.allActivitiesLogs2(type0,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,String.valueOf(e.getId()),"---","update",o,"Name:"+e.getName()+";type:"+e.getType(),"Edit Exercise By User");
                     }catch(Exception ee){
                     	
                     }
                }
              
            		
                	
            if (request.getParameter("submit").contains("Close")) {
                    String bodyPart = "";
                  if (e.getBodyParts() != null && e.getBodyParts().size() > 0) {
                        bodyPart = e.getBodyParts().getFirst().name();
                  }

                    String type2 = "";
                  if (e.getTypes() != null && e.getTypes().size() > 0) {
                        type2 = e.getTypes().getFirst().name();
                  }

                response.sendRedirect("/corp/library/?bp=" + bodyPart + "&type=" + type2);
            } 
            else {
            	if(title == "Create Exercise"){
            	   	response.sendRedirect("exercise.jsp?id=" + e.getId());
            	}else{
            		response.sendRedirect("exercise.jsp?id=" + session.getAttribute("preId"));
            	}
            }
            return;
        }
           
    }
   
    

    LinkedHashMap<String, String> bodyParts = new LinkedHashMap<String, String>();
    for (Exercise.BodyPart b : Exercise.BodyPart.values()) {
        bodyParts.put(b.name(), b.getValue());
    }
    LinkedHashMap<String, String> types = new LinkedHashMap<String, String>();
    for (Exercise.Type b : Exercise.Type.values()) {
        types.put(b.name(), b.getValue());
    }
    String[][] breadcrumbs = {{"/corp/", corp.getName()}, {"/corp/library/", "Exercises"}, {null, title}};

    String error = null;
%>
<html>
<head>
<title><%=title%></title>
<script type="text/javascript">
$(document).ready(function() {
	  $("#editmedia").fancybox({
		  	maxWidth    : 580,
		    height      : 400,
		    autoSize    : false,
		    closeClick  : false,
		    openEffect  : 'none',
		    closeEffect : 'none',
		    type        : 'iframe',
		    'afterClose':function () {
		        window.location.reload();
		    },
	   });
	 });
$( document ).ready(function showorhide(){
	if($("#HideLoc").val() == "true"){
		$("#HideLocation").hide();
	}
	var checked = $("input[name='showLocation']").attr("checked") ? "True" : "False";
	var foo = []; 
	$('#multitypedub :selected').each(function(i, selected){ 
	  foo[i] = $(selected).text(); 
	});
	$.each(foo, function( index, value ) {
	var row = $('#multitype');
	row.find("option[value="+value+']').attr('selected','selected');
		});
	if(checked == "True"){
	$("#multitype").show();
	$("#tabsedit").hide();
	$("#mediaedit").hide();
	if($("#multitype").val() != null){
	$("#errorhideorshow").hide();
	}else if($("#name").val() != ''){
		$("#errorhideorshow").show();
		$("#errorLocation").val("Please select a location");
		<%request.setAttribute("MutiError", "Please select a location");%>
	}else if($("#name").val() == ''){
		$("#errorhideorshow").hide();
	}
	}else{
	$("#multitype").hide();
	$("#errorhideorshow").hide();
	$("#tabsedit").hide();
	$("#mediaedit").hide();
	}
	
});
function showorhide(){
	var foo2 = []; 
	$('#multitypedub :selected').each(function(i, selected){ 
	  foo2[i] = $(selected).text(); 
	});
	$.each(foo2, function( index, value ) {
	var row = $('#multitype');
	row.find("option[value="+value+']').attr('selected','selected');
		});
	var checked = $("input[name='showLocation']").attr("checked") ? "True" : "False";
	if(checked == "True"){
// 	$("#edittabs").hide();
// 	$("#editmedia").hide();
// 	$("#tabsedit").show();
// 	$("#mediaedit").show();
	$("#multitype").show();
	}else{
// 	$("#edittabs").hide();
// 	$("#editmedia").hide();
// 	$("#tabsedit").show();
// 	$("#mediaedit").show();
	$("#multitype").hide();
	$("#errorhideorshow").hide();
	}
}

function showorhidetabs() {
	var foo2 = []; 
	$('#multitypedub :selected').each(function(i, selected){ 
	  foo2[i] = $(selected).text(); 
	});
	$.each(foo2, function( index, value ) {
	var row = $('#multitype');
	row.find("option[value="+value+']').attr('selected','selected');
		});
	var foo = []; 
	var foo1 = []; 
	$('#multitypename :selected').each(function(i, selected){ 
	  foo[i] = $(selected).text(); 
	});
	$('#multitype :selected').each(function(i, selected){ 
		  foo1[i] = $(selected).text(); 
		});
		  var a = foo.length; 
		  var b = foo1.length; 
		  if(a == b){
			  $("#edittabs").hide();
			  $("#editmedia").hide();
			  $("#tabsedit").show();
			  $("#mediaedit").show();
		  }else if(a <= b){
			  $("#edittabs").hide();
			  $("#editmedia").hide();
			  $("#tabsedit").show();
			  $("#mediaedit").show();
		  }else if(a >= b){
			  $("#edittabs").show();
			  $("#editmedia").show();
			  $("#tabsedit").hide();
			  $("#mediaedit").hide();
		  }
	
}
</script>
</head>
<body>
	<div class="page-header">
		<h1><%=title%></h1>
	</div>
	<%@include file="/WEB-INF/includes/breadcrumbs.jsp"%>
	<%@include file="/WEB-INF/includes/formError.jsp"%>
	<%@include file="/WEB-INF/includes/formErrorSimple.jsp"%>
	<div class="alert alert-error" id="errorhideorshow"
		style="width: 250px;">
		<p>
			<% if(request.getAttribute("MutiError") != null){%><%= request.getAttribute("MutiError") %>
			<%}%>
		</p>
	</div>
	<form action="exercise.jsp" method="post" class="form-horizontal">
		<div class="row">
			<%if(isDisabled) { %>
			<input type="hidden" id="HideLoc" value="<%=isDisabled%>">
			<div class="span6" style="margin-bottom: 10px">
				<div class="alert">
					This is library exercise if you want to edit it, copy it to your
					exercises. <br /> <input type="hidden" id="checkedExercise" /> <a
						href="exercise.jsp?action=clone&id=<%=id%>"
						style="margin-top: 10px" class="btn btn-primary">Copy To My
						Exercises</a>
				</div>
			</div>
			<% } %>
			<div class="span6">
				<input type="hidden" name="id" value="<%=id%>">
				<%=HTML.textInput("name","Name", "100", "100", name, null, errorFields)%>
				<%--=HTML.textArea("description", "Description", description, 5)--%>
				<div class="control-group">
					<label class="control-label" for="type">This exercise is</label>
					<div class="controls">
						<select id="type" name="type">
							<option value="<%=Exercise.TIMED_EXERCISE%>"
								<%=(type.equals(Exercise.TIMED_EXERCISE)) ? "selected" : ""%>>Time
								based</option>
							<option value="<%=Exercise.REP_EXERCISE%>"
								<%=(type.equals(Exercise.REP_EXERCISE)) ? "selected" : ""%>>Rep
								based</option>
						</select>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="optionsCheckbox">Show
						Sides</label>
					<div class="controls">
						<label class="checkbox"> <input type="checkbox"
							id="optionsCheckbox" name="showSides" value="true"
							<%=(showSides) ? " checked": ""%>> Display the
							left/right/both selector
						</label>
					</div>
				</div>
				<%if(isDisabled) { %>
				<div class="control-group">
					<label class="control-label" for="hideCheckbox">Hide
						Exercise</label>
					<div class="controls">
						<label class="checkbox"> <input type="checkbox"
							id="hideCheckbox" name="optionsCheckbox" value="true"
							<%=true ? " checked": ""%>> Hide this exercise from
							library
						</label>
					</div>
				</div>
				<%}else{ %>
				<div class="control-group">
					<label class="control-label" for="hideCheckbox">Hide
						Exercise</label>
					<div class="controls">
						<label class="checkbox"> <input type="checkbox"
							id="hideCheckbox" name="hideExercise" value="true"
							<%=(hideExercise) ? " checked": ""%>> Hide this exercise
							from library
						</label>
					</div>
				</div>
				<%} %>
				<%CorporationManager cm = new CorporationManager();
            if(cm.getCorporations(u.getId()).size() > 1){
            %>
				<div class="control-group" id="HideLocation">
					<label class="control-label" for="shareCheckbox">Share
						across locations</label>
					<div class="controls">
						<label class="checkbox"> <input type="checkbox"
							onclick="showorhide();" id="shareCheckbox" name="showLocation"
							value="true" <%=(showLocation) ? " checked": ""%>>
						</label> <select id="multitype" name="multitype"
							onclick="showorhidetabs();" onchange="showorhidetabs();" multiple
							style="display: none; margin: -5px 0 0 19px;">
							<% List<Corporation> corps = null;
			    if (u != null) {
// 			        CorporationManager cm = new CorporationManager();
			        corps = cm.getCorporations(u.getId());
			    }%>
							<%
            if (corps != null && corps.size() > 0) {
                    for (Corporation corpu : corps) {
                        if (c != null && c.equals(corpu)) {
                            continue;
                        }
                  %>
							<option value="<%=corpu.getId()%>"><%=corpu.getName()%></option>
							<%
                    }
                %>

							<%
                      }%>
						</select> 
						
						
						<select id="multitypename" name="multitypename" multiple
							style="display: none; margin: -5px 0 0 19px;">
							<% 
			    	List<Corporation> corpsid = null;
			    if (u != null) {
// 			        CorporationManager cm = new CorporationManager();
			        if(!name.equals("")){
			        corpsid = lm.getCorpid(name,corp.getCompanyId(),uId,id);
			        }
			    }%>
							<% 
            if (corpsid != null && corpsid.size() > 0) {
                   if (corpsid != null && corpsid.size() > 0) {
                          for (Corporation corpu1 : corpsid) {
                  %>
							<option value="<%=corpu1.getId()%>"
								<%=(corpu1.getId() == corpu1.getId()) ? "selected" : ""%>><%=corpu1.getName()%>
								<% 
                  %>
							</option>
							<%
                    }
                %>

							<%
                   }
            }%>
						</select>
					</div>
				</div>
				<%} %>
				
					
						<select id="multitypedub" name="multitypedub" multiple
							style="display: none; margin: -5px 0 0 19px;">
							<% 
             	List<Corporation> corpsid = null;
            	List<Long> Eidlist = new ArrayList<Long>();
			    if (u != null) {
// 			        CorporationManager cm = new CorporationManager();
			        if(!name.equals("")){
			        corpsid = lm.getCorpid(name,corp.getCompanyId(),uId,id);
			        }
			    }%>
							<% 
            if (corpsid != null && corpsid.size() > 0) {
                   if (corpsid != null && corpsid.size() > 0) {
                          for (Corporation corpu1 : corpsid) {
                  %>
							<option value="<%=corpu1.getId()%>"
								<%=(corpu1.getCompanyId() == corpu1.getCompanyId()) ? "selected" : ""%>><%=corpu1.getCompanyId()%>
								<% 
                		  Eidlist.add(corpu1.getId());
                  %>
							</option>
							<%
                    }
                %>

							<%
                   }
                   session.setAttribute("Eidlist", Eidlist);
            }%>
						</select> 
						
				<div class="control-group">
					<label for="Media" class="control-label">Tabs</label>
					<div class="controls">
						<% if (id == 0) {%>
						Select types and body parts after saving
						<% } else { %>
						<% if (e.getBodyParts() != null && e.getBodyParts().size() > 0) { List<String> bodyList = new ArrayList<String>();%>
						<b>Body Parts:</b>
						<%
                        int i = 0;
                        for (Exercise.BodyPart bp : e.getBodyParts()) {
                        	bodyList.add(bp.getValue());
                    %>
						<%=bp.getValue()%><%=(++i < e.getBodyParts().size()) ? "," : ""%>
						<%
                        }
                        session.setAttribute("body", bodyList);
                    %>
						<% } %>
						<% if (e.getTypes() != null && e.getTypes().size() > 0) { List<String> typeList = new ArrayList<String>();%>
						<br> <b>Exercise Types:</b>
						<%
                        int i = 0;
                        for (Exercise.Type t : e.getTypes()) {
                        	typeList.add(t.getValue());
                    %>
						<%=t.getValue()%><%=(++i < e.getTypes().size()) ? "," : ""%>
						<%
                        }
                        session.setAttribute("type", typeList);
                    %>
						<% } %>
						<br>
						<%
                    if (!isDisabled) {
                %>
						<a id="edittabs" href="inline/bodyparts.jsp?id=<%=id%>"
							class="btn fancybox" style="margin-top: 15px;">Edit Tabs</a>
						<div id="tabsedit" style="margin: 25px 0 0 !important;">
							<p>Save exercise before adding tabs</p>
						</div>
						<%
                    }
                %>
						<% } %>
					</div>
				</div>
				<%
        if (!isDisabled) {
    %>
				<div class="control-group" style="margin-top: 15px;">
					<label for="Media" class="control-label">&nbsp;</label>
					<div class="controls">
						<% if (id == 0) {%>
						Save exercise before adding media
						<% } else { %>
						<a id="editmedia" href="inline/media.jsp?id=<%=id%>"
							class="btn btn-primary">Edit Media</a>
						<div id="mediaedit">
							<p>Save exercise before adding media</p>
						</div>
						<% } %>
					</div>
				</div>
				<% if (id != 0) {%>
				<div class="control-group" style="margin-top: 15px;">
					<label for="Delete" class="control-label">&nbsp;</label>
					<div class="controls">

						<a href="inline/delete.jsp?id=<%=id%>"
							class="btn btn-danger fancybox">Delete Exercise</a>
					</div>
				</div>
				<% } %>
				<% } %>

			</div>

			<div class="span4">
				<ul class="thumbnails">
					<%

                if (id != 0) {
                    int i = 0;
                    List<ExerciseMedia> exerciseMediaList = lm.getExerciseMedia(id);
                    if(exerciseMediaList.isEmpty()){
                    	session.setAttribute("captionmedia", null);
                    }
                    for (ExerciseMedia em : exerciseMediaList) {
                        i++;
                        String url = "/images/movie_150_100.gif";
                        if (em.getType().equalsIgnoreCase("photo")) {
                            url = "/media?id=" + em.getId() + "&width=150&v=" + em.getVersion();
                        }
                            String caption = em.getCaption();
                            if (caption == null || caption.length() == 0) {
                                caption = em.getName();
                            }

            %>
					<li>
						<div class="thumbnail" style="float: left; position: relative;">
							<a
								href="inline/view.jsp?id=<%=em.getId()%>&media_id=<%=id%>&edit=<%=!isDisabled%>&name=<%=caption%>&isDisabled=<%=isDisabled%>"
								id="editmedia" class="fancybox"><img src="<%=url%>" width="150" height="105"
								alt="<%=Util.formValue(caption)%>"></a>
							<%session.setAttribute("captionmedia", em.getId());%>
							<div
								style="position: absolute; top: 5px; left: 5px; z-index: 999; background-color: black; opacity: 0.6; font-weight: bold; padding: 3px; color: white;"><%=i%></div>
						</div>
					</li>
					<%
                }
                }
            %>
				</ul>
			</div>
		</div>
		<%
        if (!isDisabled) {
    %>
		<div class="row">
			<div class="span9">
				<%=HTML.saveAndClose()%>
			</div>
		</div>
		<%
        }
    %>

	</form>

</body>
</html>