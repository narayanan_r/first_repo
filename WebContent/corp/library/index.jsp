<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.Corporation" %>
<%@ page import="au.com.ourbodycorp.model.CorporationUser" %>
<%@ page import="au.com.ourbodycorp.model.Exercise" %>
<%@ page import="au.com.ourbodycorp.model.User" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Date" %>
<%
long StartMilliSec = (long)System.currentTimeMillis();
System.out.println("Staring from fiizio/corp/library/index.jsp>>>  "+new Date());
    CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");

    User u = cu.getUser();
    Corporation corp = cu.getCorporation();
    
    String type = Util.notNull(request.getParameter("type"));
    String bodyPart = Util.notNull(request.getParameter("bp"));
    boolean exclusive = request.getParameter("exclusive") != null && Boolean.parseBoolean(request.getParameter("exclusive"));
    boolean hiddenExercise = request.getParameter("hiddenExercise") != null && Boolean.parseBoolean(request.getParameter("hiddenExercise"));
    String query = (request.getParameter("query") != null && request.getParameter("query").trim().length() > 0) ? request.getParameter("query") : "";

    String[][] breadcrumbs = {{"/corp/", corp.getName()}, {null, "Exercises"}};

    LibraryManager lm = new LibraryManager();

    Exercise.BodyPart bp = null;
    Exercise.Type tp = null;
    try {
        bp = Exercise.BodyPart.valueOf(bodyPart);
    } catch (Exception e) {
        bp = null;
    }

    try {
        tp = Exercise.Type.valueOf(type);
    } catch (Exception e) {
        tp = null;
    }
    
    List<Exercise> exercises;
    if (query != "") {
    	String activity_page="corp/library/index.jsp";
		String corp_id=String.valueOf(corp.getId());
		String corp_name=corp.getName();
		String corp_loc=corp.getSuburb();
		String user_id=String.valueOf(u.getId());
		String user_name=u.getFullName();
		String invitor_id="";
		String invitor_name="---";
		String flag="view";
	    String from="---";
	    String to="---";
		String desc="Search All Exercises By User";
	     try{
        	//Util.allActivitiesLogs("My Exercises", u.getId(), corp.getName(), "Search Exercise");
	    	Util.allActivitiesLogs2("My Exercises",activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
	    }catch(Exception ee){
	    	
	    } 
    	/* try{
        	Util.allActivitiesLogs("My Exercises", u.getId(), corp.getName(), "Search Exercise");
        }catch(Exception e){
        	
        } */
    	exercises = lm.getExercisesBySearch(bp, tp, corp.getId(), exclusive, hiddenExercise, query);
	} else {
		/* try{
	    	Util.allActivitiesLogs("My Exercises", u.getId(), corp.getName(), "View All Exercises");
	    }catch(Exception e){
	    	
	    } */
	    String activity_page="corp/library/index.jsp";
		String corp_id=String.valueOf(corp.getId());
		String corp_name=corp.getName();
		String corp_loc=corp.getSuburb();
		String user_id=String.valueOf(u.getId());
		String user_name=u.getFullName();
		String invitor_id="";
		String invitor_name="---";
		String flag="view";
	    String from="---";
	    String to="---";
		String desc="View All Exercises By User";
	    try{
	    	//Util.allActivitiesLogs("My Exercises", u.getId(), corp.getName(), "View All Exercises");
	    	Util.allActivitiesLogs2("My Exercises",activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
	    }catch(Exception ee){
	    	
	    } 
		exercises = lm.getExercises(bp, tp, corp.getId(), exclusive, hiddenExercise);
	}

%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head><title>My Exercise Library</title>
<script type="text/javascript">
$(document).ready(function(){      
     $('#query').change(function () {document.ev.submit();});
});
function queryfun1(bodyPart) {
	 	var query = $("#query").val(); 
 		var jspcall = "?type=&bp="+bodyPart+"&query="+query+"";
     	window.location.href = jspcall;
}
function queryfun2(type,bodyPart,exclusive) {
	    var query = $("#query").val(); 
 		var jspcall = "?type="+type+"&bp="+bodyPart+"&exclusive="+exclusive+"&query="+query+"";
     	window.location.href = jspcall;
}
function queryfun3(type) {
	 	var query = $("#query").val(); 
 		var jspcall = "?type="+type+"&bp=&query="+query+"";
     	window.location.href = jspcall;
}
function queryfun4(type,bodyPart,exclusive) {
	 	var query = $("#query").val(); 
 		var jspcall = "?type="+type+"&bp="+bodyPart+"&exclusive="+exclusive+"&query="+query+"";
     	window.location.href = jspcall;
}
</script>
</head>
<body>
<div class="page-header">
    <h1>My Exercise Library</h1>
</div>
<%@include file="/WEB-INF/includes/breadcrumbs.jsp"%>

<div class="row">
    <div class="span10">
        <form class="form-inline" action="index.jsp" name="ev">
            <input type="hidden" name="bp" value="<%=bodyPart%>">
            <input type="hidden" name="type" value="<%=type%>">
            <a href="exercise.jsp?type=<%=type%>&bp=<%=bodyPart%>" class="btn btn-primary" style="margin-right: 25px;float:left"><i class="icon-plus icon-white"></i> Add Exercise</a>
            <label class="checkbox" style="float:left;margin: 0 25px 0 0;"><input type="checkbox" name="exclusive" value="true" <%=exclusive?"checked":""%> onchange="document.forms.ev.submit();"> Show only my exercises</label>
            <label class="checkbox" style="float:left"><input type="checkbox" name="hiddenExercise" value="true" <%=hiddenExercise?"checked":""%> onchange="document.forms.ev.submit();"> Show hidden exercises</label>
            <div class="span3">
		        <input type="text" class="input-medium search-query" name="query" id="query" value="<%=Util.formValue(query)%>">
		        <button type="submit" class="btn">Search</button>
		    </div>
        </form>
    </div>
</div>
<div class="row" style="margin-top: 10px;">
    <div class="span10 offset1">
        <ul class="nav nav-tabs" style="margin-bottom: 0;">
            <li <%=(type.isEmpty()) ? "class=\"active\"" : ""%>>
                <a href="javascript:void(0)" onclick="queryfun1('<%=bodyPart%>');">All Types</a>
            </li>
            <%
                for (Exercise.Type et : Exercise.Type.values()) {
            %>
            <li <%=(type.equals(et.name())) ? "class=\"active\"" : ""%>>
                <a href="javascript:void(0)" onclick="queryfun2('<%=et.name()%>','<%=bodyPart%>',<%=exclusive%>);"><%=et.getValue()%></a>
            </li>
            <%
                }
            %>
        </ul>
    </div>
</div>
<div class="row">
    <div class="span1">
        <div class="tabs-left">
            <ul class="nav nav-tabs">
            <li <%=(bodyPart.isEmpty()) ? "class=\"active\"" : ""%>>
                <a href="javascript:void(0)" onclick="queryfun3('<%=type%>');">All Parts</a>
            </li>
            <%
                for (Exercise.BodyPart b : Exercise.BodyPart.values()) {
            %>
            <li <%=(bodyPart.equals(b.name())) ? "class=\"active\"" : ""%>>
                <a href="javascript:void(0)" onclick="queryfun4('<%=type%>','<%=b.name()%>',<%=exclusive%>);"><%=b.getValue()%></a>
            </li>
            <%
                }
            %>
        </ul>
        </div>
    </div>
    <div class="span9">
        <div class="row" style="margin-left: 0; margin-top: 30px;">
            <div class="span9">


        <ul class="thumbnails">
            <%
                for (Exercise exercise : exercises) {
                    String url = "/images/photo_150_100.gif";
                    if (exercise.getKeyPhotoId() > 0) {
                        url = "/media?id=" + exercise.getKeyPhotoId() + "&width=150&v=" + exercise.getKeyPhotoVersion();
                    }
            %>
            <li >
          <div class="thumbnail" style="width: 150px;">
              <img src="<%=url%>" width="150" alt="" height="105">
            <div class="caption">
              <h5 style="height: 35px; overflow: hidden;"><a href="exercise.jsp?id=<%=exercise.getId()%>"><%=exercise.getName()%></a></h5>
            </div>
          </div>
        </li>
            <%
                }
            %>


      </ul>
                </div>
        </div>
    </div>
</div>
<%long EndMilliSec = (long)System.currentTimeMillis();
long TotalMillySec = EndMilliSec - StartMilliSec;%>
<%System.out.println("Ending  from fiizio/corp/library/index.jsp>>>  "+TotalMillySec+"ms");%>
</body>
</html>