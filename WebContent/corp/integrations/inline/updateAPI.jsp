<%@page import="au.com.ourbodycorp.ImportCliniko"%>
<%@page import="java.io.Console"%>
<%@page import="com.sun.org.apache.xalan.internal.xsltc.compiler.sym"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@ page import="au.com.ourbodycorp.Config" %>
<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.MailMessage" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="au.com.ourbodycorp.model.managers.UserManager" %>
<%@ page import="java.util.Properties" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.net.URLEncoder" %>
<%@ page import="org.apache.http.client.ClientProtocolException" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");

	if (cu == null) {
	    response.sendRedirect(Util.loginUrl(Constants.LoginReason.login, request));
	    return;
	}
	
    User u = cu.getUser();
    Corporation corp = cu.getCorporation();

    long id = Long.parseLong(request.getParameter("id"));
    String apiKey = request.getParameter("updateAPIKey");
    String error = null;
    
    Connection conn = Util.getConnection();
    
    if (request.getMethod().equalsIgnoreCase("post")) {
    	if (apiKey==null || apiKey.trim().equals("")){
    		error = "Please enter a API Key.";
    	} else {
//     		new UserManager().updateAPIKey(apiKey,id);
        try {
        	StringBuilder sb = new StringBuilder();
            sb.append("update corporation set cliniko_apikey = ? where id = ?");
            
        	try{
        		
        		 PreparedStatement pst3 = conn.prepareStatement("select id from patient where corp_id = ?");
            	 pst3.setLong(1, corp.getId());	
            	 ResultSet rs2 = pst3.executeQuery();
           		 if(rs2.next()){
           		 	session.setAttribute("NoPatient", "NoPatient");
           		 }else{
           			session.setAttribute("NoPatient", null);
           		 }pst3.close();
    			new ImportCliniko().getAndParseJSON(new ImportCliniko().getDetailsFromCliniko(apiKey),corp.getId(),u.getId());
    			
                PreparedStatement pst = conn.prepareStatement(sb.toString());
                pst.setString(1, apiKey);
                pst.setLong(2, corp.getId());
                pst.executeUpdate();
                pst.close();
                
                if(session.getAttribute("NoPatient") == null){
                PreparedStatement pst5 = null;
                PreparedStatement pst4 = conn.prepareStatement("select id from patient where corp_id = ? and reference = 'Cliniko'");
	           	pst4.setLong(1, corp.getId());	
	           	ResultSet rs3 = pst4.executeQuery();
          		 while(rs3.next()){
          			pst5 = conn.prepareStatement("update patient set check_patient_exist='Checked' where id = ?");
                    pst5.setLong(1, rs3.getLong("id"));
                 	pst5.executeUpdate();
          		 }
          		pst4.close();
          		pst5.close();
                }
    		} catch (ClientProtocolException e){
//     			e.printStackTrace(); 
    			error = "Please enter a valid cliniko API key.";
    		} catch (Exception ee){
//     			ee.printStackTrace(); 
                PreparedStatement pst1 = conn.prepareStatement(sb.toString());
                pst1.setString(1, apiKey);
                pst1.setLong(2, corp.getId());
                pst1.executeUpdate();
                pst1.close();
    		}
        }
        finally {
            if (conn != null) {
                conn.close();
            }
        }
    		
    		if(error == null){
    			if(session.getAttribute("NoPatient") == null){
    				if(Config.getString("https").equals("true")){
    		    		response.sendRedirect("https://"+request.getHeader("host")+"/close.jsp?redir=" + URLEncoder.encode("/corp/integrations/", "UTF-8"));
    		    	}else{
    		    		response.sendRedirect("http://"+request.getHeader("host")+"/close.jsp?redir=" + URLEncoder.encode("/corp/integrations/", "UTF-8"));
    		    	}
    				return;
    			}else{
    		if(Config.getString("https").equals("true")){
				response.sendRedirect("https://"+request.getHeader("host")+"/corp/integrations/inline/check_patient_exist.jsp");
        	}else{
				response.sendRedirect("http://"+request.getHeader("host")+"/corp/integrations/inline/check_patient_exist.jsp");
        	}
    			}
            return;
    		}
    	}
           
    } else {
  		PreparedStatement pst2;
        pst2 = conn.prepareStatement("select cliniko_apikey from corporation where id = ?");
        pst2.setLong(1, corp.getId());
        ResultSet rs = pst2.executeQuery();
        if(rs.next()){ 
        	apiKey = rs.getString("cliniko_apikey");
        }else{
    		apiKey = "";
        }
        rs.close();
        pst2.close();
    }

    if (conn != null) {
        conn.close();
    }

%>
<html>
<head><title>Update API Key</title>
</head>
<body>
<div class="page-header">
    <h1>Update API Key</h1>
</div>
<%@include file="/WEB-INF/includes/formErrorSimple.jsp"%>
<div class="row">
    <div class="span9">
        <form action="updateAPI.jsp" method="post" class="form-horizontal">        
            <input type="hidden" name="id" id="id" value="<%=id%>">
             <%=HTML.textInput("updateAPIKey", "API Key", "100", "100", apiKey)%>
             <%=HTML.submitButton("Update API Key")%>
        </form>
    </div>
</div>
</body>
</html>