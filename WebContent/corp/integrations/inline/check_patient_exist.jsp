<%@page import="au.com.ourbodycorp.ImportCliniko"%>
<%@page import="java.io.Console"%>
<%@page import="com.sun.org.apache.xalan.internal.xsltc.compiler.sym"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@ page import="au.com.ourbodycorp.Config" %>
<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.MailMessage" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="au.com.ourbodycorp.model.managers.UserManager" %>
<%@ page import="java.util.Properties" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.net.URLEncoder" %>
<%@ page import="org.apache.http.client.ClientProtocolException" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");

	if (cu == null) {
	    response.sendRedirect(Util.loginUrl(Constants.LoginReason.login, request));
	    return;
	}
	
    User u = cu.getUser();
    Corporation corp = cu.getCorporation();

//     String apiKey = request.getParameter("updateAPIKey");
    String error = null;
    
    Connection conn = Util.getConnection();
    
    if (request.getMethod().equalsIgnoreCase("post")) {
    	session.setAttribute("isThere", null);
    	if (request.getParameter("submit").startsWith("Use the Existing patient file")) {
    		long id = Long.parseLong(request.getParameter("id"));
           	 PreparedStatement pst2 = conn.prepareStatement("Delete from patient where id = ?");
           	 pst2.setLong(1, id);
           	 pst2.executeUpdate();
           	 pst2.close();
    	}
    	if (request.getParameter("submit").startsWith("Create a New patient with the same name")) {
    		long id = Long.parseLong(request.getParameter("id"));
    		PreparedStatement pst6 = conn.prepareStatement("update patient set check_patient_exist='Checked' where id = ?");
            pst6.setLong(1, id);
         	pst6.executeUpdate();
         	pst6.close();
    	}
    	
	   	 PreparedStatement pst3 = conn.prepareStatement("select id,fname,lname,email,phone from patient where reference = 'Cliniko' and corp_id = ? and check_patient_exist = 'Check'");
     	 pst3.setLong(1, corp.getId());	
     	 ResultSet rs2 = pst3.executeQuery();
    	 while(rs2.next()){
    		PreparedStatement pst4 = conn.prepareStatement("select fname,lname,email,phone from patient where (lower(fname) = ? and lower(lname) = ? or email = ? or phone = ?) and corp_id = ? and check_patient_exist='Checked'");
           pst4.setString(1, rs2.getString("fname").toLowerCase());
           pst4.setString(2, rs2.getString("lname").toLowerCase());
           pst4.setString(3, rs2.getString("email").toLowerCase());
           pst4.setString(4, rs2.getString("phone").toLowerCase());
           pst4.setLong(5, corp.getId());
        	 ResultSet rs3 = pst4.executeQuery();
        	 if(rs3.next()){
        		session.setAttribute("isThere", "True");
    	 	 }else{
    	 		PreparedStatement pst5 = conn.prepareStatement("update patient set check_patient_exist='Checked' where id = ?");
                pst5.setLong(1, rs2.getLong("id"));
            	pst5.executeUpdate();
            	pst5.close();
    	 	 }
        	pst4.close();
         }
    	 pst3.close();
        }else{
        	 session.setAttribute("isThere", null);
        	 PreparedStatement pst3 = conn.prepareStatement("select id,fname,lname,email,phone from patient where reference = 'Cliniko' and corp_id = ? and check_patient_exist = 'Check'");
          	 pst3.setLong(1, corp.getId());	
          	 ResultSet rs2 = pst3.executeQuery();
         	 while(rs2.next()){
         		PreparedStatement pst4 = conn.prepareStatement("select fname,lname,email,phone from patient where (lower(fname) = ? and lower(lname) = ? or email = ? or phone = ?) and corp_id = ? and check_patient_exist='Checked'");
                pst4.setString(1, rs2.getString("fname").toLowerCase());
                pst4.setString(2, rs2.getString("lname").toLowerCase());
                pst4.setString(3, rs2.getString("email").toLowerCase());
                pst4.setString(4, rs2.getString("phone").toLowerCase());
                pst4.setLong(5, corp.getId());
             	 ResultSet rs3 = pst4.executeQuery();
             	 if(rs3.next()){
             		session.setAttribute("isThere", "True");
         	 	 }else{
         	 		PreparedStatement pst5 = conn.prepareStatement("update patient set check_patient_exist='Checked' where id = ?");
                    pst5.setLong(1, rs2.getLong("id"));
                 	pst5.executeUpdate();
                 	pst5.close();
         	 	 }
             	pst4.close();
              }
         	 pst3.close();
         	if(session.getAttribute("isThere") == null){
             	if(Config.getString("https").equals("true")){
            		response.sendRedirect("https://"+request.getHeader("host")+"/close.jsp?redir=" + URLEncoder.encode("/corp/integrations/", "UTF-8"));
            	}else{
            		response.sendRedirect("http://"+request.getHeader("host")+"/close.jsp?redir=" + URLEncoder.encode("/corp/integrations/", "UTF-8"));
            	}
             	return;
             	 }
        }
    if (request.getMethod().equalsIgnoreCase("post")) {
    if(session.getAttribute("isThere") == null){
     	if(Config.getString("https").equals("true")){
    		response.sendRedirect("https://"+request.getHeader("host")+"/close.jsp?redir=" + URLEncoder.encode("/corp/integrations/", "UTF-8"));
    	}else{
    		response.sendRedirect("http://"+request.getHeader("host")+"/close.jsp?redir=" + URLEncoder.encode("/corp/integrations/", "UTF-8"));
    	}
     	return;
     	 }
    }

%>
<html>
<head><title>check_patient_exist</title>
</head>
<body>
<div class="page-header">
    <h1>Patient details already exists.</h1>
</div>
<%@include file="/WEB-INF/includes/formErrorSimple.jsp"%>
<div class="row">
    <div class="span9">
    <p>This patient details already exists in your database.</p>
        <form action="check_patient_exist.jsp" method="post" class="form-horizontal"> 
        <p><b>Cliniko Patient Details:</b></p>
        <table border="1" width="100%">
        <tr><th>Name</th><th>Email</th><th>Phone</th></tr>
        <%PreparedStatement pst = conn.prepareStatement("select id,fname,lname,email,phone from patient where reference = 'Cliniko' and corp_id = ? and check_patient_exist = 'Check' Limit 1");
      	 pst.setLong(1, corp.getId());
      	 ResultSet rs = pst.executeQuery();
      	 if(rs.next()){%>
      	 <tr>
      	<td><%=rs.getString("fname") %> <%=rs.getString("lname") %></td>
      	<td><%=rs.getString("email") %></td>
      	<td><%=rs.getString("phone") %></td>
      	</tr>	  
      	 <%}
      	  %>
        </table>
      	<input type="hidden" name="id" id="id" value="<%=rs.getString("id")%>">
      	<p></p>
        <p><b>Fiizio Patient Details:</b></p>  
        <table border="1" width="100%">
        <tr><th>Name</th><th>Email</th><th>Phone</th></tr>
        <%PreparedStatement pst1 = conn.prepareStatement("select fname,lname,email,phone from patient where (lower(fname) = ? and lower(lname) = ? or email = ? or phone = ?) and corp_id = ? and check_patient_exist='Checked'");
         pst1.setString(1, rs.getString("fname").toLowerCase());
         pst1.setString(2, rs.getString("lname").toLowerCase());
         pst1.setString(3, rs.getString("email").toLowerCase());
         pst1.setString(4, rs.getString("phone").toLowerCase());
         pst1.setLong(5, corp.getId());
      	 ResultSet rs1 = pst1.executeQuery();
      	 while(rs1.next()){%>
      	 <tr>
      	<td><%=rs1.getString("fname") %> <%=rs1.getString("lname") %></td>
      	<td><%=rs1.getString("email") %></td>
      	<td><%=rs1.getString("phone") %></td>
      	</tr>	 
      	 <%}
      	 pst.close();
      	 pst1.close(); 
      	 conn.close();%>
        </table>
<div class="form-actions">
<button class="btn btn-primary" style="float: left; margin: 0px 4px 0px -90px;" value="Use the Existing patient file" type="submit" name="submit">Use the existing patient file</button>or 
<button class="btn btn-primary" value="Create a New patient with the same name" type="submit" name="submit">Create a new patient file</button>
</div>
        </form>
    </div>
</div>
</body>
</html>