<%@page import="com.opensymphony.sitemesh.Content"%>
<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="java.util.List" %>
<%@ page import="org.apache.commons.fileupload.FileItem" %>
<%@ page import="org.apache.commons.fileupload.servlet.ServletFileUpload" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.io.BufferedReader" %>
<%@ page import="java.io.InputStreamReader" %>
<%@ page import="java.io.FileReader" %>
<%@ page import="java.io.InputStream" %>
<%
    CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");

    if (cu == null) {
        response.sendRedirect(Util.loginUrl(Constants.LoginReason.login, request));
        return;
    }
    
    User u = cu.getUser();
    Corporation corp = cu.getCorporation();

    LibraryManager lm = new LibraryManager();

    String[][] breadcrumbs = {{"/corp/", corp.getName()}, {null, "My Integrations"}};
    
    /* String error = null;
    
    if (request.getMethod().equalsIgnoreCase("post")) {
    	
    System.out.println("br"+request.getAttribute("filePath"));
    
    if (request.getAttribute("filePath") == null) {
        error = "Please select a .csv file";
    }else if (!request.getAttribute("filePath").toString().toLowerCase().endsWith(".csv")) {
        error = "Please uploaded a .csv file";
    }
    
    if (error == null) {
    String splitBy = ",";
    BufferedReader br = new BufferedReader(new FileReader(request.getAttribute("filePath").toString()));
    String input;
    int count = 1;
    while((input = br.readLine()) != null)
    {
            String[] b = input.split(splitBy);
            if(count == 1){
            if(!b[0].equals("Nookal ID")){
            	error = "Please uploaded a Nookal CSV file";
            	break;
            }
            }
    	if(count > 1){
            if (error == null) {
            try{
            if(!new LibraryManager().getPatient(b[19].toLowerCase())){
            	System.out.println("New Email>>>"+b[19]);
            	Patient p = new Patient();
            	p.setId(0);
            	p.setCreated(new Date());
            	p.setCreator(cu.getUser().getId());
            	p.setCorpId(cu.getCorporation().getId());
            	p.setFirstName(b[2]);
            	p.setLastName(b[3]);
            	p.setEmail(b[19].toLowerCase());
            	p.setReference("Nookal");
            	if (b[15].length()!=0){
            		p.setPhone(b[15]);
            	} else {
            		p.setPhone("");
            	}
            	try {
            		new LibraryManager().save(p);
            		request.setAttribute("Success", "Success");
            	} catch (Exception e) {
    			// TODO Auto-generated catch block
            		e.printStackTrace();
            		error = "Please uploaded a Nookal CSV file";
            		break;
            	}
            }else{
            	System.out.println("Old Email>>>"+b[19]);
            	request.setAttribute("Failure", "Failure");
            }
            }
            catch (Exception e) {
    			// TODO Auto-generated catch block
//             		e.printStackTrace();
            		error = "Please uploaded a Nookal CSV file";
            		break;
            	}
    	}
            }
    	count++;
    }
    System.out.println("Count : "+count);
    
    br.close();
    }
    } */
%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head><title>My Integrations</title>
<style type="text/css">
.span4{
	width: 270px;
}
#hb-cta-style-button {
    animation-duration: 5s;
    animation-iteration-count: infinite;
    animation-name: wiggle;
    animation-play-state: running;
    animation-timing-function: linear;
    backface-visibility: hidden;
    box-shadow: 0 0 1px rgba(0, 0, 0, 0);
    transform: translateZ(0px);
    display: inline-block;
}
</style>
</head>
<body>
<div class="page-header">
    <h1>My Integrations</h1>
</div>
<%@include file="/WEB-INF/includes/breadcrumbs.jsp"%>
<div class="span3 well">
        <h3>Import from Cliniko
        <%-- <%if(corp.getCliniko_apikey() == null){ %><img width="30" border="0" height="14" src="/images/new.gif"><%} %> --%></h3>
        <p>To import the patient details from Cliniko: Practice Management Software</p>
        <p>Please Update API Key first before you import patient details from Cliniko.</p>
        <table><tr>
        <td>
        <div id="hb-cta-style-button">
        <a id="hb-cta-style-button" href="inline/updateAPI.jsp?id=<%=u.getId()%>" class="btn btn-primary fancybox">Update API Key</a>
        </div>
        </td>
<!--         <td><p><a href="/corp/admin/Cliniko" class="btn btn-primary">Import</a></p></td> -->
        </tr></table>
 </div>
 <form action="upload_nookal_csv" method="post" class="form-horizontal" enctype="multipart/form-data">
 <%-- <div class="span3 well">
 <%@include file="/WEB-INF/includes/formErrorSimple.jsp"%>
        <h3>Import from Nookal</h3>
        <p>To import the patient details from Nookal: Practice Management Software</p>
        <p>Please upload a CSV file from Nookal.</p>
        <table><tr>
        <td><p><input type="file" name="uploadFile" id="uploadFile" accept=".csv" /></p></td>
        </tr><tr>
        <td><p><button class="btn btn-primary" value="Save" type="submit" name="submit">Import</button></p></td>
        </tr>
        </table>
        <%if(request.getAttribute("Success") == "Success"){ %>
        <p style="color: green; z-index: 1; position: absolute; margin: -33px 0px 0px 75px;">File Uploaded Successfully</p>
        <%} %>
        <%if(request.getAttribute("Failure") == "Failure"){ %>
        <p style="color: red; z-index: 1; position: absolute; margin: -33px 0px 0px 75px;">File Upload Failed</p>
        <%} %>
 </div> --%>
 </form>
</body>
</html>