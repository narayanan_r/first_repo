<%@ page import="au.com.ourbodycorp.Config" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%@ page import="au.com.ourbodycorp.model.managers.MiniNewsManager" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.List" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.PreparedStatement" %>
<%@ page import="java.sql.*" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");

    if (cu == null) {
        response.sendRedirect(Util.loginUrl(Constants.LoginReason.login, request));
        return;
    }
    User u = cu.getUser();
    Corporation corp = cu.getCorporation();
   /*  try{
    	Util.allActivitiesLogs("News", u.getId(), corp.getName(), "View News");
    }catch(Exception e){
    	
    } */
    String activity_page="corp/news/index.jsp";
	String corp_id=String.valueOf(corp.getId());
	String corp_name=corp.getName();
	String corp_loc=corp.getSuburb();
	String user_id=String.valueOf(u.getId());
	String user_name=u.getFullName();
	String invitor_id="";
	String invitor_name="---";
	String flag="view";
    String from="---";
    String to="---";
     try{
    	//Util.allActivitiesLogs("News", u.getId(), corp.getName(), "View News");
    	Util.allActivitiesLogs2("News",activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,"View News By User");
    }catch(Exception ee){
    	
    } 
    String[][] breadcrumbs = {{"/corp/", corp.getName()}, {"/corp/news/", "News"}};

    MiniNewsManager nm = new MiniNewsManager();

    List<MiniNews> news = nm.listNews(corp.getId(), 0, 0, null);
    SimpleDateFormat sdf = new SimpleDateFormat("d MMM yyyy HH:mm");
    sdf.setTimeZone(corp.getTimeZone());
    
    String action = request.getParameter("action");
    String infoMessage = null;
    if (action != null && action.equalsIgnoreCase("sendPush")){
    	/* try{
        	Util.allActivitiesLogs("News", u.getId(), corp.getName(), "Send Push");
        }catch(Exception e){
        	
        } */
        
        String newsId = request.getParameter("id");
        long newsIdAsLong1 = Long.parseLong(newsId);
        MiniNews newsItem1 = nm.getNews(newsIdAsLong1);
	 	try{
        	//Util.allActivitiesLogs("News", u.getId(), corp.getName(), "Send Push");
	    	Util.allActivitiesLogs2("News",activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,"new","Send Push By User","NewsName:"+newsItem1.getHeadline()+";Body:"+newsItem1.getBody(),"Send Push By User");
	    }catch(Exception ee){
	    	
	    }
        try {
        	Connection conn = Util.getConnection();
            conn.setAutoCommit(false);
            long newsIdAsLong = Long.parseLong(newsId);
            MiniNews newsItem = nm.getNews(newsIdAsLong);
            nm.send(newsItem);
            PreparedStatement pst = conn.prepareStatement("UPDATE mini_news set pushed = current_timestamp where id = ?");
            pst.setLong(1, newsIdAsLong);
            pst.executeUpdate();
            conn.commit();
            conn.close();
            infoMessage = "Sent push notification";
        }
        catch (NumberFormatException e){
            //Java, why can't you just give me zero if you don't know the number, exceptions are overkill
            e.printStackTrace();
        }
    }
%>
<html>
<head><title>News</title></head>
<body>
<div class="page-header">
    <h1>News <small>Keep your clients in the loop</small></h1>
</div>
<%@include file="/WEB-INF/includes/breadcrumbs.jsp"%>
<%@include file="/WEB-INF/includes/messageSimple.jsp"%>
<div class="row">
    <div class="span9">
        <a href="edit.jsp" class="btn btn-primary"><i class="icon-plus icon-white"></i> New Item</a>
    </div>
</div>
<div class="row">
    <div class="span9">
        <table class="table">
            <thead>
            <tr>
                <th>Title</th>
                <th>Date</th>
                <% if (corp != null && corp.isPushAllowed()){ %>
                <th>Push</th>
                <% } %>
                <th>Preview</th>
            </tr>
            </thead>
            <tbody>
            <%
                for (MiniNews n : news) {
            %>
            <tr>
                <td><a href="edit.jsp?id=<%=n.getId()%>"><%=n.getHeadline()%></a></td>
                <td><%=sdf.format(n.getCreated())%></td>
                <% if (corp != null && corp.isPushAllowed()){ %>
                    <td>
                        <a href="index.jsp?action=sendPush&id=<%=n.getId()%>" class="btn btn-mini">send push</a>
                    </td>
                <% } %>
                <td>
                <% if (Config.getString("https").equals("true")){ %>
                	<a href="https://<%=corp.getSlug()%>.<%=Config.getString("baseDomain")%>/news.jsp?id=<%=n.getId()%>" class="fancybox btn btn-mini">preview</a>
                <%}else{%>
                    <a href="http://<%=corp.getSlug()%>.<%=Config.getString("baseDomain")%>/news.jsp?id=<%=n.getId()%>" class="fancybox btn btn-mini">preview</a>
                <% }%>
                    <a href="inline/delete_news.jsp?id=<%=n.getId()%>&name=<%=n.getHeadline()%>" class="fancybox"><i class="icon-trash"></i></a>
                </td>
            </tr>
            <%
                }
            %>
            </tbody>
        </table>
    </div>
</div>
</body>
</html>