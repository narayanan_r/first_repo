<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="au.com.ourbodycorp.HTML" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.Corporation" %>
<%@ page import="au.com.ourbodycorp.model.CorporationUser" %>
<%@ page import="au.com.ourbodycorp.model.MiniNews" %>
<%@ page import="au.com.ourbodycorp.model.managers.AttachmentManager" %>
<%@ page import="au.com.ourbodycorp.model.managers.MiniNewsManager" %>
<%@ page import="au.com.ourbodycorp.model.managers.CorporationManager" %>
<%@ page import="au.com.ourbodycorp.model.managers.LibraryManager" %>
<%@ page import="au.com.ourbodycorp.model.User" %>
<%@	page import="java.util.ArrayList"%>
<%@ page import="java.util.List" %>
<%@ page import="java.net.URLEncoder" %>
<%
    CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");
	List<Long> Eideditlist;
    Corporation corp = cu.getCorporation();
    long id = Long.parseLong(Util.notNull(request.getParameter("id"), "0"));
    String name = request.getParameter("name");
    if(name != null){
    session.setAttribute("name", name);
    }
    String headline = session.getAttribute("name").toString();
	User u = cu.getUser();
	/* try{
    	Util.allActivitiesLogs("News", u.getId(), corp.getName(), "Delete News");
    }catch(Exception e){
    	
    } */
    MiniNews news = null;

    MiniNewsManager mnm = new MiniNewsManager();

    if (id != 0) {
        news = mnm.getNews(id);
        if (news == null || news.getCorpId() != corp.getId()) {
            response.sendError(404, "News item not found");
            return;
        }
    } else {
        response.sendError(400, "No note ID provided");
        return;
    }
    
    if (request.getMethod().equalsIgnoreCase("get")) {
    LibraryManager lm = new LibraryManager();
	List<Corporation> corpsid = null;
	List<Long> Eidlist = new ArrayList<Long>();
    if (u != null) {
        CorporationManager cm = new CorporationManager();
        if(!headline.equals("")){
        corpsid = lm.getCorpidforNews(headline,corp.getCompanyId(),news.getShareid(),id);
        }
    }
	
	if (corpsid != null && corpsid.size() > 0) {
	       if (corpsid != null && corpsid.size() > 0) {
	              for (Corporation corpu1 : corpsid) {
	    		  Eidlist.add(corpu1.getId());
	        }
	       }
	       session.setAttribute("Eidlist", Eidlist);
	}
    }
    if (request.getMethod().equalsIgnoreCase("post")) {
        if (request.getParameter("submit").startsWith("Cancel")) {
            response.sendRedirect(corp.getHost() + "/close.jsp");
        } else {
        	String type="News";
    	    String activity_page="corp/news/inline/delete_news.jsp";
    		String corp_id=String.valueOf(corp.getId());
    		String corp_name=corp.getName();
    		String corp_loc=corp.getSuburb();
    		String user_id=String.valueOf(u.getId());
    		String user_name=u.getFullName();
    		String invitor_id="";
    		String invitor_name="---";
    		String flag="new";
    		String from="Delete News from User";
    	    String to="NewsName:"+news.getHeadline()+";Body:"+news.getBody()+";Link:"+news.getLink();
    	    try{
    	    	//Util.allActivitiesLogs("News", u.getId(), corp.getName(), "Delete News");
            	Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,"Delete News By User");
            }catch(Exception ee){
            	
            }
            Eideditlist = (ArrayList<Long>)session.getAttribute("Eidlist");
        	if(Eideditlist != null){
        	for(Long Eid : Eideditlist){
            mnm.deleteNews(Eid);
        	}
        	}

            if (news.getPhotoId() > 0) {
                AttachmentManager am = new AttachmentManager();
                am.delete(news.getPhotoId());
            }

            response.sendRedirect(corp.getHost() + "/close.jsp?redir=" + URLEncoder.encode("/corp/news/", "UTF-8"));
        }
        return;
    }

 %>

<html>
<head><title>Delete News</title></head>
<body>
<div class="page-header">
    <h1>Delete News Item</h1>
</div>
<div class="row">
    <div class="span6">
        <p>
            Delete <i><%=news.getHeadline()%></i>.
        </p>
        <p>
            Are you sure?
        </p>
        <form action="delete_news.jsp" method="post">
            <input type="hidden" name="id" value="<%=news.getId()%>">
            <%=HTML.confirmCancel()%>
        </form>
    </div>
</div>
</body>
</html>