<%@page import="au.com.ourbodycorp.model.ProgramMessage"%>
<%@page import="au.com.ourbodycorp.model.managers.ProgramMessageManager"%>
<%@page import="au.com.ourbodycorp.Util"%>
<%@page import="au.com.ourbodycorp.model.managers.ScheduledMessageManager"%>
<%@ page import="au.com.ourbodycorp.Config" %>
<%@ page import="au.com.ourbodycorp.model.Attachment" %>
<%@ page import="au.com.ourbodycorp.model.ScheduledMessage" %>
<%@ page import="au.com.ourbodycorp.model.managers.AttachmentManager" %>
<%@ page import="au.com.ourbodycorp.Util" %>
<%@ page import="au.com.ourbodycorp.model.*" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    long smId = Long.parseLong(Util.notNull(request.getParameter("smid"), "0"));
    long pmId = Long.parseLong(Util.notNull(request.getParameter("pmid"), "0"));
   /*  CorporationUser cu = (CorporationUser) request.getAttribute("corpUser");
    User u = cu.getUser();
    Corporation corp = cu.getCorporation(); */
    /* try{
    	Util.allActivitiesLogs("Messages", u.getId(), corp.getName(), "View Scheduled Messages");
    }catch(Exception e){
    	
    } */

    if (smId == 0 && pmId == 0) {
        response.sendError(400, "No ID provided");
        return;
    }

    Attachment a = null;
    ProgramMessage programMessage = null;
    ScheduledMessage scheduledMessage = null;

    if (smId > 0) {
        ScheduledMessageManager messageManager = new ScheduledMessageManager();
        scheduledMessage = messageManager.getMessage(smId);
        if (scheduledMessage == null) {
            response.sendError(404);
            return;
        }

        if (scheduledMessage.getPhotoId() > 0) {
            AttachmentManager am = new AttachmentManager();
            a = am.getAttachment(scheduledMessage.getPhotoId(), false);
        }
    }

    if (pmId > 0) {
        ProgramMessageManager messageManager = new ProgramMessageManager();
        programMessage = messageManager.getMessage(pmId);
        if (programMessage == null) {
            response.sendError(404);
            return;
        }


        if (programMessage.getPhotoId() > 0) {
            AttachmentManager am = new AttachmentManager();
            a = am.getAttachment(programMessage.getPhotoId(), false);
        }
    }

    String title = (scheduledMessage != null) ? scheduledMessage.getHeadline() : programMessage.getHeadline();
    String body = (scheduledMessage != null) ? scheduledMessage.getBody() : programMessage.getBody();
    String messageLink = (scheduledMessage != null) ? scheduledMessage.getLink() : programMessage.getLink();
  
    /* String type="Messages";
    String activity_page="message.jsp";
	String corp_id=String.valueOf(corp.getId());
	String corp_name=corp.getName();
	String corp_loc=corp.getSuburb();
	String user_id=String.valueOf(u.getId());
	String user_name=u.getFullName();
	String invitor_id="---";
	String invitor_name="---";
	String flag="view";
	String from="---";
	String to="---";
	String desc="View Scheduled Messages for "+u.getFullName()+" By User"; 
    try
    {		
     Util.allActivitiesLogs2(type,activity_page,corp_id,corp_name,corp_loc,user_id,user_name,invitor_id,invitor_name,flag,from,to,desc);
    }
    catch(Exception e)
    {
    } */
    
    
%>

<html>
    <head>
        <title><%=title%></title>
        <base href="<%=Config.getString("url")%>" />
    </head>
    <body>
        <div class="row">
            <div class="span9">
                <div class="page-header">
                    <h1><%=title%></h1>
                </div>
                <div class="row">
                    <div class="span9">
                        <% if (a != null) {%>
                        <img src="<%=a.getImageUrl(Attachment.ImageSize.f320)%>">
                        <% }%>
                        <p>
                            <%=body.replaceAll("\n", "<br>")%>
                        </p>
                        <% if (messageLink != null && messageLink.trim().length() > 0) {
                                if (!messageLink.startsWith("http://") && !messageLink.startsWith("https://")) {
                                    messageLink = "http://" + messageLink;
                                }
                        %>
                        <p>
                            <a href="<%=messageLink%>" class="btn" target="_balnk">More Info</a>
                        </p>
                        <% }%>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>